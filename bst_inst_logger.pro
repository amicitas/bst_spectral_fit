

     


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2016-06-16
;
; PURPOSE:
;   Provide a logging mechanism for BST_SPECTRAL_FIT.
;
; DESCRIPTION:
;   This routine will handle all logging to terminal, files or to any GUI
;   message area.
;
;   This will be based on my MIR_LOGGER utility.
;
;   Individual logging actions should be added to the MIR_LOGGER dispatcher.
;
;
;-==============================================================================


     PRO BST_INST_LOGGER, message_in, SCOPE_LEVEL=scope_level_in, _REF_EXTRA=_extra

       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       
       MIR_LOGGER, message_in, SCOPE_LEVEL=scope_level, _STRICT_EXTRA=_extra
  
     END ; PRO BST_INST_LOGGER
