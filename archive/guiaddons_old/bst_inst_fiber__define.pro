
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from a file or a user defined function.
; 
; DESCRIPTION:
;   If the spectrum is to be loaded from a file, then the file should 
;   be an IDL sav file containing a variables named 'spectrum' that
;   contains the structure described below.
;
;   If the spectrum is loded from a user defined function then the 
;   function should return the structure described below.
;
;   spectrum = {y:array [,x:array][,weight:array]}
;
;   This structure should contain the following tags:
;     Tags:
;        y
;
;     Tags (Optional):
;        x 
;        weight
;
;   Each of the tags should contain an array.
;
;-======================================================================



     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIBER::INIT
       
       status = self->BST_INST_ADDON_GUI::INIT()

       self.menu_title = 'Fiber Illumination Fitter'
       self.enabled = 1

       self.use_tab = 1
       self.use_control = 0


       self.conflicting = ''


       self.full_debug = 0

       RETURN, status

     END ; FUNCTION BST_INST_FIBER::INIT




     ;+=================================================================
     ;
     ; Set the addon parameters to their defaults.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::RESET_PARAMS

       ; Nothing nothing tra la la . . .

     END ; PRO BST_INST_FIBER::RESET_PARAMS




     ;+=================================================================
     ;
     ; Initialize the USER SPECTRA addon.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::ENABLE

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::ENABLE

     END ; PRO BST_INST_FIBER::ENABLE





     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_FIBER::DISABLE

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::DISABLE


     END ;PRO BST_INST_FIBER::DISABLE





     ;+=================================================================
     ; 
     ; Return a structure containing anything should should be saved
     ; for this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIBER::GET_STATE

       BST_INST_MESSAGE, 'Saving the sate of the '+OBJ_CLASS(self)+' addon.', /LOG

       save_structure = BUILD_STRUCTURE('active', self.active, STRUCT=save_structure)

       RETURN, save_structure

     END ; FUNCTION BST_INST_FIBER::GET_STATE




     ;+=================================================================
     ;
     ; Recieve a structure, of the same type as given in in
     ; [GET_STATE], and load the contents.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::LOAD_STATE, save_structure

       BST_INST_MESSAGE, 'Loading the save state for the '+OBJ_CLASS(self)+' addon.', /LOG

       ; First check if the save_structure has this addon as loaded.
       CASE 1 OF
         (save_structure.active AND NOT self.active): BEGIN
           self->ENABLE
         END
         (NOT save_structure.active AND self.active): BEGIN
           self->DISABLE
         END
         ELSE:
       ENDCASE
         
     END ; PRO BST_INST_FIBER::LOAD_STATE





; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================





     ;+=================================================================
     ;
     ; Create a compound widget for loading spectra from a 
     ; user defined function or from a file.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::BUILD_GUI
                                        
       ui_base = WIDGET_BASE(self.gui_id, /COLUMN)


       options = ['Use' $
                  ,'Save' $
                  ,'Fix All' $
                  ,'Fix Spacing' $
                  ,'Fix Intensity' $
                  ,'Fix Shift' $
                  ,'Set First Intensity to 1.0']

       ui_base_top = WIDGET_BASE(ui_base, /ROW)
         ui_bgroup_options = CW_BGROUP(ui_base_top, options $
                                       ,UNAME='ui_bgroup_options' $
                                       ,/NONEXCLUSIVE $
                                       ,FRAME=1 $
                                       ,/ROW)

       ui_base_values = WIDGET_BASE(ui_base, /ROW)
         ui_field_spacing = CW_FIELD_EXT(ui_base_values, UNAME='ui_field_spacing' $
                                         ,VALUE=0 $
                                         ,TITLE='Fiber Spacing' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,XSIZE=6 $
                                         ,FORMAT='(f-6.2)' $
                                        )
         ui_field_shift = CW_FIELD_EXT(ui_base_values, UNAME='ui_field_shift' $
                                         ,VALUE=0 $
                                         ,TITLE='Fiber Shift' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,XSIZE=6 $
                                         ,FORMAT='(f-6.2)' $
                                        )
         ui_field_profile = CW_FIELD_EXT(ui_base_values, UNAME='ui_field_profile' $
                                         ,VALUE=0 $
                                         ,TITLE='Profile' $
                                         ,/INT $
                                         ,/ALL_EVENTS $
                                         ,XSIZE=4 $
                                         ,MAXLENGTH=4 $
                                        )

         ui_button_update = WIDGET_BUTTON(ui_base, UNAME='ui_button_update' $
                                          ,VALUE='Update' $
                                         )


       id_struct = { $
                     ui_base:ui_base $
                     ,ui_bgroup_options:ui_bgroup_options $
                     ,ui_field_spacing:ui_field_spacing $
                     ,ui_field_shift:ui_field_shift $
                     ,ui_field_profile:ui_field_profile $
                     ,ui_button_update:ui_button_update $
                   }

       state = { $
                 id_struct:id_struct $
               }

       WIDGET_CONTROL, ui_base, SET_UVALUE=state

     END ; PRO BST_INST_FIBER::BUILD_GUI
     


     ;+=================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::EVENT_HANDLER, event
       COMMON BST_INSTRUMENTAL_FIT

       ; Establish catch block.
       CATCH, error          

       ; An error occurs.         
       IF (error NE 0) THEN BEGIN 
         ; Cancel catch block.
         CATCH, /CANCEL

         IF N_ELEMENTS(state) NE 0 THEN BEGIN
           WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY
         ENDIF
           
         BST_INST_MESSAGE, STRING('Caught in BST_INST_FIBER::EVENT_HANDLER')
         BST_INST_MESSAGE, STRING('Error: ', !ERROR_STATE.MSG)

         IF self.full_debug THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF

         RETURN
       ENDIF  



       ; First call the superclass method
       self->BST_INST_ADDON_GUI::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       ; Get the state holder.
       state_holder = WIDGET_INFO(Event.Handler, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY


       ; Start main event loop
       CASE event.id OF

         ;ui_bgroup_options
         state.id_struct.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           num_fibers = c_inst_model.bst_inst_fiber_illumination->GET(/NUM_FIBERS)
           intensity = INTARR(num_fibers)

           ; First set all parameters according to the options.
           ; This will also set the parameters for the first fiber.
           struct = { $
                      use:value[0] $
                      ,save:value[1] $
                      ,fixall:value[2] $
                      ,fixed:{ $
                               spacing:value[3] $
                               ,intensity:value[4] $
                               ,shift:value[5] $
                             } $
                    }
           self->SET, struct

           ; Now set things correctly for the first fiber.
           IF value[6] THEN BEGIN
             self->FIX_FIRST_INTENSITY
           ENDIF

         END

         ;ui_field_spacing
         state.id_struct.ui_field_spacing: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           struct = { $
                      param:{spacing:value} $
                    }

           self->SET, struct
         END

         ;ui_field_shift
         state.id_struct.ui_field_shift: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           struct = { $
                      param:{shift:value} $
                    }

           self->SET, struct
         END

         ;ui_field_profile
         state.id_struct.ui_field_profile: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           struct = { $
                      profile:value $
                    }

           self->SET, struct
         END

         ;ui_button_update
         state.id_struct.ui_button_update: BEGIN
           WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY
           self->UPDATE
           WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
         END

         ELSE: BEGIN
           uname = WIDGET_INFO(event.id, /UNAME)
           MESSAGE, STRING('No rule for: ', uname)
         ENDELSE
       ENDCASE


       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ;PRO BST_INST_FIBER::EVENT_HANDLER




     ;+=================================================================
     ;
     ; Set the intensity of the first fiber to 1.0 and fix.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::FIX_FIRST_INTENSITY, UNFIX=unfix
       COMMON BST_INSTRUMENTAL_FIT

       IF KEYWORD_SET(unfix) THEN BEGIN

         model_struct = c_inst_model.bst_inst_fiber_illumination->GET_STRUCT()
         model_struct.fixed.intensity[0] = 0
         c_inst_model.bst_inst_fiber_illumination->COPY_FROM, model_struct

       ENDIF ELSE BEGIN

         model_struct = c_inst_model.bst_inst_fiber_illumination->GET_STRUCT()
         model_struct.param.intensity[0] = 1.0
         model_struct.fixed.intensity[0] = 1
         c_inst_model.bst_inst_fiber_illumination->COPY_FROM, model_struct

       ENDELSE

       
       
     END ;PRO BST_INST_FIBER::FIX_FIRST_INTENSITY




     ;+=================================================================
     ;
     ; Update the gui from the current parameters in the
     ; common block.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::UPDATE_GUI
       COMMON BST_INSTRUMENTAL_FIT

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY

       ; Get the structure from the model
       model_struct = c_inst_model.bst_inst_fiber_illumination->GET_STRUCT()

       ; Check if the first fiber is constrained.
       IF model_struct.fixed.intensity[0] AND (model_struct.param.intensity[0] EQ 1.0) THEN BEGIN
         fix_first_fiber = 1
       ENDIF ELSE BEGIN
         fix_first_fiber = 0
       ENDELSE

       ; Update the options
       value = [ $
                 model_struct.use $
                 ,model_struct.save $
                 ,model_struct.fixall $
                 ,model_struct.fixed.spacing $
                 ,model_struct.fixed.intensity[1] $
                 ,model_struct.fixed.shift $
                 ,fix_first_fiber $
               ]
       WIDGET_CONTROL, state.id_struct.ui_bgroup_options, SET_VALUE=value
           
       ; Update the spacing
       WIDGET_CONTROL, state.id_struct.ui_field_spacing, SET_VALUE=model_struct.param.spacing
       WIDGET_CONTROL, state.id_struct.ui_field_shift, SET_VALUE=model_struct.param.shift
       WIDGET_CONTROL, state.id_struct.ui_field_profile, SET_VALUE=model_struct.profile


       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ;PRO BST_INST_FIBER::UPDATE_GUI



     ;+=================================================================
     ;
     ; Set the model parameters
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::SET, struct
       COMMON BST_INSTRUMENTAL_FIT

       c_inst_model.bst_inst_fiber_illumination->COPY_FROM, struct
       
       
     END ; BST_INST_FIBER::SET, struct



     ;+=================================================================
     ;
     ; Get the model parameters
     ;
     ;-=================================================================
     PRO BST_INST_FIBER::GET, get
       COMMON BST_INSTRUMENTAL_FIT

       c_inst_model.bst_inst_fiber_illumination->COPY_TO, struct
       
     END ; BST_INST_FIBER::GET, struct



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_FIBER__DEFINE

       struct = { BST_INST_FIBER $
                  ,INHERITS BST_INST_ADDON_GUI }

     END ; PRO BST_INST_FIBER__DEFINE
