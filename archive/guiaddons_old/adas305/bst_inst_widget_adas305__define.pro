




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A widget to contral GAUSSIAN models for <BST_SPECTRAL_FIT>
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_ADAS305::INIT, model $
                                                ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_ADAS305::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_ADAS305::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_ADAS305::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_WIDGET_ADAS305::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_ADAS305::GUI_DEFINE, FONT=LabelFont $
                                             ,FRAME=frame $
                                             ,SUB_FRAME=sub_frame $
                                             ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 0
       MIR_DEFAULT, sub_frame, 1


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/COLUMN $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)

       ui_bgroup_use = CW_BGROUP(ui_base, UNAME='ui_bgroup_use' $
                                 ,'Use' $
                                 ,/NONEXCLUSIVE $
                                 ,FRAME=0)

       ui_base_sub = WIDGET_BASE(ui_base, UNAME='ui_base_sub' $
                                 ,/COLUMN $
                                 ,ALIGN_CENTER=align_center $
                                 ,SENSITIVE=0 $
                                 ,FRAME=sub_frame)

         option_names = ['fix', 'save', 'full component only']
         ui_bgroup_options = CW_BGROUP(ui_base_sub, UNAME='ui_bgroup_options' $
                                       ,option_names $
                                       ,/NONEXCLUSIVE $
                                       ,/ROW)



         label_size = 100

         ui_base_pi_sigma_ratio = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_pi_sigma_ratio = CW_FIELD_EXT(ui_base_pi_sigma_ratio, UNAME='ui_field_pi_sigma_ratio' $
                                         ,VALUE='' $
                                         ,TITLE='Pi/Sigma Ratio' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_pi_sigma_ratio = CW_BGROUP(ui_base_pi_sigma_ratio, UNAME='ui_bgroup_pi_sigma_ratio' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_beam_energy = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_beam_energy = CW_FIELD_EXT(ui_base_beam_energy, UNAME='ui_field_beam_energy' $
                                         ,VALUE='' $
                                         ,TITLE='Beam Energy' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_beam_energy = CW_BGROUP(ui_base_beam_energy, UNAME='ui_bgroup_beam_energy' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_bfield_toroidal = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_bfield_toroidal = CW_FIELD_EXT(ui_base_bfield_toroidal $
                                                    , UNAME='ui_field_bfield_toroidal' $
                                                    ,VALUE='' $
                                                    ,TITLE='B Field Toroidal' $
                                                    ,/DOUBLE $
                                                    ,/ALL_EVENTS $
                                                    ,FORMAT='(f0.3)' $
                                                    ,XSIZE=10 $
                                                    ,LABEL_XSIZE=label_size)
           ui_bgroup_bfield_toroidal = CW_BGROUP(ui_base_bfield_toroidal $
                                                  , UNAME='ui_bgroup_bfield_toroidal' $
                                                  ,'fix' $
                                                  ,/NONEXCLUSIVE)

         ui_base_bfield_z = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_bfield_z = CW_FIELD_EXT(ui_base_bfield_z $
                                                    , UNAME='ui_field_bfield_z' $
                                                    ,VALUE='' $
                                                    ,TITLE='B Field Z' $
                                                    ,/DOUBLE $
                                                    ,/ALL_EVENTS $
                                                    ,FORMAT='(f0.3)' $
                                                    ,XSIZE=10 $
                                                    ,LABEL_XSIZE=label_size)
           ui_bgroup_bfield_z = CW_BGROUP(ui_base_bfield_z $
                                                  , UNAME='ui_bgroup_bfield_z' $
                                                  ,'fix' $
                                                  ,/NONEXCLUSIVE)

         ui_base_temperature = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_temperature = CW_FIELD_EXT(ui_base_temperature, UNAME='ui_field_temperature' $
                                         ,VALUE='' $
                                         ,TITLE='Temperature' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                               ,LABEL_XSIZE=label_size)
           ui_bgroup_temperature = CW_BGROUP(ui_base_temperature, UNAME='ui_bgroup_temperature' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_lambda0 = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_lambda0 = CW_FIELD_EXT(ui_base_lambda0, UNAME='ui_field_lambda0' $
                                         ,VALUE='' $
                                         ,TITLE='Lambda0' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                           ,LABEL_XSIZE=label_size)
           ui_bgroup_lambda0 = CW_BGROUP(ui_base_lambda0, UNAME='ui_bgroup_lambda0' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_intensity = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_intensity_full = CW_FIELD_EXT(ui_base_intensity $
                                                  ,UNAME='ui_field_intensity_full' $
                                                  ,VALUE='' $
                                                  ,TITLE='Intensity Full' $
                                                  ,/DOUBLE $
                                                  ,/ALL_EVENTS $
                                                  ,FORMAT='(e0.3)' $
                                                  ,XSIZE=10 $
                                                  ,LABEL_XSIZE=label_size)
           ui_field_intensity_half = CW_FIELD_EXT(ui_base_intensity $
                                                  ,UNAME='ui_field_intensity_half' $
                                                  ,VALUE='' $
                                                  ,TITLE='Intensity Half' $
                                                  ,/DOUBLE $
                                                  ,/ALL_EVENTS $
                                                  ,FORMAT='(e0.3)' $
                                                  ,XSIZE=10 $
                                                  ,LABEL_XSIZE=label_size)
           ui_field_intensity_third = CW_FIELD_EXT(ui_base_intensity $
                                                  ,UNAME='ui_field_intensity_third' $
                                                  ,VALUE='' $
                                                  ,TITLE='Intensity Third' $
                                                  ,/DOUBLE $
                                                  ,/ALL_EVENTS $
                                                  ,FORMAT='(e0.3)' $
                                                  ,XSIZE=10 $
                                                  ,LABEL_XSIZE=label_size)
           ui_bgroup_intensity = CW_BGROUP(ui_base_intensity, UNAME='ui_bgroup_intensity' $
                                           ,'fix' $
                                           ,/NONEXCLUSIVE)





       id_struct = { $
                     ui_bgroup_use:ui_bgroup_use $
                     ,ui_base_sub:ui_base_sub $
                     ,ui_bgroup_options:ui_bgroup_options $

                     ,ui_field_pi_sigma_ratio:ui_field_pi_sigma_ratio $
                     ,ui_bgroup_pi_sigma_ratio:ui_bgroup_pi_sigma_ratio $

                     ,ui_field_beam_energy:ui_field_beam_energy $
                     ,ui_bgroup_beam_energy:ui_bgroup_beam_energy $

                     ,ui_field_bfield_toroidal:ui_field_bfield_toroidal $
                     ,ui_bgroup_bfield_toroidal:ui_bgroup_bfield_toroidal $

                     ,ui_field_bfield_z:ui_field_bfield_z $
                     ,ui_bgroup_bfield_z:ui_bgroup_bfield_z $

                     ,ui_field_temperature:ui_field_temperature $
                     ,ui_bgroup_temperature:ui_bgroup_temperature $

                     ,ui_field_lambda0:ui_field_lambda0 $
                     ,ui_bgroup_lambda0:ui_bgroup_lambda0 $

                     ,ui_field_intensity_full:ui_field_intensity_full $
                     ,ui_field_intensity_half:ui_field_intensity_half $
                     ,ui_field_intensity_third:ui_field_intensity_third $
                     ,ui_bgroup_intensity:ui_bgroup_intensity $
                     }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_ADAS305::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_ADAS305::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF


         id.ui_bgroup_use: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=use
           self.model->SET_USE, use[0]
           self->SET_USE, use[0], /LEAVE_BUTTON
         END


         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_options, GET_VALUE=value_bgroup_options

           ; Deal with freezing/unfreezing parameters
           self.model->SET_FIXALL, value_bgroup_options[0]

           ; Deal with saving/unsaving parameters
           self.model->SET_SAVE, value_bgroup_options[1]

           ; Deal with the full_component_only option.
           self.model->SET_OPTIONS, {full_component_only:value_bgroup_options[2]}
         END


         id.ui_field_pi_sigma_ratio: BEGIN
           self.model->SET_PARAM, {pi_sigma_ratio:event.value}
         END
         id.ui_bgroup_pi_sigma_ratio: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {pi_sigma_ratio:fix[0]}
         END



         id.ui_field_beam_energy: BEGIN
           self.model->SET_PARAM, {beam_energy:event.value}
         END
         id.ui_bgroup_beam_energy: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {beam_energy:fix[0]}
         END



         id.ui_field_bfield_toroidal: BEGIN
           self.model->SET_PARAM, {bfield_toroidal:event.value}
         END
         id.ui_bgroup_bfield_toroidal: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {bfield_toroidal:fix[0]}
         END



         id.ui_field_bfield_z: BEGIN
           self.model->SET_PARAM, {bfield_z:event.value}
         END
         id.ui_bgroup_bfield_z: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {bfield_z:fix[0]}
         END



         id.ui_field_temperature: BEGIN
           self.model->SET_PARAM, {temperature:event.value}
         END
         id.ui_bgroup_temperature: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {temperature:fix[0]}
         END



         id.ui_field_lambda0: BEGIN
           self.model->SET_PARAM, {lambda0:event.value}
         END
         id.ui_bgroup_lambda0: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {lambda0:fix[0]}
         END


         id.ui_field_intensity_full: BEGIN
           self.model->SET_PARAM, {intensity:{full:event.value}}
         END
         id.ui_field_intensity_half: BEGIN
           self.model->SET_PARAM, {intensity:{half:event.value}}
         END
         id.ui_field_intensity_third: BEGIN
           self.model->SET_PARAM, {intensity:{third:event.value}}
         END
         id.ui_bgroup_intensity: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {intensity:{full:fix[0], half:fix[0], third:fix[0]}}
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_ADAS305::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_ADAS305::UPDATE
       
       
       id = self->GET_ID_STRUCT()


       options = self.model->GET_OPTIONS()
       param = self.model->GET_PARAM()
       fixed = self.model->GET_FIXED()

       self->SET_USE, self.model->USE()

       bg_group_value = [self.model->GET_FIXALL(), self.model->SAVE(), options.full_component_only]
       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=bg_group_value

       WIDGET_CONTROL, id.ui_field_pi_sigma_ratio, SET_VALUE=param.pi_sigma_ratio
       WIDGET_CONTROL, id.ui_bgroup_pi_sigma_ratio, SET_VALUE=[fixed.pi_sigma_ratio]

       WIDGET_CONTROL, id.ui_field_beam_energy, SET_VALUE=param.beam_energy
       WIDGET_CONTROL, id.ui_bgroup_beam_energy, SET_VALUE=[fixed.beam_energy]

       WIDGET_CONTROL, id.ui_field_bfield_toroidal, SET_VALUE=param.bfield_toroidal
       WIDGET_CONTROL, id.ui_bgroup_bfield_toroidal, SET_VALUE=[fixed.bfield_toroidal]

       WIDGET_CONTROL, id.ui_field_bfield_z, SET_VALUE=param.bfield_z
       WIDGET_CONTROL, id.ui_bgroup_bfield_z, SET_VALUE=[fixed.bfield_z]

       WIDGET_CONTROL, id.ui_field_temperature, SET_VALUE=param.temperature
       WIDGET_CONTROL, id.ui_bgroup_temperature, SET_VALUE=[fixed.temperature]

       WIDGET_CONTROL, id.ui_field_lambda0, SET_VALUE=param.lambda0
       WIDGET_CONTROL, id.ui_bgroup_lambda0, SET_VALUE=[fixed.lambda0]

       WIDGET_CONTROL, id.ui_field_intensity_full, SET_VALUE=param.intensity.full
       WIDGET_CONTROL, id.ui_field_intensity_half, SET_VALUE=param.intensity.half
       WIDGET_CONTROL, id.ui_field_intensity_third, SET_VALUE=param.intensity.third
       WIDGET_CONTROL, id.ui_bgroup_intensity, SET_VALUE=[fixed.intensity.full]

     END ; PRO BST_INST_WIDGET_ADAS305::UPDATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_ADAS305::SET_USE, use, LEAVE_BUTTON=leave_button
       
       id = self->GET_ID_STRUCT()


       ; This keyword is useful when handing button events.
       IF ~ KEYWORD_SET(leave_button) THEN BEGIN
         WIDGET_CONTROL, id.ui_bgroup_use, SET_VALUE = [use]
       ENDIF

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base_sub, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_ADAS305::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_ADAS305__DEFINE

       struct = { BST_INST_WIDGET_ADAS305 $
                           
                  ,model:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_ADAS305__DEFINE
