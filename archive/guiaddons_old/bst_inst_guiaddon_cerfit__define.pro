


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This addon handles importing of CERFIT results into the B-Stark
;   model.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERFIT::SET_FLAGS

       self->BST_INST_GUIADDON_GUI::SET_FLAGS

       self.active = 0

       self.use_tab = 1
       self.use_control = 0

       self.title = 'CERFIT'

       self.add_to_menu = 1
       self.menu_title = 'CERFIT'

       self.required = 'BST_INST_GUIADDON_BSTARK'

     END ; PRO BST_INST_GUIADDON_CERFIT::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Take any actions when instantiated.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERFIT::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON_GUI::INIT(dispatcher)

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_CERFIT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ; 
     ; DESCRIPTION:
     ;   This is called after all addons have been instantiated.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERFIT::INITIALIZE
       self->BST_INST_GUIADDON::INITIALIZE

       fitter_core = (self.dispatcher)->GET_CORE()
       self.model = fitter_core.models->GET_MODEL('BST_INST_MODEL_BSTARK')

     END ;PRO BST_INST_GUIADDON_CERFIT::INITIALIZE



     ;+=================================================================
     ; PURPOSE:
     ;   Perform prefit actions before the fit is started.
     ;
     ;   This routines loads the shot and chord information into
     ;   the [BST_INST_MODEL_CERFIT::] model.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERFIT::PREFIT

       
     END ; PRO BST_INST_GUIADDON_CERFIT::PREFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_CERFIT::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERFIT::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)


     END ;PRO BST_INST_GUIADDON_CERFIT::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERFIT::UPDATE_GUI

       self.widget->UPDATE

     END ; PRO BST_INST_GUIADDON_CERFIT::UPDATE_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     ;PRO BST_INST_GUIADDON_CERFIT::CLEANUP

     ;END ; PRO BST_INST_GUIADDON_CERFIT::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERFIT__DEFINE

       struct = { BST_INST_GUIADDON_CERFIT $

                  ,model:OBJ_NEW() $

                  ,INHERITS BST_INST_GUIADDON_GUI }

     END ; PRO BST_INST_GUIADDON_CERFIT__DEFINE
