

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from CER or B-STARK chord data using CERVIEW.
;
;   This is the default addon FOR <BST_SPECTRAL_FIT> and will
;   be loaded on startup.
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::SET_FLAGS

       self->BST_INST_GUIADDON_GUI::SET_FLAGS

       self.version.number = [1,0,0]
       self.version.date = [2009,01,26]

       self.active = 1
       self.default = 1

       ; For this addon the placement of the gui is handled by the
       ; internal option option.compact.
       self.use_tab = 0
       self.use_control = 0
       self.use_window = 0

       self.add_to_menu = 1
       self.menu_title = 'SPECTRA: CER and B-STARK'

       self.title = 'CERVIEW'

       self.conflicting = 'BST_INST_GUIADDON_USER_SPECTRA, BST_INST_GUIADDON_DETECTOR_VIEWER'

     END ; PRO BST_INST_GUIADDON_CERVIEW::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::INIT, dispatcher
       
       RESOLVE_ROUTINE, [ $
                           'bst_cerview' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE


       status = self->BST_INST_GUIADDON_GUI::INIT(dispatcher)

       self.options.compact = 1

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_CERVIEW::INIT

     


     ;+=================================================================
     ;
     ; Return a structure with the default parameters for the
     ; CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::RESET, _NULL=_null
           
       ; Set the current timelice to an invalid number.
       self.param.timeslice = -1

       self->SET_PARAM, { $
                          shot:133494L $
                          ,chord:'b01' $
                          ,beam:'' $
                          ,dark_shot:0L $ 
                          ,white:1 $
                          ,raw:0 $  
                          ,lambda0:0D $
                          
                          ,ts_min:102 $            
                          ,ts_average:1 $
                          ,ts_sub:0 $
                          ,ts_sub_average:0 $ 
                          ,ts_group_step:0 $
                          ,ts_group_average:0 $

                          ,ts_max:102 $
                        }
 
       self->BST_INST_GUIADDON_GUI::RESET

     END ; PRO BST_INST_GUIADDON_CERVIEW::RESET




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the CERVIEW addon.
     ;
     ; PROGRAMMING NOTES:
     ;   Note here that we do not check if this addon is already
     ;   enabled.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::ENABLE

       ; Call the parent method.
       self->BST_INST_GUIADDON_GUI::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       fitter = self.dispatcher->GET_CORE()
       fitter->SET_SPECTRUM_FUNCTION, 'spectrum', self

     END ; PRO BST_INST_GUIADDON_CERVIEW::ENABLE



     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::DISABLE

       ; Clean up the gui.
       self->GUI_CLEANUP

       ; Call the parent method.
       self->BST_INST_GUIADDON_GUI::DISABLE

       IF self->IS_ENABLED() THEN BEGIN
         fitter = self.dispatcher->GET_CORE()
         fitter->SET_SPECTRUM_FUNCTION, /DEFAULT
       ENDIF

     END ;PRO BST_INST_GUIADDON_CERVIEW::DISABLE
     


     ;+=================================================================
     ;
     ; Return a prefix to be used for automatic filenaming
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::GET_FILENAME_PREFIX, NO_BEAMNAME=no_beamname

       chord_char6 = BST_CHORD_NAME_PARSE(self.param.chord, /CHAR3)

       filename_prefix = STRING(self.param.shot, '_', chord_char6 $
                                ,FORMAT='(i0,a0,a0)')


       ; The /NO_BEAMNAME is a temporary workaround untill
       ; the BST_INST_OUTPUT rountines are fixed.
       IF ~ KEYWORD_SET(no_beamname) THEN BEGIN
         ; Add the beam if it was defined.
         IF self.param.beam NE '' THEN BEGIN
           filename_prefix += '_'+self.param.beam
         ENDIF
       ENDIF

       RETURN, STRLOWCASE(filename_prefix)

     END ; FUNCTION BST_INST_GUIADDON_CERVIEW::GET_FILENAME_PREFIX



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the state of addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::GET_STATE
       
       state = self->BST_INST_GUIADDON::GET_STATE()
       state = CREATE_STRUCT(state, {param:self.param })

       RETURN, state

     END ; FUNCTION BST_INST_GUIADDON_CERVIEW::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load a save structure for this addon.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::LOAD_STATE, state

       self->BST_INST_GUIADDON_GUI::LOAD_STATE, state

       STRUCT_ASSIGN, {param:state.param}, self, /NOZERO

     END ; PRO BST_INST_GUIADDON_CERVIEW::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::GET_PARAM


       RETURN, self.param

     END ; FUNCTION BST_INST_GUIADDON_CERVIEW::GET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Load the addon parameters from a structure.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::SET_PARAM, param

       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ; PRO BST_INST_GUIADDON_CERVIEW::SET_PARAM




     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the output parameters for this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::GET_OUTPUT_STRUCTURE

       RETURN, self.param

     END ; FUNCTION BST_INST_GUIADDON_CERVIEW::GET_OUTPUT_STRUCTURE



; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Switch between compact and full mode.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::SET_COMPACT, compact

       MIR_DEFAULT, compact, 1

       IF compact EQ self.options.compact THEN RETURN

       self.options.compact = compact

       ; Disable and then reinable the addon.
       ; This will force the GUI to be rebuilt.
       self->DISABLE
       self->ENABLE

     END ;PRO BST_INST_GUIADDON_CERVIEW::SET_COMPACT


     ;+=================================================================
     ; PURPOSE:
     ;   Handle switching where the gui gets placed.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::BUILD_GUI

       IF KEYWORD_SET(self.options.compact) THEN BEGIN
         self.use_control = 1
         self.use_tab = 0
       ENDIF ELSE BEGIN
         self.use_control = 0
         self.use_tab = 1
       ENDELSE

       self->BST_INST_GUIADDON_GUI::BUILD_GUI

     END ;PRO BST_INST_GUIADDON_CERVIEW::BUILD_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_CERVIEW::] 
     ;   addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::GUI_DEFINE

       ; Switch between the full and compact versions.
       IF KEYWORD_SET(self.options.compact) THEN BEGIN
         self->GUI_DEFINE_COMPACT
       ENDIF ELSE BEGIN
         self->GUI_DEFINE_FULL
       ENDELSE

     END ;PRO BST_INST_GUIADDON_CERVIEW::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Build the compact version of the GUI components for the 
     ;   [BST_INST_GUIADDON_CERVIEW::] addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::GUI_DEFINE_COMPACT

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)
       
       ; Add a widget to control the addon
       self->REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_CERVIEW_COMPACT' $
                             ,self $
                             ,DESTINATION=ui_base)


     END ;PRO BST_INST_GUIADDON_CERVIEW::GUI_DEFINE_COMPACT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the compact version of the GUI components for the 
     ;   [BST_INST_GUIADDON_CERVIEW::] addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::GUI_DEFINE_FULL

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)
       
       ; Add a widget to control the addon
       self->REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_CERVIEW_FULL' $
                             ,self $
                             ,DESTINATION=ui_base)



       ; ---------------------------------------------------------------
       ; Set up an 'ANALYZE ALL' button.
       fitter_gui = self.dispatcher->GET_GUI()
       ui_base_fit_control =  fitter_gui->GET_GUI_ID(/BASE_FIT_BUTTONS)


       ui_button_analyze_all = WIDGET_BUTTON(ui_base_fit_control $
                                             ,UNAME='ui_button_analyze_all' $
                                             ,VALUE='Analyze All' $
                                             ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                             ,UVALUE=self $
                                             ,XSIZE=160 $
                                             ,YSIZE=40 $
                                            )

       ; Store all of the accesable widgit id's here.
       id_struct = { $
                     ui_button_analyze_all:ui_button_analyze_all $
                   }

       ; Save the id structure.
       self->SET_ID_STRUCT, id_struct


     END ;PRO BST_INST_GUIADDON_CERVIEW::GUI_DEFINE_COMPACT
     


     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF  


       ; First call the superclass method
       self->BST_INST_GUIADDON_GUI::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       ; Handle events that are specific to compact or full mode.
       IF self.options.compact THEN BEGIN
         self->EVENT_HANDLER_COMPACT, event, EVENT_HANDLED=event_handled
       ENDIF ELSE BEGIN
         self->EVENT_HANDLER_FULL, event, EVENT_HANDLED=event_handled
       ENDELSE
       IF event_handled THEN RETURN


       ; Handle all of the last events.
       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_CERVIEW::EVENT_HANDLER

     
     
     ;+=================================================================
     ; PURPOSE:
     ;  Handle events for the CERVIEW addon that are specific to
     ;  compact mode.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::EVENT_HANDLER_COMPACT, event, EVENT_HANDLED=event_handled

       event_handled = 1

       ; Start main event loop
       CASE event.id OF

         ELSE: BEGIN
           event_handled = 0
         ENDELSE

       ENDCASE

     END ;PRO BST_INST_GUIADDON_CERVIEW::EVENT_HANDLER_COMPACT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;  Handle events for the CERVIEW addon that are specific to
     ;  full mode.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::EVENT_HANDLER_FULL, event, EVENT_HANDLED=event_handled


       id = self->GET_ID_STRUCT()
       event_handled = 1

       ; Start main event loop
       CASE event.id OF

         id.ui_button_analyze_all: BEGIN
           self->FIT_ALL_TIMESLICES
         END

         ELSE: BEGIN
           event_handled = 0
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_CERVIEW::EVENT_HANDLER_FULL



     ;+=================================================================
     ; PURPOSE:
     ;   Update the gui from the current parameters in the
     ;   common block.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::UPDATE_GUI

       self.widget->UPDATE
       
     END ; PRO BST_INST_GUIADDON_CERVIEW::UPDATE_GUI




     ;+=================================================================
     ; PURPOSE:
     ;   Clean up the gui when the addon is disabled
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::GUI_CLEANUP

       ; This button only exists in full mode.
       id = self->GET_ID_STRUCT()
       IF HAS_TAG(id, 'ui_button_analyze_all') THEN BEGIN
         WIDGET_CONTROL, id.ui_button_analyze_all, /DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_BSTARK::GUI_CLEANUP



; ======================================================================
; ======================================================================
; ######################################################################
;
; FIT LOOP HOOKS
;
;   These methods that will get called as part of the fitting
;   procedure.
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Take action before the fit loop is started.
     ;
     ; DESCRITPION:
     ;   Here we control the self.param.timeslice parameter. Because of
     ;   the possibility of doing multiple timeslice fits we need
     ;   to be able to save the current timeslice being fit when the
     ;   results are saved.
     ;
     ;   To do this we will set self.param.timeslice before the fit, if it
     ;   has not been set, and then reset it after the fit.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::BEFORE_FIT
       COMPILE_OPT STRICTARR

       IF self.param.timeslice LT 0 THEN BEGIN
         ; Save the current timeslice and time.
         self.param.timeslice = self.param.ts_min
  
         GET_CHORD, self.param.shot $
                    ,self.param.chord $
                    ,WHITE=self.param.white $
                    ,DARK_SHOT=self.param.dark_shot
         self.param.time = BST_CERVIEW_GET_TIME(self.param.timeslice)
       ENDIF
       
     END ; PRO BST_INST_GUIADDON_CERVIEW::BEFORE_FIT




     ;+=================================================================
     ; PURPOSE:
     ;   Take action after the fit loop is completed.
     ;
     ; DESCRITPION:
     ;   Here we control the self.timeslice parameter. Because of
     ;   the possibility of doing multiple timeslice fits we need
     ;   to be able to save the current timeslice being fit when the
     ;   results are saved.
     ;
     ;   To do this we will set self.param.timeslice before the fit and
     ;   then reset it after the fit.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::AFTER_FIT

       self.param.timeslice = -1
       self.param.time = -1D
       
     END ; PRO BST_INST_GUIADDON_CERVIEW::AFTER_FIT



; ======================================================================
; ======================================================================
; ######################################################################
;
; SPECTRUM METHODS
;
;   These are the methods that <BST_SPECTRAL_FIT> will actually
;   use to get the spectra.
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Checks the user set parameters.
     ;
     ;   This does not check all of the parameters, just those that
     ;   are likely to be set incorrectly from the gui.
     ; 
     ; DESCRIPTION:
     ;   Checks made:
     ;
     ;   chord
     ;       The chord name given by the user may be invalid or missing.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::CHECK_PARAMS

       IF self.param.chord EQ '' THEN BEGIN
         MESSAGE, 'Invalid chord.'
       ENDIF

     END ;PRO BST_INST_GUIADDON_CERVIEW::CHECK_PARAMS



     ;+=================================================================
     ; 
     ; This is the function that the the BST_INST_GUIADDON_CERVIEW addon uses
     ; to retrieve the spectra to be fit.
     ;
     ; It retrives spectra for CER or B-STARK chords using the
     ; CERVIEW routines.
     ;
     ; Basically this acts as a wrapper for [BST_CERVIEW_SPECTRUM].
     ; 
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::SPECTRUM
       COMPILE_OPT STRICTARR

       RESOLVE_ROUTINE, [ $
                          'bst_cerview' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE

       ; Check the input parameters
       self->CHECK_PARAMS

       ; We want ts_sub to be relative. 
       ; BST_CERVIEW_SPECTRUM expects ts_sub to be absolute
       IF self.param.ts_sub NE 0 THEN BEGIN
         ts_sub_absolute = self.param.timeslice + self.param.ts_sub
       ENDIF

       ; Get the spectrum
       spectrum = BST_CERVIEW_SPECTRUM( self.param.shot $
                                        ,self.param.chord $
                                        ,self.param.timeslice $
                                        ,WHITE=self.param.white $
                                        ,DARK_SHOT=self.param.dark_shot $
                                        ,TS_AVERAGE=self.param.ts_average $
                                        ,TS_SUBTRACTION=ts_sub_absolute $
                                        ,TS_SUB_AVERAGE=self.param.ts_sub_average $
                                        ,TS_GROUP_STEP=self.param.ts_group_step $
                                        ,TS_GROUP_AVERAGE=self.param.ts_group_average $
                                        ;,MESSAGE_PRO='bst_inst_message' $
                                      )




       ; We want the pixel location to start from 1 not zero.
       ; This will make the convention match CERFIT/CINSTR
       output = { $
                  x:spectrum.x + 1 $
                  ,y:spectrum.y $
                  ,weight:spectrum.weight }


  

       ; Now generate a wavelength axis.
       CATCH, error
       IF error EQ 0 THEN BEGIN

         BST_CHORD_PARAM, self.param.shot, self.param.chord, CHORD_PARAM=chord_param

         IF self.param.lambda0 GT 0D THEN BEGIN
           lambda0 = self.param.lambda0
         ENDIF ELSE BEGIN
           lambda0 = chord_param.wavelength.wavelength
         ENDELSE


         wavelength = BST_DISP_GET_WAVELENGTH(LOCATION=spectrum.x $
                                              ,LAMBDA0=lambda0 $
                                              ,/FROM_EDGE $
                                              ,SPEC_PARAM=chord_param.detector)
         output = CREATE_STRUCT(output, {wavelength:wavelength})
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
       ENDELSE



       RETURN, output


     END ; END FUNCTION BST_INST_GUIADDON_CERVIEW::SPECTRUM




; ======================================================================
; ======================================================================
; ######################################################################
;
; MULTIPLE TIMELSLICE METHODS
;    These are the methods needed to fit multiple timeslices.
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Fit all timesices and save the results using the
     ;   [BST_INST_GUIADDON_FIT_SAVER::] addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::FIT_ALL_TIMESLICES
       COMPILE_OPT STRICTARR

       ; Check the input parameters
       self->CHECK_PARAMS


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.param.timeslice = -1
         self.param.time = -1D
         fit_saver->SET_AUTOMATIC, 0
         self->MESSAGE, 'Error fitting multiple timeslices.', /ERROR
       ENDIF 

       ; Get a reference to the fitter gui
       fitter_gui = self.dispatcher->GET_GUI()

       ; Get a reference to the fit saver object and reset it.
       fit_saver = self.dispatcher->GET_ADDON('BST_INST_GUIADDON_FIT_SAVER')
       fit_saver->RESET
       fit_saver->SET_AUTOMATIC, 1


       ; Get the total number of timeslices.
       GET_CHORD, self.param.shot $
                  ,self.param.chord $
                  ,WHITE=self.param.white $
                  ,DARK_SHOT=self.param.dark_shot

       num_timeslice = BST_CERVIEW_GET_NUM_TIMESLICE()


       ; Start the fit loop.
       timeslice = -1
       WHILE 1 DO BEGIN

         ; Calculate the next timeslice.
         timeslice = self->GET_NEXT_TIMESLICE(timeslice)


         ; Check the timeslice
         IF timeslice GT num_timeslice $
            OR timeslice GT self.param.ts_max THEN BREAK

         ; Set the timeslice parameter.
         self.param.timeslice = timeslice
         self.param.time = BST_CERVIEW_GET_TIME(timeslice)

         ; Fit the current timeslice.
         fitter_gui->FIT

       ENDWHILE

       ; Turn off automatic fit saving.
       fit_saver->SET_AUTOMATIC, 0

     END ;PRO BST_INST_GUIADDON_CERVIEW::FIT_ALL_TIMESLICES




     ;+=================================================================
     ; PURPOSE:
     ;   Return the next timeslice to be fit.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERVIEW::GET_NEXT_TIMESLICE, timeslice


       IF timeslice LT 0 THEN BEGIN
         RETURN, self.param.ts_min
       ENDIF

       IF self.param.ts_average NE 0 THEN BEGIN
         ts_average = self.param.ts_average
       ENDIF ELSE BEGIN
         ts_average = 1
       ENDELSE


       IF self.param.ts_group_average NE 0 THEN BEGIN
         ts_group_average = self.param.ts_group_average
       ENDIF ELSE BEGIN
         ts_group_average = 1
       ENDELSE


       IF self.param.ts_group_step NE 0 THEN BEGIN
         ts_group_step = self.param.ts_group_step
       ENDIF ELSE BEGIN
         ts_group_step = ts_average 
       ENDELSE


       RETURN, timeslice + ts_group_step * ts_group_average

     END ; FUNCTION BST_INST_GUIADDON_CERVIEW::GET_NEXT_TIMESLICE



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget->DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_CERVIEW::REMOVE_WIDGET



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW::CLEANUP

       self->REMOVE_WIDGET

       self->BST_INST_GUIADDON_GUI::CLEANUP
     END ; PRO BST_INST_GUIADDON_CERVIEW::CLEANUP


     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERVIEW__DEFINE

       struct = { BST_INST_GUIADDON_CERVIEW $
                  
                  ,options:{ BST_INST_GUIADDON_CERVIEW__OPTIONS $
                             ,compact:0 $
                           } $

                  ,param:{ BST_INST_GUIADDON_CERVIEW__PARAM $
                           ,shot:0L $
                           ,chord:'' $
                           ,beam:'' $                         
                           ,dark_shot:0L $ 
                           ,white:0 $
                           ,raw:0 $
                           ,lambda0:0D $

                           ,timeslice:0L $
                           ,time:0D $

                           ,ts_min:0L $
                           ,ts_max:0L $
                           ,ts_average:0L $
                           ,ts_sub:0L $
                           ,ts_sub_average:0L $ 
                           ,ts_group_step:0L $
                           ,ts_group_average:0L $
                         } $

                  ,widget:OBJ_NEW() $
                           
                  ,INHERITS BST_INST_GUIADDON_GUI }

     END ; PRO BST_INST_GUIADDON_CERVIEW__DEFINE
