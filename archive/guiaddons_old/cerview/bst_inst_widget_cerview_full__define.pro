




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A widget to contral GAUSSIAN models for <BST_SPECTRAL_FIT>
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_CERVIEW_FULL::INIT, addon $
                                                ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_ADDON, addon

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_CERVIEW_FULL::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_CERVIEW_FULL::SET_ADDON, addon
       
       MIR_DEFAULT, addon, OBJ_NEW()

       self.addon =  addon

     END ; PRO BST_INST_WIDGET_CERVIEW_FULL::SET_ADDON



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_CERVIEW::] 
     ;   addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_CERVIEW_FULL::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       label_xsize = 100

       ui_base_main = WIDGET_BASE(ui_base, /ROW)

       ; Set up the basic CERVIEW options.
       ; ---------------------------------------------------------------
       ui_base_left = WIDGET_BASE(ui_base_main, /COLUMN)
       ui_base_right = WIDGET_BASE(ui_base_main, /COLUMN)

       ui_base_shot = WIDGET_BASE(ui_base_left, /COLUMN, FRAME=1)

         ; Shot number
         ui_field_shot = CW_FIELD_EXT(ui_base_shot, UNAME='ui_field_shot' $
                                      ,VALUE=0 $
                                      ,TITLE='Shot' $
                                      ,/LONG $
                                      ,/ALL_EVENTS $
                                      ,XSIZE=6 $
                                      ,MAXLENGTH=6 $
                                      ,LABEL_XSIZE=label_xsize)
         ; Chord
         ui_field_chord = CW_FIELD_EXT(ui_base_shot, UNAME='ui_field_chord' $
                                       ,VALUE=0 $
                                       ,TITLE='Chord' $
                                       ,/STRING $
                                       ,/ALL_EVENTS $
                                       ,XSIZE=6 $
                                       ,MAXLENGTH=6 $
                                       ,LABEL_XSIZE=label_xsize)
         ; Dark shot
         ui_field_dark = CW_FIELD_EXT(ui_base_shot, UNAME='ui_field_dark' $
                                      ,VALUE='' $
                                      , TITLE='Dark Shot' $
                                      ,/LONG $
                                      ,/ALL_EVENTS $
                                      ,XSIZE=6 $
                                      ,MAXLENGTH=6 $
                                      ,LABEL_XSIZE=label_xsize)

        ; Beam
        ui_field_beam = CW_FIELD_EXT(ui_base_shot, UNAME='ui_field_beam' $
                                     ,VALUE=0 $
                                     ,TITLE='Beam' $
                                     ,/STRING $
                                     ,/ALL_EVENTS $
                                     ,XSIZE=6 $
                                     ,MAXLENGTH=6 $
                                     ,LABEL_XSIZE=label_xsize)



       ui_base_time = WIDGET_BASE(ui_base_left, /COLUMN, FRAME=1)

         ; Timeslice
         ui_field_ts_min = CW_FIELD_EXT(ui_base_time $
                                        ,UNAME='ui_field_ts_min' $
                                        ,VALUE=0 $
                                        ,TITLE='TS Min' $
                                        ,/LONG $
                                        ,/ALL_EVENTS $
                                        ,XSIZE=4 $
                                        ,MAXLENGTH=4 $
                                        ,LABEL_XSIZE=label_xsize)

         ui_field_ts_max = CW_FIELD_EXT(ui_base_time $
                                        ,UNAME='ui_field_ts_max' $
                                        ,VALUE=0 $
                                        ,TITLE='TS Max' $
                                        ,/LONG $
                                        ,/ALL_EVENTS $
                                        ,XSIZE=4 $
                                        ,MAXLENGTH=4 $
                                        ,LABEL_XSIZE=label_xsize)

         ui_field_ts_average = CW_FIELD_EXT(ui_base_time $
                                            ,UNAME='ui_field_ts_average' $
                                            ,VALUE=0 $
                                            ,TITLE='TS Average' $
                                            ,/LONG $
                                            ,/ALL_EVENTS $
                                            ,XSIZE=4 $
                                            ,MAXLENGTH=4 $
                                            ,LABEL_XSIZE=label_xsize)

         ui_field_ts_sub = CW_FIELD_EXT(ui_base_time $
                                        ,UNAME='ui_field_ts_sub' $
                                        ,VALUE=0 $
                                        ,TITLE='TS sub' $
                                        ,/LONG $
                                        ,/ALL_EVENTS $
                                        ,XSIZE=4 $
                                        ,MAXLENGTH=4 $
                                        ,LABEL_XSIZE=label_xsize)

         ui_field_ts_sub_average = CW_FIELD_EXT(ui_base_time $
                                                ,UNAME='ui_field_ts_sub_average' $
                                                ,VALUE=0 $
                                                ,TITLE='TS Sub Average' $
                                                ,/LONG $
                                                ,/ALL_EVENTS $
                                                ,XSIZE=4 $
                                                ,MAXLENGTH=4 $
                                                ,LABEL_XSIZE=label_xsize)

         ui_field_ts_group_average = CW_FIELD_EXT(ui_base_time $
                                                  ,UNAME='ui_field_ts_group_average' $
                                                  ,VALUE=0 $
                                                  ,TITLE='TS Group Average' $
                                                  ,/LONG $
                                                  ,/ALL_EVENTS $
                                                  ,XSIZE=4 $
                                                  ,MAXLENGTH=4 $
                                                  ,LABEL_XSIZE=label_xsize)

         ui_field_ts_group_step = CW_FIELD_EXT(ui_base_time $
                                               ,UNAME='ui_field_ts_group_step' $
                                               ,VALUE=0 $
                                               ,TITLE='TS Group Step' $
                                               ,/LONG $
                                               ,/ALL_EVENTS $
                                               ,XSIZE=4 $
                                               ,MAXLENGTH=4 $
                                               ,LABEL_XSIZE=label_xsize)

        
         
      

       ui_base_options = WIDGET_BASE(ui_base_right $
                                     ,/COLUMN $
                                     ,FRAME=1)
         option_bgroup_names = ['white', 'raw']
         ui_bgroup_options = CW_BGROUP(ui_base_options, option_bgroup_names $
                                       ,UNAME='ui_bgroup_options' $
                                       ,/NONEXCLUSIVE $
                                       ,/COLUMN)


       ui_bgroup_compact_mode = CW_BGROUP(ui_base $
                                       ,'Switch to compact mode' $
                                       ,UNAME='ui_bgroup_compact_mode' $
                                       ,/NONEXCLUSIVE $
                                       ,/COLUMN)

       ;ui_base_control = WIDGET_BASE(ui_base_main, /ROW, /ALIGN_RIGHT)
       ;  ui_button_analyze_all = WIDGET_BUTTON(ui_base_control, VALUE='Analyze All' $
       ;                                        ,XSIZE=160 $
       ;                                        ,YSIZE=40)

       id_struct = { $
                     ui_base:ui_base $

                     ,ui_bgroup_compact_mode:ui_bgroup_compact_mode $

                     ,ui_field_shot:ui_field_shot $
                     ,ui_field_chord:ui_field_chord $
                     ,ui_field_dark:ui_field_dark $
                     ,ui_field_beam:ui_field_beam $

                     ,ui_field_ts_min:ui_field_ts_min $
                     ,ui_field_ts_max:ui_field_ts_max $
                     ,ui_field_ts_average:ui_field_ts_average $

                     ,ui_field_ts_sub:ui_field_ts_sub $
                     ,ui_field_ts_sub_average:ui_field_ts_sub_average $

                     ,ui_field_ts_group_average:ui_field_ts_group_average $
                     ,ui_field_ts_group_step:ui_field_ts_group_step $

                     ,ui_bgroup_options:ui_bgroup_options $

                     ;,ui_button_analyze_all:ui_button_analyze_all $

                   }

       self->SET_ID_STRUCT, id_struct


     END ; PRO BST_INST_WIDGET_CERVIEW_FULL::GUI_DEFINE




     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_CERVIEW_FULL::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         IF OBJ_VALID(self) THEN BEGIN
           (self.addon).dispatcher->MESSAGE, 'Error handing event.', /ERROR
         ENDIF ELSE BEGIN
           MESSAGE, /REISSUE
         ENDELSE
         RETURN
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       addon = self.addon


       ; Start main event loop
       CASE event.id OF

         id.ui_bgroup_compact_mode: BEGIN
           addon->SET_COMPACT, 1
         END

         id.ui_field_shot: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.shot = value
         END


         id.ui_field_chord: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.chord = BST_CHORD_NAME_PARSE(value, /QUIET)
         END


         id.ui_field_dark: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.dark_shot = value
         END


         id.ui_field_beam: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.beam = BST_BEAM_NAME_PARSE(value, /SHORT, /QUIET)
         END


         id.ui_field_ts_min: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_min = value
         END


         id.ui_field_ts_max: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_max = value
         END


         id.ui_field_ts_average: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_average = value
         END


         id.ui_field_ts_sub: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_sub = value
         END


         id.ui_field_ts_sub_average: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_sub_average = value
         END


         id.ui_field_ts_group_step: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_group_step = value
         END


         id.ui_field_ts_group_average: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.ts_group_average = value
         END





         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.white = value[0]
           addon.param.raw = value[1]           
         END




         ;id.ui_button_analyze_all: BEGIN
         ;  addon->FIT_ALL_TIMESLICES
         ;END



         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_CERVIEW_FULL::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_CERVIEW_FULL::UPDATE
       id = self->GET_ID_STRUCT()

       addon = self.addon

       ; Set all the widgets.

       ;ui_field_shot
       WIDGET_CONTROL, id.ui_field_shot, SET_VALUE=addon.param.shot
       
       ;ui_list_chord
       WIDGET_CONTROL, id.ui_field_chord, SET_VALUE=addon.param.chord

       ;ui_field_dark
       WIDGET_CONTROL, id.ui_field_dark, SET_VALUE=addon.param.dark_shot

       ;ui_field_beam
       WIDGET_CONTROL, id.ui_field_beam, SET_VALUE=addon.param.beam


       WIDGET_CONTROL, id.ui_field_ts_min, SET_VALUE=addon.param.ts_min
       WIDGET_CONTROL, id.ui_field_ts_max, SET_VALUE=addon.param.ts_max
       WIDGET_CONTROL, id.ui_field_ts_average, SET_VALUE=addon.param.ts_average
       WIDGET_CONTROL, id.ui_field_ts_sub, SET_VALUE=addon.param.ts_sub
       WIDGET_CONTROL, id.ui_field_ts_sub_average, SET_VALUE=addon.param.ts_sub_average
       WIDGET_CONTROL, id.ui_field_ts_group_step, SET_VALUE=addon.param.ts_group_step
       WIDGET_CONTROL, id.ui_field_ts_group_average, SET_VALUE=addon.param.ts_group_average

       ;ui_bgroup_options
       value = [ $
                 addon.param.white $
                 ,addon.param.raw $
               ]
       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=value

     END ; PRO BST_INST_WIDGET_CERVIEW_FULL::UPDATE




     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_CERVIEW_FULL__DEFINE

       struct = { BST_INST_WIDGET_CERVIEW_FULL $
                           
                  ,addon:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_CERVIEW_FULL__DEFINE
