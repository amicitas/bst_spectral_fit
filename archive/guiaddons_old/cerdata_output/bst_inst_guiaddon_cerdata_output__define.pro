

     ;+=================================================================
     ;
     ; This is a collecton of routines to write the output of
     ; <BST_SPECTRAL_FIT> to a CINSTR output file.
     ;
     ; The routines that acutally do the writing are in
     ; bst_cerfit/cinstr/bst_inst_read_cinstr.pro
     ;
     ; Here we create a structure with the correct inputs,
     ; and choose the correct filenames.
     ;
     ;
     ; Note:
     ;   It is assumed that a fit will have been completed before this
     ;   dialog is called.
     ;
     ;-=================================================================     

     


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 0
       self.use_control = 0
       self.use_window = 0

       self.title = 'CERDATA Output'

     END ; PRO BST_INST_GUIADDON_CERDATA_OUTPUT::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Take any actions when instantiated.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON::INIT(dispatcher)

       self.param.file_list = OBJ_NEW('MIR_LIST_IDL7')

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::CLEANUP

       IF OBJ_VALID(self.param.file_list) THEN BEGIN
         self.param.file_list->DESTROY
       ENDIF

       self->BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_CERDATA_OUTPUT::CLEANUP




     ;+=================================================================
     ; PURPOSE:
     ;   Set the model parameters.
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::SET_PARAM, param
       
       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ;PRO BST_INST_GUIADDON_CERDATA_OUTPUT::SET_PARAM



; #################################################################
; =================================================================
; =================================================================
; BEGIN fILENAME GENERATION ROUTINES
; =================================================================
; =================================================================
; #################################################################





     ;+=================================================================
     ;
     ; Here we generate the filenames
     ; 
     ; If the beamcomp is set then assume that
     ; profile 1 = Full beam component
     ; profile 2 = Half beam component
     ; profile 3 = Third beam component
     ;
     ; If instrumental is set then assume that
     ; profile 1 = Instrumental
     ;
     ; Othewise files will be named by the profile number.
     ;
     ; This will generally be called by:
     ;   self->UPDATE_FILENAMES
     ; which will also update the gui.
     ; 
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::GENERATE_FILENAME_LIST

       ; Reset the current list.
       self.param.file_list->REMOVE, /ALL

       fitter_core = self.dispatcher->GET_CORE()
       profile_num_list = self->GET_PROFILE_NUMBER_LIST()
       num_profiles = profile_num_list->N_ELEMENTS()

       ; Begin loop over the profiles.
       FOR ii = 0, num_profiles - 1 DO BEGIN
         profile = profile_num_list->GET(ii)
         file = self->GENERATE_PROFILE_FILENAME(profile)

         self.param.file_list->APPEND, file

       ENDFOR
       
       ; Now deal with the file name for combined output.
       IF self.param.combined_output THEN BEGIN
         file = self->GENERATE_PROFILE_FILENAME(/COMBINED)
         self.param.file_list->APPEND, file
       ENDIF

       profile_num_list->DESTROY

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GENERATE_FILENAME_LIST


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Generate filenames for the <BST_INST_GUIADDON_CERDATA_OUTPUT::>
     ;   addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GENERATE_PROFILE_FILENAME, profile $
        ,COMBINED=combined $
        ,INSTRUMENTAL=instrumental

       MIR_DEFAULT, profile, 0

       CASE 1 OF
         KEYWORD_SET(combined): BEGIN
           file = self->GENERATE_FILENAME(/COMBINED $
                                          ,BEAMCOMP=self.param.beamcomp)
         END

         self.param.beamcomp AND (ABS(profile) GE 1) AND (ABS(profile) LE 3): BEGIN
           file = self->GENERATE_FILENAME(/BEAMCOMP $
                                          ,PROFILE=profile $
                                         )
         END

         self.param.instrumental AND ((profile EQ 1) OR KEYWORD_SET(instrumental)): BEGIN
           file = self->GENERATE_FILENAME(/INSTRUMENTAL)
         END

         ELSE: BEGIN
           file = self->GENERATE_FILENAME(PROFILE=profile)
         END
       ENDCASE

       RETURN, file

     END ;FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GENERATE_PROFILE_FILENAME


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Generate filenames for the <BST_INST_GUIADDON_CERDATA_OUTPUT::>
     ;   addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GENERATE_FILENAME, INSTRUMENTAL=instrumental $
                                                                ,PROFILE=profile $
                                                                ,BEAMCOMP=beamcomp $
                                                                ,COMBINED=combined

       beam_name = BST_BEAM_NAME_PARSE(self.param.beam, /SHORT, /QUIET)

       ; First pickup any filename prefix from the addons.
       filename = self->GET_FILENAME_PREFIX(/NO_BEAM)

       IF filename NE '' THEN BEGIN
         filename += '_'
       ENDIF

       IF KEYWORD_SET(beamcomp) THEN BEGIN
         IF beam_name NE '' THEN BEGIN
           filename += STRLOWCASE(beam_name) + '_'
         ENDIF
       ENDIF

       CASE 1 OF

         KEYWORD_SET(combined): BEGIN
           filename += 'all'
         END

         KEYWORD_SET(beamcomp): BEGIN
           components = ['full', 'half', 'third']  
           
           filename += components[ABS(profile)-1]
         END

         KEYWORD_SET(instrumental): BEGIN
           filename += 'instrumental'
         END

         KEYWORD_SET(profile): BEGIN
           filename += 'profile_'+ STRING(FORMAT='(i0)',profile)
         END

         ELSE:

       ENDCASE

       filename += '.bst_inst_out'


       filepath = MIR_PATH_JOIN([self.param.path, filename])
         
       RETURN, filepath

     END ;FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GENERATE_FILENAME



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return any prefix for the generated filenames.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_FILENAME_PREFIX, _REF_EXTRA=extra
         
       ;prefix = self.dispatcher->GET_FILENAME_PREFIX(/NO_BEAM)

       RETURN, ''

     END ;FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_FILENAME_PREFIX



; #################################################################
; =================================================================
; =================================================================
; BEGIN WRITING ROUTINES
; =================================================================
; =================================================================
; #################################################################




     ;+=================================================================
     ; PURPOSE:
     ;   Check the filenames to see if any exist.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::CHECK_FILES

       files_exist = 1

       num_filenames = self.param.file_list->N_ELEMENTS()
       FOR ii=0,num_filenames-1 DO BEGIN
         filepath = self.param.file_list->GET(ii)
         IF FILE_TEST(filepath) THEN BEGIN
           files_exist = 1
           BREAK
         ENDIF
       ENDFOR

       RETURN, ~ files_exist


     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::CHECK_FILES



     ;+=================================================================
     ;
     ; This converts a profile number to a beam component.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_BEAM_COMPONENT_STRING, profile

       IF profile LT 1 OR profile GT 3 THEN BEGIN
         MESSAGE, 'Profile number must be 1, 2 or 3.'
       ENDIF

       components = ['full', 'half', 'third'] 

       RETURN, components[profile-1]

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_BEAM_COMPONENT_STRING



     ;+=================================================================
     ;
     ; This converts a profile number to a string.
     ; If /BEAMCOMP is set, this will attempt to convert the profile
     ; number into a beam component.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE_STRING, profile $
                                                  ,BEAMCOMP=beamcomp $
                                                  ,INSTRUMENTAL=instrumental

       IF KEYWORD_SET(beamcomp) AND (ABS(profile) GE 1) AND (ABS(profile) LE 3) THEN BEGIN
         RETURN, self->GET_BEAM_COMPONENT_STRING(ABS(profile))
       ENDIF

       IF KEYWORD_SET(instrumental) AND (profile EQ 1) THEN BEGIN
         RETURN, 'instrumental'
       ENDIF    

       RETURN, STRING('profile_', profile, FORMAT='(a0,i0)')

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE_STRING


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a the list of profiles numbers that will be saved 
     ;   to the output file.
     ;
     ; 
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE_NUMBER_LIST

       
       fitter_core = self.dispatcher->GET_CORE()

       bstark_model = self.dispatcher->GET_MODEL('BST_INST_MODEL_BSTARK')
       IF bstark_model->USE() THEN BEGIN
         profile_number_list = fitter_core.profiles->GET_PROFILE_NUMBER_LIST(/NEGATIVE)
       ENDIF ELSE BEGIN
         profile_number_list = fitter_core.profiles->GET_PROFILE_NUMBER_LIST(/POSITIVE)
       ENDELSE


       RETURN, profile_number_list

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE_LIST


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a the list of profiles that will be saved to the
     ;   output file.
     ;
     ; 
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE_LIST

       profile_list = OBJ_NEW('MIR_LIST_IDL7')

       ; Reset the current list.
       self.param.file_list->REMOVE, /ALL

       profile_num_list = self->GET_PROFILE_NUMBER_LIST()

       num_profiles = profile_num_list->N_ELEMENTS()
       FOR ii=0,num_profiles-1 DO BEGIN
         profile_list->APPEND, self->GET_PROFILE(profile_num_list->GET(ii))
       ENDFOR

       profile_num_list->DESTROY

       RETURN, profile_list

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE_LIST



     ;+=================================================================
     ; PURPOSE:
     ;   Return a sum of gauss object with gaussians from any model
     ;   that containes the requested profile.
     ;
     ;   This is different from the standard routine in 
     ;   <BST_INST_MODEL_DISPATCH::> as it specific to output for the
     ;   B-Stark diagnostic system.
     ;
     ; 
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE, profile_num

       fitter_core = self.dispatcher->GET_CORE()
       model_dispatch = fitter_core->GET_MODEL_DISPATCHER()
               
       profile_out = model_dispatch->GET_PROFILE(profile_num) 


       ; If these are beam components the exclude stay gaussians.
       IF self.param.beamcomp AND (ABS(profile_num) GE 1) AND (ABS(profile_num) LE 3) THEN BEGIN

         ; Use an explicitly set value for the center location.
         profile_out->SET_USE_CENTER

           
         ; By default use the centroid as the center location.
         profile_out->SET_CENTER, profile_out->CENTROID()
           
           
         ; If this profile includes a scaled profile, then use the
         ; location of the scaled profile as the center of the profile.
         model_scaled_multi = model_dispatch->GET_MODEL('BST_INST_MODEL_SCALED_MULTI')
         num_scaled = model_scaled_multi->NUM_MODELS()
           
         FOR ii=0,num_scaled-1 DO BEGIN
           model = model_scaled_multi->GET_MODEL(ii)
           IF model->HAS_PROFILE(profile_num) THEN BEGIN
             param = model->GET_PARAM()
             profile_out->SET_CENTER, param.location
           ENDIF
         ENDFOR

       ENDIF


       ; Move the profile to be centered at zero.
       profile_out->MOVE, 0D

       RETURN, profile_out


     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_PROFILE




     ;+=================================================================
     ;
     ; Return a nested structure that can be used to create output
     ; that can be directly included into the CERFIT data files.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_CERDATA_OUTPUT_STRUCTURE


       ; Get the output structure from the addons
       output_addons = self.dispatcher->GET_OUTPUT_STRUCTURE()
       

       ;profile_string = self->GET_PROFILE_STRING(profile_num $
       ;                                          ,BEAMCOMP=c_output_dialog.beamcomp $
       ;                                          ,INSTRUMENTAL=c_output_dialog.instrumental)
       

       ; For now only include output from the cerview addon
       IF HAS_TAG(output_addons, 'BST_INST_GUIADDON_CERVIEW') THEN BEGIN
         output = output_addons.bst_inst_addon_cerview
       ENDIF

       ; Attempt to extract the chord and beam.
       chord = ''
       beam = ''
       IF ~ IS_NULL(output_addons) THEN BEGIN
         FOR ii=0,N_TAGS(output_addons)-1 DO BEGIN
           IF HAS_TAG(output_addons.(ii), 'CHORD') THEN BEGIN
             chord = output_addons.(ii).chord
           ENDIF
           IF HAS_TAG(output_addons.(ii), 'BEAM') THEN BEGIN
             beam = output_addons.(ii).beam
           ENDIF
         ENDFOR
       ENDIF

       IF chord NE ''  THEN BEGIN
         chord =  BST_CHORD_NAME_PARSE(chord, /CHAR3)
       ENDIF ELSE BEGIN
         chord = 'NORMALIZED'
       ENDELSE

       IF beam NE '' THEN BEGIN
         beam =  BST_BEAM_NAME_PARSE(beam, /SHORT, /QUIET)
       ENDIF


       ; Get a list with the profiles
       profile_list = self->GET_PROFILE_LIST()

       ; Now add the profiles to the output structure.
       FOR ii=0,profile_list->N_ELEMENTS()-1 DO BEGIN
         profile = profile_list->GET(ii)
         profile->NORMALIZE
         gauss_array = profile->TO_ARRAY(/STRUCTURE)
         
         profile_string = self->GET_PROFILE_STRING(profile->GET_LABEL() $
                                                   ,BEAMCOMP=self.param.beamcomp $
                                                   ,INSTRUMENTAL=self.param.instrumental)
         output = BUILD_STRUCTURE(profile_string, gauss_array, output)
       ENDFOR

       IF beam NE '' THEN BEGIN
         output = CREATE_STRUCT(IDL_VALIDNAME(beam, /CONVERT_ALL), output)
       ENDIF
       output = CREATE_STRUCT(chord, output)

       profile_list->DESTROY


       ; Convert the fit date into a string
       ;time_string = TIME_STRING(c_fit.time, /TIME)
       ;date_string = TIME_STRING(c_fit.time, /DATE)

       ; Create the common portions of the output structure.
       ;output_common = { $
       ;                  version:VERSION_STRING(NUMBER=c_inst_param.version_num) $
       ;                  ,version_date:c_inst_param.version_date $
       ;                  ,date:date_string $
       ;                  ,time:time_string $
       ;                  ,lower_channel:c_inst_param.xrange[0] $
       ;                  ,upper_channel:c_inst_param.xrange[1] $
       ;                  ,chisq:c_fit.chisq $
       ;                  ,reduced_chisq:c_fit.reduced_chisq $
       ;                }


       ; Combine the various output structures.
       ; If we have output from the addon, put it after the common output
       ; but before the profile output.
       ;IF TAG_NAMES(output_addons, /STRUCTURE_NAME) NE 'NULL' THEN BEGIN
       ;  output = CREATE_STRUCT(output_common, output_addons)
       ;ENDIF ELSE BEGIN
       ;  output = output_common
       ;ENDELSE
       ;output_combined = output




       RETURN, output

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::GET_CERDATA_OUTPUT_STRUCTURE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Write output files with the results of a completed fit
     ;   from <BST_SPECTRAL_FIT>.
     ;
     ; IMPORTANT: 
     ;   When a scaled peak is used in a profile, the centroid is taken 
     ;   as being at the position of the scaled peak. All other 
     ;   gaussians are not considered for the centroid.
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::WRITE_OUTPUT_FILES
       COMPILE_OPT STRICTARR

       ; Set the lengths of the labels and records
       ; The label width must be at least as long as any of the tag
       ; labels given above.
       label_width = 15
       
       ; Single precision is fine for everthing here.
       ; So we don't need to go all the way to 23.
       record_width = 15

       ; Setup the file header.
       header = ['' $
                 ,'BST_SPECTRAL FIT output file.' $
                 ,'Normalized gaussians for CER/B-STARK datafiles.']



       IF self.param.beamcomp AND ~ self.param.combined_output THEN BEGIN
         MESSAGE, 'Only combined output is currently supported for beam components.'
       ENDIF


       ; Get the output structure.
       output = self->GET_CERDATA_OUTPUT_STRUCTURE()
         
       filename = self->GENERATE_PROFILE_FILENAME(COMBINED=self.param.combined_output $
                                                  ,INSTRUMENTAL=self.param.instrumental)

       LABEL_DATA_FORMAT_WRITE, output $
                                ,FILENAME=filename $
                                ,/OVERWRITE $
                                ,LABEL_WIDTH=label_width $
                                ,RECORD_WIDTH=record_width $
                                ,HEADER=header

         

     END ;PRO BST_INST_GUIADDON_CERDATA_OUTPUT::WRITE_OUTPUT_FILES



; #################################################################
; =================================================================
; =================================================================
; MAIN ROUTINE
; =================================================================
; =================================================================
; #################################################################



     ;+=================================================================
     ;
     ; Show the output dialog.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::SHOW_OUTPUT_DIALOG

       dialog = OBJ_NEW('BST_INST_DIALOG_CERDATA_OUTPUT' $
                        ,self $
                        ,/MODAL $
                        ,/SHOW $
                        ,STATUS=status)

       
       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_CERDATA_OUTPUT::BST_INST_OUTPUT




     ;+=================================================================
     ;
     ; This is main procedure to call inorder to save the fit results
     ; in output files.
     ;
     ; It is assumed that a fit was completed before this routines is
     ; called
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::CERDATA_OUTPUT

       CATCH, error
       IF error EQ 0 THEN BEGIN

         status = self->SHOW_OUTPUT_DIALOG()

         IF status THEN BEGIN
           self->WRITE_OUTPUT_FILES
         ENDIF

       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
       ENDELSE
       
     END ; PRO BST_INST_GUIADDON_CERDATA_OUTPUT::BST_INST_OUTPUT



; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI METHODS
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::GUI_DEFINE


       ; ---------------------------------------------------------------
       ; Set up menu for this addon.
       fitter_gui = self.dispatcher->GET_GUI()
       ui_menu_command =  fitter_gui->GET_GUI_ID(/MENU_COMMAND)

       ui_button_cerdata_output = WIDGET_BUTTON(ui_menu_command $
                                                 ,UNAME='ui_button_cerdata_output' $
                                                 ,VALUE='CERDATA Output' $
                                                 ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                 ,UVALUE=self $
                                                 ,/SEPARATOR $
                                                )

       ; Store all of the accesable widgit id's here.
       id_struct = { $
                     ui_button_cerdata_output:ui_button_cerdata_output $
                   }

       ; Save the id structure.
       self->SET_ID_STRUCT, id_struct

     END ; PRO BST_INST_GUIADDON_CERDATA_OUTPUT::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Event handler for this addon
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN



       ; Start main event loop
       id = self->GET_ID_STRUCT()

       CASE event.id OF

         id.ui_button_cerdata_output: BEGIN
           self->CERDATA_OUTPUT
         END

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_CERDATA_OUTPUT::EVENT_HANDLER



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ; PROGRAMING NOTES:
     ;
     ;   Property definitons:
     ;
     ;   param.instrumental = bool
     ;     If set the output will be formatted for inclusion in
     ;     in the instrumental profiles CERDATA files.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_CERDATA_OUTPUT__DEFINE

       struct = { BST_INST_GUIADDON_CERDATA_OUTPUT $
                           
                  ,param:{BST_INST_GUIADDON_CERDATA_OUTPUT__PARAM $
                          ,path:'' $
                          ,instrumental:0 $
                          ,beamcomp:0 $
                          ,beam:'' $
                          ,combined_output:0 $
                          ,file_list:OBJ_NEW() $
                          
                          ;,use_scaled_location:0 $
                          ;,use_bstark_location:0 $
                         } $

                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_CERDATA_OUTPUT__DEFINE
