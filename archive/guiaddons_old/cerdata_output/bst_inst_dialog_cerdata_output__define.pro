

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;   Output the fit to a text file.  This textfile will be written in
;   the label-data format.  The profiles will be output in a format
;   suitable for inclusion in the CERDATA profile files.
; 
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the <BST_INST_DIALOG_CERDATA_OUTPUT::> object.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::INIT, addon, STATUS=status, _REF_EXTRA=extra

       self.title = 'Write CERDATA output.'


       IF ~ ISA(addon) THEN BEGIN
         MESSAGE, 'Programming Error: Addon object not given.'
       ENDIF
       self->SET_ADDON, addon


       ; First check if there are any valid profiles.
       ;fitter_core = (self.addon).dispatcher->GET_CORE()
       ;num_profiles = fitter_core.profiles->NUM_PROFILES(/POSITIVE)
       ;IF num_profiles EQ 0 THEN BEGIN
       ;  status = 0
       ;  message= 'No profiles found.'
       ;  dialog_status = DIALOG_MESSAGE(message, /ERROR)
       ;  RETURN, 0
       ;ENDIF

       ; Set the group leader.
       fitter_gui = (self.addon).dispatcher->GET_GUI()
       group_leader = fitter_gui->GET_GUI_ID(/BASE_MAIN)


       ; Initialize the widget object.
       init_status = self->WIDGET_DIALOG::INIT(group_leader $
                                               ,STATUS=status $
                                               ,_STRICT_EXTRA=extra)

       RETURN, init_status

     END ;FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::SET_ADDON, addon
       
       MIR_DEFAULT, addon, OBJ_NEW()
       self.addon = addon

     END ; PRO BST_INST_DIALOG_CERDATA_OUTPUT::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Return the exit status for the dialog.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::GET_STATUS

       RETURN, self.status

     END ;FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::GET_STATUS




; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI ROUTINES.
;   Here we build the GUI for the dialog.  
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Update the addon with the current settings.
     ;
     ; This will generally be done either after loading or setting
     ; defaults.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::GUI_UPDATE


       id = self->GET_ID_STRUCT()

       self->UPDATE_FILENAMES

       WIDGET_CONTROL, id.ui_label_path, SET_VALUE=(self.addon).param.path
       WIDGET_CONTROL, id.ui_bgroup_auto, SET_VALUE=[(self.addon).param.instrumental $
                                                     ,(self.addon).param.beamcomp]
       WIDGET_CONTROL, id.ui_field_beam, SET_VALUE=(self.addon).param.beam $
                       ,SENSITIVE=(self.addon).param.beamcomp
       WIDGET_CONTROL, id.ui_bgroup_combined, SET_VALUE=[(self.addon).param.combined_output]

     END ; PRO BST_INST_DIALOG_CERDATA_OUTPUT::GUI_UPDATE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Build the gui components for the 
     ;   <BST_INST_DIALOG_CERDAT_OUTPUT::> object.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::GUI_DEFINE

       auto_bgroup_names = ['Instrumental', 'Beam Components']
       auto_bgroup_value = [0,0]

       combined_bgroup_names = ['Combined']
       combined_bgroup_value = [0]

       column_labels = ['Profile', 'Output Filename']




       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/COLUMN)

       ui_base_top = WIDGET_BASE(ui_base, /ROW, FRAME=1)
         ui_bgroup_auto = CW_BGROUP(ui_base_top, auto_bgroup_names $
                                    ,UNAME='ui_bgroup_auto' $
                                    ,SET_VALUE = auto_bgroup_value $ 
                                    ,/NONEXCLUSIVE $
                                    ,/ROW)
         ui_field_beam = CW_FIELD_EXT(ui_base_top $
                                      ,UNAME='ui_field_beam' $
                                      ,TITLE='Beam:' $
                                      ,VALUE='' $
                                      ,/STRING $
                                      ,MAXLENGTH=5 $
                                      ,SENSITIVE=0 $
                                      ,/ALL_EVENTS)

         ui_bgroup_combined = CW_BGROUP(ui_base_top, combined_bgroup_names $
                                      ,UNAME='ui_bgroup_combined' $
                                      ,SET_VALUE = combined_bgroup_value $ 
                                      ,/NONEXCLUSIVE $
                                      ,/ROW)
       
       ui_base_mid = WIDGET_BASE(ui_base, /ROW)
         ui_table_filenames = WIDGET_TABLE(ui_base_mid $
                                           ,UNAME='ui_table_filenames' $
                                           ,XSIZE = 2 $
                                           ,YSIZE = 4 $
                                           ,/SCROLL $
                                           ,SCR_XSIZE = 560 $
                                           ,COLUMN_WIDTHS = [60,600] $
                                           ,COLUMN_LABELS = column_labels $
                                           ,/NO_ROW_HEADERS $
                                          )

       ui_base_mid_2 = WIDGET_BASE(ui_base, /ROW)
         ui_button_path = WIDGET_BUTTON(ui_base_mid_2 $
                                        ,UNAME='ui_button_path' $
                                        ,VALUE='Choose Path')
         ui_label_path_title = WIDGET_LABEL(ui_base_mid_2 $
                                            ,UNAME='ui_label_path_title' $
                                            ,VALUE='Path: ')
         ui_label_path = WIDGET_LABEL(ui_base_mid_2 $
                                      ,UNAME='ui_label_path' $
                                      ,VALUE='' $
                                      ,/DYNAMIC_RESIZE)
       ui_base_bottom = WIDGET_BASE(ui_base, /ROW)
         ui_button_ok = WIDGET_BUTTON(ui_base_bottom $
                                      ,UNAME='ui_button_ok' $
                                      ,VALUE='Ok' $
                                      ,XSIZE=50)
         ui_button_cancel = WIDGET_BUTTON(ui_base_bottom $
                                          ,UNAME='ui_button_cancel' $
                                          ,VALUE='Cancel' $
                                          ,XSIZE=50)





       id_struct = { $
                     ui_base:ui_base $
                     ,ui_bgroup_auto:ui_bgroup_auto $
                     ,ui_field_beam:ui_field_beam $
                     ,ui_bgroup_combined:ui_bgroup_combined $
                     ,ui_table_filenames:ui_table_filenames $
                     ,ui_label_path:ui_label_path $
                     ,ui_button_path:ui_button_path $
                     ,ui_button_ok:ui_button_ok $
                     ,ui_button_cancel:ui_button_cancel $
                   }

       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_DIALOG_CERDATA_OUTPUT::BST_INST_OUTPUT_DIALOG


     ;+=================================================================
     ;
     ; Cancel saving of CERDATA output.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::EXIT_CANCEL

       self.status = 0
       self->GUI_DESTROY

     END ;PRO BST_INST_DIALOG_CERDATA_OUTPUT::EXIT_CANCEL



     ;+=================================================================
     ;
     ; Cancel saving of CERDATA output.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::EXIT_OK

       IF (self.addon).param.path EQ '' THEN BEGIN
         status = self->DIALOG_GET_PATH()
         ; If the path selection was canceled, then just return to the dialog.
         IF ~ status THEN RETURN
       ENDIF

       ; Check if files alredy exist, and the user does not want to overwrite, 
       ; then just return to the dialog.
       status = self->CHECK_FILES()

       IF status THEN BEGIN
         self.status = 1
         self->GUI_DESTROY
       ENDIF


     END ;PRO BST_INST_DIALOG_CERDATA_OUTPUT::EXIT_OK


     ;+=================================================================
     ;
     ; This is the event handler for the widget.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.addon->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF


         id.ui_button_cancel: BEGIN
           self->EXIT_CANCEL
         END


         id.ui_button_ok: BEGIN
           self->EXIT_OK
         END


         id.ui_button_path: BEGIN
           status = self->DIALOG_GET_PATH()
         END


         id.ui_bgroup_auto: BEGIN
           CASE event.value OF
             0: BEGIN

               ; Instrumental
               IF event.select THEN BEGIN
                 WIDGET_CONTROL, id.ui_bgroup_auto, SET_VALUE = [1,0]
                 self.addon->SET_PARAM, {beamcomp:0}
               ENDIF

               self.addon->SET_PARAM, {instrumental:event.select}
             END
             1: BEGIN
               ; Beam-Components

               IF event.select THEN BEGIN
                 WIDGET_CONTROL, id.ui_bgroup_auto, SET_VALUE = [0,1]
                 self.addon->SET_PARAM, {instrumental:0}
               ENDIF

               WIDGET_CONTROL, id.ui_field_beam, SENSITIVE=event.select

               self.addon->SET_PARAM, {beamcomp:event.select}
             END
           ENDCASE

           self->UPDATE_FILENAMES
         END


         id.ui_bgroup_combined: BEGIN
           self.addon->SET_PARAM, {combined_output:event.select }
           self->UPDATE_FILENAMES
         END


         id.ui_field_beam: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=beam_name
           beam_name = BST_BEAM_NAME_PARSE(beam_name, /SHORT, /QUIET)

           self.addon->SET_PARAM, {beam:beam_name}

           self->UPDATE_FILENAMES
         END

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE
       ENDCASE

     END ;PRO BST_INST_DIALOG_CERDATA_OUTPUT::EVENT_HANDLER



     ;+=================================================================
     ;
     ; This is used to update the filenames in the OUTPUT dialog.
     ; 
     ; It first regenerates the filenames, then it updates the 
     ; the table widget with the new filenames.
     ;
     ; This will be used whenever the autonaming is switch between 
     ; profiles and beam components.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT::UPDATE_FILENAMES, CLEAR=clear

       id = self->GET_ID_STRUCT()


       ; First generate the new filenames.
       self.addon->GENERATE_FILENAME_LIST
       num_filenames = (self.addon).param.file_list->N_ELEMENTS()


       ; Get the current values in the table
       WIDGET_CONTROL, id.ui_table_filenames, GET_VALUE=table_values

       ; Check the size of the table.
       table_size = SIZE(table_values, /DIM)

       IF table_size[1] LT num_filenames THEN BEGIN
         WIDGET_CONTROL, id.ui_table_filenames $
                         ,INSERT_ROW=num_filenames-table_size[1]
         
         ; Get the values agian with the expanded table
         WIDGET_CONTROL, id.ui_table_filenames, GET_VALUE=table_values
       ENDIF

       ; First clear the table
       table_values[*] = ''

       IF num_filenames GT 0 THEN BEGIN
         filenames = (self.addon).param.file_list->TO_ARRAY()
         ; Now loop over all the file names and set the table values
         FOR ii=0,num_filenames-1 DO BEGIN
           table_values[1,ii] = FILE_BASENAME(filenames[ii])
         ENDFOR
       ENDIF
       
       ; Set the table values and also the path display.
       WIDGET_CONTROL, id.ui_table_filenames, SET_VALUE=table_values 
       WIDGET_CONTROL, id.ui_label_path, SET_VALUE=(self.addon).param.path

     END ;PRO BST_INST_DIALOG_CERDATA_OUTPUT::UPDATE_FILENAMES




     ;+=================================================================
     ;
     ; Open a dialog to let the user choose the path in which
     ; the OUTPUT files will be saved.
     ;
     ; This will also cause the file names to be regenerated
     ; and the gui will be update.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::DIALOG_GET_PATH

       path = DIALOG_PICKFILE(/DIRECTORY $
                              ,PATH=(self.addon).param.path $
                              ,/MUST_EXIST $
                              ,TITLE='Choose the path where the files will be saved.')

       IF path NE '' THEN BEGIN
         status = 1
         self.addon->SET_PARAM, {path:path}
         self->UPDATE_FILENAMES
       ENDIF ELSE BEGIN
         status = 0
       ENDELSE

       RETURN, status
       
       
     END ; FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::DIALOG_GET_PATH



     ;+=================================================================
     ;
     ; This function will be called before saving to check if any of the
     ; files already exist.  
     ;
     ; If the do a dialog box will be opened to ask the user if the 
     ; want to overwrite the files or not.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::CHECK_FILES

       files_exist = self.addon->CHECK_FILES()

       IF files_exist THEN BEGIN
         message = ['One or more files specified exist.', $
                    'Overwite files?']
         overwrite = DIALOG_MESSAGE(message, /QUESTION)
         status = (overwrite EQ 'Yes')
       ENDIF ELSE BEGIN
         status = 1
       ENDELSE

       RETURN, status

     END ;FUNCTION BST_INST_DIALOG_CERDATA_OUTPUT::CHECK_FILES



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Create a new dialag widget as a derived class of
     ;   <WIDGET_OBJECT>.
     ;
     ; PROGRAMING NOTES:
     ;
     ;   Property definitons:
     ;
     ;   param.instrumental = bool
     ;     If set the output will be formatted for inclusion in
     ;     in the instrumental profiles CERDATA files.
     ;
     ;-=================================================================
     PRO BST_INST_DIALOG_CERDATA_OUTPUT__DEFINE

       struct = { BST_INST_DIALOG_CERDATA_OUTPUT $
                           
                  ,addon:OBJ_NEW() $
                  ,title:'' $
                  ,status:0 $

                  ,INHERITS WIDGET_DIALOG $
                  ,INHERITS OBJECT}

     END ; PRO BST_INST_DIALOG_CERDATA_OUTPUT__DEFINE
