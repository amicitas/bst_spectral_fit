




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A widget to contral GAUSSIAN models for <BST_SPECTRAL_FIT>
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_FIDA::INIT, model $
                                          ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_FIDA::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_FIDA::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_FIDA::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the <BST_INST_WIDGET_FIDA::>
     ;   object.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_FIDA::GUI_DEFINE, FONT=LabelFont $
                                           ,FRAME=frame $
                                           ,SUB_FRAME=sub_frame $
                                           ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 0
       MIR_DEFAULT, sub_frame, 1


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/COLUMN $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)

       ui_bgroup_use = CW_BGROUP(ui_base, UNAME='ui_bgroup_use' $
                                 ,'Use' $
                                 ,/NONEXCLUSIVE $
                                 ,FRAME=0)

       ui_base_sub = WIDGET_BASE(ui_base, UNAME='ui_base_sub' $
                                 ,/COLUMN $
                                 ,ALIGN_CENTER=align_center $
                                 ,SENSITIVE=0 $
                                 ,FRAME=sub_frame)

         option_names = ['fix', 'save']
         ui_bgroup_options = CW_BGROUP(ui_base_sub, UNAME='ui_bgroup_options' $
                                       ,option_names $
                                       ,/NONEXCLUSIVE $
                                       ,/ROW)



         label_size = 70

         ui_base_intensity = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_intensity = CW_FIELD_EXT(ui_base_intensity, UNAME='ui_field_intensity' $
                                         ,VALUE='' $
                                         ,TITLE='Intensity' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_intensity = CW_BGROUP(ui_base_intensity, UNAME='ui_bgroup_intensity' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)

           ;; BEGIN Adding location
         ui_base_location = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_location = CW_FIELD_EXT(ui_base_location, UNAME='ui_field_location' $
                                         ,VALUE='' $
                                         ,TITLE='Location' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_location = CW_BGROUP(ui_base_location, UNAME='ui_bgroup_location' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)
           ;; END Adding location

           ;; BEGIN Adding time
         ui_base_time = WIDGET_BASE(ui_base_sub, /ROW)
           ui_field_time = CW_FIELD_EXT(ui_base_time, UNAME='ui_field_time' $
                                         ,VALUE='' $
                                         ,TITLE='Time' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_time = CW_BGROUP(ui_base_time, UNAME='ui_bgroup_time' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)
           ;; END Adding time


       id_struct = { $
                     ui_bgroup_use:ui_bgroup_use $
                     ,ui_base_sub:ui_base_sub $
                     ,ui_bgroup_options:ui_bgroup_options $

                     ,ui_field_intensity:ui_field_intensity $
                     ,ui_bgroup_intensity:ui_bgroup_intensity $
                     ,ui_field_location:ui_field_location $
                     ,ui_bgroup_location:ui_bgroup_location $
                     ,ui_field_time:ui_field_time $
                     ,ui_bgroup_time:ui_bgroup_time $
                     }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_FIDA::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event handler for this widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_FIDA::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF


         id.ui_bgroup_use: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=use
           self.model->SET_USE, use[0]
           self->SET_USE, use[0], /LEAVE_BUTTON
         END


         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_options, GET_VALUE=value_bgroup_options

           ; Deal with freezing/unfreezing parameters
           self.model->SET_FIXALL, value_bgroup_options[0]

           ; Deal with saving/unsaving parameters
           self.model->SET_SAVE, value_bgroup_options[1]
         END


         id.ui_field_intensity: BEGIN
           self.model->SET_PARAM, {intensity:event.value}
         END
         id.ui_bgroup_intensity: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {intensity:fix[0]}
         END
         ;; BEGIN Adding location
         id.ui_field_location: BEGIN
           self.model->SET_PARAM, {location:event.value}
         END
         id.ui_bgroup_location: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {location:fix[0]}
         END
         ;; END Adding location
         ;; BEGIN Adding time
         id.ui_field_time: BEGIN
           self.model->SET_PARAM, {time:event.value}
         END
         id.ui_bgroup_time: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {time:fix[0]}
         END
         ;; END Adding time

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_FIDA::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_FIDA::UPDATE
       
       
       id = self->GET_ID_STRUCT()

       self->SET_USE, self.model->USE()

       param = self.model->GET_PARAM()
       fixed = self.model->GET_FIXED()


       bg_group_value = [self.model->GET_FIXALL() $
                         ,self.model->SAVE()]
       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=bg_group_value

       WIDGET_CONTROL, id.ui_field_intensity, SET_VALUE=param.intensity
       WIDGET_CONTROL, id.ui_bgroup_intensity, SET_VALUE=[fixed.intensity]

       ;; BEGIN Adding location
       WIDGET_CONTROL, id.ui_field_location, SET_VALUE=param.location
       WIDGET_CONTROL, id.ui_bgroup_location, SET_VALUE=[fixed.location]
       ;; END Adding location
       ;; BEGIN Adding time
       WIDGET_CONTROL, id.ui_field_time, SET_VALUE=param.time
       WIDGET_CONTROL, id.ui_bgroup_time, SET_VALUE=[fixed.time]
       ;; END Adding time


     END ; PRO BST_INST_WIDGET_FIDA::UPDATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_FIDA::SET_USE, use, LEAVE_BUTTON=leave_button
       
       id = self->GET_ID_STRUCT()


       ; This keyword is useful when handing button events.
       IF ~ KEYWORD_SET(leave_button) THEN BEGIN
         WIDGET_CONTROL, id.ui_bgroup_use, SET_VALUE = [use]
       ENDIF

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base_sub, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_FIDA::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_FIDA__DEFINE

       struct = { BST_INST_WIDGET_FIDA $
                           
                  ,model:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_FIDA__DEFINE
