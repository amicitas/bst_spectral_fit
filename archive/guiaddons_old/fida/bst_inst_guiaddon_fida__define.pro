

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from CER or B-STARK chord data using FIDA.
;
;   This is the default addon FOR <BST_SPECTRAL_FIT> and will
;   be loaded on startup.
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::SET_FLAGS

       self->BST_INST_GUIADDON_GUI::SET_FLAGS

       self.version.number = [1,0,0]
       self.version.date = [2009,04,29]

       self.active = 1
       self.default = 0

       self.use_tab = 1
       self.use_control = 0
       self.use_window = 0

       self.add_to_menu = 1
       self.menu_title = 'MODEL: FIDA'

       self.title = 'FIDA'

     END ; PRO BST_INST_GUIADDON_FIDA::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_FIDA::INIT, dispatcher

       status = self->BST_INST_GUIADDON_GUI::INIT(dispatcher)

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_FIDA::INIT


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ; 
     ; DESCRIPTION:
     ;   This is called after all addons have been instantiated.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::INITIALIZE
       self->BST_INST_GUIADDON::INITIALIZE

       fitter_core = (self.dispatcher)->GET_CORE()
       self.model = fitter_core.models->GET_MODEL('BST_INST_MODEL_FIDA')

     END ;PRO BST_INST_GUIADDON_FIDA::INITIALIZE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a plotlist to be added to the spectrum.
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_FIDA::GET_SPECTRUM_PLOTLIST, x_values

       cutoff = 1e-2

       plotlist = OBJ_NEW('MIR_PLOTLIST')

       IF ~ self.model->USE() THEN BEGIN
         RETURN, plotlist
       ENDIF

           
       spectrum = self.model->EVALUATE(x_values)

       where_nonzero = WHERE(ABS(spectrum) GE cutoff, count_nonzero)
       IF count_nonzero GT 0 THEN BEGIN
         plotlist->APPEND, {x:x_values[where_nonzero] $
                            ,y:spectrum[where_nonzero] $
                            ,color:MIR_COLOR('PRINT_CYAN') $
                            ,oplot:1 $
                            ,plotnum:0 $
                            ,psym:0 $
                           }
       ENDIF

       ; This method should be reimplemented by addons when needed.
       RETURN, plotlist

     END ; FUNCTION BST_INST_GUIADDON_FIDA::GET_SPECTRUM_PLOTLIST


; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Build the compact version of the GUI components for the 
     ;   [BST_INST_GUIADDON_FIDA::] addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)
       
       ; Add a widget to control the addon
       self->REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_FIDA' $
                             ,self.model $
                             ,DESTINATION=ui_base)

     END ;PRO BST_INST_GUIADDON_FIDA::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event manager for the FIDA addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing GUI event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->BST_INST_GUIADDON_GUI::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       ; Handle all of the last events.
       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_FIDA::EVENT_HANDLER




     ;+=================================================================
     ; PURPOSE:
     ;   Update the gui from the current parameters in the
     ;   common block.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::UPDATE_GUI

       self.widget->UPDATE
       
     END ; PRO BST_INST_GUIADDON_FIDA::UPDATE_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget->DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_FIDA::REMOVE_WIDGET



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA::CLEANUP

       self->REMOVE_WIDGET

       self->BST_INST_GUIADDON_GUI::CLEANUP
     END ; PRO BST_INST_GUIADDON_FIDA::CLEANUP


     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_FIDA__DEFINE

       struct = { BST_INST_GUIADDON_FIDA $
                           
                  ,model:OBJ_NEW() $
                    
                  ,widget:OBJ_NEW() $
  
                  ,INHERITS BST_INST_GUIADDON_GUI }

     END ; PRO BST_INST_GUIADDON_FIDA__DEFINE
