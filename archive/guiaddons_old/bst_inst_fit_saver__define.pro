


;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-06
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   Allows for the plotting and saving of fit restults.
;
;-======================================================================


; ======================================================================
; TO DO:
;
;
; ======================================================================





     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_SAVER::INIT
       
       status = self->BST_INST_ADDON_GUI::INIT()

       self.menu_title = 'Fit Saver'
       self.enabled = 1

       self.use_tab = 0
       self.use_control = 0

       self.full_debug = 1


       self.fit_list = OBJ_NEW('BST_INST_FIT_LIST')


       RETURN, status

     END ; FUNCTION BST_INST_FIT_SAVER::INIT



     ;+=================================================================
     ;
     ; Initialize the FIT SAVER addon.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::CLEANUP

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::CLEANUP

       ; Destroy the fit_list
       IF OBJ_VALID(self.fit_list) THEN BEGIN
         OBJ_DESTROY, self.fit_list
       ENDIF

     END ; PRO BST_INST_FIT_SAVER::ENABLE




     ;+=================================================================
     ;
     ; Initialize the FIT SAVER addon.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::ENABLE

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::ENABLE

     END ; PRO BST_INST_FIT_SAVER::ENABLE




     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::DISABLE

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::DISABLE


     END ;PRO BST_INST_FIT_SAVER::DISABLE





; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ;
     ; Create a compound widget for saving multiple fits.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::BUILD_GUI
                                        
       ui_base = WIDGET_BASE(self.gui_id, /COLUMN)    


       ; Create the base widget that will hold the table and related funcitons
       ; ---------------------------------------------------------------
       ui_base_table = WIDGET_BASE(ui_base, UNAME='ui_base_table', /COLUMN)

         column_labels = [ $
                           'Shot' $
                           ,'Mic' $
                           ,'FWHM' $
                           ,'Max' $
                         ]

         table_dim = [N_ELEMENTS(column_labels), 10]

         ; Set the width of the columns, for now just set them all equal.
         column_widths = [50,70,150,200]

         ui_table = WIDGET_TABLE(ui_base_table, UNAME='UI_TABLE' $
                                 ,COLUMN_WIDTHS=column_widths $
                                 ,UNITS=0 $
                                 ,XSIZE=table_dim[0] $
                                 ,YSIZE=table_dim[1] $
                                 ,COLUMN_LABELS=column_labels $
                                 ,/CONTEXT_EVENT $
                                 ,EVENT_PRO='BST_INST_ADDON__EVENT' $
                                 ,UVALUE=self $
                                 ,FRAME=0)

       ; Create the controls
       ; ---------------------------------------------------------------
       ui_base_controls = WIDGET_BASE(ui_base, UNAME='ui_base_controls', /ROW)

         ui_field_focus = CW_FIELD_EXT(ui_base_controls, UNAME='ui_field_focus' $
                                       ,VALUE='0.0', TITLE='Focus' $
                                       ,/FLOAT, /ALL_EVENTS, XSIZE=6)

         ui_button_getfit = WIDGET_BUTTON(ui_base_controls, UNAME='ui_button_getfit' $
                                          ,VALUE='Get Fit')

       ; Create the main menu bar
       ; ---------------------------------------------------------------
       ui_base_menu_file = WIDGET_BUTTON(self.menu_bar_id, VALUE='File', /MENU)

         ui_menu_save = WIDGET_BUTTON(ui_base_menu_file, UNAME='ui_menu_save' $
                                        ,VALUE='Save', XSIZE=200)

         ui_menu_load = WIDGET_BUTTON(ui_base_menu_file, UNAME='ui_menu_load' $
                                        ,VALUE='Load')

         ui_menu_reset = WIDGET_BUTTON(ui_base_menu_file, UNAME='ui_menu_reset' $
                                        ,VALUE='Reset')

         ui_menu_exit = WIDGET_BUTTON(ui_base_menu_file, UNAME='ui_menu_file' $
                                        ,VALUE='Exit', /SEPARATOR)

       ui_base_menu_plot = WIDGET_BUTTON(self.menu_bar_id, VALUE='Plot', /MENU)
         ui_menu_plot_options = WIDGET_BUTTON(ui_base_menu_plot, UNAME='ui_menu_plot_options' $
                                              ,VALUE='Plot Options')

       ui_base_menu_options = WIDGET_BUTTON(self.menu_bar_id, VALUE='Options', /MENU)
         ui_menu_update = WIDGET_BUTTON(ui_base_menu_options, UNAME='ui_menu_update' $
                                        ,VALUE='Update Table')

       ui_base_menu_help = WIDGET_BUTTON(self.menu_bar_id, VALUE='Help', /MENU)

         ui_menu_debug = WIDGET_BUTTON(ui_base_menu_help $
                                         ,UNAME='ui_menu_debug' $
                                         ,VALUE='Debug' $
                                         ,/CHECKED_MENU $
                                         ,UVALUE={checked:0})

       ; ---------------------------------------------------------------
       ; Create context menus
       ui_context_table_base = WIDGET_BASE(ui_table, UNAME='ui_context_table_base' $
                                           ,/CONTEXT_MENU)

         ui_context_table_delete = WIDGET_BUTTON(ui_context_table_base $
                                                  ,UNAME='ui_context_table_delete' $
                                                  ,VALUE='Clear Row')

       ; Store all of the useable wigit id's here.
       id_struct = { $
                     ui_base:ui_base $

                     ,ui_menu_save:ui_menu_save $
                     ,ui_menu_load:ui_menu_load $
                     ,ui_menu_reset:ui_menu_reset $
                     ,ui_menu_exit:ui_menu_exit $
                     ,ui_menu_plot_options:ui_menu_plot_options $
                     ,ui_menu_update:ui_menu_update $
                     ,ui_menu_debug:ui_menu_debug $

                     ,ui_table:ui_table $

                     ,ui_field_focus:ui_field_focus $

                     ,ui_button_getfit:ui_button_getfit $

                     ,ui_context_table_base:ui_context_table_base $
                     ,ui_context_table_delete:ui_context_table_delete $
                   }

       state = { $
                 id_struct:id_struct $
               }

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, SET_UVALUE=state

     END ;PRO BST_INST_FIT_SAVER::BUILD_GUI



     


     ;+=================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::EVENT_HANDLER, event
       COMMON BST_INST_GUI
 
       ; Establish catch block.
       CATCH, error                
       IF (error NE 0) THEN BEGIN 
         ; Cancel catch block.
         CATCH, /CANCEL

         BST_INST_MESSAGE, STRING('Caught in BST_INST_FIT_SAVER::EVENT_HANDLER')
         BST_INST_MESSAGE, STRING('Error: '+!ERROR_STATE.MSG)

         IF self.full_debug THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF

         RETURN
       ENDIF 

       ; This addon has two main event handlers:
       ; 1. Main event handler.
       ; 3. Plot options handler.
       ;
       ; The first thing to do is to pass the event to the proper handler.
       ; Note that the table also has an event handler, though it is called
       ; from the main event handler.
       handler_uname = WIDGET_INFO(Event.Handler, /UNAME)
       CASE handler_uname OF
         'UI_PLOT_DIALOG': BEGIN
           self->PLOT_DIALOG_EVENT_HANDLER, event
           RETURN
         END
         'UI_TABLE': BEGIN
           self->TABLE_EVENT_HANDLER, event
           RETURN
         END
         ELSE:
       ENDCASE

       ; First call the superclass method
       self->BST_INST_ADDON_GUI::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       ; Start main event loop
       CASE event.id OF


         ;ui_menu_save
         state.id_struct.ui_menu_save: BEGIN

           filename = DIALOG_PICKFILE(DIALOG_PARENT=self.gui_id $
                                      ,/WRITE $
                                      ,FILE=filename $
                                      ,PATH= c_gui_param.path_state $
                                      ,GET_PATH=get_path $
                                      ,FILTER=['*.sav'] $
                                      ,/OVERWRITE_PROMPT $
                                      ,TITLE='Chose file to save profiles in.')

           IF filename THEN BEGIN
             ; Save the current path.
             c_gui_param.path_state = get_path

             self->SAVE_RESULTS, filename, /OVERWRITE $
               ,DIALOG_PARENT=self.gui_id

           ENDIF

         END


         ;ui_menu_load
         state.id_struct.ui_menu_load: BEGIN

           filename = DIALOG_PICKFILE(DIALOG_PARENT=self.gui_id $
                                      ,/READ $
                                      ,FILE=filename $
                                      ,PATH=c_gui_param.path_state $
                                      ,GET_PATH=get_path $
                                      ,FILTER=['*.sav'] $
                                      ,/MUST_EXIST)

           IF filename THEN BEGIN
             ; Save the path.
             c_gui_param.path_state = get_path

             ; Restore the state.
             self->LOAD_RESULTS, filename

             ; Update the table
             self->UPDATE
           ENDIF

         END


         ;ui_menu_reset
         state.id_struct.ui_menu_reset: BEGIN
           self->RESET
         END


         ;ui_menu_exit
         state.id_struct.ui_menu_exit: BEGIN
           self->KILL
         END


         ;ui_menu_plot_options
         state.id_struct.ui_menu_plot_options: BEGIN
           self->PLOT_DIALOG
         END


         ;ui_menu_update
         state.id_struct.ui_menu_update: BEGIN
           self->UPDATE
         END


         ;ui_menu_debug
         state.id_struct.ui_menu_debug: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.full_debug = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue
         END


         ;ui_field_focus
         state.id_struct.ui_field_focus: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.focus = value
         END


         ;ui_button_getfit
         state.id_struct.ui_button_getfit: BEGIN
           self->SAVEFIT
         END


         ELSE: BEGIN
           MESSAGE, STRING('BST_INST_PROFILE_SAVER_EVENT: No rule for widget ID: ', event.id)
         ENDELSE
       ENDCASE

     END ;PRO BST_INST_PROFILE_SAVER_EVENT





     ;+=================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::TABLE_EVENT_HANDLER, event

       ; Establish catch block.
       CATCH, error                
       IF (error NE 0) THEN BEGIN 
         ; Cancel catch block.
         CATCH, /CANCEL
         
         BST_INST_MESSAGE, STRING('Caught in BST_INST_FIT_SAVER::EVENT_HANDLER')
         BST_INST_MESSAGE, STRING('Error: '+!ERROR_STATE.MSG)
         
         IF self.full_debug THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF
         
         RETURN
       ENDIF 

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       ; Start main event loop
       CASE event.id OF
         ;ui_table
         state.id_struct.ui_table: BEGIN

           ; First catch any context events
           IF (TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_CONTEXT') THEN BEGIN
             ; Check if this is a valid row
             IF event.row GE 0 THEN BEGIN
               ; Save the row and column information
               self.gui_param.context_row = event.row
               self.gui_param.context_col = event.col
             
               ; Call up the context menu
               context_base = state.id_struct.ui_context_table_base
               WIDGET_DISPLAYCONTEXTMENU, event.id, event.x, event.y, context_base
             ENDIF
           ENDIF

         END

         ;ui_context_delete
         state.id_struct.ui_context_table_delete: BEGIN
           self->CLEAR_ROW, self.gui_param.context_row
         END

         ELSE: BEGIN
           NAME = WIDGET_INFO(event.id, /UNAME)
           error_message = 'No rule for widget: '+name+' ID: '+ STRING(event.id, FORMAT='(I0)')
           MESSAGE, error_message
         ENDELSE
       ENDCASE

     END ; PRO BST_INST_FIT_SAVER::TABLE_EVENT_HANDLER




     ;+=================================================================
     ;
     ; Update the gui to match the current object parameters.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::UPDATE_GUI

       self->UPDATE_GUI_TABLE
       self->UPDATE_GUI_PLOT_DIALOG

     END ;PRO BST_INST_FIT_SAVER::UPDATE_GUI




     ;+=================================================================
     ;
     ; Update the table of fit results to match the currently saved
     ; fits.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::UPDATE_GUI_TABLE

       ; Start a catch block.
       CATCH, error
       IF error NE 0 THEN BEGIN
         ; For now don't do anything here.
         CATCH, /CANCEL
         MESSAGE, /REISSUE
       ENDIF


       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       ; First get the current table values
       WIDGET_CONTROL, state.id_struct.ui_table, GET_VALUE=table_values

       ; Get the size of the table.
       table_size = SIZE(table_values, /DIM)




       ; Extract the basic profile info and the size of the results.
       num_fits = self.fit_list->N_ELEMENTS()




       ; Check the size of the table.  Expand if necessary
       IF table_size[1] LT num_fits THEN BEGIN
         WIDGET_CONTROL, state.id_struct.ui_table $
           ,INSERT_ROW=num_fits-table_size[1]
         
         ; Get the values agian with the expanded table
         WIDGET_CONTROL, state.id_struct.ui_table, GET_VALUE=table_values
       ENDIF

       ; Clear the table
       table_values[*] = ''




       IF num_fits GT 0 THEN BEGIN

         ; Get the number of profiles
         num_profiles = N_TAGS((self.fit_list->GET(0)).profile)


         ; Create the string arrays
         fwhm = STRARR(num_fits)
         value_max = STRARR(num_fits)
         focus = STRARR(num_fits)
         shot = STRARR(num_fits)


         ; Get the fit result
         saves = self.fit_list->TO_ARRAY()

         ; Loop over the profiles
         FOR ii_profile = 0, num_profiles-1 DO BEGIN

           ; Add commas to strings if needed
           IF ii_profile NE 0 THEN BEGIN
             fwhm += ', '
             value_max += ', '
           ENDIF

           ; Extract strings from results.
           fwhm += STRING(saves.profile.(ii_profile).fwhm, FORMAT='(f6.2)')
           value_max += STRING(saves.profile.(ii_profile).value_max, FORMAT='(f8.1)')
             
 
         ENDFOR

         ; Get the user information, that is prameters specific to this
         ; addon.
         focus = STRING(saves.user_param.focus, FORMAT='(f8.3)')
         ;shot = STRING(results[*,0].user_param.shot, FORMAT='(i0)')
             
         ;Mic Focus
         ;table_values[0,0:results_size[0]-1] = shot
         table_values[1,0:num_fits-1] = focus
         table_values[2,0:num_fits-1] = fwhm
         table_values[3,0:num_fits-1] = value_max

       ENDIF

       WIDGET_CONTROL, state.id_struct.ui_table, SET_VALUE=table_values  


     END ;PRO BST_INST_FIT_SAVER::UPDATE_GUI_TABLE



     ;+=================================================================
     ;
     ; Add the current fit to the fit list, then update the table.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::SAVEFIT

       ; First get fit and add to core.
       self.fit_list->SAVE, USER_PARAM=self.param

       ; Now update the table.
       self->UPDATE_GUI_TABLE
       
     END ;PRO BST_INST_FIT_SAVER::SAVEFIT



     ;+=================================================================
     ;
     ; Clear an entry from the saved fit results. Then update the table.
     ; The entry to remove is determined by the row in the table.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::CLEAR_ROW, row

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       num_rows = self.fit_list->N_ELEMENTS()

       IF row GT num_rows-1 THEN RETURN

       message_text = 'Clear row ' + STRING(row, FORMAT='(I0)')+'?'
       dialog_status = DIALOG_MESSAGE(message_text, /QUESTION, /DEFAULT_NO $
                                     ,DIALOG_PARENT=state.id_struct.ui_base)

       IF dialog_status EQ 'No' THEN BEGIN
         RETURN
       ENDIF


       BST_INST_MESSAGE, STRING('Clearing Row ', row, '.', $
                                FORMAT='(a0,i0, a0)')

       
       ; Delete the row from the results list.
       self.fit_list->REMOVE, row

       ; Update the table
       self->UPDATE_GUI_TABLE


     END ;BST_INST_FIT_SAVER::CLEAR_ROW






     ;+=================================================================
     ;
     ; Clear all of the  saved fit results. Then update the table.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::CLEAR_ALL, NOPROMT=noprompt

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       IF self.fit_list->N_ELEMENTS() EQ 0 THEN RETURN

       IF ~ KEYWORD_SET(nopromt) THEN BEGIN
         message_text = 'Clear all fits?'
         dialog_status = DIALOG_MESSAGE(message_text, /QUESTION, /DEFAULT_NO $
                                        ,DIALOG_PARENT=state.id_struct.ui_base)

         IF dialog_status EQ 'No' THEN BEGIN
           RETURN
         ENDIF
       ENDIF


       BST_INST_MESSAGE, STRING('Clearing all fits.')

       
       ; Delete all of the results from the fit list.
       self.fit_list->REMOVE, /ALL

       ; Update the table
       self->UPDATE_GUI_TABLE


     END ;BST_INST_FIT_SAVER::CLEAR_ALL



; =================================================================
; =================================================================
; #################################################################
;
; BEGIN PLOT DIALOG
;
; #################################################################
; =================================================================
; =================================================================



     ;+=================================================================
     ;
     ; Create a dialog to control plot options for this addon.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::PLOT_DIALOG

       event_pro = 'BST_INST_ADDON__EVENT'

       ; Get the position of main window.
       WIDGET_CONTROL, self.gui_id, TLB_GET_OFFSET = addon_gui_position

       ui_base = WIDGET_BASE( GROUP_LEADER=self.gui_id $
                              ,UNAME='UI_PLOT_DIALOG' $
                              ,TITLE='Plot Options' $
                              ,/COLUMN $
                              ,/TLB_KILL_REQUEST_EVENTS $
                              ,MBAR=ui_base_menu_bar $
                              ,XOFFSET=addon_gui_position[0] $
                              ,YOFFSET=addon_gui_position[1] $
                              ,EVENT_PRO=event_pro $
                              ,UVALUE=self $
                            )
       
       ui_base_top = WIDGET_BASE(ui_base, /COLUMN)
         ui_text_title = CW_FIELD_EXT(ui_base_top, UNAME='ui_text_title' $
                                      ,TITLE='Title' $
                                      ,VALUE='' $
                                      ,XSIZE=100 $
                                      ,/STRING $
                                      ,/ALL_EVENTS)

       ui_base_mid = WIDGET_BASE(ui_base, /ROW)
       ui_base_mid_left = WIDGET_BASE(ui_base_mid, /COLUMN)
         plot_types = ['FWHM' $
                       ,'Max Value' $
                       ,'Intensity' $
                       ,'Spectra']
         ui_bg_plots = CW_BGROUP(ui_base_mid_left, plot_types, UNAME='ui_bg_plots' $
                                 ,/NONEXCLUSIVE $
                                 ,/RETURN_INDEX)

       ui_base_mid_right = WIDGET_BASE(ui_base_mid, /COLUMN)
         ui_base_xrange = WIDGET_BASE(ui_base_mid_right, /ROW)
           ui_field_xrange_low = CW_FIELD_EXT(ui_base_xrange, UNAME='ui_field_xrange_low' $
                                               ,VALUE=0 $
                                               , TITLE='X Range' $
                                               ,/FLOAT $
                                               ,XSIZE=4 $
                                               ,/ALL_EVENTS)
           ui_field_xrange_high = CW_FIELD_EXT(ui_base_xrange, UNAME='ui_field_xrange_high' $
                                                ,VALUE=0 $
                                                , TITLE='' $
                                                ,/FLOAT $
                                                ,XSIZE=4 $
                                                ,/ALL_EVENTS)
         ui_base_yrange = WIDGET_BASE(ui_base_mid_right, /ROW)
           ui_field_yrange_low = CW_FIELD_EXT(ui_base_yrange, UNAME='ui_field_yrange_low' $
                                               ,VALUE=0 $
                                               , TITLE='Y Range' $
                                               ,/INT $
                                               ,XSIZE=4 $
                                               ,/ALL_EVENTS)
           ui_field_yrange_high = CW_FIELD_EXT(ui_base_yrange, UNAME='ui_field_yrange_high' $
                                                ,VALUE=0 $
                                                , TITLE='' $
                                                ,/INT $
                                                ,XSIZE=4 $
                                                ,/ALL_EVENTS)

       ui_base_bottom = WIDGET_BASE(ui_base, /COLUMN)
         ui_button_plot = WIDGET_BUTTON(ui_base_bottom, UNAME='ui_button_plot' $
                                        ,VALUE='Plot' $
                                        ,XSIZE=100 $
                                        ,YSIZE=30)

       ui_base_menu_plot = WIDGET_BUTTON(ui_base_menu_bar, VALUE='Plot', /MENU)
         ui_button_plot_pdf = WIDGET_BUTTON(ui_base_menu_plot, UNAME='ui_button_plot_pdf' $
                                           ,VALUE='Plot to PDF')

       ui_base_menu_options = WIDGET_BUTTON(ui_base_menu_bar, VALUE='Options', /MENU)
         ui_menu_normalize = WIDGET_BUTTON(ui_base_menu_options $
                                           ,UNAME='ui_menu_normalize' $
                                           ,VALUE='Normalize Max Value' $
                                           ,/CHECKED_MENU $
                                           ,UVALUE={checked:0})
         ui_menu_correct = WIDGET_BUTTON(ui_base_menu_options $
                                           ,UNAME='ui_menu_correct' $
                                           ,VALUE='Correct Max Value' $
                                           ,/CHECKED_MENU $
                                           ,UVALUE={checked:0})
         ui_menu_align = WIDGET_BUTTON(ui_base_menu_options $
                                       ,UNAME='ui_menu_align' $
                                       ,VALUE='Align' $
                                       ,/CHECKED_MENU $
                                       ,UVALUE={checked:0} )
         ui_menu_vs_mic = WIDGET_BUTTON(ui_base_menu_options $
                                        ,UNAME='ui_menu_vs_mic' $
                                        ,VALUE='Plot vs. Focus' $
                                        ,/CHECKED_MENU $
                                        ,UVALUE={checked:0} $
                                        ,/SEPARATOR)
         ui_menu_vs_shot = WIDGET_BUTTON(ui_base_menu_options $
                                           ,UNAME='ui_menu_vs_shot' $
                                           ,VALUE='Plot vs. Shot' $
                                           ,/CHECKED_MENU $
                                           ,UVALUE={checked:0})
         ui_menu_vs_loc_max = WIDGET_BUTTON(ui_base_menu_options $
                                           ,UNAME='ui_menu_vs_loc_max' $
                                           ,VALUE='Plot vs. Location of Maximum' $
                                           ,/CHECKED_MENU $
                                           ,UVALUE={checked:0})
         
       id_struct = { $
                     ui_base:ui_base $
                     ,ui_text_title:ui_text_title $
                     ,ui_bg_plots:ui_bg_plots $
                     ,ui_field_xrange_low:ui_field_xrange_low $
                     ,ui_field_xrange_high:ui_field_xrange_high $
                     ,ui_field_yrange_low:ui_field_yrange_low $
                     ,ui_field_yrange_high:ui_field_yrange_high $
                     ,ui_button_plot:ui_button_plot $
                     ,ui_menu_normalize:ui_menu_normalize $
                     ,ui_menu_correct:ui_menu_correct $
                     ,ui_menu_align:ui_menu_align $
                     ,ui_menu_vs_mic:ui_menu_vs_mic $
                     ,ui_menu_vs_shot:ui_menu_vs_shot $
                     ,ui_menu_vs_loc_max:ui_menu_vs_loc_max $
                     ,ui_button_plot_pdf:ui_button_plot_pdf $
                   }
                       
       state = { $
                 id_struct:id_struct $
               }

       self.plot_dialog_id = ui_base

       state_holder = WIDGET_INFO(self.plot_dialog_id, /CHILD)
       WIDGET_CONTROL, state_holder, SET_UVALUE=state



       self->UPDATE_GUI_PLOT_DIALOG

       WIDGET_CONTROL, ui_base, /REALIZE

       XMANAGER, 'BST_INST_FIT_SAVER::PLOT_DIALOG', ui_base $
         ,/NO_BLOCK, EVENT_HANDLER=event_pro
              
     END ; PRO BST_INST_FIT_SAVER::PLOT_DIALOG




     ;+=================================================================
     ;
     ; Update the plot dialog to match the current parameters saved
     ; in the object
     ;
     ;-=================================================================
     PRO  BST_INST_FIT_SAVER::UPDATE_GUI_PLOT_DIALOG

       ; Don't do any thing if the plot dialog is not open.
       IF ~ WIDGET_INFO(self.plot_dialog_id, /VALID_ID) THEN RETURN

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.plot_dialog_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       WIDGET_CONTROL, state.id_struct.ui_text_title $
         ,SET_VALUE=self.plot_param.title

       ; xrange
       WIDGET_CONTROL, state.id_struct.ui_field_xrange_low $
         ,SET_VALUE=self.plot_param.xrange[0]
       WIDGET_CONTROL, state.id_struct.ui_field_xrange_high $
         ,SET_VALUE=self.plot_param.xrange[1]

       ;yrange
       WIDGET_CONTROL, state.id_struct.ui_field_yrange_low $
         ,SET_VALUE=self.plot_param.yrange[0]
       WIDGET_CONTROL, state.id_struct.ui_field_yrange_high $
         ,SET_VALUE=self.plot_param.yrange[1]

       plot_options = [ $
                        self.plot_param.fwhm $
                        ,self.plot_param.value_max $
                        ,self.plot_param.intensity $
                        ,self.plot_param.spectra $
                      ]

       WIDGET_CONTROL, state.id_struct.ui_bg_plots, SET_VALUE=plot_options
                        

       ; normalize
       WIDGET_CONTROL, state.id_struct.ui_menu_normalize, GET_UVALUE=uvalue
       uvalue.checked = self.plot_param.normalize
       WIDGET_CONTROL, state.id_struct.ui_menu_normalize, SET_UVALUE=uvalue
       WIDGET_CONTROL, state.id_struct.ui_menu_normalize, SET_BUTTON=uvalue.checked

       ; correct
       WIDGET_CONTROL, state.id_struct.ui_menu_correct, GET_UVALUE=uvalue
       uvalue.checked = self.plot_param.correct
       WIDGET_CONTROL, state.id_struct.ui_menu_correct, SET_UVALUE=uvalue
       WIDGET_CONTROL, state.id_struct.ui_menu_correct, SET_BUTTON=uvalue.checked

       ; align
       WIDGET_CONTROL, state.id_struct.ui_menu_align, GET_UVALUE=uvalue
       uvalue.checked = self.plot_param.align
       WIDGET_CONTROL, state.id_struct.ui_menu_align, SET_UVALUE=uvalue
       WIDGET_CONTROL, state.id_struct.ui_menu_align, SET_BUTTON=uvalue.checked

       ; plot_vs_mic
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, GET_UVALUE=uvalue
       uvalue.checked = self.plot_param.plot_vs_mic
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, SET_UVALUE=uvalue
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, SET_BUTTON=uvalue.checked

       ; plot_vs_shot
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, GET_UVALUE=uvalue
       uvalue.checked = self.plot_param.plot_vs_shot
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, SET_UVALUE=uvalue
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, SET_BUTTON=uvalue.checked

       ; plot_vs_loc_max
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, GET_UVALUE=uvalue
       uvalue.checked = self.plot_param.plot_vs_loc_max
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, SET_UVALUE=uvalue
       WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, SET_BUTTON=uvalue.checked

     END ; BST_INST_FIT_SAVER::PLOT_DIALOG


     ;+=================================================================
     ;
     ; Destroy the plot dialog widget.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::PLOT_DIALOG_KILL

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.plot_dialog_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       ; Destroy all widgets
       IF WIDGET_INFO(state.id_struct.ui_base, /VALID_ID) THEN $
         WIDGET_CONTROL, state.id_struct.ui_base, /DESTROY


     END ;PRO BST_INST_FIT_SAVER::PLOT_DIALOG_KILL



     ; =================================================================
     ; Here we have the event manager for the plot dialog
     ;
     PRO BST_INST_FIT_SAVER::PLOT_DIALOG_EVENT_HANDLER, event

       ; Establish catch block.
       CATCH, error          

       ; An error occurs.         
       IF (error NE 0) THEN BEGIN 
         ; Cancel catch block.
         CATCH, /CANCEL

         BST_INST_MESSAGE, STRING('Caught in BST_INST_PROFILE_PLOT_DIALOG_EVENT.')
         BST_INST_MESSAGE, STRING('Error: '+!ERROR_STATE.MSG)

         IF self.full_debug THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF

         RETURN
       ENDIF  

       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         self->PLOT_DIALOG_KILL
         RETURN
       ENDIF

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.plot_dialog_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state



       ; Start main event loop
       CASE event.id OF


         ;ui_text_title
         state.id_struct.ui_text_title: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.plot_param.title = value
         END


         ;ui_bg_plots
         state.id_struct.ui_bg_plots: BEGIN
           CASE event.value OF
             0: self.plot_param.fwhm = event.select
             1: self.plot_param.value_max = event.select
             2: self.plot_param.intensity = event.select
             3: self.plot_param.spectra = event.select
           ENDCASE
         END


         ;ui_field_xrange_low
         state.id_struct.ui_field_xrange_low: BEGIN 
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.plot_param.xrange[0] = value
         END


         ;ui_field_xrange_high
         state.id_struct.ui_field_xrange_high: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.plot_param.xrange[1] = value
         END


         ;ui_field_yrange_low
         state.id_struct.ui_field_yrange_low: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.plot_param.yrange[0] = value
         END


         ;ui_field_yrange_high
         state.id_struct.ui_field_yrange_high: BEGIN 
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.plot_param.yrange[1] = value
         END


         ;ui_button_plot
         state.id_struct.ui_button_plot: BEGIN
           self->PLOT
         END


         ;ui_button_plot_pdf
         state.id_struct.ui_button_plot_pdf: BEGIN
           self->PLOT, /PDF
         END


         ;ui_menu_normalize
         state.id_struct.ui_menu_normalize: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.plot_param.normalize = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue
         END


         ;ui_menu_correct
         state.id_struct.ui_menu_correct: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.plot_param.correct = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue
         END


         ;ui_menu_align
         state.id_struct.ui_menu_align: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.plot_param.align = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue
         END


         ;ui_menu_vs_mic
         state.id_struct.ui_menu_vs_mic: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.plot_param.plot_vs_mic = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue


           ; Turn off plot_vs_shot
           uvalue.checked = ~uvalue.checked
           self.plot_param.plot_vs_shot = uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, SET_BUTTON=uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, SET_UVALUE=uvalue           

           ; Turn off plot_vs_loc_max
           self.plot_param.plot_vs_loc_max = uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, SET_BUTTON=uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, SET_UVALUE=uvalue

         END


         ;ui_menu_vs_shot
         state.id_struct.ui_menu_vs_shot: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.plot_param.plot_vs_shot = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue

           ; Turn off plot_vs_mic
           uvalue.checked = ~uvalue.checked
           self.plot_param.plot_vs_mic = uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, SET_BUTTON=uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, SET_UVALUE=uvalue  

           ; Turn off plot_vs_loc_max
           self.plot_param.plot_vs_loc_max = uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, SET_BUTTON=uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_loc_max, SET_UVALUE=uvalue 
         END


         ;ui_menu_vs_loc_max
         state.id_struct.ui_menu_vs_loc_max: BEGIN
           WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

           WIDGET_CONTROL, event.id, SET_BUTTON=~uvalue.checked
           self.plot_param.plot_vs_loc_max = ~uvalue.checked
           uvalue.checked = ~uvalue.checked

           WIDGET_CONTROL, event.id, SET_UVALUE=uvalue

           ; Turn off plot_vs_mic
           uvalue.checked = ~uvalue.checked
           self.plot_param.plot_vs_mic = uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, SET_BUTTON=uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_mic, SET_UVALUE=uvalue  

           ; Turn off plot_vs_shot
           self.plot_param.plot_vs_shot = uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, SET_BUTTON=uvalue.checked
           WIDGET_CONTROL, state.id_struct.ui_menu_vs_shot, SET_UVALUE=uvalue 
         END
         ELSE: BEGIN
           uname = WIDGET_INFO(event.id, /UNAME)
           MESSAGE, STRING('No rule for: ', uname)
         ENDELSE
       ENDCASE

     END ;PRO BST_INST_FIT_SAVER::PLOT_DIALOG_EVENT_HANDLER



; =================================================================
; =================================================================
; #################################################################
;
; BEGIN CORE ROUTIENS
;
; #################################################################
; =================================================================
; =================================================================




     ;+=================================================================
     ; 
     ; Save the current fit list.
     ;
     ; NOTE: When the addon state is saved, this is done automatically.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::SAVE_RESULTS, filename $
                                           ,OVERWRITE=overwrite $
                                           ,DIALOG_PARENT=parent
       
       IF NOT KEYWORD_SET(overwrite) THEN BEGIN
         IF FILE_TEST(filename) THEN BEGIN

           message_text = filename +' exists. Overwrite?'
           dialog_status = DIALOG_MESSAGE(message_text, /QUESTION, /DEFAULT_NO $
                                          ,DIALOG_PARENT=dialog_parent)
           IF dialog_status EQ 'No' THEN BEGIN
             RETURN
           ENDIF

         ENDIF
       ENDIF
       
       fit_list = self.fit_list
       SAVE, fit_list, FILENAME=filename

     END ;PRO BST_INST_FIT_SAVER::SAVE_RESULTS



     ;+=================================================================
     ; 
     ; Restore a fit list from a file.
     ;
     ; NOTE: When the addon state is loaded, this is done automatically.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::LOAD_RESULTS, filename
       
       IF NOT  FILE_TEST(filename, /REGULAR) THEN BEGIN
         MESSAGE, filename + ' not found.'
       ENDIF

       ; First destroy the current fit_list
       IF OBJ_VALID(self.fit_list) THEN BEGIN
         OBJ_DESTROY, self.fit_list
       ENDIF

       RESTORE, filename

       self.fit_list = fit_list

       self->UPDATE

     END ;PRO BST_INST_FIT_SAVER::LOAD_RESULTS



     ;+=================================================================
     ;
     ; Print out some of the basic profile profile parameters.
     ; 
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::PRINT


       num_fits = self.fit_list->N_ELEMENTS()

       IF num_fits EQ 0 THEN BEGIN
         RETURN
       ENDIF
       
       saves = self.fit_list->TO_ARRAY()

       ; Get the number of profiles
       num_profiles = N_TAGS((self.fit_list->GET(0)).profile)


       format_v1 = '(i2, 2x, i6, 2x, f8.3, $)'
       format_v2 = '((" # ", f10.3, 2x, e10.3), $)'
       format_t0 = '(20x, a0, $)'
       format_t1 = '((" # ", a7, 1x, i1, 13x, ""), $)'
       format_t2 = '((" # ", a10, 2x, a10), $)'
       

       ;PRINT, FORMAT=format_t0, ''
       FOR ii = 0,num_profiles-1 DO BEGIN
         PRINT, FORMAT=format_t1, 'PROFILE', ii+1
       ENDFOR
       PRINT, ''

       ;PRINT, FORMAT=format_t0, ''
       FOR ii = 0,num_profiles-1 DO BEGIN
         PRINT, FORMAT=format_t2, 'FWHM', 'VALUE MAX'
       ENDFOR
       PRINT, ''
      

       ; Loop through all the saves
       FOR ii_fit = 0,num_fits-1 DO BEGIN
         ;PRINT, FORMAT=format_v1, i,  results[i,0].shot, results[i,0].focus
         FOR ii_profile = 0,results_dim[1]-1 DO BEGIN
           PRINT, FORMAT=format_v2, saves[ii_fit].(ii_profile).fwhm, saves[ii_fit].(ii_profile).value_max
         ENDFOR
         PRINT, ''
       ENDFOR
       
       
     END ;PRO BST_INST_FIT_SAVER::PRINT


     ;+=================================================================
     ; 
     ; Plot various profile parameters as a funciton of fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::PLOT, PDF=pdf $
                           ,X_FACTOR=x_factor $
                           ,X_SHIFT=x_shift $
                           ,_EXTRA=extra


       ; Establish catch block.
       CATCH, error
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL

         ; Take cleanup actions
         SUPERPLOT_CLEANUP

         ; Print out error messages.
         BST_INST_MESSAGE, STRING('Plotting Failed.')
         BST_INST_MESSAGE, STRING('Caught in BST_INST_PROFILE_PLOT.')
         BST_INST_MESSAGE, STRING('Error: '+!ERROR_STATE.MSG)

         IF self.full_debug THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF

         RETURN
       ENDIF  

       RESOLVE_ROUTINE, [ $
                          'superplot' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE



       MIR_DEFAULT, x_factor, 1E
       MIR_DEFAULT, x_shift, 0E



       num_fits = self.fit_list->N_ELEMENTS()

       IF num_fits EQ 0 THEN BEGIN
         RETURN
       ENDIF
       
       saves = self.fit_list->TO_ARRAY()

       ; Get the number of profiles
       num_profiles = N_TAGS((self.fit_list->GET(0)).profile)

  

       ; Create an index to determine the order in which the fits should 
       ; be plotted.  By default this is the order in which they were saved.  
       ; If the sort option is active, then sort by the focus parameter.
       IF KEYWORD_SET(self.plot_param.sort)  THEN BEGIN
         IF self.plot_param.sort EQ 1 THEN BEGIN
           index = SORT(saves.user_param.focus)
         ENDIF ELSE BEGIN
           index = REVERSE(SORT(saves.user_param.focus))
         ENDELSE
       ENDIF ELSE BEGIN
         index = INDGEN(num_fits)
       ENDELSE



       ; ---------------------------------------------------------------
       ; Setup SUPERPLOT for plotting

       SUPERPLOT_CLEAR
       SUPERPLOT_SET, TITLE=self.plot_param.title $
         ,XRANGE=self.plot_param.xrange $
         ,YRANGE=self.plot_param.yrange       

       IF KEYWORD_SET(pdf) THEN BEGIN
         SUPERPLOT_SET, /PDF
       END



       ; Setup the colors.
       color_spacing = (255/num_profiles)


       ; Used to determine if profile lables are needed
       profile_plot = 0
       
       num_plots = 0





       IF self.plot_param.xrange[1] GT self.plot_param.xrange[0] THEN BEGIN
         where_xrange =  WHERE( $
                         (saves.user_param.focus*x_factor + x_shift GE self.plot_param.xrange[0]) $
                         AND (saves.user_param.focus*x_factor + x_shift LE self.plot_param.xrange[1]))
       ENDIF ELSE BEGIN
         where_xrange = INDGEN(num_fits)
       ENDELSE




       ; ---------------------------------------------------------------
       ; fwhm

       IF KEYWORD_SET(self.plot_param.fwhm) THEN BEGIN
         yrange = [0E,0E]

                               
         FOR ii=0,num_profiles-1 DO BEGIN
           yrange[1] = yrange[1] > MAX(saves[where_xrange].profile.(ii).fwhm, MIN=min_fwhm)
           IF ii GT 0 THEN BEGIN
             yrange[0] = yrange[0] < min_fwhm
           ENDIF ELSE BEGIN
             yrange[0] = min_fwhm
           ENDELSE
         ENDFOR 


         oplot = 0
         FOR ii=0,num_profiles-1 DO BEGIN
           CASE 1 OF
             KEYWORD_SET(self.plot_param.plot_vs_mic):BEGIN
               x = saves[index].user_param.focus
             END
             ;KEYWORD_SET(self.plot_param.plot_vs_shot):BEGIN
             ;  x = saves[index].shot
             ;END
             KEYWORD_SET(self.plot_param.plot_vs_loc_max):BEGIN
               x = saves[index].profile.(0).location_max
             END
           ENDCASE

           plot = { $
                  x:x*x_factor + x_shift $
                  ,y:saves[index].profile.(ii).fwhm $
                  ,title:'FWHM' $
                  ,xrange:self.plot_param.xrange $
                  ,yrange:yrange $
                  ,color:color_spacing*ii $
                  ,oplot:oplot $
                }

           SUPERPLOT_ADD, plot

           oplot = 1
         ENDFOR

         profile_plot = 1
         num_plots += 1
       ENDIF




       ; ---------------------------------------------------------------
       ; value_max
       IF KEYWORD_SET(self.plot_param.value_max) THEN BEGIN


         yrange=[0E,0E]
         FOR ii=0,num_profiles-1 DO BEGIN
           yrange[1] = yrange[1] > MAX(saves.profile.(ii).value_max, MIN=min_value_max)
           IF ii GT 0 THEN BEGIN
             yrange[0] = yrange[0] < min_value_max
           ENDIF ELSE BEGIN
             yrange[0] = min_value_max
           ENDELSE
         ENDFOR


         oplot = 0
         FOR ii=0,num_profiles-1 DO BEGIN

           CASE 1 OF
             KEYWORD_SET(self.plot_param.plot_vs_mic):BEGIN
               x = saves[index].user_param.focus
             END
             ;KEYWORD_SET(self.plot_param.plot_vs_shot):BEGIN
             ;  x = saves[index].shot
             ;END
             KEYWORD_SET(self.plot_param.plot_vs_loc_max):BEGIN
               x = saves[index].profile.(0).location_max
             END
           ENDCASE

  
           ; Deal with correcting and normalizing the maximum value
           value_max = saves[index].profile.(ii).value_max

           IF KEYWORD_SET(self.plot_param.correct) THEN BEGIN
               max_intensity = MAX(saves.profile.(ii).intensity)
               value_max = value_max * max_intensity / saves.profile.(ii).intensity
           ENDIF
           
           IF KEYWORD_SET(self.plot_param.normalize) THEN BEGIN
             max_value_max =  MAX(saves.profile.(ii).value_max)
             value_max = value_max / max_value_max
           ENDIF

           plot = { $
                  x:x*x_factor + x_shift $
                  ,y:value_max $
                  ,title:'Maximim Profile Value' $
                  ,xrange:self.plot_param.xrange $
                  ,yrange:yrange $
                  ,color:color_spacing*ii $
                  ,oplot:oplot $
                  }
           SUPERPLOT_ADD, plot

           oplot = 1
         ENDFOR

         profile_plot = 1
         num_plots += 1
       ENDIF




       ; ---------------------------------------------------------------
       ; Plot the legend
       IF profile_plot THEN BEGIN
         FOR ii=0,num_profiles-1 DO BEGIN
           plot = { $
                  x:0.2 $
                  ,y:(.95-.02*ii*num_plots) $
                  ,string:STRING(FORMAT='(a0,1x,i0)', 'Profile', ii+1) $
                  ,xyouts:1 $
                  ,plotnum:0 $
                  ,color:color_spacing*ii $
                  }
           SUPERPLOT_ADD, plot
         ENDFOR
       ENDIF
       


       ; ---------------------------------------------------------------
       ; Spectra
       IF KEYWORD_SET(self.plot_param.spectra) THEN BEGIN
         self->PLOT_SPECTRUM
         num_plots += 1
       ENDIF
       


       ; ---------------------------------------------------------------
       ; Finish plotting.
       IF num_plots GT 1 THEN BEGIN
         SUPERPLOT_SET, /PORTRAIT
       ENDIF ELSE BEGIN
         SUPERPLOT_SET, /LANDSCAPE
       ENDELSE

       

       IF num_plots GT 0 THEN BEGIN
         SUPERPLOT_MULTI, DEBUG=self.full_debug
       ENDIF

       SUPERPLOT_CLEANUP
       
     END ;PRO BST_INST_FIT_SAVER::PLOT


   


     ;+=================================================================
     ; 
     ; Plot the fitted profile for every fit.
     ; 
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::PLOT_SPECTRUM

       num_saves = self.fit_list->N_ELEMENTS()
       
       IF num_saves EQ 0 THEN BEGIN
         BST_INST_MESSAGE, 'No fits saved.'
         RETURN
       ENDIF

       saves = self.fit_list->TO_ARRAY()

       IF KEYWORD_SET(self.plot_param.sort)  THEN BEGIN
         focus = DBLARR(num_saves)
         FOR ii=0,num_saves-1 DO BEGIN
           focus[ii] = saves[ii].user_param.focus
         ENDFOR
         sort_results = SORT(focus)
         IF self.plot_param.sort EQ 1 THEN BEGIN
           index = sort_results
         ENDIF ELSE BEGIN
           index = REVERSE(sort_results)
         ENDELSE
       ENDIF ELSE BEGIN
         index = INDGEN(num_saves)
       ENDELSE

       ; Set the default profile to use for alignment and ploting vs maximum location.
       IF KEYWORD_SET(self.plot_param.align) THEN BEGIN
         align = self.plot_param.align
       ENDIF ELSE BEGIN
         align = 1
       ENDELSE


       color_spacing = (255/num_saves)
      

       xyouts_x = 0.1
       xyouts_y = 0.9

       oplot = 0
       FOR ii = 0,num_saves-1 DO BEGIN
       
         IF KEYWORD_SET(self.plot_param.align) THEN BEGIN
           ; Note this assumes that the profiles are order in acending order
           ; starting from one.  I should improve this eventually.
           x = saves[index[ii]].spectrum.x - $
               (saves[index[ii]].profile.(align-1).location_max $
                - saves[index[0]].profile.(align-1).location_max )
         ENDIF ELSE BEGIN
           x = saves[index[ii]].spectrum.x
         ENDELSE


         plot = {x:x $
                ,y:saves[index[ii]].spectrum.y $
                ,color:ii*color_spacing $
                ,psym:0 $
                ,oplot:oplot}
         SUPERPLOT_ADD, plot


         CASE 1 OF
           KEYWORD_SET(self.plot_param.plot_vs_mic):BEGIN
             label = STRING(saves[index[ii]].user_param.focus, FORMAT='(f8.3)')
           END
           ;KEYWORD_SET(self.plot_param.plot_vs_shot):BEGIN
           ;  label = STRING(saves[index[ii]].fit_param.shot, FORMAT='(i0)')
           ;END
           KEYWORD_SET(self.plot_param.plot_vs_loc_max):BEGIN
             label = STRING(saves[index[ii]].profile.(align-1).location_max, FORMAT='(f8.3)')
           END
         ENDCASE


         xyouts = {x:xyouts_x $
                   ,y:xyouts_y $
                   ,string:label $
                   ,charsize:2 $
                   ,color:ii*color_spacing $
                   ,xyouts:1 $
                  }
         SUPERPLOT_ADD, xyouts


         xyouts_y -= 0.04
         oplot = 1
           
       ENDFOR
  
       


     END ;PRO BST_INST_FIT_SAVER::PLOT_SPECTRUM




     ;+=================================================================
     ;
     ; Return a structure with the default plot parameters for this 
     ; addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_SAVER::DEFAULT_PLOT_PARAMS

       default_plot_param = { $
                              title:'' $
                              ,xrange:[0D,0D] $
                              ,yrange:[0D,0D] $
                              ,fwhm:1 $
                              ,value_max: 1 $
                              ,intensity: 0 $
                              ,spectra: 0 $
                              ,sort:1 $
                              ,normalize:0 $
                              ,correct:0 $
                              ,align:0 $
                              ,xfactor:0D $
                              ,xshift:0D $
                              ,plot_vs_mic:1 $
                              ,plot_vs_shot:0 $
                              ,plot_vs_loc_max:0 $
                            }

       RETURN, default_plot_param

     END ; FUNCTION BST_INST_FIT_SAVER::DEFAULT_PLOT_PARAMS




     ;+=================================================================
     ;
     ; Return a structure with the default parameters for this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_SAVER::DEFAULT_PARAMS

       defaults = {plot_param:self->DEFAULT_PLOT_PARAMS()}

       RETURN, defaults

     END ; FUNCTION BST_INST_FIT_SAVER::DEFAULT_PARAMS




     ;+=================================================================
     ;
     ; Set the addon parameters to their defaults.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER::RESET_PARAMS

       ; Clear the fit list.
       self.fit_list->REMOVE, /ALL

       ; Reset the plot options.
       defaults = self->DEFAULT_PARAMS()
       
       STRUCT_ASSIGN, defaults, self, /NOZERO
     END ; PRO BST_INST_FIT_SAVER::RESET_PARAMS





     ;+=================================================================
     ;
     ; Return the fit_list object
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_SAVER::GET_FIT_LIST

       RETURN, self.fit_list

     END ; FUNCTION BST_INST_FIT_SAVER::DEFAULT_PARAMS





; ======================================================================
; ======================================================================
; ######################################################################
;
; EXTERNAL ROUTINES
;     These are methods that can be called from outside
;     of the object.  They may be used internally as well.
;
; ######################################################################
; ======================================================================
; ======================================================================




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_FIT_SAVER__DEFINE

       struct = { BST_INST_FIT_SAVER $
                  ,plot_dialog_id:0 $
                  ,fit_list:OBJ_NEW() $
                  ,param:{BST_INST_FIT_SAVE_PARAM $
                          ,focus:0D $
                         } $
                  ,gui_param:{BST_INST_FIT_SAVER_GUI_PARAM $
                              ,context_row:0 $
                              ,context_col:0 $
                             } $
                  ,plot_param:{BST_INST_FIT_SAVER_PLOT_PARAM $
                               ,title:'' $
                               ,xrange:[0D,0D] $
                               ,yrange:[0D,0D] $
                               ,fwhm:0 $
                               ,value_max:0 $
                               ,intensity:0 $
                               ,spectra:0 $
                               ,sort:0 $
                               ,normalize:0 $
                               ,correct:0 $
                               ,align:0 $
                               ,xfactor:0D $
                               ,xshift:0D $
                               ,plot_vs_mic:0 $
                               ,plot_vs_shot:0 $
                               ,plot_vs_loc_max:0 $
                              } $
                  ,INHERITS BST_INST_ADDON_GUI }

     END ; PRO BST_INST_FIT_SAVER__DEFINE


     ;+=================================================================
     ; 
     ;-=================================================================
