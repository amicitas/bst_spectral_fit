




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A widget to contral GAUSSIAN models for <BST_SPECTRAL_FIT>
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_BSTARK::INIT, model $
                                            ,model_continuum $
                                            ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model
       self->SET_MODEL_CONTINUUM, model_continuum

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_BSTARK::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_BSTARK::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK::SET_MODEL_CONTINUUM, model_continuum
       
       MIR_DEFAULT, model_continuum, OBJ_NEW()

       self.model_continuum =  model_continuum

     END ; PRO BST_INST_WIDGET_BSTARK::SET_MODEL_CONTINUUM



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the <BST_INST_WIDGET_BSTARK::>
     ;   object.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK::GUI_DEFINE, FONT=LabelFont $
                                             ,FRAME=frame $
                                             ,SUB_FRAME=sub_frame $
                                             ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 0
       MIR_DEFAULT, sub_frame, 1


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/COLUMN $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)

       ui_bgroup_use = CW_BGROUP(ui_base, UNAME='ui_bgroup_use' $
                                 ,'Use' $
                                 ,/NONEXCLUSIVE $
                                 ,FRAME=0)

       ui_base_sub = WIDGET_BASE(ui_base, UNAME='ui_base_sub' $
                                 ,/COLUMN $
                                 ,ALIGN_CENTER=align_center $
                                 ,SENSITIVE=0 $
                                 ,FRAME=sub_frame)

       ui_base_top =  WIDGET_BASE(ui_base_sub, UNAME='ui_base_top' $
                                 ,/ROW $
                                 ,ALIGN_CENTER=align_center)


       ui_base_top_left =  WIDGET_BASE(ui_base_top, UNAME='ui_base_top_left' $
                                       ,/COLUMN $
                                       ,ALIGN_CENTER=align_center)
       ui_base_top_center =  WIDGET_BASE(ui_base_top, UNAME='ui_base_top_center' $
                                         ,/COLUMN $
                                         ,ALIGN_CENTER=align_center $
                                         ,XSIZE=40)
       ui_base_top_right =  WIDGET_BASE(ui_base_top, UNAME='ui_base_top_right' $
                                       ,/COLUMN $
                                       ,ALIGN_CENTER=align_center)


         ; ---------------------------------------------------------------------
         option_names = ['fix' $
                         ,'save' $
                         ,'no_field' $
                         ,'fix central line width' $
                         ,'include water line' $
                         ,'include continuum' $
                         ,'pop_levels_from_full' $
                         ,'pop_levels_symmetric' $
                         ,'pop_levels_calculated' $
                        ]
         ui_bgroup_options = CW_BGROUP(ui_base_top_right, UNAME='ui_bgroup_options' $
                                       ,option_names $
                                       ,/NONEXCLUSIVE $
                                       ,/COLUMN)

         ; ---------------------------------------------------------------------
         label_size = 70

         ui_base_ratio = WIDGET_BASE(ui_base_top_left, /ROW)
           ui_field_ratio = CW_FIELD_EXT(ui_base_ratio, UNAME='ui_field_ratio' $
                                         ,VALUE='' $
                                         ,TITLE='Ratio' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_ratio = CW_BGROUP(ui_base_ratio, UNAME='ui_bgroup_ratio' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_vbeam = WIDGET_BASE(ui_base_top_left, /ROW)
           ui_field_vbeam = CW_FIELD_EXT(ui_base_vbeam, UNAME='ui_field_vbeam' $
                                         ,VALUE='' $
                                         ,TITLE='Vbeam' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(e0.3)' $
                                         ,XSIZE=10 $
                                         ,LABEL_XSIZE=label_size)
           ui_bgroup_vbeam = CW_BGROUP(ui_base_vbeam, UNAME='ui_bgroup_vbeam' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_er_eff = WIDGET_BASE(ui_base_top_left, /ROW)
           ui_field_er_eff = CW_FIELD_EXT(ui_base_er_eff, UNAME='ui_field_er_eff' $
                                         ,VALUE='' $
                                         ,TITLE='Er_Eff' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(e0.3)' $
                                         ,XSIZE=10 $
                                          ,LABEL_XSIZE=label_size)
           ui_bgroup_er_eff = CW_BGROUP(ui_base_er_eff, UNAME='ui_bgroup_er_eff' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)

         ui_base_temperature = WIDGET_BASE(ui_base_top_left, /ROW)
           ui_field_temperature = CW_FIELD_EXT(ui_base_temperature, UNAME='ui_field_temperature' $
                                         ,VALUE='' $
                                         ,TITLE='Temperature' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                               ,LABEL_XSIZE=label_size)
           ui_bgroup_temperature = CW_BGROUP(ui_base_temperature, UNAME='ui_bgroup_temperature' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ui_base_lambda0 = WIDGET_BASE(ui_base_top_left, /ROW)
           ui_field_lambda0 = CW_FIELD_EXT(ui_base_lambda0, UNAME='ui_field_lambda0' $
                                         ,VALUE='' $
                                         ,TITLE='Lambda0' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                           ,LABEL_XSIZE=label_size)
           ui_bgroup_lambda0 = CW_BGROUP(ui_base_lambda0, UNAME='ui_bgroup_lambda0' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)

         ui_base_water = WIDGET_BASE(ui_base_top_left, /ROW)
           ui_field_water = CW_FIELD_EXT(ui_base_water, UNAME='ui_field_water' $
                                         ,VALUE='' $
                                         ,TITLE='Water' $
                                         ,/DOUBLE $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=10 $
                                           ,LABEL_XSIZE=label_size)
           ui_bgroup_water = CW_BGROUP(ui_base_water, UNAME='ui_bgroup_water' $
                                       ,'fix' $
                                       ,/NONEXCLUSIVE)


         ; -------------------------------------------------------------
         ; Setup level population controls.
         ; 
         ; IDL does a terrible job of automatically sizing the tabels.
         ; Here I am explicitly specifying everything.
         column_width = 75

         ui_base_level_full = WIDGET_BASE(ui_base_sub, /COLUMN)
           ui_base_level_full_label = WIDGET_BASE(ui_base_level_full, /ROW)
             ui_label_level_full = WIDGET_LABEL(ui_base_level_full_label $
                                                ,VALUE='Level populations full' $
                                                ,/ALIGN_LEFT)
             ui_bgroup_level_full = CW_BGROUP(ui_base_level_full_label, UNAME='ui_bgroup_level_full' $
                                              ,'fix' $
                                              ,/NONEXCLUSIVE)
           ui_table_level_full = WIDGET_TABLE(ui_base_level_full, UNAME='ui_table_level_full' $
                                              ,/NO_COLUMN_HEADERS $
                                              ,/NO_ROW_HEADERS $
                                              ,SCR_XSIZE=column_width*6+4 $
                                              ,SCR_YSIZE=22 $
                                              ,XSIZE=6 $
                                              ,YSIZE=1 $
                                               ,COLUMN_WIDTH=column_width $
                                              ,FORMAT='(e10.3)' $
                                              ,/EDITABLE $
                                              ,/ALL_EVENTS $
                                             )
           WIDGET_CONTROL, ui_table_level_full, SET_TABLE_SELECT=[-1,-1,-1,-1]


         ui_base_level_half = WIDGET_BASE(ui_base_sub, /COLUMN)
           ui_base_level_half_label = WIDGET_BASE(ui_base_level_half, /ROW)
             ui_label_level_half = WIDGET_LABEL(ui_base_level_half_label $
                                                ,VALUE='Level populations half' $
                                                ,/ALIGN_LEFT)
             ui_bgroup_level_half = CW_BGROUP(ui_base_level_half_label, UNAME='ui_bgroup_level_half' $
                                              ,'fix' $
                                              ,/NONEXCLUSIVE)


           ui_table_level_half = WIDGET_TABLE(ui_base_level_half, UNAME='ui_table_level_half' $
                                              ,/NO_COLUMN_HEADERS $
                                              ,/NO_ROW_HEADERS $
                                              ,SCR_XSIZE=column_width*6+4 $
                                              ,SCR_YSIZE=22 $
                                              ,XSIZE=6 $
                                              ,YSIZE=1 $
                                               ,COLUMN_WIDTH=column_width $
                                              ,FORMAT='(e10.3)' $
                                              ,/EDITABLE $
                                              ,/ALL_EVENTS $
                                             )
           WIDGET_CONTROL, ui_table_level_half, SET_TABLE_SELECT=[-1,-1,-1,-1]


         ui_base_level_third = WIDGET_BASE(ui_base_sub, /COLUMN)
           ui_base_level_third_label = WIDGET_BASE(ui_base_level_third, /ROW)
             ui_label_level_third = WIDGET_LABEL(ui_base_level_third_label $
                                                ,VALUE='Level populations third' $
                                                ,/ALIGN_LEFT)
             ui_bgroup_level_third = CW_BGROUP(ui_base_level_third_label, UNAME='ui_bgroup_level_third' $
                                              ,'fix' $
                                              ,/NONEXCLUSIVE)
           ui_table_level_third = WIDGET_TABLE(ui_base_level_third, UNAME='ui_table_level_third' $
                                               ,/NO_COLUMN_HEADERS $
                                               ,/NO_ROW_HEADERS $
                                               ,SCR_XSIZE=column_width*6+4 $
                                               ,SCR_YSIZE=22 $
                                               ,XSIZE=6 $
                                               ,YSIZE=1 $
                                               ,COLUMN_WIDTH=column_width $
                                               ,FORMAT='(e10.3)' $
                                               ,/EDITABLE $
                                               ,/ALL_EVENTS $
                                             )
           WIDGET_CONTROL, ui_table_level_third, SET_TABLE_SELECT=[-1,-1,-1,-1]



         ; -------------------------------------------------------------
         ; Setup profile width controls
         ; 
         ; IDL does a terrible job of automatically sizing the tabels.
         ; Here I am explicitly specifying everything.
         ui_base_profile_full = WIDGET_BASE(ui_base_sub, /COLUMN)
           ui_base_profile_full_label = WIDGET_BASE(ui_base_profile_full, /ROW)
             ui_label_profile_full = WIDGET_LABEL(ui_base_profile_full_label $
                                                  ,VALUE='Profile widths full' $
                                                  ,/ALIGN_LEFT)
             ui_bgroup_profile_full = CW_BGROUP(ui_base_profile_full_label $
                                                ,UNAME='ui_bgroup_profile_full' $
                                                ,'fix' $
                                                ,/NONEXCLUSIVE)
           ui_table_profile_full = WIDGET_TABLE(ui_base_profile_full $
                                                ,UNAME='ui_table_profile_full' $
                                                ,/NO_COLUMN_HEADERS $
                                                ,/NO_ROW_HEADERS $
                                                ,SCR_XSIZE=column_width*9+4 $
                                                ,SCR_YSIZE=22 $
                                                ,XSIZE=9 $
                                                ,YSIZE=1 $
                                                ,COLUMN_WIDTH=column_width $
                                                ,FORMAT='(f0.3)' $
                                                ,/EDITABLE $
                                                ,/ALL_EVENTS $
                                               )
           WIDGET_CONTROL, ui_table_profile_full, SET_TABLE_SELECT=[-1,-1,-1,-1]


         ui_base_profile_half = WIDGET_BASE(ui_base_sub, /COLUMN)
           ui_base_profile_half_label = WIDGET_BASE(ui_base_profile_half, /ROW)
             ui_label_profile_half = WIDGET_LABEL(ui_base_profile_half_label $
                                                  ,VALUE='Profile widths half' $
                                                  ,/ALIGN_LEFT)
             ui_bgroup_profile_half = CW_BGROUP(ui_base_profile_half_label $
                                                ,UNAME='ui_bgroup_profile_half' $
                                                ,'fix' $
                                                ,/NONEXCLUSIVE)
           ui_table_profile_half = WIDGET_TABLE(ui_base_profile_half $
                                                ,UNAME='ui_table_profile_half' $
                                                ,/NO_COLUMN_HEADERS $
                                                ,/NO_ROW_HEADERS $
                                                ,SCR_XSIZE=column_width*9+4 $
                                                ,SCR_YSIZE=22 $
                                                ,XSIZE=9 $
                                                ,YSIZE=1 $
                                                ,COLUMN_WIDTH=column_width $
                                                ,FORMAT='(f0.3)' $
                                                ,/EDITABLE $
                                                ,/ALL_EVENTS $
                                               )
           WIDGET_CONTROL, ui_table_profile_half, SET_TABLE_SELECT=[-1,-1,-1,-1]


         ui_base_profile_third = WIDGET_BASE(ui_base_sub, /COLUMN)
           ui_base_profile_third_label = WIDGET_BASE(ui_base_profile_third, /ROW)
             ui_label_profile_third = WIDGET_LABEL(ui_base_profile_third_label $
                                                  ,VALUE='Profile widths third' $
                                                  ,/ALIGN_LEFT)
             ui_bgroup_profile_third = CW_BGROUP(ui_base_profile_third_label $
                                                ,UNAME='ui_bgroup_profile_third' $
                                                ,'fix' $
                                                ,/NONEXCLUSIVE)
           ui_table_profile_third = WIDGET_TABLE(ui_base_profile_third $
                                                ,UNAME='ui_table_profile_third' $
                                                ,/NO_COLUMN_HEADERS $
                                                ,/NO_ROW_HEADERS $
                                                ,SCR_XSIZE=column_width*9+4 $
                                                ,SCR_YSIZE=22 $
                                                ,XSIZE=9 $
                                                ,YSIZE=1 $
                                                ,COLUMN_WIDTH=column_width $
                                                ,FORMAT='(f0.3)' $
                                                ,/EDITABLE $
                                                ,/ALL_EVENTS $
                                               )
           WIDGET_CONTROL, ui_table_profile_third, SET_TABLE_SELECT=[-1,-1,-1,-1]



       id_struct = { $
                     ui_bgroup_use:ui_bgroup_use $
                     ,ui_base_sub:ui_base_sub $
                     ,ui_bgroup_options:ui_bgroup_options $

                     ,ui_field_ratio:ui_field_ratio $
                     ,ui_bgroup_ratio:ui_bgroup_ratio $

                     ,ui_field_vbeam:ui_field_vbeam $
                     ,ui_bgroup_vbeam:ui_bgroup_vbeam $

                     ,ui_field_er_eff:ui_field_er_eff $
                     ,ui_bgroup_er_eff:ui_bgroup_er_eff $

                     ,ui_field_temperature:ui_field_temperature $
                     ,ui_bgroup_temperature:ui_bgroup_temperature $

                     ,ui_field_lambda0:ui_field_lambda0 $
                     ,ui_bgroup_lambda0:ui_bgroup_lambda0 $

                     ,ui_field_water:ui_field_water $
                     ,ui_bgroup_water:ui_bgroup_water $

                     ,ui_table_level_full:ui_table_level_full $
                     ,ui_bgroup_level_full:ui_bgroup_level_full $

                     ,ui_table_level_half:ui_table_level_half $
                     ,ui_bgroup_level_half:ui_bgroup_level_half $

                     ,ui_table_level_third:ui_table_level_third $
                     ,ui_bgroup_level_third:ui_bgroup_level_third $

                     ,ui_table_profile_full:ui_table_profile_full $
                     ,ui_bgroup_profile_full:ui_bgroup_profile_full $

                     ,ui_table_profile_half:ui_table_profile_half $
                     ,ui_bgroup_profile_half:ui_bgroup_profile_half $

                     ,ui_table_profile_third:ui_table_profile_third $
                     ,ui_bgroup_profile_third:ui_bgroup_profile_third $

                     }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_BSTARK::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event handler for this widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF


         id.ui_bgroup_use: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=use
           self.model->SET_USE, use[0]
           self->SET_USE, use[0], /LEAVE_BUTTON
         END


         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_options, GET_VALUE=value_bgroup_options

           ; Deal with freezing/unfreezing parameters
           self.model->SET_FIXALL, value_bgroup_options[0]

           ; Deal with saving/unsaving parameters
           self.model->SET_SAVE, value_bgroup_options[1]

           ; Deal with setting the model options
           self.model->SET_OPTIONS, {no_field:value_bgroup_options[2] $
                                     ,fix_central_line_width:value_bgroup_options[3] $
                                     ,include_water_line:value_bgroup_options[4]  $
                                     ,pop_levels_from_full:value_bgroup_options[6]  $
                                     ,pop_levels_symmetric:value_bgroup_options[7]  $
                                     ,pop_levels_calculated:value_bgroup_options[8]  $
                                    }

           self.model_continuum->SET_USE, value_bgroup_options[5]
         END


         id.ui_field_ratio: BEGIN
           self.model->SET_PARAM, {ratio:event.value}
         END
         id.ui_bgroup_ratio: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {ratio:fix[0]}
         END



         id.ui_field_vbeam: BEGIN
           self.model->SET_PARAM, {vbeam:event.value}
         END
         id.ui_bgroup_vbeam: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {vbeam:fix[0]}
         END



         id.ui_field_er_eff: BEGIN
           self.model->SET_PARAM, {er_eff:event.value}
         END
         id.ui_bgroup_er_eff: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {er_eff:fix[0]}
         END



         id.ui_field_temperature: BEGIN
           self.model->SET_PARAM, {temperature:event.value}
         END
         id.ui_bgroup_temperature: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {temperature:fix[0]}
         END



         id.ui_field_lambda0: BEGIN
           self.model->SET_PARAM, {lambda0:event.value}
         END
         id.ui_bgroup_lambda0: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {lambda0:fix[0]}
         END


         id.ui_field_water: BEGIN
           ; For the water line we only ever adjust the central poplevel.
           param = self.model->GET_PARAM()
           water = param.level_population.water
           water[3] = event.value
           self.model->SET_PARAM, {level_population:{water:water}}
         END
         id.ui_bgroup_water: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           fixed = self.model->GET_FIXED()
           water = fixed.level_population.water
           water[3] = fix[0]
           self.model->SET_FIXED, {level_population:{water:water}}
         END


         id.ui_table_level_full: BEGIN
           ; Only take action if the value has changed.
           IF event.type GT 2 THEN RETURN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           value = DOUBLE(value)
           self.model->SET_PARAM, {level_population:{full:value}}
         END
         id.ui_bgroup_level_full: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {level_population:{full:REPLICATE(fix[0], 6)}}
         END

         id.ui_table_level_half: BEGIN
           ; Only take action if the value has changed.
           IF event.type GT 2 THEN RETURN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           value = DOUBLE(value)
           self.model->SET_PARAM, {level_population:{half:value}}
         END
         id.ui_bgroup_level_half: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {level_population:{half:REPLICATE(fix[0], 6)}}
         END

         id.ui_table_level_third: BEGIN
           ; Only take action if the value has changed.
           IF event.type GT 2 THEN RETURN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           value = DOUBLE(value)
           self.model->SET_PARAM, {level_population:{third:value}}
         END
         id.ui_bgroup_level_third: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {level_population:{third:REPLICATE(fix[0], 6)}}
         END


         id.ui_table_profile_full: BEGIN
           ; Only take action if the value has changed.
           IF event.type GT 2 THEN RETURN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           value = DOUBLE(value)
           self.model->SET_PARAM, {profile_width:{full:value}}
         END
         id.ui_bgroup_profile_full: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {profile_width:{full:REPLICATE(fix[0], 9)}}
         END


         id.ui_table_profile_half: BEGIN
           ; Only take action if the value has changed.
           IF event.type GT 2 THEN RETURN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           value = DOUBLE(value)
           self.model->SET_PARAM, {profile_width:{half:value}}
         END
         id.ui_bgroup_profile_half: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {profile_width:{half:REPLICATE(fix[0], 9)}}
         END


         id.ui_table_profile_third: BEGIN
           ; Only take action if the value has changed.
           IF event.type GT 2 THEN RETURN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           value = DOUBLE(value)
           self.model->SET_PARAM, {profile_width:{third:value}}
         END
         id.ui_bgroup_profile_third: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           self.model->SET_FIXED, {profile_width:{third:REPLICATE(fix[0], 9)}}
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_BSTARK::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK::UPDATE
       
       
       id = self->GET_ID_STRUCT()

       self->SET_USE, self.model->USE()

       options = self.model->GET_OPTIONS()
       param = self.model->GET_PARAM()
       fixed = self.model->GET_FIXED()

       bg_group_value = [self.model->GET_FIXALL() $
                         ,self.model->SAVE() $
                         ,options.no_field $
                         ,options.fix_central_line_width $
                         ,options.include_water_line $
                         ,self.model_continuum->USE() $
                         ,options.pop_levels_from_full $
                         ,options.pop_levels_symmetric $
                         ,options.pop_levels_calculated $
                        ]

       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=bg_group_value

       WIDGET_CONTROL, id.ui_field_ratio, SET_VALUE=param.ratio
       WIDGET_CONTROL, id.ui_bgroup_ratio, SET_VALUE=[fixed.ratio]

       WIDGET_CONTROL, id.ui_field_vbeam, SET_VALUE=param.vbeam
       WIDGET_CONTROL, id.ui_bgroup_vbeam, SET_VALUE=[fixed.vbeam]

       WIDGET_CONTROL, id.ui_field_er_eff, SET_VALUE=param.er_eff
       WIDGET_CONTROL, id.ui_bgroup_er_eff, SET_VALUE=[fixed.er_eff]

       WIDGET_CONTROL, id.ui_field_temperature, SET_VALUE=param.temperature
       WIDGET_CONTROL, id.ui_bgroup_temperature, SET_VALUE=[fixed.temperature]

       WIDGET_CONTROL, id.ui_field_lambda0, SET_VALUE=param.lambda0
       WIDGET_CONTROL, id.ui_bgroup_lambda0, SET_VALUE=[fixed.lambda0]

       WIDGET_CONTROL, id.ui_field_water, SET_VALUE=param.level_population.water[3]
       WIDGET_CONTROL, id.ui_bgroup_water, SET_VALUE=[fixed.level_population.water[3]]

       WIDGET_CONTROL, id.ui_table_level_full, SET_VALUE=param.level_population.full
       WIDGET_CONTROL, id.ui_table_level_half, SET_VALUE=param.level_population.half
       WIDGET_CONTROL, id.ui_table_level_third, SET_VALUE=param.level_population.third




       WIDGET_CONTROL, id.ui_bgroup_level_full, SET_VALUE=[fixed.level_population.full[0]]

       WIDGET_CONTROL, id.ui_bgroup_level_half, SET_VALUE=[fixed.level_population.half[0]]

       WIDGET_CONTROL, id.ui_bgroup_level_third, SET_VALUE=[fixed.level_population.third[0]]

       

       WIDGET_CONTROL, id.ui_table_profile_full, SET_VALUE=param.profile_width.full
       WIDGET_CONTROL, id.ui_bgroup_profile_full, SET_VALUE=[fixed.profile_width.full[0]]

       WIDGET_CONTROL, id.ui_table_profile_half, SET_VALUE=param.profile_width.half
       WIDGET_CONTROL, id.ui_bgroup_profile_half, SET_VALUE=[fixed.profile_width.half[0]]

       WIDGET_CONTROL, id.ui_table_profile_third, SET_VALUE=param.profile_width.third
       WIDGET_CONTROL, id.ui_bgroup_profile_third, SET_VALUE=[fixed.profile_width.third[0]]


     END ; PRO BST_INST_WIDGET_BSTARK::UPDATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK::SET_USE, use, LEAVE_BUTTON=leave_button
       
       id = self->GET_ID_STRUCT()


       ; This keyword is useful when handing button events.
       IF ~ KEYWORD_SET(leave_button) THEN BEGIN
         WIDGET_CONTROL, id.ui_bgroup_use, SET_VALUE = [use]
       ENDIF

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base_sub, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_BSTARK::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BSTARK__DEFINE

       struct = { BST_INST_WIDGET_BSTARK $
                           
                  ,model:OBJ_NEW() $
                  ,model_continuum:OBJ_NEW() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_BSTARK__DEFINE
