


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the SCALEDIAN models.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::SET_FLAGS

       self->BST_INST_GUIADDON_GUI::SET_FLAGS

       self.active = 1

       self.use_tab = 1
       self.use_control = 0

       self.title = 'B-Stark'

       self.add_to_menu = 1
       self.menu_title = 'MODEL: B-Stark'

       self.required = 'BST_INST_GUIADDON_CERVIEW'

     END ; PRO BST_INST_GUIADDON_BSTARK::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Take any actions when instantiated.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_BSTARK::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON_GUI::INIT(dispatcher)


       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_BSTARK::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ; 
     ; DESCRIPTION:
     ;   This is called after all addons have been instantiated.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::INITIALIZE
       self->BST_INST_GUIADDON::INITIALIZE

       fitter_core = (self.dispatcher)->GET_CORE()
       self.model = fitter_core.models->GET_MODEL('BST_INST_MODEL_BSTARK')

     END ;PRO BST_INST_GUIADDON_BSTARK::INITIALIZE



     ;+=================================================================
     ; PURPOSE:
     ;   Perform actions before the fit is started.
     ;
     ;   This routines loads the shot and chord information into
     ;   the [BST_INST_MODEL_BSTARK::] model.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::BEFORE_FIT

       ; Get a reference to the cerview object.
       IF self.model->USE() THEN BEGIN
         cerview_obj = self.dispatcher->GET_ADDON('BST_INST_GUIADDON_CERVIEW')
         shot_param = cerview_obj->GET_PARAM()
         self.model->SET_SHOT_PARAM, shot_param
       ENDIF
       
     END ; PRO BST_INST_GUIADDON_BSTARK::BEFORE_FIT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_BSTARK::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()


       
       ; ---------------------------------------------------------------
       ; Add a widget to control the [BST_INST_MODEL_BSTARK::] model.
       self->REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_BSTARK' $
                             ,self.model $
                             ,self.dispatcher->GET_MODEL('BST_INST_MODEL_DALPHA_CONTINUUM') $
                             ,DESTINATION=ui_base)



       ; ---------------------------------------------------------------
       ; Set up menu for this addon.
       fitter_gui = self.dispatcher->GET_GUI()
       ui_menu_bar =  fitter_gui->GET_GUI_ID(/MENU_BAR)

       ui_base_menu_bstark = WIDGET_BUTTON(ui_menu_bar $
                                           ,VALUE='B-Stark' $
                                           ,/MENU $
                                           ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                           ,UVALUE=self $
                                          )

         ui_button_load_profiles = WIDGET_BUTTON(ui_base_menu_bstark $
                                                 ,UNAME='ui_button_load_profiles' $
                                                 ,VALUE='Load Profiles' $
                                                )

       ; Store all of the accesable widgit id's here.
       id_struct = { $
                     ui_base_menu_bstark:ui_base_menu_bstark $
                     ,ui_button_load_profiles:ui_button_load_profiles $
                   }

       ; Save the id structure.
       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_GUIADDON_BSTARK::GUI_DEFINE


     ;+=================================================================
     ;
     ; Here we have the event manager for this addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->BST_INST_GUIADDON_GUI::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF


         id.ui_button_load_profiles: BEGIN
           self->LOAD_PROFILES
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_BSTARK::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Take any action when the widget is destroyed.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::CLEANUP_HANDLER
       PRINT, 'BST_INST_GUIADDON_BSTARK::CLEANUP_HANDLER'

       id = self->GET_ID_STRUCT()

       WIDGET_CONTROL, id.ui_base_menu_bstark, /DESTROY

       self->BST_INST_GUIADDON_GUI::CLEANUP_HANDLER

     END ; PRO BST_INST_GUIADDON_BSTARK::CLEANUP_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::UPDATE_GUI

       self.widget->UPDATE

     END ; PRO BST_INST_GUIADDON_BSTARK::UPDATE_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Load the effective beam profiles from a calibration file.
     ;
     ; DESCRIPTION:
     ;   This takes the shot chord and beam parameters from 
     ;   the 'CERVIEW' addon and loads them into the 'PROFILE_MULTI'
     ;   model.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::LOAD_PROFILES

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Loading of profiles failed', /ERROR, /LOG
         RETURN
       ENDIF 

       ; Clear everything in the profiles model.
       fitter_core = (self.dispatcher)->GET_CORE()
       profile_model = fitter_core.models->GET_MODEL('BST_INST_MODEL_PROFILE_MULTI')
       profile_model->RESET


       ; Get a reference to the cerview object.
       cerview_obj = self.dispatcher->GET_ADDON('BST_INST_GUIADDON_CERVIEW')
       shot_param = cerview_obj->GET_PARAM()

       ; Get the profiles from the cerdata calibration file.
       profiles = BST_PARAM_PROFILES(shot_param.shot, shot_param.chord, shot_param.beam)


       component = ['FULL', 'HALF', 'THIRD', 'WATER', 'CONTINUUM']
       profile_number = [-1, -2, -3, -4, -5]

       gauss_index = 0
       FOR ii=0,N_ELEMENTS(component)-1 DO BEGIN
         IF HAS_TAG(profiles, component[ii], INDEX=comp_index) THEN BEGIN

           profile_comp = profiles.(comp_index)
           FOR jj=0,N_ELEMENTS(profile_comp.amplitude)-1 DO BEGIN
             gauss = {amplitude:profile_comp.amplitude[jj] $
                      ,width:profile_comp.width[jj] $
                      ,location:profile_comp.location[jj] $
                     }

             profile_gauss = profile_model->GET_MODEL(gauss_index)
             profile_gauss->SET_PARAM, gauss
             profile_gauss->SET_PROFILE_NUMBER, profile_number[ii]
             profile_gauss->SET_USE, 1
             profile_gauss->SET_FIXALL, 1

             gauss_index += 1
           ENDFOR
         ENDIF
       ENDFOR


       profile_addon = self.dispatcher->GET_ADDON('BST_INST_GUIADDON_PROFILE')
       profile_addon->UPDATE
       
       
     END ; PRO BST_INST_GUIADDON_BSTARK::LOAD_PROFILES

    






; ======================================================================
; ======================================================================
; ######################################################################
;
; BEGIN METHODS TO BE CALLED BY THE FITTER OR OTHER ADDONS
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Return a plotlist to be added to the spectrum.
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_BSTARK::GET_SPECTRUM_PLOTLIST, x_values

       plot_components = 0
       plot_lines = 1
       plot_profiles = 1
       plot_gaussians = 0

       cutoff = 1e-2

       plotlist = OBJ_NEW('MIR_PLOTLIST')

       IF ~ self.model->USE() THEN BEGIN
         RETURN, plotlist
       ENDIF

       ; First lets just plot the total full, half and third emission.
       IF plot_components THEN BEGIN

         FOR ii=0,2 DO BEGIN
           CASE ii OF
             0: color= MIR_COLOR('BST_FULL')
             1: color= MIR_COLOR('BST_HALF')
             2: color= MIR_COLOR('BST_THIRD')
           ENDCASE

           spectrum = self.model->EVALUATE(x_values, COMPONENT=ii)
           where_nonzero = WHERE(spectrum NE 0, count_nonzero)
           IF count_nonzero GT 0 THEN BEGIN
             plotlist->APPEND, {x:x_values[where_nonzero] $
                                ,y:spectrum[where_nonzero] $
                                ,color:color $
                                ,oplot:1 $
                                ,plotnum:0 $
                                ,psym:0 $
                               }
           ENDIF
         ENDFOR
       ENDIF


       ; Plot all the gaussians.
       IF plot_profiles THEN BEGIN

         FOR ii=0,2 DO BEGIN
           CASE ii OF
             0: color= MIR_COLOR('BST_FULL')
             1: color= MIR_COLOR('BST_HALF')
             2: color= MIR_COLOR('BST_THIRD')
           ENDCASE

           scaled_list = self.model->GET_SCALED_LIST(COMPONENT=ii)

           ; Add the profiles
           FOR ii_scaled=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
             scaled = scaled_list->GET(ii_scaled)
             
             spectrum = scaled->EVALUATE(x_values, /CUTOFF)

             where_nonzero = WHERE(ABS(spectrum) GE cutoff, count_nonzero)
             IF count_nonzero GT 0 THEN BEGIN
               plotlist->APPEND, {x:x_values[where_nonzero] $
                                  ,y:spectrum[where_nonzero] $
                                  ,color:color $
                                  ,oplot:1 $
                                  ,plotnum:0 $
                                  ,psym:0 $
                               }
             ENDIF

           ENDFOR


           ; Destroy the sclaled list.
           FOR ii_scaled=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
             scaled = scaled_list->GET(ii_scaled)

             scaled->DESTROY
           ENDFOR

           scaled_list->DESTROY

         ENDFOR
       ENDIF

       ; Plot all the profiles
       IF plot_gaussians THEN BEGIN

         FOR ii=0,2 DO BEGIN
           CASE ii OF
             0: color= MIR_COLOR('BST_FULL')
             1: color= MIR_COLOR('BST_HALF')
             2: color= MIR_COLOR('BST_THIRD')
           ENDCASE

           scaled_list = self.model->GET_SCALED_LIST(COMPONENT=ii)

           ; Add the gaussains
           FOR ii_scaled=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
             scaled = scaled_list->GET(ii_scaled)

             sum_of_gauss = scaled->GET_SUM_OF_GAUSS()

             FOR ii_gauss = 0, sum_of_gauss->N_ELEMENTS()-1 DO BEGIN
             
               gauss = sum_of_gauss->GET(ii_gauss)

               spectrum = gauss->EVALUATE(x_values, /CUTOFF)

               where_nonzero = WHERE(spectrum NE 0, count_nonzero)
               IF count_nonzero GT 0 THEN BEGIN
                 plotlist->APPEND, {x:x_values[where_nonzero] $
                                    ,y:spectrum[where_nonzero] $
                                    ,color:color $
                                    ,oplot:1 $
                                    ,plotnum:0 $
                                    ,psym:0 $
                                   }
                 
               ENDIF

             ENDFOR

             sum_of_gauss->DESTROY

           ENDFOR


           ; Destroy the sclaled list.
           FOR ii_scaled=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
             scaled = scaled_list->GET(ii_scaled)

             scaled->DESTROY
           ENDFOR

           scaled_list->DESTROY

         ENDFOR
       ENDIF


       ; This method should be reimplemented by addons when needed.
       RETURN, plotlist

     END ; FUNCTION BST_INST_GUIADDON_BSTARK::GET_SPECTRUM_PLOTLIST



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget->DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_BSTARK::REMOVE_WIDGET


     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK::CLEANUP

       IF OBJ_VALID(self.widget) THEN BEGIN
         self->REMOVE_WIDGET
       ENDIF

       self->BST_INST_GUIADDON_GUI::CLEANUP

     END ; PRO BST_INST_GUIADDON_BSTARK::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BSTARK__DEFINE

       struct = { BST_INST_GUIADDON_BSTARK $
                           
                  ,model:OBJ_NEW() $

                  ,widget:OBJ_NEW() $

                  ,INHERITS BST_INST_GUIADDON_GUI }

     END ; PRO BST_INST_GUIADDON_BSTARK__DEFINE
