



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This object handels the xcrystal profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit He like argon spectra from the XCIS 
;   system on C-Mod.
;
; DESCRIPTION:
;
; PARAMETERS:
;   ti
;   te
;       (keV)
;       Ion and electron temperature.
;
;   
;   shift_constant
;   shift_linear
;   shift_quadratic
;       (Angstroms)
;       Parameters to calculate the shift of the lines.
;       For the XICS spectrum it is not it is not possible to directly calculate
;       the line shifts due to line integrated nature of the measurement. Instead
;       I just use a quadratic form to approximate the shift of each line and 
;       then analyze the results later
;
;
;   la1_intensity:0D $
;   la2_intensity:0D $
;       (photons/pixel/frame)
;       Intensities of various independently fit lines.
;
;       The units are photons/pixel/frame where pixel is in the spatial direction.
;       By multiplying by pixels/mm one would have a 1D intensity measurement on
;       the detector.
;
;   s2_factor
;        (photons/pixel/frame / Qd)
;        These do not represent a true intensity but rather a factor that
;        when multiplied by Qd for an individual line will provide an
;        approximation of the correct line intensity.
;
;        To get an approximation of the total intensity of the satellite
;        group this factor can be multiplied by the sum of all the lines
;        in the group.
;
;
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_FLAGS

       self.BST_INST_MODEL_XCRYSTAL::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_FLAGS



     ;+=================================================================
     ; PURPOSE
     ;   Setup the model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::PREFIT_CONSTRAINTS


       model_ar16 = self.dispatcher.GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR16')
       model_ar16.SET_NEED_EVALUATE, 0


       ; Setup for whether or not to use an atomic model to calculate
       ; the intensity of the satelight lines based on the the
       ; electron temperature.
       IF self.options.use_te THEN BEGIN

         self.fixed.s2_factor = 1
         self.fixed.s3_factor = 1
         self.fixed.s4_factor = 1
         self.fixed.s5_factor = 1
         self.fixed.s6_factor = 1

       ENDIF ELSE BEGIN

         self.fixed.te = 1

       ENDELSE


     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::PREFIT_CONSTRAINTS



     ;+=========================================================================
     ; PURPOSE
     ;   Evaluate any contstraints after the fit loop is complete.
     ;
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::POSTFIT

       IF self.options.use_te THEN BEGIN

         ; Nothing for now.  
         ; The idea would be to maybe estimate parameters for the satellight
         ; factors

         ;   self.param.s2_factor = 1
         ;   self.param.s3_factor = 1
         ;   self.param.s4_factor = 1
         ;   self.param.s5_factor = 1
         ;   self.param.s6_factor = 1

       ENDIF

     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::POSTFIT


     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the Ar16+ xcrystal model.
     ;
     ; DESCRIPTION:
     ;
     ;   In addition to the Lyman-alpha spectrum from the Ar17+ crystal on the
     ;   Ar17+ detector, there is also some cross talk from the K-alpha
     ;   spectrum from the Ar16+ crystal on te Ar17+ detector. To deal with
     ;   this I need to handle two separate spectral models with different
     ;   wavelength scales.  This evalutaion routine will handle this special
     ;   case.
     ;
     ;
     ;   A note about units:
     ;
     ;   The spectrum that is given to the fitter is in photons/pixel^2/frame.
     ;   The width (sigma) of the lines will be calculated in angstroms, and
     ;   the intensity in photons/pixel/frame, where pixel refers to the
     ;   dimension in the spatial direction.
     ;    
     ;   With these choices the model will return y in
     ;   photons/angstrom/pixel/frame.  I then convert this result into
     ;   photons/pixel^2/frame to match the spectrum.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::EVALUATE, x



       ; -----------------------------------------------------------------------
       ; First we handle the Ar17+ spectrum normally.
       y_ar17 = self.EVALUATE_AR17(x)


       ; -----------------------------------------------------------------------
       ; Add the spectrum from the Ar16+ crystal on the ar17+ detector.
       y_ar16 = self.EVALUATE_AR16(x)



       ; Finally add the two spectra.
       y = y_ar16 + y_ar17



       RETURN, y

     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::EVALUATE


     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the Ar17+ xcrystal model.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::EVALUATE_AR17, x



       ; -----------------------------------------------------------------------
       ; First we handle the Ar17+ spectrum normally.
       ;
       ; I could just call the superclass method here, but I it is more clear
       ; to just copy things in this case.

       y_ar17 = DBLARR(N_ELEMENTS(x))

       ; Convert the x dimension into angstroms
       x_angstroms = self.CHANNELS_TO_ANGSTROMS(x)

       ; Get the line list, which is a list of <MIR_VOIGT::> objects.
       linelist = self.GET_LINE_LIST()

       num_lines = linelist.N_ELEMENTS()

       FOR ii=0,num_lines-1 DO BEGIN
         
         line = linelist.GET(ii)

         y_ar17 += line.EVALUATE(x_angstroms, CUTOFF=self.options.evaluation_cutoff)
       ENDFOR
  

       ; This model returns y in units of counts/angstrom.
       ; I want to convert this to counts/channel.
       angstroms_per_channel = ABS(self.ANGSTROMS_PER_CHANNEL(x))
       y_ar17 *= angstroms_per_channel


       ; Destroy the line list and all objects.
       linelist.DESTROY, /HEAP_FREE


       RETURN, y_ar17

     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::EVALUATE


     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the Ar16+ xcrystal model.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::EVALUATE_AR16, x

       y_ar16 = DBLARR(N_ELEMENTS(x))

       ; Convert the x dimension into angstroms
       x_angstroms = self.CHANNELS_TO_ANGSTROMS_AR16(x)

       ; Get the line list, which is a list of <MIR_VOIGT::> objects.
       linelist = self.GET_LINE_LIST_AR16()

       num_lines = linelist.N_ELEMENTS()

       FOR ii=0,num_lines-1 DO BEGIN
         
         line = linelist.GET(ii)

         y_ar16 += line.EVALUATE(x_angstroms, CUTOFF=self.options.evaluation_cutoff)
       ENDFOR
       
       ; This model returns y in units of counts/angstrom.
       ; I want to convert this to counts/channel.
       angstroms_per_channel = ABS(self.ANGSTROMS_PER_CHANNEL_AR16(x))
       y_ar16 *= angstroms_per_channel


       ; Destroy the line list and all objects.
       linelist.DESTROY, /HEAP_FREE



       RETURN, y_ar16

     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::EVALUATE_AR16


     ;+=========================================================================
     ; PURPOSE:
     ;   Get a list of the lines from the Ar16+ model.
     ;
     ; DESCRIPTION:
     ;   Normally we would just let each model do it's own evalutation, but
     ;   because I need to handle two different wavelength scales, I will
     ;   do special handling of the actual model evalution.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_LIST_AR16

       model_ar16 = self.dispatcher.GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR16')

       RETURN, model_ar16.GET_LINE_LIST()

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_LIST_AR16



     ;+=========================================================================
     ; PURPOSE:
     ;   Get a list of the lines for the XCrystal model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_LIST

       linelist = OBJ_NEW('MIR_LIST_IDL7')

       const = PHYSICAL_CONSTANTS()

       ; Atomic weight of argon in amu.
       ar_amu =  39.948D
       ;fe_amu =  55.845D


       ; Get the lines from the theory file.
       line_data = self.GET_THEORY_DATA()
       theory_array = line_data.data


       ; Find the L-alpha1 line.
       la1_line_index = (WHERE(theory_array.label EQ 'La1'))[0]
       la1_wavelength = theory_array[la1_line_index].wavelength


       ; Loop through the lines and add them to the spectrum.
       num_lines = N_ELEMENTS(theory_array)

       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = theory_array[ii_line]


         ; Temporary ugliness untill I add the Element to the atomic data files.
         atomic_mass = ar_amu
         ;IF theory_struct.label EQ 'Fe_w' $
         ;   OR theory_struct.label EQ 'Fe_x' $
         ;   OR theory_struct.label EQ 'Fe_y' $
         ;   OR theory_struct.label EQ 'Fe_z' $
         ;   OR theory_struct.label EQ 'Fe_s2' $
         ;   THEN BEGIN
         ;  atomic_mass = fe_amu
         ;ENDIF


         
         ; Natural line width.
         gamma = theory_struct.line_width * theory_struct.wavelength^2  $
                 / ( 4D * !dpi * const.c * 1D10 )

         ; Doppler broadened line width.
         sigma = SQRT(self.param.ti / atomic_mass / const.amu_kg / const.c^2 * const.ev_J * 1e3) $
                 * theory_struct.wavelength

         ; Line location
         location = theory_struct.wavelength $
                    + self.param.shift_constant $
                    + self.param.shift_linear * (theory_struct.wavelength - la1_wavelength) $
                    + self.param.shift_quadratic * (theory_struct.wavelength - la1_wavelength)^2



         IF self.options.use_te THEN BEGIN
           intensity = self.GET_LINE_INTENSITY_FROM_TE(theory_struct)
         ENDIF ELSE BEGIN
           intensity = self.GET_LINE_INTENSITY(theory_struct)
         ENDELSE

         voigt = OBJ_NEW('MIR_VOIGT' $
                         ,{intensity:intensity $
                           ,location:location $
                           ,sigma:sigma $
                           ,gamma:gamma $
                          } $
                        )

         linelist.APPEND, voigt

       ENDFOR
       

       RETURN, linelist


     END ; BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY, theory_struct


       IF theory_struct.type EQ 'DIELECTRONIC' THEN BEGIN
         ; For the satellite lines, the dielectronic excitation rate for all the lines 
         ; with a given n number has approximately the same Te dependence.
         ; 
         ; We use to our advantage in being able to group the various n numbers with
         ; a single intensity parameter.
         ;
         ; Here the QD factor gives us the relative line amplitudes within an n group.
         intensity_factor =  theory_struct.qd/1D13
       ENDIF ELSE BEGIN
         intensity_factor = 1D
       ENDELSE

       
       ; The way I am handling the Fe 24+ lines is temporary.
       ; I need to add another column to the atomic data file that specifes the 
       ; species.  Then I have separate sections here to deal with Ar and Fe.
       CASE theory_struct.n OF
         
         0: BEGIN
           CASE theory_struct.label OF
             'La1': intensity = self.param.la1_intensity
             'La2': intensity = self.param.la2_intensity

             ;'Fe_w': intensity = self.param.fe_w_intensity
             ;'Fe_x': intensity = self.param.fe_x_intensity
             ;'Fe_y': intensity = self.param.fe_y_intensity
             ;'Fe_z': intensity = self.param.fe_z_intensity

             ELSE:  BEGIN
               MESSAGE, "Unknown line in theory file: "+theory_struct.label, /CONTINUE
               invalid_line = 1
             ENDELSE
           ENDCASE
         END

         2: intensity = self.param.s2_factor
         3: intensity = self.param.s3_factor
         4: intensity = self.param.s4_factor
         5: intensity = self.param.s5_factor
         6: intensity = self.param.s6_factor

         

         ;IF theory_struct.label EQ 'Fe_s2' THEN BEGIN
         ;  intensity = self.param.fe_s2_factor
         ;ENDIF

       ENDCASE
       
       intensity = intensity * intensity_factor
       
       
       RETURN, intensity
         
     END ; BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY



     ;+=========================================================================
     ; PURPOSE:
     ;   Using an atomic model calculate the intenisty of the satelight and li
     ;   lines based on the intensity of the resonant line and the electron 
     ;   temperature.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY_FROM_TE, theory_struct
       COMPILE_OPT STRICTARR

       const = PHYSICAL_CONSTANTS()

       ; Convert to eV
       te = self.param.te * 1e3

       ex_rate_la1 = XC_GET_EXCITATION_RATE(te, 'La1', SOURCE=self.options.excitation_source)

       ; To find the line amplitude, the lines need to be treated differently 
       ; depending on whether they are collisionaly excited or dielectronic recombination lines.
       CASE theory_struct.type OF

         'DIRECT': BEGIN
           CASE theory_struct.label OF
             'La1': line_intensity = self.param.la1_intensity
             'La2': BEGIN
               ex_rate = XC_GET_EXCITATION_RATE(te, 'La1', SOURCE=self.options.excitation_source)
               line_intensity = self.param.x_intensity * self.param.la1_intensity * ex_rate / ex_rate_w
             END
           ENDCASE
         END

         'DIELECTRONIC': BEGIN

           ; The dielectronic recombination rate will be approximately the same for all 
           ; of the satellites from a particular n.
           Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
               *  EXP( -1D * theory_struct.es / Te )
           
           line_intensity = self.param.la1_intensity * Z * theory_struct.qd / ex_rate_la1
           
         END ; 'SATELLITE'
         
         ELSE: BEGIN
           MESSAGE, 'Unknown line type: '+theory_struct.type
         ENDELSE

       ENDCASE

       RETURN, line_intensity

     END ; BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY_FROM_TE



     ;+=========================================================================
     ; PURPOSE
     ;   Initialize the model parameters.
     ;
     ; DESCRIPTION:
     ;   Attempt to come up with a reasonable initial guess 
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_MODEL

       MIR_DEFAULT, ti, 1.0
       MIR_DEFAULT, te, 1.0

       self.param.ti = ti
       self.param.te = te
       
       IF self.options.initialize_shifts THEN BEGIN
         self.param.shift_constant = 0.0
         self.param.shift_linear = 0.0
         self.param.shift_quadratic = 0.0
       ENDIF


       self.param.la1_intensity = 1.0
       self.INITIALIZE_INTENSITY, te


     END ; PRO PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_MODEL



     ;+=========================================================================
     ; PURPOSE
     ;   Make initial guesses for the intensities.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY, te

       self.INITIALIZE_INTENSITY_FROM_TE, te
       self.INITIALIZE_INTENSITY_TOTAL

       self.INITIALIZE_INTENSITY_AR16

     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY, te



     ;+=========================================================================
     ; PURPOSE
     ;   Make initial guesses for the intensity ratios based on an
     ;   initial electron temperature.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_FROM_TE, te

       MIR_DEFAULT, te, 1.0
       ; Convert to eV.
       te *= 1e3

       const = PHYSICAL_CONSTANTS()

       ; Get the lines from the theory file.
       line_data = self.GET_THEORY_DATA()
       theory_array = line_data.data


       ; TEMPORARY:  For now I don't have excitation rates for ar17.
       self.param.la2_intensity = 0.5 * self.param.la1_intensity
       self.param.s2_factor = 0.01 * self.param.la1_intensity
       self.param.s3_factor = 0.01 * self.param.la1_intensity
       self.param.s4_factor = 0.01 * self.param.la1_intensity
       self.param.s5_factor = 0.01 * self.param.la1_intensity
       self.param.s6_factor = 0.01 * self.param.la1_intensity

       ;self.param.fe_w_intensity = 0.1 * self.param.la1_intensity
       ;self.param.fe_x_intensity = 0.1 * self.param.la1_intensity
       ;self.param.fe_y_intensity = 0.1 * self.param.la1_intensity
       ;self.param.fe_z_intensity = 0.1 * self.param.la1_intensity
       ;self.param.fe_s2_factor = 0.1 * self.param.la1_intensity
       RETURN


       ex_rate_la1 = XC_GET_EXCITATION_RATE(te, 'La1', SOURCE=self.options.excitation_source)


       ; This is a really ugly way to do this, I'll hopefully clean this up someday.
       ; I especially don't like haveing these atomic physics equations duplicated here
       ; and in GET_LINE_INTENSITY_FROM_TE.  On the otherhand, I want 
       ; GET_LINE_INTENSITY_FROM_TE to be as readable as possible, and this initialization 
       ; routine is not as important.

       ; la2
       ex_rate = XC_GET_EXCITATION_RATE(te, 'La2', SOURCE=self.options.excitation_source)
       self.param.la2_intensity = self.param.la1_intensity * ex_rate / ex_rate_la1



       ;s2
       line_index = (WHERE(theory_array.label EQ 'S2'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s2_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13
       ;s3
       line_index = (WHERE(theory_array.label EQ 'S3'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s3_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13
       ;s4
       line_index = (WHERE(theory_array.label EQ 'S4'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s4_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13
       ;s5
       line_index = (WHERE(theory_array.label EQ 'S5'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s5_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13

       ;s6
       ; There are not enough lines in the spectrum for this.
       self.param.s6_factor = 0D

     END ; PRO PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_FROM_TE



     ;+=========================================================================
     ; PURPOSE
     ;   Make a first guess for the intensity of the lines.
     ;
     ; DESCRIPTION:
     ;   For the time being I will just use a crude method of achieveing
     ;   this.  Eventually I might want to use a linear fitting method
     ;   for this instead.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_TOTAL

       core = (self.dispatcher).core
       spectrum = (core.spectrum).GET_SPECTRUM()

       ; Here I am evaluating the model ONLY.
       ; This is equivilent of assuming that the background is zero.
       y_model = self.EVALUATE_AR17(spectrum.x)

       
       ; Just scale all the line intensities so that the total
       ; intensity matches the total intensity in the spectrum.
       ;
       ; This will only work if the relative intensities are already
       ; at appoximately the correct ratios.
       scale = TOTAL(spectrum.y)/TOTAL(y_model)

       ; Since we know that there is some contribution from the Ar16+ spectrum
       ; take only 3/4 of the total intensity.
       scale *= 0.75

       self.param.la1_intensity *= scale
       self.param.la2_intensity *= scale

       self.param.s2_factor *= scale
       self.param.s3_factor *= scale
       self.param.s4_factor *= scale
       self.param.s5_factor *= scale
       self.param.s6_factor *= scale
       
       ;self.param.fe_w_intensity *= scale
       ;self.param.fe_x_intensity *= scale
       ;self.param.fe_y_intensity *= scale
       ;self.param.fe_z_intensity *= scale
       ;self.param.fe_s2_factor *= scale
       
     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_TOTAL


     ;+=========================================================================
     ; PURPOSE
     ;   Make a first guess for the intensity of the Ar16+ lines.
     ;
     ; DESCRIPTION:
     ;   This needs to be done separately from the Ar16 model to get
     ;   the wavelengths correct.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_AR16

       model_ar16 = self.dispatcher.GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR16')
       model_ar16.SET_PARAM, {scale_factor:1D}

       core = (self.dispatcher).core
       spectrum = (core.spectrum).GET_SPECTRUM()

       ; Here I am evaluating the Ar17 ONLY.
       y_ar17 = self.EVALUATE_AR17(spectrum.x)

       ; Here I am evaluating the Ar16 ONLY.
       y_ar16 = self.EVALUATE_AR16(spectrum.x)

       scale = TOTAL(y_ar17)/TOTAL(y_ar16)*0.25

       model_ar16.SET_PARAM, {scale_factor:scale}


     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_AR16




     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from channels to angstroms.  This is done using the geometry
     ;   and known crystal and detector parameters.
     ;
     ;   In this case we want the wavelengh scale for the Ar16+ crystal on
     ;   the Ar17+ detector.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::CHANNELS_TO_ANGSTROMS_AR16, channel_in
       COMPILE_OPT STRICTARR


       g_ar17 = self.GET_GEOMETRY_OBJECT()
       config_ar17 = g_ar17.GET_CONFIG()

       ; I will eventually want to cache this the same way I do for the
       ; Ar17+ geometry object.
       g_ar16 = XC_GET_GEOMETRY_OBJECT('lhd_ar16', self.spectrum_param.shot)
       config_ar16 = g_ar16.GET_CONFIG()


       ; Create a new configuration that uses the Ar17+ detector location
       ; and the Ar16+ crystal location
       config_new = config_ar17
       config_new.crystal_name = config_ar16.crystal_name
       config_new.crystal_spacing = config_ar16.crystal_spacing
       config_new.crystal_curvature = config_ar16.crystal_curvature
       config_new.crystal_location = config_ar16.crystal_location
       config_new.crystal_normal = config_ar16.crystal_normal
       config_new.crystal_orientation = config_ar16.crystal_orientation


       ; Set the new configuration.
       g_ar17.SET_CONFIG, config_new


       ; I need to work on making this efficent (probably).
       num_channels = N_ELEMENTS(channel_in)
       wavelength = DBLARR(num_channels, /NOZERO)
       FOR ii=0,num_channels-1 DO BEGIN
         channel = [channel_in[ii] $
                  ,(self.spectrum_param.y_max + self.spectrum_param.y_min)/2D]

         wavelength[ii] =  g_ar17.WAVELENGTH_FROM_CHANNEL(channel)
       ENDFOR
         

       ; Reset to the original configuration.
       g_ar17.SET_CONFIG, config_ar17



       RETURN, wavelength

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::CHANNELS_TO_ANGSTROMS_AR16





     ;+=========================================================================
     ; PURPOSE:
     ;   Calculate the number of angstroms per channel.
     ; 
     ; DISCRIPTION:
     ;   This is done discretely for use in converting photons/angstrom to
     ;   photons/channel.  For this type of calculation this is more accurate
     ;   that using the dispersion at the center of the channel.
     ;
     ;   Here I am making the approximation that the dispersion will
     ;   be the same for all of the rows that are summed to make the spectrum;
     ;   this is fine so long as I am not trying to more than a few tens of
     ;   rows.
     ;
     ;   In this case I want the wavelengh scale for the Ar16+ crystal on
     ;   the Ar17+ detector.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::ANGSTROMS_PER_CHANNEL_AR16, channel_in
       COMPILE_OPT STRICTARR
       

       ; I am copying code here.  Bad form, but for now I just want to get 
       ; this working. Novi - 2013-02-26.


       g_ar17 = self.GET_GEOMETRY_OBJECT()
       config_ar17 = g_ar17.GET_CONFIG()

       ; I will eventually want to cache this the same way I do for the
       ; Ar17+ geometry object.
       g_ar16 = XC_GET_GEOMETRY_OBJECT('lhd_ar16', self.spectrum_param.shot)
       config_ar16 = g_ar16.GET_CONFIG()


       ; Create a new configuration that uses the Ar17+ detector location
       ; and the Ar16+ crystal location
       config_new = config_ar17
       config_new.crystal_name = config_ar16.crystal_name
       config_new.crystal_spacing = config_ar16.crystal_spacing
       config_new.crystal_curvature = config_ar16.crystal_curvature
       config_new.crystal_location = config_ar16.crystal_location
       config_new.crystal_normal = config_ar16.crystal_normal
       config_new.crystal_orientation = config_ar16.crystal_orientation


       ; Set the new configuration.
       g_ar17.SET_CONFIG, config_new





       ; I need to work on making this efficent (probably).
       num_channels = N_ELEMENTS(channel_in)
       ang_per_channel = DBLARR(num_channels, /NOZERO)
       FOR ii=0,num_channels-1 DO BEGIN
         y = (self.spectrum_param.y_max + self.spectrum_param.y_min)/2D
         channel_0 = [channel_in[ii]-0.5, y]
         channel_1 = [channel_in[ii]+0.5, y]
         wavelength_0 = g_ar17.WAVELENGTH_FROM_CHANNEL(channel_0)
         wavelength_1 = g_ar17.WAVELENGTH_FROM_CHANNEL(channel_1)

         ang_per_channel[ii] = wavelength_1 - wavelength_0
       ENDFOR
         


       ; Reset to the original configuration.
       g_ar17.SET_CONFIG, config_ar17



       RETURN, ang_per_channel

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::ANGSTROMS_PER_CHANNEL_AR16


     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_DEFAULTS

       self.BST_INST_MODEL_XCRYSTAL::SET_DEFAULTS

       self.options.use_te = 0
       self.options.line_source = 'VAINSHTEIN_AR17_2013'


       self.param = { BST_INST_MODEL_XCRYSTAL_AR17__PARAM }

       self.param.te = 1.0
       self.param.ti = 1.0
       
       self.param.shift_constant = 0D
       self.param.shift_linear = 0D
       self.param.shift_quadratic = 0D
       
       self.param.la1_intensity = 1.0
       self.param.la2_intensity = 1.0

       self.param.s2_factor = 0.5
       self.param.s3_factor = 0.1
       self.param.s4_factor = 0.1
       self.param.s5_factor = 0.1
       self.param.s6_factor = 0.1

       ;self.param.fe_w_intensity = 1.0
       ;self.param.fe_x_intensity = 1.0
       ;self.param.fe_y_intensity = 1.0
       ;self.param.fe_z_intensity = 1.0
       ;self.param.fe_s2_factor = 0.1


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self.BST_INST_MODEL_XCRYSTAL::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN

         self.SET_DEFAULTS
         
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_XCRYSTAL_AR17__FIXED }

         self.fixed.ti = 1
         self.fixed.te = 1

         self.fixed.shift_constant = 1
         self.fixed.shift_linear = 1
         self.fixed.shift_quadratic = 1

         self.fixed.la1_intensity = 1
         self.fixed.la2_intensity = 1

         self.fixed.s2_factor = 1
         self.fixed.s3_factor = 1
         self.fixed.s4_factor = 1
         self.fixed.s5_factor = 1
         self.fixed.s6_factor = 1

         ;self.fixed.fe_w_intensity = 1
         ;self.fixed.fe_x_intensity = 1
         ;self.fixed.fe_y_intensity = 1
         ;self.fixed.fe_z_intensity = 1
         ;self.fixed.fe_s2_factor = 1

       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITED }

         ; Temperature should always be positive.
         self.limited.ti = [1,0]
         self.limited.te = [1,0]

         ; Line amplitudes should always be positive
         self.limited.la1_intensity = [1,0]
         self.limited.la2_intensity = [1,0]

         self.limited.s2_factor = [1,0]
         self.limited.s3_factor = [1,0]
         self.limited.s4_factor = [1,0]
         self.limited.s5_factor = [1,0]
         self.limited.s6_factor = [1,0]

         ;self.limited.fe_w_intensity = [1,0]
         ;self.limited.fe_x_intensity = [1,0]
         ;self.limited.fe_y_intensity = [1,0]
         ;self.limited.fe_z_intensity = [1,0]
       ;  self.limited.fe_s2_factor = [1,0]
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::RESET




     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XCRYSTAL model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_XCRYSTAL_AR17__PARAM $
                 ; Ion and electron temperatures
                 ,ti:0D $
                 ,te:0D $

                 ; These parameters will be used to examine the rotation
                 ,shift_constant:0D $
                 ,shift_linear:0D $
                 ,shift_quadratic:0D $


                 ; Amplitudes of the various lines.
                 ,la1_intensity:0D $
                 ,la2_intensity:0D $

                 ,s2_factor:0D $
                 ,s3_factor:0D $
                 ,s4_factor:0D $
                 ,s5_factor:0D $
                 ,s6_factor:0D $

                 ; These are for the Fe 24+ lines in second order.
                 ;,fe_w_intensity: 0D $
                 ;,fe_x_intensity: 0D $
                 ;,fe_y_intensity: 0D $
                 ;,fe_z_intensity: 0D $

                 ;,fe_s2_factor: 0D $
               } 

       sigma = { BST_INST_MODEL_XCRYSTAL_AR17__SIGMA $
                 ,ti:0D $
                 ,te:0D $
                 ,shift_constant:0D $
                 ,shift_linear:0D $
                 ,shift_quadratic:0D $
                 ,la1_intensity:0D $
                 ,la2_intensity:0D $
                 ,s2_factor:0D $
                 ,s3_factor:0D $
                 ,s4_factor:0D $
                 ,s5_factor:0D $
                 ,s6_factor:0D $
                 ;,fe_w_intensity: 0D $
                 ;,fe_x_intensity: 0D $
                 ;,fe_y_intensity: 0D $
                 ;,fe_z_intensity: 0D $
                 ;,fe_s2_factor: 0D $
               } 


       fixed = { BST_INST_MODEL_XCRYSTAL_AR17__FIXED $
                 ,ti:0 $
                 ,te:0 $
                 ,shift_constant:0 $
                 ,shift_linear:0 $
                 ,shift_quadratic:0 $
                 ,la1_intensity:0 $
                 ,la2_intensity:0 $
                 ,s2_factor:0 $
                 ,s3_factor:0 $
                 ,s4_factor:0 $
                 ,s5_factor:0 $
                 ,s6_factor:0 $
                 ;,fe_w_intensity: 0 $
                 ;,fe_x_intensity: 0 $
                 ;,fe_y_intensity: 0 $
                 ;,fe_z_intensity: 0 $
                 ;,fe_s2_factor: 0 $
               } 
       
       limited = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITED $
                 ,ti:[0, 0] $
                 ,te:[0, 0] $
                 ,shift_constant:[0, 0] $
                 ,shift_linear:[0, 0] $
                 ,shift_quadratic:[0, 0] $
                 ,la1_intensity:[0, 0] $
                 ,la2_intensity:[0, 0] $
                 ,s2_factor:[0, 0] $
                 ,s3_factor:[0, 0] $
                 ,s4_factor:[0, 0] $
                 ,s5_factor:[0, 0] $
                 ,s6_factor:[0, 0] $
                 ;,fe_w_intensity: [0, 0] $
                 ;,fe_x_intensity: [0, 0] $
                 ;,fe_y_intensity: [0, 0] $
                 ;,fe_z_intensity: [0, 0] $
                 ;,fe_s2_factor: [0, 0] $
                 }

       limits = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITS $
                 ,ti:[0D, 0D] $
                 ,te:[0D, 0D] $
                 ,shift_constant:[0D, 0D] $
                 ,shift_linear:[0D, 0D] $
                 ,shift_quadratic:[0D, 0D] $
                 ,la1_intensity:[0D, 0D] $
                 ,la2_intensity:[0D, 0D] $
                 ,s2_factor:[0D, 0D] $
                 ,s3_factor:[0D, 0D] $
                 ,s4_factor:[0D, 0D] $
                 ,s5_factor:[0D, 0D] $
                 ,s6_factor:[0D, 0D] $
                 ;,fe_w_intensity: [0D, 0D] $
                 ;,fe_x_intensity: [0D, 0D] $
                 ;,fe_y_intensity: [0D, 0D] $
                 ;,fe_z_intensity: [0D, 0D] $
                 ;,fe_s2_factor: [0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XCRYSTAL_AR17 $

                 ,param:{ BST_INST_MODEL_XCRYSTAL_AR17__PARAM} $

                 ,fixed:{ BST_INST_MODEL_XCRYSTAL_AR17__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_XCRYSTAL_AR17__LIMITED } $

                 ,limits:{ BST_INST_MODEL_XCRYSTAL_AR17__LIMITS } $

                 ,sigma:{ BST_INST_MODEL_XCRYSTAL_AR17__SIGMA} $

                 ,INHERITS BST_INST_MODEL_XCRYSTAL $
               }


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17
