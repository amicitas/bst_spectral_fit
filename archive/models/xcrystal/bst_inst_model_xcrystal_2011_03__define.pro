



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This object handels the xcrystal profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit He like argon spectra from the XCIS 
;   system on C-Mod.
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 0
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_FLAGS





     ;+=================================================================
     ; PURPOSE
     ;   Setup the model prior to starting the fit loop
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::PREFIT

       self->LOAD_DATA_FILES

     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::PREFIT






     ;+=================================================================
     ; PURPOSE
     ;   Setup the model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::PREFIT_CONSTRAINTS


       ; Setup for whether or not to use an atomic model to calculate
       ; the i
       IF self.options.use_te THEN BEGIN

         self.fixed.q_intensity = 1
         self.fixed.r_intensity = 1
         self.fixed.s_intensity = 1
         self.fixed.t_intensity = 1

         self.fixed.w2_factor = 1
         self.fixed.w3_factor = 1
         self.fixed.w4_factor = 1
         self.fixed.w5_factor = 1
         self.fixed.w6_factor = 1

       ENDIF ELSE BEGIN

         self.fixed.te = 1
         self.fixed.li_intensity = 1

       ENDELSE


     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints before each evaluation of the model.
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::EVALUATE_CONSTRAINTS

       IF self.options.use_te THEN BEGIN
         self->LINE_INTENSITY_FROM_TE
       ENDIF

     END ; PRO BST_INST_MODEL_XCRYSTAL_2011_03::EVALUATE_CONSTRAINTS


     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the xcrystal model.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))

       ; Convert the x dimention into angstroms
       x_angstroms = self->CHANNELS_TO_ANGSTROMS(x)
       ;x_angstroms = self->CHANNELS_TO_ANGSTROMS_XC_SPECTRO(x)

       ; Get the line list
       linelist = self->GET_LINE_LIST()

       num_lines = linelist->N_ELEMENTS()

       FOR ii=0,num_lines-1 DO BEGIN
         
         line = linelist->GET(ii)

         y += line->EVALUATE(x_angstroms)

       ENDFOR
       

       ; Destroy the line list and all objects.
       linelist->DESTROY, /HEAP_FREE


       RETURN, y

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::EVALUATE




     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints after the fit loop is complete.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::POSTFIT

       IF self.options.use_te THEN BEGIN
         self->LINE_INTENSITY_FROM_TE
       ENDIF

     END ; PRO BST_INST_MODEL_XCRYSTAL_2011_03::POSTFIT




     ;+=========================================================================
     ; PURPOSE:
     ;   Get a list of the lines for the XCrystal model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_LINE_LIST

       linelist = OBJ_NEW('MIR_LIST_IDL7')

       const = PHYSICAL_CONSTANTS()

       ; Atomic weight of argon in amu.
       ar_amu =  39.948D


       ; Get the lines from the theory file and the lines file
       ; (These files should maybe be combined.
       theory_array = self->GET_THEORY_DATA()
       line_array = self->GET_LINE_DATA() 


       ; Loop through the lines and add them to the spectrum.
       num_lines = N_ELEMENTS(theory_array)

       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = theory_array[ii_line]

         ; Get the line structure for this line.
         w = WHERE(line_array.name EQ theory_struct.name, count)
         IF count EQ 0 THEN BEGIN
           ; I need some sort of warning somewhere, but this is not a good place for it.
           ;MESSAGE, 'Line: '+theory_struct.name+' not found in line parameter file.', /CONTINUE
           CONTINUE
         ENDIF
         line_struct = line_array[w[0]]
         

         ; Natural line width.
         gamma = theory_struct.line_width * 1D13 * theory_struct.wavelength^2  $
                 / ( 4D * !dpi * const.c * 1D10 )

         ; Doppler broadened line width.
         sigma = SQRT(self.param.ti / ar_amu / const.amu_kg / const.c^2 * const.ev_J * 1e3) $
                 * theory_struct.wavelength

         ; Line location
         location = theory_struct.wavelength $
                    + self.param.shift_constant $
                    + self.param.shift_linear * (theory_struct.wavelength - self.param.central_wavelength)




         ; For the satellite lines the intensity represents the strength of the
         ; dielectric recombination for the given n number.  
         ; Here we add a factor for the individual lines comming from a given n number.
         IF theory_struct.type EQ 'SATELLITE' THEN BEGIN
           intensity_factor =  theory_struct.rel_intensity
         ENDIF ELSE BEGIN
           intensity_factor = 1D
         ENDELSE




         ; Get the intensity for the line.
         invalid_line = 0

         CASE theory_struct.name OF

           'W': intensity = self.param.w_intensity
           'X': intensity = self.param.x_intensity
           'Y': intensity = self.param.y_intensity
           'Z': intensity = self.param.z_intensity

           'Q': intensity = self.param.q_intensity
           'R': intensity = self.param.r_intensity
           'S': intensity = self.param.s_intensity
           'T': intensity = self.param.t_intensity

           'W2': intensity = self.param.w2_factor
           'W3': intensity = self.param.w3_factor
           'W4': intensity = self.param.w4_factor
           'W5': intensity = self.param.w5_factor
           'W6': intensity = self.param.w6_factor

           ELSE:  BEGIN
             MESSAGE, "Unknown line in theory file: "+theory_struct.name, /CONTINUE
             invalid_line = 1
           ENDELSE
         ENDCASE

         ; IDL does not allow CONTINUE within a CASE STATEMENT, so do this the long way.
         IF invalid_line THEN CONTINUE


         intensity = intensity * intensity_factor

         voigt = OBJ_NEW('MIR_VOIGT' $
                         ,{intensity:intensity $
                           ,location:location $
                           ,sigma:sigma $
                           ,gamma:gamma $
                          } $
                        )

         linelist->APPEND, voigt

       ENDFOR
       

       RETURN, linelist


     END ; BST_INST_MODEL_XCRYSTAL_2011_03::GET_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   Using an atomic model calculate the intenisty of the satelight and li
     ;   lines based on the intensity of the resonant line and the electron 
     ;   temperature.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::LINE_INTENSITY_FROM_TE

       linelist = OBJ_NEW('MIR_LIST_IDL7')

       const = PHYSICAL_CONSTANTS()

       ; Get the lines from the theory file and the lines file
       ; (These files should maybe be combined.
       theory_array = self->GET_THEORY_DATA()
       line_array = self->GET_LINE_DATA() 


       ; The calculation of line amplitudes are all done relative to the W line.
       ; So extract the index for the W line. 
       ;   Calculate the excitation rate.
       ;   Calculate the line shift.
       w = WHERE(theory_array.name EQ 'W', count)
       index_line_w = w[0]

       ex_rate_w = theory_array[index_line_w].exc_rate_c1 / SQRT(self.param.te) $
                   * EXP(-1D * theory_array[index_line_w].exc_rate_c2 / self.param.te)


       ; Loop through the lines and add them to the spectrum.
       num_lines = N_ELEMENTS(theory_array)

       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = theory_array[ii_line]

         ; Get the line structure for this line.
         w = WHERE(line_array.name EQ theory_struct.name, count)
         IF count EQ 0 THEN BEGIN
           ; I need some sort of warning somewhere, but this is not a good place for it.
           ;MESSAGE, 'Line: '+theory_struct.name+' not found in line parameter file.', /CONTINUE
           CONTINUE
         ENDIF
         line_struct = line_array[w[0]]
  




         ; To find the line amplitude, the lines need to be treated differently 
         ; depending on whether they are normal emission lines or dielectric recombination lines.
         invalid_line = 0
         CASE theory_struct.type OF


           'LINE': BEGIN

             ; Handle the Li lines.
             IF line_struct.dependence EQ 'LI' THEN BEGIN

               ; Calculate the excitation rate for this line
               ex_rate = theory_struct.exc_rate_c1 / SQRT(self.param.te) $
                         * EXP(-1D * theory_struct.exc_rate_c2 / self.param.te)

               line_intensity = self.param.w_intensity * self.param.li_intensity * ex_rate / ex_rate_w

             ENDIF ELSE BEGIN
               ; Normal line, no need to set the intensity.
               invalid_line = 1
             ENDELSE

           END ; 'LINE'


           'SATELLITE': BEGIN
             
             factor = 5.22271E-27/self.param.te^(1.5)

             ; The dielectronic recombination rate will be the same for all 
             ; of the satellites from a particular n.

             ex_rate = factor * EXP(-1D * theory_struct.diel_recomb_rate / self.param.te)

             line_intensity = self.param.w_intensity * ex_rate / ex_rate_w * 1D13

           END ; 'SATELLITE'

           
           ELSE: BEGIN
             MESSAGE, 'Unknown line type: '+theory_struct[ii_line].type
           ENDELSE

         ENDCASE

         ; IDL does not allow CONTINUE within a CASE STATEMENT, so do this the long way.
         IF invalid_line THEN CONTINUE



         ; Save the intensity of the line.
         invalid_line = 0
         CASE theory_struct.name OF

           'Q': self.param.q_intensity = line_intensity
           'R': self.param.r_intensity = line_intensity
           'S': self.param.s_intensity = line_intensity
           'T': self.param.t_intensity = line_intensity

           'W2': self.param.w2_factor = line_intensity
           'W3': self.param.w3_factor = line_intensity
           'W4': self.param.w4_factor = line_intensity
           'W5': self.param.w5_factor = line_intensity
           'W6': self.param.w6_factor = line_intensity

           ELSE:  BEGIN
             MESSAGE, "Unknown line in theory file: "+theory_struct.name, /CONTINUE
             invalid_line = 1
           ENDELSE
         ENDCASE

         ; IDL does not allow CONTINUE within a CASE STATEMENT, so do this the long way.
         IF invalid_line THEN CONTINUE


       ENDFOR

     END ; BST_INST_MODEL_XCRYSTAL_2011_03::LINE_INTENSITY_FROM_TE



     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from channels to angstroms.  This is done using the geometry
     ;   and known crystal and detector parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::CHANNELS_TO_ANGSTROMS, channel_in
       COMPILE_OPT STRICTARR

       config = XC_SYSTEM_CONFIG(self.shot_param.system $
                                            ,self.shot_param.shot)

       ; Define channel 0 as the center of the detector.
       num_channels_x = FLOOR(FLOAT(config.x_size) / config.x_binsize)
       channel = channel_in - num_channels_x/2D

       ; Convert from channel to m
       x = channel * config.x_mm_per_channel * config.x_binsize * 1e-3

       theta_central = ASIN(self.param.central_wavelength/config.crystal_spacing/2D)

       theta = ATAN( ( self.param.detector_distance * SIN(theta_central)  $
                       + x * COS(self.param.detector_angle) ) $
                     / ( self.param.detector_distance * COS(theta_central) $
                         -  x * SIN(self.param.detector_angle) ) $
                   )


       wavelength = 2D * config.crystal_spacing * SIN(theta)

;PRINT, 'x (channels)  ', channel_in[0]
;PRINT, 'x (mm)        ', x[0]
;PRINT, 'theta_central ', theta_central/!dtor
;PRINT, 'theta         ', theta[0]/!dtor
;PRINT, 'wavelength    ', wavelength[0]
;PRINT, 'num_channels_x ', num_channels_x
;PRINT, 'central_wavelength', self.param.central_wavelength
;HELP, /ST, config

       RETURN, wavelength

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::CHANNELS_TO_ANGSTROMS



     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from angstroms to channels.  This is done using the geometry
     ;   and known crystal and detector parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::ANGSTROMS_TO_CHANNELS, wavelength
       COMPILE_OPT STRICTARR


       config = XC_SYSTEM_CONFIG(self.shot_param.system $
                                            ,self.shot_param.shot)

       ; Central angle
       theta_central = ASIN(self.param.central_wavelength/config.crystal_spacing/2D)

       ; Current angle
       theta = ASIN(wavelength/config.crystal_spacing/2D)
       
       x = self.param.detector_distance * SIN(theta - theta_central) / COS(theta - self.param.detector_angle)


       ; Convert from m to channel.
       channel = x / config.x_mm_per_channel / config.x_binsize * 1e3


       ; Define channel 0 as the edge of the detector.
       num_channels_x = FLOOR(FLOAT(config.x_size) / config.x_binsize)
       channel_out = channel + num_channels_x/2D

       RETURN, channel_out

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::ANGSTROMS_TO_CHANNELS



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_THEORY_DATA

       RETURN, *self.data.theory.ptr_array

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_THEORY_DATA


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_LINE_DATA

       RETURN, *self.data.line.ptr_array

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_LINE_DATA



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_THEORY_FILE
       COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not read theory data file.'
       ENDIF

       ; filename = 'vainshtein.xc_param'
       filename = 'xc_ar_ming.xc_param'

       path = '/u/npablant/data/xcrystal/atomic/'
       filepath = MIR_PATH_JOIN([path, filename])

       array = MIR_ASCII_QUICK_READ( filepath $
                                 ,FIELD_NAMES = [ 'NAME' $
                                                  ,'TYPE' $
                                                  ,'WAVELENGTH' $
                                                  ,'LINE_WIDTH' $
                                                  ,'EXC_RATE_C1' $
                                                  ,'EXC_RATE_C2' $
                                                  ,'DIEL_RECOMB_RATE' $
                                                  ,'REL_INTENSITY' $
                                                  ,'G_FACTOR' $
                                                ] $
                                 ,SEPARATOR = '!' $
                                 ,COMMENT = ';' $
                               )


       self.data.line.filepath = filepath
       self.data.theory.ptr_array = PTR_NEW(array)

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_THEORY_FILE



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_LINE_FILE
       COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not read line data file.'
       ENDIF


       filename = 'lines.dat'

       path = '/u/npablant/data/xcrystal'
       filepath = MIR_PATH_JOIN([path, filename])

       array = MIR_ASCII_QUICK_READ( filepath $
                                 ,FIELD_NAMES = [ 'NAME' $
                                                  ,'INCLUDE' $
                                                  ,'TYPE' $
                                                  ,'DEPENDENCE' $
                                                  ,'EXC_OR_DIEL_REC' $
                                                ] $
                                 ,FIELD_TYPES = [ 7 $
                                                  ,2 $
                                                  ,2 $
                                                  ,7 $
                                                  ,2 $
                                                ] $
                                 ,SEPARATOR = '!' $
                                 ,COMMENT = ';' $
                               )


       self.data.line.filepath = filepath
       self.data.line.ptr_array = PTR_NEW(array)


     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_LINE_FILE



     ;+=========================================================================
     ; PURPOSE:
     ;   There are a number of data files that need to be loaded for this
     ;   model.  Do that here.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_DATA_FILES, RELOAD=reload


       IF ~ self.data_loaded OR KEYWORD_SET(reload) THEN BEGIN
         self->CLEAR_DATA_FILES

         self->LOAD_THEORY_FILE
         self->LOAD_LINE_FILE

         self.data_loaded = 1
       ENDIF


     END ;PRO BST_INST_MODEL_PARAMETER::LOAD_DATA_FILES




     ;+=========================================================================
     ; PURPOSE:
     ;   Remove the data loaded from the data files.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::CLEAR_DATA_FILES

       PTR_FREE, self.data.theory.ptr_array
       PTR_FREE, self.data.line.ptr_array

       self.data_loaded = 0

     END ;PRO BST_INST_MODEL_PARAMETER::CLEAR_DATA_FILES



     ;+=========================================================================
     ; PURPOSE:
     ;   Print out the state of the model.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::PRINT

       PRINT_STRUCTURE, self.param

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::PRINT



     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_DEFAULTS


         self.param = { BST_INST_MODEL_XCRYSTAL_2011_03__PARAM }

         self.param.te = 1.0
         self.param.ti = 1.0

         self.param.shift_constant = 0D
         self.param.shift_linear = 0D

         self.param.w_intensity = 1.0
         self.param.x_intensity = 1.0
         self.param.y_intensity = 1.0
         self.param.z_intensity = 1.0

         self.param.q_intensity = 0.5
         self.param.r_intensity = 0.5
         self.param.s_intensity = 0.5
         self.param.t_intensity = 0.5

         self.param.w2_factor = 0.5
         self.param.w3_factor = 0.1
         self.param.w4_factor = 0.1
         self.param.w5_factor = 0.1
         self.param.w6_factor = 0.1

         self.param.li_intensity = 0.1


         ; Central wavelength of the detector.
         self.param.central_wavelength = 3.97


         config = XC_SYSTEM_CONFIG(self.shot_param.system $
                                              ,self.shot_param.shot $
                                              ,/QUIET)
         IF ~ IS_NULL(config) THEN BEGIN
           self.param.detector_distance = MIR_MAGNITUDE(config.crystal_location - config.detector_location)

           ; Assume the detector is perpendicular to the ray from the central wavelength.
           self.param.detector_angle = ASIN(self.param.central_wavelength/2D/config.crystal_spacing)
         ENDIF

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN

         self->SET_DEFAULTS
         
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_XCRYSTAL_2011_03__FIXED }

         self.fixed.ti = 1
         self.fixed.te = 1

         self.fixed.shift_constant = 1
         self.fixed.shift_linear = 1

         self.fixed.w_intensity = 1
         self.fixed.x_intensity = 1
         self.fixed.y_intensity = 1
         self.fixed.z_intensity = 1

         self.fixed.q_intensity = 1
         self.fixed.r_intensity = 1
         self.fixed.s_intensity = 1
         self.fixed.t_intensity = 1

         self.fixed.w2_factor = 1
         self.fixed.w3_factor = 1
         self.fixed.w4_factor = 1
         self.fixed.w5_factor = 1
         self.fixed.w6_factor = 1

         self.fixed.li_intensity = 1

         self.fixed.central_wavelength = 1
         self.fixed.detector_distance = 1
         self.fixed.detector_angle = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_XCRYSTAL_2011_03__LIMITED }

         ; Temperature should always be positive.
         self.limited.ti = [1,0]
         self.limited.te = [1,0]

         ; Line amplitudes should always be positive
         self.limited.w_intensity = [1,0]
         self.limited.x_intensity = [1,0]
         self.limited.y_intensity = [1,0]
         self.limited.z_intensity = [1,0]

         self.limited.q_intensity = [1,0]
         self.limited.r_intensity = [1,0]
         self.limited.s_intensity = [1,0]
         self.limited.t_intensity = [1,0]

         self.limited.w2_factor = [1,0]
         self.limited.w3_factor = [1,0]
         self.limited.w4_factor = [1,0]
         self.limited.w5_factor = [1,0]
         self.limited.w6_factor = [1,0]

         ; Li line amplitudes should always be positive
         self.limited.li_intensity = [1,0]
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_XCRYSTAL_2011_03__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::RESET




     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{shot_param:self->GET_SHOT_PARAM()} $
                             ,{options:self->GET_OPTIONS()} $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state



       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->SET_SHOT_PARAM, state.shot_param
       ENDIF ELSE BEGIN
         self->MESSAGE, 'Could not load shot_param.', /ERROR
         CATCH, /CANCEL
       ENDELSE


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->SET_OPTIONS, state.options
       ENDIF ELSE BEGIN
         self->MESSAGE, 'Could not load options.', /ERROR
         CATCH, /CANCEL
       ENDELSE


     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Get the shot parameters of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_SHOT_PARAM
       
       RETURN, self.shot_param

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::GET_SHOT_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model shot parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_SHOT_PARAM, shot_param
       
       STRUCT_ASSIGN, {shot_param:shot_param}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_SHOT_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the options of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2011_03::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::GET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model options
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03::SET_OPTIONS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Do a recursive copy for this model object.
     ;
     ;   The copy of the model object made by BST_INST_MODEL does 
     ;   not copy the pointer data, so do that here manually.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::COPY_FROM, object_in $
                                             ,RECURSIVE=recursive

       self->BST_INST_MODEL_PARAMETER::COPY_FROM, object_in, RECURSIVE=recursive

       IF PTR_VALID(object_in.data.theory.ptr_array) THEN BEGIN
         self.data.theory.ptr_array = PTR_NEW(*object_in.data.theory.ptr_array)
       ENDIF

       IF PTR_VALID(object_in.data.line.ptr_array) THEN BEGIN
         self.data.line.ptr_array = PTR_NEW(*object_in.data.line.ptr_array)
       ENDIF

     END ;  PRO BST_INST_MODEL_XCRYSTAL_2011_03::COPY_FROM



     ;+=========================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03::CLEANUP


       self->CLEAR_DATA_FILES

       self->BST_INST_MODEL_PARAMETER::CLEANUP


     END ;PRO BST_INST_MODEL_PARAMETER::CLEANUP



     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XCRYSTAL model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2011_03__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_XCRYSTAL_2011_03__PARAM $
                 ; Ion and electron temperatures
                 ,ti:0D $
                 ,te:0D $

                 ; These parameters will be used to examine the rotation
                 ,shift_constant:0D $
                 ,shift_linear:0D $


                 ; Amplitudes of the various lines.
                 ,w_intensity:0D $
                 ,x_intensity:0D $
                 ,y_intensity:0D $
                 ,z_intensity:0D $

                 ,q_intensity:0D $
                 ,r_intensity:0D $
                 ,s_intensity:0D $
                 ,t_intensity:0D $

                 ,w2_factor:0D $
                 ,w3_factor:0D $
                 ,w4_factor:0D $
                 ,w5_factor:0D $
                 ,w6_factor:0D $

                 ; Intensity of lines coming from a Li-like state.
                 ,li_intensity:0D $

                 ; The wavelength of the central channel on the detector.
                 ,central_wavelength:0D $
                 ; The distance from the center of the detector to the center or the crystal. (mm)
                 ,detector_distance:0D $
                 ; The angle of the detector.  Referenced to the crystal centerline.
                 ,detector_angle:0D $
               } 


       fixed = { BST_INST_MODEL_XCRYSTAL_2011_03__FIXED $
                 ,ti:0 $
                 ,te:0 $
                 ,shift_constant:0 $
                 ,shift_linear:0 $
                 ,w_intensity:0 $
                 ,x_intensity:0 $
                 ,y_intensity:0 $
                 ,z_intensity:0 $
                 ,q_intensity:0 $
                 ,r_intensity:0 $
                 ,s_intensity:0 $
                 ,t_intensity:0 $
                 ,w2_factor:0 $
                 ,w3_factor:0 $
                 ,w4_factor:0 $
                 ,w5_factor:0 $
                 ,w6_factor:0 $
                 ,li_intensity:0 $
                 ,central_wavelength:0 $
                 ,detector_distance:0 $
                 ,detector_angle:0 $
               } 
       
       limited = { BST_INST_MODEL_XCRYSTAL_2011_03__LIMITED $
                 ,ti:[0, 0] $
                 ,te:[0, 0] $
                 ,shift_constant:[0, 0] $
                 ,shift_linear:[0, 0] $
                 ,w_intensity:[0, 0] $
                 ,x_intensity:[0, 0] $
                 ,y_intensity:[0, 0] $
                 ,z_intensity:[0, 0] $
                 ,q_intensity:[0, 0] $
                 ,r_intensity:[0, 0] $
                 ,s_intensity:[0, 0] $
                 ,t_intensity:[0, 0] $
                 ,w2_factor:[0, 0] $
                 ,w3_factor:[0, 0] $
                 ,w4_factor:[0, 0] $
                 ,w5_factor:[0, 0] $
                 ,w6_factor:[0, 0] $
                 ,li_intensity:[0, 0] $
                 ,central_wavelength:[0, 0] $
                 ,detector_distance:[0, 0] $
                 ,detector_angle:[0, 0] $
                 }

       limits = { BST_INST_MODEL_XCRYSTAL_2011_03__LIMITS $
                 ,ti:[0D, 0D] $
                 ,te:[0D, 0D] $
                 ,shift_constant:[0D, 0D] $
                 ,shift_linear:[0D, 0D] $
                 ,w_intensity:[0D, 0D] $
                 ,x_intensity:[0D, 0D] $
                 ,y_intensity:[0D, 0D] $
                 ,z_intensity:[0D, 0D] $
                 ,q_intensity:[0D, 0D] $
                 ,r_intensity:[0D, 0D] $
                 ,s_intensity:[0D, 0D] $
                 ,t_intensity:[0D, 0D] $
                 ,w2_factor:[0D, 0D] $
                 ,w3_factor:[0D, 0D] $
                 ,w4_factor:[0D, 0D] $
                 ,w5_factor:[0D, 0D] $
                 ,w6_factor:[0D, 0D] $
                 ,li_intensity:[0D, 0D] $
                 ,central_wavelength:[0D, 0D] $
                 ,detector_distance:[0D, 0D] $
                 ,detector_angle:[0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Setup additional model data.

       theory = { BST_INST_MODEL_XCRYSTAL_2011_03__DATA__THEORY $
                  ,filepath:'' $
                  ,ptr_array:PTR_NEW() $
                }


       line = { BST_INST_MODEL_XCRYSTAL_2011_03__DATA__LINE $
                 ,filepath:'' $
                 ,ptr_array:PTR_NEW() $
               }



       data = { BST_INST_MODEL_XCRYSTAL_2011_03__DATA $
                ,theory:{ BST_INST_MODEL_XCRYSTAL_2011_03__DATA__THEORY } $
                ,line:{ BST_INST_MODEL_XCRYSTAL_2011_03__DATA__LINE }  $
              }


       options = { BST_INST_MODEL_XCRYSTAL_2011_03__OPTIONS $
                   ,use_te:0 $
                 }

       shot_param = { BST_INST_MODEL_XCRYSTAL_2011_03__SHOT_PARAM $
                      ,shot:0L $
                      ,system:'' $
                    }

       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XCRYSTAL_2011_03 $

                 ,param:{ BST_INST_MODEL_XCRYSTAL_2011_03__PARAM} $

                 ,fixed:{ BST_INST_MODEL_XCRYSTAL_2011_03__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_XCRYSTAL_2011_03__LIMITED } $

                 ,limits:{ BST_INST_MODEL_XCRYSTAL_2011_03__LIMITS } $

                 ,data:{ BST_INST_MODEL_XCRYSTAL_2011_03__DATA } $

                 ,options:{ BST_INST_MODEL_XCRYSTAL_2011_03__OPTIONS } $
                 
                 ,shot_param:{ BST_INST_MODEL_XCRYSTAL_2011_03__SHOT_PARAM }  $

                 ,data_loaded:0 $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_XCRYSTAL_2011_03
