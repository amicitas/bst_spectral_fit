



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This object handels the xcrystal profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit He like argon spectra from the XCIS 
;   system on C-Mod.
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 0
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_XCRYSTAL_2010_12::SET_FLAGS





     ;+=================================================================
     ; PURPOSE
     ;   Setup the model prior to starting the fit loop
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::PREFIT

       self->LOAD_DATA_FILES

     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::PREFIT



     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the xcrystal model.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))

       ; Convert the x dimention into angstroms
       x_angstroms = self->CHANNELS_TO_ANGSTROMS(x)
       ;x_angstroms = self->CHANNELS_TO_ANGSTROMS_XC_SPECTRO(x)

       ; Get the line list
       linelist = self->GET_LINE_LIST()

       num_lines = linelist->N_ELEMENTS()

       FOR ii=0,num_lines-1 DO BEGIN
         
         line = linelist->GET(ii)

         y += line->EVALUATE(x_angstroms)

       ENDFOR
       

       ; Destroy the line list and all objects.
       linelist->DESTROY, /HEAP_FREE


       RETURN, y

     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::EVALUATE


     ;+=========================================================================
     ; PURPOSE:
     ;   Get a list of the lines for the XCrystal model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_LINE_LIST, x

       linelist = OBJ_NEW('MIR_LIST_IDL7')

       const = PHYSICAL_CONSTANTS()

       ; Atomic weight of argon in amu.
       ar_amu =  39.948D

       ; Get the lines from the theory file and the lines file
       ; (These files should maybe be combined.
       theory_array = self->GET_THEORY_DATA()
       line_array = self->GET_LINE_DATA() 


       ; The calculation of line amplitudes are all done relative to the W line.
       ; So extract the index for the W line. 
       ;   Calculate the excitation rate.
       ;   Calculate the line shift.
       w = WHERE(theory_array.name EQ 'W', count)
       index_line_w = w[0]

       ex_rate_w = theory_array[index_line_w].exc_rate_c1 / SQRT(self.param.te) $
                   * EXP(-1D * theory_array[index_line_w].exc_rate_c2 / self.param.te)

       line_shift = self.param.w_wavelength - theory_array[index_line_w].wavelength



       ; Loop through the lines and add them to the spectrum.
       num_lines = N_ELEMENTS(theory_array)

       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = theory_array[ii_line]

         ; Get the line structure for this line.
         w = WHERE(line_array.name EQ theory_struct.name, count)
         IF count EQ 0 THEN BEGIN
           ; I need some sort of warning somewhere, but this is not a good place for it.
           ;MESSAGE, 'Line: '+theory_struct.name+' not found in line parameter file.', /CONTINUE
           CONTINUE
         ENDIF
         line_struct = line_array[w[0]]
         

         ; Natural line width.
         gamma = theory_struct.line_width * 1D13 * theory_struct.wavelength^2  $
                 / ( 4D * !dpi * const.c * 1D10 )

         ; Doppler broadened line width.
         sigma = SQRT(self.param.ti / ar_amu / const.amu_kg / const.c^2 * const.ev_J * 1e3) $
                 * theory_struct.wavelength

         ; Line location
         location = theory_struct.wavelength + line_shift



         ; To find the line amplitude, the lines need to be treated differently 
         ; depending on whether they are normal emission lines or dielectric recombination lines.
         CASE theory_struct.type OF


           'LINE': BEGIN
             ; Handle the aplitude W line differently, since we base all other
             ; lines off of it.
             IF theory_struct.name EQ 'W' THEN BEGIN
               line_intensity = self.param.w_intensity
             ENDIF ELSE BEGIN
               CASE line_struct.dependence OF
                 'XA': ratio = self.param.x_ratio
                 'YA': ratio = self.param.y_ratio
                 'ZA': ratio = self.param.z_ratio
                 'LI': ratio = self.param.li_ratio
                 ELSE: ratio = 1D
               ENDCASE

               ; Calculate the excitation rate for this line
               ex_rate = theory_struct.exc_rate_c1 / SQRT(self.param.te) $
                         * EXP(-1D * theory_struct.exc_rate_c2 / self.param.te)

               line_intensity = self.param.w_intensity * ratio * ex_rate / ex_rate_w
             ENDELSE

           END ; 'LINE'


           'SATELLITE': BEGIN
             
             factor = 5.22271E-27/self.param.te^(1.5)

             ; The dielectronic recombination rate will be the same for all 
             ; of the satellites from a particular n.

             ex_rate = factor * EXP(-1D * theory_struct.diel_recomb_rate / self.param.te)

             rel_intensity = theory_struct.rel_intensity * 1e13
             line_intensity = self.param.w_intensity * rel_intensity * ex_rate / ex_rate_w

           END ; 'SATELLITE'

           
           ELSE: BEGIN
             MESSAGE, 'Unknown line type: '+theory_struct[ii_line].type
           ENDELSE

         ENDCASE


         voigt = OBJ_NEW('MIR_VOIGT' $
                         ,{intensity:line_intensity $
                           ,location:location $
                           ,sigma:sigma $
                           ,gamma:gamma $
                          } $
                        )

         linelist->APPEND, voigt

       ENDFOR
       

       RETURN, linelist


     END ; BST_INST_MODEL_XCRYSTAL_2010_12::GET_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from channels to angstroms.  
     ;
     ;   I am doing something wrong in my angstroms to channels calculation.
     ;   Let me recreate the XC_SPECTRO calculation.
     ;
     ;   NOTE:  I am using named fit variables for other purposed here.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::CHANNELS_TO_ANGSTROMS_XC_SPECTRO, channel_in


       crystal = self->GET_CRYSTAL_DATA()
       detector = self->GET_DETECTOR_DATA()

       w_wavelength = 3.9494
       theta_w = ASIN(w_wavelength/crystal.crystal_spacing/2D)

       delta = (self.param.detector_angle * (channel_in - self.param.central_wavelength)) $
               / (crystal.crystal_curvature * SIN(theta_w))


       lambda = crystal.crystal_spacing * 2D * SIN(theta_w +  delta)

;num_points = N_ELEMENTS(channel_in)
;FOR ii=0,num_points-1 DO BEGIN
;  PRINT, channel_in[0]+ii, lambda[ii]
;ENDFOR


       RETURN, lambda

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::CHANNELS_TO_ANGSTROMS_XC_SPECTRO



     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from channels to angstroms.  This is done using the geometry
     ;   and known crystal and detector parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::CHANNELS_TO_ANGSTROMS, channel_in


       crystal = self->GET_CRYSTAL_DATA()
       detector = self->GET_DETECTOR_DATA()

       ; Define channel 0 as the center of the detector.
       num_channels_x =  detector.size[0]
       channel = channel_in - num_channels_x/2D

       ; Convert from channel to mm
       x = channel * detector.mm_per_channel_x 

       theta_central = ASIN(self.param.central_wavelength/crystal.crystal_spacing/2D)

       theta = ATAN( ( self.param.detector_distance * SIN(theta_central)  $
                       + x * COS(self.param.detector_angle) ) $
                     / ( self.param.detector_distance * COS(theta_central) $
                         -  x * SIN(self.param.detector_angle) ) $
                   )


       wavelength = 2D * crystal.crystal_spacing * SIN(theta)

;PRINT, 'x (channels) ', channel_in[100]
;PRINT, 'x (mm)       ', x[100]
;PRINT, 'theta_central', theta_central/!dtor
;PRINT, 'theta        ', theta[100]/!dtor
;PRINT, 'wavelength   ', wavelength[100]

       RETURN, wavelength

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::CHANNELS_TO_ANGSTROMS



     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from angstroms to channels.  This is done using the geometry
     ;   and known crystal and detector parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::ANGSTROMS_TO_CHANNELS, wavelength

       crystal = self->GET_CRYSTAL_DATA()
       detector = self->GET_DETECTOR_DATA()

       ; Central angle
       theta_central = ASIN(self.param.central_wavelength/crystal.crystal_spacing/2D)

       ; Current angle
       theta = ASIN(wavelength/crystal.crystal_spacing/2D)
       
       x = self.param.detector_distance * SIN(theta - theta_central) / COS(theta - self.param.detector_angle)


       ; Convert from mm to channel.
       channel = x / detector.mm_per_channel_x 


       ; Define channel 0 as the edge of the detector.
       num_channels_x = detector.size[0]
       channel_out = channel + num_channels_x/2D

       RETURN, channel_out

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::ANGSTROMS_TO_CHANNELS



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_THEORY_DATA

       RETURN, *self.data.theory.ptr_array

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_THEORY_DATA


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_LINE_DATA

       RETURN, *self.data.line.ptr_array

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_LINE_DATA



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_CRYSTAL_DATA

       RETURN, self.data.crystal

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_CRYSTAL_DATA



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_DETECTOR_DATA

       RETURN, self.data.detector

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL_2010_12::GET_DETECTOR_DATA


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_THEORY_FILE
       COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not read theory data file.'
       ENDIF

       ; filename = 'vainshtein.xc_param'
       filename = 'xc_ar_ming.xc_param'

       path = '/u/npablant/data/xcrystal'
       filepath = MIR_PATH_JOIN([path, filename])

       array = MIR_ASCII_QUICK_READ( filepath $
                                 ,FIELD_NAMES = [ 'NAME' $
                                                  ,'TYPE' $
                                                  ,'WAVELENGTH' $
                                                  ,'LINE_WIDTH' $
                                                  ,'EXC_RATE_C1' $
                                                  ,'EXC_RATE_C2' $
                                                  ,'DIEL_RECOMB_RATE' $
                                                  ,'REL_INTENSITY' $
                                                  ,'G_FACTOR' $
                                                ] $
                                 ,SEPARATOR = '!' $
                                 ,COMMENT = ';' $
                               )


       self.data.line.filepath = filepath
       self.data.theory.ptr_array = PTR_NEW(array)

     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_THEORY_FILE



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_LINE_FILE
       COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not read line data file.'
       ENDIF


       filename = 'lines.dat'

       path = '/u/npablant/data/xcrystal'
       filepath = MIR_PATH_JOIN([path, filename])

       array = MIR_ASCII_QUICK_READ( filepath $
                                 ,FIELD_NAMES = [ 'NAME' $
                                                  ,'INCLUDE' $
                                                  ,'TYPE' $
                                                  ,'DEPENDENCE' $
                                                  ,'EXC_OR_DIEL_REC' $
                                                ] $
                                 ,FIELD_TYPES = [ 7 $
                                                  ,2 $
                                                  ,2 $
                                                  ,7 $
                                                  ,2 $
                                                ] $
                                 ,SEPARATOR = '!' $
                                 ,COMMENT = ';' $
                               )


       self.data.line.filepath = filepath
       self.data.line.ptr_array = PTR_NEW(array)


     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_LINE_FILE



     ;+=========================================================================
     ; PURPOSE:
     ;   Load data from the crystal file.
     ;   Note for the time being I am just hard-coding the crystal to use.
     ;
     ;   Also for the time being I am assuming tha the crystal file contains:
     ;     Crystal properties
     ;     Detector properties
     ;     System geometry
     ;   
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_CRYSTAL_FILE
       COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not read crystal data file.'
       ENDIF


       filename = 'crystal.dat'
       crystal_name = 'CMOD_2010'

       path = '/u/npablant/data/xcrystal'
       filepath = MIR_PATH_JOIN([path, filename])


       array = MIR_ASCII_QUICK_READ( filepath $
                                 ,FIELD_NAMES = [ 'NAME' $
                                                  ,'CRYSTAL_SPACING' $
                                                  ,'CRYSTAL_CURVATURE' $
                                                  ,'MM_PER_CHANNEL_X' $
                                                  ,'MM_PER_CHANNEL_Y' $
                                                  ,'DETECTOR_SIZE_X' $
                                                  ,'DETECTOR_SIZE_Y' $
                                                  ,'A0' $
                                                  ,'Y0' $
                                                  ,'INSTRUMENTAL_WIDTH' $
                                                ] $
                                 ,FIELD_TYPES = [ 7 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                  ,5 $
                                                ] $
                                 ,SEPARATOR = '!' $
                                 ,COMMENT = ';' $
                               )


       w = WHERE(array.name EQ crystal_name, count)
       crystal_data = array[w[0]]

       ; Fill the crystal strucuture.
       self.data.crystal.crystal_spacing = crystal_data.crystal_spacing/2D
       self.data.crystal.crystal_curvature = crystal_data.crystal_curvature

       ; Fill the detector structure.
       self.data.detector.mm_per_channel_x = crystal_data.mm_per_channel_x
       self.data.detector.mm_per_channel_y = crystal_data.mm_per_channel_y

       ; For now just hard code the rest of the detector information.
       self.data.detector.size = [195, 1461]



     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_CRYSTAL_FILE



     ;+=========================================================================
     ; PURPOSE:
     ;   There are a number of data files that need to be loaded for this
     ;   model.  Do that here.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::LOAD_DATA_FILES, RELOAD=reload


       IF ~ self.data_loaded OR KEYWORD_SET(reload) THEN BEGIN
         self->CLEAR_DATA_FILES

         self->LOAD_THEORY_FILE
         self->LOAD_LINE_FILE
         self->LOAD_CRYSTAL_FILE

         self.data_loaded = 1
       ENDIF


     END ;PRO BST_INST_MODEL_PARAMETER::LOAD_DATA_FILES




     ;+=========================================================================
     ; PURPOSE:
     ;   Remove the data loaded from the data files.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::CLEAR_DATA_FILES

       PTR_FREE, self.data.theory.ptr_array
       PTR_FREE, self.data.line.ptr_array

       self.data_loaded = 0

     END ;PRO BST_INST_MODEL_PARAMETER::CLEAR_DATA_FILES



     ;+=========================================================================
     ; PURPOSE:
     ;   Print out the state of the model.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::PRINT

       PRINT_STRUCTURE, self.param

     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::PRINT



     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::SET_DEFAULTS


         self.param = { BST_INST_MODEL_XCRYSTAL_2010_12__PARAM }

         self.param.te = 1.0
         self.param.ti = 1.0

         self.param.w_intensity = 1.0
         self.param.x_ratio = 1.0
         self.param.y_ratio = 1.0
         self.param.z_ratio = 1.0
         self.param.li_ratio = 0.1

         ; For He-like argon.
         self.param.w_wavelength = 3.94942

         ; Central wavelength of the detector.
         self.param.central_wavelength = 3.97


         crystal = self->GET_CRYSTAL_DATA()

         ; Assume the center of the detector is on the roland circle.
         self.param.detector_distance = $
            crystal.crystal_curvature * self.param.central_wavelength/2D/crystal.crystal_spacing

         ; Assume the detector is perpendicular to the ray from the central wavelength.
         self.param.detector_angle = ASIN(self.param.central_wavelength/2D/crystal.crystal_spacing)
            


     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN

         self->SET_DEFAULTS
         
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_XCRYSTAL_2010_12__FIXED }

         self.fixed.w_wavelength = 1

         self.fixed.x_ratio = 1
         self.fixed.y_ratio = 1
         self.fixed.z_ratio = 1
         self.fixed.li_ratio = 1

         self.fixed.central_wavelength = 1
         self.fixed.detector_distance = 1
         self.fixed.detector_angle = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_XCRYSTAL_2010_12__LIMITED }
         ; Temperature should always be positive.
         self.limited.ti = [1,0]
         self.limited.te = [1,0]

         ; Line amplitudes should always be positive
         self.limited.w_intensity = [1,0]

         ; Concentrations should also be positive
         self.limited.x_ratio = [1,0]
         self.limited.y_ratio = [1,0]
         self.limited.z_ratio = [1,0]
         self.limited.li_ratio = [1,0]

       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_XCRYSTAL_2010_12__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12::RESET


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Do a recursive copy for this model object.
     ;
     ;   The copy made by BST_INST_MODEL does not copy the pointer data,
     ;   so do that here manually.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::COPY_FROM, object_in $
                                       ,RECURSIVE=recursive

       self->BST_INST_MODEL_PARAMETER::COPY_FROM, object_in, RECURSIVE=recursive

       IF PTR_VALID(object_in.data.theory.ptr_array) THEN BEGIN
         self.data.theory.ptr_array = PTR_NEW(*object_in.data.theory.ptr_array)
       ENDIF

       IF PTR_VALID(object_in.data.line.ptr_array) THEN BEGIN
         self.data.line.ptr_array = PTR_NEW(*object_in.data.line.ptr_array)
       ENDIF

     END ;  PRO BST_INST_MODEL_XCRYSTAL_2010_12::COPY_FROM


     ;+=========================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12::CLEANUP


       self->CLEAR_DATA_FILES

       self->BST_INST_MODEL_PARAMETER::CLEANUP


     END ;PRO BST_INST_MODEL_PARAMETER::CLEANUP



     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XCRYSTAL model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_2010_12__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_XCRYSTAL_2010_12__PARAM $
                 ; Ion and electron temperatures
                 ,ti:0D $
                 ,te:0D $

                 ; Amplitudes of the various lines.
                 ,w_intensity:0D $

                 ; The recorded of the doppler-shifted W line. (angstroms)
                 ,w_wavelength:0D $

                 ; These relate to the concentrations of the states that give
                 ; rise to particular lines.
                 ,x_ratio:0D $
                 ,y_ratio:0D $
                 ,z_ratio:0D $
                 ,li_ratio:0D $

                 ; The wavelength of the central channel on the detector.
                 ,central_wavelength:0D $
                 ; The distance from the center of the detector to the center or the crystal. (mm)
                 ,detector_distance:0D $
                 ; The angle of the detector.  Referenced to the crystal centerline.
                 ,detector_angle:0D $
               } 


       fixed = { BST_INST_MODEL_XCRYSTAL_2010_12__FIXED $
                 ,ti:0 $
                 ,te:0 $
                 ,w_intensity:0 $
                 ,w_wavelength:0 $
                 ,x_ratio:0 $
                 ,y_ratio:0 $
                 ,z_ratio:0 $
                 ,li_ratio:0 $
                 ,central_wavelength:0 $
                 ,detector_distance:0 $
                 ,detector_angle:0 $
               } 
       
       limited = { BST_INST_MODEL_XCRYSTAL_2010_12__LIMITED $
                   ,ti:[0,0] $
                   ,te:[0,0] $
                   ,w_intensity:[0,0] $
                   ,w_wavelength:[0,0] $
                   ,x_ratio:[0,0] $
                   ,y_ratio:[0,0] $
                   ,z_ratio:[0,0] $
                   ,li_ratio:[0,0] $
                   ,central_wavelength:[0,0] $
                   ,detector_distance:[0,0] $
                   ,detector_angle:[0,0] $
                 }

       limits = { BST_INST_MODEL_XCRYSTAL_2010_12__LIMITS $
                  ,ti:[0D,0D] $
                  ,te:[0D,0D] $
                  ,w_intensity:[0D,0D] $
                  ,w_wavelength:[0D,0D] $
                  ,x_ratio:[0D,0D] $
                  ,y_ratio:[0D,0D] $
                  ,z_ratio:[0D,0D]  $
                  ,li_ratio:[0D,0D] $
                  ,central_wavelength:[0D,0D] $
                  ,detector_distance:[0D,0D] $
                  ,detector_angle:[0D,0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Setup additional model data.

       theory = { BST_INST_MODEL_XCRYSTAL_2010_12__DATA__THEORY $
                  ,filepath:'' $
                  ,ptr_array:PTR_NEW() $
                }


       line = { BST_INST_MODEL_XCRYSTAL_2010_12__DATA__LINE $
                 ,filepath:'' $
                 ,ptr_array:PTR_NEW() $
               }

       crystal = { BST_INST_MODEL_XCRYSTAL_2010_12__DATA__CRYSTAL $
                   ,crystal_spacing:0D $
                   ,crystal_curvature:0D $
                 }

       detector = { BST_INST_MODEL_XCRYSTAL_2010_12__DATA__DETECTOR $
                    ,size:[0L,0L] $
                    ,mm_per_channel_x:0D $
                    ,mm_per_channel_y:0D $
                  }



       data = { BST_INST_MODEL_XCRYSTAL_2010_12__DATA $
                ,theory:{ BST_INST_MODEL_XCRYSTAL_2010_12__DATA__THEORY } $
                ,line:{ BST_INST_MODEL_XCRYSTAL_2010_12__DATA__LINE }  $
                ,crystal:{ BST_INST_MODEL_XCRYSTAL_2010_12__DATA__CRYSTAL } $
                ,detector:{ BST_INST_MODEL_XCRYSTAL_2010_12__DATA__DETECTOR } $
              }



       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XCRYSTAL_2010_12 $

                 ,param:{ BST_INST_MODEL_XCRYSTAL_2010_12__PARAM} $

                 ,fixed:{ BST_INST_MODEL_XCRYSTAL_2010_12__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_XCRYSTAL_2010_12__LIMITED } $

                 ,limits:{ BST_INST_MODEL_XCRYSTAL_2010_12__LIMITS } $

                 ,data:{ BST_INST_MODEL_XCRYSTAL_2010_12__DATA } $

                 ,data_loaded:0 $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_XCRYSTAL_2010_12
