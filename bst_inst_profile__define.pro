


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the profile object.
     ;-=================================================================
     FUNCTION BST_INST_PROFILE::INIT, CENTER=center $
                                      ,CENTROID=centroid $
                                      ,LOCATION_MAX=location_max

       ; By default use the centroid position.
       CASE 1 OF
         KEYWORD_SET(centroid): self->SET_USE_CENTROID
         KEYWORD_SET(center): self->SET_USE_CENTER
         KEYWORD_SET(location_max): self->SET_LOCATION_MAX
         ELSE: self->SET_USE_CENTROID
       ENDCASE

       RETURN, 1

     END ;PRO BST_INST_PROFILE::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Move the profile to be at the given center location.
     ;
     ; KEYWORDS:
     ;   CENTROID = float
     ;       This keyword is ignored.  It is retained for compatiblity
     ;       with <SUM_OF_GAUSS::MOVE>.
     ;
     ;-=================================================================
     PRO BST_INST_PROFILE::MOVE, loc_in $
                                 ,CENTROID=centroid
       
       distance = loc_in - self->LOCATION()

       self->SHIFT, distance

     END ;PRO BST_INST_PROFILE::MOVE



     ;+=================================================================
     ; PURPOSE:
     ;   Get the profile location.
     ;
     ;   This will return either the centroid, location of maximum value
     ;   or user set center based on the object preferences.
     ;-=================================================================
     FUNCTION BST_INST_PROFILE::LOCATION

       CASE 1 OF
         self.use_centroid: location = self->CENTROID()
         self.use_center: location = self->GET_CENTER()
         self.use_location_max: location = self->GET_LOCATION_MAX()
       ENDCASE
       
       RETURN, location

     END ;FUNCTION BST_INST_PROFILE::LOCATION



     ;+=================================================================
     ; PURPOSE:
     ;   Get set value for the profile center
     ;-=================================================================
     FUNCTION BST_INST_PROFILE::GET_CENTER
       
       RETURN, self.center

     END ;FUNCTION BST_INST_PROFILE::GET_CENTER



     ;+=================================================================
     ; PURPOSE:
     ;   Set the profile center.
     ;
     ;   This center value will only be used if self.use_center = 1.
     ;-=================================================================
     PRO BST_INST_PROFILE::SET_CENTER, center
       
       self.center = center

     END ;PRO BST_INST_PROFILE::SET_CENTER



     ;+=================================================================
     ; PURPOSE:
     ;   Set the location definition.
     ;-=================================================================
     PRO BST_INST_PROFILE::SET_USE_CENTER
 
       self.use_center = 1
       self.use_centroid = 0
       self.use_location_max = 0

     END ;PRO BST_INST_PROFILE::SET_USE_CENTER, bool



     ;+=================================================================
     ; PURPOSE:
     ;   Set the location definition.
     ;-=================================================================
     PRO BST_INST_PROFILE::SET_USE_CENTROID
  
       self.use_center = 0
       self.use_centroid = 1 
       self.use_location_max = 0 

     END ;PRO BST_INST_PROFILE::SET_USE_CENTROID, bool



     ;+=================================================================
     ; PURPOSE:
     ;   Set the location definition.
     ;-=================================================================
     PRO BST_INST_PROFILE::SET_USE_LOCATION_MAX

       self.use_center = 0
       self.use_centroid = 0
       self.use_location_max = 1 

     END ;PRO BST_INST_PROFILE::SET_USE_LOCATION_MAX, bool



     ;+=================================================================
     ; PURPOSE:
     ;   Return the profile label.
     ;-=================================================================
     FUNCTION BST_INST_PROFILE::GET_LABEL
       
       RETURN, self.label

     END ;FUNCTION BST_INST_PROFILE::GET_LABEL



     ;+=================================================================
     ; PURPOSE:
     ;   Set profile label.
     ;-=================================================================
     PRO BST_INST_PROFILE::SET_LABEL, label
 
       self.label = label

     END ;PRO BST_INST_PROFILE::SET_LABEL



     ;+=================================================================
     ; PURPOSE:
     ;   Copy the input object to the current object.
     ;
     ;   If recursive is set, the copy the gaussians across when
     ;   possible.
     ;   
     ;-=================================================================
     PRO BST_INST_PROFILE::COPY_FROM, data, _REF_EXTRA=extra
       
       self->SUM_OF_GAUSS::COPY_FROM, data, _STRICT_EXTRA=extra

       self.center = data.center

       self.use_center = data.use_center
       self.use_centroid = data.use_centroid
       self.use_location_max = data.use_location_max

     END ;PRO BST_INST_PROFILE::COPY_FROM



     ;+=================================================================
     ; PURPOSE:
     ;   Define the profile object.
     ;
     ; PROGRAMMING NOTES:
     ;   self.profile is a reference to a sum_of_gauss object.
     ;   We do not modify this object in any way (including destruct)
     ;
     ;-=================================================================
     PRO BST_INST_PROFILE__DEFINE

       struct ={ BST_INST_PROFILE $

                 ,center:0D $
                 
                 ,use_center:0 $
                 ,use_centroid:0 $
                 ,use_location_max:0 $

                 ,label:0L $

                 ,INHERITS SUM_OF_GAUSS $
               }

     END ;PRO BST_INST_PROFILE
