
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from a file or a user defined function.
; 
; DESCRIPTION:
;   If the spectrum is to be loaded from a file, then the file should 
;   be an IDL sav file containing a variables named 'spectrum' that
;   contains the structure described below.
;
;   If the spectrum is loded from a user defined function then the 
;   function should return the structure described below.
;
;   spectrum = {y:array [,x:array][,weight:array]}
;
;   This structure should contain the following tags:
;     Tags:
;        y
;
;     Tags (Optional):
;        x 
;        weight
;        wavelength
;
;   Each of the tags should contain an array.
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::SET_FLAGS

       self.BST_INST_GUIADDON::SET_FLAGS

       self.active = 0
       self.default = 0

       self.use_tab = 1
       self.use_control = 0

       self.add_to_menu = 1
       self.menu_title = 'MODEL: XCrystal'

       self.title = 'XCrystal'

       self.required = 'BST_INST_GUIADDON_XCRYSTAL_SPECTRA'

     END ; PRO BST_INST_GUIADDON_XCRYSTAL::SET_FLAGS


; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_XCRYSTAL::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::GUI_DEFINE
                          
 
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Add a widget to control the addon
       self.REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_XCRYSTAL' $
                             ,self.model $
                             ,DESTINATION=ui_base)


     END ; PRO BST_INST_GUIADDON_XCRYSTAL::GUI_DEFINE
     


     ;+=================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::EVENT_HANDLER, event


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF 


       ; First call the superclass method
       self.BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)


     END ;PRO BST_INST_GUIADDON_XCRYSTAL::EVENT_HANDLER



     ;+=================================================================
     ;
     ; Update the gui from the current object parameters
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::UPDATE_GUI

       self.widget.UPDATE

     END ;PRO BST_INST_GUIADDON_XCRYSTAL::UPDATE_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget.DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_XCRYSTAL::REMOVE_WIDGET



; ======================================================================
; ======================================================================
; ######################################################################
;
; BEGIN METHODS TO BE CALLED BY THE FITTER OR OTHER ADDONS
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Perform actions before the fit is started.
     ;
     ; DESCRIPTION:
     ;   In the past I was loading the model that was specified in this
     ;   addon into the spectrum addon at this point. I have now changed
     ;   this so the spectrum addon automically identified as loads
     ;   the model itself. This fixes some issues with using the
     ;   XCRYSTAL_MULTI model.
     ;
     ;   There is a potential issue in the way I am doing this in that
     ;   if we want to use the spectra from one system, with a different
     ;   model, there is not currently a way to do that.  I'll wait
     ;   untill I identify a use case before fixing this at the moment.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::BEFORE_FIT

       IF self.model.USE() THEN BEGIN
         ; Get a reference to the xcrystal spectrum object.
         ;spectra_obj = self.dispatcher.GET_ADDON('BST_INST_GUIADDON_XCRYSTAL_SPECTRA')
         
         ; Set the model in the xcrystal spectra addon.
         ;spectra_obj.model = self.model
       ENDIF

     END ; PRO BST_INST_GUIADDON_XCRYSTAL::BEFORE_FIT
       


     ;+=================================================================
     ; PURPOSE:
     ;   Return a plotlist to be added to the spectrum.
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_XCRYSTAL::GET_SPECTRUM_PLOTLIST, x_values


       plot_excitation_lines = 1
       plot_satellite_lines = 1
       plot_satellite_contribution=0

       cutoff = 1e-3

       plotlist = OBJ_NEW('MIR_PLOTLIST')

       IF ~ self.model.USE() THEN BEGIN
         RETURN, plotlist
       ENDIF


       ; Convert the x dimention into angstroms
       x_angstroms = self.model.CHANNELS_TO_ANGSTROMS(x_values)

       ; Get the line list
       linelist = self.model.GET_LINE_LIST()
       theorylist = self.model.GET_THEORY_LIST()

       ; Get the line information.
       line_data = self.model.GET_THEORY_DATA()
       theory_array = line_data.data
         
       num_lines = linelist.N_ELEMENTS()



       ; -----------------------------------------------------------------------
       ; Plot the normal excitation lines.
       IF plot_excitation_lines THEN BEGIN
         FOR ii=0,num_lines-1 DO BEGIN           
           theory_struct = theory_array[ii]

           skip = 0
           CASE theory_struct.n OF
             0: color = MIR_COLOR('print purple')
             ELSE: skip = 1
           ENDCASE
           IF skip THEN CONTINUE

           line = linelist[ii]
           y = line.EVALUATE(x_angstroms)


           ; This model returns y in units of counts/angstrom.
           ; I want to convert this to counts/channel.
           angstroms_per_channel = ABS(self.model.ANGSTROMS_PER_CHANNEL(x_values))
           y *= angstroms_per_channel

           where_nonzero = WHERE(ABS(y) GE cutoff, count_nonzero)
           IF count_nonzero GT 0 THEN BEGIN
             plotlist.APPEND, {x:x_values[where_nonzero] $
                                ,y:y[where_nonzero] $
                                ,color:color $
                                ,oplot:1 $
                                ,plotnum:0 $
                                ,psym:0 $
                               }
           ENDIF

         ENDFOR
       ENDIF

       ; -----------------------------------------------------------------------
       ; Plot the satellite lines.
       IF plot_satellite_lines THEN BEGIN
         FOR ii=0,num_lines-1 DO BEGIN                      
           theory_struct = theory_array[ii]

           skip = 0
           CASE theory_struct.n OF
             2: color = MIR_COLOR('print blue')
             3: color = MIR_COLOR('print cyan')
             4: color = MIR_COLOR('print magenta')
             5: color = MIR_COLOR('print orange')
             6: color = MIR_COLOR('print green')
             ELSE: skip = 1
           ENDCASE
           IF skip THEN CONTINUE
           
           line = linelist[ii]
           y = line.EVALUATE(x_angstroms)


           ; This model returns y in units of counts/angstrom.
           ; I want to convert this to counts/channel.
           angstroms_per_channel = ABS(self.model.ANGSTROMS_PER_CHANNEL(x_values))
           y *= angstroms_per_channel


           where_nonzero = WHERE(ABS(y) GE cutoff, count_nonzero)
           IF count_nonzero GT 0 THEN BEGIN
             plotlist.APPEND, {x:x_values[where_nonzero] $
                                ,y:y[where_nonzero] $
                                ,color:color $
                                ,oplot:1 $
                                ,plotnum:0 $
                                ,psym:0 $
                               }
           ENDIF

         ENDFOR
       ENDIF


       ; -----------------------------------------------------------------------
       ; Sum up the contribution of the  satellite lines and plot.
       IF plot_satellite_contribution THEN BEGIN

         y = DBLARR(N_ELEMENTS(x_values))
         color = MIR_COLOR('print green')

         FOR ii=0,num_lines-1 DO BEGIN
           theory_struct = theory_array[ii]

           ; Skip the normal exictation lines.
           IF theory_struct.n EQ 0 THEN CONTINUE

           line = linelist[ii]
           y += line.EVALUATE(x_angstroms)

         ENDFOR

         ; This model returns y in units of counts/angstrom.
         ; I want to convert this to counts/channel.
         angstroms_per_channel = ABS(self.model.ANGSTROMS_PER_CHANNEL(x_values))
         y *= angstroms_per_channel


         where_nonzero = WHERE(ABS(y) GE cutoff, count_nonzero)
         IF count_nonzero GT 0 THEN BEGIN
           plotlist.APPEND, {x:x_values[where_nonzero] $
                             ,y:y[where_nonzero] $
                             ,color:color $
                             ,oplot:1 $
                             ,plotnum:0 $
                             ,psym:0 $
                            }
         ENDIF

       ENDIF



       RETURN, plotlist

     END ; FUNCTION BST_INST_GUIADDON_XCRYSTAL::GET_SPECTRUM_PLOTLIST



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL::CLEANUP

       IF OBJ_VALID(self.widget) THEN BEGIN
         self.REMOVE_WIDGET
       ENDIF

       self.BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_XCRYSTAL::CLEANUP



     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL__DEFINE

       struct = { BST_INST_GUIADDON_XCRYSTAL $
                           
                  ,model:OBJ_NEW() $

                  ,widget:OBJ_NEW() $

                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_XCRYSTAL__DEFINE
