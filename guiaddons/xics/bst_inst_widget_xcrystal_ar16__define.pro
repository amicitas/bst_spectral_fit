




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   A widget to control the <BST_INST_MODEL_XCRYSTAL::> model
;  for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL_AR16::UPDATE
       
       self.BST_INST_WIDGET_XCRYSTAL::UPDATE
       
       id = self->GET_ID_STRUCT()
       param = self.model->GET_PARAM()
       options = self.model->GET_OPTIONS()
       fixed = self.model->GET_FIXED()

       gui_id_names = TAG_NAMES(id)

       ; -----------------------------------------------------------------------
       ; Gray out parameters based on the options.
       insensitive = []
       IF options.use_te THEN BEGIN
         insensitive = [insensitive $
                        ,'q_intensity' $
                        ,'r_intensity' $
                        ,'s_intensity' $
                        ,'t_intensity' $
                        ,'w2_factor' $
                        ,'w3_factor' $
                        ,'w4_factor' $
                        ,'w5_factor' $
                        ,'w6_factor' $
                       ]
       ENDIF ELSE BEGIN
         insensitive = [insensitive $
                        ,'te' $
                        ,'li_fraction' $
                       ]
       ENDELSE
       
       
       FOR ii=0,N_ELEMENTS(insensitive)-1 DO BEGIN
         name = STRUPCASE(insensitive[ii])

         w = WHERE(gui_id_names EQ 'UI_FIELD_'+name)
         WIDGET_CONTROL, id.(w[0]), SENSITIVE=0

         w = WHERE(gui_id_names EQ 'UI_BGROUP_'+name)
         WIDGET_CONTROL, id.(w[0]), SENSITIVE=0

       ENDFOR



     END ; PRO BST_INST_WIDGET_XCRYSTAL_AR16::UPDATE


     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL_AR16__DEFINE

       struct = { BST_INST_WIDGET_XCRYSTAL_AR16 $

                  ,INHERITS BST_INST_WIDGET_XCRYSTAL}

     END ; PRO BST_INST_WIDGET_XCRYSTAL_AR16__DEFINE
