




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2013-01
;
; PURPOSE:
;   A widget to control the [BST_INST_MODEL_XCRYSTAL_AR17::] model
;  for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL_AR17::UPDATE
       
       self.BST_INST_WIDGET_XCRYSTAL::UPDATE
       
       id = self->GET_ID_STRUCT()
       param = self.model->GET_PARAM()
       options = self.model->GET_OPTIONS()
       fixed = self.model->GET_FIXED()

       gui_id_names = TAG_NAMES(id)

       ; -----------------------------------------------------------------------
       ; Gray out parameters based on the options.
       insensitive = []
       IF options.use_te THEN BEGIN
         insensitive = [insensitive $
                        ,'la1_intensity' $
                        ,'la2_intensity' $
                        ,'s2_factor' $
                        ,'s3_factor' $
                        ,'s4_factor' $
                        ,'s5_factor' $
                        ,'s6_factor' $
                       ]
       ENDIF ELSE BEGIN
         insensitive = [insensitive $
                        ,'te' $
                       ]
       ENDELSE
       
       
       FOR ii=0,N_ELEMENTS(insensitive)-1 DO BEGIN
         name = STRUPCASE(insensitive[ii])
         w = WHERE(gui_id_names EQ 'UI_FIELD_'+name, count)
      
         WIDGET_CONTROL, id.(w[0]), SENSITIVE=0

         w = WHERE(gui_id_names EQ 'UI_BGROUP_'+name, count)
         WIDGET_CONTROL, id.(w[0]), SENSITIVE=0

       ENDFOR



     END ; PRO BST_INST_WIDGET_XCRYSTAL_AR17::UPDATE


     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL_AR17__DEFINE

       struct = { BST_INST_WIDGET_XCRYSTAL_AR17 $

                  ,INHERITS BST_INST_WIDGET_XCRYSTAL}

     END ; PRO BST_INST_WIDGET_XCRYSTAL_AR17__DEFINE
