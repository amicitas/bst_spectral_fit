




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   A widget to control the <BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::> addon 
;   for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::INIT, addon $
                                                ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_ADDON, addon

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the addon object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::SET_ADDON, addon
       
       MIR_DEFAULT, addon, OBJ_NEW()

       self.addon =  addon

     END ; PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::SET_ADDON



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the 
     ;   <BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::> addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ui_base_main = WIDGET_BASE(ui_base, /ROW)

       ; Set up the basic XC_GEOMETRY_SPECTRA options.
       ; ---------------------------------------------------------------
       ui_base_top = WIDGET_BASE(ui_base_main, /ROW)


       ui_base_shot = WIDGET_BASE(ui_base_top $
                                  ,/COLUMN $
                                  ,FRAME=1)


         ; Shot number
         ;ui_field_shot = CW_FIELD_EXT(ui_base_shot, UNAME='ui_field_shot' $
         ;                             ,VALUE=0 $
         ;                             ,TITLE='Shot' $
         ;                             ,/LONG $
         ;                             ,/ALL_EVENTS $
         ;                             ,XSIZE=10 $
         ;                             ,MAXLENGTH=10 $
         ;                             ,LABEL_XSIZE=50)
         ; System name
         ;ui_field_system = CW_FIELD_EXT(ui_base_shot $
         ;                               , UNAME='ui_field_system' $
         ;                               ,VALUE=0 $
         ;                               ,TITLE='System' $
         ;                               ,/STRING $
         ;                               ,/ALL_EVENTS $
         ;                               ,XSIZE=10 $
         ;                               ,LABEL_XSIZE=50)


         ui_field_num_rows = CW_FIELD_EXT(ui_base_shot, UNAME='ui_field_num_rows' $
                                          ,VALUE=0 $
                                          ,TITLE='Num Rows' $
                                          ,/LONG $
                                          ,/ALL_EVENTS $
                                          ,XSIZE=10 $
                                          ,MAXLENGTH=10 $
                                          ,LABEL_XSIZE=50)



       id_struct = { $
                     ui_base:ui_base $

                     ;,ui_field_shot:ui_field_shot $
                     ;,ui_field_system:ui_field_system $

                     ,ui_field_num_rows:ui_field_num_rows $
                   }

       self->SET_ID_STRUCT, id_struct


     END ; PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::GUI_DEFINE




     ;+=================================================================
     ;
     ; Here we have the event manager for the XC_GEOMETRY_SPECTRA addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         IF OBJ_VALID(self) THEN BEGIN
           (self.addon).dispatcher->MESSAGE, 'Error handing event.', /ERROR
         ENDIF ELSE BEGIN
           MESSAGE, /REISSUE
         ENDELSE
         RETURN
       ENDIF


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       addon = self.addon


       ; Start main event loop
       CASE event.id OF

         id.ui_field_num_rows: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           addon.param.num_rows = value
         END

         ;id.ui_field_shot: BEGIN
         ;  WIDGET_CONTROL, event.id, GET_VALUE=value
         ;  addon.param.shot = value
         ;END

         ;id.ui_field_system: BEGIN
         ;  WIDGET_CONTROL, event.id, GET_VALUE=value
         ;  addon.param.system = value
         ;END

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::UPDATE
      
       id = self->GET_ID_STRUCT()

       addon = self.addon

       ; Set all the widgets.

       WIDGET_CONTROL, id.ui_field_num_rows, SET_VALUE=addon.param.num_rows

       ;WIDGET_CONTROL, id.ui_field_shot, SET_VALUE=addon.param.shot
       ;WIDGET_CONTROL, id.ui_field_system, SET_VALUE=addon.param.system

       ;WIDGET_CONTROL, id.ui_field_ts_min, SET_VALUE=addon.param.ts_min
       ;WIDGET_CONTROL, id.ui_field_ts_max, SET_VALUE=addon.param.ts_max

       ;WIDGET_CONTROL, id.ui_field_y_min, SET_VALUE=addon.param.y_min
       ;WIDGET_CONTROL, id.ui_field_y_max, SET_VALUE=addon.param.y_max

     END ; PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA::UPDATE




     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA__DEFINE

       struct = { BST_INST_WIDGET_XC_GEOMETRY_SPECTRA $
                           
                  ,addon:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_XC_GEOMETRY_SPECTRA__DEFINE
