
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   An addon to be used for fitting the detector geometry for XICS systems.
;   
;   This addon is meant to be used as part of <XC_GEOMETRY_FIT::>.  All control
;   of the addon will be done through <XC_GEOMETRY_FIT::>.
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_FLAGS

       self.BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.use_tab = 0
       self.use_control = 1

       self.add_to_menu = 1
       self.menu_title = 'SPECTRA: XC Geometry'

       self.conflicting = 'BST_INST_GUIADDON_CERVIEW' $
                          +', BST_INST_GUIADDON_DETECTOR_VIEWER' $
                          +', BST_INST_GUIADDON_USER_SPECTRA'

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_FLAGS



     ;+=========================================================================
     ;
     ; Return a structure with the default parameters for the
     ; USER SPECTRA addon.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::RESET, _NULL=_null
 
           
       self.SET_PARAM, { $
                         num_rows:0L $
                         ,num_geom_param:16L $
                        }


       ; Set the default errors in the measured parameters (the "spectrum").
       ;
       ; Detector location measurement errors:
       ;   2mm in position.
       ;   1 degree in angle.
       ;
       ; Wavelentgth errors:
       ;   0.0001 Angstroms
       ;   This should match the theoretical accuracy and the fitting accuracy.
   
       self.SET_ERRORS, {detector_location:[0.002, 0.002, 0.002] $
                         ,detector_normal_angle:[0.02,0.02] $
                         ,detector_orientation_angle:0.02 $

                         ,crystal_location:[0.002, 0.002, 0.002] $
                         ,crystal_normal_angle:[0.02,0.02] $
                         ,crystal_orientation_angle:0.02 $

                         ,axis_location:0.002 $

                         ,wavelength:0.0001 $
                        }
 
       self.BST_INST_GUIADDON::RESET

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::RESET




     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the XC_GEOMETRY_SPECTRA addon.
     ;
     ; PROGRAMMING NOTES:
     ;   Note here that we do not check if this addon is already
     ;   enabled.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::ENABLE

       ; Call the parent method.
       self.BST_INST_GUIADDON::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       fitter = self.dispatcher.GET_CORE()
       fitter.SET_SPECTRUM_FUNCTION, 'spectrum', self

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::ENABLE



     ;+=========================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::DISABLE

       ; Call the parent method.
       self.BST_INST_GUIADDON::DISABLE

       IF self.IS_ENABLED() THEN BEGIN
         fitter = self.dispatcher.GET_CORE()
         fitter.SET_SPECTRUM_FUNCTION, /DEFAULT
       ENDIF

     END ;PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::DISABLE



     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the state of addon.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_STATE
       
       state = self.BST_INST_GUIADDON::GET_STATE()
       state = CREATE_STRUCT(state, {param:self.param})

       RETURN, state

     END ; FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Load a save structure for this addon.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::LOAD_STATE, state

       self.BST_INST_GUIADDON::LOAD_STATE, state

       STRUCT_ASSIGN, {param:state.param}, self, /NOZERO


     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::LOAD_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_PARAM

       RETURN, self.param

     END ; FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_PARAM



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the addon parameters from a structure.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_PARAM, param

       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_PARAM




     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the errors in the measured parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_ERRORS

       RETURN, self.errors

     END ; FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_ERRORS



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the errors in the measured parameters from a structure.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_ERRORS, errors

       STRUCT_ASSIGN, {errors:errors}, self, /NOZERO

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_ERRORS



; ==============================================================================
; ==============================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ==============================================================================
; ==============================================================================




     ;+=========================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GUI_DEFINE
                          
 
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)


       ; Add a widget to control the addon
       self.REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_XC_GEOMETRY_SPECTRA' $
                             ,self $
                             ,DESTINATION=ui_base)

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GUI_DEFINE
     


     ;+=========================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::EVENT_HANDLER, event


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF 


       ; First call the superclass method
       self.BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)


     END ;PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::EVENT_HANDLER




     ;+=========================================================================
     ;
     ; Update the gui from the current object parameters
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::UPDATE_GUI

       self.widget.UPDATE

     END ;PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::UPDATE_GUI



     ;+=========================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget.DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::REMOVE_WIDGET


; ==============================================================================
; ==============================================================================
; ######################################################################
;
; SPECTRUM METHODS
;    These are the methods that <BST_SPECTRAL_FIT> will actually
;    use to get the spectra.
;
; ######################################################################
; ==============================================================================
; ==============================================================================



     ;+=========================================================================
     ;
     ; Create a set of data points that we will use to fit the location of the 
     ; XICS camera.
     ;
     ; For this calibration we will assume that we are using data from a shot
     ; that has no plasma rotation.
     ;
     ; The data points are as follows:
     ;  *  Measured or design location of the camera.
     ;  *  Theoretical (unshifted) wavelengths of the helium-like Ar lines
     ;     at each row on the detector.
     ;
     ; With no plasma rotation, we would expect that when the detector is 
     ; placed correctly the fit line locations will equal the theoretical line
     ; locations everywhere on the detector.
     ;
     ; In additon we assume that the actuall detector location is not very
     ; far from the measured/design location of the detector.
     ;
     ;
     ;-=========================================================================
     FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SPECTRUM


       ; Establish catch block.
       CATCH, error                   
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, /ERROR
         MESSAGE, STRING('Could not retrieve XC_GEOMETRY_SPECTRA spectrum.') 
       ENDIF


       ; -----------------------------------------------------------------------
       ; First add the theoretical line positions to the "spectrum".

       line_list = self.private.line_list
       num_lines_to_fit = line_list.N_ELEMENTS()


       num_rows = self.param.num_rows
       num_geom_param = self.param.num_geom_param

       num_spectra_points = num_geom_param + num_lines_to_fit * num_rows
       x = INDGEN(num_spectra_points)
       y = DBLARR(num_spectra_points)
       weight = DBLARR(num_spectra_points)


       ; Loop through the lines and add them to the "spectrum".
       FOR ii_line=0, num_lines_to_fit-1 DO BEGIN

         index_start = num_geom_param + ii_line*num_rows
         index_end = index_start + num_rows - 1

         y[index_start:index_end] = line_list[ii_line]
       ENDFOR 


       ; Define line weight (1/stddev^2).
       ; I assume the error in the line wavelength is ~ 0.0001 Angstroms.
       ; This should match the theoretical accuracy and the fitting accuracy.
       line_weight = 1D / (self.errors.wavelength^2)
       weight[num_geom_param:num_spectra_points-1] = line_weight





       ; -----------------------------------------------------------------------
       ; Add the measured detector location.

       g = self.GET_GEOMETRY_OBJECT(MODULE=self.param.module)
       config = g.GET_CONFIG()

       normal_spher = MIR_CV_COORD(FROM_RECT=config.detector_normal, /TO_SPHER)
       normal_theta = normal_spher[1]
       normal_phi = normal_spher[2]

       orientation_ref = CROSSP(config.detector_normal $
                                ,CROSSP([1D, 0D, 0D], config.detector_normal))
       orientation_alpha = MIR_VECTOR_ANGLE(orientation_ref, config.detector_orientation $
                                        ,NORMAL_DIRECTION=config.detector_normal)


       ; The detector location in coordinates of the ideal detector location.
       ; That is by definition 0 here.
       y[0:2] = 0D
       y[3] = normal_theta
       y[4] = normal_phi
       y[5] = orientation_alpha

       ; Set the errors in the measured parameters.
       ; Detector location xyz
       weight[0:2] = 1D / (self.errors.detector_location^2)
       ; Normal angle theta, phi
       weight[3:4] = 1D / (self.errors.detector_normal_angle^2)
       ; Orientaiton angle.
       weight[5] = 1D / (self.errors.detector_orientation_angle^2)



       ; -----------------------------------------------------------------------
       ; Add the measured crystal location.

       normal_spher = MIR_CV_COORD(FROM_RECT=config.crystal_normal, /TO_SPHER)
       normal_theta = normal_spher[1]
       normal_phi = normal_spher[2]

       orientation_ref = CROSSP(config.crystal_normal $
                                ,CROSSP([1D, 0D, 0D], config.crystal_normal))
       orientation_alpha = MIR_VECTOR_ANGLE(orientation_ref, config.crystal_orientation $
                                        ,NORMAL_DIRECTION=config.crystal_normal)

       ; The crystal location in coordinates of the ideal crystal location.
       ; That is by definition 0 here.
       y[6:8] = 0D
       y[9] = normal_theta
       y[10] = normal_phi
       y[11] = orientation_alpha

       ; Set the errors in the measured parameters.
       ; Crystal location xyz
       weight[6:8] = 1D / (self.errors.crystal_location^2)
       ; Normal angle theta, phi
       weight[9:10] = 1D / (self.errors.crystal_normal_angle^2)
       ; Orientaiton angle.
       weight[11] = 1D / (self.errors.crystal_orientation_angle^2)



       ; -----------------------------------------------------------------------
       ; This represents the distance of the central viewing chord to
       ; the magnetic axis.  This value should always be zero.  The distance
       ; is calculated in the model for now.
       y[12] = 0D
       IF self.errors.axis_location NE 0 THEN BEGIN
         weight[12] = 1D / (self.errors.axis_location^2)
       ENDIF ELSE BEGIN
         weight[12] = 0D
       ENDELSE



       ; -----------------------------------------------------------------------
       ; Add the location of the spot from the in-vessel calibration
       y[13:15] = self.param.invessel_spot_location
       IF TOTAL(self.errors.invessel_spot_location^2) NE 0 THEN BEGIN
         weight[13:15] = 1D / (self.errors.invessel_spot_location^2) 
       ENDIF ELSE BEGIN
         weight[13:15] = 0D
       ENDELSE


       ; -----------------------------------------------------------------------
       ; Return the final spectrum.

       spectrum_out = { $
                        y:y $
                        ,x:x $
                        ,weight:weight $
                      }

       RETURN, spectrum_out


     END ; FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SPECTRUM




     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_GEOMETRY_OBJECT, g
       COMPILE_OPT STRICTARR

       self.private.geometry = g

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_GEOMETRY_OBJECT




     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_LINE_LIST, line_list
       COMPILE_OPT STRICTARR

       self.private.line_list = line_list

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::SET_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_GEOMETRY_OBJECT, MODULE_INDEX=module_index
     COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not retrive geometry object.'
       ENDIF

       MIR_DEFAULT, module_index, 0
       
       g = self.private.geometry
       IF ~OBJ_VALID(g) THEN BEGIN
         MESSAGE, 'Geometry object not loaded.'
       ENDIF

       RETURN, g.GET_MODULE_GEOMETRY(module_index)

     END ; FUNCTION BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::GET_GEOMETRY_OBJECT







; ==============================================================================
; ==============================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::CLEANUP

       self.REMOVE_WIDGET
       self.CLEAR_SPECTROMETER_DATA

       self.BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA::CLEANUP


     ;+=========================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA__DEFINE


       struct = { BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA $


                  ,param:{ BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA__PARAM $
                           ,num_rows:0L $
                           ,num_geom_param:0L $
                           ,system:'' $
                           ,shot:0L $
                           ,module:0L $
                           ,row_range:[0L, 0L] $

                           ,invessel_spot_location:[0D, 0D, 0D] $
                         } $
                  

                  ,errors:{BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA__ERRORS $
                           ,detector_location:[0D,0D,0D] $
                           ,detector_normal_angle:[0D,0D] $
                           ,detector_orientation_angle:0D $
                           ,crystal_location:[0D,0D,0D] $
                           ,crystal_normal_angle:[0D,0D] $
                           ,crystal_orientation_angle:0D $
                           ,axis_location:0D $
                           ,wavelength:0D $
                           ,invessel_spot_location:[0D, 0D, 0D] $
                           } $

                  ,private:{ BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA__PRIVATE $
                             ,geometry:OBJ_NEW() $
                             ,line_list:OBJ_NEW() $
                           } $
                  
                  ,widget:OBJ_NEW() $

                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_XC_GEOMETRY_SPECTRA__DEFINE
