
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from a file or a user defined function.
; 
; DESCRIPTION:
;   If the spectrum is to be loaded from a file, then the file should 
;   be an IDL sav file containing a variables named 'spectrum' that
;   contains the structure described below.
;
;   If the spectrum is loded from a user defined function then the 
;   function should return the structure described below.
;
;   spectrum = {y:array [,x:array][,weight:array]}
;
;   This structure should contain the following tags:
;     Tags:
;        y
;
;     Tags (Optional):
;        x 
;        weight
;        wavelength
;
;   Each of the tags should contain an array.
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_AR17::SET_FLAGS

       self.BST_INST_GUIADDON_XCRYSTAL::SET_FLAGS

       self.active = 1
       self.default = 0

       self.use_tab = 1
       self.use_control = 0

       self.add_to_menu = 1
       self.menu_title = 'XICS Ar17'

       self.title = 'XICS Ar17'

       self.required = 'BST_INST_GUIADDON_XCRYSTAL_SPECTRA'

     END ; PRO BST_INST_GUIADDON_XCRYSTAL_AR17::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ; 
     ; DESCRIPTION:
     ;   This is called after all addons have been instantiated.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_AR17::INITIALIZE
       self.BST_INST_GUIADDON_XCRYSTAL::INITIALIZE

       self.model = (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR17')

     END ;PRO BST_INST_GUIADDON_XCRYSTAL_AR17::INITIALIZE



; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_XCRYSTAL_AR17::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_AR17::GUI_DEFINE
                          
 
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Add a widget to control the addon
       self.REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_XCRYSTAL_AR17' $
                             ,self.model $
                             ,DESTINATION=ui_base)


     END ; PRO BST_INST_GUIADDON_XCRYSTAL_AR17::GUI_DEFINE
       



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_AR17__DEFINE

       struct = { BST_INST_GUIADDON_XCRYSTAL_AR17 $

                  ,INHERITS BST_INST_GUIADDON_XCRYSTAL }

     END ; PRO BST_INST_GUIADDON_XCRYSTAL_AR17__DEFINE
