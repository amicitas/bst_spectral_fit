
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT> to retrive specra for the XICS system.
;   For the time being this is specific to the CMOD system.  Eventualy this
;   will be generalized to all XICS systems (CMOD, NSTX, LHD, EAST, KSTAR).
;
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::SET_FLAGS

       self.BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.use_tab = 0
       self.use_control = 1

       self.add_to_menu = 1
       self.menu_title = 'SPECTRA: XCrystal'

       self.conflicting = 'BST_INST_GUIADDON_CERVIEW' $
                          +', BST_INST_GUIADDON_DETECTOR_VIEWER' $
                          +', BST_INST_GUIADDON_USER_SPECTRA'

     END ; PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::SET_FLAGS


; ==============================================================================
; ==============================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ==============================================================================
; ==============================================================================




     ;+=========================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_XCRYSTAL_SPECTRA::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::GUI_DEFINE
                          
       addon = self.dispatcher.GET_ADDON('BST_INST_ADDON_XCRYSTAL_SPECTRA')
       
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Add a widget to control the addon
       self.REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_XCRYSTAL_SPECTRA' $
                             ,addon $
                             ,DESTINATION=ui_base)


     END ; PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::GUI_DEFINE
     


     ;+=========================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::EVENT_HANDLER, event


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF 


       ; First call the superclass method
       self.BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)


     END ;PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::EVENT_HANDLER



     ;+=========================================================================
     ;
     ; Update the gui from the current object parameters
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::UPDATE_GUI

       self.widget.UPDATE

     END ;PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::UPDATE_GUI



     ;+=========================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget.DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA::REMOVE_WIDGET




; ==============================================================================
; ==============================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=========================================================================
     PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA__DEFINE

       struct = { BST_INST_GUIADDON_XCRYSTAL_SPECTRA $
                  ,widget:OBJ_NEW() $
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_XCRYSTAL_SPECTRA__DEFINE

