




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   A widget to control the <BST_INST_MODEL_XCRYSTAL::> model
;  for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_XCRYSTAL::INIT, model $
                                                ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_XCRYSTAL::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_XCRYSTAL::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the <BST_INST_WIDGET_XCRYSTAL::>
     ;   object.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL::GUI_DEFINE, FONT=LabelFont $
                                               ,FRAME=frame $
                                               ,SUB_FRAME=sub_frame $
                                               ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 0
       MIR_DEFAULT, sub_frame, 1


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/COLUMN $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)

       ui_bgroup_use = CW_BGROUP(ui_base $
                                 ,UNAME='ui_bgroup_use' $
                                 ,'Use' $
                                 ,/NONEXCLUSIVE $
                                 ,FRAME=0)

       ; All model controls, except for 'USE' should be in this base.
       ; The 'USE' option will toggle the 
       ui_base_model = WIDGET_BASE(ui_base $
                                   , UNAME='ui_base_model' $
                                   ,/COLUMN $
                                   ,ALIGN_CENTER=align_center $
                                   ,SENSITIVE=0 $
                                   ,FRAME=sub_frame)


       ; Setup the basic widget layout.
       ui_base_options = WIDGET_BASE(ui_base_model $
                                   ,UNAME='ui_base_param' $
                                   ,/COLUMN)

       ui_base_param = WIDGET_BASE(ui_base_model $
                                   ,UNAME='ui_base_param' $
                                   ,/ROW)
       
       ui_base_left = WIDGET_BASE(ui_base_param $
                                  ,UNAME='ui_base_left' $
                                  ,/COLUMN)
       
       ui_base_center = WIDGET_BASE(ui_base_param $
                                   ,UNAME='ui_base_center' $
                                   ,/COLUMN)
       
       ui_base_right = WIDGET_BASE(ui_base_param $
                                   ,UNAME='ui_base_right' $
                                   ,/COLUMN)
       
       



       option_names = ['fix', 'save']
       ui_bgroup_options = CW_BGROUP(ui_base_options, UNAME='ui_bgroup_options' $
                                     ,option_names $
                                     ,/NONEXCLUSIVE $
                                     ,/ROW)

       option_names = ['use Te', 'initialize']
       ui_bgroup_model_options = CW_BGROUP(ui_base_options, UNAME='ui_bgroup_model_options' $
                                           ,option_names $
                                           ,/NONEXCLUSIVE $
                                           ,/ROW)
       
       id_struct = { $
                     ui_bgroup_use:ui_bgroup_use $
                     ,ui_base_model:ui_base_model $
                     ,ui_bgroup_options:ui_bgroup_options $
                     ,ui_bgroup_model_options:ui_bgroup_model_options $
                   }
       



       label_size = 120

       model_param = self.model->GET_PARAM()
       model_param_names = TAG_NAMES(model_param)
       num_model_params = N_TAGS(model_param)

       FOR ii=0,num_model_params-1 DO BEGIN
         name = STRUPCASE(model_param_names[ii])

         CASE 1 OF
           ii LT num_model_params/3: BEGIN
             destination = ui_base_left
           END
           ii LT num_model_params*2/3: BEGIN
             destination = ui_base_center
           END
           ELSE: BEGIN
             destination = ui_base_right
           END         
         ENDCASE

         ui_base = WIDGET_BASE(destination, /ROW)

         ui_field = CW_FIELD_EXT(ui_base $
                                 ,UNAME='UI_FIELD_'+name $
                                 ,UVALUE={type:'CW_FIELD_EXT' $
                                          ,name:name $
                                         } $
                                 ,VALUE='' $
                                 ,TITLE=name $
                                 ,/DOUBLE $
                                 ,/ALL_EVENTS $
                                 ,FORMAT='(f0.4)' $
                                 ,XSIZE=10 $
                                 ,LABEL_XSIZE=label_size)

         ui_bgroup = CW_BGROUP(ui_base $
                               ,UNAME='UI_BGROUP_'+name $
                               ,UVALUE={type:'CW_BGROUP' $
                                        ,name:name $
                                       } $
                               ,'fix' $
                               ,/NONEXCLUSIVE)
         

         id_struct = CREATE_STRUCT(id_struct $
                                   ,'UI_FIELD_'+name $
                                   ,ui_field $
                                   ,'UI_BGROUP_'+name $
                                   ,ui_bgroup $
                                  )

       ENDFOR

       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_XCRYSTAL::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event handler for this widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         PRINT_MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN



       id = self->GET_ID_STRUCT()



       event_handled = 0


       ; Start main event loop
       CASE event.id OF


         id.ui_bgroup_use: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=use
           self.model->SET_USE, use[0]
           self->SET_USE, use[0], /LEAVE_BUTTON
           event_handled = 1
         END


         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_options, GET_VALUE=value_bgroup_options

           ; Deal with freezing/unfreezing parameters
           self.model->SET_FIXALL, value_bgroup_options[0]

           ; Deal with saving/unsaving parameters
           self.model->SET_SAVE, value_bgroup_options[1]
           event_handled = 1
         END


         id.ui_bgroup_model_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_model_options, GET_VALUE=value_bgroup_model_options

           ; Use the electron temperature constraint.
           self.model->SET_OPTIONS, {use_te:value_bgroup_model_options[0] $
                                     ,initialize_model:value_bgroup_model_options[1]}

           event_handled = 1
         END



         ELSE:

       ENDCASE

       IF event_handled THEN RETURN





       uname = WIDGET_INFO(event.id, /UNAME)
       WIDGET_CONTROL, event.id, GET_UVALUE=uvalue


       model_param_names = TAG_NAMES(self.model->GET_PARAM())
       w = WHERE(model_param_names EQ uvalue.name, count)
       IF count EQ 0 THEN BEGIN
         MESSAGE, 'Parameter not found in model: ' + param_name
       ENDIF

      

       CASE uvalue.type OF

         ; Button event
         'CW_FIELD_EXT': BEGIN
           struct = CREATE_STRUCT(uvalue.name, event.value)
           self.model->SET_PARAM, struct
           event_handled = 1
         END


         ; Text event
         'CW_BGROUP': BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=fix
           struct = CREATE_STRUCT(uvalue.name, fix[0])
           self.model->SET_FIXED, strucT
           event_handled = 1
         END

         ELSE:
       ENDCASE

       IF event_handled THEN RETURN
      


       MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)



     END ;PRO BST_INST_WIDGET_XCRYSTAL::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL::UPDATE
       
       
       id = self->GET_ID_STRUCT()

       self->SET_USE, self.model->USE()

       param = self.model->GET_PARAM()
       options = self.model->GET_OPTIONS()
       fixed = self.model->GET_FIXED()



       ; Update the general model control options.
       bg_group_value = [self.model->GET_FIXALL() $
                         ,self.model->SAVE() $
                        ]
       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=bg_group_value



       ; Update specific model options.
       WIDGET_CONTROL, id.ui_bgroup_model_options, SET_VALUE=[options.use_te, options.initialize_model]



       ; Update parameter values.
       gui_id_names = TAG_NAMES(id)

       model_param_names = TAG_NAMES(param)
       num_model_params = N_TAGS(param)


       FOR ii=0,num_model_params-1 DO BEGIN
         name = model_param_names[ii]

         w = WHERE(gui_id_names EQ 'UI_FIELD_'+name)
         WIDGET_CONTROL, id.(w[0]), SET_VALUE=param.(ii), SENSITIVE=1

         w = WHERE(gui_id_names EQ 'UI_BGROUP_'+name)
         WIDGET_CONTROL, id.(w[0]), SET_VALUE=[fixed.(ii)], SENSITIVE=1

       ENDFOR


     END ; PRO BST_INST_WIDGET_XCRYSTAL::UPDATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL::SET_USE, use, LEAVE_BUTTON=leave_button
       
       id = self->GET_ID_STRUCT()


       ; This keyword is useful when handing button events.
       IF ~ KEYWORD_SET(leave_button) THEN BEGIN
         WIDGET_CONTROL, id.ui_bgroup_use, SET_VALUE = [use]
       ENDIF

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base_model, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_XCRYSTAL::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_XCRYSTAL__DEFINE

       struct = { BST_INST_WIDGET_XCRYSTAL $
                           
                  ,model:OBJ_NEW() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_XCRYSTAL__DEFINE
