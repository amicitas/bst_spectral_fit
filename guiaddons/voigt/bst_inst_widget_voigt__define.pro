




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A widget to contral VOIGT models for <BST_SPECTRAL_FIT>
; 
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_VOIGT::INIT, model $
                                       ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_VOIGT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_VOIGT::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_WIDGET_VOIGT::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT::GUI_DEFINE, COLUMN=Column $
                                               ,ROW=Row $
                                               ,FONT=LabelFont $
                                               ,FRAME=frame $
                                               ,SUB_FRAME=sub_frame $
                                               ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 0 
       MIR_DEFAULT, sub_frame, 1

       IF ~ (KEYWORD_SET(row) XOR KEYWORD_SET(column)) THEN BEGIN
         row = 1
         column = 0
       ENDIF


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/ROW $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)


       ui_bgroup_use = CW_BGROUP(ui_base, UNAME='ui_bgroup_use' $
                                 ,' ' $
                                 ,/NONEXCLUSIVE $
                                 ,FRAME=0)

       ui_base_sub = WIDGET_BASE(ui_base, UNAME='ui_base_sub' $
                                 ,COLUMN=column $
                                 ,ROW=row $
                                 ,ALIGN_CENTER=align_center $
                                 ,SENSITIVE=0 $
                                 ,FRAME=sub_frame)

         ui_field_intensity = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_intensity' $
                                           ,VALUE='' $
                                           ,TITLE='Intensity' $
                                           ,/DOUBLE $
                                           ,/ALL_EVENTS $
                                           ,FORMAT='(f0.3)' $
                                           ,XSIZE=6)
         ui_field_loc = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_loc' $
                                     ,VALUE='' $
                                     ,TITLE='Loc' $
                                     ,/DOUBLE $
                                     ,/ALL_EVENTS $
                                     ,FORMAT='(f0.3)' $
                                     ,XSIZE=6)
         ui_field_sigma = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_sigma' $
                                       ,VALUE='' $
                                       ,TITLE='Sigma' $
                                       ,/DOUBLE $
                                       ,/ALL_EVENTS $
                                       ,FORMAT='(f0.3)' $
                                       ,XSIZE=6)
         ui_field_gamma = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_gamma' $
                                       ,VALUE='' $
                                       ,TITLE='Gamma' $
                                       ,/DOUBLE $
                                       ,/ALL_EVENTS $
                                       ,FORMAT='(f0.3)' $
                                       ,XSIZE=6)



         option_names = ['fix', 'save']
         ui_bgroup_options = CW_BGROUP(ui_base_sub, UNAME='ui_bgroup_options' $
                                       ,option_names $
                                       ,/NONEXCLUSIVE $
                                       ,ROW=row $
                                       ,COLUMN=column)

       id_struct = { $
                     ui_bgroup_use:ui_bgroup_use $
                     ,ui_base_sub:ui_base_sub $
                     ,ui_field_intensity:ui_field_intensity $
                     ,ui_field_loc:ui_field_loc $
                     ,ui_field_sigma:ui_field_sigma $
                     ,ui_field_gamma:ui_field_gamma $
                     ,ui_bgroup_options:ui_bgroup_options $
                     }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_VOIGT::GUI_DEFINE


    



     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF

         id.ui_bgroup_use: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_use, GET_VALUE=use

           self.model->SET_USE, use[0]

           self->SET_USE, use[0], /LEAVE_BUTTON
         END


         id.ui_field_intensity: BEGIN
           self.model->SET_PARAM, {intensity:event.value}
         END


         id.ui_field_loc: BEGIN
           self.model->SET_PARAM, {location:event.value}
         END


         id.ui_field_sigma: BEGIN
           self.model->SET_PARAM, {sigma:event.value}
         END


         id.ui_field_gamma: BEGIN
           self.model->SET_PARAM, {gamma:event.value}
         END


         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_options, GET_VALUE=value_bgroup_options

           ; Deal with freezing/unfreezing parameters
           self.model->SET_FIXALL, value_bgroup_options[0]

           ; Deal with saving/unsaving parameters
           self.model->SET_SAVE, value_bgroup_options[1]
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_VOIGT::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT::UPDATE
       
       id = self->GET_ID_STRUCT()

       self->SET_USE, self.model->USE()

       bg_group_value = [self.model->GET_FIXALL(), self.model->SAVE()]
       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=bg_group_value

       param = self.model->GET_PARAM()

       WIDGET_CONTROL, id.ui_field_intensity, SET_VALUE=param.intensity
       WIDGET_CONTROL, id.ui_field_loc, SET_VALUE=param.location
       WIDGET_CONTROL, id.ui_field_sigma, SET_VALUE=param.sigma
       WIDGET_CONTROL, id.ui_field_gamma, SET_VALUE=param.gamma


     END ; PRO BST_INST_WIDGET_VOIGT::UPDATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT::SET_USE, use, LEAVE_BUTTON=leave_button
       
       id = self->GET_ID_STRUCT()

       ; This keyword is useful when handing button events.
       IF ~ KEYWORD_SET(leave_button) THEN BEGIN
         WIDGET_CONTROL, id.ui_bgroup_use, SET_VALUE = [use]
       ENDIF

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base_sub, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_VOIGT::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT__DEFINE

       struct = { BST_INST_WIDGET_VOIGT $
                           
                  ,model:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_VOIGT__DEFINE
