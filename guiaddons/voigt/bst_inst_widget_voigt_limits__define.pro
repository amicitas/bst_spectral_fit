




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;   A widget to control the limits of the parameter in VOIGT models 
;   for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_VOIGT_LIMITS::INIT, model $
                                                   ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_VOIGT_LIMITS::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_VOIGT_LIMITS::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_WIDGET_VOIGT_LIMITS::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS::GUI_DEFINE, COLUMN=Column $
                                                      ,ROW=Row $
                                                      ,FONT=LabelFont $
                                                      ;,FRAME=frame $
                                                      ,SUB_FRAME=sub_frame $
                                                      ,ALIGN_CENTER=align_center

       ; Set the defaults
       ;MIR_DEFAULT, frame, 0 
       MIR_DEFAULT, sub_frame, 1

       IF ~ (KEYWORD_SET(row) XOR KEYWORD_SET(column)) THEN BEGIN
         row = 1
         column = 0
       ENDIF


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/ROW $
                             ,FRAME=1 $
                             ,ALIGN_CENTER=align_center)


       ui_intensity_label = WIDGET_LABEL(ui_base, VALUE='Intensity')

       ui_intensity_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_intensity_fix = CW_BGROUP(ui_intensity_base, UNAME='ui_intensity_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix intensity' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_intensity_limits_low = CW_FIELD_EXT(ui_intensity_base, UNAME='ui_intensity_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_intensity_limits_high = CW_FIELD_EXT(ui_intensity_base, UNAME='ui_intensity_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)


       ui_loc_label = WIDGET_LABEL(ui_base, VALUE=' Loc')


       ui_loc_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_loc_fix = CW_BGROUP(ui_loc_base, UNAME='ui_loc_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix location' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_loc_limits_low = CW_FIELD_EXT(ui_loc_base, UNAME='ui_loc_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_loc_limits_high = CW_FIELD_EXT(ui_loc_base, UNAME='ui_loc_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)


       ui_sigma_label = WIDGET_LABEL(ui_base, VALUE=' Sigma')

       ui_sigma_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_sigma_fix = CW_BGROUP(ui_sigma_base, UNAME='ui_sigma_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix sigma' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_sigma_limits_low = CW_FIELD_EXT(ui_sigma_base, UNAME='ui_sigma_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_sigma_limits_high = CW_FIELD_EXT(ui_sigma_base, UNAME='ui_sigma_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)


       ui_gamma_label = WIDGET_LABEL(ui_base, VALUE=' Gamma')

       ui_gamma_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_gamma_fix = CW_BGROUP(ui_gamma_base, UNAME='ui_gamma_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix gamma' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_gamma_limits_low = CW_FIELD_EXT(ui_gamma_base, UNAME='ui_gamma_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_gamma_limits_high = CW_FIELD_EXT(ui_gamma_base, UNAME='ui_gamma_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)




       id_struct = { $
                     ui_base:ui_base $

                     ,ui_intensity_fix:ui_intensity_fix $
                     ,ui_intensity_limits_low:ui_intensity_limits_low $
                     ,ui_intensity_limits_high:ui_intensity_limits_high $

                     ,ui_loc_fix:ui_loc_fix $
                     ,ui_loc_limits_low:ui_loc_limits_low $
                     ,ui_loc_limits_high:ui_loc_limits_high $

                     ,ui_sigma_fix:ui_sigma_fix $
                     ,ui_sigma_limits_low:ui_sigma_limits_low $
                     ,ui_sigma_limits_high:ui_sigma_limits_high $

                     ,ui_gamma_fix:ui_gamma_fix $
                     ,ui_gamma_limits_low:ui_gamma_limits_low $
                     ,ui_gamma_limits_high:ui_gamma_limits_high $
                   }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_VOIGT_LIMITS::GUI_DEFINE


    



     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
       ENDIF  

       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       

       ; Start main event loop
       CASE event.id OF

         id.ui_intensity_fix: BEGIN
           WIDGET_CONTROL, id.ui_intensity_fix, GET_VALUE=intensity_fix
           self.model->SET_FIXED, {intensity:intensity_fix[0]}
         END

         id.ui_intensity_limits_low: BEGIN
           self->SET_LIMITS, /INTENSITY
         END
         id.ui_intensity_limits_high: BEGIN
           self->SET_LIMITS, /INTENSITY
         END

         id.ui_loc_fix: BEGIN
           WIDGET_CONTROL, id.ui_loc_fix, GET_VALUE=loc_fix
           self.model->SET_FIXED, {location:loc_fix[0]}
         END
         id.ui_loc_limits_low: BEGIN
           self->SET_LIMITS, /LOCATION
         END
         id.ui_loc_limits_high: BEGIN
           self->SET_LIMITS, /LOCATION
         END

         id.ui_sigma_fix: BEGIN
           WIDGET_CONTROL, id.ui_sigma_fix, GET_VALUE=sigma_fix
           self.model->SET_FIXED, {sigma:sigma_fix[0]}
         END
         id.ui_sigma_limits_low: BEGIN
           self->SET_LIMITS, /SIGMA
         END
         id.ui_sigma_limits_high: BEGIN
           self->SET_LIMITS, /SIGMA
         END

         id.ui_gamma_fix: BEGIN
           WIDGET_CONTROL, id.ui_gamma_fix, GET_VALUE=gamma_fix
           self.model->SET_FIXED, {gamma:gamma_fix[0]}
         END
         id.ui_gamma_limits_low: BEGIN
           self->SET_LIMITS, /GAMMA
         END
         id.ui_gamma_limits_high: BEGIN
           self->SET_LIMITS, /GAMMA
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_VOIGT_LIMITS::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS::UPDATE
       
       id = self->GET_ID_STRUCT()

       self->SET_USE, self.model->USE()

       limited = self.model->GET_LIMITED()
       limits = self.model->GET_LIMITS()
       fixed = self.model->GET_FIXED()

       WIDGET_CONTROL, id.ui_intensity_fix, SET_VALUE=[fixed.intensity]
       WIDGET_CONTROL, id.ui_loc_fix, SET_VALUE=[fixed.location]
       WIDGET_CONTROL, id.ui_sigma_fix, SET_VALUE=[fixed.gamma]
       WIDGET_CONTROL, id.ui_gamma_fix, SET_VALUE=[fixed.gamma]



       IF ~ limited.intensity[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.intensity[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_intensity_limits_low, SET_VALUE=limits_temp

       IF ~ limited.intensity[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.intensity[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_intensity_limits_high, SET_VALUE=limits_temp



       IF ~ limited.location[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.location[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_loc_limits_low, SET_VALUE=limits_temp

       IF ~ limited.location[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.location[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_loc_limits_high, SET_VALUE=limits_temp


       IF ~ limited.sigma[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.sigma[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_sigma_limits_low, SET_VALUE=limits_temp

       IF ~ limited.sigma[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.sigma[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_sigma_limits_high, SET_VALUE=limits_temp


       IF ~ limited.gamma[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.gamma[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_gamma_limits_low, SET_VALUE=limits_temp

       IF ~ limited.gamma[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.gamma[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_gamma_limits_high, SET_VALUE=limits_temp



     END ; PRO BST_INST_WIDGET_VOIGT_LIMITS::UPDATE


     ;+=================================================================
     ; PURPOSE:
     ;   Set the limits in the model from the gui
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS::SET_LIMITS, INTENSITY=intensity $
                                                   ,LOCATION=loc $
                                                   ,SIGMA=sigma $
                                                   ,GAMMA=gamma       

       num_keywords = KEYWORD_SET(intensity) + KEYWORD_SET(sigma) + KEYWORD_SET(loc) 
       IF num_keywords NE 1 THEN BEGIN
         MESSAGE, 'Programming error: One (and only one) keyword must be set.'
       ENDIF


       id = self->GET_ID_STRUCT()

       limited = self.model->GET_LIMITED()
       limits = self.model->GET_LIMITS()

       CASE 1 OF
         KEYWORD_SET(intensity):  BEGIN
           id_low = id.ui_intensity_limits_low
           id_high = id.ui_intensity_limits_high
         END
         KEYWORD_SET(loc): BEGIN
           id_low = id.ui_loc_limits_low
           id_high = id.ui_loc_limits_high
         END
         KEYWORD_SET(sigma): BEGIN
           id_low = id.ui_sigma_limits_low
           id_high = id.ui_sigma_limits_high
         END
         KEYWORD_SET(gamma): BEGIN
           id_low = id.ui_gamma_limits_low
           id_high = id.ui_gamma_limits_high
         END
       ENDCASE

       WIDGET_CONTROL, id_low, GET_VALUE=value_low
       WIDGET_CONTROL, id_high, GET_VALUE=value_high

       value = [value_low, value_high]
       limited_temp = (value NE '')
       limits_temp = DOUBLE(value)

       CASE 1 OF
         KEYWORD_SET(intensity):  BEGIN
           limited.intensity = limited_temp
           limits.intensity = limits_temp
         END
         KEYWORD_SET(loc):  BEGIN
           limited.location = limited_temp
           limits.location = limits_temp
         END
         KEYWORD_SET(sigma):  BEGIN
           limited.sigma = limited_temp
           limits.sigma = limits_temp
         END
         KEYWORD_SET(gamma):  BEGIN
           limited.gamma = limited_temp
           limits.gamma = limits_temp
         END
       ENDCASE


       self.model->SET_LIMITED, limited
       self.model->SET_LIMITS, limits

     END ; PRO BST_INST_WIDGET_VOIGT_LIMITS::SET_LIMITS


     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS::SET_USE, use
       
       id = self->GET_ID_STRUCT()

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_VOIGT_LIMITS::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a derived class of <WIDGET_OBJECT::>
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_VOIGT_LIMITS__DEFINE

       struct = { BST_INST_WIDGET_VOIGT_LIMITS $
                           
                  ,model:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_VOIGT_LIMITS__DEFINE
