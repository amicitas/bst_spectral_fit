


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the VOIGT  models.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_VOIGT_LIMITS::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 0

       self.use_tab = 1
       self.use_control = 0

       self.add_to_menu = 1
       self.menu_title = 'MODEL: Voigt'

       self.title = 'Voigt Limits'

       self.required = 'BST_INST_GUIADDON_VOIGT'

     END ; PRO BST_INST_GUIADDON_VOIGT_LIMITS::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_VOIGT_LIMITS::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON::INIT(dispatcher)

       self.voigt = OBJ_NEW('SIMPLELIST')

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_VOIGT_LIMITS::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the 
     ;   <BST_INST_GUIADDON_VOIGT_LIMITS::> addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_VOIGT_LIMITS::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ui_base_voigt = WIDGET_BASE(ui_base $
                                      ,/COLUMN $
                                      ,Y_SCROLL_SIZE=400 $
                                      ,X_SCROLL_SIZE=600)

       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()


       ; ---------------------------------------------------------------
       ; First deal with the voigts.
       self->REMOVE_VOIGT
       
       ; Get a reference to the multi voigt model.
       model_voigt = fitter_core.models->GET_MODEL('BST_INST_MODEL_VOIGT_MULTI')
       
       num_voigt = model_voigt->NUM_MODELS()
       FOR ii=0,num_voigt-1 DO BEGIN
         self.voigt->APPEND, OBJ_NEW('BST_INST_WIDGET_VOIGT_LIMITS' $
                                        ,model_voigt->GET_MODEL(ii) $
                                        ,DESTINATION=ui_base_voigt)
       ENDFOR


     END ;PRO BST_INST_GUIADDON_VOIGT_LIMITS::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_VOIGT_LIMITS::UPDATE_GUI

       FOR ii=0,self.voigt->N_ELEMENTS()-1 DO BEGIN
         (self.voigt->GET(ii))->UPDATE
       ENDFOR

     END ; PRO BST_INST_GUIADDON_VOIGT_LIMITS::UPDATE_GUI

    


     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_VOIGT_LIMITS::CLEANUP
       
       IF OBJ_VALID(self.voigt) THEN BEGIN
         self->REMOVE_VOIGT
         self.voigt->DESTROY
       ENDIF

       self->BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_VOIGT_LIMITS::CLEANUP

    

     ;+=================================================================
     ; PURPOSE:
     ;   Remove all voigt widgets.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_VOIGT_LIMITS::REMOVE_VOIGT
       
       IF OBJ_VALID(self.voigt) THEN BEGIN
         FOR ii=0,self.voigt->N_ELEMENTS()-1 DO BEGIN
           voigt = self.voigt->GET(ii)
           IF OBJ_VALID(voigt) THEN BEGIN
             voigt->DESTROY
           ENDIF
         ENDFOR

         self.voigt->REMOVE, /ALL
       ENDIF

     END ; PRO BST_INST_GUIADDON_VOIGT_LIMITS::REMOVE_VOIGT



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_VOIGT_LIMITS__DEFINE

       struct = { BST_INST_GUIADDON_VOIGT_LIMITS $

                  ,voigt:OBJ_NEW() $
                           
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_VOIGT_LIMITS__DEFINE
