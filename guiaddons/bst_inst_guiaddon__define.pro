

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   A base class for GUI based addons for <BST_SPECTRAL_FIT>
;
;   Here a base class is provided that can be inherited to build
;   addons with a simple GUI based interface and a simple set of
;   parameters.
;
;   By using this class much of the basic functionality of a simple
;   addon is automatically handled. In particular this base class
;   has everything necessary to handle:
;
;     Enabling and Disabling the addon.
;       Will also do simple confilct/requirement checking.
;
;     Setting up a base widget.
;       The widget can either be embedded in the control area or a tab
;       of the main gui, or built as a separate window.  This can
;       be controlled using the use_tab and use_control properties.
;
;     Saving and Loading the addon state.
;     
; DESCRIPTION:
;   This base class is meant to be subclassed in order to create
;   simple gui addons.
;
;   
;   If the addon that is being built has the following requirements,
;   then this base class can be used with minimal changes:
;
;     1.  The addon has a set of parameters that can be held
;         in a single structure within the object definiton.
;         Saveing and loading of the addon state will amount to
;         saving and loading of that parameter structure.
;
;     2.  The addon has a gui than can be built within one base widget.
;
;
;   More complicated addons can also use this base class, but will
;   require more reimplementation.  A more primitive base class,
;   [BST_INST_GUIADDON] (which [BST_INST_GUIADDON] inherits) can
;   be used if this class does not do things the way you would like.
;
;   
; SUBCLASSING:
;   In order to create an addon using this base class a number of
;   methods will need to be reimplemented.
;
;   The best way to get started is to follow along with one of the
;   existing addons.  I would suggest using [BST_INST_USER_SPECTRA] as
;   a good template.
;
;   Object Definition:
;     Any subclasses will need to defined with an additonal property:
;       param
;
;     param should be a structure containing all of the parameters
;     necessary for the addon.  This param structre will be used for
;     saving and loading of the addon state.
;
;   
;  Method Reimplementation:
;     A number of methods will need to re implemented in order
;     to build an addon using this base class.
;
;     SET_FLAGS
;       The flags for the addon, as well as the addon title
;       menu title, conflicts and reqirements should be set here.
;
;       This should first call the superclass method.
;
;       Here is a list of the flags that can be set and their defaults:
;
;       self.active = bool
;           default = 0
;           If not set to 1, then this addon will be ignored.
;
;       self.menu_title = 'Addon Name'
;           default = ''
;           This will be used in the addon menu as well as for
;           the tab or window title (if applicable).
;
;       self.conflicting = string
;       self.required = string
;           default = ''
;           Any conficting or required addons can be specified useing
;           using these properties.  Here 'string' must be a
;           comma or space separated list of addon names.
;
;       self.use_tab = bool
;       self.use_control = bool
;       self.use_window = bool
;           default = 0
;           These two properties control the destination for the gui.
;           Only one of these flags may be set at a time.
;           If all of these are set to 0 then this addon will not be
;           give a gui. If use_tab == 1 then the gui will be built
;           in a new tab.  If use_control == 1 then the gui will be 
;           built in the control area, just above the PLOT and
;           ANALYZE buttons.  If use_window == 1 then the addon will
;           be opened in a new window.
;
;       self.title = string
;           default = ''
;           The title for the addon window or tab.
;
;       self.default = bool
;           default = 0
;           If this is set to true then the addon will be loaded
;           with [BST_INSTRUMENTAL_FIT_GUI] is first started.
;
;       self.internal = bool
;           default = 0
;           If set the addon will be automatically loaded and
;           cannot be disabled.  No menu item will be added.
;
;
;     INIT()
;       (optional)
;       This should first call the superclass method.
;
;       This should take any action needed when the addon object
;       is created.  If there are initialization action that
;       require the presence of other addon object, then the
;       INITIALIZE method can be be used.
;
;
;     ENABLE
;       (optional)
;       This should first call the superclass method.
;
;       Take any spectial action when the addon is enabled.
;
;       Note: This method can get called even if the addon
;             is already active.
;
;     DISABLE
;       (optional)
;       This should first call the superclass method.
;
;       Take any spectial action when the addon is disabled.
;
;       Note: This method can get called even if the addon
;             is already inactive.
;
;     UPDATE
;       (optional)
;       This should first call the superclass method.
;      
;       This routine will be called after the addon is enabled,
;       and anytime the addon state is loaded or reset.
;
;       This base class will always call the UPDATE_GUI method.
;
;       If other parts of the addon need to be updated that
;       can be done here.  Most of time reimplementaiton is
;       not necessary.
;       
;     GUI_DEFINE
;       (optional)
;
;       This method should build the gui for the addon.
;
;       The gui should be built within the base widget that is saved
;       in self.gui_id.  This base widget already takes care of setting
;       the event handler.  The base widget (self.gui_param.gui_id) 
;       should not be modified.  In particular the uvalue of the base 
;       widget must not be changed.
;
;
;       A good stratagy for this method is to first create a new
;       widget base:
;         ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)
;
;       The rest of the gui can then be built inside this
;       new base widget.  This widget can always be accessed
;       by using:
;         ui_base = WIDGET_INFO(self.gui_param.gui_id, /CHILD)
;
;       A structure with the gui ids can be saved and retrieved
;       using self->SET_ID_STRUCT and self->GET_ID_STRUCT()
;
;
;     EVENT_HANDLER
;       (optional)
;       This should first call the superclass method.
;
;       This method is the event handler for the addon gui.
;       This method should first call the superclass method, that
;       will take care of any kill events and handle the events
;       from the addon menu of the main gui.
;
;       The keyword EVENT_HANDLED should be used with the superclass
;       method to see if it handled the event.  If so this method should
;       simply return. It is important to check this since the
;       superclass method can disable the addon destroy the gui.
;
;     CLEANUP_HANDLER
;       (optional)
;       This should call the superclass method last before returning
;
;       This can be used to take any cleanup actions needed when
;       the widget is destroyed.
;
;     UPDATE_GUI
;       (optional)
;       
;       This method should be used to update the addon gui with the
;       current addon parameters.  It will be called after the addon
;       is enabled and anytime the addon state is loaded or reset.
;     
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::SET_FLAGS

       self->BST_INST_DISPATCHED::SET_FLAGS

       self.add_to_menu = 0

       self.use_tab = 0
       self.use_control = 0
       self.use_window = 0

       self.title = OBJ_CLASS(self)
       self.menu_title = OBJ_CLASS(self)

     END ; PRO BST_INST_GUIADDON::SET_FLAGS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the [BST_INST_GUIADDON] object.
     ;
     ;   This will be called when the component object is created.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON::INIT, dispatcher $
                                        ,DESTROY_OBJECT_WITH_GUI=destroy_object_with_gui $
                                        ,_REF_EXTRA=extra

       MIR_DEFAULT, destroy_object_with_gui, 0

       ; Call the INIT methods for the inherited classes.
       status = self->BST_INST_DISPATCHED::INIT(dispatcher)
       IF status NE 1 THEN MESSAGE, 'BST_INST_DISPATCHED::INIT failed.'

       status = self->WIDGET_OBJECT::INIT(DESTROY_OBJECT_WITH_GUI=destroy_object_with_gui $
                                          ,_STRICT_EXTRA=extra)
       IF status NE 1 THEN MESSAGE, 'WIDGET_OBJECT::INIT failed.'

       RETURN, 1

     END ;FUNCTION BST_INST_GUIADDON::INIT



     ;+=================================================================
     ;
     ; Return the 'param' property for this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON::GET_PARAM

       RETURN, self.param

     END ; FUNCTION BST_INST_GUIADDON::GET_PARAM



     ;+=================================================================
     ;
     ; Initialize the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::ENABLE
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Could not enable the '+OBJ_CLASS(self)+' addon.', /ERROR
         self->DISABLE
         RETURN
       ENDIF

       IF ~ self->IS_ENABLED() THEN BEGIN
         ; Call the parent method.
         self->BST_INST_DISPATCHED::ENABLE

         ; Build the gui.
         self->BUILD_GUI
       ENDIF
   
       ; Update everything now that we have enabled something new.
       self.dispatcher.UPDATE_FITTER

     END ; PRO BST_INST_GUIADDON::ENABLE



     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_GUIADDON::DISABLE

       ; Call the parent method.
       self->BST_INST_DISPATCHED::DISABLE

       ; Destroy all widgets
       IF WIDGET_INFO(self.gui_param.gui_id, /VALID_ID) THEN BEGIN
         WIDGET_CONTROL, self.gui_param.gui_id, /DESTROY
       ENDIF
   
       ; Update everything now that we have disabled something.
       ; Check for existance so this still is ok during shutdown.
       IF ISA(dispatcher) THEN BEGIN
         self.dispatcher.UPDATE_FITTER
       ENDIF
       

     END ;PRO BST_INST_GUIADDON::DISABLE



     ;+=================================================================
     ;
     ; Update the addon with the current settings.
     ;
     ; This will generally be done either after loading or setting
     ; defaults.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::UPDATE
      
       self->BST_INST_DISPATCHED::UPDATE

       self->UPDATE_GUI

     END ; PRO BST_INST_GUIADDON::UPDATE



; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI ROUTINES.
;   Here we build the GUI for the addon, if needed.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================

     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui for the addons.
     ; 
     ;-=================================================================
     PRO BST_INST_GUIADDON::BUILD_GUI

       fitter_gui = self.dispatcher->GET_GUI()

       ; Turn off GUI updating.
       fitter_gui->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       ; First choose the destination.
       CASE 1 OF
         self.use_tab: BEGIN
           destination = fitter_gui->GET_GUI_ID(/BASE_TAB)
         END
         self.use_control: BEGIN
           destination = fitter_gui->GET_GUI_ID(/BASE_CONTROL)
         END
         self.use_window: BEGIN
           group_leader = fitter_gui->GET_GUI_ID(/BASE_MAIN)
         END
         ELSE:
       END

       ; Check if any of the gui flags were set.
       ; If so we want to build a new GUI component.
       ; Otherwise just call GUI_DEFINE incase the addon just needs
       ; to modifiy existing widgets.
       IF self.use_tab OR self.use_control OR self.use_window THEN BEGIN
         self->GUI_INIT, DESTINATION=destination $
                         ,TITLE=self.title $
                         ,GROUP_LEADER=group_leader
       ENDIF ELSE BEGIN
         self->GUI_DEFINE
       ENDELSE


       ; Restore the update state
       fitter_gui->SET_GUI_UPDATE, update_old

     END ;BST_INST_GUIADDON::BUILD_GUI


     ;+=================================================================
     ; PURPOSE:
     ;   This method is used by derived classes to build their
     ;   GUIs.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::GUI_DEFINE
       
       ; This method should be reimplemented by any subclasses.

     END ; PRO BST_INST_GUIADDON::GUI_DEFINE


     ;+=================================================================
     ;
     ; This is the main event handler for the BST_INST_GUIADDON.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::EVENT_HANDLER, event $
                                            ,EVENT_HANDLED=event_handled

       ; First pass the event to the parent classes.
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       

     END ; PRO BST_INST_GUIADDON::EVENT_HANDLER



     ;+=================================================================
     ;
     ; Exit BST_INST_GUIADDON
     ;
     ; Note: This will only be used if the gui was setup in a
     ;       separate window.
     ;-=================================================================
     PRO BST_INST_GUIADDON::GUI_KILL

       self->DISABLE

     END ;BST_INST_GUIADDON::GUI_KILL




     ;+=================================================================
     ;
     ; Update the gui with the current param values.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::UPDATE_GUI
       
       ; This method should be reimplemented by any subclasses.

     END ; PRO BST_INST_GUIADDON::UPDATE_GUI



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on object destruction object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON::CLEANUP

       ; Call the CLEANP method of the base GUI classes.

       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->BST_INST_DISPATCHED::CLEANUP
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'BST_INST_DISPATCHED::CLEANUP failed.', /ERROR
       ENDELSE
       

       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->WIDGET_OBJECT::CLEANUP
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'WIDGET_OBJECT::CLEANUP failed.', /ERROR
       ENDELSE      


     END ;PRO BST_INST_GUIADDON::CLEANUP


     ;+=================================================================
     ; PUPOSE:
     ;   Define the [BST_INST_GUIADDON] class.
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON__DEFINE

       struct = { BST_INST_GUIADDON $

                  ,use_tab:0 $
                  ,use_control:0 $
                  ,use_window:0 $

                  ,INHERITS BST_INST_DISPATCHED $
                  ,INHERITS WIDGET_OBJECT}

     END ; PRO BST_INST_GUIADDON__DEFINE
