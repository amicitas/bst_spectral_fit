


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the SCALEDIAN models.
;
; WARNING:
;   This is very broken right now.
;   This should control BST_INST_ADDON_PLOT.  This is simple to
;   implement, but I have not done it yet . . .  (novi 2016-02-08)
; 
;
;-======================================================================




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT ROUTINES
;   Routines to control the object and object data.
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_MENU_GUIADDON::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 0
       self.use_control = 0
       self.use_window = 0

       self.title = 'Addons Menu'

       self.menu_ids = MIR_HASH()
       

     END ; PRO BST_INST_GUIADDON_MENU_GUIADDON::SET_FLAGS


     
; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI ROUTINES
;   Routines to control the plotting GUI.
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui.
     ;
     ; DESCRIPTION:
     ;   Since all that this addon does is to control some menu 
     ;   options, we reimplement the BUILD_GUI method to just
     ;   create the menu options.
     ;-=================================================================
     PRO BST_INST_GUIADDON_MENU_GUIADDON::BUILD_GUI

       fitter_gui = self.dispatcher->GET_GUI()

       ; Turn off GUI updating.
       fitter_gui->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       self->GUI_DEFINE

       ; Restore the update state
       fitter_gui->SET_GUI_UPDATE, update_old


     END ; PRO BST_INST_GUIADDON_MENU_GUIADDON::BUILD_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_MENU_GUIADDON::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_MENU_GUIADDON::GUI_DEFINE
       fitter_gui = self.dispatcher.GET_GUI()
       addon_disp = fitter_gui.GET_GUIADDON_DISPATCHER()

       ; ---------------------------------------------------------------
       ; Set up menu for this addon.
       ui_menu_guiaddon =  fitter_gui.GET_GUI_ID(/MENU_GUIADDONS)

       objects = addon_disp.GET_OBJECTS()

       ; Loop over all of the objects.
       FOR ii=0,objects->NUM_ENTRIES()-1 DO BEGIN
         object = objects->GET_BY_INDEX(ii, key)
         
         IF ~object.add_to_menu THEN CONTINUE
         
         name = STRLOWCASE(object.NAME())
         menu_title = object.menu_title
         IF menu_title EQ '' THEN BEGIN
           menu_title = name
         ENDIF
         
         ui_button = WIDGET_BUTTON(ui_menu_guiaddon $
                                   ,UNAME='ui_button_'+name $
                                   ,VALUE=menu_title $
                                   ,/CHECKED_MENU $
                                   ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                   ,UVALUE=self $
                                  )

         self.menu_ids[name] = ui_button
         
       ENDFOR

     END ;PRO BST_INST_GUIADDON_MENU_GUIADDON::GUI_DEFINE




     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the 
     ;   plot options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_MENU_GUIADDON::UPDATE_GUI
       fitter_gui = self.dispatcher.GET_GUI()
       addon_disp = fitter_gui.GET_GUIADDON_DISPATCHER()
       
       FOREACH id, self.menu_ids, name DO BEGIN
         addon = addon_disp.GET_GUIADDON(name)
         WIDGET_CONTROL, id, SET_BUTTON=addon.enabled
       ENDFOREACH

     END ; PRO BST_INST_GUIADDON_MENU_GUIADDON::UPDATE_GUI




     ;+=================================================================
     ;
     ; Here we have the event manager for this addon.
     ;
     ;-=================================================================
PRO BST_INST_GUIADDON_MENU_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
  
       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       fitter_gui = self.dispatcher.GET_GUI()
       addon_disp = fitter_gui.GET_GUIADDON_DISPATCHER()
       
       FOREACH id, self.menu_ids, name DO BEGIN
         IF event.id NE id THEN CONTINUE

         event_handled = 1
         addon = addon_disp.GET_GUIADDON(name)
         
         value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
         WIDGET_CONTROL, event.id, SET_BUTTON=value

         IF value THEN BEGIN
           addon.ENABLE
         ENDIF ELSE BEGIN
           addon.DISABLE
         ENDELSE

         BREAK
       ENDFOREACH

       IF ~event_handled THEN BEGIN
         MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
       ENDIF


     END ;PRO BST_INST_GUIADDON_MENU_GUIADDON::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_MENU_GUIADDON__DEFINE

       struct = { BST_INST_GUIADDON_MENU_GUIADDON $

                  ,menu_ids:OBJ_NEW() $
       
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_MENU_GUIADDON__DEFINE
