
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the SCALEDIAN models.
;
; WARNING:
;   This is very broken right now.
;   This should control BST_INST_ADDON_PLOT.  This is simple to
;   implement, but I have not done it yet . . .  (novi 2016-02-08)
; 
;
;-======================================================================




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT ROUTINES
;   Routines to control the object and object data.
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_PLOT::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 0
       self.use_control = 0
       self.use_window = 0

       self.title = 'Plot Options'

     END ; PRO BST_INST_GUIADDON_PLOT::SET_FLAGS

     
; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI ROUTINES
;   Routines to control the plotting GUI.
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui.
     ;
     ; DESCRIPTION:
     ;   Since all that this addon does is to control some menu 
     ;   options, we reimplement the BUILD_GUI method to just
     ;   create the menu options.
     ;-=================================================================
     PRO BST_INST_GUIADDON_PLOT::BUILD_GUI

MIR_LOGGER, /DEBUG, ''
MIR_LOGGER, /DEBUG, 'BST_INST_GUIADDON_PLOT::BUILD_GUI'
MIR_LOGGER, /DEBUG, ''

       fitter_gui = self.dispatcher->GET_GUI()

       ; Turn off GUI updating.
       fitter_gui->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       self->GUI_DEFINE

       ; Restore the update state
       fitter_gui->SET_GUI_UPDATE, update_old


     END ; PRO BST_INST_GUIADDON_PLOT::BUILD_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_PLOT::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_PLOT::GUI_DEFINE

MIR_LOGGER, /DEBUG, ''
MIR_LOGGER, /DEBUG, 'BST_INST_GUIADDON_PLOT::GUI_DEFINE'
MIR_LOGGER, /DEBUG, ''

       ; ---------------------------------------------------------------
       ; Set up menu for this addon.
       fitter_gui = self.dispatcher->GET_GUI()
       ui_menu_plot =  fitter_gui->GET_GUI_ID(/MENU_PLOT)

       ui_button_plot_vs_wavelength = WIDGET_BUTTON(ui_menu_plot $
                                                    ,UNAME='ui_button_plot_vs_wavelength' $
                                                    ,VALUE='Plot vs. wavelength' $
                                                    ,/CHECKED_MENU $
                                                    ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                    ,UVALUE=self $
                                                    ,/SEPARATOR $
                                                   )

       ui_button_plot_overall_fit = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_overall_fit' $
                                                  ,VALUE='Plot overall fit' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_residual = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_residual' $
                                                  ,VALUE='Plot residual' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_background = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_background' $
                                                  ,VALUE='Plot background' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_profiles = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_profiles' $
                                                  ,VALUE='Plot profiles' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_gaussians = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_gaussians' $
                                                  ,VALUE='Plot gaussians' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_profile_locations = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_profile_locations' $
                                                  ,VALUE='Plot profile locations' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_models = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_models' $
                                                  ,VALUE='Plot models' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )

       ui_button_plot_addons = WIDGET_BUTTON(ui_menu_plot $
                                                  ,UNAME='ui_button_plot_addons' $
                                                  ,VALUE='Plot addons' $
                                                  ,/CHECKED_MENU $
                                                  ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                  ,UVALUE=self $
                                                 )


       ui_button_weighted_residual = WIDGET_BUTTON(ui_menu_plot $
                                                   ,UNAME='ui_button_weighted_residual' $
                                                   ,VALUE='Weighted residual' $
                                                   ,/CHECKED_MENU $
                                                   ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                   ,UVALUE=self $
                                                   ,/SEPARATOR $
                                                 )


       ; Store all of the accesable widgit id's here.
       id_struct = { $
                     ui_button_plot_vs_wavelength:ui_button_plot_vs_wavelength $
                     ,ui_button_plot_overall_fit:ui_button_plot_overall_fit $
                     ,ui_button_plot_residual:ui_button_plot_residual $
                     ,ui_button_plot_background:ui_button_plot_background $
                     ,ui_button_plot_profiles:ui_button_plot_profiles $
                     ,ui_button_plot_gaussians:ui_button_plot_gaussians $
                     ,ui_button_plot_profile_locations:ui_button_plot_profile_locations $
                     ,ui_button_plot_models:ui_button_plot_models $
                     ,ui_button_plot_addons:ui_button_plot_addons $
                     ,ui_button_weighted_residual:ui_button_weighted_residual $
                   }

       ; Save the id structure.
       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_GUIADDON_PLOT::GUI_DEFINE




     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the 
     ;   plot options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_PLOT::UPDATE_GUI
                          
       addon = self.dispatcher.GET_ADDON('BST_INST_ADDON_PLOT')
       
       id = self->GET_ID_STRUCT()

       WIDGET_CONTROL, id.ui_button_plot_vs_wavelength, SET_BUTTON=addon.options.plot_vs_wavelength
       WIDGET_CONTROL, id.ui_button_plot_overall_fit, SET_BUTTON=addon.options.plot_overall_fit
       WIDGET_CONTROL, id.ui_button_plot_residual, SET_BUTTON=addon.options.plot_residual
       WIDGET_CONTROL, id.ui_button_plot_background, SET_BUTTON=addon.options.plot_background
       WIDGET_CONTROL, id.ui_button_plot_profiles, SET_BUTTON=addon.options.plot_profiles
       WIDGET_CONTROL, id.ui_button_plot_gaussians, SET_BUTTON=addon.options.plot_gaussians
       WIDGET_CONTROL, id.ui_button_plot_models, SET_BUTTON=addon.options.plot_models
       WIDGET_CONTROL, id.ui_button_plot_addons, SET_BUTTON=addon.options.plot_addons
       WIDGET_CONTROL, id.ui_button_plot_profile_locations, SET_BUTTON=addon.options.plot_profile_locations

       WIDGET_CONTROL, id.ui_button_weighted_residual, SET_BUTTON=addon.options.weighted_residual

     END ; PRO BST_INST_GUIADDON_PLOT::UPDATE_GUI




     ;+=================================================================
     ;
     ; Here we have the event manager for this addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_PLOT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self->BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()
                          
       addon = self.dispatcher.GET_ADDON('BST_INST_ADDON_PLOT')
       
       ; Start main event loop
       CASE event.id OF

         id.ui_button_plot_vs_wavelength: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_vs_wavelength:value}
         END

         id.ui_button_plot_overall_fit: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_overall_fit:value}
         END

         id.ui_button_plot_residual: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_residual:value}
         END

         id.ui_button_plot_background: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_background:value}
         END

         id.ui_button_plot_profiles: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_profiles:value}
         END

         id.ui_button_plot_gaussians: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_gaussians:value}
         END

         id.ui_button_plot_profile_locations: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_profile_locations:value}
         END

         id.ui_button_plot_models: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_models:value}
         END

         id.ui_button_plot_addons: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {plot_addons:value}
         END

         id.ui_button_weighted_residual: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           addon.SET_OPTIONS, {weighted_residual:value}
         END

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_PLOT::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_PLOT__DEFINE

       struct = { BST_INST_GUIADDON_PLOT $

                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_PLOT__DEFINE
