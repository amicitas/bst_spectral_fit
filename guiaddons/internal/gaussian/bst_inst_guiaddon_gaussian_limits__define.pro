


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the GAUSSIAN and BACKGROUND models.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 1
       self.use_control = 0

       self.title = 'Gaussian Limits'

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_GAUSSIAN_LIMITS::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON::INIT(dispatcher)

       self.gaussian = OBJ_NEW('SIMPLELIST')

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_GAUSSIAN_LIMITS::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_GAUSSIAN_LIMITS::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ui_base_gaussian = WIDGET_BASE(ui_base $
                                      ,/COLUMN $
                                      ,Y_SCROLL_SIZE=400 $
                                      ,X_SCROLL_SIZE=600)

       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()


       ; ---------------------------------------------------------------
       ; First deal with the gaussians.
       self->REMOVE_GAUSSIAN
       
       ; Get a reference to the multi gaussian model.
       model_gauss = fitter_core.models->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')
       
       num_gauss = model_gauss->NUM_MODELS()
       FOR ii=0,num_gauss-1 DO BEGIN
         self.gaussian->APPEND, OBJ_NEW('BST_INST_WIDGET_GAUSSIAN_LIMITS' $
                                        ,model_gauss->GET_MODEL(ii) $
                                        ,DESTINATION=ui_base_gaussian)
       ENDFOR


     END ;PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::UPDATE_GUI

       FOR ii=0,self.gaussian->N_ELEMENTS()-1 DO BEGIN
         (self.gaussian->GET(ii))->UPDATE
       ENDFOR

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::UPDATE_GUI

    


     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::CLEANUP
       
       IF OBJ_VALID(self.gaussian) THEN BEGIN
         self->REMOVE_GAUSSIAN
         self.gaussian->DESTROY
       ENDIF

       self->BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::CLEANUP

    

     ;+=================================================================
     ; PURPOSE:
     ;   Remove all gaussian widgets.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::REMOVE_GAUSSIAN
       
       IF OBJ_VALID(self.gaussian) THEN BEGIN
         FOR ii=0,self.gaussian->N_ELEMENTS()-1 DO BEGIN
           gaussian = self.gaussian->GET(ii)
           IF OBJ_VALID(gaussian) THEN BEGIN
             gaussian->DESTROY
           ENDIF
         ENDFOR

         self.gaussian->REMOVE, /ALL
       ENDIF

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS::REMOVE_GAUSSIAN



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS__DEFINE

       struct = { BST_INST_GUIADDON_GAUSSIAN_LIMITS $

                  ,gaussian:OBJ_NEW() $
                           
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_LIMITS__DEFINE
