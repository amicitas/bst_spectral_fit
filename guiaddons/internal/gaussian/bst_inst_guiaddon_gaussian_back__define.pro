


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the GAUSSIAN and BACKGROUND models.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 1
       self.use_control = 0

       self.title = 'Gaussian & Background'

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_BACK::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_GAUSSIAN_BACK::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON::INIT(dispatcher)

       self.gaussian = OBJ_NEW('SIMPLELIST')

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_GAUSSIAN_BACK::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_GAUSSIAN_BACK::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ui_base_gaussian = WIDGET_BASE(ui_base $
                                      ,/COLUMN $
                                      ,Y_SCROLL_SIZE=400 $
                                      ,X_SCROLL_SIZE=600)
       ui_base_back = WIDGET_BASE(ui_base, /COLUMN, FRAME=1)

       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()


       ; ---------------------------------------------------------------
       ; First deal with the gaussians.
       self->REMOVE_GAUSSIAN
       
       ; Get a reference to the multi gaussian model.
       model_gauss = fitter_core.models->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')
       
       num_gauss = model_gauss->NUM_MODELS()
       FOR ii=0,num_gauss-1 DO BEGIN
         self.gaussian->APPEND, OBJ_NEW('BST_INST_WIDGET_GAUSSIAN' $
                                        ,model_gauss->GET_MODEL(ii) $
                                        ,ii+1 $
                                        ,DESTINATION=ui_base_gaussian)
       ENDFOR


       ; ---------------------------------------------------------------
       ; Set up the background widget
       ui_label_back = WIDGET_LABEL(ui_base_back $
                                    ,VALUE='Background' $
                                    ,/ALIGN_LEFT)
       self->REMOVE_BACKGROUND

       ; Get a reference to the multi gaussian model.
       model_back = fitter_core.models->GET_MODEL('BST_INST_MODEL_BACKGROUND')
       
       self.background = OBJ_NEW('BST_INST_WIDGET_BACKGROUND' $
                                 ,model_back $
                                 ,DESTINATION=ui_base_back)


     END ;PRO BST_INST_GUIADDON_GAUSSIAN_BACK::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK::UPDATE_GUI

       FOR ii=0,self.gaussian->N_ELEMENTS()-1 DO BEGIN
         (self.gaussian->GET(ii))->UPDATE
       ENDFOR

       self.background->UPDATE

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_BACK::UPDATE_GUI

    


     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK::CLEANUP
       
       IF OBJ_VALID(self.gaussian) THEN BEGIN
         self->REMOVE_GAUSSIAN
         self.gaussian->DESTROY
       ENDIF

       IF OBJ_VALID(self.background) THEN BEGIN
         self->REMOVE_BACKGROUND
       ENDIF

       self->BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_BACK::CLEANUP

    


     ;+=================================================================
     ; PURPOSE:
     ;   Remove all gaussian widgets.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK::REMOVE_GAUSSIAN
       
       IF OBJ_VALID(self.gaussian) THEN BEGIN
         FOR ii=0,self.gaussian->N_ELEMENTS()-1 DO BEGIN
           gaussian = self.gaussian->GET(ii)
           IF OBJ_VALID(gaussian) THEN BEGIN
             gaussian->DESTROY
           ENDIF
         ENDFOR

         self.gaussian->REMOVE, /ALL
       ENDIF

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_BACK::REMOVE_GAUSSIAN




     ;+=================================================================
     ; PURPOSE:
     ;   Remove the background widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK::REMOVE_BACKGROUND
       
       IF OBJ_VALID(self.background) THEN BEGIN
         self.background->DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_BACK::REMOVE_BACKGROUND




     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_GAUSSIAN_BACK__DEFINE

       struct = { BST_INST_GUIADDON_GAUSSIAN_BACK $

                  ,gaussian:OBJ_NEW() $

                  ,background:OBJ_NEW() $

                           
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_GAUSSIAN_BACK__DEFINE
