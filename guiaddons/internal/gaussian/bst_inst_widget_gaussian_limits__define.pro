




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;   A widget to control the limits of the parameter in GAUSSIAN models 
;   for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_GAUSSIAN_LIMITS::INIT, model $
                                                     ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_GAUSSIAN_LIMITS::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_WIDGET_GAUSSIAN_LIMITS::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::GUI_DEFINE, COLUMN=Column $
                                                      ,ROW=Row $
                                                      ,FONT=LabelFont $
                                                      ;,FRAME=frame $
                                                      ,SUB_FRAME=sub_frame $
                                                      ,ALIGN_CENTER=align_center

       ; Set the defaults
       ;MIR_DEFAULT, frame, 0 
       MIR_DEFAULT, sub_frame, 1

       IF ~ (KEYWORD_SET(row) XOR KEYWORD_SET(column)) THEN BEGIN
         row = 1
         column = 0
       ENDIF


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/ROW $
                             ,FRAME=1 $
                             ,ALIGN_CENTER=align_center)


       ui_amp_label = WIDGET_LABEL(ui_base, VALUE='Amp')

       ui_amp_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_amp_fix = CW_BGROUP(ui_amp_base, UNAME='ui_amp_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix amplitude' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_amp_limits_low = CW_FIELD_EXT(ui_amp_base, UNAME='ui_amp_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_amp_limits_high = CW_FIELD_EXT(ui_amp_base, UNAME='ui_amp_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)



       ui_wid_label = WIDGET_LABEL(ui_base, VALUE=' Wid')

       ui_wid_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_wid_fix = CW_BGROUP(ui_wid_base, UNAME='ui_wid_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix width' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_wid_limits_low = CW_FIELD_EXT(ui_wid_base, UNAME='ui_wid_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_wid_limits_high = CW_FIELD_EXT(ui_wid_base, UNAME='ui_wid_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)


       ui_loc_label = WIDGET_LABEL(ui_base, VALUE=' Loc')


       ui_loc_base = WIDGET_BASE(ui_base $
                                 ,/ROW $
                                 ,FRAME=1 $
                                 ,XPAD=0 $
                                 ,YPAD=0)
       ui_loc_fix = CW_BGROUP(ui_loc_base, UNAME='ui_loc_fix' $
                              ,' ' $
                              ,/NONEXCLUSIVE $
                              ;,TOOLTIP='Fix location' $
                             )
       ; Temporarily hold these as string type untill I find a better solution.
       ui_loc_limits_low = CW_FIELD_EXT(ui_loc_base, UNAME='ui_loc_limits_low' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/GET_STRING $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6 $
                                        ,XPAD=0)
       
       ui_loc_limits_high = CW_FIELD_EXT(ui_loc_base, UNAME='ui_loc_limits_high' $
                                         ,VALUE='' $
                                         ,TITLE='' $
                                         ,/DOUBLE $
                                         ,/GET_STRING $
                                         ,/ALL_EVENTS $
                                         ,FORMAT='(f0.3)' $
                                         ,XSIZE=6 $
                                         ,XPAD=0)


       id_struct = { $
                     ui_base:ui_base $

                     ,ui_amp_fix:ui_amp_fix $
                     ,ui_amp_limits_low:ui_amp_limits_low $
                     ,ui_amp_limits_high:ui_amp_limits_high $

                     ,ui_wid_fix:ui_wid_fix $
                     ,ui_wid_limits_low:ui_wid_limits_low $
                     ,ui_wid_limits_high:ui_wid_limits_high $

                     ,ui_loc_fix:ui_loc_fix $
                     ,ui_loc_limits_low:ui_loc_limits_low $
                     ,ui_loc_limits_high:ui_loc_limits_high $
                   }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::GUI_DEFINE


    



     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
       ENDIF  

       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       

       ; Start main event loop
       CASE event.id OF

         id.ui_amp_fix: BEGIN
           WIDGET_CONTROL, id.ui_amp_fix, GET_VALUE=amp_fix
           self.model->SET_FIXED, {amplitude:amp_fix[0]}
         END

         id.ui_amp_limits_low: BEGIN
           self->SET_LIMITS, /AMPLITUDE
         END
         id.ui_amp_limits_high: BEGIN
           self->SET_LIMITS, /AMPLITUDE
         END


         id.ui_wid_fix: BEGIN
           WIDGET_CONTROL, id.ui_wid_fix, GET_VALUE=wid_fix
           self.model->SET_FIXED, {width:wid_fix[0]}
         END
         id.ui_wid_limits_low: BEGIN
           self->SET_LIMITS, /WIDTH
         END
         id.ui_wid_limits_high: BEGIN
           self->SET_LIMITS, /WIDTH
         END


         id.ui_loc_fix: BEGIN
           WIDGET_CONTROL, id.ui_loc_fix, GET_VALUE=loc_fix
           self.model->SET_FIXED, {location:loc_fix[0]}
         END
         id.ui_loc_limits_low: BEGIN
           self->SET_LIMITS, /LOCATION
         END
         id.ui_loc_limits_high: BEGIN
           self->SET_LIMITS, /LOCATION
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::UPDATE
       
       id = self->GET_ID_STRUCT()

       self->SET_USE, self.model->USE()

       limited = self.model->GET_LIMITED()
       limits = self.model->GET_LIMITS()
       fixed = self.model->GET_FIXED()


       WIDGET_CONTROL, id.ui_amp_fix, SET_VALUE=[fixed.amplitude]
       WIDGET_CONTROL, id.ui_wid_fix, SET_VALUE=[fixed.width]
       WIDGET_CONTROL, id.ui_loc_fix, SET_VALUE=[fixed.location]


       IF ~ limited.amplitude[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.amplitude[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_amp_limits_low, SET_VALUE=limits_temp

       IF ~ limited.amplitude[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.amplitude[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_amp_limits_high, SET_VALUE=limits_temp


       IF ~ limited.width[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.width[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_wid_limits_low, SET_VALUE=limits_temp

       IF ~ limited.width[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.width[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_wid_limits_high, SET_VALUE=limits_temp


       IF ~ limited.location[0] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.location[0]
       ENDELSE
       WIDGET_CONTROL, id.ui_loc_limits_low, SET_VALUE=limits_temp

       IF ~ limited.location[1] THEN BEGIN
         limits_temp = ''
       ENDIF ELSE BEGIN
         limits_temp = limits.location[1]
       ENDELSE
       WIDGET_CONTROL, id.ui_loc_limits_high, SET_VALUE=limits_temp




     END ; PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::UPDATE


     ;+=================================================================
     ; PURPOSE:
     ;   Set the limits in the model from the gui
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::SET_LIMITS, AMPLITUDE=amp $
                                                      ,WIDTH=wid $
                                                      ,LOCATION=loc
       

       num_keywords = KEYWORD_SET(amp) + KEYWORD_SET(wid) + KEYWORD_SET(loc) 
       IF num_keywords NE 1 THEN BEGIN
         MESSAGE, 'Programming error: One (and only one) keyword must be set.'
       ENDIF


       id = self->GET_ID_STRUCT()

       limited = self.model->GET_LIMITED()
       limits = self.model->GET_LIMITS()

       CASE 1 OF
         KEYWORD_SET(amp):  BEGIN
           id_low = id.ui_amp_limits_low
           id_high = id.ui_amp_limits_high
         END
         KEYWORD_SET(loc): BEGIN
           id_low = id.ui_loc_limits_low
           id_high = id.ui_loc_limits_high
         END
         KEYWORD_SET(wid): BEGIN
           id_low = id.ui_wid_limits_low
           id_high = id.ui_wid_limits_high
         END
       ENDCASE

       WIDGET_CONTROL, id_low, GET_VALUE=value_low
       WIDGET_CONTROL, id_high, GET_VALUE=value_high

       value = [value_low, value_high]
       limited_temp = (value NE '')
       limits_temp = DOUBLE(value)

       CASE 1 OF
         KEYWORD_SET(amp):  BEGIN
           limited.amplitude = limited_temp
           limits.amplitude = limits_temp
         END
         KEYWORD_SET(wid):  BEGIN
           limited.width = limited_temp
           limits.width = limits_temp
         END
         KEYWORD_SET(loc):  BEGIN
           limited.location = limited_temp
           limits.location = limits_temp
         END
       ENDCASE


       self.model->SET_LIMITED, limited
       self.model->SET_LIMITS, limits

     END ; PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::SET_LIMITS


     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::SET_USE, use
       
       id = self->GET_ID_STRUCT()

       ; Grey out everything if this gauss peak is not in use
       WIDGET_CONTROL, id.ui_base, SENSITIVE=use

     END ;PRO BST_INST_WIDGET_GAUSSIAN_LIMITS::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a derived class of <WIDGET_OBJECT::>
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_GAUSSIAN_LIMITS__DEFINE

       struct = { BST_INST_WIDGET_GAUSSIAN_LIMITS $
                           
                  ,model:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_GAUSSIAN_LIMITS__DEFINE
