


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the SCALEDIAN models.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_SCALED::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 1
       self.use_control = 0

       self.title = 'Scaled'

     END ; PRO BST_INST_GUIADDON_SCALED::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_SCALED::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON::INIT(dispatcher)

       self.scaled = OBJ_NEW('SIMPLELIST')

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_SCALED::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_SCALED::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_SCALED::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /ROW)

       ui_base_scaled = WIDGET_BASE(ui_base, /COLUMN)

       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()


       ; ---------------------------------------------------------------
       ; First deal with the scaleds.
       self->REMOVE_SCALED
       
       ; Get a reference to the multi scaled model.
       model_scaled = fitter_core.models->GET_MODEL('BST_INST_MODEL_SCALED_MULTI')
       
       num_scaled = model_scaled->NUM_MODELS()
       FOR ii=0,num_scaled-1 DO BEGIN
         self.scaled->APPEND, OBJ_NEW('BST_INST_WIDGET_SCALED' $
                                      ,model_scaled->GET_MODEL(ii) $
                                      ,DESTINATION=ui_base_scaled)
       ENDFOR

     END ;PRO BST_INST_GUIADDON_SCALED::GUI_DEFINE



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_SCALED::UPDATE_GUI

       FOR ii=0,self.scaled->N_ELEMENTS()-1 DO BEGIN
         (self.scaled->GET(ii))->UPDATE
       ENDFOR

     END ; PRO BST_INST_GUIADDON_SCALED::UPDATE_GUI

    

     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_SCALED::CLEANUP
       
       IF OBJ_VALID(self.scaled) THEN BEGIN
         self->REMOVE_SCALED
         self.scaled->DESTROY
       ENDIF

       self->BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_SCALED::CLEANUP

    

     ;+=================================================================
     ; PURPOSE:
     ;   Remove all scaled widgets.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_SCALED::REMOVE_SCALED
       
       IF OBJ_VALID(self.scaled) THEN BEGIN
         FOR ii=0,self.scaled->N_ELEMENTS()-1 DO BEGIN
           scaled = self.scaled->GET(ii)
           IF OBJ_VALID(scaled) THEN BEGIN
             scaled->DESTROY
           ENDIF
         ENDFOR

         self.scaled->REMOVE, /ALL
       ENDIF

     END ; PRO BST_INST_GUIADDON_SCALED::REMOVE_SCALED



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_SCALED__DEFINE

       struct = { BST_INST_GUIADDON_SCALED $

                  ,scaled:OBJ_NEW() $

                           
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_SCALED__DEFINE
