




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A widget to contral GAUSSIAN models for <BST_SPECTRAL_FIT>
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_BACKGROUND::INIT, model $
                                                ,_REF_EXTRA=extra
       
       status = self->WIDGET_OBJECT::INIT(_EXTRA=extra)

       self->SET_MODEL, model

       self->GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_BACKGROUND::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BACKGROUND::SET_MODEL, model
       
       MIR_DEFAULT, model, OBJ_NEW()

       self.model =  model

     END ; PRO BST_INST_WIDGET_BACKGROUND::SET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_WIDGET_BACKGROUND::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BACKGROUND::GUI_DEFINE, COLUMN=Column $
                                                 ,ROW=Row $
                                                 ,FONT=LabelFont $
                                                 ,FRAME=frame $
                                                 ,SUB_FRAME=sub_frame $
                                                 ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 0
       MIR_DEFAULT, sub_frame, 1

       IF ~ (KEYWORD_SET(row) XOR KEYWORD_SET(column)) THEN BEGIN
         row = 1
         column = 0
       ENDIF


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/ROW $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)

       label_num_back = ['0','1','2','3']
       ui_base_combo =WIDGET_BASE(ui_base, ALIGN_CENTER=align_center)
         ui_combo_num_back = WIDGET_COMBOBOX(ui_base_combo,  UNAME='ui_combo_num_back' $
                                             ,VALUE=label_num_back $
                                             ,FRAME=0 $
                                             ,xsize=40)

       ui_base_sub = WIDGET_BASE(ui_base, UNAME='ui_base_sub' $
                                 ,COLUMN=column $
                                 ,ROW=row $
                                 ,ALIGN_CENTER=align_center $
                                 ,SENSITIVE=0 $
                                 ,FRAME=sub_frame)

         ui_field_back_0 = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_back_0' $
                                        ,VALUE='' $
                                        ,TITLE='Background' $
                                        ,/DOUBLE $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6)
         ui_field_back_1 = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_back_1' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6)
         ui_field_back_2 = CW_FIELD_EXT(ui_base_sub, UNAME='ui_field_back_2' $
                                        ,VALUE='' $
                                        ,TITLE='' $
                                        ,/DOUBLE $
                                        ,/ALL_EVENTS $
                                        ,FORMAT='(f0.3)' $
                                        ,XSIZE=6)



         option_names = ['fix', 'save']
         ui_bgroup_options = CW_BGROUP(ui_base_sub, UNAME='ui_bgroup_options' $
                                       ,option_names $
                                       ,/NONEXCLUSIVE $
                                       ,ROW=row $
                                       ,COLUMN=column)
       id_struct = { $
                     ui_combo_num_back:ui_combo_num_back $
                     ,ui_base_sub:ui_base_sub $
                     ,ui_field_back_0:ui_field_back_0 $
                     ,ui_field_back_1:ui_field_back_1 $
                     ,ui_field_back_2:ui_field_back_2 $
                     ,ui_bgroup_options:ui_bgroup_options $
                     }


       self->SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_BACKGROUND::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event manager for the CERVIEW addon.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BACKGROUND::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         (self.model).dispatcher->MESSAGE, 'Error handing event.', /ERROR
       ENDIF  


       ; First call the superclass method
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self->GET_ID_STRUCT()

       ; Start main event loop
       CASE event.id OF


         id.ui_combo_num_back: BEGIN
           self.model->SET_NUM_BACK, event.index
           self->SET_NUM_BACK, event.index, /LEAVE_BUTTON
         END


         id.ui_field_back_0: BEGIN
           param = self.model->GET_PARAM()
           param.back[0] = event.value
           self.model->SET_PARAM, param
         END


         id.ui_field_back_1: BEGIN
           param = self.model->GET_PARAM()
           param.back[1] = event.value
           self.model->SET_PARAM, param
         END


         id.ui_field_back_2: BEGIN
           param = self.model->GET_PARAM()
           param.back[2] = event.value
           self.model->SET_PARAM, param
         END


         id.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, id.ui_bgroup_options, GET_VALUE=value_bgroup_options

           ; Deal with freezing/unfreezing parameters
           self.model->SET_FIXALL, value_bgroup_options[0]

           ; Deal with saving/unsaving parameters
           self.model->SET_SAVE, value_bgroup_options[1]
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_WIDGET_BACKGROUND::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BACKGROUND::UPDATE
       
       id = self->GET_ID_STRUCT()

       num_back = self.model->GET_NUM_BACK()
       self->SET_NUM_BACK, num_back

       bg_group_value = [self.model->GET_FIXALL(), self.model->SAVE()]
       WIDGET_CONTROL, id.ui_bgroup_options, SET_VALUE=bg_group_value

       param = self.model->GET_PARAM()
       WIDGET_CONTROL, id.ui_field_back_0, SET_VALUE=param.back[0]
       WIDGET_CONTROL, id.ui_field_back_1, SET_VALUE=param.back[1]
       WIDGET_CONTROL, id.ui_field_back_2, SET_VALUE=param.back[2]

     END ; PRO BST_INST_WIDGET_BACKGROUND::UPDATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the use state of the widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BACKGROUND::SET_NUM_BACK, num_back, LEAVE_BUTTON=leave_button
       
       id = self->GET_ID_STRUCT()

       ; Grey out everything if num_back = 0
       WIDGET_CONTROL, id.ui_base_sub, SENSITIVE=(num_back GT 0)

       IF ~ KEYWORD_SET(leave_button) THEN BEGIN
         WIDGET_CONTROL, id.ui_combo_num_back, SET_COMBOBOX_SELECT=num_back
       ENDIF

       WIDGET_CONTROL, id.ui_field_back_0, SENSITIVE=(num_back GE 1)
       WIDGET_CONTROL, id.ui_field_back_1, SENSITIVE=(num_back GE 2)
       WIDGET_CONTROL, id.ui_field_back_2, SENSITIVE=(num_back GE 3)

     END ;PRO BST_INST_WIDGET_BACKGROUND::SET_NUM_BACK



     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_BACKGROUND__DEFINE

       struct = { BST_INST_WIDGET_BACKGROUND $
                           
                  ,model:obj_new() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_BACKGROUND__DEFINE
