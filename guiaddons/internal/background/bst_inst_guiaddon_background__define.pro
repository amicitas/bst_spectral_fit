


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the GAUSSIAN and BACKGROUND models.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BACKGROUND::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.use_tab = 1
       self.use_control = 0

       self.title = 'Background'

     END ; PRO BST_INST_GUIADDON_BACKGROUND::SET_FLAGS


     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_BACKGROUND::] 
     ;   component.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BACKGROUND::GUI_DEFINE

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ui_base_back = WIDGET_BASE(ui_base, /COLUMN, FRAME=1)

       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()


       ; ---------------------------------------------------------------
       ; Set up the background widget
       ui_label_back = WIDGET_LABEL(ui_base_back $
                                    ,VALUE='Background' $
                                    ,/ALIGN_LEFT)
       self->REMOVE_WIDGET

       ; Get a reference to the multi gaussian model.
       model_back = fitter_core.models->GET_MODEL('BST_INST_MODEL_BACKGROUND')
       
       self.widget = OBJ_NEW('BST_INST_WIDGET_BACKGROUND' $
                                 ,model_back $
                                 ,DESTINATION=ui_base_back)


     END ;PRO BST_INST_GUIADDON_BACKGROUND::GUI_DEFINE


     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BACKGROUND::UPDATE_GUI

       IF ~self.enabled THEN RETURN
       
       IF ISA(self.widget) THEN BEGIN
         self.widget->UPDATE
       ENDIF

     END ; PRO BST_INST_GUIADDON_BACKGROUND::UPDATE_GUI


     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BACKGROUND::CLEANUP

       IF OBJ_VALID(self.widget) THEN BEGIN
         self->REMOVE_WIDGET
       ENDIF

       self->BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_BACKGROUND::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Remove the background widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BACKGROUND::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget->DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_BACKGROUND::REMOVE_WIDGET




     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_BACKGROUND__DEFINE

       struct = { BST_INST_GUIADDON_BACKGROUND $

                  ,widget:OBJ_NEW() $
                           
                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_BACKGROUND__DEFINE
