
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from a file or a user defined function.
; 
; DESCRIPTION:
;   If the spectrum is to be loaded from a file, then the file should 
;   be an IDL sav file containing a variables named 'spectrum' that
;   contains the structure described below.
;
;   If the spectrum is loded from a user defined function then the 
;   function should return the structure described below.
;
;   spectrum = {y:array [,x:array][,weight:array]}
;
;   This structure should contain the following tags:
;     Tags:
;        y
;
;     Tags (Optional):
;        x 
;        weight
;        wavelength
;
;   Each of the tags should contain an array.
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::SET_FLAGS

       self->BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.use_tab = 0
       self.use_control = 1

       self.add_to_menu = 1
       self.menu_title = 'SPECTRA: User defined'

       self.conflicting = 'BST_INST_GUIADDON_CERVIEW, BST_INST_GUIADDON_DETECTOR_VIEWER'

     END ; PRO BST_INST_GUIADDON_USER_SPECTRA::SET_FLAGS


     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::INIT, dispatcher
       
       status = self->BST_INST_GUIADDON::INIT(dispatcher)

       RETURN, status

     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::INIT


     


     ;+=================================================================
     ;
     ; Return a structure with the default parameters for the
     ; USER SPECTRA addon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::RESET, _NULL=_null
           
       self->SET_PARAM, { $
                          function_name:'' $
                          ,filepath:'' $
                          ,path:'' $
                          ,use_function:0 $
                        }
 
       self->BST_INST_GUIADDON::RESET

     END ; PRO BST_INST_GUIADDON_USER_SPECTRA::RESET




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the USER SPECTRA addon.
     ;
     ; PROGRAMMING NOTES:
     ;   Note here that we do not check if this addon is already
     ;   enabled.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::ENABLE

       ; Call the parent method.
       self->BST_INST_GUIADDON::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       fitter = self.dispatcher->GET_CORE()
       fitter->SET_SPECTRUM_FUNCTION, 'spectrum', self

     END ; PRO BST_INST_GUIADDON_USER_SPECTRA::ENABLE



     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::DISABLE

       ; Call the parent method.
       self->BST_INST_GUIADDON::DISABLE

       IF self->IS_ENABLED() THEN BEGIN
         fitter = self.dispatcher->GET_CORE()
         fitter->SET_SPECTRUM_FUNCTION, /DEFAULT
       ENDIF

     END ;PRO BST_INST_GUIADDON_USER_SPECTRA::DISABLE

     

     ;+=================================================================
     ;
     ; Return a prefix to be used for automatic filenaming
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::GET_FILENAME_PREFIX, _NULL=_null

       IF self.param.use_function THEN BEGIN
         filename_prefix = STRLOWCASE(self.param.function_name)
       ENDIF ELSE BEGIN
         path_parts = MIR_PATH_SPLIT(self.param.filepath)
         filename_prefix = path_parts.filehead
       ENDELSE

       RETURN, STRLOWCASE(filename_prefix)

     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::GET_FILENAME_PREFIX



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the state of addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::GET_STATE
       
       state = self->BST_INST_GUIADDON::GET_STATE()
       state = CREATE_STRUCT(state, {param:self.param})

       RETURN, state

     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load a save structure for this addon.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::LOAD_STATE, state

       self->BST_INST_GUIADDON::LOAD_STATE, state

       STRUCT_ASSIGN, {param:state.param}, self, /NOZERO

     END ; PRO BST_INST_GUIADDON_USER_SPECTRA::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::GET_PARAM

       RETURN, self.param

     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::GET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Load the addon parameters from a structure.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::SET_PARAM, param

       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ; PRO BST_INST_GUIADDON_USER_SPECTRA::SET_PARAM





; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_USER_SPECTRA::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::GUI_DEFINE
                          
 
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Set up the file picking area.
       ; ---------------------------------------------------------------
       ui_base_top = WIDGET_BASE(ui_base, /ROW)
         ui_bgroup_file = CW_BGROUP(ui_base_top $
                                    ,'From File' $
                                    ,UNAME='ui_bgroup_file' $
                                    ,/NONEXCLUSIVE $
                                    ,FRAME=0)
         ui_button_pick_file = WIDGET_BUTTON(ui_base_top $
                                             ,UNAME='ui_button_pick_file' $
                                             ,VALUE='File:' $
                                            )
         ui_text_filepath = WIDGET_TEXT(ui_base_top $
                                        ,UNAME='ui_text_filepath' $
                                        ,VALUE='Chose a file to load.' $
                                        ,YSIZE=1 $
                                        ,XSIZE=90 $
                                        ,/ALL_EVENTS $
                                        ,/EDITABLE $
                                        ,/NO_NEWLINE)
         

       ui_base_bottom = WIDGET_BASE(ui_base, /ROW)
         ui_bgroup_function = CW_BGROUP(ui_base_bottom $
                                        ,'From Function' $
                                        ,UNAME='ui_bgroup_function' $
                                        ,/NONEXCLUSIVE $
                                        ,FRAME=0)
         ui_text_function = WIDGET_TEXT(ui_base_bottom $
                                        ,UNAME='ui_text_function' $
                                        ,VALUE='Give the name of the spectrum function.' $
                                        ,YSIZE=1 $
                                        ,XSIZE=90 $
                                        ,/ALL_EVENTS $
                                        ,/EDITABLE $
                                        ,/NO_NEWLINE)



       id_struct = { $
                     ui_bgroup_file:ui_bgroup_file $
                     ,ui_button_pick_file:ui_button_pick_file $
                     ,ui_text_filepath:ui_text_filepath $

                     ,ui_bgroup_function:ui_bgroup_function $
                     ,ui_text_function:ui_text_function $
                   }


       self->SET_ID_STRUCT, id_struct


     END ; PRO BST_INST_GUIADDON_USER_SPECTRA::GUI_DEFINE
     


     ;+=================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::EVENT_HANDLER, event


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF 


       ; First call the superclass method
       self->BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       id = self->GET_ID_STRUCT()


       ; Start main event loop
       CASE event.id OF

         ;ui_bgroup_file
         id.ui_bgroup_file: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.use_function = ~value[0]

           WIDGET_CONTROL, id.ui_bgroup_function, SET_VALUE=~value
         END

         ;ui_bgroup_function
         id.ui_bgroup_function: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.use_function = value[0]
           
           WIDGET_CONTROL, id.ui_bgroup_file, SET_VALUE=~value
         END

         ;ui_button_pick_file
         id.ui_button_pick_file: BEGIN
           self->PICK_FILE
           WIDGET_CONTROL, id.ui_text_filepath, SET_VALUE=self.param.filepath
         END

         ;ui_text_filepath
         id.ui_text_filepath: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.filepath = value
         END

         ;ui_text_function
         id.ui_text_function: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.function_name = value
         END


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ;PRO BST_INST_GUIADDON_USER_SPECTRA::EVENT_HANDLER




     ;+=================================================================
     ;
     ; Update the gui from the current parameters in the
     ; common block.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::UPDATE_GUI


       id = self->GET_ID_STRUCT()

       ;speectrum source
       WIDGET_CONTROL, id.ui_bgroup_function, SET_VALUE=self.param.use_function
       WIDGET_CONTROL, id.ui_bgroup_file, SET_VALUE=~self.param.use_function

       ;ui_text_filepath
       WIDGET_CONTROL, id.ui_text_filepath, SET_VALUE=self.param.filepath

       ;ui_text_function_name
       WIDGET_CONTROL, id.ui_text_function, SET_VALUE=self.param.function_name

     END ;PRO BST_INST_GUIADDON_USER_SPECTRA::UPDATE_GUI




     ;+=================================================================
     ;
     ; Get the filepath.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA::PICK_FILE


       path = self.dispatcher->GET_CURRENT_PATH()

       filepath = DIALOG_PICKFILE(/READ $
                                  ,FILE=filepath $
                                  ,PATH=path $
                                  ,GET_PATH=get_path $
                                  ,FILTER=['*.spectrum;*.sav','*.spectrum','*.sav'] $
                                  ,/MUST_EXIST $
                                 )
       
       IF filepath NE '' THEN BEGIN
         ; Save the path.
         self.param.path = get_path
         self.dispatcher->SET_CURRENT_PATH, get_path

         ; Save the filepath
         self.param.filepath = filepath
       ENDIF ELSE BEGIN
         RETURN
       ENDELSE

     END ;PRO BST_INST_GUIADDON_USER_SPECTRA::PICK_FILE




; ======================================================================
; ======================================================================
; ######################################################################
;
; SPECTRUM METHODS
;    These are the methods that <BST_SPECTRAL_FIT> will actually
;    use to get the spectra.
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Returns a spectrum from a file.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::SPECTRUM


       ; Establish catch block.
       CATCH, error                   
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, STRING('Spectrum could not retreive user spectrum.') 
       ENDIF


       ; Get the spectra, either from a user function or from an IDL save file.
       IF self.param.use_function THEN BEGIN
         spectrum = self->SPECTRUM_FROM_FUNCTION()
       ENDIF ELSE BEGIN
         spectrum = self->SPECTRUM_FROM_FILE()
       ENDELSE

       tagnames = TAG_NAMES(spectrum)

       IF ~ HAS_TAG(spectrum, 'Y') THEN BEGIN
         MESSAGE, "Tag 'y' was not found in the 'spectrum' structure."
       ENDIF

       num_points = N_ELEMENTS(spectrum.y)


       spectrum_out = spectrum


       ; Check if the 'x' and 'weight' tags exist, if not
       ; generate the data.
       IF ~ HAS_TAG(spectrum, 'X') THEN BEGIN
         spectrum_out = BUILD_STRUCTURE('X', FINDGEN(num_points), spectrum_out)
       ENDIF

       IF ~ HAS_TAG(spectrum, 'WEIGHT') THEN BEGIN
         weight = DBLARR(num_points)
         weight[*] = 1.0
         spectrum_out = BUILD_STRUCTURE('WEIGHT', weight, spectrum_out)      
       ENDIF


       RETURN, spectrum_out


     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::SPECTRUM




     ;+=================================================================
     ;
     ; Returns a spectrum from a file.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::SPECTRUM_FROM_FILE


       ; Establish catch block.
       CATCH, error                   
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, STRING('Spectrum could not be loaded from file: ' ,self.param.filepath) 
       ENDIF

   
       filepath = self.param.filepath
       IF filepath EQ '' THEN BEGIN
         MESSAGE, 'No filename has been specified.'
       ENDIF

       IF NOT FILE_TEST(filepath) THEN BEGIN
         MESSAGE, 'The specified file could not be found.'
       ENDIF

       RESTORE, filepath

       IF N_ELEMENTS(spectrum) EQ 0 THEN BEGIN
         MESSAGE, "The 'spectrum' structure was not found in the IDL save file."
       ENDIF

       RETURN, spectrum

     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::SPECTRUM_FROM_FILE





     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Returns a spectrum from a user defined function.
     ;
     ; DESCRIPTION:
     ;   The input can be either the name of a function, or an
     ;   expression containing a function.
     ;
     ;   For example:
     ;     'GET_SPECTRUM'
     ;   or
     ;    'GET_SPECTRUM(1000.0, /BACKGROUND)'
     ;
     ; TO DO:
     ;   Rather than use the EXECUTE command I should parse the given
     ;   string and build up a structure that can be passed using
     ;   the _EXTRA keyword.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_USER_SPECTRA::SPECTRUM_FROM_FUNCTION

       IF self.param.function_name EQ '' THEN BEGIN
         MESSAGE, 'No function name has been specified.'
       ENDIF

       ; Now check to see if this is just a function name, 
       ; or an expression.  Extract the function name
       name_parts = STRSPLIT(self.param.function_name, '(', /EXTRACT, /PRESERVE_NULL)
       function_name = name_parts[0]

       use_execute = (N_ELEMENTS(name_parts) GT 1)
       

       ; Attempt to resolve the function.
       CATCH, error     
       IF error EQ 0 THEN BEGIN
         RESOLVE_ROUTINE, function_name, /COMPILE_FULL_FILE, /NO_RECOMPILE, /IS_FUNCTION
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
       ENDELSE



       ; Establish catch block.
       CATCH, error
       IF error EQ 0 THEN BEGIN        

         IF use_execute THEN BEGIN
           void = EXECUTE('spectrum = ' + self.param.function_name)
         ENDIF ELSE BEGIN
           spectrum = CALL_FUNCTION(function_name)
         ENDELSE

       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, STRING('Spectrum could not be loaded from function: ' $
                         ,self.param.function_name)
       ENDELSE


       ; Check the results.
       IF SIZE(spectrum, /TYPE) NE 8 THEN BEGIN
         spectrum = {y:spectrum}
       ENDIF


       RETURN, spectrum

     END ; FUNCTION BST_INST_GUIADDON_USER_SPECTRA::SPECTRUM_FROM_FUNCTION




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_USER_SPECTRA__DEFINE

       struct = { BST_INST_GUIADDON_USER_SPECTRA $

                  ,param:{ BST_INST_GUIADDON_USER_SPECTRA__PARAM $
                           ,function_name:'' $
                           ,filepath:'' $
                           ,path:'' $
                           ,use_function:0 $
                         } $

                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_USER_SPECTRA__DEFINE
