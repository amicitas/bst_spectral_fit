




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   A widget to control the <BST_INST_MODEL_XCRYSTAL::> model
;  for <BST_SPECTRAL_FIT>.
; 
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_WIDGET_DETECTOR_VIEWER::INIT $
       ,model $
       ,_REF_EXTRA=extra
       
       status = self.WIDGET_OBJECT::INIT(_EXTRA=extra)

       self.SET_ADDON, model

       self.GUI_INIT, _EXTRA=extra

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_DETECTOR_VIEWER::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model object reference.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_DETECTOR_VIEWER::SET_ADDON $
       ,addon
       
       MIR_DEFAULT, addon, OBJ_NEW()

       self.addon = addon

     END ; PRO BST_INST_WIDGET_DETECTOR_VIEWER::SET_ADDON



     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the <BST_INST_WIDGET_DETECTOR_VIEWER::>
     ;   object.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_DETECTOR_VIEWER::GUI_DEFINE $
       ,FONT=LabelFont $
       ,FRAME=frame $
       ,SUB_FRAME=sub_frame $
       ,ALIGN_CENTER=align_center

       ; Set the defaults
       MIR_DEFAULT, frame, 1
       MIR_DEFAULT, sub_frame, 0


       ui_base = WIDGET_BASE(self.gui_param.gui_id $
                             ,/COLUMN $
                             ,FRAME=frame $
                             ,ALIGN_CENTER=align_center)
       
       ui_label = WIDGET_LABEL(ui_base $
                               ,VALUE='Detector Viewer' $
                               ,/ALIGN_LEFT)
       
       ; All model controls, except for 'USE' should be in this base.
       ; The 'USE' option will toggle the 
       ui_base_model = WIDGET_BASE(ui_base $
                                   , UNAME='ui_base_model' $
                                   ,/COLUMN $
                                   ,ALIGN_CENTER=align_center $
                                   ,SENSITIVE=1 $
                                   ,FRAME=sub_frame)

       ui_base_param = WIDGET_BASE(ui_base_model $
                                   ,UNAME='ui_base_param' $
                                   ,/COLUMN)
       
       id_struct = { $
                     ui_base_model:ui_base_model $
                   }
       
       label_size = 120
       field_size = 4
       format = '(i0)'

       
       name = 'RANGE_ROW'
       title = 'Average Rows'
       destination = ui_base_param
       ui_base = WIDGET_BASE(destination, /ROW)
       
       ui_field_0 = CW_FIELD_EXT(ui_base $
                                 ,UNAME='UI_FIELD_0_'+name $
                                 ,UVALUE={type:'CW_FIELD_EXT' $
                                          ,isarray:1 $
                                          ,index:0 $
                                          ,name:name $
                                         } $
                                 ,VALUE='' $
                                 ,TITLE=title $
                                 ,/DOUBLE $
                                 ,/ALL_EVENTS $
                                 ,FORMAT=format $
                                 ,XSIZE=field_size $
                                 ,LABEL_XSIZE=0)
       ui_field_1 = CW_FIELD_EXT(ui_base $
                                 ,UNAME='UI_FIELD_1_'+name $
                                 ,UVALUE={type:'CW_FIELD_EXT' $
                                          ,isarray:1 $
                                          ,index:1 $
                                          ,name:name $
                                         } $
                                 ,VALUE='' $
                                 ,TITLE='' $
                                 ,/DOUBLE $
                                 ,/ALL_EVENTS $
                                 ,FORMAT=format $
                                 ,XSIZE=field_size $
                                 ,LABEL_XSIZE=0)


       id_struct = CREATE_STRUCT(id_struct $
                                 ,'UI_FIELD_0_'+name $
                                 ,ui_field_0 $
                                 ,'UI_FIELD_1_'+name $
                                 ,ui_field_1 $
                                )

       
       name = 'RANGE_COL'
       title = 'Average Cols'
       destination = ui_base_param
       ui_base = WIDGET_BASE(destination, /ROW)
       
       ui_field_0 = CW_FIELD_EXT(ui_base $
                                 ,UNAME='UI_FIELD_0_'+name $
                                 ,UVALUE={type:'CW_FIELD_EXT' $
                                          ,isarray:1 $
                                          ,index:0 $
                                          ,name:name $
                                         } $
                                 ,VALUE='' $
                                 ,TITLE=title $
                                 ,/DOUBLE $
                                 ,/ALL_EVENTS $
                                 ,FORMAT=format $
                                 ,XSIZE=field_size $
                                 ,LABEL_XSIZE=0)
       ui_field_1 = CW_FIELD_EXT(ui_base $
                                 ,UNAME='UI_FIELD_1_'+name $
                                 ,UVALUE={type:'CW_FIELD_EXT' $
                                          ,isarray:1 $
                                          ,index:1 $
                                          ,name:name $
                                         } $
                                 ,VALUE='' $
                                 ,TITLE='' $
                                 ,/DOUBLE $
                                 ,/ALL_EVENTS $
                                 ,FORMAT=format $
                                 ,XSIZE=field_size $
                                 ,LABEL_XSIZE=0)


       id_struct = CREATE_STRUCT(id_struct $
                                 ,'UI_FIELD_0_'+name $
                                 ,ui_field_0 $
                                 ,'UI_FIELD_1_'+name $
                                 ,ui_field_1 $
                                )


       self.SET_ID_STRUCT, id_struct

     END ;PRO BST_INST_WIDGET_DETECTOR_VIEWER::GUI_DEFINE



     ;+=================================================================
     ;
     ; Here we have the event handler for this widget.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_DETECTOR_VIEWER::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         PRINT_MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  


       ; First call the superclass method
       self.WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       id = self.GET_ID_STRUCT()
       event_handled = 0

       uname = WIDGET_INFO(event.id, /UNAME)
       WIDGET_CONTROL, event.id, GET_UVALUE=uvalue

       param = self.addon.GET_PARAM()
       addon_param_names = TAG_NAMES(param)
       w = WHERE(addon_param_names EQ uvalue.name, count)
       IF count EQ 0 THEN BEGIN
         MESSAGE, 'Parameter not found in addon: ' + param_name
       ENDIF

      
       
       CASE uvalue.type OF

         ; Text event
         'CW_FIELD_EXT': BEGIN
           IF uvalue.isarray THEN BEGIN
             value = param.(w[0])
             value[uvalue.index] = event.value
             struct = CREATE_STRUCT(uvalue.name, value)
             self.addon.SET_PARAM, struct
             event_handled = 1
           ENDIF ELSE BEGIN
             struct = CREATE_STRUCT(uvalue.name, event.value)
             self.addon.SET_PARAM, struct
             event_handled = 1
           ENDELSE
         END

         ELSE:
       ENDCASE

       IF event_handled THEN RETURN
      


       MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)



     END ;PRO BST_INST_WIDGET_DETECTOR_VIEWER::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Update the GUI to reflect the current state of the model.
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_DETECTOR_VIEWER::UPDATE
       
       
       id = self.GET_ID_STRUCT()

       param = self.addon.GET_PARAM()

       ; Update parameter values.
       gui_id_names = TAG_NAMES(id)

       addon_param_names = TAG_NAMES(param)
       num_addon_params = N_TAGS(param)


       FOR ii=0,num_addon_params-1 DO BEGIN
         name = addon_param_names[ii]

         num_val = N_ELEMENTS(param.(ii))
         FOR ii_val = 0, num_val-1 DO BEGIN
           w = WHERE(gui_id_names EQ 'UI_FIELD_'+STRING(ii_val, FORMAT='(i0)')+'_'+name, count)
           IF count GT 0 THEN BEGIN
             PRINT, 'I found it!', gui_id_names[w[0]]
             WIDGET_CONTROL, id.(w[0]), SET_VALUE=param.(ii)[ii_val], SENSITIVE=1
           ENDIF

           w = WHERE(gui_id_names EQ 'UI_BGROUP_'+name, count)
           if count GT 0 THEN BEGIN
             WIDGET_CONTROL, id.(w[0]), SET_VALUE=[fixed.(ii)], SENSITIVE=1
           ENDIF
         ENDFOR

       ENDFOR


     END ; PRO BST_INST_WIDGET_DETECTOR_VIEWER::UPDATE


     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [WIDGET_OBJECT::]
     ;
     ;-=================================================================
     PRO BST_INST_WIDGET_DETECTOR_VIEWER__DEFINE

       struct = { BST_INST_WIDGET_DETECTOR_VIEWER $
                           
                  ,addon:OBJ_NEW() $

                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS OBJECT }

     END ; PRO BST_INST_WIDGET_DETECTOR_VIEWER__DEFINE
