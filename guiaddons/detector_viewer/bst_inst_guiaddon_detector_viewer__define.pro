

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2017-03
;
; PURPOSE:
;   An guiaddon for <BST_SPECTRAL_FIT> to control parameters in the
;   BST_INST_ADDON_DETECTOR_VIEWER addon.
;
; TODO:
;   I need to be able to control the following options:
;     use_row
;     use_average
;     use_poisson_weights
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::SET_FLAGS

       self.BST_INST_GUIADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.use_tab = 0
       self.use_control = 1

       self.add_to_menu = 1
       self.menu_title = 'GUIADDON: Detector Viewer'

       self.title = 'Detector Viewer'

       self.required = 'BST_INST_ADDON_DETECTOR_VIEWER'

     END ; PRO BST_INST_GUIADDON_DETECTOR_VIEWER::SET_FLAGS


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize this addon.
     ; 
     ; DESCRIPTION:
     ;   This is called after all addons have been instantiated.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::INITIALIZE
       self.BST_INST_GUIADDON::INITIALIZE

       self.addon = (self.dispatcher).GET_ADDON('BST_INST_ADDON_DETECTOR_VIEWER')

     END ;PRO BST_INST_GUIADDON_DETECTOR_VIEWER::INITIALIZE


; ======================================================================
; ======================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   This will generally share a common block with the object.
;   
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_GUIADDON_DETECTOR_VIEWER::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::GUI_DEFINE
                          
 
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Add a widget to control the addon
       self.REMOVE_WIDGET
       self.widget = OBJ_NEW('BST_INST_WIDGET_DETECTOR_VIEWER' $
                             ,self.addon $
                             ,DESTINATION=ui_base)


     END ; PRO BST_INST_GUIADDON_DETECTOR_VIEWER::GUI_DEFINE
     


     ;+=================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::EVENT_HANDLER, event


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF 


       ; First call the superclass method
       self.BST_INST_GUIADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)


     END ;PRO BST_INST_GUIADDON_DETECTOR_VIEWER::EVENT_HANDLER



     ;+=================================================================
     ;
     ; Update the gui from the current object parameters
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::UPDATE_GUI

       self.widget.UPDATE

     END ;PRO BST_INST_GUIADDON_DETECTOR_VIEWER::UPDATE_GUI



     ;+=================================================================
     ; PURPOSE:
     ;   Remove the widget.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::REMOVE_WIDGET
       
       IF OBJ_VALID(self.widget) THEN BEGIN
         self.widget.DESTROY
       ENDIF

     END ; PRO BST_INST_GUIADDON_DETECTOR_VIEWER::REMOVE_WIDGET




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER::CLEANUP

       IF OBJ_VALID(self.widget) THEN BEGIN
         self.REMOVE_WIDGET
       ENDIF

       self.BST_INST_GUIADDON::CLEANUP

     END ; PRO BST_INST_GUIADDON_DETECTOR_VIEWER::CLEANUP


     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_GUIADDON]
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DETECTOR_VIEWER__DEFINE

       struct = { BST_INST_GUIADDON_DETECTOR_VIEWER $
                  
                  ,addon:OBJ_NEW() $
                  ,widget:OBJ_NEW() $

                  ,INHERITS BST_INST_GUIADDON }

     END ; PRO BST_INST_GUIADDON_DETECTOR_VIEWER__DEFINE
