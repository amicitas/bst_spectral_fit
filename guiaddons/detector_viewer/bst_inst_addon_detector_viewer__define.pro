

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from images loaded using [DETECTOR_VIEWER].
; 
;-======================================================================








     ;+=================================================================
     ;
     ; Initialize this addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DETECTOR_VIEWER::INIT
       
       status = self->BST_INST_ADDON_GUI::INIT()

       self.menu_title = 'Image Spectra'
       self.enabled = 1

       self.use_tab = 0
       self.use_control = 1

       self.conflicting = 'bst_inst_user_spectra, bst_inst_cerview'

       RETURN, status

     END ; FUNCTION BST_INST_DETECTOR_VIEWER::INIT




     ;+=================================================================
     ;
     ; Return a structure with the default parameters for the
     ; DETECTOR_VIEWER addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DETECTOR_VIEWER::DEFAULT_PARAMS
                                
       defaults =  { $
                     type:'ROW' $
                     ,do_average:1 $
                     ,avgrange_row:[0,0] $
                     ,avgrange_col:[0,0] $
                     ,image_filename:'' $
                     ,back_filename:'' $
                     ,back_subtraction:0 $
                   }

       RETURN, defaults

     END ; FUNCTION BST_INST_DETECTOR_VIEWER::DEFAULT_PARAMS




     ;+=================================================================
     ;
     ; Initialize the DETECTOR_VIEWER addon.
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::ENABLE

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       BST_INST_SET_SPECTRUM_ADDON, 'spectrum', OBJ_CLASS(self)

       ; Run Detector Viewer.
       ; If detector viewer is already open, this won't do anything.
       DETECTOR_VIEWER       


     END ; PRO BST_INST_DETECTOR_VIEWER::ENABLE



     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::DISABLE

       IF NOT self.enabled THEN RETURN

       ; Call the parent method.
       self->BST_INST_ADDON_GUI::DISABLE

       IF self.active THEN BEGIN
         ; Reset the spectrum function
         BST_INST_SET_SPECTRUM_ADDON, /DEFAULT
       ENDIF

     END ;PRO BST_INST_DETECTOR_VIEWER::DISABLE





     ;+=================================================================
     ; 
     ; Return a structure containing anything should should be saved
     ; for this addon.
     ;
     ; This is different from the base method in that it first updates
     ; the image file names from detector viewer.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DETECTOR_VIEWER::GET_STATE

       FORWARD_FUNCTION DV_GET_IMAGE_DATA $
                        ,DV_GET_IMAGE_BACK_DATA $
                        ,DV_GET_IMAGE_FINAL_DATA
       
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         BST_INST_MESSAGE, STRING('Caught in BST_INST_DETECTOR_VIEWER::GET_STATE.') $
           ,LOG=~self.print_debug
         BST_INST_MESSAGE, STRING('Error: ', !ERROR_STATE.MSG), LOG=~self.print_debug
         MESSAGE, /REISSUE_LAST
       ENDIF

       ; For this addon we  do not want to return a full save 
       ; structure unless the addon is active.
       IF NOT self.active THEN BEGIN
         RETURN, {active:self.active}
       ENDIF


       ; First load the filenames from DETECTOR_VIEWER into the addon
       ; param.
       image_data = DV_GET_IMAGE_DATA()
       image_back_data = DV_GET_IMAGE_BACK_DATA()
       image_final_data = DV_GET_IMAGE_FINAL_DATA()

       self.param.image_filename = image_data.filename
       self.param.back_filename = image_back_data.filename 
       self.param.back_subtraction = image_final_data.back_subtraction

       ; Now all the superclass method.
       save_structure = self->BST_INST_ADDON_GUI::GET_STATE()

       RETURN, save_structure


     END ; FUNCTION BST_INST_DETECTOR_VIEWER::GET_STATE




     ;+=================================================================
     ;
     ; Recieve a structure, of the same type as given in in
     ; [GET_STATE], and load the contents.
     ;
     ; This is different from the base method in that in loads
     ; the image file names into detector viewer.
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::LOAD_STATE, save_structure

       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         BST_INST_MESSAGE, STRING('Caught in BST_INST_DETECTOR_VIEWER::LOAD_STATE') $
           ,LOG=~self.print_debug
         BST_INST_MESSAGE, STRING('Error: ', !ERROR_STATE.MSG), LOG=~self.print_debug
         MESSAGE, /REISSUE_LAST
       ENDIF

       ; Unless the detector viewer addon was active when it's state was
       ; saved, do not load anything.  This is because we don't want
       ; to open DETECTOR_VIEWER unless it is necessary.
       IF (NOT save_structure.active AND self.active) THEN BEGIN
         self->RESET_PARAMS
         self->DISABLE
       ENDIF
       IF (NOT save_structure.active) THEN RETURN

       ; First call the superclass method
       self->BST_INST_ADDON_GUI::LOAD_STATE, save_structure

       ; Now load the parameters in DETECTOR_VIEWER

       ; Load DETECTOR_VIWER. 
       ; If it is already open this will bring the window to the front.
       DETECTOR_VIEWER

       ; Set the background subtraction
       DV_SET_BACK_SUBTRACTION, self.param.back_subtraction, /NO_REFRESH

       ; Load the image.
       DV_LOAD_IMAGE, self.param.image_filename, /NO_REFRESH

       ; Load the background.
       DV_LOAD_IMAGE, self.param.back_filename, /BACKGROUND

         
     END ; PRO BST_INST_DETECTOR_VIEWER::LOAD_STATE




     ;+=================================================================
     ;
     ; Create a compound widget for loading spectra from
     ; DETECTOR_VIEWER.
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::BUILD_GUI


       ui_base = WIDGET_BASE(self.gui_id, /COLUMN)
       ui_base_top = WIDGET_BASE(ui_base, /ROW, FRAME=1)
       ui_base_bottom = WIDGET_BASE(ui_base, /ROW, FRAME=1)       

         ui_base_spec_1d = WIDGET_BASE(ui_base_top, /ROW, FRAME=1)
           ui_bgroup_1D = CW_BGROUP(ui_base_spec_1d, '1D plot', /NONEXCLUSIVE, FRAME=0)

         ui_base_spec_row = WIDGET_BASE(ui_base_top, /ROW, FRAME=1)
           ui_bgroup_row = CW_BGROUP(ui_base_spec_row, 'Row average', /NONEXCLUSIVE, FRAME=0, SET_VALUE=1)

           ui_field_row_avg_low = CW_FIELD_EXT(ui_base_spec_row, UNAME='ui_field_row_avg_low' $
                                               ,VALUE=0 $
                                               ,TITLE='' $
                                               ,/INT $
                                               ,XSIZE=4 $
                                               ,MAXLENGTH=4 $
                                               ,/ALL_EVENTS)
           ui_field_row_avg_high = CW_FIELD_EXT(ui_base_spec_row, UNAME='ui_field_row_avg_high' $
                                                ,VALUE=0 $
                                                ,TITLE='' $
                                                ,/INT $
                                                ,XSIZE=4 $
                                                ,MAXLENGTH=4 $
                                                ,/ALL_EVENTS)

         ui_base_spec_col = WIDGET_BASE(ui_base_top, /ROW, FRAME=1)
           ui_bgroup_col = CW_BGROUP(ui_base_spec_col, 'Col average', /NONEXCLUSIVE, FRAME=0)

           ui_field_col_avg_low = CW_FIELD_EXT(ui_base_spec_col, UNAME='ui_field_col_avg_low' $
                                               ,VALUE=0 $
                                               ,TITLE='' $
                                               ,/INT $
                                               ,XSIZE=4 $
                                               ,MAXLENGTH=4 $
                                               ,/ALL_EVENTS)
           ui_field_col_avg_high = CW_FIELD_EXT(ui_base_spec_col, UNAME='ui_field_col_avg_high' $
                                                ,VALUE=0 $
                                                ,TITLE='' $
                                                ,/INT $
                                                ,XSIZE=4 $
                                                ,MAXLENGTH=4 $
                                                ,/ALL_EVENTS)

         ui_base_options = WIDGET_BASE(ui_base_bottom, /ROW, FRAME=1)
           ui_bgroup_options = CW_BGROUP(ui_base_options, 'Average Rows/Columns' $
                                         ,/NONEXCLUSIVE $
                                         ,FRAME=0 $
                                         ,SET_VALUE=1)

       id_struct = { $
                     ui_base:ui_base $
                     ,ui_bgroup_1D:ui_bgroup_1D $
                     ,ui_bgroup_row:ui_bgroup_row $
                     ,ui_field_row_avg_low:ui_field_row_avg_low $
                     ,ui_field_row_avg_high:ui_field_row_avg_high $
                     ,ui_bgroup_col:ui_bgroup_col $
                     ,ui_field_col_avg_low:ui_field_col_avg_low $
                     ,ui_field_col_avg_high:ui_field_col_avg_high $
                     ,ui_bgroup_options:ui_bgroup_options $
                   }


       state = { $
                 id_struct:id_struct $
               }

       ; Put the state variable in the first child.
       WIDGET_CONTROL, ui_base, SET_UVALUE=state


     END ; BST_INST_DETECTOR_VIEWER::BUILD_GUI



     
     ;+=================================================================
     ;
     ; Here we have the event manager for the DETECTOR_VIEWER addon.
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::EVENT_HANDLER, event, EVENT_HANDLED=event_handled


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL

         IF N_ELEMENTS(state_holder) NE 0 THEN BEGIN
           WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY
         ENDIF

         BST_INST_MESSAGE, STRING('Caught in BST_INST_DETECTOR_VIEWER::EVENT_HANDLER.') $
           ,LOG=~self.print_debug
         BST_INST_MESSAGE, STRING('Error: ', !ERROR_STATE.MSG), LOG=~self.print_debug

         IF self.full_debug THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF
       ENDIF




       ; First call the superclass method
       self->BST_INST_ADDON_GUI::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       ; Get the state holder.
       state_holder = WIDGET_INFO(Event.Handler, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY

       ; Start main event loop
       CASE event.id OF

         ; Setup for row, col & 1d buttons.
         ; The default is 'ROW'.
         ; If row is unchecked the next default is '1D'.
         ; -------------------------------------------------------------

         ;ui_bgroup_1D
         state.id_struct.ui_bgroup_1D: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           IF value[0] THEN BEGIN
             self.param.type = '1D'
           ENDIF ELSE BEGIN
             self.param.type = 'ROW'
           ENDELSE

           WIDGET_CONTROL, state.id_struct.ui_bgroup_row, SET_VALUE=~value
           WIDGET_CONTROL, state.id_struct.ui_bgroup_col, SET_VALUE=0
         END
         ;ui_bgroup_row
         state.id_struct.ui_bgroup_row: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           IF value[0] THEN BEGIN
             self.param.type = 'ROW'
           ENDIF ELSE BEGIN
             self.param.type = '1D'
           ENDELSE

           WIDGET_CONTROL, state.id_struct.ui_bgroup_1d, SET_VALUE=~value
           WIDGET_CONTROL, state.id_struct.ui_bgroup_col, SET_VALUE=0
         END
         ;ui_bgroup_col
         state.id_struct.ui_bgroup_col: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value

           IF value[0] THEN BEGIN
             self.param.type = 'COL'
           ENDIF ELSE BEGIN
             self.param.type = 'ROW'
           ENDELSE

           WIDGET_CONTROL, state.id_struct.ui_bgroup_row, SET_VALUE=~value
           WIDGET_CONTROL, state.id_struct.ui_bgroup_1d, SET_VALUE=0
         END

         ; -------------------------------------------------------------

         ;ui_field_row_avg_low
         state.id_struct.ui_field_row_avg_low: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.avgrange_row[0] = value
         END
         ;ui_field_row_avg_high
         state.id_struct.ui_field_row_avg_high: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.avgrange_row[1] = value
         END


         ;ui_field_col_avg_low
         state.id_struct.ui_field_col_avg_low: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.avgrange_col[0] = value

         END
         ;ui_field_col_avg_high
         state.id_struct.ui_field_col_avg_high: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.avgrange_col[1] = value

         END


         ;ui_button_do_average
         state.id_struct.ui_bgroup_options: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           self.param.do_average = value[0]
         END

         ELSE: BEGIN
           uname = WIDGET_INFO(event.id, /UNAME)
           MESSAGE, STRING('No rule for: ', uname)
         ENDELSE
       ENDCASE

       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ;PRO BST_INST_DETECTOR_VIEWER::EVENT_HANDLER




     ;+=================================================================
     ;
     ; Update the gui from the current parameters in the
     ; common block.
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::UPDATE_GUI

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY

       ; Set the type
       WIDGET_CONTROL, state.id_struct.ui_bgroup_col, SET_VALUE=0
       WIDGET_CONTROL, state.id_struct.ui_bgroup_row, SET_VALUE=0
       WIDGET_CONTROL, state.id_struct.ui_bgroup_1d, SET_VALUE=0

       CASE self.param.type OF
         '1D': WIDGET_CONTROL, state.id_struct.ui_bgroup_1d, SET_VALUE=1
         'ROW': WIDGET_CONTROL, state.id_struct.ui_bgroup_row, SET_VALUE=1
         'COL': WIDGET_CONTROL, state.id_struct.ui_bgroup_col, SET_VALUE=1
         ELSE:
       END
         

       ;ui_field_row_avg_low
       WIDGET_CONTROL,state.id_struct.ui_field_row_avg_low, SET_VALUE=self.param.avgrange_row[0]

       ;ui_field_row_avg_high
       WIDGET_CONTROL,state.id_struct.ui_field_row_avg_high, SET_VALUE=self.param.avgrange_row[1]
       
       ;ui_field_col_avg_low
       WIDGET_CONTROL,state.id_struct.ui_field_col_avg_low, SET_VALUE=self.param.avgrange_col[0]

       ;ui_field_col_avg_high
       WIDGET_CONTROL,state.id_struct.ui_field_col_avg_high, SET_VALUE=self.param.avgrange_col[1] 
       
       ;ui_button_do_average
       WIDGET_CONTROL, state.id_struct.ui_bgroup_options, SET_VALUE=[self.param.do_average]


       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ; PRO BST_INST_DETECTOR_VIEWER::UPDATE_GUI




; ======================================================================
; ======================================================================
; ######################################################################
;
; SPECTRUM METHODS
;    These are the methods that <BST_SPECTRAL_FIT> will actually
;    use to get the spectra.
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ;
     ; This is the function that the the DETECTOR_VIEWER addon uses
     ; to retrieve the spectra to be fit.
     ;
     ; This method will be called directly by <BST_SPECTRAL_FIT>
     ;
     ;-=================================================================
     FUNCTION BST_INST_DETECTOR_VIEWER::SPECTRUM

       FORWARD_FUNCTION DV_IS_IMAGE_1D_LOADED $
         ,DV_IS_IMAGE_LOADED $
         ,DV_GET_IMAGE_1D $
         ,DV_GET_IMAGE_FINAL $
         ,DV_GET_IMAGE_FINAL__DATA

       IF self.param.type EQ '1D' THEN BEGIN

         IF NOT (DV_IS_IMAGE_1D_LOADED()) THEN BEGIN
           MESSAGE, '1D image cut is not loaded.'
         ENDIF

         y = DV_GET_IMAGE_1D()
         spectrum_size = N_ELEMENTS(y)

       ENDIF ELSE BEGIN

         IF NOT (DV_IS_IMAGE_LOADED()) THEN BEGIN
           MESSAGE, 'Image is not loaded.'
         ENDIF
         
         image_data = DV_GET_IMAGE_FINAL_DATA()
         image = DV_GET_IMAGE_FINAL()

         CASE 1 OF
           self.param.type EQ 'ROW': BEGIN
             dim_index = 1
             range = self.param.avgrange_row
           END
           self.param.type EQ 'COL': BEGIN
             dim_index = 0
             range = self.param.avgrange_col
           END
         ENDCASE
            
         spectrum_size = image_data.size[dim_index]
         y = DBLARR(spectrum_size)
             

         IF (range[0] LT 0) THEN BEGIN
           MESSAGE, 'Invalid range.'
         ENDIF
             
         ; If range[1] eq 0 then we want to assume that no averaging is wanted.
         ; Just use the slice from range[0]
         CASE 1 OF
           range[1] EQ 0: BEGIN
             range[1] = range[0]
           END
           range[1] LT range[0]: BEGIN
             MESSAGE, 'Invalid range.'
           END
           range[1] GT spectrum_size-1: BEGIN
             range[1] = spectrum_size-1
           END
           ELSE:
         ENDCASE
             
         ; Sum all of the rows or columns.
         CASE 1 OF
           self.param.type EQ 'ROW': BEGIN
             FOR i = range[0], range[1] DO BEGIN
               y += image[*,i]
             ENDFOR
           END
           self.param.type EQ 'COL': BEGIN
             FOR i = range[0], range[1] DO BEGIN
               y += image[i,*]
             ENDFOR
           END
         ENDCASE

         IF self.param.do_average THEN BEGIN
           ; Divide by the number of averaged rows/columns
           y = y/(range[1]-range[0]+1)
         ENDIF

       ENDELSE
           
       x = INDGEN(spectrum_size)
           
       weight = REPLICATE(1.0, spectrum_size)

       RETURN, { $
                 x:x $
                 ,y:y $
                 ,weight:weight $
               }

     END ; FUNCTION BST_INST_DETECTOR_VIEWER::SPECTRUM





; ======================================================================
; ======================================================================
; ######################################################################
;
; EXTERNAL METHODS
;     These are methods that can be called from outside
;     of the object.  They may be used internally as well.
; 
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; Set the averaging range to be used.
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER::SET_RANGE, range, ROW=row, COL=col

       ; See if either keyword was set.
       CASE 1 OF
         ( ~ (KEYWORD_SET(row) OR KEYWORD_SET(col))): BEGIN
           type = STRUPCASE(self.type)
         END

         KEYWORD_SET(ROW): type = 'ROW'
         KEYWORD_SET(COL): type = 'COL'
       ENDCASE

       CASE type OF
         'ROW': self.param.avgrange_row = range
         'COL': self.param.avgrange_col = range      
       ENDCASE

       self->UPDATE

     END ; PRO BST_INST_DETECTOR_VIEWER::SET_RANGE



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON_GUI]
     ;
     ;-=================================================================
     PRO BST_INST_DETECTOR_VIEWER__DEFINE

       struct = { BST_INST_DETECTOR_VIEWER $

                  ,param:{ BST_INST_DETECTOR_VIEWER_PARAM $
                           ,type:'' $
                           ,do_average:0 $
                           ,avgrange_row:[0,0] $
                           ,avgrange_col:[0,0] $
                           ,image_filename:'' $
                           ,back_filename:'' $
                           ,back_subtraction:0 $
                         } $
                           
                  ,INHERITS BST_INST_ADDON_GUI }

     END ; PRO BST_INST_DETECTOR_VIEWER__DEFINE
