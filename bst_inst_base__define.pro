

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   Define a base class for classes in <BST_SPECTRAL_FIT>.
;
; DESCRIPTION:
;   The base object contains two properties:
;       core
;       guicore
;
;   These allow a standard way for objects to communicate though the
;   core and guicore.  Both are optional on initialization, and indeed
;   the guicore will often be uninitialized.  This helps with simplicity
;   given that IDL does not handle multiple inheritance well.
;
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the BST_INST_BASE object.
     ;
     ;-=================================================================
     FUNCTION BST_INST_BASE::INIT, core $
                                   ,app $
                                   ,gui $
                                   ,CORE=core_key $
                                   ,APP=app_key $
                                   ,GUICORE=gui_key

       CASE 1 OF
         ISA(core): self.SET_CORE, core
         ISA(core_key): self.SET_CORE, core_key
         ELSE:
       ENDCASE

       CASE 1 OF
         ISA(app): self.SET_APP, app
         ISA(app_key): self.SET_APP, app_key
         ELSE:
       ENDCASE
       
       CASE 1 OF
         ISA(gui): self.SET_GUI, gui
         ISA(gui_key): self.SET_GUI, gui_key
         ELSE:
       ENDCASE

       RETURN, 1

     END ;FUNCTION BST_INST_BASE::INIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the core object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_BASE::SET_CORE, core
       
       self.core = core

     END ;PRO BST_INST_BASE::SET_CORE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the app object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_BASE::SET_APP, app
       
       self.coreapp = app

     END ;PRO BST_INST_BASE::SET_APP


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the gui object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_BASE::SET_GUI, gui
       
       self.coregui = gui

     END ;PRO BST_INST_BASE::SET_GUI


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the core object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_BASE::GET_CORE
       
       RETURN, self.core

     END ;FUNCTION BST_INST_BASE::GET_CORE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the app object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_BASE::GET_APP, app
       
       RETURN, self.coreapp

     END ;FUNCTION BST_INST_BASE::GET_APP


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the gui object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_BASE::GET_GUI, gui
       
       RETURN, self.coregui

     END ;FUNCTION BST_INST_BASE::GET_GUI


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the gui object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_BASE::GET_ADDON, addon

       IF ~ISA(self.coreapp) THEN BEGIN
         MESSAGE, "BST_SPECTRAL app not defined.  Cannot get addons."
       ENDIF
       
       RETURN, self.coreapp.GET_ADDON(addon)

     END ;FUNCTION BST_INST_BASE::GET_ADDON


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the gui object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_BASE::GET_GUIADDON, guiaddon
       
       IF ~ISA(self.coregui) THEN BEGIN
         MESSAGE, "BST_SPECTRAL gui not defined.  Cannot get guiaddons."
       ENDIF
       
       RETURN, self.coregui.GET_GUIADDON(guiaddon)

     END ;FUNCTION BST_INST_BASE::GET_GUIADDON

     
     ;+=================================================================
     ; PURPOSE:
     ;   Create the BST_INST_BASE object.
     ;
     ;-=================================================================
     PRO BST_INST_BASE__DEFINE

       bst_inst_base= $
          { BST_INST_BASE $

            ,core:OBJ_NEW() $
            ,coreapp:OBJ_NEW() $
            ,coregui:OBJ_NEW() $

            ,INHERITS OBJECT $
          }

     END ;BST_INST_BASE__DEFINE
