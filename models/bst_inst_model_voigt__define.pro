



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2012-04
;
; PURPOSE:
;   This object handels the voigt model for 
;   <BST_SPECTRAL_FIT>.
;
; DESCRIPTION:
;
; PARAMETERS:
;   
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_VOIGT::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_VOIGT::SET_FLAGS



     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints before each evaluation of the model.
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_VOIGT::EVALUATE_CONSTRAINTS

       ; Nothing to do here

     END ; PRO BST_INST_MODEL_VOIGT::EVALUATE_CONSTRAINTS



     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the voigt model.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_VOIGT::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))

       voigt = OBJ_NEW('MIR_VOIGT' $
                       ,{intensity:self.param.intensity $
                         ,location:self.param.location $
                         ,sigma:self.param.sigma $
                         ,gamma:self.param.gamma $
                        } $
                      )

       y += voigt->EVALUATE(x, CUTOFF=self.options.evaluation_cutoff)

       RETURN, y

     END ;PRO BST_INST_MODEL_VOIGT::EVALUATE


     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_VOIGT::SET_DEFAULTS

       self.options.evaluation_cutoff = 1D-16

       self.param = { BST_INST_MODEL_VOIGT__PARAM }

     END ;PRO BST_INST_MODEL_VOIGT::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_VOIGT::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN
         self->SET_DEFAULTS
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_VOIGT__FIXED }
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_VOIGT__LIMITED }
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_VOIGT__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_VOIGT::RESET



     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_VOIGT::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{options:self->GET_OPTIONS()} $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_VOIGT::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_VOIGT::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state

       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->SET_OPTIONS, state.options
       ENDIF ELSE BEGIN
         self->MESSAGE, 'Could not load options.', /ERROR
         CATCH, /CANCEL
       ENDELSE


     END ;PRO BST_INST_MODEL_VOIGT::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the options of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_VOIGT::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_VOIGT::GET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model options
     ;-=================================================================
     PRO BST_INST_MODEL_VOIGT::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_VOIGT::SET_OPTIONS



     ;+=========================================================================
     ; PURPOSE:
     ;   Define the VOIGT model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_VOIGT__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_VOIGT__PARAM $
                 ,intensity:0D $
                 ,location:0D $
                 ,sigma:0D $
                 ,gamma:0D $
               } 

       sigma = { BST_INST_MODEL_VOIGT__SIGMA $
                 ,intensity:0D $
                 ,location:0D $
                 ,sigma:0D $
                 ,gamma:0D $
               } 


       fixed = { BST_INST_MODEL_VOIGT__FIXED $
                 ,intensity:0 $
                 ,location:0 $
                 ,sigma:0 $
                 ,gamma:0 $
               } 
       
       limited = { BST_INST_MODEL_VOIGT__LIMITED $
                   ,intensity:[0, 0] $
                   ,location:[0, 0] $
                   ,sigma:[0, 0] $
                   ,gamma:[0, 0] $
                 }

       limits = { BST_INST_MODEL_VOIGT__LIMITS $
                  ,intensity:[0D, 0D] $
                  ,location:[0D, 0D] $
                  ,sigma:[0D, 0D] $
                  ,gamma:[0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Setup additional model data.

       options = { BST_INST_MODEL_VOIGT__OPTIONS $
                   ,evaluation_cutoff:0D $
                 }


       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_VOIGT $

                 ,param:{ BST_INST_MODEL_VOIGT__PARAM} $

                 ,sigma:{ BST_INST_MODEL_VOIGT__SIGMA} $

                 ,fixed:{ BST_INST_MODEL_VOIGT__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_VOIGT__LIMITED } $

                 ,limits:{ BST_INST_MODEL_VOIGT__LIMITS } $

                 ,options:{ BST_INST_MODEL_VOIGT__OPTIONS } $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_VOIGT
