



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object handels the gaussian model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit gaussians to the the spectrum.
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_GAUSSIAN::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 0
       self.need_evaluate = 1
       self.gaussian = 1


     END ; PRO BST_INST_MODEL_GAUSSIAN::SET_FLAGS



     ;+=================================================================
     ; Here we set any inital values
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::INIT, dispatcher

       status = self->BST_INST_MODEL_PARAMETER::INIT(dispatcher)

       self.num_param = 3

       RETURN, status

     END ;PRO BST_INST_MODEL_GAUSSIAN::INIT



     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fix_index & gui_id untouched
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_GAUSSIAN::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,SIGMA=sigma $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,SIGMA=sigma $
         ,ALL=all


       IF all THEN BEGIN
         self.profile = 0
       ENDIF

     END ;PRO BST_INST_MODEL_GAUSSIAN::RESET


     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the gaussian.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::EVALUATE, x

       ; Check the profile number.  
       ; If it is less that zero, don't evaluate.
       IF self.profile LT 0 THEN RETURN, DBLARR(N_ELEMENTS(x))

       gaussian = OBJ_NEW('GAUSSIAN', self.param)
       y = gaussian->EVALUATE(x, CUTOFF=self.evaluation_cutoff)
       
       RETURN, y

     END ;PRO BST_INST_MODEL_GAUSSIAN::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   get a gaussian with the model parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::GET_GAUSSIAN
       
       RETURN, OBJ_NEW('GAUSSIAN', self.param)

     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::GET_GAUSSIAN



     ;+=================================================================
     ; PURPOSE:
     ;   Get the profile number.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::GET_PROFILE_NUMBER
       
       RETURN, self.profile

     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::GET_PROFILE_NUMBER



     ;+=================================================================
     ; PURPOSE:
     ;   Set the profile number.
     ;-=================================================================
     PRO BST_INST_MODEL_GAUSSIAN::SET_PROFILE_NUMBER, profile_num
       
       self.profile = profile_num

     END ;PRO BST_INST_MODEL_GAUSSIAN::SET_PROFILE_NUMBER



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the 
     ;   gaussians from the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::GET_GAUSSIANS


       sum_of_gauss = OBJ_NEW('BST_INST_PROFILE')

       sum_of_gauss->APPEND, (self->GET_GAUSSIAN())->GET_COPY()
  
       RETURN, sum_of_gauss
     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::GET_GAUSSIANS



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the Gaussians
     ;   for the given profile in the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::GET_PROFILE, profile_num
        
       sum_of_gauss = OBJ_NEW('BST_INST_PROFILE')

       IF self->HAS_PROFILE(profile_num) THEN BEGIN
         sum_of_gauss->APPEND, (self->GET_GAUSSIAN())->GET_COPY()
       ENDIF

       RETURN, sum_of_gauss

     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::GET_PROFILE, profile_num



     ;+=================================================================
     ; PURPOSE:
     ;   Return whether or not this model has gaussians associated
     ;   with the given profile number.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::HAS_PROFILE, profile_num
       RETURN, (self.profile EQ profile_num)
     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::HAS_PROFILE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list with the numbers of any profiles contained
     ;   within the model.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::GET_PROFILE_NUMBER_LIST
       list = OBJ_NEW('SIMPLELIST')
       list->APPEND, self.profile

       RETURN, list
     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::GET_PROFILE_NUMBER_LIST



     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_GAUSSIAN::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state, {profile:self.profile})

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_GAUSSIAN::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_GAUSSIAN::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state
       STRUCT_ASSIGN, {profile:state.profile}, self, /NOZERO

     END ;PRO BST_INST_MODEL_GAUSSIAN::LOAD_STATE



     ;+=================================================================
     ; Here we set up the object that will hold a gaussian parameter
     ;
     ; Note that this must also hold some of the gui parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_GAUSSIAN__DEFINE

       struct ={ BST_INST_MODEL_GAUSSIAN $

                 ,profile:0 $

                 ,param:{ BST_INST_MODEL_GAUSSIAN__PARAM $
                          ,amplitude:0D $
                          ,width:0D $
                          ,location:0D $
                        } $

                 ,sigma:{ BST_INST_MODEL_GAUSSIAN__SIGMA $
                          ,amplitude:0D $
                          ,width:0D $
                          ,location:0D $
                        } $

                 ,fixed:{ BST_INST_MODEL_GAUSSIAN__FIXED $
                          ,amplitude:0 $
                          ,width:0 $
                          ,location:0 $
                        } $

                 ,limited:{ BST_INST_MODEL_GAUSSIAN__LIMITED $
                            ,amplitude:[0,0] $
                            ,width:[0,0] $
                            ,location:[0,0] $
                          } $

                 ,limits:{ BST_INST_MODEL_GAUSSIAN__LIMITS $
                            ,amplitude:[0D,0D] $
                            ,width:[0D,0D] $
                            ,location:[0D,0D] $
                          } $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_GAUSSIAN__DEFINE
