


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This is a base class for all models used in <BST_SPECTRAL_FIT> that
;   have free parameters.
;
;   These models are the the core of <BST_SPECTRAL_FIT>.
;   Each model must do a number of things:
;     1. Provide fit variables, with appropriate limits to the fitter.
;     2. Evaluate their contribution to the spectrum.
;
;   Models may communicate with each other.
;
; DESCRIPTION:
;   All model objects used in <BST_SPECTRAL_FIT> that have free parameters
;   should inherit this base model class.
;
;   The best way to get started is to look at an existing model
;   and follow allong.
;
; 
; DATA FOR CHILD OBJECTS:
;   All child objects should have the following properties in their definition.
;
;     param
;
;     fixed
;
;     limited
;
;     limits
;
;     SIGMA
;
;
;
; METHODS FOR CHILD OBJECTS:
;   All model objects (those that inherit this object) should have
;   the following methods:
;
;
;
;-==============================================================================






     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the model object upon instantiation
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_PARAMETER::INIT, dispatcher


       status = self->BST_INST_MODEL::INIT(dispatcher)

       RETURN, status

     END ;FUNCTION BST_INST_MODEL_PARAMETER::INIT



     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the model object.
     ;-=========================================================================
     PRO BST_INST_MODEL_PARAMETER::INITIALIZE

       self.BST_INST_MODEL::INITIALIZE

       self.num_param = N_ELEMENTS_STRUCTURE(self.param)

     END ;PRO BST_INST_MODEL_PARAMETER::INITIALIZE



;=======================================================================
;=======================================================================
;#######################################################################
;
; MODEL STATE ROUTINES
;
;#######################################################################
;=======================================================================
;=======================================================================


     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the gui_id untouched
     ;
     ; NOTE: num_param is a parameter set in the INIT method.
     ;       It is instrinsic to the model and should never be 
     ;       changed or reset.
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::RESET, FIXED=fixed $
                                          ,LIMITS=limits $
                                          ,LIMITED=limited $
                                          ,PARAMS=params $
                                          ,SIGMA=sigma $
                                          ,ALL=all

       self->BST_INST_MODEL::RESET, ALL=all $
                                    ,FIXED=fixed

       self->CHECK_RESET_KEYWORDS, FIXED=fixed $
                                   ,LIMITS=limits $
                                   ,LIMITED=limited $
                                   ,PARAMS=params $
                                   ,SIGMA=sigma $
                                   ,ALL=all

       empty_struct = CREATE_STRUCT(NAME=OBJ_CLASS(self))

       IF params OR all THEN BEGIN
         self.param = empty_struct.param
       ENDIF

       IF fixed OR all THEN BEGIN
         self.fixed = empty_struct.fixed
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = empty_struct.limited
       ENDIF

       IF limits OR all THEN BEGIN
         self.limits = empty_struct.limits
       ENDIF

       IF sigma OR all THEN BEGIN
         self.sigma = empty_struct.sigma
       ENDIF
       

     END ;PRO BST_INST_MODEL_PARAMETER::RESET




     ;+=================================================================
     ; This routine resets all of the object parameters.
     ;
     ; IMPORTANT: The way this works is:
     ;  1. This routine calls self->RESET
     ;     This will be the RESET function of the individual model,
     ;     i.e. BST_INST_MODEL_GAUSSIAN::RESET, not BST_INST_MODEL::RESET.
     ;
     ;  2. The individual model reset function will call
     ;     BST_INST_MODEL::RESET
     ;
     ;  3. We reset what ever is left.
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::RESET_ALL
       
       self->RESET 

     END ;PRO BST_INST_MODEL_PARAMETER::RESET_ALL




     ;+=================================================================
     ; 
     ; This routine will check if any of the reset keywords are set.
     ; If not it will set them all to true.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::CHECK_RESET_KEYWORDS $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,SIGMA=sigma $
                       ,ALL=all
       
       fixed = KEYWORD_SET(fixed)
       limits = KEYWORD_SET(limits)
       limited = KEYWORD_SET(limited)
       params = KEYWORD_SET(params)
       sigma = KEYWORD_SET(sigma)
       all = KEYWORD_SET(all)

       keyword_array = [ $
                         fixed $
                         ,limits $
                         ,limited $
                         ,params $
                         ,sigma $
                       ]

       IF TOTAL(keyword_array) EQ 0 THEN BEGIN
         all=1
       ENDIF

     END ;PRO BST_INST_MODEL_PARAMETER::CHECK_RESET_KEYWORDS


     ;+=================================================================
     ; PURPOSE:
     ;   Print out the parameters of the model.
     ;
     ; DESCRIPTION:
     ;   This should be reimplemented by derived classes when
     ;   appropriate.
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::PRINT, param $
                                          ,sigma $
                                          ,LEVEL=level
       
       MIR_DEFAULT, param, self.param
       MIR_DEFAULT, sigma, self.sigma
       MIR_DEFAULT, level, 0

       num_tags = N_TAGS(param)
       labels = TAG_NAMES(param)
      
       indent = 4
       label_width = 20

       fmt_i = STRING(indent * level + 1, FORMAT='(i0, "x")')
       fmt_lw = STRING(label_width, FORMAT='(i0)')
      
       fmt_nested = '('+fmt_i+', a-'+fmt_lw+')'
       fmt_values = '('+fmt_i+', a-'+fmt_lw+', 2x, e15.5, "  (",e8.2,")")'

       FOR ii_tag=0,num_tags-1 DO BEGIN
         
         IF ISA(param.(ii_tag), 'STRUCT') THEN BEGIN
           self.MESSAGE, STRING(labels[ii_tag], FORMAT=fmt_nested)
           self.PRINT, param.(ii_tag), sigma.(ii_tag), LEVEL=level+1
         ENDIF ELSE BEGIN
           self.MESSAGE, STRING(labels[ii_tag], (param.(ii_tag))[0], (sigma.(ii_tag))[0], FORMAT=fmt_values)

           num_elements = N_ELEMENTS(param.(ii_tag))
           FOR ii_arr=1, num_elements-1 DO BEGIN
             self.MESSAGE, STRING('', (param.(ii_tag))[ii_arr], (sigma.(ii_tag))[ii_arr], FORMAT=fmt_values)
           ENDFOR

         ENDELSE

       ENDFOR

     END ;PRO BST_INST_MODEL_PARAMETER::PRINT





;=======================================================================
;=======================================================================
;#######################################################################
;
; FITTING LOOP ROUTINES
;
;#######################################################################
;=======================================================================
;=======================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Return an array of <BST_INST_PARAMETER_INFO> sructures.  This is the
     ;   array eventually needed for input into <MPFIT>.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_PARAMETER::GET_PARAMETER_INFO

       num_param = self->NUM_PARAM()
       parameter_info = REPLICATE({BST_INST_PARAMETER_INFO}, num_param)

       self->FILL_PARAMETER_INFO, self.param $
                                  ,self.fixed $
                                  ,self.limited $
                                  ,self.limits $
                                  ,parameter_info

       self->SET_PARAMETER_INFO_FLAGS, parameter_info

       RETURN, parameter_info

     END ; FUNCTION BST_INST_MODEL_PARAMETER::GET_PARAMETER_INFO


     ;+=========================================================================
     ; PURPOSE:
     ;   Fill the parameter_info structure.
     ;
     ;   This will only work (I think) if each tag in the self.param structure 
     ;   corresponds to a single value (as opposed to an array).
     ;
     ;   If arrays are used within param, then this method should be 
     ;   reimplemented. (or at least this method sholud be tested/fixed.)
     ;
     ; DESCRIPTION:
     ;   Here we assume that parinfo is given as array of 
     ;   <BST_INST_PARAMETER_INFO::> structures.
     ;
     ;   This method is very similar to the function <STRUCTURE_FILL_ARRAY>.  
     ;
     ;   Note that this is a recursive method.
     ;-=========================================================================
     PRO BST_INST_MODEL_PARAMETER::FILL_PARAMETER_INFO, param $
                                                        ,fixed $
                                                        ,limited $
                                                        ,limits $
                                                        ,parinfo $
                                                        ,INDEX=index


       num_tags = N_TAGS(param)

       MIR_DEFAULT, index, 0

       FOR ii=0,num_tags-1 DO BEGIN

         IF SIZE(param.(ii), /TYPE) EQ 8 THEN BEGIN
           self->FILL_PARAMETER_INFO, param.(ii) $
                                      ,fixed.(ii) $
                                      ,limited.(ii) $
                                      ,limits.(ii) $
                                      ,parinfo $
                                      ,INDEX=index
         ENDIF ELSE BEGIN
           num_elements = N_ELEMENTS(param.(ii))

           parinfo[index:index+num_elements-1].value = param.(ii)
           parinfo[index:index+num_elements-1].fixed = fixed.(ii)
           parinfo[index:index+num_elements-1].limited = limited.(ii)
           parinfo[index:index+num_elements-1].limits = limits.(ii)

           index += num_elements
         ENDELSE

       ENDFOR

     END ; PRO BST_INST_MODEL_PARAMETER::FILL_PARAMETER_INFO



     ;+=========================================================================
     ; PURPOSE:
     ;   Extract the parameters from the fit_array back into the object
     ;   structure.
     ;-=========================================================================
     PRO BST_INST_MODEL_PARAMETER::EXTRACT_PARAMETER_ARRAY, parameter_array

       self.param = STRUCTURE_FILL_FROM_ARRAY(self.param, parameter_array, /NO_COPY)

     END ;PRO BST_INST_MODEL_PARAMETER::EXTRACT_PARAMETER_ARRAY



     ;+=========================================================================
     ; PURPOSE:
     ;   Extract the parameters from the sigma_array back into the object
     ;   structure.
     ;-=========================================================================
     PRO BST_INST_MODEL_PARAMETER::EXTRACT_SIGMA_ARRAY, sigma_array

       self.sigma = STRUCTURE_FILL_FROM_ARRAY(self.sigma, sigma_array, /NO_COPY)

     END ;PRO BST_INST_MODEL_PARAMETER::EXTRACT_SIGMA_ARRAY


;=======================================================================
;=======================================================================
;#######################################################################
;
; PARAMTER ACCESS
;
;#######################################################################
;=======================================================================
;=======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the parameters of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_PARAMETER::GET_PARAM
       
       RETURN, self.param

     END ;PRO BST_INST_MODEL_PARAMETER::GET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::SET_PARAM, param
       
       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ;PRO BST_INST_MODEL_PARAMETER::SET_PARAM




     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the parameters of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_PARAMETER::GET_SIGMA
       
       RETURN, self.sigma

     END ;PRO BST_INST_MODEL_PARAMETER::GET_SIGMA



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::SET_SIGMA, sigma
       
       STRUCT_ASSIGN, {sigma:sigma}, self, /NOZERO

     END ;PRO BST_INST_MODEL_PARAMETER::SET_SIGMA


     ;+=================================================================
     ; PURPOSE;
     ;   This will return a structure with all of the properties of
     ;   the model object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_PARAMETER::GET_STATE

       RETURN, { $
                 version:self.version $
                 
                 ,use:self.use $
                 ,fixall:self.fixall $
                 ,save:self.save $

                 ,param:self->GET_PARAM() $
                 ,fixed:self->GET_FIXED() $
                 ,limited:self->GET_LIMITED() $
                 ,limits:self->GET_LIMITS() $
                 ,sigma:self->GET_SIGMA() $
               }

     END ;FUNCTION BST_INST_MODEL_PARAMETER::GET_STATE


     ;+=================================================================
     ;
     ; Load the state for the this model.
     ;
     ;   By default this will load all of the object properties from
     ;   the loaded state structure except for:
     ;
     ;   dispatcher
     ;   model_active
     ;   version
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::LOAD_STATE, state
       
       self.use = state.use
       self.fixall = state.fixall
       self.save = state.save

       self->SET_PARAM, state.param
       self->SET_FIXED, state.fixed
       self->SET_LIMITED, state.limited
       self->SET_LIMITS, state.limits
       self->SET_SIGMA, state.sigma


     END ;PRO BST_INST_MODEL_PARAMETER::LOAD_STATE


     ;+=================================================================
     ; PURPOSE
     ;   Restore the initial state of the model after a fit.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_PARAMETER::RESTORE_INITIAL_STATE, model_initial $
                                                          ,UPDATE_PARAMETERS=update_param

       num_param = self->NUM_PARAM()

       ; If save was set save the new parameter values.
       IF model_initial->SAVE() AND (num_param GT 0) AND KEYWORD_SET(update_param) THEN BEGIN
         new_params = self->GET_PARAM()
         new_sigma = self->GET_SIGMA()
       ENDIF


       ; Copy the initial model parameters to the model.
       self->COPY_FROM, model_initial, /RECURSIVE


       ; If save was set save the parameter values.
       IF model_initial->SAVE() AND (num_param GT 0) AND KEYWORD_SET(update_param) THEN BEGIN
         self->SET_PARAM, new_params
         self->SET_SIGMA, new_sigma
       ENDIF

     END ;PRO BST_INST_MODEL_PARAMETER::RESTORE_INITIAL_STATE



;=======================================================================
;=======================================================================
;#######################################################################
;
; OBJECT DEFINITION
;
;#######################################################################
;=======================================================================
;=======================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Here we set up the base model class.  
     ;
     ;   All models should be derived from this class
     ; 
     ; DESCRIPTION:
     ;
     ;   All models derived from the class must define the 
     ;   following properties:
     ;
     ;   param
     ;   fixed
     ;   limited
     ;   limits
     ;   sigma
     ;
     ;   See one of the exsting models for an example of how this is 
     ;   done.
     ;-=========================================================================
     PRO BST_INST_MODEL_PARAMETER__DEFINE

       struct ={ BST_INST_MODEL_PARAMETER $
                ,INHERITS BST_INST_MODEL $
              }


      ; All models derived from the class must define the 
      ; following properties:
      ;
      ;  param
      ;  fixed
      ;  limited
      ;  limits
      ;  sigma

     END ;PRO BST_INST_MODEL_PARAMETER__DEFINE

