

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This model allows for convolution of the gaussian model with
;   and instrumental responce.
;
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::SET_FLAGS

       self->BST_INST_MODEL::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 0
       self.need_evaluate = 1

       self.save = 0
       self.use = 0


     END ; PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;
     ; DESCRIPTION:
     ;   This model has no parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_CONVOLVE_GAUSSIANS::INIT, dispatcher

       status = self->BST_INST_MODEL::INIT(dispatcher)

       self.num_param = 0

       RETURN, status

     END ;PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Reset to defaults.
     ;-=================================================================
     PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::RESET, ALL=all, _REF_EXTRA=extra

       self->BST_INST_MODEL::RESET, ALL=all, _STRICT_EXTRA=extra
       
       IF all THEN BEGIN
         self.save = 0
         self.use = 1
       ENDIF

     END ; PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::RESET



     ;+=================================================================
     ; PURPOSE:
     ;   Setup constaints.
     ;-=================================================================
     PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::PREFIT_CONSTRAINTS

       IF ~ self->USE() THEN RETURN
         
       CATCH, error
       IF error EQ 0 THEN BEGIN
         model_gauss_multi = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')
         model_gauss_multi->SET_EVALUATE, 0

       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         (self.dispatcher)->MESSAGE, 'Could not turn off evaluation for the GAUSSIAN models.'
       ENDELSE

     END ;PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::PREFIT_CONSTRAINTS




     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the models.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_CONVOLVE_GAUSSIANS::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))


       model_gauss_multi = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')

       num_gauss = model_gauss_multi->NUM_MODELS()
       FOR ii=0,num_gauss-1 DO BEGIN
         model = model_gauss_multi->GET_MODEL(ii)

         ; #########################
         ; Do the convolution here!
         ; #########################


         y += model->EVALUATE(x)

       ENDFOR


       RETURN, y


     END ;FUNCTION BST_INST_MODEL_CONVOLVE_GAUSSIANS::EVALUATE




     ;+=================================================================
     ; PURPOSE;
     ;   This will return a structure with all of the properties of
     ;   this model.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_CONVOLVE_GAUSSIANS::GET_STATE

       RETURN, { $
                 version:self.version $
                 
                 ,use:self.use $
                 ,save:self.save $
               }

     END ;FUNCTION BST_INST_MODEL_CONVOLVE_GAUSSIANS::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the state for the this model.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::LOAD_STATE, state
       
       self.use = state.use
       self.save = state.save

     END ;PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the model object.
     ;
     ;   For now don't store anything.
     ;-=================================================================
     PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS__DEFINE

       struct ={ BST_INST_MODEL_CONVOLVE_GAUSSIANS $

                 ,INHERITS BST_INST_MODEL $
               }


     END ;PRO BST_INST_MODEL_CONVOLVE_GAUSSIANS__DEFINE
