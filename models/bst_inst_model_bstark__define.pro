


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object handels the bstark profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit bstark profiles to the spectrum.
;
; REQUIREMENTS:
;   CERVIEW must be running.
;   This model must have access to the B-Stark IDL library.
;
; TO DO:
;   The fix_central_line_width option is not handled properly.
;   It should be handeled more similarly to the no_field option.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_BSTARK::SET_FLAGS



     ;+=================================================================
     ; Here we set any inital values
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::INIT, dispatcher

       
       RESOLVE_ROUTINE, [ $
                           'bst_stark_model' $
                          ,'bst_dispersion' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE

       status = self->BST_INST_MODEL_PARAMETER::INIT(dispatcher)

       self.num_levels = N_ELEMENTS(self.param.level_population.(0))
       self.num_components = N_TAGS(self.param.level_population)
       self.num_lines = N_ELEMENTS(self.param.profile_width.(0))

       RETURN, status

     END ;PRO BST_INST_MODEL_BSTARK::INIT



     ;+=================================================================
     ; PURPOSE
     ;   Setup the model prior to starting the fit loop
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::PREFIT

       BST_CHORD_PARAM, self.shot_param.shot $
                        ,self.shot_param.chord $
                        ,self.shot_param.beam

     END ;FUNCTION BST_INST_MODEL_BSTARK::PREFIT




     ;+=================================================================
     ; PURPOSE
     ;   Setup the model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::PREFIT_CONSTRAINTS

       self.fixed.profile_width_slope.full = 1
       self.fixed.profile_width_slope.half = 1
       self.fixed.profile_width_slope.third = 1

       IF self.options.no_field THEN BEGIN
         self.fixed.er_eff = 1
         self.param.er_eff = 0D

         self.fixed.ratio = 1
         self.param.ratio = 1D

         ; Leave only the 4'th population level free.
         ; This level only contributes to the central stark line.
         self.fixed.level_population.full[0:2] = 1
         self.fixed.level_population.full[4:*] = 1
         self.param.level_population.full[0:2] = 0D
         self.param.level_population.full[4:*] = 0D
 

         self.fixed.level_population.half[0:2] = 1
         self.fixed.level_population.half[4:*] = 1
         self.param.level_population.half[0:2] = 0D
         self.param.level_population.half[4:*] = 0D

         
         self.fixed.level_population.third[0:2] = 1
         self.fixed.level_population.third[4:*] = 1
         self.param.level_population.third[0:2] = 0D
         self.param.level_population.third[4:*] = 0D


         self.fixed.level_population.water[0:2] = 1
         self.fixed.level_population.water[4:*] = 1
         self.param.level_population.water[0:2] = 0D
         self.param.level_population.water[4:*] = 0D


         ; Leave only the 5'th (central) line width free.
         self.fixed.profile_width.full[0:3] = 1
         self.fixed.profile_width.full[5:*] = 1

         self.fixed.profile_width.half[0:3] = 1
         self.fixed.profile_width.half[5:*] = 1

         self.fixed.profile_width.third[0:3] = 1
         self.fixed.profile_width.third[5:*] = 1

         self.fixed.profile_width.water[0:3] = 1
         self.fixed.profile_width.water[5:*] = 1

       ENDIF
  

       IF self.options.pop_levels_from_full THEN BEGIN
         ;Only fit the total intensity for the half and third components
         self.fixed.level_population.half[0:2] = 1
         self.fixed.level_population.half[4:*] = 1

         self.fixed.level_population.third[0:2] = 1
         self.fixed.level_population.third[4:*] = 1
       ENDIF

       IF self.options.pop_levels_symmetric THEN BEGIN
         self.fixed.level_population.full[4:5] = 1
         self.fixed.level_population.half[4:5] = 1
         self.fixed.level_population.third[4:5] = 1
       ENDIF

       IF self.options.pop_levels_calculated THEN BEGIN
         self.fixed.level_population.full[0:2] = 1
         self.fixed.level_population.full[4:*] = 1

         self.fixed.level_population.half[0:2] = 1
         self.fixed.level_population.half[4:*] = 1

         self.fixed.level_population.third[0:2] = 1
         self.fixed.level_population.third[4:*] = 1
       ENDIF


       IF self.options.fix_central_line_width THEN BEGIN
         ; Fix the width of the 5'th (central) line.
         self.fixed.profile_width.full[4] = 1
         self.fixed.profile_width.half[4] = 1
         self.fixed.profile_width.third[4] = 1
         self.fixed.profile_width.water[4] = 1
       ENDIF


       ; Deal with the water line.
       self.fixed.profile_width.water[*] = 1
       IF self.options.include_water_line THEN BEGIN
         ; There is not enough resolution or intensity to fit the population 
         ; levels for the water line.  So just fit the total intensity.
         self.fixed.level_population.water[0:2] = 1
         self.fixed.level_population.water[4:*] = 1
       ENDIF ELSE BEGIN
         ; If we are not including the water line, then fix all the 
         ; population levels.
         self.fixed.level_population.water[*] = 1
       ENDELSE


     END ;FUNCTION BST_INST_MODEL_BSTARK::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints before each evaluation of the model.
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::EVALUATE_CONSTRAINTS

       IF self.options.include_water_line THEN BEGIN
         ; Use the relative populations levels from the full component scaled
         ; to the water component.
         self.param.level_population.water = self.param.level_population.full $
                                             / self.param.level_population.full[3] $
                                             * self.param.level_population.water[3]
       ENDIF


       IF self.options.pop_levels_from_full THEN BEGIN
         self.param.level_population.half = self.param.level_population.full $
                                             / self.param.level_population.full[3] $
                                             * self.param.level_population.half[3]

         self.param.level_population.third = self.param.level_population.full $
                                             / self.param.level_population.full[3] $
                                             * self.param.level_population.third[3]
       ENDIF

       IF self.options.pop_levels_symmetric THEN BEGIN
         self.param.level_population.full[5] = self.param.level_population.full[0]
         self.param.level_population.full[4] = self.param.level_population.full[1]

         self.param.level_population.half[5] = self.param.level_population.half[0]
         self.param.level_population.half[4] = self.param.level_population.half[1]

         self.param.level_population.third[5] = self.param.level_population.third[0]
         self.param.level_population.third[4] = self.param.level_population.third[1]
       ENDIF

       IF self.options.pop_levels_calculated THEN BEGIN
         self.param.level_population.full = self.param.level_population.full[3]
         self.param.level_population.half = self.param.level_population.half[3]
         self.param.level_population.third = self.param.level_population.third[3]
       ENDIF

     END ; PRO BST_INST_MODEL_BSTARK::EVALUATE_CONSTRAINTS



     ;+=================================================================
     ; Evaluate the bstark profile.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::EVALUATE, x, COMPONENTS=components

       y = DBLARR(N_ELEMENTS(x))

       ; Get a scaled object with all of the model lines.
       scaled_list = self->GET_SCALED_LIST(COMPONENTS=components)

       ; Evaluate the model.
       FOR ii=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
         scaled = scaled_list->GET(ii)

         y += scaled->EVALUATE(x,CUTOFF=self.evaluation_cutoff)
       ENDFOR


       ; Destroy the sclaled list.
       FOR ii=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
         scaled = scaled_list->GET(ii)

         scaled->DESTROY
       ENDFOR
       scaled_list->DESTROY


       RETURN, y

     END ;PRO BST_INST_MODEL_BSTARK::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Perform any actions after the fit loop is complete.
     ;
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to do anything after the fit loop is complete.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::POSTFIT

       self->EVALUATE_CONSTRAINTS

     END ;FUNCTION BST_INST_MODEL_BSTARK::POSTFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list of scaled objects for each of the lines
     ;   in the stark model.
     ;
     ; PROGRAMMING NOTES:
     ;   The profiles used for the line shape are normalized to have
     ;   an intensity of 1 before being added to the scaled objects.
     ;   This is especially important if the line shape is allowed to
     ;   vary in the fits.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_SCALED_LIST, COMPONENTS=components

       ; Reload the chord parameters.
       ;   NOTE: The parameters are cached, so this is fast.
       BST_CHORD_PARAM, self.shot_param.shot $
                        ,self.shot_param.chord $
                        ,self.shot_param.beam


       ; Get the line locations and intensities.
       lines = self->GET_STARK_LINES()




       scaled_list = OBJ_NEW('SIMPLELIST')


       ; Chose which profiles to return. By default return all lines.
       IF ~ ISA(components) THEN BEGIN
         ; We assume that the tags in lines will be in the order: full, half, third, water.
         components = INDGEN(N_TAGS(lines))
       ENDIF
       

       ; Here we will assume the profile numebers are:
       ; full: -1
       ; half: -2
       ; third: -3
       ; water: -4
       FOR ii=0,N_ELEMENTS(components)-1 DO BEGIN
         profile_num = components(ii)

         ; Get the profile.
         profile = self->GET_BEAM_PROFILE(profile_num)

         ; Normalize the profile.
         profile->NORMALIZE

         FOR jj=0,self.num_lines-1 DO BEGIN
           scaled = OBJ_NEW('BST_INST_SCALED')
           scaled->SET_PROFILE, profile

           param = {amplitude:lines.(profile_num).intensity[jj] $
                    ,width:self.param.profile_width.(profile_num)[jj] $
                    ,location:lines.(profile_num).location[jj] $
                   }

           scaled->SET_PARAM, param

           scaled_list->APPEND, scaled
         ENDFOR
       ENDFOR

       RETURN, scaled_list

     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_SCALED_LIST




     ;+=================================================================
     ; PURPOSE:
     ;   Return the profile for the requested beam component.
     ;
     ; DESCRIPTION:
     ;   Here we will assume the profile numbers are:
     ;   full:  -1
     ;   half:  -2
     ;   third: -3
     ;   water: -4
     ;
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_BEAM_PROFILE, component_in $
                                                       ,FULL=full $
                                                       ,HALF=half $
                                                       ,THIRD=third $
                                                       ,WATER=water $
                                                       ,CONTINUUM=continuum

       ; Parse the inputs.
       IF ISA(component_in) THEN BEGIN
         component = component_in
       ENDIF ELSE BEGIN
         CASE 1 OF
           KEYWORD_SET(full): component = 0
           KEYWORD_SET(half): component = 1
           KEYWORD_SET(third): component = 2
           KEYWORD_SET(water): component = 3
           KEYWORD_SET(continuum): component = 4
           ELSE: MESSAGE, 'No component given.'
         ENDCASE
       ENDELSE


       ; Get a reference to the profiles object.
       profile_dispatch = (self.dispatcher)->GET_PROFILES_OBJECT()


       ; Assume the profile numbers are:
       ; full:  -1
       ; half:  -2
       ; third: -3
       profile_number = -1*(component + 1)
       
       ; First we get the profile object for the given profile number.
       profile = profile_dispatch->GET_PROFILE(profile_number)
  



       ; ---------------------------------------------------------------
       ; I need to choose how the line profile locations are determined 
       ; based on where the profiles are taken from.  
       ; 
       ; If the profiles are taken directly for the Gaussian and scaled 
       ; models, then I should use the line centroid excluding stray 
       ; Gaussians. 
       ;
       ; If taken from the profiles model I shoud assume that the 
       ; profiles are centered at zero.
       ;
       
       model_profile = self.dispatcher->GET_MODEL('BST_INST_MODEL_PROFILE_MULTI')
       IF model_profile->HAS_PROFILE(profile_number)  THEN BEGIN
         use_scaled_location = 0
         use_zero_location = 1
       ENDIF ELSE BEGIN
         use_scaled_location = 1
         use_zero_location = 0
       ENDELSE

       CASE 1 OF
         use_scaled_location: BEGIN
           ; Use an explicitly set value for the center location.
           profile->SET_USE_CENTER

           ; By default use the centroid as the center location.
           profile->SET_CENTER, profile->CENTROID()

           ; If this profile includes a scaled profile, then use the
           ; location of the scaled profile as the center of the profile.
           model_scaled_multi = self.dispatcher->GET_MODEL('BST_INST_MODEL_SCALED_MULTI')
           num_scaled = model_scaled_multi->NUM_MODELS()

           FOR ii=0,num_scaled-1 DO BEGIN
             model = model_scaled_multi->GET_MODEL(ii)
             IF model->USE() THEN BEGIN
               IF model->HAS_PROFILE(profile_number) THEN BEGIN
                 param = model->GET_PARAM()
                 profile->SET_CENTER, param.location
               ENDIF
             ENDIF
           ENDFOR
         END
       
         
         use_zero_location: BEGIN
           ; Use an explicitly set value for the center location.
           profile->SET_USE_CENTER
           
           ; Set the center location to zero
           profile->SET_CENTER, 0D
         END

       ENDCASE
       
       RETURN, profile

     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_BEAM_PROFILE

     

     ;+=================================================================
     ; PURPOSE:
     ;   Return the wavelengths and intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_LINES

       ; Here we choose wether or not to include the water line.
       IF self.options.include_water_line THEN BEGIN
         lines = { $
                   full:self->GET_STARK_LINES_COMPONENT(/FULL) $
                   ,half:self->GET_STARK_LINES_COMPONENT(/HALF) $
                   ,third:self->GET_STARK_LINES_COMPONENT(/THIRD) $
                   ,water:self->GET_STARK_LINES_COMPONENT(/WATER) $
                 }
       ENDIF ELSE BEGIN
         lines = { $
                   full:self->GET_STARK_LINES_COMPONENT(/FULL) $
                   ,half:self->GET_STARK_LINES_COMPONENT(/HALF) $
                   ,third:self->GET_STARK_LINES_COMPONENT(/THIRD) $
                 }
       ENDELSE

       RETURN, lines

     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_LINES

     

     ;+=================================================================
     ; PURPOSE:
     ;   Return the channel locations of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_LINES_COMPONENT, _REF_EXTRA=extra

       RETURN, { $
                 location:self->GET_STARK_LOCATION_COMPONENT(_STRICT_EXTRA=extra) $
                 ,intensity:self->GET_STARK_INTENSITY_COMPONENT(_STRICT_EXTRA=extra) $
               }


     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_LINES_COMPONENT


     
     ;+=================================================================
     ; PURPOSE:
     ;   Return the intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_LOCATION_COMPONENT, _REF_EXTRA=extra
       COMPILE_OPT STRICTARR

       COMMON BST_CHORD_PARAM, chord_param

       wavelength = self->GET_STARK_WAVELENGTH_COMPONENT(_STRICT_EXTRA=extra)

       location = BST_DISP_GET_LOCATION(WAVELENGTH=wavelength $
                                        ,LAMBDA0=self.param.lambda0 $
                                        ,SPEC_PARAM=chord_param.detector $
                                        ,/FROM_EDGE)
   
       ; The value returned above assumes that the first pixel is
       ; pixel 0.  The CERVIEW spectra start the pixels from 1.
       location += 1
       
       RETURN, location

     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_LOCATION_COMPONENT



     ;+=================================================================
     ; PURPOSE:
     ;   Return the wavelengths and intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_WAVELENGTH_COMPONENT, FULL=full $
                                                                    ,HALF=half $
                                                                    ,THIRD=third $
                                                                    ,WATER=water
       COMPILE_OPT STRICTARR

       COMMON BST_CHORD_PARAM, chord_param

       hydrogen = 0
       deuterium = 1

       CASE 1 OF
         KEYWORD_SET(full): BEGIN
           factor = 1D
         END
         KEYWORD_SET(half): BEGIN
           factor = SQRT(2D)
         END
         KEYWORD_SET(third): BEGIN
           factor = SQRT(3D)
         END
         KEYWORD_SET(water): BEGIN
           factor = SQRT(20D)/SQRT(2D)
           ;hydrogen = 1
           ;deuterium = 0
         END
       ENDCASE

       RETURN, BST_STARK_MODEL_GET_WAVELENGTH(self.param.er_eff/factor $
                                              ,self.param.vbeam/factor $
                                              ,chord_param.geometry.angle_chord_beam $
                                              ,HYDROGEN=hydrogen $
                                              ,DEUTERIUM=deuterium)


     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_WAVELENGTH_COMPONENT



     ;+=================================================================
     ; PURPOSE:
     ;   Return the intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_INTENSITY_COMPONENT, FULL=full $
                                                                    ,HALF=half $
                                                                    ,THIRD=third $
                                                                    ,WATER=water
       COMPILE_OPT STRICTARR

       COMMON BST_CHORD_PARAM, chord_param

       CASE 1 OF
         KEYWORD_SET(full): BEGIN
           comp = 0
         END
         KEYWORD_SET(half): BEGIN
           comp = 1
         END
         KEYWORD_SET(third): BEGIN
           comp = 2
         END
         KEYWORD_SET(water): BEGIN
           comp = 3
         END
       ENDCASE

       RETURN,  BST_STARK_MODEL_GET_INTENSITY( self.param.ratio $
                                               ,self.param.level_population.(comp) )


     END ; FUNCTION BST_INST_MODEL_BSTARK::GET_STARK_INTENSITY_COMPONENT



     ;+=================================================================
     ; PURPOSE:
     ;   Print out the state of the model.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::PRINT
       
       ; Reload the chord parameters.
       ;   NOTE: The parameters are cached, so this is fast.
       BST_CHORD_PARAM, self.shot_param.shot $
                        ,self.shot_param.chord $
                        ,self.shot_param.beam


       ; Get the line locations and intensities.
       lines = self->GET_STARK_LINES()


       ; Get the debug flags
       debug = self.dispatcher->GET_OPTIONS_DEBUG()


       ; Create the format codes.
       fmt_f = '(a12, ":", 2x, f12.5)'
       fmt_e = '(a12, ":", 2x, e12.5)'

       ; Print out the parameters.
       PRINT, FORMAT=fmt_f, 'ratio', self.param.ratio
       PRINT, FORMAT=fmt_e, 'vbeam_proj', self.param.vbeam
       PRINT, FORMAT=fmt_e, 'er_eff', self.param.er_eff
       PRINT, FORMAT=fmt_f, 'temp', self.param.temperature
       PRINT, FORMAT=fmt_f, 'lambda0', self.param.lambda0


       ; Print out the wavelengths of the lines.
       str_num_lines = STRING(self.num_lines, FORMAT='(i0)')
       format = '(2x, a10, ":", 2x, ' + str_num_lines + '(2X, f9.3))'
       wavelength = self->GET_STARK_WAVELENGTH_COMPONENT(/FULL)
       location = lines.full.location
       intensity = lines.full.intensity
       PRINT, 'Full'
       PRINT, 'Wavelength', wavelength, FORMAT=format
       PRINT, 'Location', location, FORMAT=format
       PRINT, 'Intensity', intensity, FORMAT=format

       wavelength = self->GET_STARK_WAVELENGTH_COMPONENT(/HALF)
       location = lines.half.location
       intensity = lines.half.intensity
       PRINT, 'Half'
       PRINT, 'Wavelength', wavelength, FORMAT=format
       PRINT, 'Location', location, FORMAT=format
       PRINT, 'Intensity', intensity, FORMAT=format


       wavelength = self->GET_STARK_WAVELENGTH_COMPONENT(/THIRD)
       location = lines.third.location
       intensity = lines.third.intensity
       PRINT, 'Third'
       PRINT, 'Wavelength', wavelength, FORMAT=format
       PRINT, 'Location', location, FORMAT=format
       PRINT, 'Intensity', intensity, FORMAT=format

       IF self.options.include_water_line THEN BEGIN
         wavelength = self->GET_STARK_WAVELENGTH_COMPONENT(/WATER)
         location = lines.water.location
         intensity = lines.water.intensity
         PRINT, 'Water'
         PRINT, 'Wavelength', wavelength, FORMAT=format
         PRINT, 'Location', location, FORMAT=format
         PRINT, 'Intensity', intensity, FORMAT=format
       ENDIF

     END ;PRO BST_INST_MODEL_BSTARK::PRINT



     ;+=================================================================
     ; PURPOSE:
     ;   Set which model parameters are fixed.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::SET_FIXED, fixed
       
       STRUCT_ASSIGN, {fixed:fixed}, self, /NOZERO

     END ;PRO BST_INST_MODEL_BSTARK::SET_FIXED



     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
         self.shot_param = { BST_INST_MODEL_BSTARK__SHOT_PARAM }
         self.options.fix_central_line_width = 1
         self.options.no_field = 0
         self.options.include_water_line = 0
       ENDIF

       IF params OR all THEN BEGIN
         self.param = { BST_INST_MODEL_BSTARK__PARAM }
         self.param.ratio = 1D

         self.param.level_population.full = 1D
         self.param.level_population.half = 1D
         self.param.level_population.third = 1D
         self.param.level_population.water = 1D

         self.param.profile_width.full = 1D
         self.param.profile_width.half = 1D
         self.param.profile_width.third = 1D
         self.param.profile_width.water = 1D
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_BSTARK__FIXED }
         self.fixed.temperature = 1

         self.fixed.profile_width.full = 1
         self.fixed.profile_width.half = 1
         self.fixed.profile_width.third = 1
         self.fixed.profile_width.water = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_BSTARK__LIMITED }
         ; Temperature should always be positive.
         self.limited.temperature = [1,0]

         ; Level populations should always be positive.
         self.limited.level_population.full[0,*] = 1
         self.limited.level_population.half[0,*] = 1
         self.limited.level_population.third[0,*] = 1
         self.limited.level_population.water[0,*] = 1

         ; Profile width should always be positive.
         self.limited.profile_width.full[0,*] = 1
         self.limited.profile_width.half[0,*] = 1
         self.limited.profile_width.third[0,*] = 1
         self.limited.profile_width.water[0,*] = 1
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_BSTARK__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_BSTARK::RESET



     ;+=================================================================
     ; This routine does a full reset of the object.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::RESET_ALL

       ; This will also call self->RESET
       self->BST_INST_MODEL_PARAMETER::RESET_ALL

     END ;PRO BST_INST_MODEL_BSTARK::RESET_ALL



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the shot parameters 
     ;   of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_SHOT_PARAM
       
       RETURN, self.shot_param

     END ;PRO BST_INST_MODEL_BSTARK::GET_SHOT_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model shot parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::SET_SHOT_PARAM, shot_param
       
       STRUCT_ASSIGN, {shot_param:shot_param}, self, /NOZERO

     END ;PRO BST_INST_MODEL_BSTARK::SET_SHOT_PARAM




     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the shot parameters 
     ;   of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_BSTARK::GET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model shot parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_BSTARK::SET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BSTARK::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{shot_param:self->GET_SHOT_PARAM()} $
                             ,{options:self->GET_OPTIONS()} $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_BSTARK::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state
       self->SET_SHOT_PARAM, state.shot_param

       IF HAS_TAG(state, 'OPTIONS') THEN BEGIN
         self->SET_OPTIONS, state.options
       ENDIF

     END ;PRO BST_INST_MODEL_BSTARK::LOAD_STATE



     ;+=================================================================
     ; Here we set up the structure that will hold a bstark parameter.
     ;
     ; Note that this must also hold some of the gui parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_BSTARK__DEFINE

       num_levels = 6
       num_lines = 9

       shot_param = { BST_INST_MODEL_BSTARK__SHOT_PARAM $
                      ,shot:0L $
                      ,chord:'' $
                      ,beam:'' $
                    }

       param = { BST_INST_MODEL_BSTARK__PARAM $
                 ,ratio:0D $
                 ,vbeam:0D $
                 ,er_eff:0D $
                 ,temperature:0D $
                 ,lambda0:0D $
                 ,level_population:{ BST_INST_MODEL_BSTARK__PARAM__LEVEL_POP $
                                     ,full:DBLARR(num_levels) $
                                     ,half:DBLARR(num_levels) $
                                     ,third:DBLARR(num_levels) $
                                     ,water:DBLARR(num_levels) $
                                   } $
                 ,profile_width:{ BST_INST_MODEL_BSTARK__PARAM__PROFILE_WIDTH $
                                  ,full:DBLARR(num_lines) $
                                  ,half:DBLARR(num_lines) $
                                  ,third:DBLARR(num_lines) $
                                  ,water:DBLARR(num_lines) $
                                } $
                 ,profile_width_slope:{ BST_INST_MODEL_BSTARK__PARAM__PROFILE_WIDTH__SLOPE $
                                        ,full:0D $
                                        ,half:0D $
                                        ,third:0D $
                                      } $
               } 

       sigma = { BST_INST_MODEL_BSTARK__SIGMA $
                 ,ratio:0D $
                 ,vbeam:0D $
                 ,er_eff:0D $
                 ,temperature:0D $
                 ,lambda0:0D $
                 ,level_population:{ BST_INST_MODEL_BSTARK__SIGMA__LEVEL_POP $
                                     ,full:DBLARR(num_levels) $
                                     ,half:DBLARR(num_levels) $
                                     ,third:DBLARR(num_levels) $
                                     ,water:DBLARR(num_levels) $
                                   } $
                 ,profile_width:{ BST_INST_MODEL_BSTARK__SIGMA__PROFILE_WIDTH $
                                  ,full:DBLARR(num_lines) $
                                  ,half:DBLARR(num_lines) $
                                  ,third:DBLARR(num_lines) $
                                  ,water:DBLARR(num_lines) $
                                } $
                 ,profile_width_slope:{ BST_INST_MODEL_BSTARK__SIGMA__PROFILE_WIDTH__SLOPE $
                                        ,full:0D $
                                        ,half:0D $
                                        ,third:0D $
                                      } $
               } 

       fixed = { BST_INST_MODEL_BSTARK__FIXED $
                 ,ratio:0 $
                 ,vbeam:0 $
                 ,er_eff:0 $
                 ,temperature:0 $
                 ,lambda0:0 $
                 ,level_population:{ BST_INST_MODEL_BSTARK__FIXED__LEVEL_POP $
                                     ,full:INTARR(num_levels) $
                                     ,half:INTARR(num_levels) $
                                     ,third:INTARR(num_levels) $
                                     ,water:INTARR(num_levels) $
                                   } $
                 ,profile_width:{ BST_INST_MODEL_BSTARK__FIXED__PROFILE_WIDTH $
                                  ,full:INTARR(num_lines) $
                                  ,half:INTARR(num_lines) $
                                  ,third:INTARR(num_lines) $
                                  ,water:INTARR(num_lines) $
                                } $
                 ,profile_width_slope:{ BST_INST_MODEL_BSTARK__FIXED__PROFILE_WIDTH__SLOPE $
                                        ,full:0 $
                                        ,half:0 $
                                        ,third:0 $
                                      } $
               } 
       
       limited = { BST_INST_MODEL_BSTARK__LIMITED $
                   ,ratio:[0,0] $
                   ,vbeam:[0,0] $
                   ,er_eff:[0,0] $
                   ,temperature:[0,0] $
                   ,lambda0:[0,0] $
                   ,level_population:{ BST_INST_MODEL_BSTARK__LIMITED__LEVEL_POP $
                                       ,full:INTARR(2, num_levels) $
                                       ,half:INTARR(2, num_levels) $
                                       ,third:INTARR(2, num_levels) $
                                       ,water:INTARR(2, num_levels) $
                                     } $
                   ,profile_width:{ BST_INST_MODEL_BSTARK__LIMITED__PROFILE_WIDTH $
                                    ,full:INTARR(2, num_lines) $
                                    ,half:INTARR(2, num_lines) $
                                    ,third:INTARR(2, num_lines) $
                                    ,water:INTARR(2, num_lines) $
                                  } $
                   ,profile_width_slope:{ BST_INST_MODEL_BSTARK__LIMITED__PROFILE_WIDTH__SLOPE $
                                          ,full:[0,0] $
                                          ,half:[0,0] $
                                          ,third:[0,0] $
                                        } $
                 }

       limits = { BST_INST_MODEL_BSTARK__LIMITS $
                  ,ratio:[0D,0D] $
                  ,vbeam:[0D,0D] $
                  ,er_eff:[0D,0D] $
                  ,temperature:[0D,0D] $
                  ,lambda0:[0D,0D] $
                  ,level_population:{ BST_INST_MODEL_BSTARK__LIMITS__LEVEL_POP $
                                      ,full:DBLARR(2, num_levels) $
                                      ,half:DBLARR(2, num_levels) $
                                      ,third:DBLARR(2, num_levels) $
                                      ,water:DBLARR(2, num_levels) $
                                    } $
                  ,profile_width:{ BST_INST_MODEL_BSTARK__LIMITS__PROFILE_WIDTH $
                                   ,full:DBLARR(2, num_lines) $
                                   ,half:DBLARR(2, num_lines) $
                                   ,third:DBLARR(2, num_lines) $
                                   ,water:DBLARR(2, num_lines) $
                                 } $
                  ,profile_width_slope:{ BST_INST_MODEL_BSTARK__LIMITS__PROFILE_WIDTH__SLOPE $
                                         ,full:[0D,0D] $
                                         ,half:[0D,0D] $
                                         ,third:[0D,0D] $
                                       } $
                } 



       struct ={ BST_INST_MODEL_BSTARK $

                 ,options:{ BST_INST_MODEL_BSTARK__OPTIONS $
                            ,use_line_width_slope:0 $
                            ,fix_central_line_width:0 $
                            ,no_field:0 $
                            ,include_water_line:0 $

                            ,pop_levels_from_full:0 $
                            ,pop_levels_symmetric:0 $
                            ,pop_levels_calculated:0 $
                          } $

                 ,num_levels:0 $
                 ,num_components:0 $
                 ,num_lines:0 $

                 ,shot_param:{ BST_INST_MODEL_BSTARK__SHOT_PARAM} $

                 ,param:{ BST_INST_MODEL_BSTARK__PARAM} $

                 ,sigma:{ BST_INST_MODEL_BSTARK__SIGMA} $

                 ,fixed:{ BST_INST_MODEL_BSTARK__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_BSTARK__LIMITED } $

                 ,limits:{ BST_INST_MODEL_BSTARK__LIMITS } $
                 
                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_BSTARK
