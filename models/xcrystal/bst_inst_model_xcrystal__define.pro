



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This object handels the xcrystal profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This is the base class for the XICS models.  This is subcalassed for the
;   indiviual spectral models: ar16+ and ar17+:
;     BST_INST_MODEL_XCRYSTAL_AR16::
;     BST_INST_MODEL_XCRYSTAL_AR17::
;  
;
; DESCRIPTION:
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::SET_FLAGS

       self.BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 0

     END ; PRO BST_INST_MODEL_XCRYSTAL::SET_FLAGS




     ;+=================================================================
     ; PURPOSE
     ;   Setup the model prior to starting the fit loop.
     ;
     ;   Initialize the model parameters if needid.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL::PREFIT
       COMPILE_OPT STRICTARR

       ; Note: the order of the initialization calls is important here.
       ;       in particular, the conversion between channels and 
       ;       angstroms needs to be loaded before the line list is
       ;       loaded.

       ; Get the spectrum.
       core = (self.dispatcher).core
       spectrum = (core.spectrum).GET_SPECTRUM()

       ; Pre-calculate conversions betweens channels and angstroms.
       self.LOAD_ANGSTROMS_PER_CHANNEL, spectrum
       self.LOAD_CHANNELS_TO_ANGSTROMS, spectrum

       ; Create the line list.
       self.LOAD_LINE_LIST

       ; Initialize the model if needed.
       IF ~ self.fixall THEN BEGIN
         CASE 1 OF
           self.options.initialize_model: BEGIN
             self.MESSAGE, /DEBUG, 'Initializing model parameters.'
             self.INITIALIZE_MODEL, spectrum
           END
           self.options.initialize_scale: BEGIN
             self.MESSAGE, /DEBUG, 'Initializing scale factor.'
             self.INITIALIZE_SCALE, spectrum
           END
           ELSE:
         ENDCASE
       ENDIF

     END ;FUNCTION BST_INST_MODEL_XCRYSTAL::PREFIT



     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the xcrystal model.
     ;
     ; DESCRIPTION:
     ;
     ;   For speed reasions a few arrays need to be precalculated before the
     ;   fit can be started. For this reason the input "x" is ingnored by this
     ;   evaluation statement.  These arrays are calculated when [::PREFIT]
     ;   is called.
     ;
     ;   A note about units:
     ;
     ;   The spectrum that is given to the fitter is in photons/pixel^2/frame.
     ;   The width (sigma) of the lines will be calculated in angstroms, and
     ;   the intensity in photons/pixel/frame, where pixel refers to the
     ;   dimension in the spatial direction.
     ;    
     ;   With these choices the model will return y in
     ;   photons/angstrom/pixel/frame.  I then convert this result into
     ;   photons/pixel^2/frame to match the spectrum.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::EVALUATE, x

       ; Convert the x dimension into angstroms
       x_angstroms = self.GET_CHANNELS_TO_ANGSTROMS()

       y = DBLARR(N_ELEMENTS(x_angstroms))

       ; Get the line list, which is a list of <MIR_VOIGT::> objects.
       linelist = self.GET_LINE_LIST()

       num_lines = linelist.N_ELEMENTS()


       FOR ii=0,num_lines-1 DO BEGIN
         line = linelist[ii]

         y += line.EVALUATE(x_angstroms, CUTOFF=self.options.evaluation_cutoff)
       ENDFOR

       
       ; This model returns y in units of counts/angstrom.
       ; I want to convert this to counts/channel.
       angstroms_per_channel = ABS(self.GET_ANGSTROMS_PER_CHANNEL())
       y *= angstroms_per_channel

       RETURN, y

     END ;PRO BST_INST_MODEL_XCRYSTAL::EVALUATE



     ;+=========================================================================
     ; PURPOSE
     ;   Make a first guess for the model scale factor
     ;
     ; DESCRIPTION:
     ;   For the time being I will just use a crude method of achieveing
     ;   this.  Eventually I might want to use a linear fitting method
     ;   for this instead.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::INITIALIZE_SCALE, spectrum

       self.param.scale_factor = 1.0

       IF ~ ISA(spectrum) THEN BEGIN
         core = (self.dispatcher).core
         spectrum = (core.spectrum).GET_SPECTRUM()
       ENDIF

       ; Here I am evaluating the model ONLY.
       ; This is equivilent of assuming that the background is zero.
       y_model = self.EVALUATE(spectrum.x)

       
       ; Just scale all the line intensities so that the total
       ; intensity matches the total intensity in the spectrum.
       ;
       ; This will only work if the relative intensities are already
       ; at appoximately the correct ratios.
       scale = TOTAL(spectrum.y)/TOTAL(y_model)

       self.param.scale_factor = scale
       
     END ; PRO BST_INST_MODEL_XCRYSTAL::INITIALIZE_SCALE


     ;+=========================================================================
     ; PURPOSE
     ;   Initialize the background level.
     ;
     ; DESCRIPTION:
     ;   This will initialize the background model with an approximate
     ;   background level.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::INITIALIZE_BACKGROUND, spectrum

       ; Approximate the background to the the median at 5%.
       back_const = MIR_MEDIAN(spectrum.y, 0.05)

       model_back = self.dispatcher.GET_MODEL('BST_INST_MODEL_BACKGROUND')
       model_back.SET_PARAM, {back:[back_const, 0, 0]}
       
     END ; PRO BST_INST_MODEL_XCRYSTAL::INITIALIZE_BACKGROUND

     
     ;+=========================================================================
     ; PURPOSE:
     ;  Load the channels_to_angstroms array into the cache.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::LOAD_CHANNELS_TO_ANGSTROMS, spectrum

       IF ~ ISA(spectrum) THEN BEGIN
         core = (self.dispatcher).core
         spectrum = (core.spectrum).GET_SPECTRUM()
       ENDIF

       channels_to_angstroms = self.CHANNELS_TO_ANGSTROMS(spectrum.x)
       self.param_internal.ptr_channels_to_angstroms = PTR_NEW(channels_to_angstroms)

     END ; PRO BST_INST_MODEL_XCRYSTAL::LOAD_CHANNELS_TO_ANGSTROMS


     ;+=========================================================================
     ; PURPOSE:
     ;  Load the angstroms_per_channel array into the cache.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::LOAD_ANGSTROMS_PER_CHANNEL, spectrum

       IF ~ ISA(spectrum) THEN BEGIN
         core = (self.dispatcher).core
         spectrum = (core.spectrum).GET_SPECTRUM()
       ENDIF

       angstroms_per_channel = self.ANGSTROMS_PER_CHANNEL(spectrum.x)
       self.param_internal.ptr_angstroms_per_channel = PTR_NEW(angstroms_per_channel)

     END ; PRO BST_INST_MODEL_XCRYSTAL::LOAD_ANGSTROMS_PER_CHANNEL

     ;+=========================================================================
     ; PURPOSE:
     ;  Get the precalculated channels_to_angstroms array..
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_CHANNELS_TO_ANGSTROMS

       RETURN, *self.param_internal.ptr_channels_to_angstroms

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::GET_CHANNELS_TO_ANGSTROMS


     ;+=========================================================================
     ; PURPOSE:
     ;  Get the precalculated angstroms_per_channel array.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_ANGSTROMS_PER_CHANNEL

       RETURN, *self.param_internal.ptr_angstroms_per_channel

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::GET_ANGSTROMS_PER_CHANNEL


     ;+=========================================================================
     ; PURPOSE:
     ;   Convert from channels to angstroms.  This is done using the geometry
     ;   and known crystal and detector parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::CHANNELS_TO_ANGSTROMS, channel_in
       COMPILE_OPT STRICTARR

       g = self.GET_GEOMETRY_OBJECT()

       ; I need to work on making this efficent (probably).
       num_channels = N_ELEMENTS(channel_in)
       wavelength = DBLARR(num_channels, /NOZERO)

       FOR ii=0,num_channels-1 DO BEGIN
         channel = [channel_in[ii] $
                  ,(self.spectrum_param.y_max + self.spectrum_param.y_min)/2D]

         wavelength[ii] =  g.WAVELENGTH_FROM_CHANNEL(channel)
       ENDFOR
       
       RETURN, wavelength

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::CHANNELS_TO_ANGSTROMS


     ;+=========================================================================
     ; PURPOSE:
     ;   Calculate the number of angstroms per channel.
     ; 
     ; DISCRIPTION:
     ;   This is done discretely for use in converting photons/angstrom to
     ;   photons/channel.  For this type of calculation this is more accurate
     ;   that using the dispersion at the center of the channel.
     ;
     ;   Here that I am making the approximation that the dispersion will
     ;   be the same for all of the rows that are summed to make the spectrum.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::ANGSTROMS_PER_CHANNEL, channel_in
       COMPILE_OPT STRICTARR

       g = self.GET_GEOMETRY_OBJECT()

       ; I need to work on making this efficent (probably).
       num_channels = N_ELEMENTS(channel_in)
       ang_per_channel = DBLARR(num_channels, /NOZERO)

       FOR ii=0,num_channels-1 DO BEGIN
         y = (self.spectrum_param.y_max + self.spectrum_param.y_min)/2D
         channel_0 = [channel_in[ii]-0.5, y]
         channel_1 = [channel_in[ii]+0.5, y]
         wavelength_0 = g.WAVELENGTH_FROM_CHANNEL(channel_0)
         wavelength_1 = g.WAVELENGTH_FROM_CHANNEL(channel_1)

         ang_per_channel[ii] = wavelength_1 - wavelength_0
       ENDFOR

       RETURN, ang_per_channel

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::ANGSTROMS_PER_CHANNEL





     ;+=========================================================================
     ; PURPOSE:
     ;  Load the channels_to_angstroms array into the cache.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_SPECTRUM_RANGE, SORT=sort
       x_angstroms = self.GET_CHANNELS_TO_ANGSTROMS()
       spectrum_range = [x_angstroms[0], x_angstroms[-1]]

       IF KEYWORD_SET(sort) THEN BEGIN
         spectrum_range = spectrum_range[SORT(spectrum_range)]
       ENDIF

       RETURN, spectrum_range

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::GET_SPECTRUM_RANGE



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_THEORY_DATA
     COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self.MESSAGE, /ERROR
         MESSAGE, 'Could not read theory data file.'
       ENDIF

       ; This routine does cacheing of the line data, so OK to call repeatedly.
       line_data = XC_READ_XCRYSTAL_LINES_FILE(self.options.line_source)

       RETURN, line_data

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::GET_THEORY_DATA


     ;+=========================================================================
     ; PURPOSE:
     ;   Get an updated list of the lines for the XCrystal model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_THEORY_LIST

       RETURN, self.param_internal.theory_list

     END ; BST_INST_MODEL_XCRYSTAL::GET_THEORY_LIST


     ;+=========================================================================
     ; PURPOSE:
     ;   Get an updated list of the lines for the XCrystal model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_LINE_LIST

       self.UPDATE_LINE_LIST
       RETURN, self.param_internal.line_list

     END ; BST_INST_MODEL_XCRYSTAL::GET_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_WAVELENGTH_REF
     COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self.MESSAGE, /ERROR
         MESSAGE, 'Could not retrive wavelength reference.'
       ENDIF

       g = self.GET_GEOMETRY_OBJECT()
       wavelength_ref = g.config.wavelength_ref

       RETURN, wavelength_ref

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::GET_WAVELENGTH_REF

     
     ;+=================================================================
     ; PURPOSE
     ;   Load the geometry object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL::LOAD_GEOMETRY_OBJECT
       COMPILE_OPT STRICTARR

       ; There are a few cases where I want to load the configuration
       ; from a shot that is different from the shot I am fitting.
       ;
       ; For example when doing a geometery fit, I want to load the
       ; configuration from a calibariton configuraiton, but then
       ; fit actual data.

       IF self.spectrum_param.config_shot GE 0 THEN BEGIN
         shot = self.spectrum_param.config_shot 
       ENDIF ELSE BEGIN
         shot = self.spectrum_param.shot 
       ENDELSE

       self.param_internal.geometry = XC_GET_GEOMETRY_OBJECT(self.options.system $
                                                             ,shot)

     END ;FUNCTION BST_INST_MODEL_XCRYSTAL::LOAD_GEOMETRY_OBJECT


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_GEOMETRY_OBJECT
     COMPILE_OPT STRICTARR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self.MESSAGE, /ERROR
         MESSAGE, 'Could not retrive geometry object.'
       ENDIF

       
       g = self.param_internal.geometry
       IF ~OBJ_VALID(g) THEN BEGIN
         MESSAGE, 'Geometry object not loaded.'
       ENDIF

       RETURN, g

     END ; FUNCTION BST_INST_MODEL_XCRYSTAL::GET_GEOMETRY_OBJECT



     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::SET_DEFAULTS

       self.options = { BST_INST_MODEL_XCRYSTAL__OPTIONS }
       self.options.system = ''
       self.options.use_te = 0
       self.options.line_source = ''
       self.options.excitation_source = 'MARCHUK_2004'
       self.options.atomic_number = 0
       self.options.evaluation_cutoff = 1D-16
       self.options.initialize_shifts = 1
       self.options.initialize_scale = 0
       self.options.intensity_cutoff = 0E
       self.options.exclude_outside_lines = 0

       self.spectrum_param = { BST_INST_MODEL_XCRYSTAL__SPECTRUM_PARAM }
       self.spectrum_param.config_shot = -1L


     END ;PRO BST_INST_MODEL_XCRYSTAL::SET_DEFAULTS


     
     ;+=========================================================================
     ; PURPOSE:
     ;   Return a plotlist for this model.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_PLOTLIST, x, _REF_EXTRA=extra

       plot_cutoff = 1e-8
       
       ; Create a new PLOTLIST object
       plotlist = OBJ_NEW('MIR_PLOTLIST')

       ; Convert the x dimension into angstroms
       x_angstroms = self.CHANNELS_TO_ANGSTROMS(x)
       angstroms_per_channel = ABS(self.ANGSTROMS_PER_CHANNEL(x))

       ; Get the line list, which is a list of <MIR_VOIGT::> objects.
       line_list = self.GET_LINE_LIST()
       theory_list = self.GET_THEORY_LIST()
       num_lines = line_list.N_ELEMENTS()

       FOR ii=0,num_lines-1 DO BEGIN
         line = line_list[ii]
         theory = theory_list[ii]
         
         y = line.EVALUATE(x_angstroms, CUTOFF=plot_cutoff)
         ; This model returns y in units of counts/angstrom.
         ; I want to convert this to counts/channel.
         y *= angstroms_per_channel

         w = WHERE(y GT plot_cutoff)

         color = MIR_COLOR('GRAY_50')
         CASE theory.type OF
           'DIELECTRONIC': BEGIN
             CASE theory.n OF
               2: color = MIR_COLOR('PRINT_PURPLE')
               3: color = MIR_COLOR('PRINT_ORANGE')
               4: color = MIR_COLOR('PRINT_BLUE')
               5: color = MIR_COLOR('PRINT_YELLOW')
               6: color = MIR_COLOR('PRINT_GREEN')
             ENDCASE
           END
           'DIRECT': BEGIN
             color = MIR_COLOR('PRINT_CYAN')
           END
         ENDCASE
         
         ; Add the line
         plotlist->APPEND, {x:x[w] $
                            ,y:y[w] $
                            ,color:color $
                            ;,thick:2 $
                            ,psym:0 $
                            ,oplot:1 $
                            ,plotnum:0 $
                           }
       ENDFOR
       
       RETURN, plotlist

     END ;FUNCTION BST_INST_MODEL_XCRYSTAL::GET_PLOTLIST



     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self.BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all

     END ; BST_INST_MODEL_XCRYSTAL::RESET


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_STATE

       state = self.BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{spectrum_param:self.GET_SPECTRUM_PARAM()} $
                             ,{options:self.GET_OPTIONS()} $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_XCRYSTAL::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL::LOAD_STATE, state

       self.BST_INST_MODEL_PARAMETER::LOAD_STATE, state



       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.SET_SPECTRUM_PARAM, state.spectrum_param
       ENDIF ELSE BEGIN
         self.MESSAGE, 'Could not load spectrum_param.', /ERROR
         CATCH, /CANCEL
       ENDELSE


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.SET_OPTIONS, state.options
       ENDIF ELSE BEGIN
         self.MESSAGE, 'Could not load options.', /ERROR
         CATCH, /CANCEL
       ENDELSE


     END ;PRO BST_INST_MODEL_XCRYSTAL::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Get the shot parameters of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_SPECTRUM_PARAM
       
       RETURN, self.spectrum_param

     END ;PRO BST_INST_MODEL_XCRYSTAL::GET_SPECTRUM_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model shot parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL::SET_SPECTRUM_PARAM, spectrum_param
       
       STRUCT_ASSIGN, {spectrum_param:spectrum_param}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XCRYSTAL::SET_SPECTRUM_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Get the shot parameters of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_PARAM_INTERNAL
       
       RETURN, self.param_internal

     END ;PRO BST_INST_MODEL_XCRYSTAL::GET_PARAM_INTERNAL


     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the options of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_XCRYSTAL::GET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model options
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XCRYSTAL::SET_OPTIONS



     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XCRYSTAL model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL__DEFINE



       ; -----------------------------------------------------------------------
       ; Setup additional model data.

       ; initialize_model
       ;   If True then before fitting an initializaiton procedure will be used 
       ;   to guess good starting values for all of the fit parameters.
       ;
       ; initialize_shifts
       ;   If True then during model intitializetion the shifts will be reset
       ;   to zero.  If False they will not be reset.  Setting this option
       ;   to false is useful when doing geometry calibrations.
       ;
       ; initialize_scale
       ;   If true the scale factor will be initialized, but none of the model
       ;   parameters.
       ;
       ; intensity_cutoff
       ;   This option is used to exclude dielectric lines from the model
       ;   that have a relative intensity less than ths value.  This option
       ;   is used for fast fitting procedures where a reduced model is 
       ;   desired.
       ;
       ; exclude_outside_lines
       ;   If set to non-zero values then any lines centered outside of the 
       ;   spectral range will be excluded from the model.  This option is used 
       ;   for fast fittng procedures where a reduced model is desired.
       options = { BST_INST_MODEL_XCRYSTAL__OPTIONS $
                   ,spectrum:'' $
                   ,system:'' $
                   ,use_te:0 $
                   ,initialize_model:0 $
                   ,initialize_shifts:0 $
                   ,initialize_scale:0 $

                   ,line_source:'' $
                   ,excitation_source:'' $
                   ,atomic_number:0 $
                   ,evaluation_cutoff:0D $

                   ,intensity_cutoff:0E $
                   ,exclude_outside_lines:0 $
                 }

       spectrum_param = { BST_INST_MODEL_XCRYSTAL__SPECTRUM_PARAM $
                          ,shot:0L $
                          ,config_shot:0L $
                          ,system:'' $
                          
                          ,ts_min:0L $
                          ,ts_max:0L $

                          ,y_min:0L $
                          ,y_max:0L $
                        }


       param_internal = { BST_INST_MODEL_XCRYSTAL__INTERNAL $
                    ,geometry:OBJ_NEW() $
                    ,ptr_channels_to_angstroms:PTR_NEW() $
                    ,ptr_angstroms_per_channel:PTR_NEW() $

                    ,line_list:OBJ_NEW() $
                    ,theory_list:OBJ_NEW() $
                    ,wavelength_ref:0D $
                  }

       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XCRYSTAL $
                 ,temp:OBJ_NEW() $
       
                 ,options:{ BST_INST_MODEL_XCRYSTAL__OPTIONS } $
                 
                 ,spectrum_param:{ BST_INST_MODEL_XCRYSTAL__SPECTRUM_PARAM }  $

                 ,param_internal:{ BST_INST_MODEL_XCRYSTAL__INTERNAL } $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_XCRYSTAL
