



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   npablant@pppl.gov
;   novimir.pablant@gmail.com
;
; CREATION DATE:
;   2010-09
;
; LAST UPDATE:
;   2018-06
;
; PURPOSE:
;   This object handels the xcrystal profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit He like argon spectra from the XCIS 
;   system on C-Mod.
;
; DESCRIPTION:
;
; PARAMETERS:
;   ti
;   te
;       (keV)
;       Ion and electron temperature.
;
;   
;   shift_constant
;   shift_linear
;   shift_quadratic
;       (Angstroms)
;       Parameters to calculate the shift of the lines.
;       For the XICS spectrum it is not it is not possible to directly calculate
;       the line shifts caused by a rotation due to line integrated nature of the 
;       measurement. Instead I just use a quadratic form to approximate the shift 
;       of each line and then analyze the results later.
;
;
;   w_intensity:0D $
;   x_intensity:0D $
;   y_intensity:0D $
;   z_intensity:0D $
;       (photons/pixel/frame)
;       Intensities of various independently fit lines.
;
;       The units are photons/pixel/frame where pixel is in the spatial direction.
;       By multiplying by pixels/mm one would have a 1D intensity measurement on
;       the detector.
;
;
;   q_intensity
;   r_intensity
;   s_intensity
;   t_intensity
;
;   w2_factor
;   w3_factor
;   w4_factor
;   w5_factor
;   w6_factor
;        (photons/pixel/frame / Qd)
;        These do not represent a true intensity but rather a factor that
;        when multiplied by Qd for an individual line will provide an
;        approximation of the correct line intensity.
;
;        To get an approximation of the total intensity of the satellite
;        group this factor can be multiplied by the sum of all the lines
;        in the group.
;
;   be2_factor
;       Intensity of the n=2 berillyum like satellites.
;
;   li_fraction
;       Intensity of lines coming from a Li-like state.
;
;   inst_offset
;   inst_constant
;   inst_quadratic
;       (keV)
;       These parameters are used to allow a correction of the line width
;       to account for instrumental broadening.  Instrumental broadening
;       is primarily caused by the Johann error and defocussing effects.
;       Line with correction is done by adding an effective 'instrumental
;       temperature'. This does not correct for line shifts due to
;       instrumental effects.
;
;       This quadratic fuction is centered on the w line.
;
;       If the system is well focused on the w line, then inst_offset and
;       inst_constant should be zero. 
;      
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::SET_FLAGS

       self.BST_INST_MODEL_XCRYSTAL::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_XCRYSTAL_AR16::SET_FLAGS



     ;+=================================================================
     ; PURPOSE
     ;   Setup the model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::PREFIT_CONSTRAINTS


       ; Setup for whether or not to use an atomic model to calculate
       ; the i
       IF self.options.use_te THEN BEGIN

         self.fixed.q_intensity = 1
         self.fixed.r_intensity = 1
         self.fixed.s_intensity = 1
         self.fixed.t_intensity = 1

         self.fixed.w2_factor = 1
         self.fixed.w3_factor = 1
         self.fixed.w4_factor = 1
         self.fixed.w5_factor = 1
         self.fixed.w6_factor = 1

         self.fixed.Be2_factor = 1

       ENDIF ELSE BEGIN

         self.fixed.te = 1
         self.fixed.li_fraction = 1

       ENDELSE


     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_AR16::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints after the fit loop is complete.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::POSTFIT

       IF self.options.use_te THEN BEGIN


         ; Nothing for now.  
         ; The idea would be to maybe estimate parameters for:
         ;   self.param.q_intensity = 1
         ;   self.param.r_intensity = 1
         ;   self.param.s_intensity = 1
         ;   self.param.t_intensity = 1

         ;   self.param.w2_factor = 1
         ;   self.param.w3_factor = 1
         ;   self.param.w4_factor = 1
         ;   self.param.w5_factor = 1
         ;   self.param.w6_factor = 1

       ENDIF

       ; TEMPORARY for some development
       line_list = self.GET_LINE_LIST()
       BST_INST_LOGGER, /DEBUG, 'Number of lines included in XCRYSTAL model: '+STRING(line_list.N_ELEMENTS())

     END ; PRO BST_INST_MODEL_XCRYSTAL_AR16::POSTFIT



     ;+=========================================================================
     ; PURPOSE:
     ;   Check to see if a given line should be included in the model.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR16::INCLUDE_LINE, theory_struct

       use_line = 1

       IF ((theory_struct.qd LT self.options.intensity_cutoff) $ 
           AND (theory_struct.type EQ 'DIELECTRONIC')) THEN BEGIN
         use_line = 0
       ENDIF

       IF (self.options.exclude_outside_lines) THEN BEGIN
         spectrum_range = self.GET_SPECTRUM_RANGE(/SORT)

         IF ((theory_struct.wavelength LT spectrum_range[0]) $
             OR (theory_struct.wavelength GT spectrum_range[1])) THEN BEGIN
           use_line = 0
         ENDIF
       ENDIF


       RETURN, use_line

     END ;  FUNCTION BST_INST_MODEL_XCRYSTAL_AR16::INCLUDE_LINE

     

     ;+=========================================================================
     ; PURPOSE:
     ;   Create a list of the lines for the XCrystal model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::LOAD_LINE_LIST

       line_list = OBJ_NEW('MIR_LIST')
       theory_list = OBJ_NEW('MIR_LIST')

       ; Get the lines from the theory file.
       line_data = self.GET_THEORY_DATA()
       theory_array = line_data.data

       ; Loop through the lines and add them to the spectrum.
       num_lines = N_ELEMENTS(theory_array)

       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = theory_array[ii_line]
         
         IF self.INCLUDE_LINE(theory_struct) THEN BEGIN
           theory_list.ADD, theory_struct
           voigt = OBJ_NEW('MIR_VOIGT')
           line_list.ADD, voigt
         ENDIF
       ENDFOR

       self.param_internal.theory_list = theory_list
       self.param_internal.line_list = line_list


     END ; BST_INST_MODEL_XCRYSTAL_AR16::LOAD_LINE_LIST

     
     ;+=========================================================================
     ; PURPOSE:
     ;   Update the line list using the current model parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::UPDATE_LINE_LIST

       const = PHYSICAL_CONSTANTS()

       ; Atomic weight of argon in amu.
       atomic_mass =  39.948D

       ; Get the wavelength of the W line which we use as our reference wavelength.
       wavelength_ref = self.GET_WAVELENGTH_REF()

       ; Loop through the lines and update the parameters.
       num_lines = N_ELEMENTS(self.param_internal.line_list)
       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = (self.param_internal.theory_list)[ii_line]

         ; Natural line width.
         gamma = theory_struct.line_width * theory_struct.wavelength^2  $
                 / ( 4D * !dpi * const.c * 1D10 )

         ; Doppler broadened line width.
         sigma = SQRT(self.param.ti / atomic_mass / const.amu_kg / const.c^2 * const.ev_J * 1e3) $
                 * theory_struct.wavelength

         ; Instrumental Width.
         inst_width = self.param.inst_quadratic $
                      * (theory_struct.wavelength - wavelength_ref - self.param.inst_offset)^2 $
                      + self.param.inst_constant

         ; Convolve the line width with the instrumental width (assuming gaussian shapes).
         sigma = SQRT(sigma^2 + inst_width^2)
    
         ; Line location
         location = theory_struct.wavelength $
                    + self.param.shift_constant $
                    + self.param.shift_linear * (theory_struct.wavelength - wavelength_ref) $
                    + self.param.shift_quadratic * (theory_struct.wavelength - wavelength_ref)^2


         ; This is temporary as I try to figure out the spectra I see in LHD 
         ; shot number 112842 on the ar17 camera.
         ; 
         ; This should eventually be removed.
         ; 2013-02-25 - Novimir
         ;IF theory_struct.charge_state EQ 14 AND theory_struct.type EQ 'DIELECTRONIC' THEN BEGIN
         ;  location += 0.0016
         ;ENDIF


         IF self.options.use_te THEN BEGIN
           intensity = self.GET_LINE_INTENSITY_FROM_TE(theory_struct)
         ENDIF ELSE BEGIN
           intensity = self.GET_LINE_INTENSITY(theory_struct)
         ENDELSE


         ; Finally multiply by the scale factor.
         intensity *= self.param.scale_factor


         voigt = (self.param_internal.line_list)[ii_line]
         voigt.SET, {intensity:intensity $
                     ,location:location $
                     ,sigma:sigma $
                     ,gamma:gamma $
                    }
        

       ENDFOR


     END ; BST_INST_MODEL_XCRYSTAL_AR16::UPDATE_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   Calculate the line intensities based on information in the atomic data 
     ;   files.
     ;
     ;   Note:  Lines can appear multiple times in the atomic data file if they
     ;          can be excited by more than one processes. 
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR16::GET_LINE_INTENSITY, theory_struct
       
       CASE theory_struct.type OF

         'DIELECTRONIC': BEGIN

           ; For the satellite lines, the dielectronic excitation rate for all the lines 
           ; with a given n number has approximately the same Te dependence.
           ; 
           ; We use to our advantage in being able to group the various n numbers with
           ; a single intensity parameter.
           ;
           ; Here the QD factor gives us the relative line amplitudes within an n group.
           intensity_factor =  theory_struct.qd/1D13
           CASE theory_struct.label OF
             'Li2': intensity = self.param.w2_factor
             'Li3': intensity = self.param.w3_factor
             'Li4': intensity = self.param.w4_factor
             'Li5': intensity = self.param.w5_factor
             'Li6': intensity = self.param.w6_factor
             
             'Be2': intensity = self.param.be2_factor
             
             ELSE:  BEGIN
              
               ; I really need to label these better.
               if theory_struct.n EQ 2 THEN BEGIN
                 intensity = self.param.w2_factor
               ENDIF ELSE BEGIN
                 MESSAGE, "Unknown dielectronic satellite line in theory file: "+theory_struct.label, /CONTINUE
                 intensity = 0D
               ENDELSE
             ENDELSE
           ENDCASE
         END


         'DIRECT': BEGIN
           intensity_factor = 1D
           CASE theory_struct.label OF
             'W': intensity = self.param.w_intensity
             'X': intensity = self.param.x_intensity
             'Y': intensity = self.param.y_intensity
             'Z': intensity = self.param.z_intensity
             
             'Q': intensity = self.param.q_intensity
             'R': intensity = self.param.r_intensity
             'S': intensity = self.param.s_intensity
             'T': intensity = self.param.t_intensity
             'U': intensity = self.param.u_intensity
             'V': intensity = self.param.v_intensity
             'M2': intensity = self.param.m2_intensity

             'Beta': intensity = self.param.beta_intensity
             
             ELSE:  BEGIN
               MESSAGE, "Unknown direct excitation line in theory file: "+theory_struct.label, /CONTINUE
               intensity = 0D
             ENDELSE
           ENDCASE
         END


         ELSE: BEGIN
           MESSAGE, "Unknown excitation type: "+theory_struct.type, /CONTINUE
           intensity_factor = 1D
           intensity = 0D
         ENDELSE
       ENDCASE

       intensity = intensity * intensity_factor
       
       RETURN, intensity
         
     END ; BST_INST_MODEL_XCRYSTAL_AR16::GET_LINE_INTENSITY



     ;+=========================================================================
     ; PURPOSE:
     ;   Using an atomic model calculate the intenisty of the satelight and li
     ;   lines based on the intensity of the resonant line and the electron 
     ;   temperature.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR16::GET_LINE_INTENSITY_FROM_TE, theory_struct
       COMPILE_OPT STRICTARR

       const = PHYSICAL_CONSTANTS()

       ; Convert to eV
       te = self.param.te * 1e3

       ex_rate_w = XC_GET_EXCITATION_RATE(te, 'W', SOURCE=self.options.excitation_source)

       ; To find the line amplitude, the lines need to be treated differently 
       ; depending on whether they are collisionaly excited or dielectronic recombination lines.
       CASE theory_struct.type OF


         'DIRECT': BEGIN

           CASE theory_struct.label OF
             'W': line_intensity = self.param.w_intensity
             'X': BEGIN
               ex_rate = XC_GET_EXCITATION_RATE(te, 'x', SOURCE=self.options.excitation_source)
               line_intensity = self.param.x_intensity * self.param.w_intensity * ex_rate / ex_rate_w
             END
             'Y': BEGIN
               ex_rate = XC_GET_EXCITATION_RATE(te, 'y', SOURCE=self.options.excitation_source)
               line_intensity = self.param.y_intensity * self.param.w_intensity * ex_rate / ex_rate_w
             END
             'Z': BEGIN
               ex_rate = XC_GET_EXCITATION_RATE(te, 'z', SOURCE=self.options.excitation_source)
               line_intensity = self.param.z_intensity * self.param.w_intensity * ex_rate / ex_rate_w
             END
             ELSE: BEGIN
               ex_rate = 0.0
               line_intensity = 0.0
             ENDELSE
           ENDCASE

           ; Deal with the li-like lines.
           IF theory_struct.charge_state EQ 15 THEN BEGIN
             ; Calculate the excitation rate for this line
             ; 
             ; In Marchuk's data the excitation rate needs to be multiplied by the branching ratio.
             ; This will *probably* be true for other sources, but I have not checked.
             IF self.options.excitation_source NE 'MARCHUK_2004' THEN BEGIN
               MESSAGE, 'Only "MARCHUK_2004" is currently supported.'
             ENDIF

             IF theory_struct.label EQ 'V' OR theory_struct.label EQ 'M2' THEN BEGIN
               ; I don't currently have excitation data for the V line or the M2 line, so for now 
               ; I'll just copy from the U line.
               ex_rate = XC_GET_EXCITATION_RATE(te, 'u', SOURCE=self.options.excitation_source)
             ENDIF ELSE BEGIN
               ex_rate = XC_GET_EXCITATION_RATE(te, theory_struct.label, SOURCE=self.options.excitation_source)
             ENDELSE
             ex_rate = ex_rate * theory_struct.k

             line_intensity = self.param.w_intensity * self.param.li_fraction * ex_rate / ex_rate_w
           ENDIF

           ; I am missing the Beta line here.
           
         END ; DIRECT


         'DIELECTRONIC': BEGIN

           ; The dielectronic recombination rate will be approximately the same for all 
           ; of the satellites from a particular n.
           Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
               *  EXP( -1D * theory_struct.es / Te )
           
           line_intensity = self.param.w_intensity * Z * theory_struct.qd / ex_rate_w
           
         END ; 'DIELECTRONIC'
         
         ELSE: BEGIN
           MESSAGE, 'Unknown line type: '+theory_struct.type
         ENDELSE

       ENDCASE

       RETURN, line_intensity

     END ; BST_INST_MODEL_XCRYSTAL_AR16::GET_LINE_INTENSITY_FROM_TE



     ;+=========================================================================
     ; PURPOSE
     ;   Initialize the model parameters.
     ;
     ; DESCRIPTION:
     ;   Attempt to come up with a reasonable initial guess 
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_MODEL, spectrum

       IF ~ ISA(spectrum) THEN BEGIN
         core = (self.dispatcher).core
         spectrum = (core.spectrum).GET_SPECTRUM()
       ENDIF

       MIR_DEFAULT, ti, 1.0
       MIR_DEFAULT, te, 2.0

       self.param.ti = ti
       self.param.te = te

       self.param.li_fraction = 0.10

       self.param.scale_factor = 1.0

       self.param.w_intensity = 1.0
       self.INITIALIZE_INTENSITY, spectrum, te

       IF self.options.initialize_shifts THEN BEGIN
         self.param.shift_constant = 0.0
         self.param.shift_linear = 0.0
         self.param.shift_quadratic = 0.0
       ENDIF
       
     END ; PRO PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_MODEL



     ;+=========================================================================
     ; PURPOSE
     ;   Make initial guesses for the intensities.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_INTENSITY, spectrum, te

       self.INITIALIZE_INTENSITY_FROM_TE, te
       self.INITIALIZE_BACKGROUND, spectrum
       self.INITIALIZE_INTENSITY_TOTAL, spectrum

     END ; PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_INTENSITY, te



     ;+=========================================================================
     ; PURPOSE
     ;   Make initial guesses for the intensity ratios based on an
     ;   initial electron temperature.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_INTENSITY_FROM_TE, te

       MIR_DEFAULT, te, 1.0
       ; Convert to eV.
       te *= 1e3

       const = PHYSICAL_CONSTANTS()

       ; Get the lines from the theory file.
       line_data = self.GET_THEORY_DATA()
       theory_array = line_data.data


       ex_rate_w = XC_GET_EXCITATION_RATE(te, 'W', SOURCE=self.options.excitation_source)


       ; This is a really ugly way to do this, I'll hopefully clean this up someday.
       ; I especially don't like haveing these atomic physics equations duplicated here
       ; and in GET_LINE_INTENSITY_FROM_TE.  On the otherhand, I want that other routine
       ; to be as readable as possible, and this initialization routine is not as important.

       ; x
       ex_rate = XC_GET_EXCITATION_RATE(te, 'X', SOURCE=self.options.excitation_source)
       self.param.x_intensity = self.param.w_intensity * ex_rate / ex_rate_w
       ; y
       ex_rate = XC_GET_EXCITATION_RATE(te, 'Y', SOURCE=self.options.excitation_source)
       self.param.y_intensity = self.param.w_intensity * ex_rate / ex_rate_w
       ; z
       ex_rate = XC_GET_EXCITATION_RATE(te, 'Z', SOURCE=self.options.excitation_source)
       self.param.z_intensity = self.param.w_intensity * ex_rate / ex_rate_w
       

       ;q
       line_index = (WHERE(theory_array.label EQ 'Q'))[0]
       theory_struct = theory_array[line_index]
       ex_rate = XC_GET_EXCITATION_RATE(te, theory_struct.label, SOURCE=self.options.excitation_source)
       ex_rate = ex_rate * theory_struct.k
       self.param.q_intensity = self.param.w_intensity * self.param.li_fraction * ex_rate / ex_rate_w
       ;r
       line_index = (WHERE(theory_array.label EQ 'R'))[0]
       theory_struct = theory_array[line_index]
       ex_rate = XC_GET_EXCITATION_RATE(te, theory_struct.label, SOURCE=self.options.excitation_source)
       ex_rate = ex_rate * theory_struct.k
       self.param.r_intensity = self.param.w_intensity * self.param.li_fraction * ex_rate / ex_rate_w
       ;s
       line_index = (WHERE(theory_array.label EQ 'S'))[0]
       theory_struct = theory_array[line_index]
       ex_rate = XC_GET_EXCITATION_RATE(te, theory_struct.label, SOURCE=self.options.excitation_source)
       ex_rate = ex_rate * theory_struct.k
       self.param.s_intensity = self.param.w_intensity * self.param.li_fraction * ex_rate / ex_rate_w
       ;t
       line_index = (WHERE(theory_array.label EQ 'T'))[0]
       theory_struct = theory_array[line_index]
       ex_rate = XC_GET_EXCITATION_RATE(te, theory_struct.label, SOURCE=self.options.excitation_source)
       ex_rate = ex_rate * theory_struct.k
       self.param.t_intensity = self.param.w_intensity * self.param.li_fraction * ex_rate / ex_rate_w

       ; The U, V, M2 and Beta lines are direct excitation from the Li like state.
       ; For now just turn them all off.
       self.param.u_intensity = 0D
       self.param.v_intensity = 0D
       self.param.m2_intensity = 0D
       self.param.beta_intensity = 0D


       ;w2
       line_index = (WHERE(theory_array.label EQ 'Li2'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.w2_factor = self.param.w_intensity * Z / ex_rate_w * 1D13
       ;w3
       line_index = (WHERE(theory_array.label EQ 'Li3'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.w3_factor = self.param.w_intensity * Z / ex_rate_w * 1D13
       ;w4
       line_index = (WHERE(theory_array.label EQ 'Li4'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.w4_factor = self.param.w_intensity * Z / ex_rate_w * 1D13
       ;w5
       line_index = (WHERE(theory_array.label EQ 'Li5'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.w5_factor = self.param.w_intensity * Z / ex_rate_w * 1D13

       ;w6
       ; There are not enough lines in the spectrum for this.
       self.param.w6_factor = 0D


       ;Be2
       ;  These lines are excited by dielectronic recombination from Li-like
       ;  argon. Their intensity will depend on the direct-excitaiton u line
       ;  as well as the electron temperature.  Since I don't really have a
       ;  good model for this yet, and since in my normal Ar16+ fits these
       ;  lines will not be visible, I will just set this to zero for now.
       ;
       ;line_index = (WHERE(theory_array.label EQ 'Be2'))[0]
       ;theory_struct = theory_array[line_index]
       ;Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
       ;    *  EXP( -1D * theory_struct.es / Te )
       ;self.param.be2_factor = self.param.u_intensity * Z / ex_rate_u * 1D13
       self.param.be2_factor = 0

     END ; PRO PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_INTENSITY_FROM_TE

     
     ;+=========================================================================
     ; PURPOSE
     ;   Make a first guess for the intensity of the lines.
     ;
     ; DESCRIPTION:
     ;   For the time being I will just use a crude method of achieveing
     ;   this.  Eventually I might want to use a linear fitting method
     ;   for this instead.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_INTENSITY_TOTAL, spectrum

       ; Here I am evaluating the model ONLY.
       ; This is equivilent of assuming that the background is zero.
       y_model = self.EVALUATE(spectrum.x)

       ; Get the background level from the background model.
       ; Assoume the background model has already been initialized.
       model_back = self.dispatcher.GET_MODEL('BST_INST_MODEL_BACKGROUND')
       param_back = model_back.GET_PARAM()
       back_const = param_back.back[0]

       ; Just scale all the line intensities so that the total
       ; intensity matches the total intensity in the spectrum.
       ;
       ; This will only work if the relative intensities are already
       ; at appoximately the correct ratios.
       scale = TOTAL(spectrum.y-back_const)/TOTAL(y_model)

       self.param.w_intensity *= scale
       self.param.x_intensity *= scale
       self.param.y_intensity *= scale
       self.param.z_intensity *= scale
       
       self.param.q_intensity *= scale
       self.param.r_intensity *= scale
       self.param.s_intensity *= scale
       self.param.t_intensity *= scale
       self.param.u_intensity *= scale
       self.param.v_intensity *= scale
       self.param.m2_intensity *= scale

       self.param.beta_intensity *= scale

       self.param.w2_factor *= scale
       self.param.w3_factor *= scale
       self.param.w4_factor *= scale
       self.param.w5_factor *= scale
       self.param.w6_factor *= scale

       self.param.be2_factor *= scale
       
     END ; PRO BST_INST_MODEL_XCRYSTAL_AR16::INITIALIZE_INTENSITY_TOTAL



     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::SET_DEFAULTS

       self.BST_INST_MODEL_XCRYSTAL::SET_DEFAULTS

       self.options.spectrum = 'ar16'
       self.options.system = ''
       self.options.line_source = 'VAINSHTEIN_AR16_2011'


       self.param = { BST_INST_MODEL_XCRYSTAL_AR16__PARAM }

       self.param.te = 1.0
       self.param.ti = 1.0
       
       self.param.shift_constant = 0D
       self.param.shift_linear = 0D
       self.param.shift_quadratic = 0D
       
       self.param.w_intensity = 1.0
       self.param.x_intensity = 1.0
       self.param.y_intensity = 1.0
       self.param.z_intensity = 1.0

       self.param.q_intensity = 0.5
       self.param.r_intensity = 0.5
       self.param.s_intensity = 0.5
       self.param.t_intensity = 0.5
       self.param.u_intensity = 0.5
       self.param.v_intensity = 0.5
       self.param.m2_intensity = 0.5

       self.param.beta_intensity = 0.5

       self.param.w2_factor = 0.5
       self.param.w3_factor = 0.1
       self.param.w4_factor = 0.1
       self.param.w5_factor = 0.1
       self.param.w6_factor = 0.1

       self.param.be2_factor = 0.1

       self.param.li_fraction = 0.1

       self.param.scale_factor = 1.0
       
       self.param.inst_offset = 0D
       self.param.inst_constant = 0D
       self.param.inst_quadratic = 0D

     END ;PRO BST_INST_MODEL_XCRYSTAL_AR16::SET_DEFAULTS



     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self.BST_INST_MODEL_XCRYSTAL::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN

         self.SET_DEFAULTS
         
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_XCRYSTAL_AR16__FIXED }

         self.fixed.ti = 1
         self.fixed.te = 1

         self.fixed.shift_constant = 1
         self.fixed.shift_linear = 1
         self.fixed.shift_quadratic = 1

         self.fixed.w_intensity = 1
         self.fixed.x_intensity = 1
         self.fixed.y_intensity = 1
         self.fixed.z_intensity = 1

         self.fixed.q_intensity = 1
         self.fixed.r_intensity = 1
         self.fixed.s_intensity = 1
         self.fixed.t_intensity = 1
         self.fixed.u_intensity = 1
         self.fixed.v_intensity = 1
         self.fixed.m2_intensity = 1

         self.fixed.beta_intensity = 1

         self.fixed.w2_factor = 1
         self.fixed.w3_factor = 1
         self.fixed.w4_factor = 1
         self.fixed.w5_factor = 1
         self.fixed.w6_factor = 1

         self.fixed.be2_factor = 1

         self.fixed.li_fraction = 1

         self.fixed.scale_factor = 1

         self.fixed.inst_offset = 1
         self.fixed.inst_constant = 1
         self.fixed.inst_quadratic = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_XCRYSTAL_AR16__LIMITED }

         ; Temperature should always be positive.
         self.limited.ti = [1,0]
         self.limited.te = [1,0]

         ; Line amplitudes should always be positive
         self.limited.w_intensity = [1,0]
         self.limited.x_intensity = [1,0]
         self.limited.y_intensity = [1,0]
         self.limited.z_intensity = [1,0]

         self.limited.q_intensity = [1,0]
         self.limited.r_intensity = [1,0]
         self.limited.s_intensity = [1,0]
         self.limited.t_intensity = [1,0]
         self.limited.u_intensity = [1,0]
         self.limited.v_intensity = [1,0]
         self.limited.m2_intensity = [1,0]

         self.limited.beta_intensity = [1,0]

         self.limited.w2_factor = [1,0]
         self.limited.w3_factor = [1,0]
         self.limited.w4_factor = [1,0]
         self.limited.w5_factor = [1,0]
         self.limited.w6_factor = [1,0]

         self.limited.be2_factor = [1,0]

         ; Li line amplitudes should always be positive
         self.limited.li_fraction = [1,0]

         ; Scale factor should always be positive
         self.limited.scale_factor = [1,0]

         ; Instrumental response coefficents should always be positive.
         self.limited.inst_constant = [1,0]
         self.limited.inst_quadratic = [1,0]
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_XCRYSTAL_AR16__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR16::RESET




     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XCRYSTAL model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR16__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_XCRYSTAL_AR16__PARAM $
                 ; Ion and electron temperatures
                 ,ti:0D $
                 ,te:0D $

                 ; These parameters will be used to examine the rotation
                 ,shift_constant:0D $
                 ,shift_linear:0D $
                 ,shift_quadratic:0D $

                 ; Direct excitation from Ar16+
                 ,w_intensity:0D $
                 ,x_intensity:0D $
                 ,y_intensity:0D $
                 ,z_intensity:0D $

                 ; Direct excitation from Ar15+
                 ,q_intensity:0D $
                 ,r_intensity:0D $
                 ,s_intensity:0D $
                 ,t_intensity:0D $
                 ,u_intensity:0D $
                 ,v_intensity:0D $
                 ,m2_intensity:0D $

                 ; Direct excitation from Ar14+
                 ,beta_intensity:0D $

                 ,w2_factor:0D $
                 ,w3_factor:0D $
                 ,w4_factor:0D $
                 ,w5_factor:0D $
                 ,w6_factor:0D $

                 ; Berilyum like satellites.
                 ,be2_factor:0D $

                 ; Intensity of lines coming from direct excitation of  Li-like state.
                 ,li_fraction:0D $

                 ; An overall scale factor to apply to the spectrum.
                 ,scale_factor:0D $

                 ; A set of parameters to control the instrumental width of the spectral lines.
                 ,inst_offset:0D $
                 ,inst_constant:0D $
                 ,inst_quadratic:0D $
               } 

       sigma = { BST_INST_MODEL_XCRYSTAL_AR16__SIGMA $
                 ,ti:0D $
                 ,te:0D $
                 ,shift_constant:0D $
                 ,shift_linear:0D $
                 ,shift_quadratic:0D $
                 ,w_intensity:0D $
                 ,x_intensity:0D $
                 ,y_intensity:0D $
                 ,z_intensity:0D $
                 ,q_intensity:0D $
                 ,r_intensity:0D $
                 ,s_intensity:0D $
                 ,t_intensity:0D $
                 ,u_intensity:0D $
                 ,v_intensity:0D $
                 ,m2_intensity:0D $
                 ,beta_intensity:0D $
                 ,w2_factor:0D $
                 ,w3_factor:0D $
                 ,w4_factor:0D $
                 ,w5_factor:0D $
                 ,w6_factor:0D $
                 ,be2_factor:0D $
                 ,li_fraction:0D $
                 ,scale_factor:0D $
                 ,inst_offset:0D $
                 ,inst_constant:0D$
                 ,inst_quadratic:0D $
               } 


       fixed = { BST_INST_MODEL_XCRYSTAL_AR16__FIXED $
                 ,ti:0 $
                 ,te:0 $
                 ,shift_constant:0 $
                 ,shift_linear:0 $
                 ,shift_quadratic:0 $
                 ,w_intensity:0 $
                 ,x_intensity:0 $
                 ,y_intensity:0 $
                 ,z_intensity:0 $
                 ,q_intensity:0 $
                 ,r_intensity:0 $
                 ,s_intensity:0 $
                 ,t_intensity:0 $
                 ,u_intensity:0 $
                 ,v_intensity:0 $
                 ,m2_intensity:0 $
                 ,beta_intensity:0 $
                 ,w2_factor:0 $
                 ,w3_factor:0 $
                 ,w4_factor:0 $
                 ,w5_factor:0 $
                 ,w6_factor:0 $
                 ,be2_factor:0 $
                 ,li_fraction:0 $
                 ,scale_factor:0 $
                 ,inst_offset:0 $
                 ,inst_constant:0 $
                 ,inst_quadratic:0  $
               } 
       
       limited = { BST_INST_MODEL_XCRYSTAL_AR16__LIMITED $
                 ,ti:[0, 0] $
                 ,te:[0, 0] $
                 ,shift_constant:[0, 0] $
                 ,shift_linear:[0, 0] $
                 ,shift_quadratic:[0, 0] $
                 ,w_intensity:[0, 0] $
                 ,x_intensity:[0, 0] $
                 ,y_intensity:[0, 0] $
                 ,z_intensity:[0, 0] $
                 ,q_intensity:[0, 0] $
                 ,r_intensity:[0, 0] $
                 ,s_intensity:[0, 0] $
                 ,t_intensity:[0, 0] $
                 ,u_intensity:[0, 0] $
                 ,v_intensity:[0, 0] $
                 ,m2_intensity:[0, 0] $
                 ,beta_intensity:[0, 0] $
                 ,w2_factor:[0, 0] $
                 ,w3_factor:[0, 0] $
                 ,w4_factor:[0, 0] $
                 ,w5_factor:[0, 0] $
                 ,w6_factor:[0, 0] $
                 ,be2_factor:[0, 0] $
                 ,li_fraction:[0, 0] $
                 ,scale_factor:[0, 0] $
                 ,inst_offset:[0, 0] $
                 ,inst_constant:[0, 0] $
                 ,inst_quadratic:[0, 0] $
                 }

       limits = { BST_INST_MODEL_XCRYSTAL_AR16__LIMITS $
                 ,ti:[0D, 0D] $
                 ,te:[0D, 0D] $
                 ,shift_constant:[0D, 0D] $
                 ,shift_linear:[0D, 0D] $
                 ,shift_quadratic:[0D, 0D] $
                 ,w_intensity:[0D, 0D] $
                 ,x_intensity:[0D, 0D] $
                 ,y_intensity:[0D, 0D] $
                 ,z_intensity:[0D, 0D] $
                 ,q_intensity:[0D, 0D] $
                 ,r_intensity:[0D, 0D] $
                 ,s_intensity:[0D, 0D] $
                 ,t_intensity:[0D, 0D] $
                 ,u_intensity:[0D, 0D] $
                 ,v_intensity:[0D, 0D] $
                 ,m2_intensity:[0D, 0D] $
                 ,beta_intensity:[0D, 0D] $
                 ,w2_factor:[0D, 0D] $
                 ,w3_factor:[0D, 0D] $
                 ,w4_factor:[0D, 0D] $
                 ,w5_factor:[0D, 0D] $
                 ,w6_factor:[0D, 0D] $
                 ,be2_factor:[0D, 0D] $
                 ,li_fraction:[0D, 0D] $
                 ,scale_factor:[0D, 0D] $
                 ,inst_offset:[0D, 0D] $
                 ,inst_constant:[0D, 0D] $
                 ,inst_quadratic:[0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XCRYSTAL_AR16 $

                 ,param:{ BST_INST_MODEL_XCRYSTAL_AR16__PARAM} $

                 ,fixed:{ BST_INST_MODEL_XCRYSTAL_AR16__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_XCRYSTAL_AR16__LIMITED } $

                 ,limits:{ BST_INST_MODEL_XCRYSTAL_AR16__LIMITS } $

                 ,sigma:{ BST_INST_MODEL_XCRYSTAL_AR16__SIGMA} $

                 ,INHERITS BST_INST_MODEL_XCRYSTAL $
               }


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR16
