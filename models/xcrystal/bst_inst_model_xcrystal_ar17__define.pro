



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This object handels the xcrystal profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit He like argon spectra from the XCIS 
;   system on C-Mod.
;
; DESCRIPTION:
;
; PARAMETERS:
;   ti
;   te
;       (keV)
;       Ion and electron temperature.
;
;   
;   shift_constant
;   shift_linear
;   shift_quadratic
;       (Angstroms)
;       Parameters to calculate the shift of the lines.
;       For the XICS spectrum it is not it is not possible to directly calculate
;       the line shifts due to line integrated nature of the measurement. Instead
;       I just use a quadratic form to approximate the shift of each line and 
;       then analyze the results later
;
;
;   la1_intensity:0D $
;   la2_intensity:0D $
;       (photons/pixel/frame)
;       Intensities of various independently fit lines.
;
;       The units are photons/pixel/frame where pixel is in the spatial direction.
;       By multiplying by pixels/mm one would have a 1D intensity measurement on
;       the detector.
;
;   s2_factor
;   s3_factor
;   s4_factor
;   s5_factor
;   s6_factor
;        (photons/pixel/frame / Qd)
;        These do not represent a true intensity but rather a factor that
;        when multiplied by Qd for an individual line will provide an
;        approximation of the correct line intensity.
;
;        To get an approximation of the total intensity of the satellite
;        group this factor can be multiplied by the sum of all the lines
;        in the group.
;
;   inst_offset
;   inst_constant
;   inst_quadratic
;       (keV)
;       These parameters are used to allow a correction of the line width
;       to account for instrumental broadening.  Instrumental broadening
;       is primarily caused by the Johann error and defocussing effects.
;       Line with correction is done by adding an effective 'instrumental
;       temperature'. This does not correct for line shifts due to
;       instrumental effects.
;
;       This quadratic fuction is centered on the La1 line.
;
;       If the system is well focused on the w line, then inst_offset and
;       inst_constant should be zero. 
;
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_FLAGS

       self.BST_INST_MODEL_XCRYSTAL::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_FLAGS



     ;+=================================================================
     ; PURPOSE
     ;   Setup the model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::PREFIT_CONSTRAINTS


       ; Setup for whether or not to use an atomic model to calculate
       ; the intensity of the satelight lines based on the the
       ; electron temperature.
       IF self.options.use_te THEN BEGIN

         self.fixed.s2_factor = 1
         self.fixed.s3_factor = 1
         self.fixed.s4_factor = 1
         self.fixed.s5_factor = 1
         self.fixed.s6_factor = 1

       ENDIF ELSE BEGIN

         self.fixed.te = 1

       ENDELSE


     END ;FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::PREFIT_CONSTRAINTS



     ;+=========================================================================
     ; PURPOSE
     ;   Evaluate any contstraints after the fit loop is complete.
     ;
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::POSTFIT

       IF self.options.use_te THEN BEGIN

         ; Nothing for now.  
         ; The idea would be to maybe estimate parameters for the satellight
         ; factors

         ;   self.param.s2_factor = 1
         ;   self.param.s3_factor = 1
         ;   self.param.s4_factor = 1
         ;   self.param.s5_factor = 1
         ;   self.param.s6_factor = 1

       ENDIF

     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::POSTFIT


     ;+=========================================================================
     ; PURPOSE:
     ;   Check to see if a given line should be included in the model.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::INCLUDE_LINE, theory_struct

       use_line = 1

       IF ((theory_struct.qd LT self.options.intensity_cutoff) $ 
           AND (theory_struct.type EQ 'DIELECTRONIC')) THEN BEGIN
         use_line = 0
       ENDIF

       IF (self.options.exclude_outside_lines) THEN BEGIN
         spectrum_range = self.GET_SPECTRUM_RANGE(/SORT)

         IF ((theory_struct.wavelength LT spectrum_range[0]) $
             OR (theory_struct.wavelength GT spectrum_range[1])) THEN BEGIN
           use_line = 0
         ENDIF
       ENDIF


       RETURN, use_line

     END ;  FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::INCLUDE_LINE


     
     ;+=========================================================================
     ; PURPOSE:
     ;   Create a list of the lines for the XCrystal model.
     ;
     ;   This list contains no information, but simply creates the required
     ;   <MIR_VOIGT::> object for each line.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::LOAD_LINE_LIST

       line_list = OBJ_NEW('MIR_LIST')
       theory_list = OBJ_NEW('MIR_LIST')


       ; Get the lines from the theory file.
       line_data = self.GET_THEORY_DATA()
       theory_array = line_data.data


       ; Loop through the lines and add them to the spectrum.
       num_lines = N_ELEMENTS(theory_array)

       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = theory_array[ii_line]

         IF self.INCLUDE_LINE(theory_struct) THEN BEGIN
           theory_list.ADD, theory_struct
           voigt = OBJ_NEW('MIR_VOIGT')
           line_list.ADD, voigt
         ENDIF

       ENDFOR
       
       self.param_internal.theory_list = theory_list
       self.param_internal.line_list = line_list


     END ; BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_LIST
     
     
     ;+=========================================================================
     ; PURPOSE:
     ;   Update the line list using the current model parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::UPDATE_LINE_LIST
       const = PHYSICAL_CONSTANTS()

       ; Atomic weight of argon in amu.
       atomic_mass = 39.948D
       ;fe_amu =  55.845D
         
       ; Get our reference wavelength.
       wavelength_ref = self.GET_WAVELENGTH_REF()

       ; Loop through the lines and update the parameters.
       num_lines = N_ELEMENTS(self.param_internal.line_list)
       FOR ii_line=0,num_lines-1 DO BEGIN

         ; Get the theory structure for this line.
         theory_struct = (self.param_internal.theory_list)[ii_line]
         
         ; Natural line width.
         gamma = theory_struct.line_width * theory_struct.wavelength^2  $
                 / ( 4D * !dpi * const.c * 1D10 )

         ; Doppler broadened line width.
         sigma = SQRT(self.param.ti / atomic_mass / const.amu_kg / const.c^2 * const.ev_J * 1e3) $
                 * theory_struct.wavelength
         
         ; Instrumental Width.
         inst_width = self.param.inst_quadratic $
                      * (theory_struct.wavelength - wavelength_ref - self.param.inst_offset)^2 $
                      + self.param.inst_constant

         ; Convolve the line width with the instrumental width (assuming gaussian shapes).
         sigma = SQRT(sigma^2 + inst_width^2)
    
         ; Line location
         location = theory_struct.wavelength $
                    + self.param.shift_constant $
                    + self.param.shift_linear * (theory_struct.wavelength - wavelength_ref) $
                    + self.param.shift_quadratic * (theory_struct.wavelength - wavelength_ref)^2

         IF self.options.use_te THEN BEGIN
           intensity = self.GET_LINE_INTENSITY_FROM_TE(theory_struct)
         ENDIF ELSE BEGIN
           intensity = self.GET_LINE_INTENSITY(theory_struct)
         ENDELSE

         ; Finally multiply by the scale factor.
         intensity *= self.param.scale_factor

       
         voigt = (self.param_internal.line_list)[ii_line]
         voigt.SET, {intensity:intensity $
                     ,location:location $
                     ,sigma:sigma $
                     ,gamma:gamma $
                    }

       ENDFOR


     END ; BST_INST_MODEL_XCRYSTAL_AR17::UPDATE_LINE_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY, theory_struct


       CASE theory_struct.type OF

         'DIELECTRONIC': BEGIN

           ; For the satellite lines, the dielectronic excitation rate for all the lines 
           ; with a given n number has approximately the same Te dependence.
           ; 
           ; We use to our advantage in being able to group the various n numbers with
           ; a single intensity parameter.
           ;
           ; Here the QD factor gives us the relative line amplitudes within an n group.
           intensity_factor =  theory_struct.qd/1D13
           CASE theory_struct.label OF
             'He2': intensity = self.param.s2_factor
             'He3': intensity = self.param.s3_factor
             'He4': intensity = self.param.s4_factor
             'He5': intensity = self.param.s5_factor
             'He6': intensity = self.param.s6_factor
             
             ELSE:  BEGIN
               MESSAGE, "Unknown dielectronic satellite line in theory file: "+theory_struct.label, /CONTINUE
               invalid_line = 1
             ENDELSE
           ENDCASE
         END


         'DIRECT': BEGIN
           intensity_factor = 1D
           CASE theory_struct.label OF
             'La1': intensity = self.param.la1_intensity
             'La2': intensity = self.param.la2_intensity
             
             ELSE:  BEGIN
               MESSAGE, "Unknown direct excitation line in theory file: "+theory_struct.label, /CONTINUE
               invalid_line = 1
             ENDELSE
           ENDCASE
         END


         ELSE: BEGIN
           MESSAGE, "Unknown excitation type: "+theory_struct.type, /CONTINUE
           invalid_line = 1
         ENDELSE
       ENDCASE


       intensity = intensity * intensity_factor
       
       RETURN, intensity
         
     END ; BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY



     ;+=========================================================================
     ; PURPOSE:
     ;   Using an atomic model calculate the intenisty of the satelight and li
     ;   lines based on the intensity of the resonant line and the electron 
     ;   temperature.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY_FROM_TE, theory_struct
       COMPILE_OPT STRICTARR

       const = PHYSICAL_CONSTANTS()

       ; Convert to eV
       te = self.param.te * 1e3

       ex_rate_la1 = XC_GET_EXCITATION_RATE(te, 'La1', SOURCE=self.options.excitation_source)

       ; To find the line amplitude, the lines need to be treated differently 
       ; depending on whether they are collisionaly excited or dielectronic recombination lines.
       CASE theory_struct.type OF

         'DIRECT': BEGIN
           CASE theory_struct.label OF
             'La1': line_intensity = self.param.la1_intensity
             'La2': BEGIN
               ex_rate = XC_GET_EXCITATION_RATE(te, 'La1', SOURCE=self.options.excitation_source)
               line_intensity = self.param.x_intensity * self.param.la1_intensity * ex_rate / ex_rate_w
             END
           ENDCASE
         END

         'DIELECTRONIC': BEGIN

           ; The dielectronic recombination rate will be approximately the same for all 
           ; of the satellites from a particular n.
           Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
               *  EXP( -1D * theory_struct.es / Te )
           
           line_intensity = self.param.la1_intensity * Z * theory_struct.qd / ex_rate_la1
           
         END ; 'SATELLITE'
         
         ELSE: BEGIN
           MESSAGE, 'Unknown line type: '+theory_struct.type
         ENDELSE

       ENDCASE

       RETURN, line_intensity

     END ; BST_INST_MODEL_XCRYSTAL_AR17::GET_LINE_INTENSITY_FROM_TE



     ;+=========================================================================
     ; PURPOSE
     ;   Initialize the model parameters.
     ;
     ; DESCRIPTION:
     ;   Attempt to come up with a reasonable initial guess 
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_MODEL, spectrum

       IF ~ ISA(spectrum) THEN BEGIN
         core = (self.dispatcher).core
         spectrum = (core.spectrum).GET_SPECTRUM()
       ENDIF
       
       MIR_DEFAULT, ti, 1.0
       MIR_DEFAULT, te, 2.0

       self.param.ti = ti
       self.param.te = te

       self.param.scale_factor = 1.0

       self.param.la1_intensity = 1.0
       self.INITIALIZE_INTENSITY, spectrum, te
    
       IF self.options.initialize_shifts THEN BEGIN
         self.param.shift_constant = 0.0
         self.param.shift_linear = 0.0
         self.param.shift_quadratic = 0.0
       ENDIF
       
     END ; PRO PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_MODEL



     ;+=========================================================================
     ; PURPOSE
     ;   Make initial guesses for the intensities.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY, spectrum, te

       self.INITIALIZE_INTENSITY_FROM_TE, te
       self.INITIALIZE_BACKGROUND, spectrum
       self.INITIALIZE_INTENSITY_TOTAL, spectrum

     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY, te



     ;+=========================================================================
     ; PURPOSE
     ;   Make initial guesses for the intensity ratios based on an
     ;   initial electron temperature.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_FROM_TE, te

       MIR_DEFAULT, te, 1.0
       ; Convert to eV.
       te *= 1e3

       const = PHYSICAL_CONSTANTS()

       ; Get the lines from the theory file.
       line_data = self.GET_THEORY_DATA()
       theory_array = line_data.data


       ; TEMPORARY:  For now I don't have excitation rates for ar17.
       self.param.la2_intensity = 0.53 * self.param.la1_intensity
       self.param.s2_factor = 0.001 * self.param.la1_intensity
       self.param.s3_factor = 0.001 * self.param.la1_intensity
       self.param.s4_factor = 0.001 * self.param.la1_intensity
       self.param.s5_factor = 0.001 * self.param.la1_intensity
       self.param.s6_factor = 0D * self.param.la1_intensity
       
       RETURN


       ex_rate_la1 = XC_GET_EXCITATION_RATE(te, 'La1', SOURCE=self.options.excitation_source)


       ; This is a really ugly way to do this, I'll hopefully clean this up someday.
       ; I especially don't like haveing these atomic physics equations duplicated here
       ; and in GET_LINE_INTENSITY_FROM_TE.  On the otherhand, I want 
       ; GET_LINE_INTENSITY_FROM_TE to be as readable as possible, and this initialization 
       ; routine is not as important.

       ; la2
       ex_rate = XC_GET_EXCITATION_RATE(te, 'La2', SOURCE=self.options.excitation_source)
       self.param.la2_intensity = self.param.la1_intensity * ex_rate / ex_rate_la1



       ;s2
       line_index = (WHERE(theory_array.label EQ 'S2'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s2_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13
       ;s3
       line_index = (WHERE(theory_array.label EQ 'S3'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s3_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13
       ;s4
       line_index = (WHERE(theory_array.label EQ 'S4'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s4_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13
       ;s5
       line_index = (WHERE(theory_array.label EQ 'S5'))[0]
       theory_struct = theory_array[line_index]
       Z = const.h^3/(2D*!dpi * const.me)^(3D/2D)/2D / (Te*const.ev_j)^(3D/2D) $
           *  EXP( -1D * theory_struct.es / Te )
       self.param.s5_factor = self.param.la1_intensity * Z / ex_rate_la1 * 1D13

       ;s6
       ; There are not enough lines in the spectrum for this.
       self.param.s6_factor = 0D

     END ; PRO PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_FROM_TE


     ;+=========================================================================
     ; PURPOSE
     ;   Make a first guess for the intensity of the lines.
     ;
     ; DESCRIPTION:
     ;   For the time being I will just use a crude method of achieveing
     ;   this.  Eventually I might want to use a linear fitting method
     ;   for this instead.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_TOTAL, spectrum

       ; Here I am evaluating the model ONLY.
       ; This is equivilent of assuming that the background is zero.
       y_model = self.EVALUATE(spectrum.x)

       ; Get the background level from the background model.
       ; Assoume the background model has already been initialized.
       model_back = self.dispatcher.GET_MODEL('BST_INST_MODEL_BACKGROUND')
       param_back = model_back.GET_PARAM()
       back_const = param_back.back[0]

       ; Just scale all the line intensities so that the total
       ; intensity matches the total intensity in the spectrum.
       ;
       ; This will only work if the relative intensities are already
       ; at appoximately the correct ratios.
       scale = TOTAL(spectrum.y-back_const)/TOTAL(y_model)

       self.param.la1_intensity *= scale
       self.param.la2_intensity *= scale

       self.param.s2_factor *= scale
       self.param.s3_factor *= scale
       self.param.s4_factor *= scale
       self.param.s5_factor *= scale
       self.param.s6_factor *= scale
       
       ;self.param.fe_w_intensity *= scale
       ;self.param.fe_x_intensity *= scale
       ;self.param.fe_y_intensity *= scale
       ;self.param.fe_z_intensity *= scale
       ;self.param.fe_s2_factor *= scale
       
     END ; PRO BST_INST_MODEL_XCRYSTAL_AR17::INITIALIZE_INTENSITY_TOTAL


     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_DEFAULTS

       self.BST_INST_MODEL_XCRYSTAL::SET_DEFAULTS

       self.options.spectrum = 'ar17'
       self.options.system = ''
       self.options.line_source = 'VAINSHTEIN_AR17_2013'
       
       self.options.use_te = 0

       self.param = { BST_INST_MODEL_XCRYSTAL_AR17__PARAM }

       self.param.te = 1.0
       self.param.ti = 1.0
       
       self.param.shift_constant = 0D
       self.param.shift_linear = 0D
       self.param.shift_quadratic = 0D
       
       self.param.la1_intensity = 1.0
       self.param.la2_intensity = 1.0

       self.param.s2_factor = 0.5
       self.param.s3_factor = 0.1
       self.param.s4_factor = 0.1
       self.param.s5_factor = 0.1
       self.param.s6_factor = 0.1

       self.param.scale_factor = 1.0

       self.param.inst_offset = 0D
       self.param.inst_constant = 0D
       self.param.inst_quadratic = 0D

     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self.BST_INST_MODEL_XCRYSTAL::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN

         self.SET_DEFAULTS
         
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_XCRYSTAL_AR17__FIXED }

         self.fixed.ti = 1
         self.fixed.te = 1

         self.fixed.shift_constant = 1
         self.fixed.shift_linear = 1
         self.fixed.shift_quadratic = 1

         self.fixed.la1_intensity = 1
         self.fixed.la2_intensity = 1

         self.fixed.s2_factor = 1
         self.fixed.s3_factor = 1
         self.fixed.s4_factor = 1
         self.fixed.s5_factor = 1
         self.fixed.s6_factor = 1
         
         self.fixed.scale_factor = 1
         
         self.fixed.inst_offset = 1
         self.fixed.inst_constant = 1
         self.fixed.inst_quadratic = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITED }

         ; Temperature should always be positive.
         self.limited.ti = [1,0]
         self.limited.te = [1,0]

         ; Line amplitudes should always be positive
         self.limited.la1_intensity = [1,0]
         self.limited.la2_intensity = [1,0]

         self.limited.s2_factor = [1,0]
         self.limited.s3_factor = [1,0]
         self.limited.s4_factor = [1,0]
         self.limited.s5_factor = [1,0]
         self.limited.s6_factor = [1,0]
         
         ; Scale factor should always be positive
         self.limited.scale_factor = [1,0]

         ; Instrumental response coefficents should always be positive.
         self.limited.inst_constant = [1,0]
         self.limited.inst_quadratic = [1,0]
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17::RESET




     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XCRYSTAL model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XCRYSTAL_AR17__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_XCRYSTAL_AR17__PARAM $
                 ; Ion and electron temperatures
                 ,ti:0D $
                 ,te:0D $

                 ; These parameters will be used to examine the rotation
                 ,shift_constant:0D $
                 ,shift_linear:0D $
                 ,shift_quadratic:0D $

                 ; Direct exitation from Ar17+
                 ,la1_intensity:0D $
                 ,la2_intensity:0D $

                 ; Dielectronic Satellight lines
                 ,s2_factor:0D $
                 ,s3_factor:0D $
                 ,s4_factor:0D $
                 ,s5_factor:0D $
                 ,s6_factor:0D $
                 
                 ; An overall scale factor to apply to the spectrum.
                 ,scale_factor:0D $
                 
                 ; A set of parameters to control the instrumental width of the spectral lines.
                 ,inst_offset:0D $
                 ,inst_constant:0D $
                 ,inst_quadratic:0D $
               } 

       sigma = { BST_INST_MODEL_XCRYSTAL_AR17__SIGMA $
                 ,ti:0D $
                 ,te:0D $
                 ,shift_constant:0D $
                 ,shift_linear:0D $
                 ,shift_quadratic:0D $
                 ,la1_intensity:0D $
                 ,la2_intensity:0D $
                 ,s2_factor:0D $
                 ,s3_factor:0D $
                 ,s4_factor:0D $
                 ,s5_factor:0D $
                 ,s6_factor:0D $
                 ,scale_factor:0D $
                 ,inst_offset:0D $
                 ,inst_constant:0D$
                 ,inst_quadratic:0D $
               } 


       fixed = { BST_INST_MODEL_XCRYSTAL_AR17__FIXED $
                 ,ti:0 $
                 ,te:0 $
                 ,shift_constant:0 $
                 ,shift_linear:0 $
                 ,shift_quadratic:0 $
                 ,la1_intensity:0 $
                 ,la2_intensity:0 $
                 ,s2_factor:0 $
                 ,s3_factor:0 $
                 ,s4_factor:0 $
                 ,s5_factor:0 $
                 ,s6_factor:0 $
                 ,scale_factor:0 $
                 ,inst_offset:0 $
                 ,inst_constant:0 $
                 ,inst_quadratic:0  $
               } 
       
       limited = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITED $
                 ,ti:[0, 0] $
                 ,te:[0, 0] $
                 ,shift_constant:[0, 0] $
                 ,shift_linear:[0, 0] $
                 ,shift_quadratic:[0, 0] $
                 ,la1_intensity:[0, 0] $
                 ,la2_intensity:[0, 0] $
                 ,s2_factor:[0, 0] $
                 ,s3_factor:[0, 0] $
                 ,s4_factor:[0, 0] $
                 ,s5_factor:[0, 0] $
                 ,s6_factor:[0, 0] $
                 ,scale_factor:[0, 0] $
                 ,inst_offset:[0, 0] $
                 ,inst_constant:[0, 0] $
                 ,inst_quadratic:[0, 0] $
                 }

       limits = { BST_INST_MODEL_XCRYSTAL_AR17__LIMITS $
                 ,ti:[0D, 0D] $
                 ,te:[0D, 0D] $
                 ,shift_constant:[0D, 0D] $
                 ,shift_linear:[0D, 0D] $
                 ,shift_quadratic:[0D, 0D] $
                 ,la1_intensity:[0D, 0D] $
                 ,la2_intensity:[0D, 0D] $
                 ,s2_factor:[0D, 0D] $
                 ,s3_factor:[0D, 0D] $
                 ,s4_factor:[0D, 0D] $
                 ,s5_factor:[0D, 0D] $
                 ,s6_factor:[0D, 0D] $
                 ,scale_factor:[0D, 0D] $
                 ,inst_offset:[0D, 0D] $
                 ,inst_constant:[0D, 0D] $
                 ,inst_quadratic:[0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XCRYSTAL_AR17 $

                 ,param:{ BST_INST_MODEL_XCRYSTAL_AR17__PARAM} $

                 ,fixed:{ BST_INST_MODEL_XCRYSTAL_AR17__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_XCRYSTAL_AR17__LIMITED } $

                 ,limits:{ BST_INST_MODEL_XCRYSTAL_AR17__LIMITS } $

                 ,sigma:{ BST_INST_MODEL_XCRYSTAL_AR17__SIGMA} $

                 ,INHERITS BST_INST_MODEL_XCRYSTAL $
               }


     END ;PRO BST_INST_MODEL_XCRYSTAL_AR17
