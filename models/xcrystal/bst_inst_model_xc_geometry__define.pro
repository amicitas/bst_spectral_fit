



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_XC_GEOMETRY::SET_FLAGS



     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints before each evaluation of the model.
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::EVALUATE_CONSTRAINTS

       ; Nothing to do here

     END ; PRO BST_INST_MODEL_XC_GEOMETRY::EVALUATE_CONSTRAINTS


     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the xcrystal model.
     ;
     ; DESCRIPTION:
     ;   This generates a set of values (the 'spectrum') that will be
     ;   compared to the measured values provided by the XC_GEOMETRY_SPECTRA
     ;   addon.  It is important that these values are provided in exactly
     ;   the same order.
     ;
     ;   The order should be as follows:
     ;     detector geometry (measured)
     ;     crystal geometry (measured)
     ;     in-vessel spot locations (measured)
     ;     channel wavelengths (fit)
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))

       ; Adjust the geometry.
       self.ADJUST_DETECTOR_GEOMETRY


       g_guess = self.GET_GEOMETRY_GUESS_OBJECT()
       g = self.GET_GEOMETRY_OBJECT()

       index = 0
       
       ; -----------------------------------------------------------------------
       ; Set the detector and crystal geometry values.

       num_geom_param = 6

       angles = g_guess.GET_DETECTOR_GEOMETRY_ANGULAR()
       y[index+0] = self.param.detector_x
       y[index+1] = self.param.detector_y
       y[index+2] = self.param.detector_z
       y[index+3] = self.param.detector_normal_theta - angles.theta
       y[index+4] = self.param.detector_normal_phi - angles.phi
       y[index+5] = self.param.detector_orientation_alpha - angles.alpha
       index += num_geom_param
       
       angles = g_guess.GET_CRYSTAL_GEOMETRY_ANGULAR()
       y[index+0] = self.param.crystal_x
       y[index+1] = self.param.crystal_y
       y[index+2] = self.param.crystal_z
       y[index+3] = self.param.crystal_normal_theta - angles.theta
       y[index+4] = self.param.crystal_normal_phi - angles.phi
       y[index+5] = self.param.crystal_orientation_alpha - angles.alpha
       index += num_geom_param

       ; -----------------------------------------------------------------------
       ; Set the magnetic axis constraint from the tomograpic inversion.
       
       ; Find the vertical distance between the viewing chord from the
       ; central channel and the magnetic axis.
       ;w_wavelength = 3.9492
       ;ray = g.OBJECT_RAY_FROM_WAVELENGTH_YCHANNEL(w_wavelength, self.spectrum_param.central_row)
       ;xyz = g.INTERSECT_MAJOR_RADIUS(ray, self.spectrum_param.axis_location[0])
       ;y[12] = xyz[2] - self.spectrum_param.axis_location[1]

       
       ; -----------------------------------------------------------------------
       ; Calculate a distance of the source sightlines to the target locations.
       ;
       ; For now simply add the x,y,z distances and three separate parameters.
       ; There are actually only two pieces of independend information
       ; (as opposed to three) but this should work fine for now.

       IF ISA(self.const.target_list) THEN BEGIN
         source_list = self.const.source_list
         target_list = self.const.target_list
         num_target = target_list.N_ELEMENTS()
         FOR ii_target = 0, num_target-1 DO BEGIN
           source = source_list[ii_target]
           target = target_list[ii_target]
           distance = self.DISTANCE_TO_TARGET(source, target, /VECTOR)
           y[index+0:index+2] = distance
           index += 3
         ENDFOR
       ENDIF
       
       ; Note: I am assuming that the spot location given to the
       ;       XC_INST_ADDON_XC_GEOMETRY_SPECTRA is in machine 
       ;       co-ordinates.

       ; The source here is currently given in pixels, in detector coordinates. 
       ;source_pixel = [0D, 0D, 0D]
       ;wavelength = g.WAVELENGTH_FROM_PIXEL(source_pixel)
       ;object_ray = g.OBJECT_RAY_FROM_WAVELENGTH_YPIXEL(wavelength, source_pixel[1])

       ;invessel_spot = g.INTERSECT_MAJOR_RADIUS(object_ray, self.spectrum_param.target_radius)

       ;y[13:15] = invessel_spot


       ; -----------------------------------------------------------------------
       ; Set the channel/wavlength values
       
       channel_list = self.const.channel_list
       
       num_lines = channel_list.N_ELEMENTS()

       ; Determine the number of rows that we are including here.
       num_rows = N_ELEMENTS((channel_list[0])[0,*])

       FOR ii_line = 0, num_lines-1 DO BEGIN
         line_data = channel_list[ii_line]

         FOR ii_row =0, num_rows-1 DO BEGIN
           wavelength = g.WAVELENGTH_FROM_CHANNEL(line_data[*,ii_row])
           y[index] = wavelength
           index += 1
         ENDFOR

       ENDFOR
       
       RETURN, y

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::EVALUATE

     
     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the xcrystal model.
     ;
     ; DESCRIPTION:
     ;   Calculate the distance between an object ray, originiating from the
     ;   source location, to a target location. All locations are in machine
     ;   cooridinates.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::DISTANCE_TO_TARGET, source, target, VECTOR=vector
       COMPILE_OPT STRICTARR

       g = self.GET_GEOMETRY_OBJECT()
       ray = g.OBJECT_RAY_FROM_MACHINE_XYZ(source)

       distance = g.RAY_DISTANCE_TO_POINT(ray, target, VECTOR=vector)

       RETURN, distance
       
     END ; FUNCTION BST_INST_MODEL_XC_GEOMETRY::DISTANCE_TO_TARGET

     
     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Allow the location and orientation of the detector to be adjusted.
     ;
     ;   Allowed adjustments:
     ;     x
     ;     y
     ;     z
     ;     normal_theta
     ;     normal_phi
     ;     orientation_alpha
     ;
     ;   
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::ADJUST_DETECTOR_GEOMETRY


; TEMPORARY
;self.param.detector_orientation_alpha = !dpi
       
       g_guess = self.GET_GEOMETRY_GUESS_OBJECT()
       config_guess = g_guess.GET_CONFIG()

       g = self.GET_GEOMETRY_OBJECT()
       

       ; -----------------------------------------------------------------------
       ; Adjust the detector geometry.


       ; The location of the detector is given by self.param.x,y,z.
       ; These parameters represent the new location of the detector in 
       ; detector coordinates from the guess geometery.
       ;
       ; We need to convert these into machine coordinates.
       detector_location = g_guess.POINT_MACHINE_FROM_DETECTOR([self.param.detector_x $
                                                                ,self.param.detector_y $
                                                                ,self.param.detector_z])

       crystal_location = g_guess.POINT_MACHINE_FROM_CRYSTAL([self.param.crystal_x $
                                                              ,self.param.crystal_y $
                                                              ,self.param.crystal_z])

       ; Determine the normal vectors in machine coordinates.
       detector_normal = MIR_CV_COORD(FROM_SPHER=[1D $
                                                  ,self.param.detector_normal_theta $
                                                  ,self.param.detector_normal_phi] $
                                      ,/TO_RECT)

       crystal_normal = MIR_CV_COORD(FROM_SPHER=[1D $
                                                 ,self.param.crystal_normal_theta $
                                                 ,self.param.crystal_normal_phi] $
                                     ,/TO_RECT)





       ; Finally determine the orientation vectors.
       detector_orientation = CROSSP(detector_normal, CROSSP([1D, 0D, 0D], detector_normal))
       detector_orientation = MIR_VECTOR_ROTATE(detector_orientation $
                                                ,detector_normal $
                                                ,self.param.detector_orientation_alpha)
       detector_orientation = MIR_NORMALIZE(detector_orientation)


       crystal_orientation = CROSSP(crystal_normal, CROSSP([1D, 0D, 0D], crystal_normal))
       crystal_orientation = MIR_VECTOR_ROTATE(crystal_orientation $
                                               ,crystal_normal $
                                               ,self.param.crystal_orientation_alpha)
       crystal_orientation = MIR_NORMALIZE(crystal_orientation)



       config_new = {detector_location:detector_location $
                     ,detector_normal:detector_normal $
                     ,detector_orientation:detector_orientation $

                     ,crystal_location:crystal_location $
                     ,crystal_normal:crystal_normal $
                     ,crystal_orientation:crystal_orientation $
                    }

       
       g.UPDATE_GEOMETRY, config_new

       ;PRINT, ''
       ;PRINT, '', FORMAT='(a0, 80("-"))'
       ;PRINT_STRUCTURE, self.param
       ;PRINT, ''
       ;PRINT_STRUCTURE, config_new
       ;PRINT, ''
       ;g.PRINT_CONFIG
       ;
       ;c_guess = g_guess.GET_CONFIG()
       ;c_new = g.GET_CONFIG()
       ;PRINT, 'obj_geom_guess:', c_guess.detector_location
       ;PRINT, 'obj_geom_new:  ', c_new.detector_location

       ;STOP
       
     END ; PRO BST_INST_MODEL_XC_GEOMETRY::ADJUST_DETECTOR_GEOMETRY



     ;+=================================================================
     ; PURPOSE
     ;   Load the geometry objects from configuration files..
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::LOAD_GEOMETRY_OBJECTS
       COMPILE_OPT STRICTARR


       IF self.options.config_shot GE 0 THEN BEGIN
         shot = self.options.config_shot 
       ENDIF ELSE BEGIN
         shot = self.options.shot 
       ENDELSE

       self.MESSAGE, 'Loading geometry object from shot: '+ XC_GET_SHOT_STRING(self.options.system, shot)

       self.param_internal.geometry = XC_GET_GEOMETRY_OBJECT(self.options.system $
                                                             ,shot)

       self.param_internal.geometry_guess = XC_GET_GEOMETRY_OBJECT(self.options.system $
                                                             ,shot)
       
     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::LOAD_GEOMETRY_OBJECTS


     ;+=================================================================
     ; PURPOSE
     ;   Set the geometry object
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_GEOMETRY_OBJECT, g
       COMPILE_OPT STRICTARR

       self.param_internal.geometry = g

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::SET_GEOMETRY_OBJECT


     ;+=================================================================
     ; PURPOSE
     ;   Set the guess geometry object
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_GEOMETRY_GUESS_OBJECT, g
       COMPILE_OPT STRICTARR

       self.param_internal.geometry_guess = g

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::SET_GEOMETRY_GUESS_OBJECT

     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_GEOMETRY_OBJECT, MODULE_INDEX=module_index
       COMPILE_OPT STRICTARR

       IF NOT ISA(module_index) THEN BEGIN
         module_index = self.options.module
       ENDIF
       
       ; Handle negative module indexes.
       IF ISA(module_index) THEN BEGIN
         IF module_index LT 0 THEN BEGIN
           module_index = !NULL
         ENDIF
       ENDIF
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not retrive geometry object.'
       ENDIF

       
       g = self.param_internal.geometry
       IF ~OBJ_VALID(g) THEN BEGIN
         MESSAGE, 'Geometry object not loaded.'
       ENDIF


       IF ISA(module_index) THEN BEGIN
         RETURN, g.GET_MODULE_GEOMETRY(module_index)
       ENDIF ELSE BEGIN
         RETURN, g
       ENDELSE

     END ; FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_GEOMETRY_OBJECT


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_GEOMETRY_GUESS_OBJECT, MODULE_INDEX=module_index
     COMPILE_OPT STRICTARR

       IF NOT ISA(module_index) THEN BEGIN
         module_index = self.options.module
       ENDIF
            
       ; Handle negative module indexes.
       IF ISA(module_index) THEN BEGIN
         IF module_index LT 0 THEN BEGIN
           module_index = !NULL
         ENDIF
       ENDIF
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not retrive guess geometry object.'
       ENDIF

       
       g = self.param_internal.geometry_guess
       IF ~OBJ_VALID(g) THEN BEGIN
         MESSAGE, 'Guess geometry object not loaded.'
       ENDIF


       IF  ISA(module_index) THEN BEGIN
         RETURN, g.GET_MODULE_GEOMETRY(module_index)
       ENDIF ELSE BEGIN
         RETURN, g
       ENDELSE

     END ; FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_GEOMETRY_GUESS_OBJECT



     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_CHANNEL_LIST, list
       COMPILE_OPT STRICTARR

       self.const.channel_list = list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::SET_CHANNEL_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_CHANNEL_LIST
       COMPILE_OPT STRICTARR

       RETURN, self.const.channel_list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_CHANNEL_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_WAVELENGTH_LIST, list
       COMPILE_OPT STRICTARR

       self.const.wavelength_list = list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::SET_CHANNEL_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_WAVELENGTH_LIST
       COMPILE_OPT STRICTARR

       RETURN, self.const.wavelength_list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_WAVELENGTH_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_SOURCE_LIST, list
       COMPILE_OPT STRICTARR

       self.const.source_list = list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::SET_SOURCE_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_SOURCE_LIST
       COMPILE_OPT STRICTARR

       RETURN, self.const.source_list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_SOURCE_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_TARGET_LIST, list
       COMPILE_OPT STRICTARR

       self.const.target_list = list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::SET_TARGET_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_TARGET_LIST
       COMPILE_OPT STRICTARR

       RETURN, self.const.target_list

     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_TARGET_LIST
     
     
     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default values for the parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::INITIALIZE_PARAM

       g = self.GET_GEOMETRY_GUESS_OBJECT()
       config = g.GET_CONFIG()


       ; -----------------------------------------------------------------------
       ; Detector Geometry.
       self.param.detector_x = 0D
       self.param.detector_y = 0D
       self.param.detector_z = 0D
       
       angles = g.GET_DETECTOR_GEOMETRY_ANGULAR()
       self.param.detector_normal_theta = angles.theta
       self.param.detector_normal_phi = angles.phi
       self.param.detector_orientation_alpha = angles.alpha


       ; -----------------------------------------------------------------------
       ; Crystal Geometry.
       self.param.crystal_x = 0D
       self.param.crystal_y = 0D
       self.param.crystal_z = 0D
       
       angles = g.GET_CRYSTAL_GEOMETRY_ANGULAR()
       self.param.crystal_normal_theta = angles.theta
       self.param.crystal_normal_phi = angles.phi
       self.param.crystal_orientation_alpha = angles.alpha
       

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::INITIALIZE_PARAM


     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;-=========================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_DEFAULTS

       self.options = { BST_INST_MODEL_XC_GEOMETRY__OPTIONS }
       self.const = { BST_INST_MODEL_XC_GEOMETRY__CONST }
       self.param = { BST_INST_MODEL_XC_GEOMETRY__PARAM }

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN
         self->SET_DEFAULTS
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_XC_GEOMETRY__FIXED }
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_XC_GEOMETRY__LIMITED }
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_XC_GEOMETRY__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_XC_GEOMETRY::RESET


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{const:self->GET_CONST()} $
                             ,{options:self->GET_OPTIONS()} $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_STATE


     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state

       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->SET_SPECTRUM_PARAM, state.const
       ENDIF ELSE BEGIN
         self->MESSAGE, 'Could not load model constants.', /ERROR
         CATCH, /CANCEL
       ENDELSE


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->SET_OPTIONS, state.options
       ENDIF ELSE BEGIN
         self->MESSAGE, 'Could not load options.', /ERROR
         CATCH, /CANCEL
       ENDELSE


     END ;PRO BST_INST_MODEL_XC_GEOMETRY::LOAD_STATE


     ;+=================================================================
     ; PURPOSE:
     ;   Get the constants of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_CONST
       
       RETURN, self.const

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::GET_CONST


     ;+=================================================================
     ; PURPOSE:
     ;   Set the model constants.
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_CONST, const
       
       STRUCT_ASSIGN, {const:const}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::SET_CONST


     ;+=================================================================
     ; PURPOSE:
     ;   Get the errors for the evaluated values (the 'spectrum').
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_ERRORS
       
       RETURN, self.errors

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::GET_ERRORS


     ;+=================================================================
     ; PURPOSE:
     ;   Set the errors for the evaluated values (the 'spectrum').
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_ERRORS, errors
       
       STRUCT_ASSIGN, {errors:errors}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::SET_ERRORS

     
     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the options of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_XC_GEOMETRY::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::GET_OPTIONS


     ;+=================================================================
     ; PURPOSE:
     ;   Set the model options
     ;-=================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_XC_GEOMETRY::SET_OPTIONS



     ;+=========================================================================
     ; PURPOSE:
     ;   Define the XC_GEOMETRY model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_XC_GEOMETRY__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_XC_GEOMETRY__PARAM $
                 ,detector_x:0D $
                 ,detector_y:0D $
                 ,detector_z:0D $
                 ,detector_normal_theta:0D $
                 ,detector_normal_phi:0D $
                 ,detector_orientation_alpha:0D $

                 ,crystal_x:0D $
                 ,crystal_y:0D $
                 ,crystal_z:0D $
                 ,crystal_normal_theta:0D $
                 ,crystal_normal_phi:0D $
                 ,crystal_orientation_alpha:0D $
               } 


       sigma = { BST_INST_MODEL_XC_GEOMETRY__SIGMA $
                 ,detector_x:0D $
                 ,detector_y:0D $
                 ,detector_z:0D $
                 ,detector_normal_theta:0D $
                 ,detector_normal_phi:0D $
                 ,detector_orientation_alpha:0D $

                 ,crystal_x:0D $
                 ,crystal_y:0D $
                 ,crystal_z:0D $
                 ,crystal_normal_theta:0D $
                 ,crystal_normal_phi:0D $
                 ,crystal_orientation_alpha:0D $
               } 


       fixed = { BST_INST_MODEL_XC_GEOMETRY__FIXED $
                 ,detector_x:0 $
                 ,detector_y:0 $
                 ,detector_z:0 $
                 ,detector_normal_theta:0 $
                 ,detector_normal_phi:0 $
                 ,detector_orientation_alpha:0 $

                 ,crystal_x:0 $
                 ,crystal_y:0 $
                 ,crystal_z:0 $
                 ,crystal_normal_theta:0 $
                 ,crystal_normal_phi:0 $
                 ,crystal_orientation_alpha:0 $
               } 
       
       limited = { BST_INST_MODEL_XC_GEOMETRY__LIMITED $
                 ,detector_x:[0, 0] $
                 ,detector_y:[0, 0] $
                 ,detector_z:[0, 0] $
                 ,detector_normal_theta:[0, 0] $
                 ,detector_normal_phi:[0, 0] $
                 ,detector_orientation_alpha:[0, 0] $

                 ,crystal_x:[0, 0] $
                 ,crystal_y:[0, 0] $
                 ,crystal_z:[0, 0] $
                 ,crystal_normal_theta:[0, 0] $
                 ,crystal_normal_phi:[0, 0] $
                 ,crystal_orientation_alpha:[0, 0] $
                 }

       limits = { BST_INST_MODEL_XC_GEOMETRY__LIMITS $
                 ,detector_x:[0D, 0D] $
                 ,detector_y:[0D, 0D] $
                 ,detector_z:[0D, 0D] $
                 ,detector_normal_theta:[0D, 0D] $
                 ,detector_normal_phi:[0D, 0D] $
                 ,detector_orientation_alpha:[0D, 0D] $

                 ,crystal_x:[0D, 0D] $
                 ,crystal_y:[0D, 0D] $
                 ,crystal_z:[0D, 0D] $
                 ,crystal_normal_theta:[0D, 0D] $
                 ,crystal_normal_phi:[0D, 0D] $
                 ,crystal_orientation_alpha:[0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Setup additional model data.
       ;
       ; The axis location is in R,Z coordinates.

       options = { BST_INST_MODEL_XC_GEOMETRY__OPTIONS $
                   ,system:'' $
                   ,shot:0L $
                   ,config_shot:0L $
                   ,module:0L $
                 }

       ; wavelength_list
       ;   The wavelength of the calibration lines being used for this
       ;   calibration. In Angstroms.
       ; channel_list
       ;   The fitted location of the calibration lines on the detector,
       ;   in channels. This will be a 2D array with dim=rows,wavelengths.
       ; source_list
       ;   The location of the LED's or laser used for invessel calibration.
       ;   In machine xyz coordinates.
       ; target_list
       ;   The location of the center of the invessel spot locations
       ;   coresponding to the source_list. In machine xyz coordinates.
       const = { BST_INST_MODEL_XC_GEOMETRY__CONST $
                 ,wavelength_list:OBJ_NEW() $
                 ,channel_list:OBJ_NEW() $
                 ,source_list:OBJ_NEW() $
                 ,target_list:OBJ_NEW() $
                 ,central_row:0D $
                 ,axis_location:[0D, 0D] $
               }

       ; Define the erros in the measurements
       errors = { BST_INST_MODEL_XC_GEOMETRY__ERRORS $
                  ,detector_location:[0D,0D,0D] $
                  ,detector_normal_angle:[0D,0D] $
                  ,detector_orientation_angle:0D $
                  ,crystal_location:[0D,0D,0D] $
                  ,crystal_normal_angle:[0D,0D] $
                  ,crystal_orientation_angle:0D $
                  ,wavelength:0D $
                  ,target_location:[0D, 0D, 0D] $
                  ,axis_location:0D $
                }

       param_internal = { BST_INST_MODEL_XC_GEOMETRY__PARAM_INTERNAL $
                          ,geometry:OBJ_NEW() $
                          ,geometry_guess:OBJ_NEW() $
                        }

       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_XC_GEOMETRY $

                 ,param:{ BST_INST_MODEL_XC_GEOMETRY__PARAM} $

                 ,fixed:{ BST_INST_MODEL_XC_GEOMETRY__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_XC_GEOMETRY__LIMITED } $

                 ,limits:{ BST_INST_MODEL_XC_GEOMETRY__LIMITS } $

                 ,sigma:{ BST_INST_MODEL_XC_GEOMETRY__SIGMA} $

                 ,options:{ BST_INST_MODEL_XC_GEOMETRY__OPTIONS } $
                 
                 ,const:{ BST_INST_MODEL_XC_GEOMETRY__CONST } $
                 
                 ,errors:{ BST_INST_MODEL_XC_GEOMETRY__ERRORS } $

                 ,param_internal:{ BST_INST_MODEL_XC_GEOMETRY__PARAM_INTERNAL } $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_XC_GEOMETRY
