


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object handels the scaled profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit scaled profiles to the spectrum.
;
;-======================================================================




     ;+=================================================================
     ; Here we set any inital values
     ;-=================================================================
     FUNCTION BST_INST_SCALED::INIT
       RETURN, 1

     END ;PRO BST_INST_SCALED::INIT



     ;+=================================================================
     ; Evaluate the scaled profile.
     ;-=================================================================
     FUNCTION BST_INST_SCALED::EVALUATE, x, CUTOFF=cutoff 


       ; If there is no amplitude, then do not evaluate.
       IF self.param.amplitude EQ 0 THEN BEGIN
         RETURN, DBLARR(N_ELEMENTS(x))
       ENDIF

       ; Get a sum_of_gauss object with all of the gaussians in the profile.
       sum_of_gauss = self->GET_SUM_OF_GAUSS()
          
       ; Evaluate the sum of gaussian model.
       y = sum_of_gauss->EVALUATE(x, CUTOFF=cutoff)

       sum_of_gauss->DESTROY

       RETURN, y

     END ;PRO BST_INST_SCALED::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a sum of gauss object with all of the gaussians
     ;   appropriately scaled.
     ;-=================================================================
     FUNCTION BST_INST_SCALED::GET_SUM_OF_GAUSS


       ; Copy the internal sum of gauss object
       sum_of_gauss = self.profile->GET_COPY(/RECURSIVE)

       ; Scale the profile.
       ; Save the centroid location.
       sum_of_gauss->SCALE, WIDTH=self.param.width $
                            ,AMPLITUDE=self.param.amplitude $
                            ,CENTROID=centroid 

       ; Move the profile.
       ; Used the saved centroid location.
       sum_of_gauss->MOVE, self.param.location $
                           ,CENTROID=centroid


       RETURN, sum_of_gauss
     END ; FUNCTION BST_INST_SCALED::GET_SUM_OF_GAUSS



     ;+=================================================================
     ; PURPOSE:
     ;   Return the profile object.
     ;
     ;-=================================================================
     FUNCTION BST_INST_SCALED::GET_PROFILE
       
       RETURN, self.profile

     END ;FUNCTION BST_INST_SCALED::GET_PROFILE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the profile object.
     ;
     ;-=================================================================
     PRO BST_INST_SCALED::SET_PROFILE, profile
       
       self.profile = profile

     END ;PRO BST_INST_SCALED::SET_PROFILE.



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the parameters of the model
     ;
     ;-=================================================================
     FUNCTION BST_INST_SCALED::GET_PARAM
       
       RETURN, self.param

     END ;PRO BST_INST_SCALED::GET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model parameters.
     ;
     ;-=================================================================
     PRO BST_INST_SCALED::SET_PARAM, param
       
       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ;PRO BST_INST_SCALED::SET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Cleanup on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_SCALED::CLEANUP, LEAVEDATA=leavedata
       
       IF ~ KEYWORD_SET(leavedata) THEN BEGIN
         IF OBJ_VALID(self.profile) THEN BEGIN
           self.profile->DESTROY
         ENDIF
       ENDIF

     END ;PRO BST_INST_SCALED::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Define the scaled object.
     ;
     ; PROGRAMMING NOTES:
     ;   self.profile is a reference to a <BST_INST_PROFILE::>  object.
     ;
     ;-=================================================================
     PRO BST_INST_SCALED__DEFINE

       struct ={ BST_INST_SCALED $
                 ,profile:OBJ_NEW() $

                 ,param:{ BST_INST_SCALED__PARAM $
                           ,amplitude:0D $
                           ,width:0D $
                           ,location:0D $
                         } $

                 ,INHERITS OBJECT $

               }

     END ;PRO BST_INST_SCALED
