


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   This is the base class for all models used in 
;   <BST_SPECTRAL_FIT>.
;
;   These models are the the core of <BST_SPECTRAL_FIT>.
;   Each model must do a number of things:
;     1. Provide fit variables, with appropriate limits to the fitter.
;     2. Evaluate their contribution to the spectrum.
;
;   Models may communicate with each other.
;
; DESCRIPTION:
;   All model objects used in <BST_SPECTRAL_FIT> should inherit
;   this base model class.
;
;   The best way to get started is to look at an existing model
;   and follow allong.
;
; 
; DATA FOR CHILD OBJECTS:
;   All child objects should have the following properties in their definition.
;
;     param
;
;     fixed
;
;     limited
;
;     limits
;
;
;
; METHODS FOR CHILD OBJECTS:
;   All model objects (those that inherit this object) should have
;   the following methods:
;
;   SET_FLAGS
;     This should generally call
;       self->BST_INST_MODEL::SET_FLAGS
;
;   INIT() 
;     This should generally call
;       self->BST_INST_MODEL::INIT()
;
;   RESET
;     This sholud generally call
;       self->BST_INST_MODEL::RESET
;
;   RESET_ALL
;     This sholud generally call
;       self->BST_INST_MODEL::RESET_ALL
;     That function will in turn call
;       stlf->RESET
;
;   EVALUATE()
;
;
;   EXTRACT_PARAMETER_ARRAY
;
; TO DO:
;   The models dispatch object now inherits from the general dispatch
;   object.  This base model object should now inherit from
;   [BST_INST_DISPATCHED::].  When we do this we need to standard
;   a few of the object parameters: use->enabled, model_active->active.
;   Once this is done I also need to remove the IS_ENABLED method which
;   will inherited from BST_INST_DISPATCHED.
;
;
;   I want to move all parameter models to inherit from 
;   <BST_INST_MODEL_PARAMETER>.  Onece this is done I need to clean
;   up this object by making the following changes (and others):
;     remove: GET_PARAM
;     remove: SET_PARAM
;     simplify: RESTORE_INITIAL_STATE
;         Should not do anything about parameters.
;
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;   This method should be reimplemented by derived classes.
     ;
     ; AVAILABLE FLAGS:
     ;   model_active
     ;   need_evaluate
     ;   gaussian
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::SET_FLAGS

       ; Set the default flags for the model:

       ; Set models to inactive.
       self.model_active = 0

       ; Do not require evaluation.
       self.need_evaluate = 0

       ; Not a gaussian based model
       self.gaussian = 0

     END ; PRO BST_INST_MODEL::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the current model object.
     ;
     ; DESCRIPTION:
     ;   This method will first call the RESET_ALL method, then 
     ;   set any defaults.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL::INIT, dispatcher
       

       IF ISA(dispatcher) THEN BEGIN
         self.dispatcher = dispatcher
       ENDIF

       self->SET_FLAGS


       RETURN, 1

     END ;FUNCTION BST_INST_MODEL::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the dispatcher object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::SET_DISPATCHER, dispatcher
       
       self.dispatcher = dispatcher

     END ;PRO BST_INST_MODEL::SET_DISPATCHER





     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle messages.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::MESSAGE, message $
                                  ,SCOPE_LEVEL=scope_level_in $
                                  ,_REF_EXTRA=extra
       
       ; Update the scope.
       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       
       IF OBJ_VALID(self.dispatcher) THEN BEGIN
         (self.dispatcher)->MESSAGE, message $
                                     ,SCOPE_LEVEL=scope_level $
                                     ,_STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         MIR_LOGGER, message $
                     ,SCOPE_LEVEL=scope_level $
                     ,_EXTRA=extra
       ENDELSE

     END ;PRO BST_INST_MODEL::MESSAGE


;=======================================================================
;=======================================================================
;#######################################################################
;
; PARAMETER INFO METHODS
; 
; These are methods that deal with the conversion to and from the model
; parameters and a parameter info array
;
;#######################################################################
;=======================================================================
;=======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_PARAMETER_INFO

       MESSAGE, 'This method must be reimplemented by derived classes.'

     END ;FUNCTION BST_INST_MODEL::GET_PARAMETER_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Set approprate global flags on the parameter info structure.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_PARAMETER_INFO_FLAGS, parameter_info
       
       IF ~ self.use OR self.fixall THEN BEGIN
         FOR ii=0, self->NUM_PARAM()-1 DO BEGIN
           parameter_info[ii].fixed = 1
         ENDFOR
       ENDIF
        
     END ;PRO BST_INST_MODEL::SET_PARAMETER_INFO_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;
     ; PROGRAMMING NOTES:
     ;   This method is not an efficent way of doing things.
     ;   If this ever needs to be used in the fit loop, then the
     ;   models should reimplement a more efficent method.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_PARAMETER_ARRAY

       parinfo = self->GET_PARAMETER_INFO()
       RETURN, parinfo.value

     END ;FUNCTION BST_INST_MODEL::GET_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Extract the parameter array into the internal structures.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::EXTRACT_PARAMETER_ARRAY, param_array

       MESSAGE, 'This method must be reimplemented by derived classes.'
 
     END ;PRO BST_INST_MODEL::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
    ;   Extract the sigma array into the internal structures.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::EXTRACT_SIGMA_ARRAY, sigma_array

       MESSAGE, 'This method must be reimplemented by derived classes.', /CONTINUE
 
     END ;PRO BST_INST_MODEL::EXTRACT_SIGMA_ARRAY



;=======================================================================
;=======================================================================
;#######################################################################
;
; PROFILE METHODS
; 
; Gaussians models can have a concept of a 'profile' that can be
; used by other models.  These types of model will have the flag
; gaussian = 1.
;
; Below are methods to deal with profiles.
;
;#######################################################################
;=======================================================================
;=======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the 
     ;   gaussians from the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_GAUSSIANS

       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF

       ; This method should be reimplemented by any derived models
       ; based on gaussians.
       MESSAGE, 'Not yet implemented.'

     END ;FUNCTION BST_INST_MODEL::GET_GAUSSIANS



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the Gaussians
     ;   for the given profile in the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_PROFILE, profile_num

       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF

       ; This method should be reimplemented by any derived models
       ; based on gaussians.
       MESSAGE, 'Not yet implemented.'

     END ;FUNCTION BST_INST_MODEL::GET_PROFILE, profile_num



     ;+=================================================================
     ; PURPOSE:
     ;   Return whether or not this model has gaussians associated
     ;   with the given profile number.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::HAS_PROFILE, profile_num

       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF

       ; This method should be reimplemented by any derived models
       ; based on gaussians.
       MESSAGE, 'Not yet implemented.'

     END ;FUNCTION BST_INST_MODEL::HAS_PROFILE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list with the numbers of any profiles contained
     ;   within the model.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_PROFILE_NUMBER_LIST

       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF

       ; This method should be reimplemented by any derived models
       ; based on gaussians.
       MESSAGE, 'Not yet implemented.'

     END ;FUNCTION BST_INST_MODEL::GET_PROFILE_NUMBER_LIST



;=======================================================================
;=======================================================================
;#######################################################################
;
; Below are routines that in most cases do not need to be 
; reimplemented by derived models.
;
;#######################################################################
;=======================================================================
;=======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the model.
     ;
     ; DESCRIPTION:
     ;   This method will be called after all model objects have
     ;   been instantiated.
     ;
     ;   This allows for any actions to be taken that rely on the
     ;   presence of other models.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::INITIALIZE

       self->RESET

     END ;FUNCTION BST_INST_MODEL::INITIALIZE



     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the gui_id untouched
     ;
     ; NOTE: num_param is a parameter set in the INIT method.
     ;       It is instrinsic to the model and should never be 
     ;       changed or reset.
     ;-=================================================================
     PRO BST_INST_MODEL::RESET, ALL=all $
                                ,FIXED=fixed
       
       self->CHECK_RESET_KEYWORDS, FIXED=fixed $
                                   ,ALL=all
                                
       IF all THEN BEGIN
         self.use = 0
         self.save = 1
       ENDIF

       IF fixed OR all THEN BEGIN
         self.fixall = 0
       ENDIF

     END ;PRO BST_INST_MODEL::RESET



     ;+=================================================================
     ; This routine resets all of the object parameters.
     ;
     ; IMPORTANT: The way this works is:
     ;  1. This routine calls self->RESET
     ;     This will be the RESET function of the individual model,
     ;     i.e. BST_INST_MODEL_GAUSSIAN::RESET, not BST_INST_MODEL::RESET.
     ;
     ;  2. The individual model reset function will call
     ;     BST_INST_MODEL::RESET
     ;
     ;  3. We reset what ever is left.
     ;-=================================================================
     PRO BST_INST_MODEL::RESET_ALL
       
       self->RESET 

     END ;PRO BST_INST_MODEL::RESET_ALL



     ;+=================================================================
     ; 
     ; This routine will check if any of the reset keywords are set.
     ; If not it will set them all to true.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::CHECK_RESET_KEYWORDS, FIXED=fixed $
                                               ,ALL=all
       
       fixed = KEYWORD_SET(fixed)
       all = KEYWORD_SET(all)

       keyword_array = [ $
                         fixed $
                       ]

       IF TOTAL(keyword_array) EQ 0 THEN BEGIN
         all=1
       ENDIF


     END ;PRO BST_INST_MODEL::CHECK_RESET_KEYWORDS



     ;+=================================================================
     ; Print out information on the model state.
     ;-=================================================================
     PRO BST_INST_MODEL::PRINT

     END ;PRO BST_INST_MODEL::PRINT

     
     
     ;+=================================================================
     ; Print out information on the model state.
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_PLOTLIST, x_values, _REF_EXTRA=extra
       plotlist = !NULL
       RETURN, plotlist
     END ;FUNCTION BST_INST_MODEL::GET_PLOTLIST


     
     ;+=================================================================
     ; Here we setup the method to set various properties
     ;-=================================================================
     PRO BST_INST_MODEL::SET, EVALUATION_CUTOFF=evaluation_cutoff
                            
       
       IF N_ELEMENTS(evaluation_cutoff) NE 0 THEN BEGIN
         self.evaluation_cutoff = KEYWORD_SET(evaluation_cutoff)
       ENDIF


     END ;PRO BST_INST_MODEL::SET



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the parameters of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_PARAM
       
       RETURN, self.param

     END ;PRO BST_INST_MODEL::GET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model parameters.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_PARAM, param
       
       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ;PRO BST_INST_MODEL::SET_PARAM



     ;+=================================================================
     ;   PURPOSE:
     ;   Here we setup the method to get the fixed status
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_FIXED
       
       RETURN, self.fixed

     END ;PRO BST_INST_MODEL::GET_FIXED



     ;+=================================================================
     ; PURPOSE:
     ;   Set whethere the model parameters are fixed.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_FIXED, fixed
       
       STRUCT_ASSIGN, {fixed:fixed}, self, /NOZERO

     END ;PRO BST_INST_MODEL::SET_FIXED



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the limited status
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_LIMITED
       
       RETURN, self.limited

     END ;PRO BST_INST_MODEL::GET_LIMITED



     ;+=================================================================
     ; PURPOSE:
     ;   Set whether the model parameters are limited.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_LIMITED, limited
       
       STRUCT_ASSIGN, {limited:limited}, self, /NOZERO

     END ;PRO BST_INST_MODEL::SET_LIMITED



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the limits
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_LIMITS
       
       RETURN, self.limits

     END ;PRO BST_INST_MODEL::GET_LIMITS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the parameter limits of the model.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_LIMITS, limits
       
       STRUCT_ASSIGN, {limits:limits}, self, /NOZERO

     END ;PRO BST_INST_MODEL::SET_LIMITS



     ;+=================================================================
     ; PURPOSE:
     ;   Retrive whether all parametres are fixed.
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_FIXALL
       
       RETURN, self.fixall

     END ;PRO BST_INST_MODEL::GET_FIXALL



     ;+=================================================================
     ; PURPOSE:
     ;   Set whether all parametres should be fixed/unfixed.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_FIXALL, fixall
       
       self.fixall = fixall

     END ;PRO BST_INST_MODEL::SET_FIXALL



     ;+=================================================================
     ; PURPOSE:
     ;   Return whether to update the model values after the fit is
     ;   completed.
     ;-=================================================================
     FUNCTION BST_INST_MODEL::SAVE

       RETURN, self.save

     END ;PRO BST_INST_MODEL::SAVE




     ;+=================================================================
     ; PURPOSE:
     ;   Set whether or not to save the fit results
     ;-=================================================================
     PRO BST_INST_MODEL::SET_SAVE, save
       
       self.save = save

     END ;PRO BST_INST_MODEL::SET_SAVE




     ;+=================================================================
     ;   Find out if this model is currently in use.
     ;
     ;   Whether or not a model is used can be changed by either the
     ;   user or internally.
     ;-=================================================================
     FUNCTION BST_INST_MODEL::USE

       RETURN, self.use

     END ;PRO BST_INST_MODEL::USE



     ;+=================================================================
     ; PURPOSE:
     ;   Set whether or not to save the fit results
     ;-=================================================================
     PRO BST_INST_MODEL::SET_USE, use
       
       self.use = use

     END ;PRO BST_INST_MODEL::SET_USE



     ;+=================================================================
     ;
     ;   Return whether or not this model needs to be evaluated.
     ;
     ;   The self.need_evaluate flag should be set by the models
     ;   INIT method.  By default it is set to 1.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::NEED_EVALUATE

       RETURN, self.need_evaluate

     END ;FUNCTION BST_INST_MODEL::NEED_EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Set whether all parametres should be fixed/unfixed.
     ;-=================================================================
     PRO BST_INST_MODEL::SET_NEED_EVALUATE, evaluate
       
       self.need_evaluate = evaluate

     END ;PRO BST_INST_MODEL::SET_NEED_EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Return the number of free parmeters for this model.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::NUM_PARAM

       RETURN, self.num_param

     END ;PRO BST_INST_MODEL::NUM_PARAM




     ;+=================================================================
     ;   Return whether or not this model is currently active.
     ;
     ;   When a model is inactive it cannot be accessed by the fitter.
     ;-=================================================================
     FUNCTION BST_INST_MODEL::IS_ACTIVE

       RETURN, self.model_active

     END ;PRO BST_INST_MODEL::IS_ACTIVE



     ;+=================================================================
     ;   Return whether or not this model is a gaussian model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL::IS_GAUSSIAN

       RETURN, self.gaussian

     END ;PRO BST_INST_MODEL::IS_GAUSSIAN



;=======================================================================
;=======================================================================
;#######################################################################
;
; FIT LOOP ROUTINES
; 
; These are the routines that get called as part of the fitting
; procedure.
;
;#######################################################################
;=======================================================================
;=======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Setup the model prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to be initialized before the fit loop.
     ;
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.  It will be called after the
     ;   inital models state has been saved, so changes to
     ;   model properties will not be retained after the fit.
     ;
     ;   It is different from the INIT or INITIALIZE  methods as this 
     ;   method is not used to initialized the model object, but rather 
     ;   to do any initialization of the model parameters that need to be
     ;   done before each fit.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::PREFIT

       ; This method should be reimplemented if needed

     END ;FUNCTION BST_INST_MODEL::PREFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Setup any model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to setup constraints before the fit loop is started.
     ;
     ;   
     ;   This method will be called by the fitter once
     ;   before the fit loop is started, after the [::PREFIT] method
     ;   has been called on all models.
     ;
     ;   This method is primarily of use when this model (model_1)
     ;   modifies another model (model_2) and requires that model_2 
     ;   is setup beforehand.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::PREFIT_CONSTRAINTS

       ; This method should be reimplemented if needed

     END ;PRO BST_INST_MODEL::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate any model constraints for each iteration of the
     ;   fit loop.
     ;
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to evaluate constraints.
     ;
     ;   
     ;   For each interation of the fit loop [::EVALUATE_CONSTRAINTS]
     ;   will be called for all models.  Next [::EVALUATE] will be 
     ;   called.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::EVALUATE_CONSTRAINTS

       ; This method should be reimplemented if needed

     END ;PRO BST_INST_MODEL::EVALUATE_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate this model.  The result should be the spectrum
     ;   produced by this model.
     ; 
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to be evalutated.
     ;
     ;
     ;   If a model does not need to be evaluated (such as those that
     ;   only apply constraints) then its INIT method should set
     ;   self.need_evaluate = 0.
     ;
     ;   Setting that flag  will prevent the fitter from calling this
     ;   evaluation routine and therefore run slightly faster.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::EVALUATE, x

       ; This method should be reimplemented.

       RETURN, DBLARR(N_ELEMENTS(x))


     END ;FUNCTION BST_INST_MODEL::EVALUATE




     ;+=================================================================
     ; PURPOSE:
     ;   Perform any actions after the fit loop is complete.
     ;
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to do anything after the fit loop is complete.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::POSTFIT

       ; This method should be reimplemented if needed

     END ;FUNCTION BST_INST_MODEL::POSTFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Perform any actions after the fit loop is complete,
     ;   and after the initial state has been restored.
     ;
     ; DESCRIPTION:
     ;   This method should be reimplemented by any model that
     ;   needs to do anything after the fit loop is complete, and
     ;   after the initial model parameter have been restored.
     ;
     ;   Really this should probably never be used.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::AFTER_FIT

       ; This method should be reimplemented if needed

     END ;FUNCTION BST_INST_MODEL::AFTER_FIT



     ;+=================================================================
     ; PURPOSE;
     ;   This will return a structure with all of the properties of
     ;   the model object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::GET_STATE

       RETURN, { $
                 version:self.version $
                 
                 ,use:self.use $
                 ,fixall:self.fixall $
                 ,save:self.save $
               }

     END ;FUNCTION BST_INST_MODEL::GET_STATE



     ;+=================================================================
     ;
     ; Load the state for the this model.
     ;
     ;   By default this will load all of the object properties from
     ;   the loaded state structure except for:
     ;
     ;   dispatcher
     ;   model_active
     ;   version
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::LOAD_STATE, state
       
       self.use = state.use
       self.fixall = state.fixall
       self.save = state.save

     END ;PRO BST_INST_MODEL::LOAD_STATE



     ;+=================================================================
     ; PURPOSE
     ;   Restore the initial state of the model after a fit.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::RESTORE_INITIAL_STATE, model_initial $
                                                ,UPDATE_PARAMETERS=update_param

       ; Copy the initial model parameters to the model.
       self->COPY_FROM, model_initial, /RECURSIVE

     END ;PRO BST_INST_MODEL::RESTORE_INITIAL_STATE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Do a recursive copy of the given object.
     ;
     ;   This is a reimplimentation of [OBJECT::COPY_FROM_RECURSIVE]
     ;   That does not make a copy of the dispatcher object.
     ;
     ; PROGRAMING NOTES:
     ;   The important thing here is that we want to do a recursive
     ;   copy except for the reference to the dispatcher.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL::COPY_FROM, data $
                                    ,RECURSIVE=recursive
 
       recursive = KEYWORD_SET(recursive)
       tags = self->TAG_NAMES()

       FOR ii=0,self->N_TAGS()-1 DO BEGIN
         IF OBJ_VALID(self.(ii)[0]) AND recursive THEN BEGIN
           IF tags[ii] NE 'DISPATCHER' THEN BEGIN

             self.(ii)->COPY_FROM, data.(ii), RECURSIVE=recursive

           ENDIF
         ENDIF ELSE BEGIN
           self.(ii) = data.(ii)
         ENDELSE
       ENDFOR


     END ;PRO BST_INST_MODEL::COPY_FROM





     ;+=================================================================
     ; PURPOSE;
     ;   This is a temporary workaround untill I change BST_INST_MODE
     ;   to inherit from BST_INST_DISPATCHED.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL::IS_ENABLED

       RETURN, self.use
       
     END ;FUNCTION BST_INST_MODEL::IS_ENABLED

     
     ;+=================================================================
     ; PURPOSE:
     ;   Here we set up the base model class.  
     ;
     ;   All models should be derived from this class
     ; 
     ; DESCRIPTION:
     ;
     ;   All models derived from the class must define the 
     ;   following properties:
     ;
     ;   param
     ;   fixed
     ;   limited
     ;   limits
     ;
     ;   See one of the exsting models for an example of how this is 
     ;   done.
     ;-=================================================================
     PRO BST_INST_MODEL__DEFINE

      struct ={ BST_INST_MODEL $

                ,model_active:0 $
                ,num_param:0 $
                ,need_evaluate:0 $

                ,evaluation_cutoff:0 $

                ,gaussian:0 $

                ,use:0 $
                ,fixall:0 $
                ,save:0 $

                ,INHERITS BST_INST_DISPATCHED $
              }

      ; All models derived from the class must define the 
      ; following properties:
      ;
      ;  param
      ;  fixed
      ;  limited
      ;  limits


     END ;PRO BST_INST_MODEL__DEFINE

