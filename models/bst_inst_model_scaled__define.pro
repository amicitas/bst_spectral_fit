


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object handels the scaled profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit scaled profiles to the spectrum.
;
;-======================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 0
       self.need_evaluate = 1
       self.gaussian = 1


     END ; PRO BST_INST_MODEL_SCALED::SET_FLAGS



     ;+=================================================================
     ; Here we set any inital values
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::INIT, dispatcher


       status = self->BST_INST_MODEL_PARAMETER::INIT(dispatcher)

       self.num_param = 3

       RETURN, status

     END ;PRO BST_INST_MODEL_SCALED::INIT



     ;+=================================================================
     ; Evaluate the scaled profile.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::EVALUATE, x


       ; Check the profile number.  
       ; If it is less that zero, don't evaluate.
       IF self.profile LT 0 THEN RETURN, DBLARR(N_ELEMENTS(x))
       IF self.profile EQ self.scaled_profile THEN BEGIN
         MESSAGE, 'A scaled profile cannot scale itself.'
       ENDIF

       ; If there is no amplitude, then do not evaluate.
       IF self.param.amplitude EQ 0 THEN BEGIN
         RETURN, DBLARR(N_ELEMENTS(x))
       ENDIF

       ; Get a sum_of_gauss object with all of the gaussians in the profile.
       sum_of_gauss = self->GET_SUM_OF_GAUSS()
           
       ; Evaluate the sum of gaussian model.
       y = sum_of_gauss->EVALUATE(x,CUTOFF=self.evaluation_cutoff)

       sum_of_gauss->DESTROY

       RETURN, y

     END ;PRO BST_INST_MODEL_SCALED::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a sum of gauss object with all of the gaussians
     ;   appropriately scaled.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_SUM_OF_GAUSS


       ; Get a reference to the profiles object.
       profiles_obj = (self.dispatcher)->GET_PROFILES_OBJECT()


       ; Get the profiles as a [SUM_OF_GAUSS::] object.
       sum_of_gauss = profiles_obj->GET_PROFILE(self.scaled_profile)
       
       IF sum_of_gauss->N_ELEMENTS() EQ 0 THEN BEGIN
         MESSAGE, 'Profile to scale is empty.'
       ENDIF


       ; Scale the profile.
       ; Save the centroid location.
       sum_of_gauss->SCALE, WIDTH=self.param.scale $
                            ,AMPLITUDE=self.param.amplitude $
                            ,CENTROID=centroid 

       ; Move the profile.
       ; Used the saved centroid location.
       sum_of_gauss->MOVE, self.param.location $
                           ,CENTROID=centroid 

       RETURN, sum_of_gauss
     END ; FUNCTION BST_INST_MODEL_SCALED::GET_SUM_OF_GAUSS



     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
         self.profile = 0
         self.scaled_profile = 0
       ENDIF

       IF params OR all THEN BEGIN
         self.param.amplitude = 1D
         self.param.scale = 1D
         self.param.location = 0D
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed.amplitude = 0
         self.fixed.scale = 0
         self.fixed.location = 0
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited.amplitude = [0,0]
         self.limited.scale = [1,0]
         self.limited.location = [0,0]
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits.amplitude = [0D,0D]
         self.limits.scale = [0D,0D]
         self.limits.location = [0D,0D]
       ENDIF


     END ;PRO BST_INST_MODEL_SCALED::RESET



     ;+=================================================================
     ; Here we setup the method to get a structure element
     ;-=================================================================
     ;FUNCTION BST_INST_MODEL_SCALED::GET, AMPLITUDE=amplitude $
     ;                              ,LOCATION=location $
     ;                              ,SCALE=scale $
     ;                              ,PROFILE=profile $
     ;                              ,SCALED_PROFILE=scaled_profile $
     ;                              ,_REF_EXTRA=extra
     ;  
     ;  CASE 1 OF
     ;    KEYWORD_SET(amplitude): RETURN, self.param.amplitude
     ;    KEYWORD_SET(location): RETURN, self.param.location
     ;    KEYWORD_SET(scale): RETURN, self.param.scale
     ;    KEYWORD_SET(profile): RETURN, self.profile
     ;    KEYWORD_SET(scaled_profile): RETURN, self.scaled_profile
     ;    ELSE: BEGIN
     ;      RETURN, self->BST_INST_MODEL_PARAMETER::GET(_EXTRA=extra)
     ;    ENDELSE
     ;  ENDCASE

     ;END ;PRO BST_INST_MODEL_SCALED::GET



     ;+=================================================================
     ; PURPOSE:
     ;   Get the profile number.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_SCALED_PROFILE_NUMBER
       
       RETURN, self.scaled_profile

     END ;FUNCTION BST_INST_MODEL_SCALED::GET_SCALED_PROFILE_NUMBER



     ;+=================================================================
     ; PURPOSE:
     ;   Set the profile number.
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED::SET_SCALED_PROFILE_NUMBER, scaled_profile_number
       
       self.scaled_profile = scaled_profile_number

     END ;PRO BST_INST_MODEL_SCALED::SET_SCALED_PROFILE_NUMBER




     ;+=================================================================
     ; PURPOSE:
     ;   Get the profile number.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_PROFILE_NUMBER
       
       RETURN, self.profile

     END ;FUNCTION BST_INST_MODEL_SCALED::GET_PROFILE_NUMBER



     ;+=================================================================
     ; PURPOSE:
     ;   Set the profile number.
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED::SET_PROFILE_NUMBER, profile_num
       
       self.profile = profile_num

     END ;PRO BST_INST_MODEL_SCALED::SET_PROFILE_NUMBER




     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the 
     ;   gaussians from the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_GAUSSIANS


       sum_of_gauss = OBJ_NEW('BST_INST_PROFILE')

       sum_of_gauss->APPEND, (self->GET_GAUSSIAN())->GET_COPY()
  
       RETURN, sum_of_gauss
     END ;FUNCTION BST_INST_MODEL_SCALED::GET_GAUSSIANS



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the Gaussians
     ;   for the given profile in the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_PROFILE, profile_num

       IF self->HAS_PROFILE(profile_num) THEN BEGIN
         RETURN, self->GET_SUM_OF_GAUSS()
       ENDIF ELSE BEGIN
         RETURN, OBJ_NEW('BST_INST_PROFILE')
       ENDELSE

       RETURN, sum_of_gauss

     END ;FUNCTION BST_INST_MODEL_SCALED::GET_PROFILE, profile_num



     ;+=================================================================
     ; PURPOSE:
     ;   Return whether or not this model has gaussians associated
     ;   with the given profile number.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::HAS_PROFILE, profile_num
       RETURN, (self.profile EQ profile_num)
     END ;FUNCTION BST_INST_MODEL_SCALED::HAS_PROFILE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list with the numbers of any profiles contained
     ;   within the model.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_PROFILE_NUMBER_LIST

       list = OBJ_NEW('SIMPLELIST')
       list->APPEND, self.profile

       RETURN, list
     END ;FUNCTION BST_INST_MODEL_SCALED::GET_PROFILE_NUMBER_LIST


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{ profile:self.profile $
                                ,scaled_profile:self.scaled_profile $
                              } $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_SCALED::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state
       STRUCT_ASSIGN, {profile:state.profile $
                       ,scaled_profile:state.scaled_profile}, self, /NOZERO

     END ;PRO BST_INST_MODEL_SCALED::LOAD_STATE


     ;+=================================================================
     ; Here we set up the structure that will hold a scaled parameter.
     ;
     ; Note that this must also hold some of the gui parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED__DEFINE

       struct ={ BST_INST_MODEL_SCALED $

                 ,scaled_profile:0 $

                 ,profile:0 $

                 ,param:{ BST_INST_MODEL_SCALED__PARAM $
                           ,amplitude:0D $
                           ,scale:0D $
                           ,location:0D $
                         } $

                 ,sigma:{ BST_INST_MODEL_SCALED__SIGMA $
                           ,amplitude:0D $
                           ,scale:0D $
                           ,location:0D $
                         } $

                 ,fixed:{ BST_INST_MODEL_SCALED__FIXED $
                          ,amplitude:0 $
                          ,scale:0 $
                          ,location:0 $
                        } $

                 ,limited:{ BST_INST_MODEL_SCALED__LIMITED $
                            ,amplitude:[0,0] $
                            ,scale:[0,0] $
                            ,location:[0,0] $
                          } $

                 ,limits:{ BST_INST_MODEL_SCALED__LIMITS $
                            ,amplitude:[0D,0D] $
                            ,scale:[0D,0D] $
                            ,location:[0D,0D] $
                          } $

                 
                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_SCALED
