




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   This is a model containing multiple scaled models.
;
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED_MULTI::SET_FLAGS

       self->BST_INST_MODEL_MULTI::SET_FLAGS
  
       self.model_active = 1
       self.gaussian = 1

     END ; PRO BST_INST_MODEL_SCALED_MULTI::SET_FLAGS


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object
     ;-=================================================================
     FUNCTION BST_INST_MODEL_SCALED_MULTI::INIT, dispatcher

       ; NOTE: The order here is important.  
       ;       I need to find a way to simplify this.
       
       ; Set the object parameters.
       self.model_class = 'BST_INST_MODEL_SCALED'
       self.num_models = 6

       ; Initalize the base classes
       status = self->BST_INST_MODEL_MULTI::INIT(dispatcher)

       RETURN, status

     END ;PRO BST_INST_MODEL_MULTI::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Define a model with multiple gaussians.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_SCALED_MULTI__DEFINE

      struct ={ BST_INST_MODEL_SCALED_MULTI $
                ,INHERITS BST_INST_MODEL_MULTI $
              }

     END ;PRO BST_INST_MODEL_SCALED_MULTI__DEFINE
