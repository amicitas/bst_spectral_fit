

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object handels the gaussian model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit gaussians to the the spectrum.
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::SET_FLAGS

       self->BST_INST_MODEL::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 0

       self.save = 0
       self.use = 1


     END ; PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;
     ; DESCRIPTION:
     ;   This model has no parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_POSITIVE_AMPLITUDES::INIT, dispatcher

       status = self->BST_INST_MODEL::INIT(dispatcher)

       self.num_param = 0

       RETURN, status

     END ;PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Reset to defaults.
     ;-=================================================================
     PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::RESET, ALL=all, _REF_EXTRA=extra

       self->BST_INST_MODEL::RESET, ALL=all, _STRICT_EXTRA=extra
       
       IF all THEN BEGIN
         self.use = 1
         self.save = 0
       ENDIF

     END ; PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::RESET



     ;+=================================================================
     ; PURPOSE:
     ;   Setup constaints.
     ;-=================================================================
     PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::PREFIT_CONSTRAINTS

       IF ~ self->USE() THEN RETURN
         
       CATCH, error
       IF error EQ 0 THEN BEGIN
         model_gauss_multi = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')
         num_gauss = model_gauss_multi->NUM_MODELS()
         FOR ii=0,num_gauss-1 DO BEGIN
           model = model_gauss_multi->GET_MODEL(ii)
           
           param = model->GET_PARAM()
           limited = model->GET_LIMITED()
           limits = model->GET_LIMITS()

           limited.amplitude[0] = 1
           IF limits.amplitude[0] LT 0 THEN BEGIN
             limits.amplitude[0] = 0D
           ENDIF
           IF param.amplitude LT 0 THEN BEGIN
             param.amplitude = 0D
           ENDIF

           model->SET_LIMITED, limited
           model->SET_LIMITS, limits
           model->SET_PARAM, param
         ENDFOR
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         (self.dispatcher)->MESSAGE, 'Could not set limits for the GAUSSIAN models.'
       ENDELSE

     END ;PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE;
     ;   This will return a structure with all of the properties of
     ;   this model.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_POSITIVE_AMPLITUDES::GET_STATE

       RETURN, { $
                 version:self.version $
                 
                 ,use:self.use $
                 ,save:self.save $
               }

     END ;FUNCTION BST_INST_MODEL_POSITIVE_AMPLITUDES::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the state for the this model.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::LOAD_STATE, state
       
       self.use = state.use
       self.save = state.save

     END ;PRO BST_INST_MODEL_POSITIVE_AMPLITUDES::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Define the model object.
     ;
     ;   For now don't store anything.
     ;-=================================================================
     PRO BST_INST_MODEL_POSITIVE_AMPLITUDES__DEFINE

       struct ={ BST_INST_MODEL_POSITIVE_AMPLITUDES $

                 ,INHERITS BST_INST_MODEL $
               }


     END ;PRO BST_INST_MODEL_POSITIVE_AMPLITUDES__DEFINE
