;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant w/ B. A. Grierson
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-04
;
; PURPOSE:
;   This object handels the fida model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit a fast ion slowing to the the spectrum.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_FIDA::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1

       self.save = 0
       self.use = 0


     END ; PRO BST_INST_MODEL_FIDA::SET_FLAGS

     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;
     ; DESCRIPTION:
     ;   This model has no parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_FIDA::INIT, dispatcher

       status = self->BST_INST_MODEL_PARAMETER::INIT(dispatcher)

       ;; Modified for location to add a second parameter.
       self.num_param = 2
       self->INIT_PARAMETER_INDEX

       RETURN, status

     END ;PRO BST_INST_MODEL_FIDA::INIT


     ;+=================================================================
     ; PURPOSE:
     ;   Reset to defaults.
     ;-=================================================================
     PRO BST_INST_MODEL_FIDA::RESET, ALL=all, _REF_EXTRA=extra

       self->BST_INST_MODEL_PARAMETER::RESET, ALL=all, _STRICT_EXTRA=extra
       
       IF all THEN BEGIN
         self.save = 0
       ENDIF

     END ; PRO BST_INST_MODEL_FIDA::RESET


     ;+=================================================================
     ; PURPOSE;
     ;   This will return a structure with all of the properties of
     ;   this model.
     ;
     ;
     ;-=================================================================
;      FUNCTION BST_INST_MODEL_FIDA::GET_STATE

;        RETURN, { $
;                  version:self.version $
                 
;                  ,use:self.use $
;                  ,save:self.save $
;                }

;      END ;FUNCTION BST_INST_MODEL_FIDA::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the state for the this model.
     ;
     ;-=================================================================
;      PRO BST_INST_MODEL_FIDA::LOAD_STATE, state
       
;        self.use = state.use
;        self.save = state.save

;      END ;PRO BST_INST_MODEL_FIDA::LOAD_STATE


     ;+=================================================================
     ; Here we get the fida function.
     ;
     ; The fida spectrum f(pixel) is computed externally.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_FIDA::EVALUATE, x

     COMMON CERVIEW_COMMON

     model_bstark=self.dispatcher->GET_MODEL('BST_INST_MODEL_BSTARK')
     shot_param=model_bstark->GET_SHOT_PARAM()
     addon=bst_inst_get_addon('BST_INST_ADDON_CERVIEW')
     ;; Get the timeslice from CERVIEW_COMMON
     ts=(addon->get_struct()).param.ts_min
     time=chord_data.t_start[ts-1]+chord_data.t_integ[ts-1]/2.
;     PRINT,'Chord Time:'+STRTRIM(time,2)
     ;; Get the closest fbm time from EFIT
     files = FILE_SEARCH('/u/grierson/fbm/'+STRTRIM(shot_param.shot,2)+'/fbm_'+$
                         shot_param.chord+'_'+shot_param.beam+'t.*')
     times=INTARR(N_ELEMENTS(files))
     FOR i=0,N_ELEMENTS(files)-1 DO BEGIN
         split=STRSPLIT(files[i],'.',/EXTRACT)
         times[i]=FIX(split[N_ELEMENTS(split)-1])
     ENDFOR
;     PRINT,'Available fbm times: ',times
     tmp=MIN((times-time)^2,wh)
     time=times[wh]
;     PRINT,'Nearest fbm time: ',time
     self.param.time=time

     PRINT,'FIDA amp: '+STRTRIM(self.param.intensity,2)+$
       ', loc:'+STRTRIM(self.param.location,2)+$
       ', time:'+STRTRIM(self.param.time,2)
     

     file='/u/grierson/fbm/'+$
       STRTRIM(shot_param.shot,2)+$
       '/fbm_'+shot_param.chord+$
       '_'+shot_param.beam+'t.'+STRTRIM(FIX(time),2)
;     PRINT,file
     test=FILE_TEST(file)
     IF test THEN BEGIN
;         PRINT,'Restoring:'+file
         RESTORE,file
         spectrum=fbm.spectrum/MAX(fbm.spectrum)
         spectrum=SMOOTH(spectrum,5,/EDGE_TRUNCATE)>0.
         y=INTERPOL(spectrum*self.param.intensity,$
                    fbm.pixel+self.param.location,x)
     ENDIF ELSE BEGIN
         PRINT,'**** FBM File Not Found ****'
         y=FLTARR(N_ELEMENTS(x))
     ENDELSE

     RETURN, y

     END ;PRO BST_INST_MODEL_FIDA::EVALUATE

     ;+=================================================================
     ; Here we get the fida function.
     ;
     ; The fida spectrum f(pixel) is computed externally.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_FIDA::GET_RESULT,x

     COMMON CERVIEW_COMMON

     model_bstark=self.dispatcher->GET_MODEL('BST_INST_MODEL_BSTARK')
     shot_param=model_bstark->GET_SHOT_PARAM()
     addon=bst_inst_get_addon('BST_INST_ADDON_CERVIEW')
     ;; Get the timeslice from CERVIEW_COMMON
     ts=(addon->get_struct()).param.ts_min
     time=chord_data.t_start[ts-1]+chord_data.t_integ[ts-1]/2.
;     PRINT,'Chord Time:'+STRTRIM(time,2)
     ;; Get the closest fbm time from EFIT
     SPAWN,'ls /u/grierson/fbm/'+STRTRIM(shot_param.shot,2)+'/fbm_'+$
       shot_param.chord+'_'+shot_param.beam+'t.*',files
     times=INTARR(N_ELEMENTS(files))
     FOR i=0,N_ELEMENTS(files)-1 DO BEGIN
         split=STRSPLIT(files[i],'.',/EXTRACT)
         times[i]=FIX(split[N_ELEMENTS(split)-1])
     ENDFOR
;     PRINT,'Available fbm times: ',times
     tmp=MIN((times-time)^2,wh)
     time=times[wh]
;     PRINT,'Nearest fbm time: ',time
     self.param.time=time

     PRINT,'FIDA amp: '+STRTRIM(self.param.intensity,2)+$
       ', loc:'+STRTRIM(self.param.location,2)+$
       ', time:'+STRTRIM(self.param.time,2)
     

     file='/u/grierson/fbm/'+$
       STRTRIM(shot_param.shot,2)+$
       '/fbm_'+shot_param.chord+$
       '_'+shot_param.beam+'t.'+STRTRIM(FIX(time),2)
;     PRINT,file
     test=FILE_TEST(file)
     IF test THEN BEGIN
;         PRINT,'Restoring:'+file
         RESTORE,file
         spectrum=fbm.spectrum/MAX(fbm.spectrum)
         spectrum=SMOOTH(spectrum,5,/EDGE_TRUNCATE)>0.
         y=INTERPOL(spectrum*self.param.intensity,$
                    fbm.pixel+self.param.location,x)
     ENDIF ELSE BEGIN
         PRINT,'**** FBM File Not Found ****'
         y=FLTARR(N_ELEMENTS(x))
     ENDELSE

     RETURN, {x:x,y:y}
     
     
     END ;; BST_INST_MODEL_FIDA::GET_RESULT


     ;+=================================================================
     ; PURPOSE:
     ;   To simplify conversions between a parameter array and
     ;   the model parametrs we save the locations of the parameters
     ;   in the index array.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_FIDA::INIT_PARAMETER_INDEX

       self.parameter_index.intensity = 0
       self.parameter_index.location = 1

     END ;PRO BST_INST_MODEL_FIDA::INIT_PARAMETER_INDEX



     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_FIDA::GET_PARAMETER_INFO

       num_param = self->NUM_PARAM()
       parameter_info = REPLICATE({BST_INST_PARAMETER_INFO}, num_param)

       index = self.parameter_index.intensity
       parameter_info[index].value = self.param.intensity
       parameter_info[index].fixed = self.fixed.intensity
       parameter_info[index].limited = self.limited.intensity
       parameter_info[index].limits = self.limits.intensity

       index = self.parameter_index.location
       parameter_info[index].value = self.param.location
       parameter_info[index].fixed = self.fixed.location
       parameter_info[index].limited = self.limited.location
       parameter_info[index].limits = self.limits.location

       self->SET_PARAMETER_INFO_FLAGS, parameter_info

       RETURN, parameter_info

     END ;PRO BST_INST_MODEL_FIDA::GET_PARAMETER_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Extract the parameters from a parameter array.
     ;-=================================================================
     PRO BST_INST_MODEL_FIDA::EXTRACT_PARAMETER_ARRAY, param_array

       self.param.intensity = param_array[self.parameter_index.intensity]
       self.param.location = param_array[self.parameter_index.location]

     END ;PRO BST_INST_MODEL_FIDA::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Define the model object.
     ;
     ;   For now don't store anything.
     ;-=================================================================
     PRO BST_INST_MODEL_FIDA__DEFINE

       struct ={ BST_INST_MODEL_FIDA $

                 ,param:{ BST_INST_MODEL_FIDA__PARAM $
                          ,intensity:0D $
                          ,location:0D $
                          ,time:0D $
                        } $

                 ,sigma:{ BST_INST_MODEL_FIDA__SIGMA $
                          ,intensity:0D $
                          ,location:0D $
                          ,time:0D $
                        } $

                 ,fixed:{ BST_INST_MODEL_FIDA__FIXED $
                          ,intensity:0 $
                          ,location:0 $
                          ,time:0 $
                        } $

                 ,limited:{ BST_INST_MODEL_FIDA__LIMITED $
                            ,intensity:[0,0] $
                            ,location:[0,0] $
                            ,time:[0,0] $
                          } $

                 ,limits:{ BST_INST_MODEL_FIDA__LIMITS $
                           ,intensity:[0D,0D] $
                           ,location:[0D,0D] $
                           ,time:[0D,0D] $
                         } $

                 ,parameter_index:{ BST_INST_MODEL_FIDA__PARAMETER_INDEX $
                                    ,intensity:0 $
                                    ,location:0 $
                                    ,time:0 $
                                  } $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_FIDA__DEFINE


