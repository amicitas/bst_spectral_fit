



;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   This object handels the dalpha_continuum profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit He like argon spectra from the XCIS 
;   system on C-Mod.
;
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the model flags.
     ;
     ;-=========================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_DALPHA_CONTINUUM::SET_FLAGS





     ;+=================================================================
     ; PURPOSE
     ;   Setup the model prior to starting the fit loop
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::PREFIT
       COMPILE_OPT STRICTARR


     END ;FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::PREFIT






     ;+=================================================================
     ; PURPOSE
     ;   Setup the model constraints prior to starting the fit loop.
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::PREFIT_CONSTRAINTS


     END ;FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE
     ;   Evaluate any contstraints before each evaluation of the model.
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::EVALUATE_CONSTRAINTS

       ; Nothing to do here

     END ; PRO BST_INST_MODEL_DALPHA_CONTINUUM::EVALUATE_CONSTRAINTS


     ;+=========================================================================
     ; PURPOSE:
     ;   Evaluate the dalpha_continuum model.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))

       ; Get the line list
       linelist = self->GET_LINE_LIST()

       FOR ii=0,linelist->N_ELEMENTS()-1 DO BEGIN
         line = linelist->GET(ii)
         y += line->EVALUATE(x)
       ENDFOR
       

       ; Destroy the line list and all objects.
       linelist->DESTROY, /HEAP_FREE


       RETURN, y

     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::EVALUATE




     ;+=========================================================================
     ; PURPOSE:
     ;   Get a list of the lines for the Dalpha_Continuum model.
     ;
     ;   The list will contain a <MIR_VOIGT::> object for each line.
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_LINE_LIST

       linelist = OBJ_NEW('MIR_LIST_IDL7')

       ; Get a reference to the bstark model
       bstark_model = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_BSTARK')
       
       ; Get the profile for the continuum.
       profile = bstark_model->GET_BEAM_PROFILE(/CONTINUUM)

       ; Normalize the profile.
       profile->NORMALIZE


       scaled = OBJ_NEW('BST_INST_SCALED')
       scaled->SET_PROFILE, profile

       param = {amplitude:self->GET_LINE_INTENSITY() $
                ,width:1.0D $
                ,location: self->GET_LINE_LOCATION()$
               }

       scaled->SET_PARAM, param

       linelist->APPEND, scaled

       RETURN, linelist


     END ; BST_INST_MODEL_DALPHA_CONTINUUM::GET_LINE_LIST




     ;+=========================================================================
     ; PURPOSE:
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_LINE_INTENSITY

       ; Get a reference to the bstark model
       bstark_model = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_BSTARK')

       full_intensities = bstark_model->GET_STARK_INTENSITY_COMPONENT(/THIRD)

       total_full_intensity = TOTAL(full_intensities)

       RETURN, total_full_intensity*self.param.intensity

     END ; FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_LINE_INTENSITY




     ;+=========================================================================
     ; PURPOSE:
     ;
     ;-=========================================================================
     FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_LINE_LOCATION

       ; Get a reference to the bstark model
       bstark_model = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_BSTARK')

       locations = bstark_model->GET_STARK_LOCATION_COMPONENT(/FULL)

       central_location = locations[4]

       RETURN, central_location + self.param.location

     END ; FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_LINE_LOCATION



     ;+=========================================================================
     ; PURPOSE:
     ;   Print out the state of the model.
     ;-=========================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::PRINT

       PRINT_STRUCTURE, self.param

     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::PRINT



     ;+=========================================================================
     ; PURPOSE:
     ;   Setup default parameters.
     ;-=========================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::SET_DEFAULTS

       self.param = { BST_INST_MODEL_DALPHA_CONTINUUM__PARAM }
       self.param.intensity = 1.0
       self.param.location = 0.0

     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::SET_DEFAULTS


     ;+=========================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=========================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
       ENDIF

       IF params OR all THEN BEGIN

         self->SET_DEFAULTS
         
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_DALPHA_CONTINUUM__FIXED }

         self.fixed.intensity = 1
         self.fixed.location = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_DALPHA_CONTINUUM__LIMITED }

         ; Temperature should always be positive.
         self.limited.intensity = [1,0]

       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_DALPHA_CONTINUUM__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::RESET




     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       ;state = CREATE_STRUCT(state $
       ;                      ,{spectrum_param:self->GET_SPECTRUM_PARAM()} $
       ;                      ,{options:self->GET_OPTIONS()} $
       ;                     )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state

       ;CATCH, error
       ;IF error EQ 0 THEN BEGIN
       ;  self->SET_OPTIONS, state.options
       ;ENDIF ELSE BEGIN
       ;  self->MESSAGE, 'Could not load options.', /ERROR
       ;  CATCH, /CANCEL
       ;ENDELSE


     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the options of the model
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DALPHA_CONTINUUM::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::GET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model options
     ;-=================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM::SET_OPTIONS



     ;+=========================================================================
     ; PURPOSE:
     ;   Define the DALPHA_CONTINUUM model object
     ;
     ;   All of the free model parameters must be defined here.
     ;-=========================================================================
     PRO BST_INST_MODEL_DALPHA_CONTINUUM__DEFINE


       ; -----------------------------------------------------------------------
       ; Setup fit parameters

       param = { BST_INST_MODEL_DALPHA_CONTINUUM__PARAM $
                 ,intensity:0D $
                 ,location:0D $
               } 

       sigma = { BST_INST_MODEL_DALPHA_CONTINUUM__SIGMA $
                 ,intensity:0D $
                 ,location:0D $
               } 


       fixed = { BST_INST_MODEL_DALPHA_CONTINUUM__FIXED $
                 ,intensity:0 $
                 ,location:0 $
               } 
       
       limited = { BST_INST_MODEL_DALPHA_CONTINUUM__LIMITED $
                 ,intensity:[0, 0] $
                 ,location:[0, 0] $
                 }

       limits = { BST_INST_MODEL_DALPHA_CONTINUUM__LIMITS $
                 ,intensity:[0D, 0D] $
                 ,location:[0D, 0D] $
                } 


       ; -----------------------------------------------------------------------
       ; Setup additional model data.



       ; -----------------------------------------------------------------------
       ; Define the object structure.

       struct ={ BST_INST_MODEL_DALPHA_CONTINUUM $

                 ,param:{ BST_INST_MODEL_DALPHA_CONTINUUM__PARAM} $

                 ,fixed:{ BST_INST_MODEL_DALPHA_CONTINUUM__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_DALPHA_CONTINUUM__LIMITED } $

                 ,limits:{ BST_INST_MODEL_DALPHA_CONTINUUM__LIMITS } $

                 ,sigma:{ BST_INST_MODEL_DALPHA_CONTINUUM__SIGMA} $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_DALPHA_CONTINUUM
