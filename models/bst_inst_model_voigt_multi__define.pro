




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2012-04
;
; PURPOSE:
;   This is a model containing multiple voigts.
;
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_VOIGT_MULTI::SET_FLAGS

       self->BST_INST_MODEL_MULTI::SET_FLAGS
  
       self.model_active = 1

     END ; PRO BST_INST_MODEL_VOIGT_MULTI::SET_FLAGS


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object
     ;-=================================================================
     FUNCTION BST_INST_MODEL_VOIGT_MULTI::INIT, dispatcher
       
       ; Set the object parameters.
       self.model_class = 'BST_INST_MODEL_VOIGT'
       self.num_models = 10

       ; Initalize the base classes
       status = self->BST_INST_MODEL_MULTI::INIT(dispatcher)

       RETURN, status

     END ;PRO BST_INST_MODEL_MULTI::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Define a model with multiple voigts.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_VOIGT_MULTI__DEFINE

      struct ={ BST_INST_MODEL_VOIGT_MULTI $
                ,INHERITS BST_INST_MODEL_MULTI $
              }

     END ;PRO BST_INST_MODEL_VOIGT_MULTI__DEFINE
