

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object contains the BACKGROUND MODEL used in
;   <BST_SPECTRAL_FIT>
;
; DESCRIPTION:
;   For the background model we allow up to a quadratic polynomial.
;   In order to make placing the quadratic easyer I use the folowing
;   form:
;     y = a0 + x*a1*(a2 - x/2)
;
;   The advantage of this is that a2 is now the maximum (or minumum)
;   of the quadratic (db/dx = 0 @ x = a2).   
;   The disadvatage is that a1 has no obvious meaning.  Furthermore to 
;   make the quadratic function linear a1 must be very small and a2 
;   very large (d2b/dx2 = -a1/a2).
;-======================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_BACKGROUND::SET_FLAGS



     ;+=================================================================
     ; Here we set any inital values
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BACKGROUND::INIT, dispatcher

       
       status = self->BST_INST_MODEL_PARAMETER::INIT(dispatcher)
       self.num_param = 3

       RETURN, status

     END ;PRO BST_INST_MODEL_BACKGROUND::INIT


     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,SIGMA=sigma $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,SIGMA=sigma $
         ,ALL=all

       IF all THEN BEGIN
         self.num_back=0
       ENDIF

     END ;PRO BST_INST_MODEL_BACKGROUND::RESET


     ;+=================================================================
     ; This routine does a full reset of the object.
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND::RESET_ALL

       ; This will also call self->RESET
       self->BST_INST_MODEL_PARAMETER::RESET_ALL

     END ;PRO BST_INST_MODEL_BACKGROUND::RESET_ALL


     ;+=================================================================
     ; Here we evaluate the background model.
     ;
     ; Note the form used for a quadratic background.
     ; See the model description above.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BACKGROUND::EVALUATE, x

       ; return the background
       CASE self.num_back OF
         1: BEGIN
           y = REPLICATE((self.param.back[0]), N_ELEMENTS(x))
         END
         2: BEGIN
           y = ( $
                    (self.param.back[0]) + $
                    x*(self.param.back[1]) $
                  )
           
         END
         3: BEGIN
           y = ( $
                    (self.param.back[0]) + $
                    x*(self.param.back[1]) * $
                    (self.param.back[2] - x/2D) $
                  )
         END
         ELSE: BEGIN
           y = REPLICATE(0D, N_ELEMENTS(x))
         END
       ENDCASE

       RETURN, y

     END ;PRO BST_INST_MODEL_BACKGROUND::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BACKGROUND::GET_PARAMETER_INFO

       num_param = self->NUM_PARAM()
       parameter_info = REPLICATE({BST_INST_PARAMETER_INFO}, num_param)

       FOR ii=0,num_param-1 DO BEGIN
         parameter_info[ii].value = self.param.back[ii]
         parameter_info[ii].fixed = self.fixed.back[ii]
         parameter_info[ii].limited = self.limited.back[*,ii]
         parameter_info[ii].limits = self.limits.back[*,ii]
       ENDFOR

       self->SET_PARAMETER_INFO_FLAGS, parameter_info

       RETURN, parameter_info

     END ;PRO BST_INST_MODEL_BACKGROUND::GET_PARAMETER_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Return an array with the model parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BACKGROUND::GET_PARAMETER_ARRAY

       parameter_array = self.param.back

       RETURN, parameter_array


     END ;BST_INST_MODEL_BACKGROUND::GET_PARAMETER_ARRAY



     ;+=================================================================
     ; Here we set up the method to extract from the fit_array
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND::EXTRACT_PARAMETER_ARRAY, param_array
       
       self.param.back = param_array

     END ;PRO BST_INST_MODEL_BACKGROUND::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Get the number of background parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BACKGROUND::GET_NUM_BACK
       
       RETURN, self.num_back

     END ;FUNCTION BST_INST_MODEL_BACKGROUND::GET_NUM_BACK



     ;+=================================================================
     ; PURPOSE:
     ;   Set the number of background parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND::SET_NUM_BACK, num_back
       
       self.num_back = num_back

       self.use = (num_back GT 0)

     END ;PRO BST_INST_MODEL_BACKGROUND::SET_NUM_BACK



     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_BACKGROUND::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state, {num_back:self.num_back})

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_BACKGROUND::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state
       STRUCT_ASSIGN, {num_back:state.num_back}, self, /NOZERO

     END ;PRO BST_INST_MODEL_BACKGROUND::LOAD_STATE



     ;+=================================================================
     ; Here we set up the structure that will hold the background 
     ; parameters
     ;
     ; Note that this must also hold some of the gui parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_BACKGROUND__DEFINE

       struct ={ BST_INST_MODEL_BACKGROUND $
                 ,num_back:0 $

                 ,param:{ BST_INST_MODEL_BACKGROUND__PARAM $
                           ,back:[0D, 0D, 0D] $
                         } $

                 ,sigma:{ BST_INST_MODEL_BACKGROUND__SIGMA $
                           ,back:[0D, 0D, 0D] $
                         } $

                 ,fixed:{ BST_INST_MODEL_BACKGROUND__FIXED $
                          ,back:[0, 0, 0] $
                        } $

                 ,limited:{ BST_INST_MODEL_BACKGROUND__LIMITED $
                            ,back:[[0,0], [0,0], [0,0]] $
                          } $

                 ,limits:{ BST_INST_MODEL_BACKGROUND__LIMITS $
                            ,back:[[0D,0D], [0D,0D], [0D,0D]] $
                          } $

                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_BACKGROUND__DEFINE
