



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Define a class to handle multiple models.
;   
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET_FLAGS

       self->BST_INST_MODEL::SET_FLAGS
  
       self.model_active = 0
       self.need_evaluate = 1

     END ; PRO BST_INST_MODEL_MULTI::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object
     ;
     ; KEYWORDS:
     ;   /INSTANTIATE
     ;       If set the internal models will not be instantiated.
     ;       This keyword is used internally, see [::GET_COPY].
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::INIT, dispatcher


       self.models = OBJ_NEW('BST_INST_MODELLIST')

       ; Initalize the base classes.
       status = self->BST_INST_MODEL::INIT(dispatcher)

       self->INSTANTIATE_MODELS


       ; These should not be changed.
; THESE NEED TO BE MOVED INTO THE RESET METHOD?
       self.use = 1
       self.fixall = 0
       self.save = 1

       RETURN, status

     END ;PRO BST_INST_MODEL_MULTI::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Instantiate models.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::INSTANTIATE_MODELS

                       
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         self.models->APPEND, OBJ_NEW(self.model_class, self.dispatcher)
       ENDFOR

     END ;FUNCTION BST_INST_MODEL_MULTI::INSTANTIATE_MODELS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the dispatcher object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET_DISPATCHER, dispatcher
       
       ; Call the base class
       self->BST_INST_MODEL::SET_DISPATCHER, dispatcher

       ; Set the dispatcher on all of the models
                       
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->SET_DISPATCHER, dispatcher
       ENDFOR

     END ;PRO BST_INST_MODEL_MULTI::SET_DISPATCHER



     ;+=================================================================
     ; PURPOSE:
     ;   Get an individual model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_MODEL, index


       RETURN, self.models->GET(index)

     END ;PRO BST_INST_MODEL_MULTI::GET_MODEL





     ;+=================================================================
     ; PURPOSE:
     ;   Return the number of models in this multi model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::NUM_MODELS

       RETURN, self.num_models

     END ;PRO BST_INST_MODEL_MULTI::NUM_MODELS




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the models.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::INITIALIZE

                       
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->INITIALIZE
       ENDFOR


     END ;FUNCTION BST_INST_MODEL_MULTI::INITIALIZE




     ;+=================================================================
     ; PURPOSE:
     ;   This routine does a partial reset of the objects.
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::RESET, _REF_EXTRA=extra

       ; If the models are not instantiated then just return.
       IF self.models->N_ELEMENTS() EQ 0 THEN RETURN


       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->RESET, _STRICT_EXTRA=extra
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::RESET




     ;+=================================================================
     ; PURPOSE:
     ;   This routine resets all of the object parameters.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::RESET_ALL
                       
       ; If the models are not instantiated then just return.
       IF self.models->N_ELEMENTS() EQ 0 THEN RETURN

       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->RESET_ALL
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::RESET_ALL




     ;+=================================================================
     ; PURPOSE:
     ;   Set whether all parametres should be fixed/unfixed.
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET_FIXALL, fixall
       
                       
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->SET_FIXALL, fixall
       ENDFOR

     END ;PRO BST_INST_MODEL_MULTI::SET_FIXALL




     ;+=================================================================
     ; PURPOSE:
     ;   Return a the parameters for all of the models.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_PARAM

       param_list = OBJ_NEW('MIR_LIST_IDL7')
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         param_list->APPEND, (self.models->GET(ii))->GET_PARAM()
       ENDFOR

       param_array = param_list->TO_ARRAY()
       param_list->DESTROY

       RETURN, param_array

     END ;PRO BST_INST_MODEL_MULTI::GET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the parameters of all the models.
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET_PARAM, param_array
       
                       
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->SET_PARAM, param_array[ii]
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::SET_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set whether or not to save the fit results
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET_SAVE, save
       
                       
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->SET_SAVE, save
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::SET_SAVE




     ;+=================================================================
     ; PURPOSE:
     ;   Set whether or not to save the fit results
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET_USE, use
       
                       
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->SET_USE, use
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::SET_USE



     ;+=================================================================
     ; PURPOSE:
     ;   Method to set various properties
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::SET, _REF_EXTRA=extra
                            
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->SET, _STRICT_EXTRA=extra
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::SET



     ;+=================================================================
     ; PURPOSE:
     ;   Return the number of free parmeters for this model.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::NUM_PARAM

       RETURN, (self.models->GET(0))->NUM_PARAM() * self->NUM_MODELS()  

     END ;PRO BST_INST_MODEL_MULTI::NUM_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Call the EVALUATE_CONSTRAINTS method on all models.
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::EVALUATE_CONSTRAINTS

     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           (self.models->GET(ii))->EVALUATE_CONSTRAINTS
         ENDIF
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::EVALUATE_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE:
     ;   Call the PREFIT_CONSTRAINTS method on all models.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::PREFIT_CONSTRAINTS

     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           (self.models->GET(ii))->PREFIT_CONSTRAINTS
         ENDIF
       ENDFOR


     END ;PRO BST_INST_MODEL_MULTI::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the models.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::EVALUATE, x

       y = DBLARR(N_ELEMENTS(x))

       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           y += (self.models->GET(ii))->EVALUATE(x)
         ENDIF
       ENDFOR

       RETURN, y


     END ;FUNCTION BST_INST_MODEL_MULTI::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Call the PREFIT method on all models.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::PREFIT

       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           (self.models->GET(ii))->PREFIT
         ENDIF
       ENDFOR

     END ;FUNCTION BST_INST_MODEL_MULTI::PREFIT




     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_PARAMETER_INFO

       
       parinfo_list = OBJ_NEW('SIMPLELIST')


       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         parinfo_list->APPEND, (self.models->GET(ii))->GET_PARAMETER_INFO()
       ENDFOR
       parinfo_array = parinfo_list->TO_ARRAY()

       OBJ_DESTROY, parinfo_list

       RETURN, parinfo_array


     END ;FUNCTION BST_INST_MODEL_MULTI::GET_PARAMETER_INFO




     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_PARAMETER_ARRAY
       
       param_list = OBJ_NEW('SIMPLELIST')


       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         param_list->APPEND, (self.models->GET(ii))->GET_PARAMETER_ARRAY()
       ENDFOR
       param_array = param_list->TO_ARRAY()

       OBJ_DESTROY, param_list

       RETURN, param_array


     END ;FUNCTION BST_INST_MODEL_MULTI::GET_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Cleanup on object destruction
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::EXTRACT_PARAMETER_ARRAY, param_array
    
       index = 0
       num_param = (self.models->GET(0))->NUM_PARAM()

       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           (self.models->GET(ii))->EXTRACT_PARAMETER_ARRAY, param_array[index:index+num_param-1]
         ENDIF
         index += num_param
       ENDFOR
 
     END ;PRO BST_INST_MODEL_MULTI::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Extract the parameters from the sigma_array back into the object
     ;   structure.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::EXTRACT_SIGMA_ARRAY, sigma_array
    
       index = 0
       num_param = (self.models->GET(0))->NUM_PARAM()

       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           (self.models->GET(ii))->EXTRACT_SIGMA_ARRAY, sigma_array[index:index+num_param-1]
         ENDIF
         index += num_param
       ENDFOR
 
     END ;PRO BST_INST_MODEL_MULTI::EXTRACT_SIGMA_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the 
     ;   gaussians from the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_GAUSSIANS

       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF

       sum_of_gauss = OBJ_NEW('BST_INST_PROFILE')

       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN

           sum_of_gauss_temp = (self.models->GET(ii))->GET_GAUSSIANS()

           sum_of_gauss->JOIN, sum_of_gauss_temp->TO_ARRAY()

           sum_of_gauss_temp->DESTROY
         ENDIF
       ENDFOR

       RETURN, sum_of_gauss
     END ;FUNCTION BST_INST_MODEL_MULTI::GET_GAUSSIANS



     ;+=================================================================
     ; PURPOSE:
     ;   Return a SUM_OF_GAUSS object with all of the Gaussians
     ;   for the given profile in the model.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_PROFILE, profile_num


       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF


       sum_of_gauss = OBJ_NEW('BST_INST_PROFILE')


       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         model = self.models->GET(ii)
         IF model->USE() THEN BEGIN
           IF model->HAS_PROFILE(profile_num) THEN BEGIN

             sum_of_gauss_temp = model->GET_PROFILE(profile_num)

             sum_of_gauss->JOIN, sum_of_gauss_temp->TO_ARRAY()

             sum_of_gauss_temp->DESTROY

           ENDIF
         ENDIF
       ENDFOR


       RETURN, sum_of_gauss

     END ;FUNCTION BST_INST_MODEL_MULTI::GET_PROFILE, profile_num



     ;+=================================================================
     ; PURPOSE:
     ;   Return whether or not this model has gaussians associated
     ;   with the given profile number.
     ;
     ;   Only needed for gaussian based models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::HAS_PROFILE, profile_num

       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF


       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         model = self.models->GET(ii)
         IF model->USE() THEN BEGIN
           IF model->HAS_PROFILE(profile_num) THEN BEGIN
             RETURN, 1
           ENDIF
         ENDIF
       ENDFOR

       ; If we get to this point then none of models had
       ; the requested profile.
       RETURN, 0

     END ;FUNCTION BST_INST_MODEL_MULTI::HAS_PROFILE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list with the numbers of any profiles contained
     ;   within the model.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_PROFILE_NUMBER_LIST


       IF ~ self.gaussian THEN BEGIN
         MESSAGE, 'Not a Gaussian model.'
       ENDIF

       list = OBJ_NEW('SIMPLELIST')



       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         model = self.models->GET(ii)
         IF model->USE() THEN BEGIN
           list_temp = model->GET_PROFILE_NUMBER_LIST()

           IF list_temp->N_ELEMENTS() GT 0 THEN BEGIN
             list->JOIN, list_temp->TO_ARRAY()
           ENDIF

           list_temp->DESTROY
         ENDIF
       ENDFOR



       IF list->N_ELEMENTS() GT 0 THEN BEGIN
         array = list->TO_ARRAY()
         array = GET_UNIQUE(array)
         list->FROM_ARRAY, array
       ENDIF

       RETURN, list
     END ;FUNCTION BST_INST_MODEL_MULTI::GET_PROFILE_NUMBER_LIST



     ;+=================================================================
     ; PURPOSE
     ;   Restore the initial state of the model after a fit.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::RESTORE_INITIAL_STATE, model_initial $
                                                      ,UPDATE_PARAMETERS=update_param
     
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         (self.models->GET(ii))->RESTORE_INITIAL_STATE, model_initial.models->GET(ii) $
            ,UPDATE_PARAMETERS=update_param
       ENDFOR

       ; WARNING!!!!
       ;
       ;     This really should be done in <BST_INST_MODEL::>.
       ;     When I get a chance I need to remove all references to parameters from
       ;     <BST_INST_MODEL::> then create a derived class specific to parameter 
       ;     based models. 
       self.dispatcher = model_initial.dispatcher
       self.version = model_initial.version

       self.model_active = model_initial.model_active
       self.num_param = model_initial.num_param
       self.need_evaluate = model_initial.need_evaluate

       self.evaluation_cutoff = model_initial.evaluation_cutoff

       self.gaussian = model_initial.gaussian

       self.use = model_initial.use
       self.fixall = model_initial.fixall
       self.save = model_initial.save


     END ;PRO BST_INST_MODEL_MULTI::RESTORE_INITIAL_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the state of the models.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::LOAD_STATE, state

       num_current_models = self->NUM_MODELS()
       num_saved_models = N_ELEMENTS(state)

       ; In general we will try to load everything from the save file.
       ; In certain cases we will only want to load the models
       ; that were in use in the save state.
       check_use = 0

       IF num_current_models LT num_saved_models THEN BEGIN
         self.dispatcher->MESSAGE, ['Too many models in the save state.' $
                                ,'Attempting to load only those that were in use.']
         check_use = 1
       ENDIF


       index = 0
       FOR ii=0,num_saved_models-1 DO BEGIN

         IF index GE num_current_models THEN BEGIN
           MESSAGE, 'Too many models in the save state.'
         ENDIF


         IF check_use THEN BEGIN
           IF ~ (state[ii])->USE() THEN BEGIN
             CONTINUE
           ENDIF
         ENDIF

         (self.models->GET(index))->LOAD_STATE, state[ii]
         index += 1
       ENDFOR

     END ;PRO BST_INST_MODEL_MULTI::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Save the state of the models.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_MULTI::GET_STATE

       num_models = self->NUM_MODELS()
       
       state_list = OBJ_NEW('SIMPLELIST')


       FOR ii=0,num_models-1 DO BEGIN
         state_list->APPEND, (self.models->GET(ii))->GET_STATE()
       ENDFOR
       state_array = state_list->TO_ARRAY()

       OBJ_DESTROY, state_list

       RETURN, state_array

     END ;FUNCTION BST_INST_MODEL_MULTI::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Print out the state of the models.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::PRINT
    
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
         IF (self.models->GET(ii))->USE() THEN BEGIN
           (self.models->GET(ii))->PRINT
           PRINT, ''
         ENDIF
       ENDFOR
 
     END ;PRO BST_INST_MODEL_MULTI::EXTRACT_SIGMA_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Cleanup on object destruction
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI::CLEANUP
    
       FOR ii=0,self->NUM_MODELS()-1 DO BEGIN
           (self.models->GET(ii))->DESTROY
       ENDFOR
                       
       self.models->DESTROY

       self->BST_INST_MODEL::CLEANUP

     END ;FUNCTION BST_INST_MODEL_MULTI::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Define a class to handle multiple models.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_MULTI__DEFINE

      struct ={ BST_INST_MODEL_MULTI $
                ,num_models:0L $
                ,model_class:'' $

                ,models:OBJ_NEW() $

                ,INHERITS BST_INST_MODEL $
              }

     END ;PRO BST_INST_MODEL_MULTI__DEFINE
