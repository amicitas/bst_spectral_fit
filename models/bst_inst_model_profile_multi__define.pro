




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   This is a model containing multiple gaussians.
;
;   It is exactly the same as [BST_INST_MODEL_GAUSSIAN_MULTI].  It
;   is kept separate from the gaussian model so that it can be used
;   to handle line profiles for the B-Stark model.
;
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_PROFILE_MULTI::SET_FLAGS

       self->BST_INST_MODEL_MULTI::SET_FLAGS
  
       self.model_active = 1
       self.gaussian = 1

     END ; PRO BST_INST_MODEL_PROFILE_MULTI::SET_FLAGS


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object
     ;-=================================================================
     FUNCTION BST_INST_MODEL_PROFILE_MULTI::INIT, dispatcher

       ; NOTE: The order here is important.  
       ;       I need to find a way to simplify this.
       
       ; Set the object parameters.
       self.model_class = 'BST_INST_MODEL_GAUSSIAN'
       self.num_models = 20

       ; Initalize the base classes
       status = self->BST_INST_MODEL_MULTI::INIT(dispatcher)

       RETURN, status

     END ;PRO BST_INST_MODEL_MULTI::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Define a model with multiple gaussians.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_PROFILE_MULTI__DEFINE

      struct ={ BST_INST_MODEL_PROFILE_MULTI $
                ,INHERITS BST_INST_MODEL_MULTI $
              }

     END ;PRO BST_INST_MODEL_PROFILE_MULTI__DEFINE
