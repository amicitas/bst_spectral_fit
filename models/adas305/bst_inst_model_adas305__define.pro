


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This object handels the bstark profile model for 
;   <BST_SPECTRAL_FIT>.
;
;   This model is used to fit bstark profiles to the spectrum.
;
; REQUIREMENTS:
;   CERVIEW must be running.
;   This model must have access to the B-Stark IDL library.
;
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::SET_FLAGS

       self->BST_INST_MODEL_PARAMETER::SET_FLAGS
  
       ; Set the flags.
       self.model_active = 1
       self.need_evaluate = 1


     END ; PRO BST_INST_MODEL_ADAS305::SET_FLAGS



     ;+=================================================================
     ; Here we set any inital values
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::INIT, dispatcher

       
       RESOLVE_ROUTINE, [ $
                           'bst_stark_model' $
                          ,'bst_dispersion' $
                          ,'mst_dispersion' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE

       status = self->BST_INST_MODEL_PARAMETER::INIT(dispatcher)

       self.num_components = 3

       self.num_param = N_ELEMENTS_STRUCTURE(self.param)

       self->INIT_PARAMETER_INDEX

       RETURN, status

     END ;PRO BST_INST_MODEL_ADAS305::INIT



     ;+=================================================================
     ; PURPOSE
     ;   Setup the model prior to starting the fit loop
     ;
     ; DESCRIPTION:
     ;
     ;   This method will be called by the fitter once before the 
     ;   fit loop is started.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::PREFIT

       ;BST_CHORD_PARAM, self.shot_param.shot $
       ;                 ,self.shot_param.chord $
       ;                 ,self.shot_param.beam

     END ;FUNCTION BST_INST_MODEL_ADAS305::PREFIT



     ;+=================================================================
     ; Evaluate the bstark profile.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::EVALUATE, x, COMPONENTS=components


       y = DBLARR(N_ELEMENTS(x))

       ; Get a scaled object with all of the model lines.
       scaled_list = self->GET_SCALED_LIST(COMPONENTS=components)

       ; Evaluate the model.
       FOR ii=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
         scaled = scaled_list->GET(ii)

         y += scaled->EVALUATE(x,CUTOFF=self.evaluation_cutoff)
       ENDFOR


       ; Destroy the sclaled list.
       FOR ii=0,scaled_list->N_ELEMENTS()-1 DO BEGIN
         scaled = scaled_list->GET(ii)

         scaled->DESTROY
       ENDFOR
       scaled_list->DESTROY


       RETURN, y

     END ;PRO BST_INST_MODEL_ADAS305::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list of scaled objects for each of the lines
     ;   in the stark model.
     ;
     ; PROGRAMMING NOTES:
     ;   The profiles used for the line shape are normalized to have
     ;   an intensity of 1 before being added to the scaled objects.
     ;   This is especially important if the line shape is allowed to
     ;   vary in the fits.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_SCALED_LIST, COMPONENTS=components

       ; Reload the chord parameters.
       ;   NOTE: The parameters are cached, so this is fast.
       ;BST_CHORD_PARAM, self.shot_param.shot $
       ;                 ,self.shot_param.chord $
       ;                 ,self.shot_param.beam


       ; Get the line locations and intensities.
       lines = self->GET_STARK_LINES()


       scaled_list = OBJ_NEW('SIMPLELIST')

       ; Get a reference to the profiles object.
       profiles_obj = (self.dispatcher)->GET_PROFILES_OBJECT()

       ; Chose which profiles to return.
       MIR_DEFAULT, components, [0,1,2]

       ; Here we will assume the profile numebers are:
       ; full: -1
       ; half: -2
       ; third: -3
       FOR ii=0,N_ELEMENTS(components)-1 DO BEGIN
         profile_num = components(ii)

         ; Get the profile.
         profile = self->GET_BEAM_PROFILE(profile_num)
         
         ; Normalize the profile.
         profile->NORMALIZE

         FOR jj=0,N_ELEMENTS(lines.(profile_num).intensity)-1 DO BEGIN
           scaled = OBJ_NEW('BST_INST_SCALED')
           scaled->SET_PROFILE, profile

           param = {amplitude:lines.(profile_num).intensity[jj] $
                    ,width:1D $
                    ,location:lines.(profile_num).channel[jj] $
                   }

           scaled->SET_PARAM, param

           scaled_list->APPEND, scaled

         ENDFOR
       ENDFOR

       RETURN, scaled_list

     END ; FUNCTION BST_INST_MODEL_ADAS305::GET_SCALED_LIST





     ;+=================================================================
     ; PURPOSE:
     ;   Return the profile for the requested beam component.
     ;
     ; DESCRIPTION:
     ;   Here we will assume the profile numbers are:
     ;   full:  -1
     ;   half:  -2
     ;   third: -3
     ;
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_BEAM_PROFILE, component_in $
                                                       ,FULL=full $
                                                       ,HALF=half $
                                                       ,THIRD=third

       ; Parse the inputs.
       IF ISA(component_in) THEN BEGIN
         component = component_in
       ENDIF ELSE BEGIN
         CASE 1 OF
           KEYWORD_SET(full): component = 0
           KEYWORD_SET(half): component = 1
           KEYWROD_SET(third): component = 2
           ELSE: MESSAGE, 'No component given.'
         ENDCASE
       ENDELSE


       ; Get a reference to the profiles object.
       profile_dispatch = (self.dispatcher)->GET_PROFILES_OBJECT()


       ; Assume the profile numbers are:
       ; full:  -1
       ; half:  -2
       ; third: -3
       profile_number = -1*(component + 1)
       
       ; First we get the profile object for the given profile number.
       profile = profile_dispatch->GET_PROFILE(profile_number)
  



       ; ---------------------------------------------------------------
       ; I need to choose how the line profile locations are determined 
       ; based on where the profiles are taken from.  
       ; 
       ; If the profiles are taken directly for the Gaussian and scaled 
       ; models, then I should use the line centroid excluding stray 
       ; Gaussians. 
       ;
       ; If taken from the profiles model I shoud assume that the 
       ; profiles are centered at zero.
       ;
       
       model_profile = self.dispatcher->GET_MODEL('BST_INST_MODEL_PROFILE_MULTI')
       IF model_profile->HAS_PROFILE(profile_number)  THEN BEGIN
         use_scaled_location = 0
         use_zero_location = 1
       ENDIF ELSE BEGIN
         use_scaled_location = 1
         use_zero_location = 0
       ENDELSE

       CASE 1 OF
         use_scaled_location: BEGIN
           ; Use an explicitly set value for the center location.
           profile->SET_USE_CENTER

           ; By default use the centroid as the center location.
           profile->SET_CENTER, profile->CENTROID()


           ; If this profile includes a scaled profile, then use the
           ; location of the scaled profile as the center of the profile.
           model_scaled_multi = self.dispatcher->GET_MODEL('BST_INST_MODEL_SCALED_MULTI')
           num_scaled = model_scaled_multi->NUM_MODELS()

           FOR ii=0,num_scaled-1 DO BEGIN
             model = model_scaled_multi->GET_MODEL(ii)
             IF model->HAS_PROFILE(profile_number) THEN BEGIN
               param = model->GET_PARAM()
               profile->SET_CENTER, param.location
             ENDIF
           ENDFOR
         END
       
         
         use_zero_location: BEGIN
           ; Use an explicitly set value for the center location.
           profile->SET_USE_CENTER
           
           ; Set the center location to zero
           profile->SET_CENTER, 0D
         END

       ENDCASE
       

       RETURN, profile

     END ; FUNCTION BST_INST_MODEL_ADAS305::GET_BEAM_PROFILE

     

     ;+=================================================================
     ; PURPOSE:
     ;   Return the wavelengths and intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_STARK_LINES

       comp_label = ['full', 'half', 'third']

       IF self.options.full_component_only THEN BEGIN
         num_components = 1 
       ENDIF ELSE BEGIN
         num_components = self.num_components
       ENDELSE


       FOR ii = 0, num_components-1 DO BEGIN
         lines = BUILD_STRUCTURE(comp_label[ii], self->GET_STARK_LINES_CHANNEL(ii), lines)
       ENDFOR

       RETURN, lines

     END ; FUNCTION BST_INST_MODEL_ADAS305::GET_STARK_LINES


     
     ;+=================================================================
     ; PURPOSE:
     ;   Return the intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_STARK_LINES_CHANNEL, comp, _REF_EXTRA=extra
       COMPILE_OPT STRICTARR

       COMMON BST_CHORD_PARAM, chord_param

       lines = self->GET_STARK_LINES_WAVELENGTH(comp, _STRICT_EXTRA=extra)

       location = BST_DISP_GET_LOCATION(WAVELENGTH=lines.wavelength $
       ;location = MST_DISP_GET_LOCATION(WAVELENGTH=lines.wavelength $
                                        ,LAMBDA0=self.param.lambda0 $
                                        ,SPEC_PARAM=chord_param.detector $
                                        ,/FROM_EDGE)
       
       ; The value returned above assumes that the first pixel is
       ; pixel 0.  The CERVIEW spectra start the pixels from 1.
       location += 1
       
       lines = BUILD_STRUCTURE('channel', location, lines)

       RETURN, lines

     END ; FUNCTION BST_INST_MODEL_ADAS305::GET_STARK_LOCATION_COMPONENT



     ;+=================================================================
     ; PURPOSE:
     ;   Return the wavelengths and intensities of the stark lines.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_STARK_LINES_WAVELENGTH, comp $
                                                                  ,FULL=full $
                                                                  ,HALF=half $
                                                                  ,THIRD=third

       IF KEYWORD_SET(third) THEN comp=2
       IF KEYWORD_SET(half) THEN comp=1
       IF KEYWORD_SET(full) THEN comp=0

       ;input = {b:{x:self.param.bfield_magnitude, y:0D, z:0D} $
       ;         ,beam_energy:self.param.beam_energy/factor $
       ;         ,pi_sigma_ratio:self.param.pi_sigma_ratio $
       ;         ,intensity:self.param.intensity $
       ;        }
       
       input = self->GET_ADAS305_INPUT(comp)

       RETURN, ADAS305_MODEL(input)


     END ; FUNCTION BST_INST_MODEL_ADAS305::GET_STARK_LINES_WAVELENGTH



     ;+=================================================================
     ; PURPOSE:
     ;   Return the structure that needs to be given to the ADAS305
     ;   model.
     ;   
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_ADAS305_INPUT, comp
       COMMON BST_CHORD_PARAM, chord_param

       CASE comp OF
         0: BEGIN
           factor = 1D
         END
         1: BEGIN
           factor = 2D
         END
         2: BEGIN
           factor = 3D
         END
       ENDCASE
       
       ; Reload the chord parameters.
       ;   NOTE: The parameters are cached, so this is fast.
       BST_CHORD_PARAM, self.shot_param.shot $
                        ,self.shot_param.chord $
                        ,self.shot_param.beam
       

       ; Get the toroidal direction from the chord geometry.
       ;bfield_toroidal = chord_param.geometry.toroidal
       ;bfield_toroidal = MIR_NORMALIZE(bfield_toroidal, self.param.bfield_toroidal)

       ;bfield_vertical = chord_param.geometry.vertical
       ;bfield_vertical =  MIR_NORMALIZE(bfield_vertical, self.param.bfield_z)

       ;bfield_vector = bfield_toroidal + bfield_vertical


       bfield_toroidal = chord_param.geometry.toroidal
       bfield_toroidal = MIR_NORMALIZE(bfield_toroidal)

       bfield_vertical = chord_param.geometry.vertical
       bfield_vertical =  MIR_NORMALIZE(bfield_vertical, self.param.bfield_z)

       bfield_vector = bfield_toroidal + bfield_vertical
       bfield_vector = MIR_NORMALIZE(bfield_vector, self.param.bfield_toroidal)

       beam_vector = MIR_NORMALIZE(chord_param.geometry.beam, self.param.beam_energy/factor)
       input = { $
                 bfield:{vector:bfield_vector} $
                 ,beam:{vector:beam_vector $
                        ,mass:2.0D} $
                 ,efield:{vector:[0D, 0D, 0D]} $
                 ,view:{vector:chord_param.geometry.view $
                        ,pi_sigma_ratio:self.param.pi_sigma_ratio} $
                 ,intensity:self.param.intensity.(comp) $
               }

       RETURN, input


     END ; FUNCTION BST_INST_MODEL_ADAS305::GET_ADAS305_INPUT



     ;+=================================================================
     ; PURPOSE:
     ;   Print out the state of the model.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::PRINT

       ; Reload the chord parameters.
       ;   NOTE: The parameters are cached, so this is fast.
       ;BST_CHORD_PARAM, self.shot_param.shot $
       ;                 ,self.shot_param.chord $
       ;                 ,self.shot_param.beam


       ; Get the line locations and intensities.
       lines = self->GET_STARK_LINES()


       ; Get the debug flags
       debug = self.dispatcher->GET_OPTIONS_DEBUG()


       ; Print out the wavelengths of the lines.
       num_lines = N_ELEMENTS(lines.(0).intensity)
       PRINT, '', 'Wavelength', 'Location', 'Intensity', FORMAT='(a2, 2x, 3(a15, 2x, :))'
       FOR ii=0,num_lines-1 DO BEGIN

         sort = SORT(lines.(0).wavelength)
         PRINT, ii $
                ,lines.(0).wavelength[sort[ii]] $
                ,lines.(0).channel[sort[ii]] $
                ,lines.(0).intensity[sort[ii]] $
                ,FORMAT= '(a2, 2x, 3(f15.3, 2x, :))'

       ENDFOR

     END ;PRO BST_INST_MODEL_ADAS305::PRINT



     ;+=================================================================
     ; PURPOSE:
     ;   Set which model parameters are fixed.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::SET_FIXED, fixed
       
       STRUCT_ASSIGN, {fixed:fixed}, self, /NOZERO

       ; Here I need to check if full_component_only is set.
       ; If so freeze the half and third intensities.

     END ;PRO BST_INST_MODEL_ADAS305::SET_FIXED



     ;+=================================================================
     ; This routine does a partial reset of the object.
     ; It resets all the parameters and flags but leaves
     ; the fit_index & gui_id untouched.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::RESET $
                       ,FIXED=fixed $
                       ,LIMITS=limits $
                       ,LIMITED=limited $
                       ,PARAMS=params $
                       ,ALL=all


       ; This will also check the keywords, set them all to 1 or 0 
       ; and set ALL=1 if no other keywords are given.
       self->BST_INST_MODEL_PARAMETER::RESET $
         ,FIXED=fixed $
         ,LIMITS=limits $
         ,LIMITED=limited $
         ,PARAMS=params $
         ,ALL=all



       IF all THEN BEGIN
         self.shot_param = { BST_INST_MODEL_ADAS305__SHOT_PARAM }
         self.options.full_component_only = 0
       ENDIF

       IF params OR all THEN BEGIN
         self.param = { BST_INST_MODEL_ADAS305__PARAM }
         self.param.pi_sigma_ratio = 1D
       ENDIF
       
       IF fixed OR all THEN BEGIN
         self.fixed = { BST_INST_MODEL_ADAS305__FIXED }
         self.fixed.temperature = 1
       ENDIF

       IF limited OR all THEN BEGIN
         self.limited = { BST_INST_MODEL_ADAS305__LIMITED }
         ; Temperature should always be positive.
         self.limited.temperature = [1,0]

         ; The ratio should always be positive.
         self.limited.pi_sigma_ratio = [1,0]

         ; The intensity should always be positive.
         FOR ii=0,N_TAGS(self.param.intensity)-1 DO BEGIN
           self.limited.intensity.(ii) = [1,0]
         ENDFOR
         
       ENDIF
       
       IF limits OR all THEN BEGIN
         self.limits = { BST_INST_MODEL_ADAS305__LIMITS }
       ENDIF


     END ;PRO BST_INST_MODEL_ADAS305::RESET



     ;+=================================================================
     ; This routine does a full reset of the object.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::RESET_ALL

       self.parameter_index = { BST_INST_MODEL_ADAS305__PARAMETER_INDEX }

       ; This will also call self->RESET
       self->BST_INST_MODEL_PARAMETER::RESET_ALL

     END ;PRO BST_INST_MODEL_ADAS305::RESET_ALL



     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the shot parameters 
     ;   of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_SHOT_PARAM
       
       RETURN, self.shot_param

     END ;PRO BST_INST_MODEL_ADAS305::GET_SHOT_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model shot parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::SET_SHOT_PARAM, shot_param
       
       STRUCT_ASSIGN, {shot_param:shot_param}, self, /NOZERO

     END ;PRO BST_INST_MODEL_ADAS305::SET_SHOT_PARAM




     ;+=================================================================
     ; PURPOSE:
     ;   Here we setup the method to get the shot parameters 
     ;   of the model.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_OPTIONS
       
       RETURN, self.options

     END ;PRO BST_INST_MODEL_ADAS305::GET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the model shot parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::SET_OPTIONS, options
       
       STRUCT_ASSIGN, {options:options}, self, /NOZERO

     END ;PRO BST_INST_MODEL_ADAS305::SET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a structure with the state of the model.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_STATE

       state = self->BST_INST_MODEL_PARAMETER::GET_STATE()
       state = CREATE_STRUCT(state $
                             ,{shot_param:self->GET_SHOT_PARAM()} $
                             ,{options:self->GET_OPTIONS()} $
                            )

       RETURN, state
     END ;FUNCTION BST_INST_MODEL_ADAS305::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load the model state from a save structure.
     ; 
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::LOAD_STATE, state

       self->BST_INST_MODEL_PARAMETER::LOAD_STATE, state
       self->SET_SHOT_PARAM, state.shot_param

       IF HAS_TAG(state, 'OPTIONS') THEN BEGIN
         self->SET_OPTIONS, state.options
       ENDIF

     END ;PRO BST_INST_MODEL_ADAS305::LOAD_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of parameter info structures.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_PARAMETER_INFO

       num_param = self->NUM_PARAM()
       parameter_info = REPLICATE({BST_INST_PARAMETER_INFO}, num_param)

       index = self.parameter_index.pi_sigma_ratio
       parameter_info[index].value = self.param.pi_sigma_ratio
       parameter_info[index].fixed = self.fixed.pi_sigma_ratio
       parameter_info[index].limited = self.limited.pi_sigma_ratio
       parameter_info[index].limits = self.limits.pi_sigma_ratio
       parameter_info[index].step = 1e-6

       index = self.parameter_index.beam_energy
       parameter_info[index].value = self.param.beam_energy
       parameter_info[index].fixed = self.fixed.beam_energy
       parameter_info[index].limited = self.limited.beam_energy
       parameter_info[index].limits = self.limits.beam_energy
       parameter_info[index].step = 1e-5

       index = self.parameter_index.bfield_toroidal
       parameter_info[index].value = self.param.bfield_toroidal
       parameter_info[index].fixed = self.fixed.bfield_toroidal
       parameter_info[index].limited = self.limited.bfield_toroidal
       parameter_info[index].limits = self.limits.bfield_toroidal
       parameter_info[index].step = 1e-6

       index = self.parameter_index.bfield_z
       parameter_info[index].value = self.param.bfield_z
       parameter_info[index].fixed = self.fixed.bfield_z
       parameter_info[index].limited = self.limited.bfield_z
       parameter_info[index].limits = self.limits.bfield_z
       parameter_info[index].step = 1e-4

       index = self.parameter_index.temperature
       parameter_info[index].value = self.param.temperature
       parameter_info[index].fixed = self.fixed.temperature
       parameter_info[index].limited = self.limited.temperature
       parameter_info[index].limits = self.limits.temperature

       index = self.parameter_index.lambda0
       parameter_info[index].value = self.param.lambda0
       parameter_info[index].fixed = self.fixed.lambda0
       parameter_info[index].limited = self.limited.lambda0
       parameter_info[index].limits = self.limits.lambda0

       FOR ii=0,N_TAGS(self.param.intensity)-1 DO BEGIN
         index = self.parameter_index.intensity.(ii)
         parameter_info[index].value = self.param.intensity.(ii)
         parameter_info[index].fixed = self.fixed.intensity.(ii)
         parameter_info[index].limited = self.limited.intensity.(ii)
         parameter_info[index].limits = self.limits.intensity.(ii)
       ENDFOR


       self->SET_PARAMETER_INFO_FLAGS, parameter_info

       RETURN, parameter_info

     END ;PRO BST_INST_MODEL_ADAS305::GET_PARAMETER_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Return an array with the model parameters.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_ADAS305::GET_PARAMETER_ARRAY


       parameter_array = DBLARR(self->NUM_PARAM(), /NOZERO)


       index = self.parameter_index.pi_sigma_ratio
       parameter_array[index] = self.param.pi_sigma_ratio

       index = self.parameter_index.beam_energy
       parameter_array[index] = self.param.beam_energy

       index = self.parameter_index.bfield_toroidal
       parameter_array[index] = self.param.bfield_toroidal

       index = self.parameter_index.bfield_z
       parameter_array[index] = self.param.bfield_z

       index = self.parameter_index.temperature
       parameter_array[index] = self.param.temperature

       index = self.parameter_index.lambda0
       parameter_array[index] = self.param.lambda0

       FOR ii=0,N_TAGS(self.param.intensity)-1 DO BEGIN
         index = self.parameter_index.intensity.(ii)
         parameter_array[index] = self.param.intensity.(ii)
       ENDFOR

       RETURN, parameter_array


     END ;BST_INST_MODEL_ADAS305::GET_PARAMETER_ARRAY



     ;+=================================================================
     ; Here we set up the method to extract from the fit_array
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::EXTRACT_PARAMETER_ARRAY, parameter_array

       index = self.parameter_index.pi_sigma_ratio
       self.param.pi_sigma_ratio = parameter_array[index]

       index = self.parameter_index.beam_energy
       self.param.beam_energy = parameter_array[index] 

       index = self.parameter_index.bfield_toroidal
       self.param.bfield_toroidal = parameter_array[index] 

       index = self.parameter_index.bfield_z
       self.param.bfield_z = parameter_array[index] 

       index = self.parameter_index.temperature
       self.param.temperature = parameter_array[index] 

       index = self.parameter_index.lambda0
       self.param.lambda0 = parameter_array[index] 

       FOR ii=0,N_TAGS(self.param.intensity)-1 DO BEGIN
         index = self.parameter_index.intensity.(ii)
         self.param.intensity.(ii) = parameter_array[index] 
       ENDFOR

     END ;PRO BST_INST_MODEL_ADAS305::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   To simplify conversions between a parameter array and
     ;   the model parametrs we save the locations of the parameters
     ;   in the index array.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305::INIT_PARAMETER_INDEX

       
       self.parameter_index = FILL_INDEX_STRUCTURE(self.parameter_index, /NO_COPY)


     END ;PRO BST_INST_MODEL_ADAS305::INIT_PARAMETER_INDEX



     ;+=================================================================
     ; Here we set up the structure that will hold a bstark parameter.
     ;
     ; Note that this must also hold some of the gui parameters.
     ;-=================================================================
     PRO BST_INST_MODEL_ADAS305__DEFINE

       num_components = 3

       shot_param = { BST_INST_MODEL_ADAS305__SHOT_PARAM $
                      ,shot:0L $
                      ,chord:'' $
                      ,beam:'' $
                    }

       param = { BST_INST_MODEL_ADAS305__PARAM $
                 ,pi_sigma_ratio:0D $
                 ,beam_energy:0D $
                 ,bfield_toroidal:0D $
                 ,bfield_z:0D $
                 ,temperature:0D $
                 ,lambda0:0D $
                 ,intensity:{BST_INST_MODEL_ADAS305__PARAM__INTENSITY $
                             ,full:0D $
                             ,half:0D $
                             ,third:0D $
                            } $
               } 

       sigma = { BST_INST_MODEL_ADAS305__SIGMA $
                 ,pi_sigma_ratio:0D $
                 ,beam_energy:0D $
                 ,bfield_toroidal:0D $
                 ,bfield_z:0D $
                 ,temperature:0D $
                 ,lambda0:0D $
                 ,intensity:{BST_INST_MODEL_ADAS305__SIGMA__INTENSITY $
                             ,full:0D $
                             ,half:0D $
                             ,third:0D $
                            } $
               } 

       fixed = { BST_INST_MODEL_ADAS305__FIXED $
                 ,pi_sigma_ratio:0 $
                 ,beam_energy:0 $
                 ,bfield_toroidal:0 $
                 ,bfield_z:0 $
                 ,temperature:0 $
                 ,lambda0:0 $
                 ,intensity:{BST_INST_MODEL_ADAS305__FIXED__INTENSITY $
                             ,full:0D $
                             ,half:0D $
                             ,third:0D $
                            } $
               } 
       
       limited = { BST_INST_MODEL_ADAS305__LIMITED $
                   ,pi_sigma_ratio:[0,0] $
                   ,beam_energy:[0,0] $
                   ,bfield_toroidal:[0,0] $
                   ,bfield_z:[0,0] $
                   ,temperature:[0,0] $
                   ,lambda0:[0,0] $
                   ,intensity:{BST_INST_MODEL_ADAS305__LIMITED__INTENSITY $
                               ,full:[0,0] $
                               ,half:[0,0] $
                               ,third:[0,0] $
                              } $
                 }

       limits = { BST_INST_MODEL_ADAS305__LIMITS $
                  ,pi_sigma_ratio:[0D,0D] $
                  ,beam_energy:[0D,0D] $
                  ,bfield_toroidal:[0D,0D] $
                  ,bfield_z:[0D,0D] $
                  ,temperature:[0D,0D] $
                  ,lambda0:[0D,0D] $
                  ,intensity:{BST_INST_MODEL_ADAS305__LIMITS__INTENSITY $
                              ,full:[0D,0D] $
                              ,half:[0D,0D] $
                              ,third:[0D,0D] $
                             } $
                } 
       
       parameter_index = { BST_INST_MODEL_ADAS305__PARAMETER_INDEX $
                           ,pi_sigma_ratio:0 $
                           ,beam_energy:0 $
                           ,bfield_toroidal:0 $
                           ,bfield_z:0 $
                           ,temperature:0 $
                           ,lambda0:0 $
                           ,intensity:{BST_INST_MODEL_ADAS305__PARAMETER_INDEX__INTENSITY $
                                       ,full:0D $
                                       ,half:0D $
                                       ,third:0D $
                                      } $
                         } 





       struct ={ BST_INST_MODEL_ADAS305 $

                 ,options:{ BST_INST_MODEL_ADAS305__OPTIONS $
                            ,full_component_only:0 $
                          } $

                 ,num_components:0 $

                 ,shot_param:{ BST_INST_MODEL_ADAS305__SHOT_PARAM} $

                 ,param:{ BST_INST_MODEL_ADAS305__PARAM} $

                 ,sigma:{ BST_INST_MODEL_ADAS305__SIGMA} $

                 ,fixed:{ BST_INST_MODEL_ADAS305__FIXED } $
                 
                 ,limited:{ BST_INST_MODEL_ADAS305__LIMITED } $

                 ,limits:{ BST_INST_MODEL_ADAS305__LIMITS } $

                 ,parameter_index:{ BST_INST_MODEL_ADAS305__PARAMETER_INDEX } $
                 
                 ,INHERITS BST_INST_MODEL_PARAMETER $
               }


     END ;PRO BST_INST_MODEL_ADAS305
