;**************************************************
;* ADAS Feature Framework Example
;* Created: Fri Apr 24 16:56:04 2009
;**************************************************
;* The following provides an example of how to use
;* the underlying ADAS feature framework to
;* generate the feature for use in 3rd party codes
;*
;* The object's class type and available methods are
;* outlined below.
;*
;* OBJECT CLASS: AFG_STARK
;*   FUNCTION METHODS:
;*     AFG_STARK::DOCALC
;*     AFG_STARK::INIT
;*     AFG_STARK::INITPARS
;*     AFG_STARK::SETDESC
;*   PROCEDURE METHODS:
;*     AFG_STARK::CLEANUP
;*
;* The object's  superclass and available methods are
;* outlined below.
;*
;* SUPERCLASS:  AFG_API
;*   FUNCTION METHODS:
;*     AFG_API::BEGINWAIT
;*     AFG_API::CALC
;*     AFG_API::DOCALC
;*     AFG_API::ENDWAIT
;*     AFG_API::GETCALC
;*     AFG_API::GETDESC
;*     AFG_API::GETINTENSITY
;*     AFG_API::GETPARS
;*     AFG_API::GETRES
;*     AFG_API::GETWV
;*     AFG_API::GETWVRESOLVED
;*     AFG_API::INIT
;*     AFG_API::SETDELAYHANDLER
;*     AFG_API::SETDESC
;*     AFG_API::SETPARS
;*     AFG_API::SETWVRESOLVED
;*     AFG_API::WRITECODE
;*   PROCEDURE METHODS:
;*     AFG_API::CLEANUP
;**************************************************

FUNCTION adas305_model, input

  ; The input function sould be of the form
  ; {  b:{x, y, z} $ ; (Tesla)
  ;   ,beam_energy $ ; (KeV)
  ;   ,pi_sigma_ratio $
  ;   ,intensity $
  ; }
  b = SQRT(input.b.x^2 + input.b.y^2 + input.b.z^2)

  ;create the object:
  o = OBJ_NEW('AFG_STARK')

  ;obtain the feature parameters using getPars method
  ;which will return the parameter structure:
  pars = o->getPars()

  ;modify each of the parameter values:
  pars.beam_mass=1.00000
  pars.beam_energy=input.beam_energy

  ; Only needed if we turn brodening on.
  pars.beam_te=3.50000

  ;Beam density does not matter unless we care about absolute intensity.
  pars.beam_density=1.0

  pars.plasma_mass=2.00000
  pars.plasma_te=300.000
  pars.plasma_density=1.00007e+13
  pars.plasma_zeff=4.00000

  pars.beam_dc_x=0.00000
  pars.beam_dc_y=1.00000
  pars.beam_dc_z=0.00000

  pars.bfield_value=b
  pars.bfield_dc_x=1.00000
  pars.bfield_dc_y=0.00000
  pars.bfield_dc_z=0.00000

  pars.efield_value=0.00000
  pars.efield_dc_x=1.00000
  pars.efield_dc_y=0.00000
  pars.efield_dc_z=0.00000

  ; Viewing geometry for MST central chord.
  pars.obs_dc_x=0.00000
  pars.obs_dc_y=-0.923900
  pars.obs_dc_z=-0.382700

  pars.obs_pi=input.pi_sigma_ratio
  pars.obs_sigma=1.00000


  pars.broaden=0

;alternatively you can set the parameters by defining a structure like this:
;  pars = {ADAS_FEATURE_AFG_STARK, $
;    BEAM_MASS: 1.00000, $
;    BEAM_ENERGY: 46.0000, $
;    BEAM_TE: 3.50000, $
;    BEAM_DENSITY: 4.09261e+09, $
;    PLASMA_MASS: 2.00000, $
;    PLASMA_TE: 300.000, $
;    PLASMA_DENSITY: 1.00007e+13, $
;    PLASMA_ZEFF: 4.00000, $
;    BEAM_DC_X: 0.00000, $
;    BEAM_DC_Y: 1.00000, $
;    BEAM_DC_Z: 0.00000, $
;    BFIELD_VALUE: 0.250000, $
;    BFIELD_DC_X: 1.00000, $
;    BFIELD_DC_Y: 0.00000, $
;    BFIELD_DC_Z: 0.00000, $
;    EFIELD_VALUE: 0.00000, $
;    EFIELD_DC_X: 1.00000, $
;    EFIELD_DC_Y: 0.00000, $
;    EFIELD_DC_Z: 0.00000, $
;    OBS_DC_X: 0.00000, $
;    OBS_DC_Y: -0.923900, $
;    OBS_DC_Z: -0.382700, $
;    OBS_SIGMA: 1.00000, $
;    OBS_PI: 0.00000, $
;    BROADEN: 1 $
;  }

  ;now set these values to be used by the feature object:
  pars = o->AFG_API::SETPARS(PARS=pars)

  ;perform calculation using these parameters:
  result = o->doCalc()

  ;obtain the wavelength and intensity arrays:
  wavelength = o->getWv()
  intensity = o->afg_api::getIntensity()

  ; Normalize the intensities.
  intensity = intensity/TOTAL(intensity) * input.intensity

  ; Suppress underflow errors.
  math_error = CHECK_MATH(32)

  RETURN, {wavelength:wavelength, intensity:intensity}
END

