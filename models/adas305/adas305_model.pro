


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-01
;
; PURPOSE:
;   Calculate the intensity and wavelength of the lines for the stark
;   split D-alpha spectrum using the ADAS305 model.
;
;   The inputs and outputs are taylored to be used in a model
;   for [BST_INSTRUMENT_FIT].
;
; DESCRIPTION
;   The ADAS305 code currently does a poor job of identifing which
;   are the sigma or pi lines.  I have written a simple algorithm
;   for identifying the lines based on their locations.  For the time
;   being I am using this algorithm instead on relying on the ADAS305
;   identification.
;
;-======================================================================




     FUNCTION ADAS305_MODEL, input

       ; The input function sould be of the form.
       ; The vectors can be given in any arbitrary coordinate system.
       ;
       ; {  $
       ;    bfield:{vector} $ ; (Tesla)
       ;   ,beam:{vector, mass} $ ; (KeV)
       ;   ,efield:{vector} $ ; (Volt)
       ;   ,view:{vector, pi_sigma_ratio} $ ; (arbitrary)
       ;   ,intensity $
       ; }



       ; ---------------------------------------------------------------
       ; Build the input structures for ADAS305

       beam_direction = MIR_NORMALIZE(input.beam.vector)
       beam = { $
                mass:input.beam.mass $
                ,energy:MIR_MAGNITUDE(input.beam.vector)*1e3 $
                ,dc_x:beam_direction[0] $
                ,dc_y:beam_direction[1] $
                ,dc_z:beam_direction[2] $
                ; Only needed if we turn brodening on.
                ,te:3.5D $
                ;Beam density does not matter unless we care about the absolute intensity.
                ,density:1.0D $
              }

       plasma = { $
                  mass:2.0D $
                  ,te:300.0D $
                  ,density:1.0D13 $
                  ,zeff:4.0D $
                }

       bfield_direction = MIR_NORMALIZE(input.bfield.vector)
       bfield = { $
                  value:MIR_MAGNITUDE(input.bfield.vector) $
                  ,dc_x:bfield_direction[0] $
                  ,dc_y:bfield_direction[1] $
                  ,dc_z:bfield_direction[2] $
                }
 

       efield_value = MIR_MAGNITUDE(input.efield.vector)
       IF efield_value NE 0 THEN BEGIN
         efield_direction = MIR_NORMALIZE(input.efield.vector)
       ENDIF ELSE BEGIN
         efield_direction = [1D, 0D, 0D]
       ENDELSE

       efield = { $
                  value:efield_value $
                  ,dc_x:efield_direction[0] $
                  ,dc_y:efield_direction[1] $
                  ,dc_z:efield_direction[2] $
                }
                  
       view_direction = MIR_NORMALIZE(input.view.vector)
       obs = { $
               dc_x:view_direction[0] $
               ,dc_y:view_direction[1] $
               ,dc_z:view_direction[2] $
               ,pi:1.0D $
               ,sigma:1.0D $
             }



       ; ---------------------------------------------------------------
       ; Run ADAS305 and post process.

       ; Run ADAS305
       adas305_get_stark,  BEAM = beam $  
                           ,PLASMA = plasma $
                           ,BFIELD = bfield $
                           ,EFIELD = efield $
                           ,OBS = obs $  
                           ,N_LOWER = 2 $
                           ,N_UPPER = 3 $
                           ,WAVE_COMP = wavelength $
                           ,EMISS_COMP = intensity

       ; Identify the lines.
       ADAS305_STARK_IDENTIFY, BEAM = beam $  
                               ,BFIELD = bfield $
                               ,EFIELD = efield $
                               ,OBS = obs $
                               ,WAVE_COMP = wavelength $
                               ,EMISS_COMP = intensity $
                               ,SIGMA = index_sigma $
                               ,PI = index_pi $
                               ,/WARN


       ; Apply the pi/sigma factor
       intensity[index_pi] = intensity[index_pi] * input.view.pi_sigma_ratio



       ; Normalize the intensities.
       intensity = intensity/TOTAL(intensity) * input.intensity

       ; Suppress underflow errors.
       math_error = CHECK_MATH(32)

       RETURN, {wavelength:wavelength, intensity:intensity}
     END;   FUNCTION ADAS305_MODEL

