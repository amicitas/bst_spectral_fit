

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Define a structure to hold information on the free fit parameters
;   for <BST_SPECTRAL_FIT>.
;
; DESCRIPTION:
;   This structure is used in bulding an array that converts from
;   the parameter in the modles to an array of parameters that
;   can be input in to the fitting routines [MPCURVEFIT]
;   (or [CURVEFIT_SWITCH])
;
;   The parameter defined here are the same as those defined in
;   the PARINFO sturcture used in [MPCURVEFIT].  However we only use
;   the parameters actually needed for <BST_SPECTRAL_FIT>.
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Define the BST_INST_PARAMETER_INFO structure.
     ;
     ;-=================================================================
     PRO BST_INST_PARAMETER_INFO__DEFINE

       bst_inst_parameter_info = $
          { BST_INST_PARAMETER_INFO $
            ,value:0D $
            ,fixed:0 $
            ,limited:[0,0] $
            ,limits:[0D,0D] $
            ,step:0D $
          }

     END ;PRO BST_INST_MODEL_DISPATCH__DEFINE
