

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Define a base class for the addon and model dispatch objects.
;
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::INIT, core, app, gui, _REF_EXTRA=extra

       status = self->BST_INST_BASE::INIT(core, app, gui, _STRICT_EXTRA=extra)

       self.objects = OBJ_NEW('MIR_DICTIONARY')

       RETURN, status

     END ;FUNCTION BST_INST_DISPATCH::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Return the loaded Object list.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::GET_OBJECTS
       
       RETURN, self.objects

     END ;FUNCTION BST_INST_DISPATCH::GET_OBJECTS


     
     ;+=================================================================
     ; PURPOSE:
     ;   Find all of the objects in a given directory with the given
     ;   properties, instantiate and intialize them, then return
     ;   a dictionary with the object references.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::LOAD_OBJECTS, directory_array $
                                         ,CONFIG_SECTION=config_section $
                                         ,SUPERCLASS=superclass $
                                         ,TEST_METHODS=test_method_array

       ; First we destroy all existing objects.
       self->REMOVE_OBJECTS

       ; Get a list of all of the objects in the given directories.
       object_name_list = self->FIND_OBJECTS(directory_array)

       ; Fill the objects dictionary with instances of all the objects.
       self->INSTANTIATE_OBJECTS, object_name_list $
                                  ,CONFIG_SECTION=config_section $
                                  ,SUPERCLASS=superclass $
                                  ,TEST_METHODS=test_method_array

       OBJ_DESTROY, object_name_list



       ; Print out some debuging information.
       self.core->MESSAGE, 'Loaded '+self.object_type+'s:', /DEBUG
       IF self.objects->NUM_ENTRIES() GT 0 THEN BEGIN
         self.core->MESSAGE, '  ' + self.objects->GET_KEYS(), /DEBUG
       ENDIF

     END ;FUNCTION BST_INST_DISPATCH::LOAD_OBJECTS



     ;+=================================================================
     ; PURPOSE:
     ;   Search the object directory and find any available objects.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::FIND_OBJECTS, directory_array
       
       object_name_list = OBJ_NEW('simplelist')


       pattern = '*__define.pro'

       ; Generate the patten to use for the search.
       file_list = FILE_SEARCH(directory_array, pattern, /FOLD_CASE)

       IF file_list[0] NE '' THEN BEGIN
         object_names = STRUPCASE(FILE_BASENAME(file_list, '__define.pro', /FOLD_CASE))
         object_name_list->JOIN, object_names
       ENDIF

       
       RETURN, object_name_list

     END ;FUNCTION BST_INST_DISPATCH::FIND_OBJECTS



     ;+=================================================================
     ; PURPOSE:
     ;   Go through the files found by [::FIND_OBJECT] and, if 
     ;   appropriate, instantiate the object.
     ;
     ; PROGRAMMING NOTES:
     ;   The input to this method is all of the objects found within
     ;   the directory for the component type (addon or model). I then
     ;   determine if the objects are of the correct type, that is
     ;   derived from the appropriate base class. To determine the
     ;   object type I use the function <OBJ_IS_DERIVED()>.  This
     ;   routine will attempt to resolve all of the superclasses
     ;   of the given object untill a match with the desired type is
     ;   found.  If one of the superclasses cannot be resolved, this
     ;   routine will be very slow to respond with an error as
     ;   IDL will search the entire IDL path in an attempt to
     ;   find an appropriate file.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::INSTANTIATE_OBJECTS, object_name_list $
                                                 ,CONFIG_SECTION=config_section $
                                                 ,SUPERCLASS=superclass $
                                                 ,TEST_METHODS=test_method_array

       ; Extract the appropriate section from the configuration settings.
       config = self.core->GET_CONFIG()
       IF ISA(config_section) THEN BEGIN
         IF config.HASKEY(config_section) THEN BEGIN
           config_object = config[config_section]
         ENDIF
       ENDIF


       ; Loop though all of objects found in the objects directories.
       FOR ii=0,object_name_list->N_ELEMENTS()-1 DO BEGIN 

         object_name = object_name_list->GET(ii)
         object = OBJ_NEW()


         ; Establish catch block.
         CATCH, error                 
         IF (error NE 0) THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not instantiate object: ' + object_name, /ERROR, /LOG
           CONTINUE
         ENDIF

         use_object = 1

         ; Choose whether objects are loaded by default, or if the must be explicitly specified.
         ; in the configuration. For this we look in the [GENERAL] section for an entry like 
         ; MODELS_DEFAULT_ACTIVE=1
         IF ISA(config) THEN BEGIN
           IF config.HASKEY('GENERAL') THEN BEGIN
             config_general = config['GENERAL']
             IF config_general.HASKEY(config_section+'_DEFAULT_ACTIVE') THEN BEGIN
               use_object = config_general[config_section+'_DEFAULT_ACTIVE']
             ENDIF
           ENDIF
         ENDIF

         ; Check if this object name is in the config file.
         IF ISA(config_object) THEN BEGIN
           IF config_object.HASKEY(object_name) THEN BEGIN
             use_object = config_object[object_name]
           ENDIF
         ENDIF

         ; Check if this object is derived from the given superclass.
         IF use_object AND ISA(superclass) THEN BEGIN
           ; Check to see if this object is derived from the given superclass
           IF ~ OBJ_IS_DERIVED(object_name, superclass, /RESOLVE) THEN BEGIN
             use_object = 0
           ENDIF
         ENDIF

         IF use_object AND ISA(test_method_array) THEN BEGIN
           ; If the names of any test methods were given, then perform
           ; all the given tests.  If any fail we do not load the object.

           ; First instantiate the object.
           object = OBJ_NEW(object_name)
           
           ; Check the standard flags.
           IF object.internal OR object.default THEN BEGIN
             use_object = 1
           ENDIF

           FOR jj=0,N_ELEMENTS(test_method_array)-1 DO BEGIN
             IF ~ CALL_METHOD(test_method_array[jj], object) THEN BEGIN
               use_object = 0
             ENDIF
           ENDFOR
         ENDIF

         self.core->MESSAGE, STRING(use_object, object_name, FORMAT='(i1,2x,a0)'), /DEBUG

         IF use_object THEN BEGIN
           ; Save a reference to the dispatcher object.
           object->SET_DISPATCHER, self

           ; Save the object reference.
           self.objects->SET, object_name, object
         ENDIF ELSE BEGIN
           ; If the above conditions were not met then skip this object.
           OBJ_DESTROY, object
         ENDELSE


; DEBUG ----------------------------------------------------------------
HEAP_GC, /VERBOSE
; DEBUG ----------------------------------------------------------------

       ENDFOR


     END ; PRO BST_INST_DISPATCH::INSTANTIATE_OBJECTS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize all of the objects.
     ;
     ; DESCRIPTION
     ;   This will loop through all of the objects, and call their
     ;   INITIALIZE methods.
     ; 
     ;   This will be called after all of the objects have been
     ;   instantiated.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::INITIALIZE_OBJECTS

       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii, key)
         
         ; Establish catch block.
         CATCH, error                 
         IF (error NE 0) THEN BEGIN 
           self.core->MESSAGE, 'Could not initialize object: ' + key, /ERROR, /LOG
           CATCH, /CANCEL
           CONTINUE
         ENDIF

         object->INITIALIZE
       ENDFOR

     END ;PRO BST_INST_DISPATCH::INITIALIZE_OBJECTS


     ;+=================================================================
     ; PURPOSE:
     ;   Clear all cached values. Also applies to MODELS and ADDONS.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::CLEAR_CACHE, _REF_EXTRA=extra


       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not clear cache for object: '+OBJ_CLASS(object), /ERROR, /LOG
           CONTINUE
         ENDIF

         ; This should apply to all objects, even those that are inactive.
         object->CLEAR_CACHE, _STRICT_EXTRA=extra

       ENDFOR

     END ; PRO BST_INST_DISPATCH::CLEAR_CACHE

     
     ;+=================================================================
     ; PURPOSE:
     ;   This routines allows us to reset all of the objects.
     ;   It will loop though all of the objects and call their
     ;   RESET methods.
     ;
     ;   This method can also be used to reset specific portions
     ;   of the methods by using keywords.
     ;
     ; DESCRIPTION:
     ;   NOTE: This routines does a partial reset: it resets all of
     ;         the parameters but leavse the core object reference
     ;         inplace.
     ;-=================================================================
     PRO BST_INST_DISPATCH::RESET, _REF_EXTRA=extra
 

       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii, key)
          

         ; Establish catch block.
         CATCH, error                 
         IF (error NE 0) THEN BEGIN 
           self.core->MESSAGE, 'Could not reset state for ' $
                                 + self.object_type + $
                                 ': ' + key, /ERROR, /LOG
           CATCH, /CANCEL
           CONTINUE
         ENDIF


         object->RESET, _STRICT_EXTRA=extra
       ENDFOR

     END ;PRO BST_INST_DISPATCH::RESET



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the number of objects stored in the dispatcher.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::NUM_OBJECTS

       RETURN, self.objects->NUM_ENTRIES()

     END ;FUNCTION BST_INST_DISPATCH::NUM_OBJECTS


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a object object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::HAS_OBJECT, name_in, PREFIX=prefix


       name = STRUPCASE(name_in)


       IF self.objects->HAS_KEY(name, data) THEN BEGIN
         RETURN, 1
       ENDIF 

       IF ISA(prefix) THEN BEGIN
         IF self.objects->HAS_KEY(prefix+name, data) THEN BEGIN
           RETURN, 1
         ENDIF
       ENDIF 
       
       RETURN, 0

     END ;FUNCTION BST_INST_DISPATCH::HAS_OBJECT

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a object object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::GET_OBJECT, name_in, PREFIX=prefix


       name = STRUPCASE(name_in)


       IF self.objects->HAS_KEY(name, data) THEN BEGIN
         RETURN, data
       ENDIF 

       IF ISA(prefix) THEN BEGIN
         IF self.objects->HAS_KEY(prefix+name, data) THEN BEGIN
           RETURN, data
         ENDIF
       ENDIF 
       
       ; We don't want to get here.  
       MESSAGE, 'Could not find '+self.object_type+': '+name

     END ;FUNCTION BST_INST_DISPATCH::GET_OBJECT




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure containing the state of all of the objects.
     ;
     ;   This structure will be saved in the save files.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::GET_STATE



       state_dict = OBJ_NEW('MIR_DICTIONARY')

       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii, key)

         ; Establish catch block.
         CATCH, error                 
         IF (error NE 0) THEN BEGIN 
           self.core->MESSAGE, 'Could not save state for ' $
                                 + self.object_type + $
                                 ': ' + key, /ERROR, /LOG
           CATCH, /CANCEL
           CONTINUE
         ENDIF
          

         self.core->MESSAGE, 'Saving the state for the ' $
                               + key $
                               + ' ' + self.object_type + '.' $
                               ,/DEBUG, /LOG


         state_dict->SET, key, object->GET_STATE()
       ENDFOR

       state_struct = state_dict->TO_STRUCTURE()
       
       state_dict->DESTROY

       RETURN, state_struct

     END ;FUNCTION BST_INST_DISPATCH::GET_STATE




     ;+=================================================================
     ; PURPOSE:
     ;   Load the state of all of the objects.
     ;
     ; PRGRAMING NOTES:
     ;   When loading the object states we cannot count on the
     ;   order of the objects, or even their existance, being
     ;   the same as when the state was saved.
     ;
     ;   Therefor we do the loading by name.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::LOAD_STATE, state_struct


       ; The first thing to do is to reset all of the objects
       self->RESET

       state_dict = OBJ_NEW('MIR_DICTIONARY')
       state_dict->FROM_STRUCTURE, state_struct

       object_keys = self.objects->GET_KEYS()


       ; The actuall loading is done by looping through the 
       ; currently active objects and looking for matches in the
       ; the save structure. This will display a message if no
       ; state exists for a currently active object.
       ;
       ; I also want to print a warning if a state exists for
       ; a object that is not currently active.
       ; I will check for that case first.
       FOR ii = 0, state_dict->NUM_ENTRIES()-1 DO BEGIN
         key = state_dict->GET_KEY_BY_INDEX(ii)
         IF ~ self.objects->HAS_KEY(key) THEN BEGIN
           self.core->MESSAGE, $
              'The state given for the ' $
              + key $
              + ' object could not be loaded because the ' $
              + self.object_type $
              + ' is not currently active.' $
             ,/DEBUG, /LOG
         ENDIF
       ENDFOR
       

       ; Loop over all of the currently active objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii, key)

         ; Establish catch block.
         CATCH, error                 
         IF (error NE 0) THEN BEGIN 
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not load state for ' $
                                 + self.object_type $
                                 + ': ' + key, /ERROR, /LOG
           CONTINUE
         ENDIF

         
         IF state_dict->HAS_KEY(key, data) THEN BEGIN
           self.core->MESSAGE, 'Loading the save state for the ' $
                                 + key $
                                 + ' ' + self.object_type + '.' $
                                 ,/DEBUG, /LOG
           object->LOAD_STATE, data
         ENDIF ELSE BEGIN
           self.core->MESSAGE, $
              'No state given for the '+key+' '+self.object_type+'.' $
              ,/DEBUG, /LOG
         ENDELSE
       ENDFOR


       ; Destroy the state_dictionary.
       state_dict->DESTROY

     END ;FUNCTION BST_INST_DISPATCH::LOAD_STATE




     ;+=================================================================
     ; PURPOSE:
     ;   This routines allows us to print output from
     ;   all of the objects.
     ;   It will loop though all of the objects and call their
     ;   PRINT methods.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::PRINT
 

       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii, key)
          

         ; Establish catch block.
         CATCH, error                 
         IF (error NE 0) THEN BEGIN 
           self.core->MESSAGE, 'Could not reset state for ' $
                                 + self.object_type + $
                                 ': ' + key, /ERROR, /LOG
           CATCH, /CANCEL
           CONTINUE
         ENDIF

         IF object->USE() THEN BEGIN


           ; Print a separator.
           self.core->MESSAGE, ''
           self.core->MESSAGE, STRING('',FORMAT='(72("="),a0)')

           ; Print a header.
           self.core->MESSAGE, key

           object->PRINT
         ENDIF
       ENDFOR

     END ;PRO BST_INST_DISPATCH::PRINT



     ;+=================================================================
     ; PURPOSE:
     ;   Handle messages from the components.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::MESSAGE, message_array $
                                    ,SCOPE_LEVEL=scope_level_in $
                                    ,_REF_EXTRA=extra
       ; Update the scope.
       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1

       IF OBJ_VALID(self.core) THEN BEGIN
         self.core->MESSAGE, message_array $
                             ,SCOPE_LEVEL=scope_level $
                             ,_STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         MIR_LOGGER, message $
                     ,SCOPE_LEVEL=scope_level $
                     ,_EXTRA=extra
       ENDELSE

     END ;BST_INST_DISPATCH::MESSAGE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the debug options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::GET_OPTIONS_DEBUG

       RETURN, self.core->GET_OPTIONS_DEBUG()

     END ;FUNCTION BST_INST_FIT::GET_OPTIONS_DEBUG



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current path
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCH::GET_CURRENT_PATH

       RETURN, self.core->GET_CURRENT_PATH()

     END ; FUNCTION BST_INST_DISPATCH::GET_CURRENT_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the current path
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::SET_CURRENT_PATH, path

       self.core->SET_CURRENT_PATH, path

     END ; PRO BST_INST_DISPATCH::SET_CURRENT_PATH



     ;+=================================================================
     ; PURPOSE:
     ;   Destroy all objects and deallocate storage.
     ;-=================================================================
     PRO BST_INST_DISPATCH::REMOVE_OBJECTS


       ; Destroy all of the objects.
       IF OBJ_VALID(self.objects) THEN BEGIN
         FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
           object = self.objects->GET_BY_INDEX(ii)
           
           IF OBJ_VALID(object) THEN BEGIN
class = OBJ_CLASS(object)
             object->DESTROY
MIR_LOGGER, /DEBUG, class + ' destroyed.'
HEAP_GC, /VERBOSE
           ENDIF

         ENDFOR


         self.objects->REMOVE, /ALL
       ENDIF

     END ;PRO BST_INST_DISPATCH::REMOVE_OBJECTS



     ;+=================================================================
     ; PURPOSE:
     ;   Destroy all objects and deallocate storage.
     ;-=================================================================
     PRO BST_INST_DISPATCH::DEALLOCATE


       IF OBJ_VALID(self.objects) THEN BEGIN
         self->REMOVE_OBJECTS
         self.objects->DESTROY
       ENDIF


     END ;PRO BST_INST_DISPATCH::DEALLOCATE



     ;+=================================================================
     ; PURPOSE:
     ;   Cleanup before destroying the object.
     ;
     ; DESTRIPTIONS:
     ;   This will destroy all of the objects, and their initial states,
     ;   then destroy all internal objects.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::CLEANUP

       self->DEALLOCATE

     END ;PRO BST_INST_DISPATCH::CLEANUP




;=======================================================================
;#######################################################################
;#######################################################################
;
; Fit loop methods.
;
;#######################################################################
;#######################################################################
;=======================================================================

     
     ;+=================================================================
     ; PURPOSE:
     ;   CallS the higest level UPDATE method available.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::UPDATE_FITTER
  
       CASE 1 OF
         ISA(self.coregui): self.coregui.UPDATE
         ISA(self.coreapp): self.coreapp.UPDATE
       ENDCASE
       
     END ; PRO BST_INST_DISPATCH::UPDATE_FITTER

     
     ;+=================================================================
     ; PURPOSE:
     ;   Call the UPDATE method on all of the objects.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::UPDATE

       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)

         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not update object: '+OBJ_CLASS(object), /ERROR, /LOG
           CONTINUE
         ENDIF

         ; Don't update unless the object in enabled.
         IF object->IS_ENABLED() THEN BEGIN
           object->UPDATE
         ENDIF

       ENDFOR

     END ; PRO BST_INST_DISPATCH::UPDATE


     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called before the fit is started.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;   This method is the first thing when starting a fit. It is
     ;   called before any of the CORE is setup.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::BEFORE_FIT

       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not complete BEFORE_FIT for object: ' + OBJ_CLASS(object) $
                                 , /ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this object is enabled.
         IF object->IS_ENABLED() THEN BEGIN
           object->BEFORE_FIT
         ENDIF

       ENDFOR

     END ; PRO BST_INST_DISPATCH::BEFORE_FIT


     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called immediatly before the fit loop is 
     ;   started.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;   This method is called after all of the models have been
     ;   setup and immediatly before the fit loop is started.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::PREFIT


       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not complete PREFIT for object: ' + OBJ_CLASS(object) $
                                 , /ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this object is enabled.
         IF object->IS_ENABLED() THEN BEGIN
           object->PREFIT
         ENDIF

       ENDFOR


     END ;PRO BST_INST_DISPATCH::PREFIT


     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called immediatly after the fit loop is 
     ;   completed.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;
     ;   This method is called immediatly after the fit loop is 
     ;   complete.
     ;
     ;   Any analysis of the final fit results shoud happen here.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::POSTFIT



       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not complete POSTFIT for object: '+OBJ_CLASS(object) $
                                 , /ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this object is enabled.
         IF object->IS_ENABLED() THEN BEGIN
           object->POSTFIT
         ENDIF

       ENDFOR


     END ;PRO BST_INST_DISPATCH::POSTFIT


     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called after the fit procedure is complete.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;   This method is called after the fit procedure is complete 
     ;   and the models will have been restored to their initial
     ;   state except for parameters flagged with 'save'.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::AFTER_FIT


       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not complete AFTER_FIT for object: '+OBJ_CLASS(object) $
                                 , /ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this object is enabled.
         IF object->IS_ENABLED() THEN BEGIN
           object->AFTER_FIT
         ENDIF

       ENDFOR


     END ; PRO BST_INST_DISPATCH::AFTER_FIT


     ;+=================================================================
     ; PURPOSE:
     ;   Enable an object given its name.
     ;
     ;   In general if the requested object is not found an error
     ;   will be raised.  If the /QUIET keyword is set, then this
     ;  routine will quietly ignore the request.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::ENABLE_OBJECT, name_in

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self.core->MESSAGE, 'Could not enable object: '+name_in, /ERROR, /LOG
         RETURN
       ENDIF


       object = self->GET_OBJECT(name_in)

       object->ENABLE

     END ;PRO BST_INST_OBJECTS_ENABLE


     ;+=================================================================
     ; PURPOSE:
     ;   Disable an object given its name.
     ;
     ;   In general if the requested object is not found an error
     ;   will be raised.  If the /QUIET keyword is set, then this
     ;  routine will quietly ignore the request.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::DISABLE_OBJECT, name_in, QUIET=quiet

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         IF ~ KEYWORD_SET(quiet) THEN BEGIN
           self.core->MESSAGE, 'Could not disable object: '+name_in, /DEBUG
           MESSAGE, /REISSUE
         ENDIF
         RETURN
       ENDIF

       IF self.HAS_OBJECT(name_in) THEN BEGIN
         object = self->GET_OBJECT(name_in)

         object->DISABLE
       ENDIF

     END ;PRO BST_INST_OBJECTS_DISABLE


     ;+=================================================================
     ; PURPOSE:
     ;   Enable the default objects.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::ENABLE_DEFAULT


       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)

         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not enable object: '+OBJ_CLASS(object), /ERROR, /LOG
           CONTINUE
         ENDIF

         ; Check if this is a default or internal object.
         IF object->IS_DEFAULT() OR object->IS_INTERNAL() THEN BEGIN
           object->ENABLE
         ENDIF

       ENDFOR

     END ; PRO BST_INST_DISPATCH::ENABLE_DEFAULT


     ;+=================================================================
     ; PURPOSE:
     ;   Disable all non-internal objects.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH::DISABLE_ALL


       ; Loop over all of the objects.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         object = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not disable object: '+OBJ_CLASS(object), /ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this is a default or internal object.
         IF ~ object->IS_INTERNAL() THEN BEGIN
           object->DISABLE
         ENDIF

       ENDFOR

     END ; PRO BST_INST_DISPATCH::DISABLE_ALL

     
;=======================================================================
;#######################################################################
;#######################################################################
;
; Object Definition.
;
;#######################################################################
;#######################################################################
;=======================================================================

     
     ;+=================================================================
     ; PURPOSE:
     ;   Create the BST_INST_DISPATCH object.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCH__DEFINE

       bst_inst_dispatch = $
          { BST_INST_DISPATCH $

            ,objects:OBJ_NEW() $

            ,object_type:'' $

            ,INHERITS BST_INST_BASE $
          }
     END ;PRO BST_INST_DISPATCH__DEFINE
