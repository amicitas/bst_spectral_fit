


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Provides access to the internals of <BST_SPECTRAL_FIT>.
;   These are here mostly for debuging and scripting purposes.
;-======================================================================




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT ACCESS
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Return the fitter object.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GET_FITTER, fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter
       IF ~ OBJ_VALID(fitter) THEN MESSAGE, 'No valid fitter object given.'

       RETURN, fitter

     END ; FUNCITON BST_INST_GET_FITTER



     ;+=================================================================
     ; PURPOSE:
     ;   Return the reference for a model given its name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GET_MODEL, model_name, fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter
       IF ~ OBJ_VALID(fitter) THEN MESSAGE, 'No valid fitter object given.'

       fitter_core = fitter->GET_CORE()
       models = fitter_core->GET_MODEL_DISPATCHER()
       
       RETURN, models->GET_MODEL(model_name)

     END ; FUNCITON BST_INST_GET_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   Return the reference for a addon given its name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GET_ADDON, addon_name, fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter
       IF ~ OBJ_VALID(fitter) THEN MESSAGE, 'No valid fitter object given.'

       addons = fitter->GET_ADDON_DISPATCHER()
       
       RETURN, addons->GET_ADDON(addon_name)

     END ; FUNCITON BST_INST_GET_ADDON

    

     ;+=================================================================
     ; PURPOSE:
     ;   Return the reference for a addon given its name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GET_GUIADDON, addon_name, fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter
       IF ~ OBJ_VALID(fitter) THEN MESSAGE, 'No valid fitter object given.'

       addons = fitter->GET_GUIADDON_DISPATCHER()
       
       RETURN, addons->GET_GUIADDON(addon_name)

     END ; FUNCITON BST_INST_GET_GUIADDON


     
     ;+=================================================================
     ; PURPOSE:
     ;   Return the spectrum.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GET_SPECTRUM, fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter
       IF ~ OBJ_VALID(fitter) THEN MESSAGE, 'No valid fitter object given.'
       
       fitter_core = fitter->GET_CORE()
       RETURN, fitter_core->GET_SPECTRUM()

     END ; FUNCITON BST_INST_GET_ADDON



     ;+=================================================================
     ; PURPOSE:
     ;   Recover from an error when error handling fails.
     ;
     ;-=================================================================
     PRO BST_INST_POSTERROR, fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter
       IF ~ OBJ_VALID(fitter) THEN MESSAGE, 'No valid fitter object given.'

       fitter->POSTERROR

     END ; FUNCITON BST_INST_POSTERROR




     ;+=================================================================
     ; Nothing here, just for compilation
     ; 
     ; TAGS:
     ;   exclude
     ;-=================================================================
     PRO BST_INST_PUBLIC
     END ; PRO BST_INST_PUBLIC
