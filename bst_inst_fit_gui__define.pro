



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Define the top level object for the GUI interface for the
;   BST_SPECTRAL_FIT application.
;
; DESCRIPTION:
;   The GUI is only a reflection (and way of controlling) the app
;   state. Importantly it does not  have a save state.
;   It is however possible to change global options, which will then
;   become part of the save state.
;
; PROGRAMMING NOTES:
;   There are a number of routines that simply wrap identically named
;   routines in the app.  This is done in an attempt to issolate the 
;   API for the gui from that of the app and the core.
;   
;
;-======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle initialization the <BST_INST_FIT_GUI::> object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::INIT, _REF_EXTRA=_extra

       ; Call the INIT methods for the base class.
       status = self->BST_INST_GUI_BASE::INIT(DESTROY_OBJECT_WITH_GUI=1)
       IF status NE 1 THEN MESSAGE, 'BST_INST_GUI_BASE::INIT failed.'


       self->INITIALIZE, _STRICT_EXTRA=_extra

       RETURN, 1

     END ;FUNCTION BST_INST_FIT_GUI::INIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the <BST_INST_FIT_GUI::> object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::INITIALIZE, _REF_EXTRA=_extra

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         ; Destroy the app if possible.
         IF OBJ_VALID(self.coreapp) THEN BEGIN
           self.coreapp->MESSAGE, 'Could not initialize the GUI.', /SEVERE_ERROR
           self.coreapp->DESTROY
         ENDIF

         MESSAGE, 'Could not initialize [BST_INST_FIT_GUI::].', /INFO
         MESSAGE, /REISSUE_LAST
       ENDIF
           

       ; NOTE: The order things are done here is important.
       ;       In particular the core must be initialized before
       ;       anything else.  This enables the log etc.

       ; First initialize the fitter core.
       self->MESSAGE, 'Initializing the fitter core.', /STATUS
       self->INITIALIZE_APP, _STRICT_EXTRA=_extra

       ; Build the GUI.
       self->MESSAGE, 'Building the GUI.', /STATUS
       self->INITIALIZE_GUI

       ; Initialize the widgets.
       self->MESSAGE, 'Initializing guiaddons.', /STATUS
       self->INITIALIZE_GUIADDONS

       ; Update the GUI.
       self->UPDATE

       ; Now that the GUI and all guiaddons have been initialized,
       ; allow the GUI to update.
       self->MESSAGE, 'Displaying GUI.', /STATUS
       self->SET_GUI_UPDATE, 1
       self->SET_GUI_MAP, 1

     END ;PRO BST_INST_FIT_GUI::INITIALIZE


     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> core object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::INITIALIZE_APP, _REF_EXTRA=_extra

       IF OBJ_VALID(self.coreapp) THEN BEGIN
         self.coreapp->DESTROY
       ENDIF

       self.coreapp = OBJ_NEW('BST_INST_FIT_APP', _STRICT_EXTRA=_extra)
       self.core = self.coreapp.core

       self.coreapp.coregui = self

     END ;PRO BST_INST_FIT_GUI::INITIALIZE_APP


     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> guiaddons object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::INITIALIZE_GUIADDONS

       IF OBJ_VALID(self.guiaddons) THEN BEGIN
         self.guiaddons->DESTROY
       ENDIF

       self.guiaddons = OBJ_NEW('BST_INST_GUIADDON_DISPATCH', self.core, self.coreapp, self)
       self.guiaddons.INITIALIZE

     END ;PRO BST_INST_FIT_GUI::INITIALIZE_GUIADDONS




;=======================================================================
;#######################################################################
;#######################################################################
;
; Methods to handle the GUI.
;
;#######################################################################
;#######################################################################
;=======================================================================



     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the main GUI window
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::INITIALIZE_GUI

       ; Geneate the title.
       title = self.coreapp->GET_APP_NAME()

       ; Build the GUI.
       self->GUI_INIT, TITLE=title, UPDATE=0, MAP=0, /MENUBAR


     END ;PRO BST_INST_FIT_GUI::INITIALIZE_GUI


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Build up the actuall gui components.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::GUI_DEFINE


       FORWARD_FUNCTION BST_CW_BACK, BST_CW_SCALED, BST_CW_GAUSS, BST_CW_BEAM_COMP

       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)



       ; Create the base containter widgets.
       ui_tab = WIDGET_TAB(ui_base, UNAME='ui_tab')

       ui_base_control = WIDGET_BASE(ui_base, /COLUMN)
         ; Add an empty base widget so that IDL resizes ui_base_control correctly.
         ui_base_control_empty = WIDGET_BASE(ui_base_control)

       ui_base_fit_control = WIDGET_BASE(ui_base, /ROW, /ALIGN_RIGHT)

       ui_base_status = WIDGET_BASE(ui_base, /ROW)

          

       ; -----------------------------------------------------------
       ; Crate the fit controls.
       
       ui_base_xrange = WIDGET_BASE(ui_base_fit_control, /ROW, FRAME=1)
         ui_field_xrange_low = CW_FIELD_EXT(ui_base_xrange $
                                            ,VALUE=0 $
                                            ,TITLE='Fit Range' $
                                            ,/INT $
                                            ,/ALL_EVENTS $
                                            ,XSIZE=4 $
                                            ,MAXLENGTH=4)

         ui_field_xrange_high = CW_FIELD_EXT(ui_base_xrange $
                                             ,VALUE=0 $
                                             ,TITLE='' $
                                             ,/INT $
                                             ,/ALL_EVENTS $
                                             ,XSIZE=4 $
                                             ,MAXLENGTH=4)
 
       ui_base_fit_buttons = WIDGET_BASE(ui_base_fit_control, /ROW, /ALIGN_CENTER)
         ui_button_plot = WIDGET_BUTTON(ui_base_fit_buttons, VALUE='Plot' $
                                        ,XSIZE=160 $
                                        ,YSIZE=40)
         ui_button_analyze = WIDGET_BUTTON(ui_base_fit_buttons, VALUE='Analyze' $
                                           ,XSIZE=160 $
                                           ,YSIZE=40)



       ; -----------------------------------------------------------
       ; Create the status bar.
       ;
       ui_label_status = WIDGET_LABEL(ui_base_status, UNAME='ui_label_status' $
                                    ,VALUE=' ' $
                                    ,FRAME=0 $
                                    ,/DYNAMIC_RESIZE)


       ; -----------------------------------------------------------
       ; Create the main menu bar
       ;
       ui_base_menu_file = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='File', /MENU)
         ui_button_save_state = WIDGET_BUTTON(ui_base_menu_file, VALUE='Save State')
         ui_button_load_state = WIDGET_BUTTON(ui_base_menu_file, VALUE='Load State')
         ui_button_reset = WIDGET_BUTTON(ui_base_menu_file, VALUE='Reset State')

         ui_button_exit = WIDGET_BUTTON(ui_base_menu_file, VALUE='Exit', $
                                        XSIZE=150, /SEPARATOR)

       ui_base_menu_fix = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='Fix', /MENU)
         ui_button_fix = WIDGET_BUTTON(ui_base_menu_fix $
                                          ,UNAME='ui_button_fix' $
                                          ,VALUE='Fix All')
         ui_button_unfix = WIDGET_BUTTON(ui_base_menu_fix $
                                            ,UNAME='ui_button_unfix' $
                                            ,VALUE='Unfix All')

       ui_base_menu_plot = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='Plot', /MENU)
         ui_button_plot_pdf = WIDGET_BUTTON(ui_base_menu_plot $
                                           ,UNAME='ui_button_plot_pdf' $
                                           ,VALUE='Plot to PDF')
         ui_button_plot_ps = WIDGET_BUTTON(ui_base_menu_plot $
                                           ,UNAME='ui_button_plot_ps' $
                                           ,VALUE='Plot to PostScript')

       ui_base_menu_commands = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='Commands', /MENU)
         ui_button_clear_cache = WIDGET_BUTTON(ui_base_menu_commands $
                                              ,UNAME='ui_button_clear_cache' $
                                              ,VALUE='Clear Cache')
         
       ui_base_menu_options = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='Options', /MENU)
         ui_button_curvefit = WIDGET_BUTTON(ui_base_menu_options $
                                           ,UNAME='ui_button_curvefit' $
                                           ,VALUE='Fit with CURVEFIT' $
                                           ,/CHECKED_MENU $
                                           ,UVALUE={checked:0})

       ui_base_menu_models = WIDGET_BUTTON(self.gui_param.ui_menu_bar $
                                           ,/MENU $
                                           ,VALUE='Models' $
                                           ,UNAME='ui_base_menu_models' $
                                           ,EVENT_PRO='bst_inst_addons_event')

       ui_base_menu_addons = WIDGET_BUTTON(self.gui_param.ui_menu_bar, /MENU, VALUE='Addons' $
                                           ,UNAME='ui_base_menu_addons' $
                                           ,EVENT_PRO='bst_inst_addons_event')

       ui_base_menu_guiaddons = WIDGET_BUTTON(self.gui_param.ui_menu_bar, /MENU, VALUE='GuiAddons' $
                                           ,UNAME='ui_base_menu_guiaddons' $
                                           ,EVENT_PRO='bst_inst_guiaddons_event')

       ui_base_menu_debug = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='Debug', /MENU, /ALIGN_RIGHT)
         ui_button_debug_update = WIDGET_BUTTON(ui_base_menu_debug $
                                              ,UNAME='ui_button_debug_update' $
                                              ,VALUE='Update GUI' $
                                              ,/CHECKED_MENU)
         ui_button_debug_error = WIDGET_BUTTON(ui_base_menu_debug $
                                               ,UNAME='ui_button_debug_error' $
                                               ,VALUE='Show error' $
                                               ,/CHECKED_MENU $
                                               ,/SEPARATOR)
         ui_button_debug_debug = WIDGET_BUTTON(ui_base_menu_debug $
                                               ,UNAME='ui_button_debug_debug' $
                                               ,VALUE='Show debug' $
                                               ,/CHECKED_MENU)
         ui_button_debug_models = WIDGET_BUTTON(ui_base_menu_debug $
                                               ,UNAME='ui_button_debug_models' $
                                               ,VALUE='Show model debug' $
                                               ,/CHECKED_MENU)
         ui_button_debug_last_error = WIDGET_BUTTON(ui_base_menu_debug $
                                                    ,UNAME='ui_button_debug_last_error' $
                                                    ,VALUE='Show last error' $
                                                    ,/CHECKED_MENU $
                                                    ,/SEPARATOR)
         ui_button_debug_traceback = WIDGET_BUTTON(ui_base_menu_debug $
                                                   ,UNAME='ui_button_debug_traceback' $
                                                   ,VALUE='Show traceback' $
                                                   ,/CHECKED_MENU)
         ui_button_debug_stop = WIDGET_BUTTON(ui_base_menu_debug $
                                              ,UNAME='ui_button_debug_stop' $
                                              ,VALUE='Stop on error' $
                                              ,/CHECKED_MENU $
                                              ,/SEPARATOR)
         ui_button_debug_timer = WIDGET_BUTTON(ui_base_menu_debug $
                                              ,UNAME='ui_button_debug_timer' $
                                              ,VALUE='Run profiler' $
                                              ,/CHECKED_MENU)


       ui_base_menu_help = WIDGET_BUTTON(self.gui_param.ui_menu_bar, VALUE='Help', /MENU, /ALIGN_RIGHT)
         ui_button_help = WIDGET_BUTTON(ui_base_menu_help, VALUE='Help')


         

       ; Store all of the accesable widgit id's here.
       id_struct = { $
                     ui_base:ui_base $
                     ,ui_tab:ui_tab $
                     ,ui_base_control:ui_base_control $
                     ,ui_base_fit_control:ui_base_fit_control $
                     ,ui_base_fit_buttons:ui_base_fit_buttons $
                     ,ui_base_menu_plot:ui_base_menu_plot $
                     ,ui_base_menu_commands:ui_base_menu_commands $
                     ,ui_base_menu_options:ui_base_menu_options $
                     ,ui_base_menu_models:ui_base_menu_models $
                     ,ui_base_menu_addons:ui_base_menu_addons $
                     ,ui_base_menu_guiaddons:ui_base_menu_guiaddons $

                     ,ui_button_exit:ui_button_exit $
                     ,ui_button_save_state:ui_button_save_state $
                     ,ui_button_load_state:ui_button_load_state $
                     ,ui_button_reset:ui_button_reset $

                     ,ui_button_fix:ui_button_fix $
                     ,ui_button_unfix:ui_button_unfix $
                     
                     ,ui_button_plot_ps:ui_button_plot_ps $
                     ,ui_button_plot_pdf:ui_button_plot_pdf $
                     ,ui_button_clear_cache:ui_button_clear_cache $
                     ,ui_button_help:ui_button_help $      
               
                     ,ui_button_debug_update:ui_button_debug_update $
                     ,ui_button_debug_error:ui_button_debug_error $
                     ,ui_button_debug_debug: ui_button_debug_debug $
                     ,ui_button_debug_models: ui_button_debug_models $
                     ,ui_button_debug_last_error:ui_button_debug_last_error $
                     ,ui_button_debug_traceback:ui_button_debug_traceback $
                     ,ui_button_debug_stop:ui_button_debug_stop $
                     ,ui_button_debug_timer:ui_button_debug_timer $

                     ,ui_button_curvefit:ui_button_curvefit $
                     
                     ,ui_field_xrange_low:ui_field_xrange_low $
                     ,ui_field_xrange_high:ui_field_xrange_high $
                     ,ui_button_plot:ui_button_plot $
                     ,ui_button_analyze:ui_button_analyze $
                     
                     ,ui_label_status:ui_label_status $
                   }
                   
       ; Save the id structure.
       self->SET_ID_STRUCT, id_struct
 

     END ;PRO BST_INST_FIT_GUI::GUI_DEFINE


     ;+=================================================================
     ;
     ; This is the main event handler for the BST_INST_FIT_GUI.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::EVENT_HANDLER, event $
                                          ,EVENT_HANDLED=event_handled

       ; First pass the event to the app class
       self->BST_INST_GUI_BASE::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing GUI event.', /ERROR
         RETURN
       ENDIF

       ; Get the widget id structure
       id = self->GET_ID_STRUCT()

       CASE event.id OF


         id.ui_button_exit: BEGIN
           self->CLEANUP_HANDLER
         END


         id.ui_button_save_state: BEGIN
           self->SAVE
         END


         id.ui_button_load_state: BEGIN
           self->LOAD
         END


         id.ui_button_reset: BEGIN
           self->RESET_STATE
         END


         id.ui_button_fix: BEGIN
           self->SET_FIXALL, 1
         END


         id.ui_button_unfix: BEGIN
           self->SET_FIXALL, 0
         END


         id.ui_button_plot_ps: BEGIN
           self->PLOT, /POSTSCRIPT
         END


         id.ui_button_plot_pdf: BEGIN
           self->PLOT, /PDF
         END

         
         id.ui_button_clear_cache: BEGIN
           self->CLEAR_CACHE
         END

         
         ; -------------------------------------------------------------
         ; Handle buttons on the Options menu.
         id.ui_button_curvefit: BEGIN
           use_curvefit = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=use_curvefit
           self.coreapp->SET_OPTIONS_USER, {use_curvefit:use_curvefit}
         END


         ; -------------------------------------------------------------
         ; Handle buttons on the help menu.
         id.ui_button_help: BEGIN
           ONLINE_HELP, BOOK='bst_spectral_fit.adp'
         END


         ; Handle buttons on the Debug menu
         ; -------------------------------------------------------------
         id.ui_button_debug_update: BEGIN
           self->UPDATE
         END


         id.ui_button_debug_error: BEGIN
           error = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=error
           self.coreapp->SET_OPTIONS_DEBUG, {error:error}
         END


         id.ui_button_debug_debug: BEGIN
           debug = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=debug
           self.coreapp->SET_OPTIONS_DEBUG, {debug:debug}
         END


         id.ui_button_debug_models: BEGIN
           models = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=models
           self.coreapp->SET_OPTIONS_DEBUG, {models:models}
         END


         id.ui_button_debug_last_error: BEGIN
           last_error = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=last_error
           self.coreapp->SET_OPTIONS_DEBUG, {last_error:last_error}
         END


         id.ui_button_debug_traceback: BEGIN
           traceback = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=traceback
           self.coreapp->SET_OPTIONS_DEBUG, {traceback:traceback}
         END


         id.ui_button_debug_stop: BEGIN
           stop = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=stop
           self.coreapp->SET_OPTIONS_DEBUG, {stop:stop}
         END


         id.ui_button_debug_timer: BEGIN
           timer = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=timer
           self.coreapp->SET_OPTIONS_DEBUG, {timer:timer}
         END


         ; Handle the fit controls
         ; -------------------------------------------------------------
         id.ui_field_xrange_low: BEGIN
           WIDGET_CONTROL, id.ui_field_xrange_low, GET_VALUE=value_low
           WIDGET_CONTROL, id.ui_field_xrange_high, GET_VALUE=value_high
           self.coreapp->SET_OPTIONS_USER, {xrange:[value_low, value_high]}
         END


         id.ui_field_xrange_high: BEGIN
           WIDGET_CONTROL, id.ui_field_xrange_low, GET_VALUE=value_low
           WIDGET_CONTROL, id.ui_field_xrange_high, GET_VALUE=value_high
           self.coreapp->SET_OPTIONS_USER, {xrange:[value_low, value_high]}
         END


         id.ui_button_analyze: BEGIN
           ; I only want to show profiler information if the fit was
           ; started from the fit button.
           IF (self.core).debug.timer THEN self.PROFILER_RESET
           self->FIT
           IF (self.core).debug.timer THEN self.PROFILER_REPORT
         END


         id.ui_button_plot: BEGIN
           self->PLOT_SPECTRUM
         END
         

         ; Handle base widget events
         ; -------------------------------------------------------------
         id.ui_tab:
         self.gui_param.gui_id:


         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE
       ENDCASE

     END ; PRO BST_INST_FIT_GUI::EVENT_HANDLER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Update the state of the GUI and all guiaddons.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::UPDATE

       ; Turn off GUI updating.
       self->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       IF ISA(self.coreapp) THEN BEGIN
         self.coreapp->UPDATE
       ENDIF

       ; It probably would be better to have an option to disable
       ; updates while doing initialization.  This works though.
       IF ISA(self.guiaddons) THEN BEGIN
         self.guiaddons->UPDATE
       ENDIF
       
       self->GUI_UPDATE

       ; Restore the update state
       self->SET_GUI_UPDATE, update_old

     END ;PRO BST_INST_FIT_GUI::UPDATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Update the state of the GUI. 
     ;   This does not handle any guiaddons. In general [::UPDATE] should
     ;   be used instead.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::GUI_UPDATE


       ; Turn off GUI updating.
       self->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old


       self->UPDATE_OPTIONS
       self->UPDATE_OPTIONS_DEBUG


       ; Restore the update state
       self->SET_GUI_UPDATE, update_old

     END ;PRO BST_INST_FIT_GUI::GUI_UPDATE
     

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Update the options displayed on the gui.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::UPDATE_OPTIONS

       ; Get the widget id structure
       id = self->GET_ID_STRUCT()

       options = self.coreapp->GET_OPTIONS_USER()

       WIDGET_CONTROL, id.ui_field_xrange_low, SET_VALUE=options.xrange[0]
       WIDGET_CONTROL, id.ui_field_xrange_high, SET_VALUE=options.xrange[1]

       WIDGET_CONTROL, id.ui_button_curvefit, SET_BUTTON=options.use_curvefit
       
     END ;PRO BST_INST_FIT_GUI::UPDATE_OPTIONS


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Update the options displayed on the gui.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::UPDATE_OPTIONS_DEBUG

       ; Get the widget id structure
       id = self->GET_ID_STRUCT()

       options = self.coreapp->GET_OPTIONS_DEBUG()

       WIDGET_CONTROL, id.ui_button_debug_error, SET_BUTTON=options.error
       WIDGET_CONTROL, id.ui_button_debug_debug, SET_BUTTON=options.debug
       WIDGET_CONTROL, id.ui_button_debug_models, SET_BUTTON=options.models
       WIDGET_CONTROL, id.ui_button_debug_last_error, SET_BUTTON=options.last_error
       WIDGET_CONTROL, id.ui_button_debug_traceback, SET_BUTTON=options.traceback
       WIDGET_CONTROL, id.ui_button_debug_stop, SET_BUTTON=options.stop
       WIDGET_CONTROL, id.ui_button_debug_timer, SET_BUTTON=options.timer
       
     END ;PRO BST_INST_FIT_GUI::UPDATE_OPTIONS_DEBUG



;=======================================================================
;#######################################################################
;#######################################################################
;
; Methods to handle interaction with the app and the guiaddons.
;
;#######################################################################
;#######################################################################
;=======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This is the main fit loop for the GUI application.
     ; 
     ; DESCRIPTION:
     ;   This method will be called when a fit loop is started.  This
     ;   is the default action for when the 'Analyze' button is pressed.
     ;
     ;   The fit loop is similar to [BST_INST_FIT::QUICKFIT], however
     ;   hooks are provided for the guiaddons.
     ;
     ;   Here is a simplified description of what is taking place. It
     ;   is important to note the order in which these actions take
     ;   place.
     ;
     ;     gui->BEFOREFIT
     ;       guiaddons->BEFORE_FIT
     ;     app->BEFORE_FIT
     ;       addons->BEFORE_FIT
     ;     core->BEFORE_FIT
     ;       spectrum->LOAD_SPECTRUM
     ;       models->SAVE_INITIAL_STATE
     ;
     ;     gui->PREFIT
     ;       guiaddons->PREFIT
     ;     app->PREFIT
     ;       addons->PREFIT
     ;     core->PPREFIT
     ;       models->PREFIT
     ;       models->PREFIT_CONSTRAINTS
     ;
     ;     FIT
     ;
     ;     core->POSTFIT
     ;       models->RESTORE_INITIAL_STATE
     ;     app->POSTFIT
     ;       addons->POSTFIT
     ;     gui->POSTFIT
     ;       guiaddons->POSTFIT
     ;    
     ;
     ;     core->AFTER_FIT
     ;     app->AFTER_FIT
     ;       addons->AFTER_FIT
     ;     gui->AFTER_FIT
     ;       guiaddons->AFTER_FIT
     ;
     ;     gui->UPDATE
     ;       guiaddons->UPDATE
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::FIT
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         self->MESSAGE, 'Fit failed.', /ERROR

         self->POSTERROR
         RETURN
       ENDIF

       IF self.core.debug.timer THEN MIR_TIMER, /START

       IF self.core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_GUI::FIT | PRE-FITTING', /START
       self->BEFORE_FIT
       self->PREFIT
       IF self.core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_GUI::FIT | PRE-FITTING', /STOP


       IF self.core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_GUI::FIT | FITTING', /START
       self.core->FIT
       IF self.core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_GUI::FIT | FITTING', /STOP


       IF self.core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_GUI::FIT | POST-FITTING', /START
       self->POSTFIT
       self->AFTER_FIT
       IF self.core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_GUI::FIT | POST-FITTING', /STOP


       IF self.core.debug.timer THEN MIR_TIMER, /STOP

       self->MESSAGE, 'Fit complete.', /STATUS

     END ;PRO BST_INST_FIT_GUI::FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action before starting starting the fit.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::BEFORE_FIT
       self->MESSAGE, 'Preparing for fit.', /STATUS

       options_user = self.GET_OPTIONS_USER()
       IF ~options_user.disable_gui_update THEN BEGIN
         self->LOCK_FOR_FIT, /LOCK
       ENDIF

       self.guiaddons->BEFORE_FIT
       self.coreapp->BEFORE_FIT

     END ;PRO BST_INST_FIT_GUI::BEFORE_FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action immediatly before enting the fit loop.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PREFIT
       self->MESSAGE, 'GUI: Performing prefit.', /STATUS

       self.guiaddons->PREFIT
       self.coreapp->PREFIT

     END ;PRO BST_INST_FIT_GUI::PREFIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action immediatly after exiting the fit loop.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::POSTFIT
       self->MESSAGE, 'GUI: Performing postfit.', /STATUS

       ; Perform any postfit actions.
       self.coreapp->POSTFIT
       self.guiaddons->POSTFIT

     END ;PRO BST_INST_FIT_GUI::POSTFIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action after a fit has been completed
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::AFTER_FIT
       self->MESSAGE, 'GUI: Performing postfit.', /STATUS

       ; Perform any postfit actions.
       self.coreapp->AFTER_FIT
       self.guiaddons->AFTER_FIT


       options_user = self.GET_OPTIONS_USER()
       IF ~options_user.disable_gui_update THEN BEGIN
         ; Update the GUI and all guiaddons.
         self->UPDATE

         self->LOCK_FOR_FIT, /UNLOCK
       ENDIF

     END ;PRO BST_INST_FIT_GUI::AFTER_FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action if an error occurs while doing a fit.
     ;
     ; DESCRIPTION:
     ;   At this point this is almost identical to AFTER_FIT.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::POSTERROR
       self->MESSAGE, 'Cleaning up after failed fit.', /DEBUG

       self.coreapp->POSTERROR

       self.guiaddons->AFTER_FIT

       options_user = self.GET_OPTIONS_USER()
       IF ~options_user.disable_gui_update THEN BEGIN
         ; Update the GUI and all guiaddons.
         self->UPDATE

         self->LOCK_FOR_FIT, /UNLOCK
       ENDIF
       
     END ;PRO BST_INST_FIT_GUI::POSTERROR


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Before starting the fit we want to disable certain parts
     ;   of the gui (such as the analyze and plot buttons.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::LOCK_FOR_FIT, LOCK=lock_key, UNLOCK=unlock_key


       id = self->GET_ID_STRUCT()

       IF ~ (KEYWORD_SET(lock_key) XOR KEYWORD_SET(unlock_key)) THEN BEGIN
         MESSAGE, 'Programming error: one of /LOCK or /UNLOCK should be used.'
       ENDIF

       lock = KEYWORD_SET(lock_key)

       WIDGET_CONTROL, id.ui_button_analyze, SENSITIVE=~lock
       WIDGET_CONTROL, id.ui_button_plot, SENSITIVE=~lock
       WIDGET_CONTROL, self.gui_param.ui_menu_bar, SENSITIVE=~lock
       

     END ;PRO BST_INST_FIT_GUI::LOCK_FOR_FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Load the application state from a save file.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::LOAD, filename, FORCE=force

       

       ; Turn off GUI updating.
       self->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       self.coreapp->LOAD, filename, FORCE=force, DIALOG_PARENT=self.gui_param.gui_id

       ; Update the gui.
       self->UPDATE

       ; Restore the update state
       self->SET_GUI_UPDATE, update_old

       
     END ;PRO BST_INST_FIT_GUI::LOAD


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the save state for
     ;   [BST_INST_FIT_GUI]
     ;
     ; DESCRIPTION:
     ;   This will return the state of:
     ;     fitter gui
     ;     fitter core
     ;     gui components
     ;     addons
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_STATE

       state = self.coreapp->GET_STATE()

       state = CREATE_STRUCT(state, {guiaddons:self.guiaddons->GET_STATE()})

       RETURN, state

     END ; FUNCTION BST_INST_FIT_GUI::GET_STATE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Load the application state from a save file.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::LOAD_STATE, state, FORCE=force


       ; Turn off GUI updating.
       self->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       self.coreapp->LOAD_STATE, state, FORCE=force

       ; Check to see if there are any saved guiaddon states.
       ; If so load them.
       tagnames = TAG_NAMES(state)
       w = WHERE(tagnames EQ 'GUIADDONS', count_w)
       
       IF count_w NE 0 THEN BEGIN
         CATCH, error
         IF error EQ 0 THEN BEGIN
           self.guiaddons->LOAD_STATE, state.guiaddons
         ENDIF ELSE BEGIN
           CATCH, /CANCEL
           self->MESSAGE, 'Error loading guiaddons state.', /ERROR, /LOG
         ENDELSE
       ENDIF

       
       ; Update the gui.
       self->UPDATE

       ; Restore the update state
       self->SET_GUI_UPDATE, update_old

     END ;PRO BST_INST_FIT_GUI::LOAD_STATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reset the application.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::RESET_STATE


       ; Turn off GUI updating.
       self->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       self.coreapp->RESET_STATE

       ; Update the gui.
       self->UPDATE

       ; Restore the update state
       self->SET_GUI_UPDATE, update_old

     END ;PRO BST_INST_FIT_GUI::RESET_STATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Fix all parameters.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::SET_FIXALL, fixall

       (self.core).models->SET_FIXALL, fixall
       self->UPDATE


     END ;PRO BST_INST_FIT_GUI::FIX_ALL


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the save status on all parameters.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::SET_SAVE, save

       (self.core).models->SET_SAVE, save
       self->UPDATE


     END ;PRO BST_INST_FIT_GUI::FIX_ALL

     
     ;+=================================================================
     ;
     ; Get the id's for various widgets.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_GUI_ID, BASE_MAIN=base_main $
                                            ,BASE_TAB=base_tab $
                                            ,BASE_CONTROL=base_control $
                                            ,BASE_FIT_CONTROL=base_fit_control $
                                            ,BASE_FIT_BUTTONS=base_fit_buttons $

                                            ,MENU_BAR=menu_bar $
                                            ,MENU_PLOT=menu_plot $
                                            ,MENU_COMMANDS=menu_commands $
                                            ,MENU_OPTIONS=menu_options $
                                            ,MENU_MODELS=menu_models $
                                            ,MENU_ADDONS=menu_addons $
                                            ,MENU_GUIADDONS=menu_guiaddons


       id = self->GET_ID_STRUCT()

       CASE 1 OF

         KEYWORD_SET(base_main): BEGIN
           id_out = self.gui_param.gui_id
         END

         KEYWORD_SET(base_tab): BEGIN
           id_out = id.ui_tab
         END

         KEYWORD_SET(base_control): BEGIN
           id_out = id.ui_base_control
         END

         KEYWORD_SET(base_fit_control): BEGIN
           id_out = id.ui_base_fit_control
         END

         KEYWORD_SET(base_fit_buttons): BEGIN
           id_out = id.ui_base_fit_buttons
         END

         KEYWORD_SET(menu_bar): BEGIN
           id_out = self.gui_param.ui_menu_bar
         END

         KEYWORD_SET(menu_plot): BEGIN
           id_out = id.ui_base_menu_plot
         END

         KEYWORD_SET(menu_commands): BEGIN
           id_out = id.ui_base_menu_commands
         END

         KEYWORD_SET(menu_options): BEGIN
           id_out = id.ui_base_menu_options
         END

         KEYWORD_SET(menu_models): BEGIN
           id_out = id.ui_base_menu_models
         END

         KEYWORD_SET(menu_addons): BEGIN
           id_out = id.ui_base_menu_addons
         END

         KEYWORD_SET(menu_guiaddons): BEGIN
           id_out = id.ui_base_menu_guiaddons
         END
         
         ELSE: BEGIN
           id_out = self.gui_param.gui_id
         END
       ENDCASE
       

       RETURN, id_out

     END ;END FUNCTION BST_INST_FIT_GUI::GET_GUI_ID


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the fitter core object
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_CORE

       RETURN, self.core

     END ; FUNCTION BST_INST_FIT_GUI::GET_CORE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the fitter app object
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_APP

       RETURN, self.coreapp

     END ; FUNCTION BST_INST_FIT_GUI::GET_APP

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the guiaddon dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_ADDON_DISPATCHER

       RETURN, self.coreapp.addons

     END ; FUNCTION BST_INST_FIT_GUI::GET_ADDON_DISPATCHER
     
     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the guiaddon dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_GUIADDON_DISPATCHER

       RETURN, self.guiaddons

     END ; FUNCTION BST_INST_FIT_GUI::GET_GUIADDON_DISPATCHER





;=======================================================================
;#######################################################################
;#######################################################################
;
; Debugging methods.
;
;#######################################################################
;#######################################################################
;=======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Start the profiler.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PROFILER_START

       MIR_TIMER, /BEGIN_TIMING
       self.SET_OPTIONS_DEBUG, {timer:1}
       self.PROFILER_RESET

     END ;PRO BST_INST_FIT::START_PROFILER


     ;+=================================================================
     ; PURPOSE:
     ;   Stop the profiler and display the results.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PROFILER_STOP

       self.PROFILER_REPORT
       self.SET_OPTIONS_DEBUG, {timer:0}

       ; I probably don't want to stop the timer, incase I am timing
       ; a progarme at a higher level.
       ;MIR_TIMER, /END_TIMING

     END ;PRO BST_INST_FIT::STOP_PROFILER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reset the MIR_TIMER for profiling.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PROFILER_RESET

       MIR_TIMER, /RESET

     END ;PRO BST_INST_FIT_GUI::PROFILER_RESET


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Report the profiling results from MIR_TIMER.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PROFILER_REPORT

       PRINT, '--------------------------------------'
       PRINT, 'Profiling results for BST_SPECTRAL_FIT'
       MIR_TIMER, /REPORT

     END ;PRO BST_INST_FIT_GUI::PROFILER_REPORT




;=======================================================================
;#######################################################################
;#######################################################################
;
; Pass through methods.  
; These just call the equivilant app method.
;
;#######################################################################
;#######################################################################
;=======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Save the state of the application to a save file.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::SAVE, filename, _REF_EXTRA=_extra

       self.coreapp->SAVE, filename, /DIALOG, _STRICT_EXTRA=_extra

     END ;PRO BST_INST_FIT_GUI::SAVE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Clear all cached values. Also applies to MODELS and ADDONS.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::CLEAR_CACHE, _REF_EXTRA=_extra

       self.coreapp->CLEAR_CACHE, _STRICT_EXTRA=_extra

     END ;PRO BST_INST_FIT_GUI::CLEAR_CACHE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Plot the spectrum and the fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PLOT, _REF_EXTRA=_extra

       self.coreapp->PLOT, _STRICT_EXTRA=_extra

     END ;PRO BST_INST_FIT_GUI::PLOT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Plot the spectrum without displaying the fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::PLOT_SPECTRUM, _REF_EXTRA=_extra

       self.coreapp->PLOT_SPECTRUM, _STRICT_EXTRA=_extra

     END ;PRO BST_INST_FIT_GUI::PLOT_SPECTRUM


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the configuration dictionary.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_CONFIG

       RETURN, self.coreapp->GET_CONFIG()

     END ;FUNCTION BST_INST_FIT_GUI::GET_CONFIG


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the installation path.  This assumes that the
     ;   installation path has already be found and saved in the app.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_INSTALL_PATH

       RETURN, self.coreapp->GET_INSTALL_PATH()

     END ; FUNCTION BST_INST_FIT_GUI::GET_INSTALL_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current path.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_CURRENT_PATH

       RETURN, self.coreapp->GET_CURRENT_PATH()

     END ; FUNCTION BST_INST_FIT_GUI::GET_CURRENT_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current path.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::SET_CURRENT_PATH, path

       self.coreapp->SET_CURRENT_PATH, path

     END ; PRO BST_INST_FIT_GUI::SET_CURRENT_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Seth the user options
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::SET_OPTIONS_USER, struct

       self.coreapp->SET_OPTIONS_USER, struct

     END ;PRO BST_INST_FIT_GUI::SET_OPTIONS_USER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the user options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_OPTIONS_USER

       RETURN, self.coreapp->GET_OPTIONS_USER() 

     END ;FUNCTION BST_INST_FIT_GUI::GET_OPTIONS_USER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the debug options
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::SET_OPTIONS_DEBUG, struct

       self.coreapp->SET_OPTIONS_DEBUG, struct

     END ;PRO BST_INST_FIT_GUI::SET_OPTIONS_DEBUG


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the debug options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_OPTIONS_DEBUG

       RETURN, self.coreapp->GET_OPTIONS_DEBUG() 

     END ;FUNCTION BST_INST_FIT_GUI::GET_OPTIONS_DEBUG


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle messages.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::MESSAGE, message_array $
                                    ,SCOPE_LEVEL=scope_level_in $
                                    ,_REF_EXTRA=extra

       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1

       IF OBJ_VALID(self.coreapp) THEN BEGIN
         ; Pass off the message to the app messaging routine.
         self.coreapp->MESSAGE, message_array $
                             ,SCOPE_LEVEL=scope_level $
                             ,_STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         ; The app is not initialized, so just print the message.
         MIR_LOGGER, message_array $
                     ,SCOPE_LEVEL=scope_level $
                     ,_EXTRA=extra
       ENDELSE

     END ;PRO BST_INST_FIT_GUI::MESSAGE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current application version.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_GUI::GET_VERSION

       RETURN, self.coreapp->GET_VERSION()

     END ; FUNCTION BST_INST_FIT_GUI::GET_VERSION






;=======================================================================
;#######################################################################
;#######################################################################
;
; Object definition.
;
;#######################################################################
;#######################################################################
;=======================================================================

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Quit BST_INST_FIT.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::QUIT
       WIDGET_CONTROL, self.gui_param.gui_id, /DESTROY

     END ; PRO BST_INST_FIT_GUI::QUIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on object destruction object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI::CLEANUP

       ; Clean up garbage.
       CATCH, error
       IF error EQ 0 THEN BEGIN
PRINT, 'Starting: BST_INST_FIT_GUI::CLEANUP'
HEAP_GC, /VERBOSE
       ENDIF ELSE BEGIN
         MESSAGE, 'Error cleaning up lost references.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE

       ; Turn off gui updating.
       self->SET_GUI_MAP, 0
       self->SET_GUI_UPDATE, 0

       error=0

       ; Destroy the guiaddons.
       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.guiaddons->DESTROY
PRINT, 'Guiaddons destroyed.'
HEAP_GC, /VERBOSE
       ENDIF ELSE BEGIN
         MESSAGE, 'Error destroying guiaddons.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE


       ; Destroy the fitter app.
       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.coreapp->DESTROY
PRINT, 'App destroyed.'
HEAP_GC, /VERBOSE
       ENDIF ELSE BEGIN
         MESSAGE, 'Error destroying fitting app.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE


       ; Call the CLEANP method of the base GUI class.
       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->BST_INST_GUI_BASE::CLEANUP
       ENDIF ELSE BEGIN
         MESSAGE, 'BST_INST_GUI_BASE::CLEANUP failed.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE

PRINT, 'Done: BST_INST_FIT_GUI::CLEANUP'
HEAP_GC, /VERBOSE

     END ;PRO BST_INST_FIT_GUI::CLEANUP




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object defines the main GUI for <BST_SPECTRAL_FIT>
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_GUI__DEFINE

       struct = { BST_INST_FIT_GUI $

                  ,guiaddons:OBJ_NEW() $

                  ,INHERITS BST_INST_GUI_BASE $
                }

     END ;PRO BST_INST_FIT_GUI__DEFINE
