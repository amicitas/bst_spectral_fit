

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-10
;
; PURPOSE:
;   Handle retrival for profiles for <BST_SPECTRAL_FIT>.
;
;
;
;-======================================================================
     




     ;+=================================================================
     ; PURPOSE:
     ;   Return a <SUM_OF_GAUSS::> object with all of the gaussians
     ;   in the given profile
     ;-=================================================================
     FUNCTION BST_INST_FIT_PROFILES::GET_PROFILE, profile_num

       models = (self.core).models

       RETURN, models->GET_PROFILE(profile_num)

     END ; FUNCTION BST_INST_FIT_PROFILES::GET_PROFILE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a list of the currently defined profiles.
     ;-=================================================================
     FUNCTION BST_INST_FIT_PROFILES::GET_PROFILE_NUMBER_LIST, _REF_EXTRA=extra

       models = (self.core).models

       RETURN, models->GET_PROFILE_NUMBER_LIST(_STRICT_EXTRA=extra)

     END ;FUNCTION BST_INST_FIT_PROFILES::GET_PROFILE_NUMBER_LIST



     ;+=================================================================
     ; PURPOSE:
     ;   Return the number of unique profile numbers currently defined.
     ;-=================================================================
     FUNCTION BST_INST_FIT_PROFILES::NUM_PROFILES, _REF_EXTRA=extra

       profile_list = self->GET_PROFILE_NUMBER_LIST(_STRICT_EXTRA=extra)

       num_profiles = profile_list->N_ELEMENTS()

       profile_list->DESTROY

       RETURN, num_profiles

     END ;FUNCTION BST_INST_FIT_PROFILES::NUM_PROFILES




     ;+=================================================================
     ;
     ; Create the BST_INST_PRFOFILES object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_PROFILES__DEFINE

       bst_inst_fit_profiles = $
          { BST_INST_FIT_PROFILES $

            ,INHERITS BST_INST_BASE $
          }

     END ;PRO BST_INST_FIT_PROFILES__DEFINE
