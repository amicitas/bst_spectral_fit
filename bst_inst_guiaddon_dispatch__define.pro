
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An object to handle the adddons for <BST_SPECTRAL_FIT>.
;
; DESCRIPTION:
;   This object handles all of the guiaddons for <BST_SPECTRAL_FIT>.
;   It serves as an interface between the application core and the
;   guiaddons.  As such it contains three types of routines:
;     1. Methods to find and initialize the guiaddons.
;     2. Methods for the application to access the guiaddons
;     3. Methods for  the guiaddons to access the application.
;
;   Note that there two places that guiaddons can be found
;     guiaddons/
;     guiaddons_internal/
;
;   The guiaddons_internal contains guiaddons that should allways be loaded
;   for <BST_SPECTRAL_FIT>.  This includes, for example, an
;   guiaddon to control the gaussian, background and scaled models.
;
; PROGRAMMING NOTES:
;   There is a lot of duplicate code at the moment in the following
;   theree objects.  This should be put into a superclass.
;     BST_INST_MODEL_DISPATCH
;     BST_INST_ADDON_DISPATCH
;     BST_INST_GUIADDON_DISPATCH
;
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_DISPATCH::INIT, core, app, gui, _REF_EXTRA=extra

       self.object_type = 'guiaddon'

       status = self->BST_INST_DISPATCH::INIT(core, app, gui, _STRICT_EXTRA=extra)

       ; Programming Note:
       ;  After instantiation it is still nessecary to call initialize.
       ;  This is important since individual addons may need access to this
       ;  dispatcher during their instantiation/initialization.

       RETURN, status

     END ;FUNCTION BST_INST_GUIADDON_DISPATCH::INIT


     ;+=================================================================
     ; PURPOSE:
     ;   Here we find and initialize all of the guiaddons.
     ;
     ;   All constraints are assumed to be implemented as a guiaddon.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DISPATCH::INITIALIZE


       directory = ['guiaddons']
       FOR ii=0,N_ELEMENTS(directory)-1 DO BEGIN
         directory[ii] = MIR_PATH_JOIN([self.core->GET_INSTALL_PATH(), directory[ii]])
       ENDFOR
       config_section = 'GUIADDONS'
       superclass = 'BST_INST_GUIADDON'
       test_methods_array = ['IS_ACTIVE']

       self->LOAD_OBJECTS, directory  $
                          ,CONFIG_SECTION=config_section $
                          ,SUPERCLASS=superclass $
                          ,TEST_METHODS=test_methods_array

       ; Initialize all of the guiaddons.
       self->INITIALIZE_GUIADDONS


     END ;PRO BST_INST_INIT_GUIADDONS


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize all of the guiaddons.
     ;
     ; DESCRIPTION
     ;   This will loop through all of the guiaddons, and call their
     ;   INITIALIZE methods.
     ; 
     ;   This will be called after all of the guiaddons have been
     ;   instantiated.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DISPATCH::INITIALIZE_GUIADDONS

       ; This calls the 'INITIALIZE' method on all of the guiaddons.
       self->BST_INST_DISPATCH::INITIALIZE_OBJECTS

       ; Enable the default guiaddons.
       SELF->ENABLE_DEFAULT

     END ;PRO BST_INST_GUIADDON_DISPATCH::INITIALIZE_GUIADDONS


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a guiaddon object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_DISPATCH::GET_GUIADDON, name_in

       RETURN, self->BST_INST_DISPATCH::GET_OBJECT(name_in, PREFIX='BST_INST_GUIADDON_')

     END ;FUNCTION BST_INST_GUIADDON_DISPATCH::GET_GUIADDON


;=======================================================================
;#######################################################################
;#######################################################################
;
; Methods to access the application.  To be used by the guiaddons.
;
;#######################################################################
;#######################################################################
;=======================================================================
     

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a model object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUIADDON_DISPATCH::GET_MODEL, name_in

       app = self->GET_APP()
       model_dispatch = app->GET_MODEL_DISPATCHER()

       RETURN, model_dispatch->GET_MODEL(name_in)

     END ;FUNCTION BST_INST_GUIADDON_DISPATCH::GET_MODEL



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Create the BST_INST_GUIADDON_DISPATCH object.
     ;
     ;-=================================================================
     PRO BST_INST_GUIADDON_DISPATCH__DEFINE

       bst_inst_guiaddon_dispatch = $
          { BST_INST_GUIADDON_DISPATCH $

            ,INHERITS BST_INST_DISPATCH $
          }
     END ;PRO BST_INST_GUIADDON_DISPATCH__DEFINE
