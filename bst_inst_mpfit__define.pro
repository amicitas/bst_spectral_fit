


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
;
; DATE:
;   2011-10-26
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   This object contains the interface to MPFIT for
;   <BST_SPECTRAL_FIT>
;
; DESCRIPTION:
;   This object allows a MPFIT to be used as the core fitting algorithm
;   for <BST_SPECTRAL_FIT>.  Here <MPFIT> is acessed through <MIR_MPFIT>
;   to allow object oriented programming.
;
;-======================================================================

     
     

     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::INIT, core, _REF_EXTRA=extra

       status = self.BST_INST_BASE::INIT(core, _STRICT_EXTRA=extra)
       
       self.input = MIR_HASH()
       self.results = MIR_HASH()

       RETURN, status
     END ;FUNCTION BST_INST_MPFIT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Return a blanck structure for the fit variable parameters
     ;   accepted by MPFIT and used by BST_INST_FIT.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::BLANK_PARINFO, VALUE=value $
                                             ,FIXED=fixed $
                                             ,LIMETED=limited $
                                             ,LIMITS=limits
       MIR_DEFAULT, value, 0D
       MIR_DEFAULT, fixed, 0
       MIR_DEFAULT, limited, [0,0]
       MIR_DEFAULT, limits, [0D,0D]


       parinfo = { $
                   value:value $
                   ,fixed:fixed $
                   ,limited:limited $
                   ,limits:limits $
                 }
       RETURN, parinfo

     END ;FUNCTION BST_INST_MPFIT::BLANK_PARINFO



     ;+=================================================================
     ; PURPOSE:
     ;   Create the array of independant fit variables and the
     ;   array of assosiated parameters from the models.
     ;
     ; DESCRIPTION:
     ;   MPFIT requires as inputs:
     ;    1. An array with the independant fit variables.
     ;    2. An array of structures with the parameters for each
     ;       fit variable.
     ;
     ;   This routines extracts these parameters from the models
     ;   and saves them into the object.
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::MODELS_TO_MPFIT

       self.CLEAR_INPUT

       ; Fill the fit array
       parinfo = (self.core).models.GET_PARAMETER_INFO()

       (self.input)['parinfo'] =  parinfo

     END ;PRO BST_INST_MPFIT::MODELS_TO_MPFIT

     

     ;+=================================================================
     ; PURPOSE:
     ;   Take the array of independant fit variables (which is what
     ;   is actually manipulated by the fitter) and update the models 
     ;   with these values.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::MPFIT_TO_MODELS

       ; Fill the fit array.
       self.EXTRACT_PARAMETER_ARRAY, (self.results)['parameters']

       ; Fill the sigma array.
       self.EXTRACT_SIGMA_ARRAY, (self.results)['sigma']

     END ;PRO BST_INST_MPFIT::MPFIT_TO_MODELS



     ;+=================================================================
     ; PURPOSE:
     ;   Take the array of independant fit variables (which is what
     ;   is actually manipulated by the fitter) and update the models 
     ;   with these values.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::EXTRACT_PARAMETER_ARRAY, parameter_array

       ; Fill the fit array
       (self.core).models.EXTRACT_PARAMETER_ARRAY, parameter_array

     END ;PRO BST_INST_MPFIT::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Take the array of errors in the fits and up pdate the models 
     ;   with these values.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::EXTRACT_SIGMA_ARRAY, sigma_array

       ; Fill the fit array
       (self.core).models.EXTRACT_SIGMA_ARRAY, sigma_array

     END ;PRO BST_INST_MPFIT::EXTRACT_SIGMA_ARRAY




     ;+=================================================================
     ; PURPOSE:
     ;   Load the spectrum from the spectrum function.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::LOAD_SPECTRUM

       (self.input)['spectrum'] = (self.core).spectrum.GET_SPECTRUM()

     END ;PRO BST_INST_MPFIT::LOAD_SPECTRUM



     ;+=================================================================
     ; PURPOSE:
     ;   Do the actual fit.
     ; 
     ;-=================================================================
     PRO BST_INST_MPFIT::FIT

       ; Do the fit
       ; ----------

       self.CLEAR_RESULTS

       ; Create the fit array from the model objects.
       ; the fit array is saved in a common variable.
       self.MODELS_TO_MPFIT


       ; Make a local copy of parinfo
       parinfo = (self.input)['parinfo']


       num_param = N_ELEMENTS(parinfo)
       num_free_param = TOTAL(~ parinfo.fixed)


       ; Load the spectrum
       self.LOAD_SPECTRUM


       IF (self.core).options_user.spectrum_weights_from_model THEN BEGIN
         (self.core).MESSAGE, 'Spectrum weights taken from model.', /DEBUG
       ENDIF


       ; Get the user set options:
       mpfit_ftol = (self.core).options_user.mpfit_ftol
       mpfit_xtol = (self.core).options_user.mpfit_xtol
       mpfit_gtol = (self.core).options_user.mpfit_gtol
       mpfit_maxiter = (self.core).options_user.mpfit_maxiter

       ; PRINT, 'mpfit_ftol', mpfit_ftol
       ; PRINT, 'mpfit_xtol', mpfit_xtol
       ; PRINT, 'mpfit_gtol', mpfit_gtol
       ; PRINT, 'mpfit_maxiter', mpfit_maxiter

       ; Ignore though if there were no free parameters.
       IF num_free_param GT 0 THEN BEGIN
         IF self.core.debug.timer THEN MIR_TIMER, 'MIR_MPFIT', /START
                
         ; Do the actual fit
         param_final = MIR_MPFIT('EVALUATE_RESIDUALS' $
                                 ,OBJECT_REFERENCE = self $
                                 
                                 ,PARINFO=parinfo $
                                 
                                 ,/AUTODERIVATIVE $
                                 ,FTOL=mpfit_ftol $
                                 ,XTOL=mpfit_xtol $
                                 ,GTOL=mpfit_gtol $
                                 ,MAXITER=mpfit_maxiter $
                                 
                                 ,BESTNORM=bestnorm $
                                 ,PERROR=perror $
                                 
                                 ,NITER=num_iterations $

                                 ,/QUIET $
                                 ,/NOCATCH $
                                 ,STATUS=status $
                                 ,ERRMSG=errmsg $
                                )
       
         IF self.core.debug.timer THEN MIR_TIMER, 'MIR_MPFIT', /STOP
         
         ; Display the fit status.
         status_message = MPCURVEFIT_STATUS(status)
         (self.core).MESSAGE, STRCOMPRESS(STRING('MPFIT status:', status, ' ', status_message))
         
         CASE 1 OF
           status LE 0: BEGIN
             ; Status equals 0 means that something bad happend.
             MESSAGE, 'MPFIT failure: '+errmsg
           END
           status GE 4: BEGIN
             (self.core).MESSAGE, STRCOMPRESS(STRING('MPFIT possible failure'))
           END
           ELSE:
         ENDCASE

       ENDIF ELSE BEGIN
         param_final = parinfo.value
         perror = DBLARR(num_param)
         bestnorm = 0D
         status = 0
         num_iterations = 0
       ENDELSE

       (self.core).MESSAGE, STRING(FORMAT='(a0, i0)', 'Free parameters: ', num_free_param)
       (self.core).MESSAGE, STRING(FORMAT='(a0, i0)', 'Number of iterations: ', num_iterations)       



       ; Do a final update of the model state.
       (self.results)['parameters'] = param_final
       (self.results)['sigma'] = perror

       self.MPFIT_TO_MODELS
       
       (self.results)['yfit'] = self.EVALUATE(param_final)
       (self.results)['weighted_residual'] = self.EVALUATE_RESIDUALS(param_final)
       (self.results)['perror'] = param_final
       
       chisq = TOTAL(((self.results)['weighted_residual'])^2)
       (self.results)['chisq'] = chisq
       (self.results)['reduced_chisq'] = chisq / (N_ELEMENTS((self.input)['spectrum'].y) - num_free_param)
       (self.results)['status'] = status
       (self.results)['time'] = SYSTIME(/JULIAN)


     END ;PRO BST_INST_MPFIT::FIT



     ;+=================================================================
     ; PURPOSE:
     ;   Clear the results. 
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::CLEAR_RESULTS

       self.results.REMOVE, /ALL
       ;self.results.yfit.REMOVE, /ALL
       ;self.results.parameters.REMOVE, /ALL
       ;self.results.sigma.REMOVE, /ALL
       ;self.results.chisq = 0D
       ;self.results.reduced_chisq = 0D
       ;self.results.status = 0
       ;self.results.time = 0D

     END ;PRO BST_INST_MPFIT::CLEAR_RESULTS



     ;+=================================================================
     ; PURPOSE:
     ;   Get a structure with the current results.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::GET_RESULTS

       ; Make sure that there is a valid fit saved.
       IF self.results.N_ELEMENTS() NE 0 THEN BEGIN
         results = { $
                     yfit:(self.results)['yfit'] $
                     ,weighted_residual:(self.results)['weighted_residual'] $
                     ,parameters:(self.results)['parameters'] $
                     ,sigma:(self.results)['sigma'] $
                     ,chisq:(self.results)['chisq'] $
                     ,reduced_chisq:(self.results)['reduced_chisq'] $
                     ,status:(self.results)['status'] $
                     ,time:(self.results)['time'] $
                   }
       ENDIF ELSE BEGIN
         results = {NULL}
       ENDELSE

       RETURN, results

     END ;FUNCTION BST_INST_MPFIT::GET_RESULTS



     ;+=================================================================
     ; PURPOSE:
     ;   Clear the input. 
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT::CLEAR_INPUT

       self.input.REMOVE, /ALL

     END ;PRO BST_INST_MPFIT::CLEAR_INPUT



     ;+=================================================================
     ; PURPOSE:
     ;   This is where we actually compute residuals from the fit 
     ;   parameters. 
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::EVALUATE_RESIDUALS, fit_array

       IF (self.core).options_user.spectrum_weights_from_model THEN BEGIN
         RETURN, self.EVALUATE_RESIDUALS_WEIGHTS_FROM_MODEL(fit_array)
       ENDIF ELSE BEGIN
         RETURN, self.EVALUATE_RESIDUALS_WEIGHTS_FROM_SPECTRUM(fit_array)
       ENDELSE

     END ;PRO BST_INST_FIT_MPFIT::EVALUATE_RESIDUALS



     ;+=================================================================
     ; PURPOSE:
     ;   This is where we actually compute residuals from the fit 
     ;   parameters. 
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::EVALUATE_RESIDUALS_WEIGHTS_FROM_SPECTRUM, fit_array

       ; Evaluate the models.
       yfit = self.EVALUATE(fit_array)

       ; Evaluate the residuals.
       residual = ((self.input)['spectrum'].y - yfit)*SQRT((self.input)['spectrum'].weight)

       RETURN, residual

     END ;PRO BST_INST_FIT_MPFIT::EVALUATE_RESIDUALS_WEIGHTS_FROM_SPECTRUM



     ;+=================================================================
     ; PURPOSE:
     ;   This is where we actually compute residuals from the fit 
     ;   parameters. 
     ;
     ;   Here I am going to try to use the fit restults for the 
     ;   weighting instead of the raw spectrum.  For a well behaved 
     ;   model this allows me to more accurately get the weights when
     ;   the number of photons is very small.
     ;
     ;   Note;  Here I am still assuming gaussian weighting of the 
     ;          spectrum, even if the number of counts is very small
     ;          In this case weighting is really poisson distributed,
     ;          and the value I return here is incorrect.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::EVALUATE_RESIDUALS_WEIGHTS_FROM_MODEL, fit_array
       ; Evaluate the models.
       yfit = self.EVALUATE(fit_array)

       w_negative = WHERE(yfit LT 0, count_negative)
       IF count_negative GT 0 THEN BEGIN
         MESSAGE, /WARNING, 'Weights cannot be taken from model: Negative values in the spectrum.'
         yfit[w_negative] = 0.0
       ENDIF

       sigma = SQRT(yfit/(self.input)['spectrum'].photons_per_count)
       w_zero = WHERE(sigma EQ 0.0, count_zero)
       IF count_zero GT 0 THEN BEGIN
         sigma[w_zero] = 1d/(self.input)['spectrum'].photons_per_count
       ENDIF


       ; Include any sources of noise that are not from photon statistics.
       sigma = SQRT(sigma^2 + ((self.input)['spectrum'].sigma_dark)^2)


       residual = ((self.input)['spectrum'].y - yfit)/sigma


       RETURN, residual

     END ;PRO BST_INST_FIT_MPFIT::EVALUATE_RESIDUALS_WEIGHTS_FROM_MODEL



     ;+=================================================================
     ; PURPOSE:
     ;   This is where we actually compute residuals from the fit 
     ;   parameters. 
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MPFIT::EVALUATE, fit_array


       ; Copy the fit array to the model objects
       self.EXTRACT_PARAMETER_ARRAY, fit_array


       ; Evaluate any constraints.
       (self.core).models.EVALUATE_CONSTRAINTS


       ; Evaluate the models.
       yfit = (self.core).models.EVALUATE((self.input)['spectrum'].x)


       RETURN, yfit

     END ;PRO BST_INST_FIT_MPFIT::EVALUATE



     ;+=================================================================
     ;
     ; Create the BST_INST_PRFOFILES object.
     ;
     ;-=================================================================
     PRO BST_INST_MPFIT__DEFINE

       bst_inst_mpfit = $
          { BST_INST_MPFIT $

            ,input:OBJ_NEW() $
            ,results:OBJ_NEW() $

            ,INHERITS BST_INST_BASE $
          }

     END ;PRO BST_INST_MPFIT__DEFINE
