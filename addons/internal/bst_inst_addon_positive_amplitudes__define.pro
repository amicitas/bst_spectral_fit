
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT> to control the POSITIVE_AMPLITUDES model.
;
; DESCRIPTION:
;   
;
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::SET_FLAGS

       self->BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.default = 1

       self.use_tab = 0
       self.use_control = 0

       self.add_to_menu = 0

     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::SET_FLAGS


     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the addon.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::INITIALIZE

       self->BST_INST_ADDON::INITIALIZE

       self.model = (self.dispatcher)->GET_MODEL('BST_INST_MODEL_POSITIVE_AMPLITUDES')

     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::INITIALIZE




; ==============================================================================
; ==============================================================================
; ######################################################################
;
; GUI COMPOUND WIDGET ROUTINES.
;   Here we build a compound widget that will be used by the addon
;   object.
;
;   
;
; ######################################################################
; ==============================================================================
; ==============================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Build the gui.
     ;
     ; DESCRIPTION:
     ;   Since all that this addon does is to control some menu 
     ;   options, we reimplement the BUILD_GUI method to just
     ;   create the menu options.
     ;-=================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::BUILD_GUI

       fitter_gui = self.dispatcher->GET_GUI()

       ; Turn off GUI updating.
       fitter_gui->SET_GUI_UPDATE, 0, CURRENT_STATUS=update_old

       self->GUI_DEFINE

       ; Restore the update state
       fitter_gui->SET_GUI_UPDATE, update_old


     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::BUILD_GUI



     ;+=========================================================================
     ; PURPOSE:
     ;   Build the gui components for the [BST_INST_ADDON_POSITIVE_AMPLITUDES::] 
     ;   addon.
     ;
     ; 
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::GUI_DEFINE
  

       ; ---------------------------------------------------------------
       ; Set up menu for this addon.
       fitter_gui = self.dispatcher->GET_GUI()
       ui_menu_options =  fitter_gui->GET_GUI_ID(/MENU_OPTIONS)


       ui_button_positive_amplitudes = WIDGET_BUTTON(ui_menu_options $
                                                     ,UNAME='ui_button_positive_amplitudes' $
                                                     ,VALUE='Positive Amplitudes' $
                                                     ,/CHECKED_MENU $
                                                     ,EVENT_PRO='WIDGET_OBJECT_EVENT' $
                                                     ,UVALUE=self $
                                                    )


       ; Store all of the accesable widgit id's here.
       id_struct = { $
                     ui_button_positive_amplitudes:ui_button_positive_amplitudes $
                   }


       ; Save the id structure.
       self->SET_ID_STRUCT, id_struct


     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::GUI_DEFINE
     


     ;+=========================================================================
     ;
     ; Here we have the event manager for the addon gui.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::EVENT_HANDLER, event


       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self->MESSAGE, 'Error handing GUI event.', /ERROR
       ENDIF 


       ; First call the superclass method
       self->BST_INST_ADDON::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       id = self->GET_ID_STRUCT()
       fitter_core = (self.dispatcher)->GET_CORE()

       ; Start main event loop
       CASE event.id OF

         id.ui_button_positive_amplitudes: BEGIN
           value = ~ WIDGET_INFO(event.id, /BUTTON_SET)
           WIDGET_CONTROL, event.id, SET_BUTTON=value
           self.model->SET_USE, value
         END

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE



     END ;PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::EVENT_HANDLER



     ;+=========================================================================
     ;
     ; Update the gui from the current object parameters
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::UPDATE_GUI

       id = self->GET_ID_STRUCT()

       WIDGET_CONTROL, id.ui_button_positive_amplitudes, SET_BUTTON=self.model->USE()

     END ;PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::UPDATE_GUI



     ;+=========================================================================
     ; PURPOSE:
     ;   Remove the menu item.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::REMOVE_MENU_ITEM

       id = self->GET_ID_STRUCT()

       IF WIDGET_INFO(id.ui_button_positive_amplitudes, /VALID_ID) THEN BEGIN
         WIDGET_DESTROY, id.ui_button_positive_amplitudes
       ENDIF

     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::REMOVE_MENU_ITEM




; ==============================================================================
; ==============================================================================
; ##############################################################################
;
; OBJECT DEFINITION
;
; ##############################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::CLEANUP

       self->REMOVE_MENU_ITEM

     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES::CLEANUP


     ;+=========================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON]
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_POSITIVE_AMPLITUDES__DEFINE

       struct = { BST_INST_ADDON_POSITIVE_AMPLITUDES $
                  
                  ,model:OBJ_NEW() $

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_POSITIVE_AMPLITUDES__DEFINE
