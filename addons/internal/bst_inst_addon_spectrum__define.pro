
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-04
;
; PURPOSE:
;   An base addon for <BST_SPECTRAL_FIT> to allow spectra to be loaded into
;   the fitter and to be saved in the save file.
;
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_SPECTRUM::SET_FLAGS

       self->BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       add_to_menu = 0

     END ; PRO BST_INST_ADDON_SPECTRUM::SET_FLAGS



     ;+=========================================================================
     ;
     ; Return a structure with the default parameters for the
     ; USER SPECTRA addon.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_SPECTRUM::RESET, _NULL=_null
           
       self.ptr_spectrum = PTR_NEW()
 
       self->BST_INST_ADDON::RESET

     END ; PRO BST_INST_ADDON_SPECTRUM::RESET



     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the state of addon.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_SPECTRUM::GET_STATE
       
       state = self->BST_INST_ADDON::GET_STATE()

       state = CREATE_STRUCT(state, {spectrum:self->GET_SPECTRUM()})

       RETURN, state

     END ; FUNCTION BST_INST_ADDON_SPECTRUM::GET_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Load a save structure for this addon.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_SPECTRUM::LOAD_STATE, state

       self->BST_INST_ADDON::LOAD_STATE, state
       self->SET_SPECTRUM, state.spectrum

     END ; PRO BST_INST_ADDON_SPECTRUM::LOAD_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_SPECTRUM::GET_SPECTRUM

       RETURN, *(self.ptr_spectrum)

     END ; FUNCTION BST_INST_ADDON_SPECTRUM::GET_SPECTRUM



     ;+=========================================================================
     ; PURPOSE:
     ;   Load the addon parameters from a structure.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_SPECTRUM::SET_SPECTRUM, spectrum

       self.ptr_spectrum = PTR_NEW(spectrum)

     END ; PRO BST_INST_ADDON_SPECTRUM::SET_SPECTRUM



; ==============================================================================
; ==============================================================================
; ##############################################################################
;
; BEGIN METHODS TO BE CALLED BY THE FITTER
;
; ##############################################################################
; ==============================================================================
; ==============================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Save the spectrum to be used for this fit.  This needs to be
     ;   done after the spectrum has been loaded.
     ;
     ;   See <BST_INST_ADDON::PREFIT>.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_SPECTRUM::PREFIT

       core = self.dispatcher->GET_CORE()

       self->SET_SPECTRUM, core.spectrum->GET_SPECTRUM()

     END ; PRO BST_INST_ADDON_SPECTRUM::PREFIT



; ==============================================================================
; ==============================================================================
; ##############################################################################
;
; OBJECT DEFINITION
;
; ##############################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ;
     ; Define the object as a subclass of <BST_INST_ADDON>
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_SPECTRUM__DEFINE

       struct = { BST_INST_ADDON_SPECTRUM $

                  ,ptr_spectrum:PTR_NEW() $ 

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_SPECTRUM__DEFINE
