


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-01
;
; PURPOSE:
;   Save the state of the fitter.
; 
;
;-======================================================================






     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::SET_FLAGS

       self->BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.title = 'Fit Saver'

     END ; PRO BST_INST_ADDON_FIT_SAVER::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Take any actions when instantiated.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_FIT_SAVER::INIT, dispatcher
       
       status = self->BST_INST_ADDON::INIT(dispatcher)

       self.state_list = OBJ_NEW('MIR_LIST_IDL7')

       RETURN, status

     END ; FUNCTION BST_INST_ADDON_FIT_SAVER::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::CLEANUP

       IF OBJ_VALID(self.widget) THEN BEGIN
         self->REMOVE_WIDGET
       ENDIF

       IF OBJ_VALID(self.state_list) THEN BEGIN
         self.state_list->DESTROY, /HEAP_FREE
       ENDIF

       self->BST_INST_ADDON::CLEANUP

     END ; PRO BST_INST_ADDON_FIT_SAVER::CLEANUP



; ======================================================================
; ======================================================================
; ######################################################################
;
; FIT SAVER METHODS
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Save the current state of the fitter.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::SAVE_STATE

       app = self.dispatcher->GET_APP()

       state = app->GET_STATE()

       IF self.options.anonymous THEN BEGIN
         state = STRUCT_ANONYMOUS(state)
       ENDIF
       
       self.state_list->APPEND, state

     END ; PRO BST_INST_ADDON_FIT_SAVER::SAVE_STATE


     ;+=================================================================
     ; PURPOSE:
     ;   Take actions immidiatly after the fit loop.
     ;
     ;   If self.options.automatic == 1 then this will automatically
     ;   save the state.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::POSTFIT

       IF self.options.automatic THEN BEGIN
         self->SAVE_STATE
       ENDIF

     END ; PRO BST_INST_ADDON_FIT_SAVER::POSTFIT


     ;+=================================================================
     ; PURPOSE:
     ;   Remove the contents of the state list.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::RESET, _REF_EXTRA=extra

       IF OBJ_VALID(self.state_list) THEN BEGIN
         self.state_list->REMOVE, /ALL
       ENDIF

       self.options.automatic = 0

       ; By default always save the state using anonymous structures.
       self.options.anonymous = 1
       
     END ; PRO BST_INST_ADDON_FIT_SAVER::RESET


     ;+=================================================================
     ; PURPOSE:
     ;   Set the addon to autimatically save fits or not.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::SET_AUTOMATIC, automatic

       MIR_DEFAULT, automatic, 1

       self.options.automatic = automatic

     END ; PRO BST_INST_ADDON_FIT_SAVER::SET_AUTOMATIC


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a copy of the state list.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_FIT_SAVER::GET_STATE_LIST, NO_COPY=no_copy

       IF KEYWORD_SET(no_copy) THEN BEGIN
         RETURN, self.state_list
       ENDIF ELSE BEGIN
         RETURN, self.state_list->GET_COPY(/RECURSIVE)
       ENDELSE

     END ; FUNCTION BST_INST_ADDON_FIT_SAVER::GET_STATE_LIST


     ;+=================================================================
     ; PURPOSE:
     ;   Retrive a copy of the state list converted into a structure.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_FIT_SAVER::GET_STATE_STRUCTURE, ANONYMOUS=anonymous

       IF self.state_list->N_ELEMENTS() EQ 0 THEN BEGIN
         MESSAGE, 'No states are saved.'
       ENDIF

       ; Convert the statelist to a structure.
       state_structure = self.state_list->TO_STRUCTURE()

       ; Annonymize the state structure.
       ;
       ; If the anonymous option was set while saving the sates, 
       ; then this is not necessary.
       IF KEYWORD_SET(anonymous) THEN BEGIN
         state_structure = STRUCT_ANONYMOUS(state_structure)
       ENDIF
         
         

       RETURN, state_structure

     END ; FUNCTION BST_INST_ADDON_FIT_SAVER::GET_STATE_STRUCTURE


     ;+=================================================================
     ; PURPOSE:
     ;   Save the state list to a file
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::WRITE_STATE_LIST, filename

       IF self.state_list->N_ELEMENTS() EQ 0 THEN BEGIN
         MESSAGE, 'No states are saved.'
       ENDIF

       ; Get a the statelist as a structure
       state_structure = self->GET_STATE_STRUCTURE()

       ; Finally save the final structure.
       MIR_SAVE_OBJECT, state_structure, FILENAME=filename



       self->MESSAGE, 'Results saved to: ' + filename, /STATUS


     END ; PRO BST_INST_ADDON_FIT_SAVER::WRITE_STATE_LIST


     ;+=================================================================
     ; PURPOSE:
     ;   Save the state list to a file
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER::SAVE_TO_FILE

       IF self.state_list->N_ELEMENTS() EQ 0 THEN BEGIN
         MESSAGE, 'No states are saved.'
       ENDIF


       fitter_gui = self.dispatcher->GET_GUI()

       ; Generate a probable filename.
       filename = self->GET_SAVE_FILENAME()

       ; Let the user pick the file in which to save.
       filename = DIALOG_PICKFILE(/WRITE $
                                  ,FILE=filename $
                                  ,PATH=self.dispatcher->GET_CURRENT_PATH() $
                                  ,GET_PATH=new_path $
                                  ,FILTER=['*.bst_inst_result;*.sav','*.bst_inst_result','*.sav'] $
                                  ,/OVERWRITE_PROMPT $
                                  ,TITLE='Chose file to save the results in.' $
                                  ,DIALOG_PARENT=fitter_gui->GET_GUI_ID(/BASE_MAIN))

       ; Check that a file was picked.
       IF filename EQ '' THEN RETURN

       ; Save the new path.
       IF new_path NE '' THEN BEGIN
         self.dispatcher->SET_CURRENT_PATH, new_path
       ENDIF

       ; Save the state list.
       self->WRITE_STATE_LIST, filename

     END ; PRO BST_INST_ADDON_FIT_SAVER::SAVE_TO_FILE


     ;+=================================================================
     ; PURPOSE:
     ;   Get a filename for the save file.
     ;
     ; DESCRIPTION:
     ;   This is bascially the same as 
     ;   [BST_INST_FIT_GUI::GET_SAVE_FILENAME()] except that the
     ;   file extention and default is different.
     ;
     ;   It is important that a different extention is used as
     ;   these save files cannot be loaded to restore the fitter state.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_FIT_SAVER::GET_SAVE_FILENAME

       ; First get the prefix from the addons.
       filename = self.dispatcher->GET_FILENAME_PREFIX()


       ; If no prefix was found generate a default.
       MIR_DEFAULT, filename, 'saved_fit_list'

       ; Add a file extension.
       filename += '.bst_inst_result'

       RETURN, filename
       

     END ; FUNCTION BST_INST_ADDON_FIT_SAVER::GET_SAVE_FILENAME


     ;+=================================================================
     ; PURPOSE:
     ;   Return the addon options dictionary.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_FIT_SAVER::GET_OPTIONS

       RETURN, self.options

     END ; FUNCTION BST_INST_ADDON_FIT_SAVER::GET_OPTIONS

     
; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_ADDON]
     ;
     ; PROGRAMING NOTES:
     ;
     ;   Property definitons:
     ;
     ;   options.automatic = bool
     ;       If set, the fitter state will automatically be saved
     ;       after each fit.  This will be done with [::POSTFIT] is
     ;       called.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_FIT_SAVER__DEFINE

       struct = { BST_INST_ADDON_FIT_SAVER $
                           
                  ,options:{BST_INST_ADDON_FIT_SAVER__OPTIONS $
                            ,automatic:0 $
                            ,anonymous:0 $
                           } $

                  ,state_list:OBJ_NEW() $

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_FIT_SAVER__DEFINE
