



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-10
;
; PURPOSE:
;   Methods for printing the results from [BST_INST_FIT::].
; 
;   These methods are part of the [BST_INST_FIT::] object
;
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PRINT::SET_FLAGS

       self->BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.title = 'Print Options'

     END ; PRO BST_INST_ADDON_PRINT::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Take any actions when instantiated.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_PRINT::INIT, dispatcher
       
       status = self->BST_INST_ADDON::INIT(dispatcher)

       RETURN, status

     END ; FUNCTION BST_INST_ADDON_PRINT::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action immidiatly after the fit is complete.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PRINT::POSTFIT

       self->PRINT

     END ; PRO BST_INST_ADDON_PRINT::POSTFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Print out some results.
     ;
     ; DESCRIPTION:
     ;   IMPORTANT: When a scaled peak is used in a profile, the 
     ;              centroid is taken as being at the position of the
     ;              scaled peak. All other gaussians are not considered
     ;              for the centroid.
     ;-=================================================================
     PRO BST_INST_ADDON_PRINT::PRINT


       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         self->MESSAGE, 'Error printing results to terminal.', /ERROR
         RETURN
       ENDIF



       ; Get a reference to the fitter core.
       fitter_core = (self.dispatcher)->GET_CORE()

       ; First print out information from the models.
       models = fitter_core.models
       models->PRINT


       ; Now print out the basic fit information.
       results = fitter_core.fitter->GET_RESULTS()

       ; Print out fit information.
       IF (results.reduced_chisq GE 1E4) THEN BEGIN
         fmt = '(1x, a0, e10.2, 2x, a0, e10.2, a0)'
       ENDIF ELSE BEGIN
         fmt = '(1x, a0, f10.2, 2x, a0, f10.3, a0)'
       ENDELSE

       self->MESSAGE, STRING('Chisq:', results.chisq $
                                        ,'(Reduced Chisq:', results.reduced_chisq, ')' $
                                        ,FORMAT=fmt)


       self->PRINT_PROFILE

     END ;PRO BST_INST_ADDON_PRINT::PRINT



     ;+=================================================================
     ; PURPOSE:
     ;   Print out some information on any profiles.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PRINT::PRINT_PROFILE



       ; Define the formats.
       fmt = '(2X, a0,2X, i3, 2(2X, a0, f8.3), (2X, a0, e10.3), (2X, a0, f8.3), (2X, a0, e10.3))'
       fmt_fwpm = '(4x,a0,1x,f3.0,a0,1x,f8.3)'

       fitter_core = (self.dispatcher)->GET_CORE()

       ; Get a reference to the profiles object.
       profiles_obj = fitter_core.profiles


       profile_num_list = profiles_obj->GET_PROFILE_NUMBER_LIST()
       num_profiles = profile_num_list->N_ELEMENTS()


       IF num_profiles EQ 0 THEN RETURN

       profile_num_array = profile_num_list->TO_ARRAY()
       profile_num_list->DESTROY

       where_pos = WHERE(profile_num_array GT 0, num_pos_profiles)
       IF num_pos_profiles EQ 0 THEN RETURN


       ;Loop over the profiles
       FOR ii=0, num_pos_profiles-1 DO BEGIN


         ; Exclude background and reference profiles.
         profile_num = profile_num_array[where_pos[ii]]

         ; Get the profile object.
         profile = profiles_obj->GET_PROFILE(profile_num)

         ; Calulate a few things.
         centroid = profile->CENTROID()
         intensity = profile->INTENSITY()

         CATCH, error
         IF error EQ 0 THEN BEGIN
           ; This may fail in certain cases, so check for errors.
           fwhm = profile->FULL_WIDTH(0.5, LOCATION_MAX=location_max, VALUE_MAX=value_max)
         ENDIF ELSE BEGIN
           CATCH, /CANCEL
           fwhm = !values.d_nan
           location_max = !values.d_nan
           value_max = !values.d_nan
         ENDELSE
         
         self->MESSAGE, STRING( $
                                FORMAT=fmt, 'Profile', profile_num $
                                ,'Centroid: ', centroid $
                                ,'FWHM:', fwhm $
                                ,'Intensity:', intensity $
                                ,'Location Max:', location_max $
                                ,'Value Max:', value_max $
                              )

         
       ENDFOR


     END ; PRO BST_INST_ADDON_PRINT::PRINT_PROFILE



     ;+=================================================================
     ;
     ; Create the BST_INST_ADDON_PRINT object.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PRINT__DEFINE

       bst_inst_print = $
          { BST_INST_ADDON_PRINT $

            ,INHERITS BST_INST_ADDON $
          }

     END ;PRO BST_INST_ADDON_PRINT__DEFINE
