


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An internal addon for <BST_SPECTRAL_FIT>
;
;   This addon controls the SCALEDIAN models.
; 
;
;-======================================================================




; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT ROUTINES
;   Routines to control the object and object data.
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::SET_FLAGS

       self->BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.internal = 1

       self.title = 'Plot Options'

     END ; PRO BST_INST_ADDON_PLOT::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize default plotting options.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::INITIALIZE_OPTIONS

       self.options.thesis_yfactor = 3L

       self.options.yrange = [0D, 0D]
       self.options.residual_yrange = [0D, 0D]

       self.options.xrange = [0D,0D]
       self.options.wavelengthrange = [0D,0D]

       self.options.normalize = 0


       self.options.plot_vs_wavelength = 0
       self.options.plot_overall_fit = 1
       self.options.plot_residual = 1
       self.options.plot_background = 1
       self.options.plot_profiles = 0
       self.options.plot_gaussians = 0
       self.options.plot_profile_locations = 0
       self.options.plot_error_bars = 0
       self.options.plot_addons = 0
       self.options.plot_models = 0

       self.options.weighted_residual = 1

       self.options.show_grid = 0

     END ;PRO BST_INST_ADDON_PLOT::INITIALIZE_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize plot addon parameters.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::INITIALIZE_PARAM

       self.param.window = -1L
       self.param.pdf = 0
       self.param.postscript = 0


     END ;PRO BST_INST_ADDON_PLOT::INITIALIZE_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Take any actions when instantiated.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_PLOT::INIT, dispatcher
       
       status = self->BST_INST_ADDON::INIT(dispatcher)

       self->INITIALIZE_OPTIONS
       self->INITIALIZE_PARAM

       RETURN, status

     END ; FUNCTION BST_INST_ADDON_PLOT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Set the plotting options.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::SET_OPTIONS, struct

       options = TEMPORARY(self.options)
       STRUCT_ASSIGN, struct, options, /NOZERO
       self.options = TEMPORARY(options)

     END ;PRO BST_INST_ADDON_PLOT::SET_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Set the plotting parameters.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::SET_PARAM

       param = TEMPORARY(self.param)
       STRUCT_ASSIGN, struct, param, /NOZERO
       self.param = TEMPORARY(param)

     END ;PRO BST_INST_ADDON_PLOT::SET_PARAM




; ======================================================================
; ======================================================================
; ######################################################################
;
; PLOTTING ROUTINES
;   Routines that handle the actual plotting.
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action immidiatly after the fit is complete.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::POSTFIT

       self->PLOT

     END ; PRO BST_INST_ADDON_PLOT::POSTFIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Plot the fit
     ;
     ; DESCRIPTION:
     ;   A fit must have been completed before this routine is called.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::PLOT, SPECTRUM_ONLY=spectrum_only $
                                    ,PDF=key_pdf

       ; PRINT, 'BST_INST_ADDON_PLOT::PLOT - PDF', KEYWORD_SET(key_pdf)
       
       ; We expect that the fit can been completed before this
       ; routine is called.  If not it will fail.

       ; TEMPORARY since I don't know why using the pdf button does not work.
       ; key_pdf = 1

       fitter_core = (self.dispatcher)->GET_CORE()

       ; Copy for simplicity
       postscript = self.param.postscript
       pdf = self.param.pdf
       options = self.options
       path = fitter_core->GET_CURRENT_PATH()
       

       IF options.reload_spectrum OR KEYWORD_SET(spectrum_only) THEN BEGIN
         ; If only the spectrum was requested, then we have no reason
         ; to assume that the spectrum is already loaded.
         fitter_core.spectrum->LOAD_SPECTRUM
       ENDIF

       IF KEYWORD_SET(spectrum_only) THEN BEGIN
         ; Turn of everything but the spectrum.
         options.plot_overall_fit = 0
         options.plot_residual = 0
         options.plot_background = 0
         options.plot_profiles = 0
         options.plot_gaussians = 0
         options.plot_profile_locations = 0
         options.plot_models = 0
         options.plot_addons = 0
       ENDIF

       IF ISA(key_pdf) THEN BEGIN
         pdf = KEYWORD_SET(key_pdf)
       ENDIF

       ; Check to see if we want to output to postscript or screen.
       IF ~ postscript AND ~ pdf THEN BEGIN
         
         WINDOWSET, self.param.window, WINDOW=window, /CHECK

         ; Save the actual window in case it was different from the one given.
         self.param.window = window
       ENDIF

       
       ; Retrive the spectrum.
       spectrum = fitter_core.spectrum->GET_SPECTRUM()

       ; Check if plot_vs_wavelength was set.
       IF options.plot_vs_wavelength THEN BEGIN
         IF ~ HAS_TAG(spectrum, 'WAVELENGTH') THEN BEGIN
           options.plot_vs_wavelength = 0
         ENDIF
       ENDIF

       IF options.plot_vs_wavelength THEN BEGIN
         x_spectrum = spectrum.wavelength
         xtitle = 'Wavelength'
         IF options.wavelengthrange[0] NE options.wavelengthrange[1] THEN BEGIN
           xrange = options.wavelengthrange
         ENDIF
       ENDIF ELSE BEGIN
         x_spectrum = spectrum.x
         xtitle = 'Channel number'
         IF options.xrange[0] NE options.xrange[1] THEN BEGIN
           xrange = options.xrange
         ENDIF
       ENDELSE
  
       IF ~ISA(xrange) THEN BEGIN
         xmax = MAX(x_spectrum, MIN=xmin)
         xrange = [xmin,xmax]
       ENDIF

; Temporary, my plots are currently backwards when plotting from CERFIT.
;IF options.plot_vs_wavelength THEN BEGIN
;  xrange = [xmax,xmin]
;ENDIF


; Temporary, I need to make some quick plots.
;IF options.plot_vs_wavelength THEN BEGIN
;  PRINT, 'xrange: ', xmin, xmax
;  xrange = [6511.6478, 6575.1892]
;ENDIF

       IF options.normalize THEN BEGIN
         scale = 1D/MAX(spectrum.y)
       ENDIF ELSE BEGIN
         scale = 1.0
       ENDELSE


       IF options.yrange[1] GT options.yrange[0] THEN BEGIN
         yrange = options.yrange
       ENDIF ELSE BEGIN
         extra_range = 0.1
         yrange = [0D, 0D]
         yrange[0] = MIN(spectrum.y) < 0
         yrange[1] = MAX(spectrum.y)
         yrange_span = yrange[1] - yrange[0]         
         yrange[0] -= yrange_span * extra_range
         yrange[1] += yrange_span * extra_range

         IF yrange_span EQ 0 THEN BEGIN
           yrange = [-0.1, 1.0]
         ENDIF

       ENDELSE

       ; Setup for show_grid option
       IF options.show_grid THEN BEGIN
         xticklen = 1.0
         yticklen = 1.0
         yminor = 1
         xminor = 1
         xmajor = 20
         ymajor = 10
         ygridstyle = 1
         xgridstyle = 1
         ticklen_inches = 0
       ENDIF ELSE BEGIN
         xticklen = 0
         yticklen = 0
         yminor = 0
         xminor = 0
         xmajor = 0
         ymajor = 0
         ygridstyle = 0
         xgridstyle = 0
         ticklen_inches = -1
       ENDELSE

       ; Create a new PLOTLIST object
       plotlist = OBJ_NEW('MIR_PLOTLIST')

       ; Setup the defaults
       plotlist->SET_DEFAULTS, {xrange:xrange $
                                ,decomposed:1 $
                                ,thesis_size:1 $
                                ,thesis_yfactor:options.thesis_yfactor $
                                ,pdf:pdf $
                                ,postscript:postscript $
                                ,path:path $
                               }

       ; First plot the spectrum
       plotlist->APPEND, {x:x_spectrum $
                          ,y:spectrum.y * scale $
                          ,xstyle:1 $
                          ,yrange:yrange * scale $
                          ,color:MIR_COLOR('BLACK') $
                          ,psym:0 $
                          ;,thick:2 $
                          ,plotnum:0 $
                          ,plot_ysize:0.75 $
                          ,ytitle:'Intensity' $
                          ,xtitle:xtitle $
                          ,xticklen:xticklen $
                          ,yticklen:yticklen $
                          ,ticklen_inches:ticklen_inches $
                          ,xminor:xminor $
                          ,yminor:yminor $
                          ,xmajor:xmajor $
                          ,ymajor:ymajor $
                          ,ygridstyle:ygridstyle $
                          ,xgridstyle:xgridstyle $
                         }

       IF options.plot_overall_fit THEN BEGIN
         fit = fitter_core.fitter->GET_RESULTS()

         ; Now overlay the fit result
         plotlist->APPEND, {x:x_spectrum $
                            ,y:fit.yfit * scale $
                            ,color:MIR_COLOR('PRINT_RED') $
                            ;,thick:2 $
                            ,psym:0 $
                            ,oplot:1 $
                            ,plotnum:0 $
                           }
       ENDIF


       IF options.plot_gaussians THEN BEGIN
         self->ADD_PROFILES, plotlist, 0, spectrum.x, x_spectrum, scale, /GAUSSIANS
       ENDIF


       IF options.plot_background THEN BEGIN
         self->ADD_BACKGROUND, plotlist, 0, spectrum.x, x_spectrum, scale
       ENDIF


       IF options.plot_profiles THEN BEGIN
         self->ADD_PROFILES, plotlist, 0, spectrum.x, x_spectrum, scale, /PROFILES
       ENDIF


       IF options.plot_profile_locations THEN BEGIN
         self->ADD_PROFILES, plotlist, 0, spectrum.x, x_spectrum, scale, /LOCATIONS
       ENDIF


       IF options.plot_addons THEN BEGIN
         self->ADD_ADDONS, plotlist, 0, spectrum.x, x_spectrum, scale
       ENDIF


       IF options.plot_models THEN BEGIN
         self->ADD_MODELS, plotlist, 0, spectrum.x, x_spectrum, scale
       ENDIF


       IF options.plot_residual THEN BEGIN

         ; The fit was probably already loaded.
         IF ~ ISA(fit) THEN BEGIN
           fit = fitter_core.fitter->GET_RESULTS()
         ENDIF

         weighted_residual = fit.weighted_residual
         real_residual = (spectrum.y - fit.yfit)

         IF options.weighted_residual THEN BEGIN
           residual = weighted_residual
           ytitle = 'Weighted residual'
         ENDIF ELSE BEGIN
           residual = real_residual
           ytitle = 'Residual'
         ENDELSE

         IF options.residual_yrange[1] GT options.residual_yrange[0] THEN BEGIN
           residual_yrange = options.residual_yrange
         ENDIF ELSE BEGIN
           extra_range = 0.0
           residual_yrange = [0D, 0D]
           ; The residual yrange should always be symmetric.
           residual_yrange[1] = ABS(MAX(residual, /ABS))
           residual_yrange[0] = -1 * residual_yrange[1]
           yrange_span = residual_yrange[1] - residual_yrange[0]
           residual_yrange[0] -= yrange_span * extra_range
           residual_yrange[1] += yrange_span * extra_range
         ENDELSE


         ; Now add the residuals plot
         plotlist->APPEND, {x:x_spectrum $
                            ,y:residual $
                            ,yrange:residual_yrange $
                            ,xstyle:1+4 $
                            ,ymajor:4 $
                            ,yminor:2 $
                            ,color:MIR_COLOR('BLACK') $
                            ,psym:1 $
                            ,symsize:0.5 $
                            ,plotnum:1 $
                            ,plot_ysize:0.25 $
                            ,suppress_margin_top:1 $
                            ,ytitle:ytitle $
                           }

         ; Add the x axis as a dotted line.
         plotlist->APPEND, {x:[MAX(x_spectrum), MIN(x_spectrum)] $
                            ,y:[0.0, 0.0] $
                            ,plots:1 $
                            ,data:1 $
                            ,linestyle:1 $
                            ,plotnum:1 $
                           }

       ENDIF

       ; Do the actuall plotting.
       SUPERPLOT_MULTI, plotlist, GET_PATH=path, /DEBUG

       OBJ_DESTROY, plotlist

       IF postscript OR pdf THEN BEGIN
         ;IF path NE '' THEN BEGIN
         ;  ; Save the last path used.
         ;  self.dispatcher->SET_CURRENT_PATH, path
         ;ENDIF
       ENDIF ELSE BEGIN
         WINDOWSET, /RESTORE
       ENDELSE
       
     END ;PRO BST_INST_ADDON_PLOT::FIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Add the profiles to the plotlist
     ;
     ; TO DO:
     ;   Currently when evaluating the profiles, I am always using
     ;   the /CUTOFF option.  This should really come from the user 
     ;   option.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::ADD_PROFILES, plotlist $
                                            ,plotnum $
                                            ,x_values $
                                            ,x_labels $
                                            ,scale $
                                            ,PROFILES=profiles $
                                            ,GAUSSIANS=gaussians $
                                            ,LOCATIONS=locations


       cutoff = 0d

       fitter_core = (self.dispatcher)->GET_CORE()

       ; Get a reference to the profiles object.
       profiles_obj = fitter_core.profiles


       profile_num_list = profiles_obj->GET_PROFILE_NUMBER_LIST()
       num_profiles = profile_num_list->N_ELEMENTS()

       IF num_profiles EQ 0 THEN RETURN

       profile_num_array = profile_num_list->TO_ARRAY()
       profile_num_list->DESTROY

       where_pos = WHERE(profile_num_array GT 0, num_pos_profiles)
       IF num_pos_profiles EQ 0 THEN RETURN



       ; If we there are only a few profiles then set the colors manually,
       ; otherwise just take them off from the color table.
       max_profile_num = MAX(profile_num_array)
       colors = self->GET_PROFILE_COLORS(max_profile_num)


       ;Loop over the profiles
       FOR ii=0, num_pos_profiles-1 DO BEGIN

         ; Exclude background and reference profiles.
         profile_num = profile_num_array[where_pos[ii]]

         ; Get the profile object
         profile = profiles_obj->GET_PROFILE(profile_num)
         


         IF KEYWORD_SET(profiles) THEN BEGIN
; Cutoff should come from the user option.
           spectrum_y = profile->EVALUATE(x_values, /CUTOFF)

           where_nonzero = WHERE(ABS(spectrum_y) GT cutoff, count_nonzero)

           IF count_nonzero GE 1 THEN BEGIN
             plotlist->APPEND, {x:x_labels[where_nonzero] $
                                ,y:spectrum_y[where_nonzero] * scale $
                                ,color:colors[profile_num] $
                                ,oplot:1 $
                                ,plotnum:plotnum $
                                ,psym:0 $
                               }
           ENDIF
         ENDIF



         IF KEYWORD_SET(gaussians) THEN BEGIN
           FOR jj=0, profile->N_ELEMENTS()-1 DO BEGIN

             gauss = profile->GET(jj)

             ; Cutoff should come from the user option.
             spectrum_y = gauss->EVALUATE(x_values, /CUTOFF)
             

             where_nonzero = WHERE(spectrum_y NE 0D, count_nonzero)

             IF count_nonzero GE 1 THEN BEGIN
               plotlist->APPEND, {x:x_labels[where_nonzero] $
                                  ,y:spectrum_y[where_nonzero] * scale $
                                  ,color:colors[profile_num] $
                                  ,oplot:1 $
                                  ,plotnum:plotnum $
                                  ,psym:0 $
                                 }
             ENDIF
           ENDFOR
         ENDIF



         IF KEYWORD_SET(LOCATIONS) THEN BEGIN
           centroid = profile->CENTROID()
           intensity = profile->INTENSITY()

           cetroid_label = INTERPOL(x_labels, x_values, centroid)
           plotlist->APPEND, {x:[centroid_label,centroid_label] $
                              ,y:[0, intensity] * scale $
                              ,color:colors[profile_num] $
                              ,plots:1 $
                              ,thick:2 $
                              ,plotnum:plotnum $
                              ,psym:0 $
                             }
         ENDIF


         profile->DESTROY
       ENDFOR

     END ;PRO BST_INST_ADDON_PLOT::ADD_PROFILES



     ;+=================================================================
     ; PURPOSE:
     ;   Add the background to the plotlist
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::ADD_BACKGROUND, plotlist $
                                              ,plotnum $
                                              ,x_values $
                                              ,x_labels $
                                              ,scale


       ; Background profile
       profile_num = 0

       fitter_core = (self.dispatcher)->GET_CORE()

       ; Get a reference to the profiles object.
       profiles_obj = fitter_core.profiles

       ; Get any background profiles.
       profile = profiles_obj->GET_PROFILE(profile_num)



       model_back = self.dispatcher->GET_MODEL('BST_INST_MODEL_BACKGROUND')

       spectrum_y = model_back->EVALUATE(x_values)
       spectrum_y += profile->EVALUATE(x_values, /CUTOFF)


       profile->DESTROY
        
       plotlist->APPEND, {x:x_labels $
                          ,y:spectrum_y * scale $
                          ,color:self->GET_PROFILE_COLORS(/BACKGROUND) $
                          ,oplot:1 $
                          ,plotnum:plotnum $
                          ,linestyle:2 $
                          ,psym:0 $
                         }

     END ;PRO BST_INST_ADDON_PLOT::ADD_BACKGROUND



     ;+=================================================================
     ; PURPOSE:
     ;   Add individual models
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::ADD_MODELS, plotlist $
                                          ,plotnum $
                                          ,x_values $
                                          ,x_labels $
                                          ,scale

       fitter_core = (self.dispatcher).GET_CORE()
       model_dispatch = fitter_core.GET_MODEL_DISPATCHER()
       model_plotlist = model_dispatch.GET_PLOTLIST(x_values)

       ; Convert the x_values returned from the model into the x_labels.
       FOR ii=0,model_plotlist.N_ELEMENTS()-1 DO BEGIN
         plotstruct = model_plotlist.GET(ii)

         plotstruct.x = INTERPOL(x_labels, x_values, plotstruct.x)
         plotstruct.y = plotstruct.y * scale

         model_plotlist.SET, ii, plotstruct
       ENDFOR
       
       plotlist.JOIN, model_plotlist
       model_plotlist.DESTROY
       
     END ;PRO BST_INST_ADDON_PLOT::ADD_MODELS



     ;+=================================================================
     ; PURPOSE:
     ;   Add any ploting output from the addons.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT::ADD_ADDONS, plotlist $
                                          ,plotnum $
                                          ,x_values $
                                          ,x_labels $
                                          ,scale

       addon_plotlist = self.dispatcher->GET_SPECTRUM_PLOTLIST(x_values)

       ; Convert the x_values returned from the addons into the x_labels.
       FOR ii=0,addon_plotlist->N_ELEMENTS()-1 DO BEGIN
         plotstruct = addon_plotlist->GET(ii)

         plotstruct.x = INTERPOL(x_labels, x_values, plotstruct.x)
         plotstruct.y = plotstruct.y * scale

         addon_plotlist->SET, ii, plotstruct
       ENDFOR


       plotlist->JOIN, addon_plotlist

       addon_plotlist->DESTROY

     END ;PRO BST_INST_ADDON_PLOT::ADD_ADDONS



     ;+=================================================================
     ; PURPOSE:
     ;   Define the color scheme for plotting profiles.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_PLOT::GET_PROFILE_COLORS, max_profile_num $
                                                       ,BACKGROUND=background

       IF KEYWORD_SET(background) THEN RETURN, MIR_COLOR('BLACK')

       IF max_profile_num LE 6 THEN BEGIN
         colors = [MIR_COLOR('BLACK') $
                   ,MIR_COLOR('PRINT_PURPLE') $
                   ,MIR_COLOR('PRINT_GREEN') $
                   ,MIR_COLOR('PRINT_CYAN') $
                   ,MIR_COLOR('PRINT_YELLOW') $
                   ,MIR_COLOR('PRINT_ORANGE') $
                   ,MIR_COLOR('PRINT_BLUE')]
       ENDIF ELSE BEGIN
         MIR_LOADCT, 0, /SILENT
         colors = MIR_COLOR_FROM_INDEX(FIX(INDGEN(max_profile_num+1)*(255/max_profile_num)))
       ENDELSE

       RETURN, colors

     END ;FUNCTION BST_INST_ADDON_PLOT::GET_PROFILE_COLORS


     ;+=================================================================
     ; PURPOSE:
     ;   Define the object as a subclass of [BST_INST_ADDON]
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_PLOT__DEFINE

       ; First define a structure with the plot options.
       options = $
          {BST_INST_ADDON_PLOT__OPTIONS $
           ,thesis_yfactor:0L $

           ,yrange:[0D,0D] $
           ,residual_yrange:[0D,0D] $

           ,xrange:[0D,0D] $
           ,wavelengthrange:[0D,0D] $

           ,normalize:0 $

           ,plot_vs_wavelength:0 $
           ,plot_overall_fit:0 $
           ,plot_residual:0 $
           ,plot_background:0 $
           ,plot_profiles:0 $
           ,plot_gaussians:0 $
           ,plot_profile_locations:0 $
           ,plot_error_bars:0 $
           ,plot_addons:0 $
           ,plot_models:0 $

           ,weighted_residual:0 $
       
           ,show_grid:0 $

           ,reload_spectrum:0 $
          }

       param = $
          {BST_INST_ADDON_PLOT__PARAM $
           ,window:0L $
           ,pdf:0 $
           ,postscript:0 $
          }

       struct = { BST_INST_ADDON_PLOT $
                  ,options:options $
                  ,param:param $

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_PLOT__DEFINE
