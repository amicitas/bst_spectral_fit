

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT>
;
;   This is an addon for <BST_SPECTRAL_FIT> that allows spectra to
;   be taken from images loaded using [DETECTOR_VIEWER].
; 
;-======================================================================




     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER::SET_FLAGS

       self.BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.add_to_menu = 1
       self.menu_title = 'SPECTRA: Detector Viewer'

       self.conflicting = 'BST_INST_ADDON_USER_SPECTRA'

     END ; PRO BST_INST_ADDON_DETECTOR_VIEWER::SET_FLAGS



     ;+=================================================================
     ;
     ; Return a structure with the default parameters for the
     ; DETECTOR_VIEWER addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::RESET, _NULL=_null
  
       self.BST_INST_ADDON::RESET
       
       defaults =  { $
                     use_row:1 $
                     ,range_row:[0,0] $
                     ,range_col:[0,0] $
                     ,use_average:1 $
                     ,use_poission_weights:1 $
                     ,image_filename:'' $
                     ,back_filename:'' $
                     ,back_subtraction:0 $
                   }

       self.SET_PARAM, defaults

     END ; FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::DEFAULT_PARAMS


     ;+=================================================================
     ;
     ; Initialize the DETECTOR_VIEWER addon.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER::ENABLE

       ; Call the parent method.
       self.BST_INST_ADDON::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       core = self.dispatcher.GET_CORE()
       core.SET_SPECTRUM_FUNCTION, 'spectrum', self

       ; Run Detector Viewer.
       ; If detector viewer is already open, this won't do anything.
       DETECTOR_VIEWER

     END ; PRO BST_INST_ADDON_DETECTOR_VIEWER::ENABLE



     ;+=================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER::DISABLE

       ; Call the parent method.
       self.BST_INST_ADDON::DISABLE

       IF self.IS_ENABLED() THEN BEGIN
         fitter = self.dispatcher.GET_CORE()
         fitter.SET_SPECTRUM_FUNCTION, /DEFAULT
       ENDIF

     END ;PRO BST_INST_ADDON_DETECTOR_VIEWER::DISABLE



     ;+=================================================================
     ; 
     ; Return a structure containing anything should should be saved
     ; for this addon.
     ;
     ; This is different from the base method in that it first updates
     ; the image file names from detector viewer.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::GET_STATE

       FORWARD_FUNCTION DV_GET_IMAGE_DATA $
                        ,DV_GET_IMAGE_BACK_DATA $
                        ,DV_GET_IMAGE_FINAL_DATA
       

       ; First load the filenames from DETECTOR_VIEWER into the addon param.
       image_data = DV_GET_IMAGE_DATA()
       image_back_data = DV_GET_IMAGE_BACK_DATA()
       image_final_data = DV_GET_IMAGE_FINAL_DATA()

       self.param.image_filename = image_data.filename
       self.param.back_filename = image_back_data.filename 
       self.param.back_subtraction = image_final_data.back_subtraction

       
       ; Now call the superclass method.
       state = self.BST_INST_ADDON::GET_STATE()
       state = CREATE_STRUCT(state, {param:self.param})

       RETURN, state
   


     END ; FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::GET_STATE




     ;+=================================================================
     ;
     ; Recieve a structure, of the same type as given in in
     ; [GET_STATE], and load the contents.
     ;
     ; This is different from the base method in that in loads
     ; the image file names into detector viewer.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER::LOAD_STATE, state

       self.BST_INST_ADDON::LOAD_STATE, state
       STRUCT_ASSIGN, {param:state.param}, self, /NOZERO

       
       ; Now load parameters in DETECTOR_VIEWER only if this addon is enabled.
       IF self.IS_ENABELD() THEN BEGIN
         ; Load DETECTOR_VIWER. 
         ; If it is already open this will bring the window to the front.
         DETECTOR_VIEWER

         ; Set the background subtraction
         DV_SET_BACK_SUBTRACTION, self.param.back_subtraction, /NO_REFRESH

         ; Load the image.
         DV_LOAD_IMAGE, self.param.image_filename, /NO_REFRESH

         ; Load the background.
         DV_LOAD_IMAGE, self.param.back_filename, /BACKGROUND
       ENDIF

         
     END ; PRO BST_INST_ADDON_DETECTOR_VIEWER::LOAD_STATE


     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::GET_PARAM

       RETURN, self.param

     END ; FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::GET_PARAM


     ;+=========================================================================
     ; PURPOSE:
     ;   Load the addon parameters from a structure.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER::SET_PARAM, param

       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ; PRO BST_INST_ADDON_DETECTOR_VIEWER::SET_PARAM
     

; ======================================================================
; ======================================================================
; ######################################################################
;
; SPECTRUM METHODS
;    These are the methods that <BST_SPECTRAL_FIT> will actually
;    use to get the spectra.
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ;
     ; This is the function that the the DETECTOR_VIEWER addon uses
     ; to retrieve the spectra to be fit.
     ;
     ; This method will be called directly by <BST_SPECTRAL_FIT>
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::SPECTRUM

       FORWARD_FUNCTION DV_IS_IMAGE_1D_LOADED $
         ,DV_IS_IMAGE_LOADED $
         ,DV_GET_IMAGE_1D $
         ,DV_GET_IMAGE_FINAL $
         ,DV_GET_IMAGE_FINAL__DATA

       IF self.param.type EQ '1D' THEN BEGIN

         IF NOT (DV_IS_IMAGE_1D_LOADED()) THEN BEGIN
           MESSAGE, '1D image cut is not loaded.'
         ENDIF

         y = DV_GET_IMAGE_1D()
         spectrum_size = N_ELEMENTS(y)
         
         weight = REPLICATE(1.0, spectrum_size)
         
       ENDIF ELSE BEGIN

         IF NOT (DV_IS_IMAGE_LOADED()) THEN BEGIN
           MESSAGE, 'Image is not loaded.'
         ENDIF
         
         image_data = DV_GET_IMAGE_FINAL_DATA()
         image = DV_GET_IMAGE_FINAL()

         CASE 1 OF
           self.param.type EQ 'ROW': BEGIN
             dim_index = 1
             range = self.param.range_row
           END
           self.param.type EQ 'COL': BEGIN
             dim_index = 0
             range = self.param.range_col
           END
         ENDCASE
            
         spectrum_size = image_data.size[dim_index]
         y = DBLARR(spectrum_size)
             

         IF (range[0] LT 0) THEN BEGIN
           MESSAGE, 'Invalid range.'
         ENDIF
             
         ; If range[1] eq 0 then we want to assume that no averaging is wanted.
         ; Just use the slice from range[0]
         CASE 1 OF
           range[1] EQ 0: BEGIN
             range[1] = range[0]
           END
           range[1] LT range[0]: BEGIN
             MESSAGE, 'Invalid range.'
           END
           range[1] GT spectrum_size-1: BEGIN
             range[1] = spectrum_size-1
           END
           ELSE:
         ENDCASE
             
         ; Sum all of the rows or columns.
         CASE 1 OF
           self.param.type EQ 'ROW': BEGIN
             FOR i = range[0], range[1] DO BEGIN
               y += image[*,i]
             ENDFOR
           END
           self.param.type EQ 'COL': BEGIN
             FOR i = range[0], range[1] DO BEGIN
               y += image[i,*]
             ENDFOR
           END
         ENDCASE

         ; Note that this will get overwritten below if
         ; poisson weighting is not used.
         weight = SQRT(spectram_size)
         
         IF self.param.use_average THEN BEGIN
           ; Divide by the number of averaged rows/columns
           y = y/(range[1]-range[0]+1)
           weight = weight/(range[1]-range[0]+1)
         ENDIF

       ENDELSE
           
       x = INDGEN(spectrum_size)
       
       IF ~self.param.use_poisson_weights THEN BEGIN
         weight = REPLICATE(1.0, spectrum_size)
       ENDIF

       RETURN, { $
                 x:x $
                 ,y:y $
                 ,weight:weight $
               }

     END ; FUNCTION BST_INST_ADDON_DETECTOR_VIEWER::SPECTRUM



; ======================================================================
; ======================================================================
; ######################################################################
;
; EXTERNAL METHODS
;     These are methods that can be called from outside
;     of the object.  They may be used internally as well.
; 
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; Set the averaging range to be used.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER::SET_RANGE, range, ROW=row, COL=col

       ; See if either keyword was set.
       CASE 1 OF
         ( ~ (KEYWORD_SET(row) OR KEYWORD_SET(col))): BEGIN
           type = STRUPCASE(self.type)
         END

         KEYWORD_SET(ROW): type = 'ROW'
         KEYWORD_SET(COL): type = 'COL'
       ENDCASE

       CASE type OF
         'ROW': self.param.range_row = range
         'COL': self.param.range_col = range      
       ENDCASE

       self->UPDATE

     END ; PRO BST_INST_ADDON_DETECTOR_VIEWER::SET_RANGE



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON]
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DETECTOR_VIEWER__DEFINE

       struct = { BST_INST_ADDON_DETECTOR_VIEWER $

                  ,param:{ BST_INST_ADDON_DETECTOR_VIEWER_PARAM $
                           ,use_row:0 $
                           ,range_row:[0,0] $
                           ,range_col:[0,0] $
                           ,use_average:0 $
                           ,image_filename:'' $
                           ,back_filename:'' $
                           ,back_subtraction:0 $
                         } $
                           
                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_DETECTOR_VIEWER__DEFINE
