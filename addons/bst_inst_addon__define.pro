

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   This is the base class for <BST_SPECTRAL_FIT> addons.
;
;   All addon objects should be derived from this class.
;   If the addon will be attached to the GUI then it is better to
;   inherit from [BST_INST_ADDON::] instead of directly from this
;   one.
;   
;
;
;-======================================================================



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Define the base class for <BST_SPECTRAL_FIT> addons.
     ;
     ;   Most addons should inherit [BST_INST_ADDON::] wich is
     ;   derived from this addon.
     ;-=================================================================
     PRO BST_INST_ADDON__DEFINE


       struct = { BST_INST_ADDON $

                  ,INHERITS BST_INST_DISPATCHED $
                }

     END ; PRO BST_INST_ADDON__DEFINE
