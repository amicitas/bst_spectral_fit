
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   An addon to be used for fitting the detector geometry for XICS systems.
;   
;   This addon is meant to be used as part of <XC_GEOMETRY_FIT::>.  All control
;   of the addon will be done through <XC_GEOMETRY_FIT::>.
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA::SET_FLAGS

       self.BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.add_to_menu = 1
       self.menu_title = 'SPECTRA: XC Geometry'

       self.conflicting = 'BST_INST_ADDON_CERVIEW' $
                          +', BST_INST_ADDON_DETECTOR_VIEWER' $
                          +', BST_INST_ADDON_USER_SPECTRA'

     END ; PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA::SET_FLAGS


     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the XC_GEOMETRY_SPECTRA addon.
     ;
     ; PROGRAMMING NOTES:
     ;   Note here that we do not check if this addon is already
     ;   enabled.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA::ENABLE

       ; Call the parent method.
       self.BST_INST_ADDON::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       fitter = self.dispatcher.GET_CORE()
       fitter.SET_SPECTRUM_FUNCTION, 'spectrum', self

     END ; PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA::ENABLE


     ;+=========================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=========================================================================
     PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA::DISABLE

       ; Call the parent method.
       self.BST_INST_ADDON::DISABLE

       IF self.IS_ENABLED() THEN BEGIN
         fitter = self.dispatcher.GET_CORE()
         fitter.SET_SPECTRUM_FUNCTION, /DEFAULT
       ENDIF

     END ;PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA::DISABLE


; ==============================================================================
; ==============================================================================
; ######################################################################
;
; SPECTRUM METHODS
;    These are the methods that <BST_SPECTRAL_FIT> will actually
;    use to get the spectra.
;
; ######################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ;
     ; Create a set of data points that we will use to fit the location of the 
     ; XICS camera.
     ;
     ; For this calibration we will assume that we are using data from a shot
     ; that has no plasma rotation.
     ;
     ; The data points are as follows:
     ;  *  Measured or design location of the camera.
     ;  *  Theoretical (unshifted) wavelengths of the helium-like Ar lines
     ;     at each row on the detector.
     ;
     ; With no plasma rotation, we would expect that when the detector is 
     ; placed correctly the fit line locations will equal the theoretical line
     ; locations everywhere on the detector.
     ;
     ; In additon we assume that the actuall detector location is not very
     ; far from the measured/design location of the detector.
     ;
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XC_GEOMETRY_SPECTRA::SPECTRUM


       ; Establish catch block.
       CATCH, error                   
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, /ERROR
         MESSAGE, STRING('Could not retrieve XC_GEOMETRY_SPECTRA spectrum.') 
       ENDIF

       model = self.dispatcher.GET_MODEL('XC_GEOMETRY')
       const = model.GET_CONST()
       errors = model.GET_ERRORS()

       y = MIR_LIST()
       sigma = MIR_LIST()
       index = 0

       ; -----------------------------------------------------------------------
       ; Add the measured detector location.

       ; The distance of the current detector location from the starting location.
       ; That is by definition 0 here.
       y.ADD, [0D, 0D, 0D, 0D, 0D, 0D], /EXTRACT

       ; Set the errors in the measured parameters.
       ; Detector location xyz
       sigma.ADD, errors.detector_location, /EXTRACT
       ; Normal angle theta, phi
       sigma.ADD, errors.detector_normal_angle, /EXTRACT
       ; Orientaiton angle.
       sigma.ADD, errors.detector_orientation_angle, /EXTRACT

       index += 6
       

       ; -----------------------------------------------------------------------
       ; Add the measured crystal location.
       
       ; The distance of the current crystal location from the starting location.
       ; That is by definition 0 here.
       y.ADD, [0D, 0D, 0D, 0D, 0D, 0D], /EXTRACT

       ; Set the errors in the measured parameters.
       ; Crystal location xyz
       sigma.ADD, errors.crystal_location, /EXTRACT
       ; Normal angle theta, phi
       sigma.ADD, errors.crystal_normal_angle, /EXTRACT
       ; Orientaiton angle.
       sigma.ADD, errors.crystal_orientation_angle, /EXTRACT

       index += 6

       
       ; -----------------------------------------------------------------------
       ; Add the target locations.
       IF ISA(const.target_list) THEN BEGIN
         num_target = const.target_list.N_ELEMENTS()
         FOR ii_target = 0, num_target - 1 DO BEGIN
           y.ADD, [0D, 0D, 0D], /EXTRACT
           sigma.ADD, errors.target_location, /EXTRACT
         ENDFOR
       ENDIF

       
       ; -----------------------------------------------------------------------
       ; Add the theoretical line positions to the "spectrum".
       IF ISA(const.wavelength_list) THEN BEGIN
         num_lines = const.wavelength_list.N_ELEMENTS()
         num_rows = N_ELEMENTS((const.channel_list[0])[0,*])

         ; Loop through the lines and add them to the "spectrum".
         FOR ii_line=0, num_lines-1 DO BEGIN
           FOR ii_row=0, num_rows-1 DO BEGIN
             y.ADD, const.wavelength_list[ii_line], /EXTRACT
             sigma.ADD, errors.wavelength, /EXTRACT
           ENDFOR
         ENDFOR 

       ENDIF
       
       y = y.TOARRAY()
       sigma = sigma.TOARRAY()
       
       ; Define line weight (1/stddev^2).
       weight = 1D / (sigma^2)

       x = FINDGEN(N_ELEMENTS(y))



       ; -----------------------------------------------------------------------
       ; This represents the distance of the central viewing chord to
       ; the magnetic axis.  This value should always be zero.  The distance
       ; is calculated in the model for now.
       ;y[12] = 0D
       ;IF self.errors.axis_location NE 0 THEN BEGIN
       ;  weight[12] = 1D / (self.errors.axis_location^2)
       ;ENDIF ELSE BEGIN
       ;  weight[12] = 0D
       ;ENDELSE


       ; -----------------------------------------------------------------------
       ; Return the final spectrum.

       spectrum_out = { $
                      y:y $
                      ,x:x $
                      ,sigma:sigma $
                      ,weight:weight $
                      }

       RETURN, spectrum_out


     END ; FUNCTION BST_INST_ADDON_XC_GEOMETRY_SPECTRA::SPECTRUM


; ==============================================================================
; ==============================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON]
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA__DEFINE

       struct = { BST_INST_ADDON_XC_GEOMETRY_SPECTRA $

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_XC_GEOMETRY_SPECTRA__DEFINE
