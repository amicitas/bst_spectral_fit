
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT> to fit complete shots from the XICS system.
;
; DESCRIPTION:
;   For the time being each fit will be done using the same initial conditions.
;
;   In the future I may want to start each fit using the results from the 
;   previous fit.  Do do this properly is a bit complicated.  For the row fits,
;   rather than just fitting from bottom to top, I will need to do the
;   fit in two stages.  Starting from the middle each time and going out to
;   each edge.
;
;   Also I will need to reset the fit for each timeslice.
;
;   Lastly I will need a way to see if a fit is bad, and ignore the results
;   in those cases.
; 
;
; TO DO:
;   Allow time_min and time_max parameters to be used.
;
; PROGRAMMING NOTES:
;   This addon reqires IDL version 8.0 or later.  
;   While everything in here should be back compatable (mostly for consistency),
;   some of the utilites that I use rely on features introduced in IDL 8.0.
;
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_FLAGS

       self.BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.add_to_menu = 0
       self.menu_title = 'XCrystal Shot Analysis'

       self.required = 'BST_INST_ADDON_XCRYSTAL_SPECTRA'

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_FLAGS



     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the addon object
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::INIT

       IF ~ self.BST_INST_ADDON::INIT() THEN RETURN, 0


       ; Check the idl version.
       IF ~ MIR_CHECK_IDL_VERSION(8) THEN BEGIN
         MESSAGE, 'This addon requires IDL 8.0 or later.'
         RETURN, 0
       ENDIF


       RETURN, 1

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::INIT



     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the addon.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::INITIALIZE

       self.BST_INST_ADDON::INITIALIZE

       ; Use my old IDL 7.0 style list for consistenly with the rest of
       ; <BST_SPECTRAL_FIT>.
       self.fit_list = MIR_LIST_IDL7()

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::INITIALIZE



     ;+=========================================================================
     ;
     ; Return a structure with the default parameters for the
     ; USER SPECTRA addon.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::RESET, _NULL=_null
           
       self.SET_DEFAULTS
 
       self.BST_INST_ADDON::RESET

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::RESET



     ;+=========================================================================
     ;
     ; Set default values for the addon parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_DEFAULTS
           
       self.SET_PARAM, { $
                          ts_min:0L $
                          ,ts_max:0L $

                          ,ts_average:1L $
                          ,ts_step:1L $

                          ,y_min:0L $
                          ,y_max:0L $

                          ,y_average:1L $
                          ,y_step:1L $
                        }

       self.SET_Y_BINS, []
       self.SET_TS_BINS, []

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_DEFAULTS


     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the state of addon.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_STATE
       
       state = self.BST_INST_ADDON::GET_STATE()
       param = self.GET_PARAM()
       state = CREATE_STRUCT(state, {param:param})

       RETURN, state

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Load a save structure for this addon.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::LOAD_STATE, state

       self.BST_INST_ADDON::LOAD_STATE, state
       self.SET_PARAM, state.param

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::LOAD_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_PARAM

       param = self.param
       param = CREATE_STRUCT(param $
                             ,{ y_bins:self.GET_Y_BINS() $
                                ,ts_bins:self.GET_TS_BINS() } $
                            )
       
       RETURN, param

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_PARAM



     ;+=========================================================================
     ; PURPOSE:
     ;   Load the addon parameters from a structure.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_PARAM, param

       STRUCT_ASSIGN, {param:param}, self, /NOZERO

       IF HAS_TAG(param, 'Y_BINS') THEN BEGIN
         self.set_y_bins, param.y_bins
       ENDIF

       IF HAS_TAG(param, 'TS_BINS') THEN BEGIN
         self.set_ts_bins, param.ts_bins
       ENDIF


     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_PARAM



     ;+=========================================================================
     ; PURPOSE:
     ;   Return the array of y bins.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_Y_BINS

       y_bins = *(self.private.y_bins)
       IF ~ ISA(y_bins) THEN BEGIN
         y_bins = [0,0]
       ENDIF

       RETURN, y_bins

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_Y_BINS



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the array of y bins.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_Y_BINS, y_bins

       self.private.y_bins = PTR_NEW(y_bins)

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_Y_BINS


     ;+=========================================================================
     ; PURPOSE:
     ;   Return the array of ts bins.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_TS_BINS


       ts_bins = *(self.private.ts_bins)
       IF ~ ISA(ts_bins) THEN BEGIN
         ts_bins = [0,0]
       ENDIF

       RETURN, ts_bins

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_TS_BINS



     ;+=========================================================================
     ; PURPOSE:
     ;   Set the array of ts bins.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_TS_BINS, ts_bins

       self.private.ts_bins = PTR_NEW(ts_bins)

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::SET_TS_BINS


; ==============================================================================
; ==============================================================================
; ##############################################################################
;
; FITTING METHODS
;   Fitting will done by looping over timeslices.  For each timeslice, every
;   row will be fit.
;   
;
;
; ##############################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Fit all timeslices and rows, save the results internally.
     ;
     ; DESCRIPTION:
     ;   This is the method to call to start the fit.
     ;
     ;   All it does is call FIT_ALL_TIMESILCES, but has a more generic name
     ;   so that the timeslice-row order does not need to be known by the user.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::FIT_ALL

       self.FIT_ALL_TIMESLICES

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::FIT_ALL



     ;+=========================================================================
     ; PURPOSE:
     ;   Fit all timeslices and save the results internally.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::FIT_ALL_TIMESLICES
       COMPILE_OPT STRICTARR

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         BST_INST_LOGGER, 'Error fitting multiple timeslices.', /ERROR
         RETURN
       ENDIF 


       self.fit_list.REMOVE, /ALL



       ; -----------------------------------------------------------------------
       ; Setup for the fit.
       
       ; Get a reference to the spectra addon.
       xcrystal_spectra = self.dispatcher.GET_ADDON('BST_INST_ADDON_XCRYSTAL_SPECTRA')

       
       ; -----------------------------------------------------------------------
       ; Start the fit loop.
       ts_bins = self.GET_OR_GENERATE_TS_BINS()
       num_ts_bins = N_ELEMENTS(ts_bins[0,*])

       ; Start the fit loop.
       FOR ii = 0, num_ts_bins-1 DO BEGIN

         ; Get the next ts bin range.
         ts_range = ts_bins[*,ii]

         ; Set the ts range in the <BST_INST_ADDON_XCRYSTAL_SPECTRA::> addon.
         xcrystal_spectra.SET_PARAM, {ts_min:ts_range[0], ts_max:ts_range[1]}

         ; Fit this timeslice.
         row_fit = self.FIT_ALL_ROWS()

         IF ISA(row_fit) THEN BEGIN
           self.fit_list.APPEND, row_fit
         ENDIF

       ENDFOR

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::FIT_ALL_TIMESLICES



     ;+=========================================================================
     ; PURPOSE:
     ;   Fit all rows and save the results using the
     ;   [BST_INST_ADDON_FIT_SAVER::] addon.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::FIT_ALL_ROWS
       COMPILE_OPT STRICTARR

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         fit_saver.SET_AUTOMATIC, 0
         BST_INST_LOGGER, /ERROR
         MESSAGE, 'Error fitting multiple rows.'
       ENDIF 



       ; -----------------------------------------------------------------------
       ; Setup for the fit.

       ; Get a reference to the fitter core
       fitter_app = self.dispatcher.GET_APP()
       
       ; Get a reference to the spectra addon.
       xcrystal_spectra = self.dispatcher.GET_ADDON('BST_INST_ADDON_XCRYSTAL_SPECTRA')

       ; Get a reference to the fit saver object and reset it.
       fit_saver = self.dispatcher.GET_ADDON('BST_INST_ADDON_FIT_SAVER')
       fit_saver.RESET

       ; Set the fit saver addon to automatically save the results from each fit.
       fit_saver.SET_AUTOMATIC, 1

       ; Turn off saving of the fit.  See notes in the file header.
       fitter_app.SET_SAVE, 0

       ; Initialize the XCRYSTAL_SPECTRA addon since we need spectra for
       ; signal to noise checking.
       xcrystal_spectra.BEFORE_FIT

       ; -----------------------------------------------------------------------
       ; Start the fit loop.

       y_bins = self.GET_OR_GENERATE_Y_BINS()
       num_y_bins = N_ELEMENTS(y_bins[0,*])

       ; Start the fit loop.
       FOR ii = 0, num_y_bins-1 DO BEGIN

         ; Get the next y bin range.
         row_range = y_bins[*,ii]
         
         
         ; Set the row range in the <BST_INST_ADDON_XCRYSTAL_SPECTRA::> addon.
         xcrystal_spectra.SET_PARAM, {y_min:row_range[0], y_max:row_range[1]}

         ; Now call BST_INST_ADDON_XCRYSTAL_SPECTRA::BEFORE_FIT.
         ; This ensures that we synced the model and the spectra.
         xcrystal_spectra.BEFORE_FIT
         

         BST_INST_LOGGER, /INFO, STRING(xcrystal_spectra.param.ts_min, xcrystal_spectra.param.ts_max $
                                     ,FORMAT='("TS_RANGE: ", i4, 2x, i4)')
         BST_INST_LOGGER, /INFO, STRING(xcrystal_spectra.param.y_min, xcrystal_spectra.param.y_max $
                                     ,FORMAT='("Y_RANGE:  ", i4, 2x, i4)')


         ; Check the signal to noise.
         ; If not good then don't fit this timeslice.
         IF ~ self.CHECK_SIGNAL_TO_NOISE() THEN BEGIN
           BST_INST_LOGGER, /INFO, 'Spectra did not pass signal to noise test. Skipping this row.'
           CONTINUE
         ENDIF

         
; TEMPORARY.  I want to profile every fit.
;             This will override any profiling that was started before this call.
;fitter_app.PROFILER_START

         ; Fit the current timeslice.
         fitter_app.FIT

;fitter_app.PROFILER_STOP

       ENDFOR


       ; Turn off automatic fit saving.
       fit_saver.SET_AUTOMATIC, 0


       ; Retrive the results.
       row_fit_list = fit_saver.GET_STATE_LIST()

       IF row_fit_list.N_ELEMENTS() GT 0 THEN BEGIN
         RETURN, row_fit_list.TO_ARRAY()
       ENDIF ELSE BEGIN
         RETURN, []
       ENDELSE


     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::FIT_ALL_ROWS


     ;+=========================================================================
     ; PURPOSE:
     ;   Retrieve the y bins to use.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_OR_GENERATE_Y_BINS
       y_bins = self.GET_Y_BINS()
       IF ARRAY_EQUAL(y_bins, 0) THEN BEGIN
         y_bins = self.GENERATE_Y_BINS()
       ENDIF

       IF ~ ISA(y_bins) THEN BEGIN
         MESSAGE, 'Could not retrieve a valid set of y bins.'
       ENDIF

       RETURN, y_bins

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_OR_GENERATE_Y_BINS



     ;+=========================================================================
     ; PURPOSE:
     ;   Retrieve the ts bins to use.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_OR_GENERATE_TS_BINS
       ts_bins = self.GET_TS_BINS()
       IF ARRAY_EQUAL(ts_bins, 0) THEN BEGIN
         ts_bins = self.GENERATE_TS_BINS()
       ENDIF

       IF ~ ISA(ts_bins) THEN BEGIN
         MESSAGE, 'Could not retrieve a valid set of ts bins.'
       ENDIF


       RETURN, ts_bins

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_OR_GENERATE_TS_BINS


     ;+=========================================================================
     ; PURPOSE:
     ;   Generate the array of y bins to use.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GENERATE_Y_BINS

       y_bins = []

       WHILE 1 DO BEGIN

         ; Calculate the next timeslice.
         row_range = self.GET_NEXT_ROW_RANGE(row_range)

         ; Check the row_range to see if this is the last bin.
         IF row_range[1] GT self.param.y_max THEN BREAK

         y_bins = [[y_bins], [row_range]]
       ENDWHILE

       RETURN, y_bins

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GENERATE_Y_BINS



     ;+=========================================================================
     ; PURPOSE:
     ;   Generate the array of ts bins to use.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GENERATE_TS_BINS

       ts_bins = []


       ; Get a reference to the spectra addon.
       xcrystal_spectra = self.dispatcher.GET_ADDON('BST_INST_ADDON_XCRYSTAL_SPECTRA')

       data = XC_DATA()
       info = data.GET_INFO(xcrystal_spectra.param.system, xcrystal_spectra.param.SHOT)
       num_frames = info['num_frames']

       
       WHILE 1 DO BEGIN

         ; Calculate the next timeslice.
         ts_range = self.GET_NEXT_TS_RANGE(ts_range)

         ; Check the ts_range to see if this is the last bin.
         IF ts_range[1] GT self.param.ts_max THEN BREAK
         IF ts_range[1] GT num_frames-1 THEN BREAK

         ts_bins = [[ts_bins], [ts_range]]
       ENDWHILE

       RETURN, ts_bins

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GENERATE_TS_BINS



     ;+=========================================================================
     ; PURPOSE:
     ;   Get the next row_range to fit.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_NEXT_ROW_RANGE, row_range_last

       row_range = [0L, 0L]

       ; First set the start of the range
       IF ISA(row_range_last) THEN BEGIN
         row_range[0] = row_range_last[0] + self.param.y_step
       ENDIF ELSE BEGIN
         row_range[0] = self.param.y_min
       ENDELSE

       ; Now set the end of the range
       row_range[1] = row_range[0] + self.param.y_average - 1

       RETURN, row_range

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_NEXT_ROW_RANGE



     ;+=========================================================================
     ; PURPOSE:
     ;   Get the next ts_range (timeslice) to fit.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_NEXT_TS_RANGE, ts_range_last

       ts_range = [0L, 0L]

       ; First set the start of the range
       IF ISA(ts_range_last) THEN BEGIN
         ts_range[0] = ts_range_last[0] + self.param.ts_step
       ENDIF ELSE BEGIN
         ts_range[0] = self.param.ts_min
       ENDELSE

       ; Now set the end of the range
       ts_range[1] = ts_range[0] + self.param.ts_average - 1

       RETURN, ts_range

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_NEXT_TS_RANGE
     

     ;+=========================================================================
     ; PURPOSE:
     ;   Return the fit list.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::CHECK_SIGNAL_TO_NOISE

       ; Get a reference to the spectra addon.
       xcrystal_spectra = self.dispatcher.GET_ADDON('BST_INST_ADDON_XCRYSTAL_SPECTRA')

       spectrum = xcrystal_spectra.SPECTRUM()

       
       ; Do median filtering of the data.

       ; Find the values within which 95.0% of data is below.
       percent = 0.95

       ; Anything more than twice this value time will be considered bad.
       ; Note: This was fine for LHD, but may not work for W7X which has less
       ;       spectral resolution. Peaks may only have a single bright point. 
       cutoff = 2.0

       median_percent = MIR_MEDIAN(spectrum.y, percent)
       y_max = median_percent * cutoff

       IF y_max EQ 0 THEN BEGIN
         BST_INST_LOGGER, /DEBUG, 'No signal.'
         RETURN, 0
       ENDIF

       w_good = WHERE(spectrum.y LT y_max, count_good)

       ; Compare the standard deviation in the data with the expected error in 
       ; the data.  A small number, much less than one, is good.
       test = MEAN(1/SQRT(spectrum.weight[w_good]))/STDDEV(spectrum.y[w_good])

       BST_INST_LOGGER, /DEBUG, 'Signal to noise factor: '+STRING(test)
       
       ; I need to choose a cutoff.  This is somewhat arbitrary.
       RETURN, (test LE .50)

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::CHECK_SIGNAL_TO_NOISE



     ;+=========================================================================
     ; PURPOSE:
     ;   Return the fit list.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_FIT_LIST, NO_COPY = no_copy

       IF KEYWORD_SET(no_copy) THEN BEGIN
         RETURN, self.fit_list
       ENDIF ELSE BEGIN
         RETURN, self.fit_list.GET_COPY(/RECURSIVE)
       ENDELSE


     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_FIT_LIST



     ;+=========================================================================
     ; PURPOSE:
     ;   Return the fit list converted to a structure.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_FIT_STRUCTURE

       IF self.fit_list.N_ELEMENTS() EQ 0 THEN BEGIN
         MESSAGE, 'No states are saved.'
       ENDIF

       ; Convert the statelist to a structure.
       fit_structure = self.fit_list.TO_STRUCTURE()

       ; Annonymize the state structure.
       fit_structure = STRUCT_ANONYMOUS(fit_structure)

       RETURN, fit_structure

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_MULTI::GET_FIT_STRUCTURE




; ==============================================================================
; ==============================================================================
; ##############################################################################
;
; OBJECT DEFINITION
;
; ##############################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI::CLEANUP

       self.REMOVE_WIDGET

       IF OBJ_VALID(self.fit_list) THEN BEGIN
         self.fit_list.DESTROY
       ENDIF

       self.BST_INST_ADDON::CLEANUP

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI::CLEANUP


     ;+=========================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON]
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_MULTI__DEFINE

       struct = { BST_INST_ADDON_XCRYSTAL_MULTI $

                  ,fit_list:OBJ_NEW() $
                  
                  ,param:{ BST_INST_ADDON_XCRYSTAL_MULTI__PARAM $
                           ;,system:'' $
                           ;,shot:0L $
                           
                           ,ts_min:0L $
                           ,ts_max:0L $

                           ,ts_average:0L $
                           ,ts_step:0L $

                           ,y_min:0L $
                           ,y_max:0L $

                           ,y_average:0L $
                           ,y_step:0L $
                         } $

                  
                  ,private:{ BST_INST_ADDON_XCRYSTAL_MULTI__PRIVATE $
                             ,y_bins:PTR_NEW() $
                             ,ts_bins:PTR_NEW() $
                           } $

                  ,widget:OBJ_NEW() $

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_XCRYSTAL_MULTI__DEFINE
