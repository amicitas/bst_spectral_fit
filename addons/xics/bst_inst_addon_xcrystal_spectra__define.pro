
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   An addon for <BST_SPECTRAL_FIT> to retrive specra for the XICS system.
;   For the time being this is specific to the CMOD system.  Eventualy this
;   will be generalized to all XICS systems (CMOD, NSTX, LHD, EAST, KSTAR).
;
;   
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::SET_FLAGS

       self.BST_INST_ADDON::SET_FLAGS

       self.active = 1
       self.default = 0

       self.add_to_menu = 1
       self.menu_title = 'XICS'

       self.conflicting = 'BST_INST_ADDON_CERVIEW' $
                          +', BST_INST_ADDON_DETECTOR_VIEWER' $
                          +', BST_INST_ADDON_USER_SPECTRA'

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::SET_FLAGS



     ;+=========================================================================
     ;
     ; Return a structure with the default parameters for the
     ; USER SPECTRA addon.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::RESET, _NULL=_null
          
       self.BST_INST_ADDON::RESET
       
       self.data_object = OBJ_NEW('XC_DATA')

       ; By setting the config_shot to -1, the shot will be used as the config
       ; shot.
       self.SET_PARAM, { $
                         shot:1100323015L $
                         ,config_shot:-1L $
                         ,system:'cmod_helium' $

                         ,ts_min:100L $
                         ,ts_max:100L $

                         ,y_min:700L $
                         ,y_max:700L $
                       }


     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::RESET

     
     ;+=========================================================================
     ; PURPOSE:
     ;   Clear all cached values
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::CLEAR_CACHE, _NULL=_null

       self.CLEAR_XICS_DATA

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::CLEAR_CACHE
     
     
     ;+=========================================================================
     ; PURPOSE:
     ;   Initialize the XCRYSTAL_SPECTRA addon.
     ;
     ; PROGRAMMING NOTES:
     ;   Note here that we do not check if this addon is already
     ;   enabled.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::ENABLE

       ; Call the parent method.
       self.BST_INST_ADDON::ENABLE

       ; Set the spectrum function for <BST_SPECTRAL_FIT>
       core = self.dispatcher.GET_CORE()
       core.SET_SPECTRUM_FUNCTION, 'spectrum', self

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::ENABLE


     ;+=========================================================================
     ;
     ; Destroy all windows and perform any cleanup actions.
     ;
     ; This can be called from both the main application or internally.
     ;
     ; NOTE: This will always get called when the addon object is
     ;       destroyed.
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::DISABLE

       ; Call the parent method.
       self.BST_INST_ADDON::DISABLE

       IF self.IS_ENABLED() THEN BEGIN
         fitter = self.dispatcher.GET_CORE()
         fitter.SET_SPECTRUM_FUNCTION, /DEFAULT
       ENDIF

     END ;PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::DISABLE

     
     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the state of addon.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_STATE
       
       state = self.BST_INST_ADDON::GET_STATE()
       state = CREATE_STRUCT(state, {param:self.param})

       RETURN, state

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_STATE



     ;+=========================================================================
     ; PURPOSE:
     ;   Load a save structure for this addon.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::LOAD_STATE, state

       self.BST_INST_ADDON::LOAD_STATE, state

       STRUCT_ASSIGN, {param:state.param}, self, /NOZERO

       ; For back compatability with older savefiles.
       IF HAS_TAG(state.param, 'SYSTEM_NAME') THEN BEGIN
         self.param.system = state.param.system_name
       ENDIF

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::LOAD_STATE


     ;+=========================================================================
     ; PURPOSE:
     ;   Return a structure with the addon parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_PARAM

       RETURN, self.param

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_PARAM


     ;+=========================================================================
     ; PURPOSE:
     ;   Load the addon parameters from a structure.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::SET_PARAM, param

       STRUCT_ASSIGN, {param:param}, self, /NOZERO

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::SET_PARAM


     ;+=================================================================
     ; PURPOSE:
     ;   Perform actions before the fit is started.
     ;
     ;   This routines loads the data  files into
     ;   the [BST_INST_MODEL_XCRYSTAL::] model.
     ;
     ; DESCRIPTION:
     ;   In general a single XICS 'system' may have multiple overlaping
     ;   spectra.  Each spectra requires a separate model and
     ;   possibly also a different geometry configuration.
     ;
     ;   This is my first attempt at generalization.  It may be better
     ;   to implements this in the xcrystal spectra addon,
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::BEFORE_FIT

       ; Set the model using the current parameters.
       self.SET_MODELS
       spectrum_param = self.GET_PARAM()
       
       FOREACH model, self.models DO BEGIN
         IF model.USE() THEN BEGIN
           ; Copy the spectrum parames over to the model.
           spectrum = model.options.spectrum
           CASE 1 OF
             STRPOS(spectrum_param.system, 'w7x_qsx') GE 0: BEGIN
               system = 'w7x_qsx_'+spectrum
             END
             STRPOS(spectrum_param.system, 'w7x') GE 0: BEGIN
               system = 'w7x_'+spectrum
             END
             STRPOS(spectrum_param.system, 'lhd') GE 0: BEGIN
               system = 'lhd_'+spectrum
             END
             ELSE: BEGIN
               system = spectrum_param.system
             END
           ENDCASE
           
           model.SET_SPECTRUM_PARAM, spectrum_param
           model.SET_OPTIONS, {system:system}
           model.LOAD_GEOMETRY_OBJECT
         ENDIF
       ENDFOREACH

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::BEFORE_FIT



; ==============================================================================
; ==============================================================================
; ######################################################################
;
; SPECTRUM METHODS
;    These are the methods that <BST_SPECTRAL_FIT> will actually
;    use to get the spectra.
;
; ######################################################################
; ==============================================================================
; ==============================================================================



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Setup the model that will be used.
     ;
     ; PROGRAMMING NOTES: 
     ;   For the time being I am allowing several different names to be used
     ;   for the same system, e.g. w7x_fe24 and w7x_ar17.  This is temporary
     ;   solution that allows much of my code base, which was built around
     ;   the idea of 'one system, one geometry, one model', to be used without
     ;   modification. It turns out that I need multiple models, and possibly 
     ;   multiple geometries; in the future I'll need to think about how
     ;   to better implement this accross all my code.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::SET_MODELS, models
       COMPILE_OPT STRICTARR

       IF ISA(models) THEN BEGIN
         self.models = models
       ENDIF ELSE BEGIN
         IF self.param.system EQ '' THEN BEGIN
           MESSAGE, 'System name has not been set. Cannot determine models.'
         ENDIF

         models = MIR_LIST()
         CASE self.param.system OF
           'lhd_ar16': models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR16')
           'w7x_ar16': models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR16')
           'lhd_ar17': models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR17')
           'w7x_ar17': BEGIN
             models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR17')
             models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_FE24')
           END
           'w7x_fe24': BEGIN
             models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_FE24')
             models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_AR17')
           END
           'w7x_qsx': BEGIN
             models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_FE24')
           END
           'w7x_qsx_fe24': BEGIN
             models.ADD, (self.dispatcher).GET_MODEL('BST_INST_MODEL_XCRYSTAL_FE24')
           END
           ELSE: BEGIN
             MESSAGE, 'System name is not recognized. Cannot determine appropriate model.'
           END
         ENDCASE

         self.models = models
       ENDELSE
       
     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::SET_MODELS


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Retrieve the current model.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_MODELS, models
       COMPILE_OPT STRICTARR

       RETURN, self.models
       
     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_MODELS

     
     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Load the data from the save file.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::LOAD_XICS_DATA
       COMPILE_OPT STRICTARR

       self.data_object.LOAD_XICS_DATA
       
     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::LOAD_XICS_DATA



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Clear the XICS data cache.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::CLEAR_XICS_DATA
       COMPILE_OPT STRICTARR

       self.data_object.CLEAR_XICS_DATA

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::CLEAR_XICS_DATA



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Get the raw data from the spectrometer.
     ;
     ;   Used cached data if available.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_DATA, POINTER=pointer
       COMPILE_OPT STRICTARR

       RETURN, self.data_object.GET_DATA(POINTER=pointer)
       
     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_DATA

     
     ;+=========================================================================
     ; PURPOSE:
     ;   Returns a wavelength axis for this spectrum.
     ;
     ; DESCRIPTION: 
     ;   For the XCRYSTAL model, the dispersion is dependent on the model
     ;   parameters.  This complicates the process of plotting with the proper
     ;   wavelength scale.
     ;
     ;   Actually I don't think this is true anymore.  I used to include some
     ;   of the geometry as part of the model, but I think I have removed all
     ;   of that now.  I should revisit this.  Novimir, 2013-02-21.
     ;
     ; PROGRAMMING NOTES:
     ;   Right now for this to work the model object must be specifed 
     ;   (self.model).  This will be done by the XCRYSTAL addons during the
     ;   [::BEFORE_FIT] method.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_SPECTRUM_WAVELENGTH, channel
       
       CATCH, error                   
       IF (error NE 0) THEN BEGIN
         self.MESSAGE, 'Could not determine wavelength scale.', /ERROR
         RETURN, []
       ENDIF

       IF ~ OBJ_VALID(self.models) THEN BEGIN
         self.MESSAGE, 'Wavelength scale not available unless a model has been specified.', /DEBUG
         RETURN, []
       ENDIF
       
       ; Choose the first model that is in use and get the wavelength from
       ; there. In general it should not matter which model is chosen since I
       ; expect them to all share a common wavelength axis.
       ;
       ; Anyway this is just for plotting, so it really shouldn't matter.
       FOREACH model, self.models DO BEGIN
         IF model.USE() THEN BEGIN
           wavelength = model.CHANNELS_TO_ANGSTROMS(channel)
           RETURN, wavelength
         ENDIF
       ENDFOREACH

       ; By default do not return any wavelength scale
       self.MESSAGE, 'Could not determine wavelength scale.', /ERROR
       RETURN, []

     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::GET_SPECTRUM_WAVELENGTH


     ;+=========================================================================
     ;
     ; Returns a spectrum from a file.
     ;
     ; NOTE:
     ;   The wavelength information that is returned by this function is not
     ;   used by the XCRYSTAL (XICS) models.  There are important reasons 
     ;   for the models to handle their own conversion between channel and
     ;   wavelength.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::SPECTRUM


       ; Establish catch block.
       CATCH, error                   
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         self.MESSAGE, /ERROR
         MESSAGE, STRING('Could not retrieve XCRYSTAL_SPECTRA spectrum.') 
       ENDIF


IF 1 THEN BEGIN

       spectrum = self.data_object.GET_SPECTRUM(PARAM=self.param)

       ; Get the wavelength information if possible.
       ; I have now added the ability to get the wavelength directly from the
       ; XC_DATA object in call to GET_SPECTRUM. I belive that I can just
       ; use that spectrum here, but I need to check first whether there are
       ; any cases in which using the geometry saved in the model is
       ; important here.  (I don't think it is.)
       ;
       ; -- Novimir 2018-07-11
       wavelength = self.GET_SPECTRUM_WAVELENGTH(spectrum.x)

       ; Create a default value for w_inrange
       w_inrange = INDGEN(N_ELEMENTS(spectrum.x))

       ; Create a mask for the data that is in range.
       IF ISA(wavelength) THEN BEGIN
         ; Only return the portion of the spectrum that in within the given range.
         ; This can only be done if wavelength information is available.
         IF ~ARRAY_EQUAL(self.param.wavelength_range, 0) THEN BEGIN
           w_inrange = WHERE(wavelength GT self.param.wavelength_range[0] $
                             AND wavelength LT self.param.wavelength_range[1] $
                             ,count_inrange)

           IF count_inrange EQ 0 THEN BEGIN
             MESSAGE, STRING('No points in the spectrum are within the specified wavelengthrange.')
           ENDIF
         ENDIF
       ENDIF

       ; Rewrite the spectrum array with wavelength information and masked to
       ; the approprate wavelength range.
       spectrum = { $
                  x:spectrum.x[w_inrange] $
                  ,y:spectrum.y[w_inrange] $
                  ,wavelength:wavelength[w_inrange] $
                  ,weight:spectrum.weight[w_inrange] $
                  
                  ,photons_per_count:spectrum.photons_per_count[w_inrange] $
                  ,sigma_dark:spectrum.sigma_dark[w_inrange] $
                  }
       
ENDIF

         
; TEMPORARY ====================================================================
;   This is here just for some work that I am doing with Eric Wang on
;   deconvolution of the the XICS spectra.
;
;   I couldne't use the user spectra addon since I wanted to get the wavelengh
;   from the xcrystal model.
IF 0 THEN BEGIN
       ;filename = "/u/npablant/temp/2012-02-19_eric_wang_deconvolved_spectra__z.sav"
       filename = '/u/npablant/analysis/xics/cmod_helium/1070614011/data/2013-02-25_Eric_Wang_deconvolved_spectra__z8.sav'


       MIR_LOGGER, /INFO, "WARNING: Temporarily loading spectrum from Savefile."
       MIR_LOGGER, /INFO, "         "+filename

       RESTORE, FILENAME=filename

       ; Add wavelength information if possible.
       wavelength = self.GET_SPECTRUM_WAVELENGTH(spectrum.x)
       IF ISA(wavelength) THEN BEGIN
         spectrum.wavelength = wavelength
       ENDIF
ENDIF
; TEMPORARY ====================================================================



       RETURN, spectrum


     END ; FUNCTION BST_INST_ADDON_XCRYSTAL_SPECTRA::SPECTRUM




; ==============================================================================
; ==============================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ==============================================================================
; ==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   Clean up on object destruction.
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::CLEANUP

       self.CLEAR_XICS_DATA

       self.BST_INST_ADDON::CLEANUP

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA::CLEANUP


     ;+=========================================================================
     ;
     ; Define the object as a subclass of [BST_INST_ADDON]
     ;
     ;-=========================================================================
     PRO BST_INST_ADDON_XCRYSTAL_SPECTRA__DEFINE

       struct = { BST_INST_ADDON_XCRYSTAL_SPECTRA $

                  ,data_object:OBJ_NEW() $ 

                  ,models:OBJ_NEW() $

                  ,param:{ BST_INST_ADDON_XCRYSTAL_SPECTRA__PARAM $
                           ,shot:0L $
                           ,config_shot:0L $
                           ,system:'' $

                           ,ts_min:0L $
                           ,ts_max:0L $
                           
                           ,y_min:0L $
                           ,y_max:0L $

                           ,wavelength_range:[0D, 0D] $
                         } $

                  ,INHERITS BST_INST_ADDON }

     END ; PRO BST_INST_ADDON_XCRYSTAL_SPECTRA__DEFINE

