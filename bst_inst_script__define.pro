
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;  Scripting interface for BST_SPECTRAL_FIT.
;
; DESCRIPTION:
;  It is usefull in certain cases to be able to control BST_SPECTRAL_FIT using
;  an external script.  Doing this however can turn out to be somewhat complex
;  given the modularity and complexity of the application.
;
;  This scripting interface is meant to simplify scripting by presenting a
;  simple interface to certain functionality of BST_SPECTRAL_FIT.
;
;  This scripting interface assumes that the the full application will
;  be used. The use of the gui is optional.
;  
;
;-==============================================================================





     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Initialize the script object.
     ;
     ;   An existing 'BST_INST_FIT_GUI' object can be given, otherwise one
     ;   will be created.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::INIT, GUI=gui, _REF_EXTRA=_extra
       COMMON BST_SPECTRAL_FIT, c_fitter
       
       MIR_DEFAULT, gui, 0
       
       IF ~ OBJ_VALID(fitter) THEN BEGIN
         IF gui THEN BEGIN
           fitter_gui = OBJ_NEW('BST_INST_FIT_GUI', _STRICT_EXTRA=_extra)
           fitter_app = fitter_gui.GET_APP()
         ENDIF ELSE BEGIN
           fitter_app = OBJ_NEW('BST_INST_FIT_APP', _STRICT_EXTRA=_extra)
         ENDELSE
       ENDIF

       self.fitter_app = fitter_app
       c_fitter = fitter_app

       RETURN, 1

     END ;FUNCTION BST_INST_SCRIPT::INIT



     ;+=========================================================================
     ;
     ; Do a fit using the current parameters.
     ;
     ;-=========================================================================
     PRO BST_INST_SCRIPT::FIT

       self.fitter_app.FIT

     END ; PRO BST_INST_SCRIPT::FIT



     ;+=========================================================================
     ;
     ; Load a save state file.
     ;
     ;-=========================================================================
     PRO BST_INST_SCRIPT::LOAD, filepath, _REF_EXTRA=_extra

       self.fitter_app.LOAD, filepath, _STRICT_EXTRA=_extra

     END ; PRO BST_INST_SCRIPT::LOAD


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Get fit results.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_FIT_RESULTS

       core = self.fitter_app.GET_CORE()
       
       RETURN, core.fitter.GET_RESULTS()

     END ; FUNCTION BST_INST_SCRIPT::GET_FIT_RESULTS


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Get fit results.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_SPECTRUM

       core = self.fitter_app.GET_CORE()
       
       RETURN, core.spectrum.GET_SPECTRUM()

     END ; FUNCTION BST_INST_SCRIPT::GET_FIT_RESULTS


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Return an object reference for a given model.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_MODEL, model_name
       
       RETURN, self.fitter_app.GET_MODEL(model_name)

     END ; FUNCTION BST_INST_SCRIPT::GET_MODEL



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Return an object reference for a given addon.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_ADDON, addon_name
       
       RETURN, self.fitter_app.GET_ADDON(addon_name)

     END ; FUNCTION BST_INST_SCRIPT::GET_ADDON


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Return an object reference to the fitter core
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_CORE

       core = self.fitter_app.GET_CORE()
       
       RETURN, core

     END ; FUNCTION BST_INST_SCRIPT::GET_CORE


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Return an object reference to the fitter app
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_APP
       
       RETURN, self.fitter_app

     END ; FUNCTION BST_INST_SCRIPT::GET_APP


     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Return an object reference to the fitter gui.
     ;
     ;   if the GUI is inactive, this will return an empty object.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT::GET_GUI
       
       RETURN, self.fitter_gui

     END ; FUNCTION BST_INST_SCRIPT::GET_GUI
     

     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Update the main GUI and all the addons.
     ;
     ;-=========================================================================
     PRO BST_INST_SCRIPT::UPDATE

       self.fitter_app.UPDATE

     END ; PRO BST_INST_SCRIPT::UPDATE



     ;+=========================================================================
     ;
     ; Define the object.
     ;
     ;-=========================================================================
     PRO BST_INST_SCRIPT__DEFINE

       struct = { BST_INST_SCRIPT $
                           
                  ,fitter_gui:OBJ_NEW() $
                  ,fitter_app:OBJ_NEW() $

                  ,INHERITS MIR_OBJECT }

     END ; PRO BST_INST_SCRIPT__DEFINE
