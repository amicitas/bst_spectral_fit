
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-10
;
; PURPOSE:
;   This program is designed to fit a spectra.
;   The basic model uses a sum of Gaussians plus background parameters.
;
; DESCRIPTION:
;   There is a GUI, <BST_INST_FIT_GUI__DEFINE> that is meant to be 
;   used with this spectral fitter.  To start the fitter with the
;   GUI, use the routine <BST_SPECTRAL_FIT>.
;
;   The fitting core can be used without the GUI.
; 
;
;   This was originaly built to be used to fit spectra for the
;   B-Stark and CER diagnostics at DIII-D.  It replaces the old
;   instrumental responce fitting code CINSTR.
;
;   Options and Configuration:
;     This program has both a 'configuration' and an 'options' specification.
;     The configuration is meant to be used only for specification of which
;     addons, guiaddons and models should be loaded and made active.
;     The options should contain everything else.
;     Finally, note that each addon and model will generally have it's own
;     set of options.
;
; OPTIONS:
;   User defined options can be loaded using the method SET_OPTIONS_USER.
;
;   For the moment options need to be loaded into the application and
;   each addon and model separately.  There is no centralized way
;   to save or load everything at once.  This should be updated.
;   
; CONFIGURATION:
;   A user configuration file can be created to specify which
;   models, addons and guiaddons will be active.
;   This file can be specifed using the environmental variable
;   'BST_SPECTRAL_CONFIG'.  Otherwise the default location:
;   '~/.bst_spectral/bst_spectral.config' will be checked.
;   These files will overide the default confguration which is found
;   in the installation directory and always loaded.
;
;
; PROGRAMING NOTES:
;   
;   The Main Fit Loop
;
;   Here is a diagram showing how the main fit loop works. This is what
;   takes place when <::QUICKFIT()> is called.
;
;     QUICKFIT
;       BEFORE_FIT
;         spectrum->LOAD_SPECTRUM
;         models->SAVE_INITIAL_STATE
;   
;       PREFIT
;         models->PREFIT
;         models->PREFIT_CONSTRAINTS
;   
;       FIT
;   
;       AFTER_FIT
;         models->RESTORE_INITAL_STATE
;         
;
;   An application that uses <BST_INST_FIT::> can insert commands
;   inbetween the various parts of the fit loop.
;
;
; CHANGELOG:
;   A change log can be found in the file CHANGELOG.
;
;   All changes are tracked using a subversion repository.Detailed 
;   changes and commit comments are available through the
;   repository, this should be considered the main record of changes.
;
; TO DO:
;   A todo list can be found in the file TODO.
;
;-======================================================================




     ;+=================================================================
     ;
     ; Return the current version of <BST_SPECTRAL_FIT>
     ;
     ; A single version number is used to track the main program as well
     ; as all supported addons and models.
     ;
     ;
     ; Change in:
     ;   Major: Dramatic changes in program structure or behavior.
     ;          Save files from a a different major version cannot
     ;          (or should not) be loaded.  
     ;
     ;   Minor: Significant changes in program or model behavior.
     ;          Save files from a different minor version can probably
     ;          be still be loaded, but may be incomplete.  
     ;
     ;   Revision: Only minor changes to program or model behavior.
     ;             Save files from a different revsision can be safely
     ;             loaded.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::VERSION

       version_number = [4,3,6]
       version_date   = [2019, 04, 04]


       version = {VERSION}
       version.number = version_number
       version.date = version_date

       RETURN, version

     END ;FUNCTION BST_INSTFIT::VERSION



     ;+=================================================================
     ;
     ; Initialize the BST_INST_FIT object
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::INIT, _REF_EXTRA=extra


       RESOLVE_ROUTINE, [ $
                           'bst_inst_model_dispatch__define' $
                          ,'bst_inst_spectrum__define' $
                          ,'bst_inst_curvefit__define' $
                          ,'bst_inst_fit_profiles__define' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE


       ; Initialize the version number.
       self.version = self->VERSION()
       
       ; Call the main initialization routine.
       self->INITIALIZE, _STRICT_EXTRA=extra


       RETURN, 1

     END ;FUNCTION BST_INSTFIT::INIT


     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize the <BST_SPECTRAL_FIT::> core.
     ;
     ; DESCRIPTION:
     ;   This routine sets up the internal objects and the default 
     ;   parameters and objects.
     ;
     ;   This method will generally be called when the BST_INST_FIT
     ;   object is first called.  It may also be called to reset the
     ;   fitter state.
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE, CONFIGURATION=configuration $
                                   ,_REF_EXTRA=extra              

       ; Establish catch block
       CATCH, error    
       IF (error NE 0) THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, /ERROR
         MESSAGE, 'Could not initialize fitter.'
       ENDIF 


       ; NOTE: The order of the calls here is important.
       ;       In particular the parameters must must be initialized
       ;       before the models. Also the debug options should be
       ;       initialized first.

       ; Set up the default debugging flags.
       self->INITIALIZE_DEBUG, _STRICT_EXTRA=extra

       ; Initialize internal parameters
       self->INITIALIZE_PARAM, CONFIGURATION=configuration

       ; Initialize the log.
       self->INITIALIZE_LOG

       ; Initialize default options.
       self->INITIALIZE_OPTIONS_INTERNAL
       self->INITIALIZE_OPTIONS_USER

       ; Initialize the model object.
       self->INITIALIZE_MODELS

       ; Initialize the profiles object.
       self->INITIALIZE_PROFILES

       ; Initialize the spectrum object.
       self->INITIALIZE_SPECTRUM

       ; Initialize the fitter core
       self->INITIALIZE_FITTER

     END ;PRO BST_INST_FIT::INITIALIZE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reset the fitter to the default state.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::RESET

       ; Initialize internal parameters
       self->INITIALIZE_PARAM

       ; Initialize default options.
       self->INITIALIZE_OPTIONS_INTERNAL
       self->INITIALIZE_OPTIONS_USER

     END ;PRO BST_INST_FIT::RESET



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize debug options for <BST_SPECTRAL_FIT>.
     ;
     ; PROGRAMMING NOTES:
     ;   Here are descriptions of the available parameters:
     ;
     ;   debug
     ;       Messages set with /DEBUG will be displayed.
     ;
     ;   error
     ;       Messages set with /ERROR will be displayed.
     ;       
     ;   traceback
     ;       When a message given with /ERROR is encountered,
     ;       a full traceback will be displayed.
     ;  
     ;   last_error 
     ;       When a message given with /ERROR is encountered,
     ;       the last error will be displayed.
     ;
     ;   stop
     ;       When a message given with /ERROR is encountered,
     ;       the program execution will be stopped.
     ;
     ;   timer
     ;       Code profiling will be run.
     ;
     ;   models
     ;       Not currently used.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_DEBUG, DEBUG=debug

       self.debug.debug = 0
       self.debug.error = 1
       self.debug.traceback = KEYWORD_SET(debug)
       self.debug.last_error = 0
       self.debug.stop = 0
       self.debug.timer = 0
       self.debug.models = 0

     END ;PRO BST_INST_FIT::INITIALIZE_DEBUG



     ;+=================================================================
     ; PURPOSE:
     ;   Return the installation path for <BST_SPECTRAL_FIT>.
     ;
     ; DESCRIPTION:
     ;   This routine finds and saves the installation path of the
     ;   currently running instance of <BST_SPECTRAL_FIT>
     ;-=================================================================
     FUNCTION BST_INST_FIT::FIND_INSTALL_PATH

       ; Find the installation path.
       class = OBJ_CLASS(self)

       bst_inst_info = ROUTINE_INFO(class+'__define', /SOURCE)
       install_path = FILE_DIRNAME(bst_inst_info.path)


       RETURN, install_path

     END ;FUNCTION BST_INST_FIT::FIND_INSTALL_PATH



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize default parameters for <BST_SPECTRAL_FIT>.
     ;
     ; PROGRAMMING NOTES:
     ;   Here are descriptions of the available parameters:
     ;
     ;   message_pro
     ;       The name of a messaging routine to which messages should
     ;       be sent.  If not given, all output will be to the terminal.
     ;
     ;   install_path
     ;       The location of this instance of <BST_SPECTRAL_FIT>.
     ;
     ;   configuration
     ;       A set of user defined configuration options read from a
     ;       configuration file.
     ;
     ;   spectrum_function  
     ;       The name of the function that will return the spectrum to 
     ;       be fit.                 
     ;
     ;   spectrum_object
     ;       If given, then spetrum_fuction will be assumed to be a
     ;       method of the object given here.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_PARAM, _REF_EXTRA=_extra

       self.param.message_pro = 'MIR_LOGGER'

       self.param.install_path = self->FIND_INSTALL_PATH()


       self.param.spectrum_function = ''
       self.param.spectrum_object = OBJ_NEW()

       self->INITIALIZE_CONFIGURATION, _STRICT_EXTRA=_extra


     END ;PRO BST_INST_FIT::INITIALIZE_PARAM



     ;+=================================================================
     ; PURPOSE:
     ;   Read the user defined configuration file.
     ;
     ; DESCRIPTION:
     ;   This routine will first check if the environmental variable
     ;   'BST_SPECTRAL_CONFIG' is set, if so it will use the file
     ;   defined there.  Otherwise it will try the default location:
     ;   '~/.bst_spectral/bst_spectral.config'.  If neither of those
     ;   files exist, it will use the file in in the instalation path.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_CONFIGURATION, CONFIGURATION=configuration


       ; Establish catch block
       CATCH, error    
       IF (error NE 0) THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Could not read configuration file.', /ERROR

         RETURN
       ENDIF 

       ; Clear the configuration.
       self.param.config = MIR_HASH()

       
       ; First load the default configuration.
       config_file = MIR_PATH_JOIN([self.param.install_path, 'bst_spectral.config'])
       IF FILE_TEST(config_file) THEN BEGIN
         self.MESSAGE, /INFO, 'Reading configuration from: '+config_file
         config_struct = MIR_READ_INI(config_file) 
         self.SET_CONFIG, config_struct
       ENDIF

       
       ; Look for a user configuration.
       config_file = GETENV('BST_SPECTRAL_CONFIG')

       IF config_file EQ '' THEN BEGIN
         config_file = '~/.bst_spectral/bst_spectral.config'
       ENDIF
       
       IF FILE_TEST(config_file) THEN BEGIN
         ; Now we have the proper file.  Read the configuration file.
         self.MESSAGE, /INFO, 'Reading configuration from: '+config_file
         config_struct = MIR_READ_INI(config_file) 

         IF ISA(config_struct) THEN BEGIN
           self.SET_CONFIG, config_struct
         ENDIF
       ENDIF

       ; Lastly load any runtime configuration passed in.
       IF ISA(configuration) THEN BEGIN
         self.SET_CONFIG, configuration
       ENDIF

     END ;PRO BST_INST_FIT::INITIALIZE_CONFIGURATION
     


     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize the logging system
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_LOG

       MIR_LOGGER, /WARNING, ''
       MIR_LOGGER, /WARNING, 'BST_INST_FIT::INITIALIZE_LOG not yet implemented.'
       MIR_LOGGER, /WARNING, ''


     END ;PRO BST_INST_FIT::INITIALIZE_LOG



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize default parameters for <BST_SPECTRAL_FIT>.
     ;
     ; PROGRAMMING NOTES:
     ;   Here are descriptions of the available parameters:
     ;
     ;   xrange   
     ;       The x range of the spectrum over which to do the fit
     ;
     ;   path
     ;       The current input/output path will be saved here.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_OPTIONS_INTERNAL

       self.options_internal.path = ''

     END ;PRO BST_INST_FIT::INITIALIZE_OPTIONS_INTERNAL



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize default options for <BST_SPECTRAL_FIT>.
     ;
     ; PROGRAMMING NOTES:
     ;   Here are descriptions of the available options:
     ;
     ;   comment
     ;       A comment that can be used to specify any user provided
     ;       information about this particual fit.
     ;
     ;   positive_amplitude
     ;       If TRUE, then the amplitudes of certain portions of
     ;       spectral models will be kept positive.
     ;       In particular this will force all gaussians in the
     ;       in the GAUSSIAN model to stay positive.  Other models
     ;       may also make use of this option.
     ;
     ;   positive_background
     ;       When set the background will be forced to stay positive.
     ;       This is not currently implemented.
     ;
     ;   evaluation_cutoff
     ;       If TRUE, the evaluation of the models will only be
     ;       performed where the result will be greater than
     ;       1E-16.  Otherwise evaluation will be done to machine
     ;       accuracy (1E-308).  For most cases turing this option on
     ;       will provide greater speed with no significant difference
     ;       in the final fit.
     ;
     ;   mpfit_ftol
     ;   mpfit_xtol
     ;   mpfit_gtol
     ;   mpfit_maxiter
     ;       These are options that control the convergence criteria
     ;       for MPFIT, which is used for the actual fitting.  See the
     ;       MPFIT documentaiton for descriptions of these options.
     ;    
     ;       By default these are explicitly set to the MPFIT default
     ;       values.
     ;
     ;   spectrum_weights_from_model
     ;       If TRUE, weighting of the spectrum will be calculated from
     ;       the spectral model, instead of from the spectrum itself.
     ;       Weighting is calculated assuming gaussian counting 
     ;       statistics.  The weighting is recalculated every interation
     ;       of the fit.  
     ; 
     ;       This option was introduced to deal with the issue of 
     ;       calculating appropriate weighting for spectra with a very 
     ;       small number of counts per channel.  In these circumstances 
     ;       the error for channels with low counts can be significantly 
     ;       underestimated if the the spectrum is used to calculate the 
     ;       weights (the error for spectra with high counts will be 
     ;       overestimated).  So long as the spectral model is accurate
     ;       using the model to determine the errors will eventually 
     ;       give a more accurate error estimate.
     ;
     ;       This method always assumes Gaussian statistics, which will
     ;       be inaccurete at very small counts (N<10) where the 
     ;       difference from Poisson statistics becomes important.
     ;       This is still better than trying to use the measured
     ;       spectrum, but does not really produce correct fits at
     ;       very low signal levels.
     ;
     ;   disable_gui_update
     ;       Turn off any updating of of the gui during the fitting
     ;       procedure. This is of limited use now that the gui has
     ;       been separated from the core application.
     ;
     ;   use_curvefit
     ;       If TRUE, then CURVEFIT will be be used as the non-linear
     ;       least squares fitter.  By default MPCURVEFIT is used.
     ;       This option is depretiated, and is no longer mantained.
     ;       Don't use this.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_OPTIONS_USER

       self.options_user.comment = ''
       
       self.options_user.positive_amplitude = 1
       self.options_user.positive_background = 1
       
       self.options_user.evaluation_cutoff = 1
       self.options_user.xrange = [0L,0L]

       self.options_user.mpfit_ftol = 1D-10
       self.options_user.mpfit_xtol = 1D-10
       self.options_user.mpfit_gtol = 1D-10
       self.options_user.mpfit_maxiter = 200
       
       self.options_user.spectrum_weights_from_model = 0
       
       self.options_user.disable_gui_update = 0

       ; Depretiated don't use this.
       self.options_user.use_curvefit = 0
       
     END ;PRO BST_INST_FIT::INITIALIZE_OPTIONS_USER




     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> models object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_MODELS
       self->MESSAGE, 'Initializing models.', /STATUS

       IF OBJ_VALID(self.models) THEN BEGIN
         self.models->DESTROY
       ENDIF

       self.models = OBJ_NEW('BST_INST_MODEL_DISPATCH', self)

     END ;PRO BST_INST_FIT::INITIALIZE_MODELS



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> profiles object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_PROFILES

       IF OBJ_VALID(self.profiles) THEN BEGIN
         self.profiles->DESTROY
       ENDIF

       self.profiles = OBJ_NEW('BST_INST_FIT_PROFILES', self)

     END ;PRO BST_INST_FIT::INITIALIZE_PROFILES



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> spectrum object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_SPECTRUM

       IF OBJ_VALID(self.spectrum) THEN BEGIN
         self.spectrum->DESTROY
       ENDIF

       self.spectrum = OBJ_NEW('BST_INST_SPECTRUM', self)

     END ;PRO BST_INST_FIT::INITIALIZE_SPECTRUM



     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> fitter object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::INITIALIZE_FITTER

       IF OBJ_VALID(self.fitter) THEN BEGIN
         self.fitter->DESTROY
       ENDIF

       ;self.fitter = OBJ_NEW('BST_INST_CURVEFIT', self)
       self.fitter = OBJ_NEW('BST_INST_MPFIT', self)

     END ;PRO BST_INST_FIT::INITIALIZE_FITTER



     ;+=================================================================
     ; PURPOSE:
     ;   Perform a fit with the current model parameters.
     ;
     ; DESCRIPTION:
     ;   A fitting application may not want to call this directly,
     ;   but rather use this routine as a template and add in
     ;   additional applicaiton specific commands.  
     ; 
     ;   As an example see [BST_INST_FIT_GUI] which also initializies 
     ;   a set of addons.
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_FIT::QUICKFIT


       ; Establish catch block
       CATCH, error    
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL

         ; Clean up after a failure.
         self->POSTERROR

         self->MESSAGE, /ERROR
         MESSAGE, 'Error in fit loop.'
       ENDIF 
         

       ; Setup for the fit.
       self->BEFORE_FIT

       ; Take prefit actions.
       self->PREFIT

       ; Do the fit
       self->FIT

       ; Take any post fit actions.
       self->POSTFIT

       ; Clean up after the fit.
       ;
       ; NOTE: This will restore the inital state of the models
       ;       unless save was specified.  Any analysis should
       ;       be done before this is called.
       self->AFTER_FIT


     END ; PRO BST_INST_FIT::QUICKFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Setup for the fit.
     ;
     ; DESCRIPTION:
     ;   A number of setup actions are necessary before the fit
     ;   is started.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::BEFORE_FIT
       self->MESSAGE, 'CORE: Setting up for fit.', /DEBUG

       ; Get the spectrum
       self->MESSAGE, 'CORE: Loading spectrum.', /DEBUG
       self.spectrum->LOAD_SPECTRUM

       ; Make a copy of the current model state.
       self.models->SAVE_INITIAL_STATE

     END ;PRO BST_INST_FIT::PREFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Take any prefit actions.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::PREFIT
       self->MESSAGE, 'CORE: Performing prefit.', /DEBUG

       ; Set any global user options that should overide the model options.
       self.models->SET_USER_OPTIONS

       self.models->PREFIT
       self.models->PREFIT_CONSTRAINTS

     END ;PRO BST_INST_FIT::PREFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Start the fit loop.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::FIT

       ; Establish catch block
       CATCH, error    
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL

         self->MESSAGE, /ERROR
         MESSAGE, 'The fit has failed.'
       ENDIF 

       ; Do the fit
       self->MESSAGE, 'CORE: Fitting.', /DEBUG
       self.fitter->FIT

     END ;PRO BST_INST_FIT::FIT



     ;+=================================================================
     ; PURPOSE:
     ;   Take any post-fit actions
     ;
     ;-=================================================================
     PRO BST_INST_FIT::POSTFIT

       self->MESSAGE, 'CORE: Performing POSTFIT.', /DEBUG

       self.models->POSTFIT

     END ;PRO BST_INST_FIT::POSTFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Take any post-fit actions
     ;
     ;-=================================================================
     PRO BST_INST_FIT::AFTER_FIT
       self->MESSAGE, 'CORE: Performing AFTER_FIT.', /DEBUG

       ; Restore the initial state of the models,
       ; but update any parmeters with the 'save' flag.
       self.models->RESTORE_INITIAL_STATE, /UPDATE_PARAMETERS

       ; Remove the initial model state.
       self.models->REMOVE_OBJECTS_INITIAL

     END ;PRO BST_INST_FIT::AFTER_FIT



     ;+=================================================================
     ; PURPOSE:
     ;   Take action after a failed fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::POSTERROR

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         RETURN
       ENDIF

       self->MESSAGE, 'CORE: Cleaning up after failed fit.', /INFO

       IF self.models.objects_initial->NUM_ENTRIES() NE 0 THEN BEGIN

         ; Restore the initial model state.
         self->RESTORE_INITIAL_STATE

         ; Remove the initial model state.
         self->REMOVE_OBJECTS_INITIAL

       ENDIF

     END ;PRO BST_INST_FIT::POSTERROR




     ;+=================================================================
     ; PURPOSE:
     ;   Perform any analysis on the fit results.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::ANALYZE_RESULTS

       ; Extract the profiles. And calculate properties.
       self->MESSAGE, 'Calculating profile parameters.', /DEBUG
       ; First get the profiles
       self.profiles->GET_PROFILES

       ; Now calculate the fwhm and other nice things.
       self.profiles->FWHM


     END ;PRO BST_INST_FIT::ANALYZE_RESULTS



     ;+=================================================================
     ; PURPOSE:
     ;   Start the profiler if desired.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::PROFILER_START

       self.SET_OPTIONS_DEBUG, {timer:1}
       MIR_TIMER, /RESET

     END ;PRO BST_INST_FIT::PROFILER_START



     ;+=================================================================
     ; PURPOSE:
     ;   Stop the profiler and display the results if it was started.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::PROFILER_STOP

       MIR_TIMER, /REPORT
       self.SET_OPTIONS_DEBUG, {timer:0}

     END ;PRO BST_INST_FIT::PROFILER_STOP

     
     ;+=================================================================
     ; PURPOSE:
     ;   Clear all cached values. Also applies to MODELS and ADDONS.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::CLEAR_CACHE

       self.models->CLEAR_CACHE

     END ;PRO BST_INST_FIT::CLEAR_CACHE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Here we cleanup when destroying the BST_INST_FIT object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::CLEANUP

MIR_LOGGER, /DEBUG, 'Starting: BST_INST_FIT::CLEANUP'
HEAP_GC, /VERBOSE

       self.models->DESTROY
MIR_LOGGER, /DEBUG, 'models'
HEAP_GC, /VERBOSE

       self.spectrum->DESTROY
MIR_LOGGER, /DEBUG, 'spectrum'
HEAP_GC, /VERBOSE

       self.fitter->DESTROY
MIR_LOGGER, /DEBUG, 'fitter'
HEAP_GC, /VERBOSE

       self.profiles->DESTROY
MIR_LOGGER, /DEBUG, 'profiles'
HEAP_GC, /VERBOSE

     END ;PRO BST_INST_FIT::CLEANUP



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Sets the procedure to use for message handling.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_MESSAGE_PRO, message_pro

       self.param.message_pro = message_pro

     END ;PRO BST_INST_FIT::SET_MESSAGE_PRO



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle debug flags.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_OPTIONS_DEBUG, struct

       debug = TEMPORARY(self.debug)
       STRUCT_ASSIGN, struct, debug, /NOZERO
       self.debug = TEMPORARY(debug)

     END ;PRO BST_INST_FIT::SET_OPTIONS_DEBUG



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the user options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_OPTIONS_DEBUG

       RETURN, self.debug

     END ;FUNCTION BST_INST_FIT::GET_OPTIONS_DEBUG



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Seth the internal options
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_OPTIONS_INTERNAL, struct

         options_internal = TEMPORARY(self.options_internal)
         STRUCT_ASSIGN, struct, options_internal, /NOZERO
         self.options_internal = TEMPORARY(options_internal)

     END ;PRO BST_INST_FIT::SET_OPTIONS_INTERNAL



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the user options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_OPTIONS_INTERNAL

       RETURN, self.options_internal

     END ;FUNCTION BST_INST_FIT::GET_OPTIONS_USER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Seth the user options
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_OPTIONS_USER, struct

       options_user = TEMPORARY(self.options_user)
       STRUCT_ASSIGN, struct, options_user, /NOZERO, /VERBOSE
       self.options_user = TEMPORARY(options_user)

     END ;PRO BST_INST_FIT::SET_OPTIONS_USER



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the user options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_OPTIONS_USER

       RETURN, self.options_user 

     END ;FUNCTION BST_INST_FIT::GET_OPTIONS_USER



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle debug flags.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_PARAM, struct

       param = TEMPORARY(self.param)
       STRUCT_ASSIGN, struct, param, /NOZERO
       self.param = TEMPORARY(param)

     END ;PRO BST_INST_FIT::SET_OPTIONS


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the configuration dictionary.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_CONFIG

       RETURN, self.param.config

     END ;FUNCTION BST_INST_FIT::GET_CONFIG


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Copy the given config hash to the configuration dictionary.
     ;
     ; PROGRAMMING NOTES:
     ;   This is written with the assumption that each section
     ;   contains only a single level of option.  That is, no nesting
     ;   of options is allowed within sections.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_CONFIG, config_in

       IF MIR_IS_STRUCT(config_in) THEN BEGIN
         config = MIR_HASH(config_in, /EXTRACT)
       ENDIF ELSE BEGIN
         config = config_in
       ENDELSE
         
       FOREACH value, config, key DO BEGIN
         IF MIR_IS_STRUCT(value) THEN BEGIN
           value = MIR_HASH(value, /EXTRACT)
         ENDIF
         
         IF self.param.config.HASKEY(key) THEN BEGIN
           section = (self.param.config)[key]
         ENDIF ELSE BEGIN
           section = MIR_HASH()
         ENDELSE
         
         section.JOIN, value
         (self.param.config)[key] = section
       ENDFOREACH

     END ;PRO BST_INST_FIT::SET_CONFIG


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the save state for the
     ;   [BST_INST_FIT::] object only.
     ;
     ; DESCRIPTION:
     ;   This will only return thet state of the fitter, not the
     ;   state of any of the models or addons.
     ;
     ;   Here we save any internal or user options.
     ;   We do not save the internal parameters or the debug flags.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_STATE_CORE

       RETURN, {options_internal:self.options_internal $
                ,options_user:self.options_user}

     END ; FUNCTION BST_INST_FIT::GET_STATE_CORE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Load the state of the [BST_INST_FIT::] object from a
     ;   saved state structure.  This will only load the state
     ;   of the fitter core.
     ;
     ; DESCRIPTION:
     ;   This will only load the state of the fitter, not the
     ;   state of any of the models or addons
     ;
     ;-=================================================================
     PRO BST_INST_FIT::LOAD_STATE_CORE, state

       self->SET_OPTIONS_INTERNAL, state.options_internal
       self->SET_OPTIONS_USER, state.options_user

     END ; PRO BST_INST_FIT::LOAD_STATE_CORE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the save state for
     ;   [BST_INST_FIT]
     ;
     ; DESCRIPTION:
     ;   This will return the state of the fitter and all components
     ;   and models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_STATE

       state = {version:self->GET_VERSION() $
                ,core:self->GET_STATE_CORE() $
                ,models:self.models->GET_STATE() $
               }

       RETURN, state

     END ; FUNCTION BST_INST_FIT::GET_STATE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Load the state of [BST_INST_FIT] from a saved state structure.
     ;
     ; DESCRIPTION:
     ;   This will load the state of the fitter and all components
     ;   and models.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::LOAD_STATE, state


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->RESET
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'CORE: Error reseting state.', /ERROR, /LOG
       ENDELSE

       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->LOAD_STATE_CORE, state.core
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'CORE: Error loading core state.', /ERROR, /LOG
       ENDELSE

       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.models->LOAD_STATE, state.models
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'CORE: Error loading models state.', /ERROR, /LOG
       ENDELSE


     END ; PRO BST_INST_FIT::LOAD_STATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the version structure.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_VERSION

       RETURN, self.version

     END ; FUNCTION BST_INST_FIT::GET_VERSION



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the installation path.  This assumes tha the path
     ;   has already be found and saved in the object poperties..
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_INSTALL_PATH

       RETURN, self.param.install_path

     END ; FUNCTION BST_INST_FIT::GET_INSTALL_PATH



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current path
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_CURRENT_PATH

       RETURN, self.options_internal.path

     END ; FUNCTION BST_INST_FIT::GET_CURRENT_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the current path
     ;
     ;-=================================================================
     PRO BST_INST_FIT::SET_CURRENT_PATH, path

       self.options_internal.path = path

     END ; PRO BST_INST_FIT::SET_CURRENT_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the reference for the model dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_MODEL_DISPATCHER

       RETURN, self.models

     END ; FUNCTION BST_INST_FIT::GET_MODEL_DISPATCHER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the reference for the model dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_MODEL, name
       model_dispatcher = self.GET_MODEL_DISPATCHER()
       
       RETURN, model_dispatcher->GET_MODEL(name)

     END ; FUNCTION BST_INST_FIT::GET_MODEL

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the spectrum from the current spectrum object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT::GET_SPECTRUM

       RETURN, self.spectrum->GET_SPECTRUM()

     END ; FUNCTION BST_INST_FIT::GET_SPECTRUM



     ;+=================================================================
     ; PURPOSE:
     ;   Allows the spectrum method, used by <BST_SPECTRAL_FIT>,
     ;   to be set.
     ;
     ;   It also allows this function to be reset to the last used
     ;   function or method, or to the default function.
     ;-=================================================================
     PRO BST_INST_FIT::SET_SPECTRUM_FUNCTION, function_name_in $
                                              ,object_ref_in $
                                              ,DEFAULT=default

       IF KEYWORD_SET(default) THEN BEGIN
         function_name = ''
         object_ref = OBJ_NEW()
       ENDIF ELSE BEGIN
         MIR_DEFAULT, function_name_in, ''
         MIR_DEFAULT, object_ref_in, OBJ_NEW()
         function_name = function_name_in
         object_ref = object_ref_in
       ENDELSE
       
       self->SET_PARAM, {spectrum_function:function_name}
       self->SET_PARAM, {spectrum_object:object_ref}

     END ;PRO BST_INST_FIT::SET_SPECTRUM_FUNCTION


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Sets the procedure to use for message handling.
     ;
     ;-=================================================================
     PRO BST_INST_FIT::MESSAGE, message_array $
                                ,SCOPE_LEVEL=scope_level_in $
                                ,_REF_EXTRA=extra

       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       MIR_LOGGER, message_array, SCOPE_LEVEL=scope_level, _EXTRA=extra

       ; This method is unfinished.
       ; I should be calling the procedure saved in self.param.message_pro 
       
     END ;PRO BST_INST_FIT::SET_MESSAGE_PRO

    
     ;+=================================================================
     ; PURPOSE:
     ;   Create the BST_INST_FIT object.
     ;
     ; PROGRAMMING NOTES:
     ;   Below are descriptions of the main data structures.
     ;   Detailed information can be found in the initialization
     ;   routines.
     ;   
     ;   param
     ;       These are internal parameters for [BST_INST_FIT::].
     ; 
     ;       Values set here are not saved in save files.
     ;
     ;   options_internal
     ;       These are options than can be changed internally or
     ;       by addons.  They should not be directly controled by the
     ;       user.
     ;  
     ;       These values are saved in save files.
     ;
     ;   options_user
     ;       These are options than can be directly controlled
     ;       by the user.  These should not be changed without
     ;       interaction with the user.
     ;
     ;       These values are saved in save files.
     ;  
     ;       spectrum_weights_from_model
     ;           Use the results from the spectral model to determine the
     ;           weighting of the point in the spectrum.
     ;
     ;           This can be useful when fitting spectra with very low
     ;           photon statistics.  There are however a number of
     ;           potential problems with this approtch.  In addition,
     ;           with very low photon statistics using a least-squares
     ;           fitting approtch is not actually valid.
     ;
     ;
     ;   debug
     ;       The currently active debugging flags.
     ;
     ;       Values set here are not saved in save files.
     ;      
     ;     
     ;   
     ;     
     ; 
     ;-=================================================================
     PRO BST_INST_FIT__DEFINE


       ; Define general fitting options.
       bst_inst_fit__options_user = $
          {BST_INST_FIT__OPTIONS_USER $
           ,spectrum_weights_from_model:0 $

           ,positive_amplitude:0 $
           ,positive_background:0 $

           ,evaluation_cutoff:0 $
           ,use_curvefit:0 $
           ,xrange:[0L,0L] $

           ,mpfit_ftol:0D $
           ,mpfit_xtol:0D $
           ,mpfit_gtol:0D $
           ,mpfit_maxiter:0 $

           ,disable_gui_update:0 $

           ,comment:'' $
          }


       
       ; Now define the BST_INST_FIT structure.
       bst_inst_fit = $
          { BST_INST_FIT $

            ,param:{BST_INST_FIT__PARAM $
                    ,message_pro:'' $
                    ,install_path:'' $
                    ,spectrum_function:'' $
                    ,spectrum_object:OBJ_NEW() $
                    ,config:OBJ_NEW() $
                   } $

            ,options_internal:{BST_INST_FIT__OPTIONS_INTERNAL $
                               ,path:'' $
                              } $

            ,options_user:{BST_INST_FIT__OPTIONS_USER} $

            ,debug:{BST_INST_FIT__DEBUG $
                    ,debug:0 $
                    ,models:0 $
                    ,error:0 $
                    ,traceback:0 $
                    ,last_error:0 $
                    ,stop:0 $
                    ,timer:0 $
                   } $

            ,version:{VERSION} $

            ,models:OBJ_NEW() $

            ,spectrum:OBJ_NEW() $

            ,fitter:OBJ_NEW() $

            ,profiles:OBJ_NEW() $

           ,INHERITS OBJECT $
          }
       
     END ;PRO BST_INST_FIT__DEFINE
