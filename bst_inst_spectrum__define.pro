
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-10
;
; PURPOSE:
;   Handle spectra for <BST_SPECTRAL_FIT>.
;
; TODO:
;   Should a pixel range with xrange[1] < xrange[0] be accepted?
;     Currently this will produce an error message.
;
;
;-==============================================================================




     ;+=========================================================================
     ; PURPOSE:
     ;   Return the stored spectrum
     ;
     ;-=========================================================================
     FUNCTION BST_INST_SPECTRUM::GET_SPECTRUM, RELOAD=reload

       IF KEYWORD_SET(reload) THEN BEGIN
         self->LOAD_SPECTRUM
       ENDIF

       IF ~ PTR_VALID(self.spectrum) THEN BEGIN
         MESSAGE, 'No spectrum is currently loaded.'
       ENDIF

       RETURN, *self.spectrum

     END ;FUNCTION BST_INST_SPECTRUM::GET_SPECTRUM


     ;+=========================================================================
     ; PURPOSE:
     ;   Get the spectrum that will be fit.
     ; 
     ; DESCRIPTION:
     ;    This does three things:
     ;      1. Get the spectrum from the specifed function or addon.
     ;      2. Extract the desired range.
     ;      3. Store the spectrum into a common block.
     ;   
     ;    This routine will save a spectrum with standard tags and
     ;    definitions.
     ;
     ;    The user defined function that returns the spectrum is 
     ;    expected to return a structure with the following tags:
     ;      x
     ;      y
     ;      weight
     ;   
     ;    The following optional tags will also be accepted.
     ;      wavelength
     ;      photons_per_count
     ;         A conversion factor to convert the y values into number 
     ;         of photons.  This can be different for every channel. 
     ;      sigma_dark
     ;         The error in each channel that is not from photon 
     ;         statistics, for example dark noise.
     ;   
     ;    The parameters 'photons_per_count' and 'sigma_dark' can be
     ;    used by the fitter to determine the spectral weights from
     ;    the model instead of the raw data.  These parameters will
     ;    not make sense for all types of spectra.
     ;
     ;
     ;
     ;    If the spectrum is coming from a function, the function should
     ;    must be specified, as a string in:
     ;      self.param.spectrum_function
     ;   
     ;    If the spectrum is coming from an object then both the object
     ;    and the spectrum method must be specified.
     ;   
     ;   
     ;-=========================================================================
     PRO BST_INST_SPECTRUM::LOAD_SPECTRUM

       IF (self.core).debug.timer THEN MIR_TIMER, /START


       ; First reset the object state.
       self->RESET

       ; Extract the spectrum function.
       spectrum_function = (self.core).param.spectrum_function
       spectrum_object = (self.core).param.spectrum_object

       ; Check the current setting for the spectrum structure.
       IF spectrum_function EQ '' THEN BEGIN
         MESSAGE, 'No spectrum retreival function has been set.'
       ENDIF

       ; Next check if an addon was specifed for the spectrum.
       IF spectrum_object NE OBJ_NEW() THEN BEGIN

         IF ~ OBJ_VALID(spectrum_object) THEN BEGIN
           MESSAGE, 'The addon specified for spectrum retreival cannot be found.'
         ENDIF

         self.core->MESSAGE, /DEBUG, 'Using spectrum object: '+OBJ_CLASS(spectrum_object)
         IF ~ spectrum_object->IS_ENABLED() THEN BEGIN
           MESSAGE, 'The addon specified for spectrum retreival is disabled.'
         ENDIF
         
 
       ENDIF


       CATCH, error
       IF (error NE 0) THEN BEGIN
         CATCH, /CANCEL
         self.core->MESSAGE, /ERROR
         MESSAGE, 'The spectrum could not be retrieved.'
       ENDIF

       ; Call the function that returns the spectrum structure.
       ; This function should return a single spectrum.
       IF spectrum_object NE OBJ_NEW() THEN BEGIN
         spectrum = CALL_METHOD(spectrum_function, spectrum_object)
       ENDIF ELSE BEGIN
         spectrum = CALL_FUNCTION(spectrum_function)
       ENDELSE



       ; ---------------------------------------------------------------
       ; Check the received spectrum structure
       tagnames = TAG_NAMES(spectrum)

       ; The spectrum structure must contain spectrum.y
       IF (WHERE(tagnames EQ 'Y'))[0] EQ -1 THEN BEGIN
         MESSAGE, "The 'y' tag was not found in the spectrum structure."
       ENDIF

       num_points = N_ELEMENTS(spectrum.y)

       ; Check if the 'x' and 'weight' tags exist,
       ;  if not generate the data.
       IF (WHERE(tagnames EQ 'X'))[0] EQ -1 THEN BEGIN
         x = FINDGEN(num_points)
       ENDIF ELSE BEGIN
         x = spectrum.x
       ENDELSE
       
       IF (WHERE(tagnames EQ 'WEIGHT'))[0] EQ -1 THEN BEGIN
         weight = REPLICATE(1.0, num_points)
       ENDIF ELSE BEGIN
         weight = spectrum.weight
       ENDELSE

       y = spectrum.y


       ; ---------------------------------------------------------------
       ; See if an xrange was specified
       ; If the two range values are the same, we will assume that the full
       ; range is wanted.
       options = self.core->GET_OPTIONS_USER()
       xrange = options.xrange


       IF xrange[1] NE xrange[0] THEN BEGIN
         no_data = 0

         ; Limit the range to the actual data, if it exists.
         w = WHERE(xrange LT MIN(x), count)
         IF count EQ 1 THEN BEGIN
           xrange[w] = MIN(x)
         ENDIF
         IF count GT 1 THEN BEGIN
           no_data = 1
         ENDIF

         w = WHERE(xrange GT MAX(x), count)
         IF count EQ 1 THEN BEGIN
           xrange[w] = MAX(x)
         ENDIF
         IF count GT 1 THEN BEGIN
           no_data = 1
         ENDIF

         IF no_data THEN BEGIN
           MESSAGE, 'No data in the given range.'
         ENDIF

         valid_range = 1
       ENDIF ELSE BEGIN
         ; If the upper or or lower xrange are the same show the whole spectrum.
         valid_range = 0
       ENDELSE

       IF xrange[1] LT xrange[0] THEN BEGIN
         MESSAGE, 'Invalid pixel range.'
       ENDIF

       
       IF valid_range THEN BEGIN

         _null = MIR_NEAREST(x, xrange[0], index_min, /GREATER)
         _null = MIR_NEAREST(x, xrange[1], index_max, /LESS)
         
         x = x[index_min:index_max]
         y = y[index_min:index_max]
         weight = weight[index_min:index_max]
       ENDIF




       ; ---------------------------------------------------------------
       ; Build the final spectrum structure.
       spectrum_out = {x:x $
                       ,y:y $
                       ,weight:weight}




       ; -----------------------------------------------------------------------
       ; If any of the other accepted tags were given, 
       ; add them to the structure.

       IF (WHERE(tagnames EQ 'WAVELENGTH'))[0] NE -1 THEN BEGIN
         wavelength = spectrum.wavelength
         IF valid_range THEN BEGIN
           wavelength = wavelength[index_min:index_max]
         ENDIF

         spectrum_out = CREATE_STRUCT(spectrum_out $
                                      ,'WAVELENGTH' $
                                      ,wavelength)
       ENDIF

       IF (WHERE(tagnames EQ 'PHOTONS_PER_COUNT'))[0] NE -1 THEN BEGIN
         photons_per_count = spectrum.photons_per_count
         IF valid_range THEN BEGIN
           photons_per_count = photons_per_count[index_min:index_max]
         ENDIF

         spectrum_out = CREATE_STRUCT(spectrum_out $
                                      ,'PHOTONS_PER_COUNT' $
                                      ,photons_per_count)
       ENDIF

       IF (WHERE(tagnames EQ 'SIGMA_DARK'))[0] NE -1 THEN BEGIN
         sigma_dark = spectrum.sigma_dark
         IF valid_range THEN BEGIN
           sigma_dark = sigma_dark[index_min:index_max]
         ENDIF

         spectrum_out = CREATE_STRUCT(spectrum_out $
                                      ,'SIGMA_DARK' $
                                      ,sigma_dark)
       ENDIF


       self.spectrum = PTR_NEW(spectrum_out)


       IF (self.core).debug.timer THEN MIR_TIMER, /STOP

     END ;PRO BST_INST_SPECTRUM::LOAD_SPECTRUM



     ;+=========================================================================
     ; PURPOSE:
     ;   Cleanup for object destruction.
     ;
     ;-=========================================================================
     PRO BST_INST_SPECTRUM::CLEANUP
       
       self->RESET

     END ;PRO BST_INST_SPECTRUM::CLEANUP


     ;+=========================================================================
     ; PURPOSE:
     ;   Reset the state of the object.
     ;
     ;-=========================================================================
     PRO BST_INST_SPECTRUM::RESET

       PTR_FREE, self.spectrum

     END ;PRO BST_INST_SPECTRUM::RESET


     ;+=========================================================================
     ;
     ; Create the BST_INST_SPECTRUM object.
     ;
     ;-=========================================================================
     PRO BST_INST_SPECTRUM__DEFINE

       bst_inst_model_dispatch = $
          { BST_INST_SPECTRUM $
            ,spectrum:PTR_NEW() $

            ,INHERITS BST_INST_BASE $
          }
     END ;PRO BST_INST_SPECTRUM__DEFINE
