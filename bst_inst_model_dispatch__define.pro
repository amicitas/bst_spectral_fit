

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An object to handle the models for <BST_SPECTRAL_FIT>.
;
; DESCRIPTION:
;   This object handles all of the models for <BST_SPECTRAL_FIT>.
;
;   There are two types of routines in this file:
;   1.  Model management routines.
;         These are routines that initialize, destroy, copy or otherwise
;         manage the model objects.
;
;   2.  Model looping routines
;         These are routines that will loop through the individual models,
;         call a given method for each model, and where aprropriate
;         combine the restults.
;
;
;
; 
;   Each model should be made as an object.
;   All models should inherit from the 'BST_INST_MODEL' class
;   [BST_INST_MODEL__DEFINE].
;
;   To build a working model a number of methods will need
;   to be reimplemented and a number of properties defined.  
;
;   See the the documentation in [file:BST_INST_MODEL__DEFINE] for
;   more information.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::INIT, core, _REF_EXTRA=extra

       self.object_type = 'model'

       status = self->BST_INST_DISPATCH::INIT(core, _STRICT_EXTRA=extra)

       self.objects_initial = OBJ_NEW('MIR_DICTIONARY')

       self->INITIALIZE

       RETURN, status

     END ;FUNCTION BST_INST_MODEL_DISPATCH::INIT




     ;+=================================================================
     ; PURPOSE:
     ;   Here we find and initialize all of the models.
     ;
     ;   All constraints are assumed to be implemented as a model.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::INITIALIZE


       directory = MIR_PATH_JOIN([self.core->GET_INSTALL_PATH(), 'models'])
       config_section = 'MODELS'
       superclass = 'BST_INST_MODEL'
       test_methods_array = ['IS_ACTIVE']

       self->LOAD_OBJECTS, directory  $
                          ,CONFIG_SECTION=config_section $
                          ,SUPERCLASS=superclass $
                          ,TEST_METHODS=test_methods_array

       ; Initialize all of the models.
       self->INITIALIZE_MODELS

     END ;PRO BST_INST_INIT_MODELS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize all of the models.
     ;
     ; DESCRIPTION
     ;   This will loop through all of the models, and call their
     ;   INITIALIZE methods.
     ; 
     ;   This will be called after all of the models have been
     ;   instantiated.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::INITIALIZE_MODELS

       self->BST_INST_DISPATCH::INITIALIZE_OBJECTS

     END ;PRO BST_INST_MODEL_DISPATCH::INITIALIZE_MODELS





     ;+=================================================================
     ; PURPOSE:
     ;   Return the total number of possible free pramameters for
     ;   all of the models.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::NUM_PARAM

       num_param = 0

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         num_param += model->NUM_PARAM()

       ENDFOR


       RETURN, num_param

     END ; FUNCTION BST_INST_MODEL_DISPATCH::NUM_PARAM




     ;+=================================================================
     ; PURPOSE:
     ;   Return a array of parinfo structures for every free parameter
     ;   specified in the models.
     ;
     ; DESCRIPTION:
     ;   <BST_SPECTRAL_FIT> uses MPCURVEFIT (part of the 
     ;   MPFIT package) to do the non-linear least squares fit.
     ;   This routine is used to put together the correct array needed
     ;   for MPCURVEFIT.
     ;
     ;   The returned parinfo array will include all possible
     ;   model parameters, even thouse that are not in use or fixed.
     ;   we will then set the unused or fixed parameters to fixed,
     ;   causing the fitter to ignore the parameters.
     ;
     ;
     ;   If in the future the fit_array gets too big then I think
     ;   it would still be best to create the overall fit_array and
     ;   then compact the fit array before fitting using a map.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_PARAMETER_INFO

       ; First get the total number of possible free parameters.
       num_param = self->NUM_PARAM()

       ; Create a blank param_info array.
       param_info = REPLICATE({BST_INST_PARAMETER_INFO}, num_param)

       self->FILL_PARAMETER_INFO, param_info


       RETURN, param_info

     END ;FUNCTION BST_INST_MODEL_DISPATCH::GET_PARAMETER_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Return a array of with every possible free parameter
     ;   specified in the models.
     ;
     ; DESCRIPTION:
     ;   <BST_SPECTRAL_FIT> uses MPCURVEFIT (part of the 
     ;   MPFIT package) to do the non-linear least squares fit.
     ;   This routine is used to put together the correct array
     ;   of (possibly) free variables needed for MPCURVEFIT.
     ;
     ;   The returned array will include all possible
     ;   model parameters, even thouse that are not in use or fixed.
     ;   The unused or fixed parameters will then be specified as
     ;   fixed, causing the fitter to ignore the parameters.
     ;
     ;   If in the future the parameter_array gets too big then I think
     ;   it would still be best to create the overall fit_array and
     ;   then compact the fit array before fitting using a map.
     ; 
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_PARAMETER_ARRAY

       ; First get the total number of possible free parameters.
       num_param = self->NUM_PARAM()

       ; Create a blank param_info array.
       param_array = REPLICATE(0D, num_param)

       self->FILL_PARAMETER_ARRAY, param_array

       RETURN, param_array

     END ;PRO BST_INST_MODEL_DISPATCH::GET_PARAMETER_ARRAY




     ;+=================================================================
     ; PURPOSE:
     ;   Fill the PARAMITER_INFO array with the parametrs from the 
     ;   models.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::FILL_PARAMETER_INFO, param_info


       index = 0

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         num_param = model->NUM_PARAM()
         IF num_param GT 0 THEN BEGIN
           param_info[index:index+num_param-1] = model->GET_PARAMETER_INFO()
           index += num_param
         ENDIF

       ENDFOR


     END ; PRO BST_INST_MODEL_DISPATCH::FILL_PARAMETER_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Fill fit the parameter array (fit array) with the values
     ;   from the model objects.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::FILL_PARAMETER_ARRAY, param_array


       index = 0

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         num_param = model->NUM_PARAM()
         IF num_param GT 0 THEN BEGIN
           param_array[index:index+num_param-1] = model->GET_PARAMETER_ARRAY()
           index += num_param
         ENDIF

       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::FILL_FIT_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Here is where we extract the values in the parameter array 
     ;   into the model objects.
     ;
     ; DESCRIPTION:
     ;   Note: The order that this is done must match the order that
     ;         the the parameters were filled in FILL_PARAMETER_ARRAY.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::EXTRACT_PARAMETER_ARRAY, param_array

       index = 0

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         num_param = model->NUM_PARAM()
         IF num_param GT 0 THEN BEGIN
           model->EXTRACT_PARAMETER_ARRAY, param_array[index:index+num_param-1]
           index += num_param
         ENDIF

       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Here is where we extract the values in the sigma array 
     ;   into the model objects.
     ;
     ; DESCRIPTION:
     ;   Note: The order that this is done must match the order that
     ;         the the parameters were filled in FILL_PARAMETER_ARRAY.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::EXTRACT_SIGMA_ARRAY, sigma_array

       index = 0

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         num_param = model->NUM_PARAM()
         IF num_param GT 0 THEN BEGIN
           model->EXTRACT_SIGMA_ARRAY, sigma_array[index:index+num_param-1]
           index += num_param
         ENDIF

       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::EXTRACT_SIGMA_ARRAY





;=======================================================================
;=======================================================================
;#######################################################################
;
; PROFILE METHODS
; 
; Gaussians models can have a concept of a 'profile' that can be
; used by other models.  These types of model will have the flag
; gaussian = 1.
;
; Below are methods to deal with profiles.
;
;#######################################################################
;=======================================================================
;=======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Return a sum of gauss object with gaussians from any model
     ;   that containes the requested profile.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_PROFILE, profile_num

       profile = OBJ_NEW('BST_INST_PROFILE')
       profile->SET_LABEL, profile_num

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii, key)
         
         IF model->IS_GAUSSIAN() THEN BEGIN
           IF model->USE() AND model->IS_GAUSSIAN() THEN BEGIN  

             IF model->HAS_PROFILE(profile_num) THEN BEGIN

               profile_temp = model->GET_PROFILE(profile_num)
              
               profile->JOIN, profile_temp->TO_ARRAY()
               
               profile_temp->DESTROY
             ENDIF

           ENDIF
         ENDIF
       ENDFOR

       RETURN, profile

     END ; PRO BST_INST_MODEL_DISPATCH::GET_PROFILE, profile_num





     ;+=================================================================
     ; PURPOSE:
     ;   Return a list of the available profiles from all models
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_PROFILE_NUMBER_LIST, POSITIVE=positive $
                                                                ,NEGATIVE=negative


       list = OBJ_NEW('SIMPLELIST')


       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         IF model->IS_GAUSSIAN() THEN BEGIN
           IF model->USE() AND model->IS_GAUSSIAN() THEN BEGIN 

             list_temp = model->GET_PROFILE_NUMBER_LIST()

             IF list_temp->N_ELEMENTS() GT 0 THEN BEGIN
               list->JOIN, list_temp->TO_ARRAY()
             ENDIF

             list_temp->DESTROY

           ENDIF
         ENDIF
       ENDFOR

       IF list->N_ELEMENTS() GT 0 THEN BEGIN
         array = list->TO_ARRAY()
         array = GET_UNIQUE(array)

         CASE 1 OF
           KEYWORD_SET(positive): BEGIN
             ; Return only positive profile numbers.
             index = WHERE(array GT 0, count_index)
           END
           KEYWORD_SET(negative): BEGIN
             ; Return only negative profile numbers.
             index = WHERE(array LT 0, count_index)
           END
           ELSE: BEGIN
             count_index = N_ELEMENTS(array)
             index = INDGEN(count_index)
           END
         ENDCASE


         IF count_index GT 0 THEN BEGIN
           array = array[index]
         ENDIF

         list->FROM_ARRAY, array
       ENDIF

       RETURN, list

     END ; PRO BST_INST_MODEL_DISPATCH::GET_PROFILE_NUMBER_LIST






;=======================================================================
;=======================================================================
;#######################################################################
;
; PREFIT AND EVALUATION METHODS
; 
;
;#######################################################################
;=======================================================================
;=======================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   Set any global user options.
     ;
     ; DESCRIPTION:
     ;   Set any global options that need to overide the model
     ;   options.  This is done after saving the initial model state
     ;   so changes in these options do not get sent back to the user.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::SET_USER_OPTIONS


       ; Loop over all of the models, and copy the initial state.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii, key)

         model->SET, EVALUATION_CUTOFF=(self.core).options_user.evaluation_cutoff
       ENDFOR


     END ;PRO BST_INST_MODEL_DISPATCH::SET_USER_OPTIONS



     ;+=================================================================
     ; PURPOSE:
     ;   Setup all of the models.
     ;
     ; DESCRIPTION
     ;   This will loop through all of the models, check if they
     ;   are in use, and call their PREFIT methods.
     ; 
     ;   This will be called by <BST_SPECTRAL_FIT> everytime 
     ;   before a fit loop is started.  
     ;
     ;   It will be called after the initial model state has been
     ;   saved.  It will be called before self->PREFIT_CONSTRAINTS.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::PREFIT, x

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         IF model->USE() THEN BEGIN
           model->PREFIT
         ENDIF
       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::PREFIT



     ;+=================================================================
     ; PURPOSE:
     ;   Setup constraints imposed by any of the models
     ;
     ; DESCRIPTION:
     ;   This will loop through all of the models, check if they are in
     ;   use, and call their INITIALIZE_CONSTRANTS methods.
     ;

     ;   This will be called by <BST_SPECTRAL_FIT> everytime 
     ;   before a fit loop is started.  
     ;
     ;   It will be called after the initial model state has been
     ;   saved.  It will be called after self->PREFIT.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::PREFIT_CONSTRAINTS


       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         IF model->USE() THEN BEGIN  
           model->PREFIT_CONSTRAINTS
         ENDIF
       ENDFOR


     END ; PRO BST_INST_MODEL_DISPATCH::PREFIT_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the constraints imposed by any of the models.
     ;
     ; DESCRIPTION:
     ;   This will loop through all of the models, check if they are in
     ;   use, and call their EVALUATE_CONSTRANTS methods.
     ;
     ;   This will be called by <BST_SPECTRAL_FIT> every time
     ;   the model needs to be evaluated.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::EVALUATE_CONSTRAINTS


       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         IF model->USE() THEN BEGIN  
           model->EVALUATE_CONSTRAINTS
         ENDIF
       ENDFOR


     END ; PRO BST_INST_MODEL_DISPATCH::EVALUATE_CONSTRAINTS



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate all of the models.
     ;
     ; DESCRIPTION:
     ;   This will loop through all of the models, check if they
     ;   are in use, and call their EVALUATION methods.
     ; 
     ;   This will be called by <BST_SPECTRAL_FIT> every time
     ;   the model needs to be evaluated.
     ;
     ;   Note:
     ;     Before this routine is called [::EVALUATE_CONSTRAINTS]
     ;     will already have been called.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::EVALUATE, x

       debug = self.core.GET_OPTIONS_DEBUG()
       IF debug.timer THEN MIR_TIMER, /START

       ; Create an empty array for the results.
       y = DBLARR(N_ELEMENTS(x))

       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         
         IF model->USE() AND model->NEED_EVALUATE() THEN BEGIN
           IF debug.timer THEN MIR_TIMER, OBJ_CLASS(model)+'::EVALUATE', /START

           y += model->EVALUATE(x)

           IF debug.timer THEN MIR_TIMER, OBJ_CLASS(model)+'::EVALUATE', /STOP
         ENDIF
       ENDFOR


       IF debug.timer THEN MIR_TIMER, /STOP

       RETURN, y
     END ;FUNCTION BST_INST_MODEL_DISPATCH::EVALUATE


     ;+=================================================================
     ; PURPOSE:
     ;   Save the initial state of the models.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::SAVE_INITIAL_STATE


       ; First reset the value of the initial model state.
       self->REMOVE_OBJECTS_INITIAL

       ; Loop over all of the models, and copy the initial state.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii, key)

         ; Do a recursive copy so that all appropriate internal 
         ; objects are copied.
         self.objects_initial->SET, key, model->GET_COPY(/RECURSIVE)
       ENDFOR


     END ;PRO BST_INST_MODEL_DISPATCH::SAVE_INITIAL_STATE


     ;+=================================================================
     ; PURPOSE:
     ;   Save the initial state of the models.
     ;
     ; PROGRAMING NOTES:
     ;   This is not the most effient way to do this, however
     ;   this will ensure that only the parameters can be affected
     ;   in anyway by the fit loop.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::RESTORE_INITIAL_STATE, UPDATE_PARAMETERS=update_param

       ; Loop over all of the models, and copy the initial state.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
         model_initial = self.objects_initial->GET_BY_INDEX(ii)
         
         model->RESTORE_INITIAL_STATE, model_initial, UPDATE_PARAMETERS=update_param
       ENDFOR


     END ;PRO BST_INST_MODEL_DISPATCH::RESTORE_INITIAL_STATE




;=======================================================================
;=======================================================================
;#######################################################################
;
; MODEL CONTROL METHODS
; 
;
;#######################################################################
;=======================================================================
;=======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Print out the current state of all models.
     ;
     ; DESCRIPTION:
     ;   By default this will only print the state of models that are
     ;   in use.  The /ALL keyword will print the state for all
     ;   active models.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::PRINT_PARAM, ALL=all, _REF_EXTRA=extra

       ; Loop over all of the currently active models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii, key)

         ; Print a separator.
         self.core->MESSAGE, ''
         self.core->MESSAGE, STRING('',FORMAT='(72("="),a0)')

         ; Print a header.
         self.core->MESSAGE, key

         IF model->USE() OR KEYWORD_SET(all) THEN BEGIN  
           model->PRINT_STRUCT, _STRICT_EXTRA=extra
         ENDIF ELSE BEGIN
           ; If the model is not in use, and /ALL is not set, then just
           ; print out the 'use' property.
           PRINT_STRUCTURE, {USE:model->USE()}, _STRICT_EXTRA=extra
         ENDELSE

       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::PRINT_PARAM

     
     
     ;+=================================================================
     ; PURPOSE:
     ;   Return a plotlist from all models that are in use.
     ;
     ; DESCRIPTION:
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_PLOTLIST, x_values, _REF_EXTRA=extra

       ; Create a new PLOTLIST object
       plotlist = OBJ_NEW('MIR_PLOTLIST')
       
       ; Loop over all of the currently active models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii, key)

         IF model->USE() THEN BEGIN  
           model_plotlist = model->GET_PLOTLIST(x_values, _STRICT_EXTRA=extra)
           IF ISA(model_plotlist) THEN BEGIN
             plotlist.JOIN, model_plotlist
             model_plotlist.DESTROY
           ENDIF
         ENDIF 

       ENDFOR

       RETURN, plotlist
       
     END ;PRO BST_INST_MODEL_DISPATCH::GET_PLOTLIST


     
     ;+=================================================================
     ; PURPOSE:
     ;   Set all the models to fixed
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::SET_SAVE, save



       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
  
         model->SET_SAVE, save

       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::SET_FIXALL



     ;+=================================================================
     ; PURPOSE:
     ;   Set all the models to fixed
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::SET_FIXALL, fixall



       ; Loop over all of the models.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         model = self.objects->GET_BY_INDEX(ii)
  
         model->SET_FIXALL, fixall

       ENDFOR

     END ;PRO BST_INST_MODEL_DISPATCH::SET_FIXALL




;=======================================================================
;=======================================================================
;#######################################################################
;
; MODEL DISPATCH METHODS
; 
;
;#######################################################################
;=======================================================================
;=======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the number of models.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::NUM_MODELS

       RETURN, self->BST_INST_DISPATCH::NUM_OBJECTS()

     END ;FUNCTION BST_INST_MODEL_DISPATCH::NUM_MODELS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a model object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_MODEL, name_in

       RETURN, self->BST_INST_DISPATCH::GET_OBJECT(name_in, PREFIX='BST_INST_MODEL_')

     END ;FUNCTION BST_INST_MODEL_DISPATCH::GET_MODEL



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a model object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_MODEL_DISPATCH::GET_PROFILES_OBJECT

       RETURN, (self.core).profiles

     END ;FUNCTION BST_INST_MODEL_DISPATCH::GET_PROFILES_OBJECT



     ;+=================================================================
     ; PURPOSE:
     ;   Destroy all inital object states  and deallocate storage.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::REMOVE_OBJECTS_INITIAL


       ; Destroy all of the initial model states.
       IF OBJ_VALID(self.objects_initial) THEN BEGIN
         FOR ii=0,self.objects_initial->NUM_ENTRIES()-1 DO BEGIN
           model = self.objects_initial->GET_BY_INDEX(ii)
           
           IF OBJ_VALID(model) THEN BEGIN
             model->DESTROY
           ENDIF
         ENDFOR

         self.objects_initial->REMOVE, /ALL
       ENDIF

     END ;PRO BST_INST_MODEL_DISPATCH::REMOVE_OBJECTS_INITIAL



     ;+=================================================================
     ; PURPOSE:
     ;   Destroy all objects and deallocate storage.
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH::DEALLOCATE

       self->BST_INST_DISPATCH::DEALLOCATE

       IF OBJ_VALID(self.objects_initial) THEN BEGIN
         self->REMOVE_OBJECTS_INITIAL
         self.objects_initial->DESTROY
       ENDIF


     END ;PRO BST_INST_MODEL_DISPATCH::DEALLOCATE



     ;+=================================================================
     ; PURPOSE:
     ;   Create the BST_INST_MODEL_DISPATCH object.
     ;
     ;-=================================================================
     PRO BST_INST_MODEL_DISPATCH__DEFINE

       bst_inst_model_dispatch = $
          { BST_INST_MODEL_DISPATCH $

            ,objects_initial:OBJ_NEW() $

            ,INHERITS BST_INST_DISPATCH $
          }
     END ;PRO BST_INST_MODEL_DISPATCH__DEFINE
