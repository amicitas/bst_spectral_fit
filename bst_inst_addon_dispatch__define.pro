
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   An object to handle the adddons for <BST_SPECTRAL_FIT>.
;
; DESCRIPTION:
;   This object handles all of the addons for <BST_SPECTRAL_FIT>.
;   It serves as an interface between the application core and the
;   addons.  As such it contains three types of routines:
;     1. Methods to find and initialize the addons.
;     2. Methods for the application to access the addons
;     3. Methods for  the addons to access the application.
;
;   Note that there two places that addons can be found
;     addons/
;     addons_internal/
;
;   The addons_internal contains addons that should allways be loaded
;   for <BST_SPECTRAL_FIT>.  This includes, for example, an
;   addon to control the gaussian, background and scaled models.
;
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DISPATCH::INIT, core, app, _REF_EXTRA=extra

       self.object_type = 'addon'

       status = self->BST_INST_DISPATCH::INIT(core, app, _STRICT_EXTRA=extra)

       ; Programming Note:
       ;  After instantiation it is still nessecary to call initialize.
       ;  This is important since individual addons may need access to this
       ;  dispatcher during their instantiation/initialization.


       RETURN, status

     END ;FUNCTION BST_INST_ADDON_DISPATCH::INIT


     ;+=================================================================
     ; PURPOSE:
     ;   Here we find and initialize all of the addons.
     ;
     ;   All constraints are assumed to be implemented as a addon.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DISPATCH::INITIALIZE


       directory = ['addons']
       FOR ii=0,N_ELEMENTS(directory)-1 DO BEGIN
         directory[ii] = MIR_PATH_JOIN([self.core->GET_INSTALL_PATH(), directory[ii]])
       ENDFOR
       config_section = 'ADDONS'
       superclass = 'BST_INST_ADDON'
       test_methods_array = ['IS_ACTIVE']

       self->LOAD_OBJECTS, directory  $
                          ,CONFIG_SECTION=config_section $
                          ,SUPERCLASS=superclass $
                          ,TEST_METHODS=test_methods_array

       ; Initialize all of the addons.
       self->INITIALIZE_ADDONS


     END ;PRO BST_INST_INIT_ADDONS


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize all of the addons.
     ;
     ; DESCRIPTION
     ;   This will loop through all of the addons, and call their
     ;   INITIALIZE methods.
     ; 
     ;   This will be called after all of the addons have been
     ;   instantiated.
     ;
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DISPATCH::INITIALIZE_ADDONS

       ; This calls the 'INITIALIZE' method on all of the addons.
       self->BST_INST_DISPATCH::INITIALIZE_OBJECTS

       ; Enable the default addons.
       self->ENABLE_DEFAULT

     END ;PRO BST_INST_ADDON_DISPATCH::INITIALIZE_ADDONS


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a addon object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DISPATCH::GET_ADDON, name_in

       RETURN, self->BST_INST_DISPATCH::GET_OBJECT(name_in, PREFIX='BST_INST_ADDON_')

     END ;FUNCTION BST_INST_ADDON_DISPATCH::GET_ADDON


     ;+=================================================================
     ; PURPOSE:
     ;   This function will return an filename_prefix that can be
     ;   used for automatic filenameing
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DISPATCH::GET_FILENAME_PREFIX, _REF_EXTRA=extra


       filename_prefix = ''

       ; Loop over all of the addons.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         addon = self.objects->GET_BY_INDEX(ii)


         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not retreve filename prefix from addon: '+OBJ_CLASS(addon) $
                                 , /ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this addon is enabled.
         IF addon->IS_ENABLED() THEN BEGIN
           filename_prefix += addon->GET_FILENAME_PREFIX(_STRICT_EXTRA=extra)
         ENDIF

       ENDFOR


       RETURN, filename_prefix
 
       
     END ; FUNCTION BST_INST_ADDON_DISPATCH::GET_FILENAME_PREFIX


     ;+=================================================================
     ; PURPOSE:
     ;   This function will return a structure that contains any
     ;   data that should be included in the output file.
     ;
     ;   This structure may be nested if appropriate.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DISPATCH::GET_OUTPUT_STRUCTURE
       output_dict = OBJ_NEW('MIR_DICTIONARY')

       ; Loop over all of the addons.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         addon = self.objects->GET_BY_INDEX(ii, key)

         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not retreve output structure from addon: '+OBJ_CLASS(addon) $
                                 ,/ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this addon is enabled.
         IF addon->IS_ENABLED() THEN BEGIN
           addon_output = addon->GET_OUTPUT_STRUCTURE()
           IF TAG_NAMES(addon_output, /STRUCTURE_NAME) NE 'NULL' THEN BEGIN
             output_dict->SET, key, addon_output, OVERWRITE=0
           ENDIF
         ENDIF

       ENDFOR

       IF output_dict->NUM_ENTRIES() GT 0 THEN BEGIN
         output_struct = output_dict->TO_STRUCTURE()
       ENDIF ELSE BEGIN
         output_struct = {NULL}
       ENDELSE

       output_dict->DESTROY

       RETURN, output_struct
 
       
     END ; FUNCTION BST_INST_ADDON_DISPATCH::GET_OUTPUT_STRUCTURE



     ;+=================================================================
     ; PURPOSE:
     ;   This function will return a plotlist with all plotting
     ;   output from the addons.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DISPATCH::GET_SPECTRUM_PLOTLIST, x_values

       plotlist = OBJ_NEW('MIR_PLOTLIST')

       ; Loop over all of the addons.
       FOR ii=0,self.objects->NUM_ENTRIES()-1 DO BEGIN
         addon = self.objects->GET_BY_INDEX(ii, key)

         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           self.core->MESSAGE, 'Could not retreve plotlist from addon: '+OBJ_CLASS(addon) $
                                 ,/ERROR, /LOG
           CONTINUE
         ENDIF


         ; Check if this addon is enabled.
         IF addon->IS_ENABLED() THEN BEGIN
           addon_plotlist = addon->GET_SPECTRUM_PLOTLIST(x_values)
           IF addon_plotlist->N_ELEMENTS() NE 0 THEN BEGIN
             plotlist->JOIN, addon_plotlist
           ENDIF
           addon_plotlist->DESTROY
         ENDIF

       ENDFOR

       RETURN, plotlist
 
       
     END ; FUNCTION BST_INST_ADDON_DISPATCH::GET_SPECTRUM_PLOTLIST




;=======================================================================
;#######################################################################
;#######################################################################
;
; Methods to access the application.  To be used by the addons.
;
;#######################################################################
;#######################################################################
;=======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive a model object reference by name.
     ;
     ;-=================================================================
     FUNCTION BST_INST_ADDON_DISPATCH::GET_MODEL, name_in

       fitter_core = self->GET_CORE()
       model_dispatch = fitter_core->GET_MODEL_DISPATCHER()

       RETURN, model_dispatch->GET_MODEL(name_in)

     END ;FUNCTION BST_INST_ADDON_DISPATCH::GET_MODEL



; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Create the BST_INST_ADDON_DISPATCH object.
     ;
     ;-=================================================================
     PRO BST_INST_ADDON_DISPATCH__DEFINE

       bst_inst_addon_dispatch = $
          { BST_INST_ADDON_DISPATCH $

            ,INHERITS BST_INST_DISPATCH $
          }
     END ;PRO BST_INST_ADDON_DISPATCH__DEFINE
