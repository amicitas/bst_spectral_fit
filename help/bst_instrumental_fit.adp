<!DOCTYPE DCF>  
<assistantconfig version="3.3.0">  
   <profile>  
      <property name="name">BST_INSTRUMENTAL_FIT 0.2.6</property>  
      <property name="title">Help System</property>  
      <property name="startpage">home.html</property>  
      <property name="aboutmenutext">About BST_INSTRUMENTAL_FIT</property>  
      <property name="abouturl">bst_inst_about.txt</property>  
      <property name="assistantdocs">.</property>  
   </profile>  
   <DCF ref="home.html" title="BST Instrumental Fit">
      <section ref="home.html" title="Introduction">  
         <keyword ref="home.html">Introduction</keyword>
      </section> 
      <section ref="usage.html" title="Usage">  
         <keyword ref="usage.html">Usage</keyword>
	 <keyword ref="usage.html#profiles">Profiles</keyword>
	 <keyword ref="usage.html#scaled">Scaled Profiles</keyword>
	 <keyword ref="usage.html#background">Background</keyword>
	 <keyword ref="usage.html#constraints">Constraints</keyword>
	 <keyword ref="usage.html#commands">Commands</keyword>
	 <keyword ref="usage.html#fixing">Fixing/Saving Parameters</keyword>
	 <keyword ref="usage.html#output">Output</keyword>
	 <keyword ref="usage.html#save_state">Saving/Loading</keyword>
	 <keyword ref="usage.html#options">Options</keyword>
      </section> 
      <section ref="limitations.html" title="Limitations">   
         <keyword ref="limitations.html">Limitations</keyword>  
      </section>  
      <section ref="developing.html" title="Developing">   
         <keyword ref="developing.html">Developing</keyword>  
      </section>
   </DCF>  
</assistantconfig>
