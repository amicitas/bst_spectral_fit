




;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   An object that allows the results of fits from 
;   <BST_SPECTRAL_FIT> to be saved.  
;
;   The expected use is to save the fit results for a series of spectra.
;   This object can then be used to retive the data for analysis.
;
;-======================================================================

; ======================================================================
; TO DO:
;   I need to add a version number.  I should keep track of this
;   number when loading or saving the object state.
;
;   This object should inherit 'LIST' instead of 'SIMPLE_LIST'.
;   'LIST' alreay handles all the pointe stuff.
;
; ======================================================================



     ;+=================================================================
     ; 
     ; Get some basic information about the profiles.
     ;
     ; NOTE:  I actually don't think this is particularly usefull.
     ;        I either need to expand this or just get rid of it.
     ;        As far as I know I am no longer using this method
     ;        anywhere.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_LIST::GET_BASIC_PROFILE_INFO

       
       IF self.num_elements EQ 0 THEN BEGIN
         MESSAGE, 'No fits saved.'
       ENDIF

       saves = self->GET_POINTER()

       ; Assume that all the saves have the same number of profiles
       num_profiles = N_TAGS((*saves[0]).profile)
       
       IF num_profiles EQ 0 THEN BEGIN
         MESSAGE, 'No profiles.'
       ENDIF


       ; Create the structure that we are going to output for
       ; each fit.
       struct = {centroid:0D $
                 ,intensity:0D $
                 ,fwhm:0D $
                 ,location_max:0D $
                 ,value_max:0D $
                 ,chisq:0D $
                 ,user_param:(*saves[0]).user_param $
                }


       ; Create the results array
       result = REPLICATE(struct, [self.num_elements, num_profiles])

       ; Loop through all the saves
       FOR save_num = 0,self.num_elements-1 DO BEGIN
         ; Loop though all the profiles
         FOR profile_num = 0, num_profiles-1 DO BEGIN
           STRUCT_ASSIGN, (*saves[save_num]).profile.(profile_num), struct, /NOZERO
           STRUCT_ASSIGN, (*saves[save_num]).fit, struct, /NOZERO

           STRUCT_ASSIGN, {user_param:(*saves[save_num]).user_param}, struct, /NOZERO

           result[save_num, profile_num] = struct
         ENDFOR
       ENDFOR
       
       RETURN, result

     END ; FUNCTION BST_INST_FIT_LIST::GET_BASIC_PROFILE_INFO






     ;+=================================================================
     ; 
     ; This will return the pointer to the fit results.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_LIST::GET_POINTER, index, ALL=all

       IF N_PARAMS() EQ 0 THEN BEGIN
         RETURN, self->SIMPLELIST::GET(ALL=all)
       ENDIF ELSE BEGIN
         RETURN, self->SIMPLELIST::GET(index, ALL=all)
       ENDELSE

     END ; FUNCTION BST_INST_FIT_LIST::GET_POINTER





     ;+=================================================================
     ; 
     ; This will return the the results.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_LIST::GET, index, ALL=all

       ; First get the pointers.
       IF N_PARAMS() EQ 0 THEN BEGIN
         ptr_array = self->GET_POINTER(ALL=all)
       ENDIF ELSE BEGIN
         ptr_array = self->GET_POINTER(index, ALL=all)
       ENDELSE

 
       IF N_ELEMENTS(ptr_array) EQ 1 THEN BEGIN
         RETURN, *ptr_array
       ENDIF

       ; Now create an array with the results
       output = REPLICATE(*ptr_array[0], self.num_elements)

       FOR ii=0,self.num_elements-1 DO BEGIN
         output[ii] = *ptr_array[ii]
       ENDFOR

       RETURN, output
      
     END ; FUNCTION BST_INST_FIT_LIST::GET




     ;+=================================================================
     ; 
     ; Save the results of this fit 
     ; 
     ;-=================================================================
     PRO BST_INST_FIT_LIST::SAVE, USER_PARAM=user_param
       COMMON BST_INSTRUMENTAL_FIT

       MIR_DEFAULT, user_param, {NULL}

       ; First build the structure to save
       save = {fitter_param:c_inst_param $
               ,fit:c_fit $
               ,profile:c_profile $
               ,spectrum:c_spectrum $
               ,model_state:BST_INST_MODEL_GET_STATE() $
               ,addon_state:BST_INST_ADDONS_GET_STATE() $
               ,user_param:user_param $
              }

       ; Create a pointer to structure
       ptr_save = PTR_NEW(save)

       ; Add the pointer to the common bock
       self->APPEND, ptr_save
       
     END ;PRO BST_INST_FIT_LIST::SAVE




     ;+=================================================================
     ;
     ; Delete the fit result at the given index.
     ;
     ; If /ALL is given then all fit results will be deleted.
     ; 
     ;-=================================================================
     PRO BST_INST_FIT_LIST::REMOVE, index, ALL=all
       
       IF self.num_elements EQ 0 THEN BEGIN
         RETURN
       ENDIF

       ; Extract the elements of the list that we are going to remove.
       IF N_PARAMS() EQ 1 THEN BEGIN
         deleted_rows = self->GET_POINTER(index, ALL=all)
       ENDIF ELSE BEGIN
         deleted_rows = self->GET_POINTER(ALL=all)
       ENDELSE
       
       ; The elements that we extracted are pointers,
       ; so we need to free them here.
       PTR_FREE, deleted_rows

       ; Now call the super method
       self->SIMPLELIST::REMOVE, index, ALL=all


     END ;PRO BST_INST_FIT_LIST::REMOVE




     ;+=================================================================
     ; 
     ; Clean up when the object is destroyed.
     ; Delete all pointers and the list object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_LIST::CLEANUP     

       self->REMOVE, /ALL

     END ; PRO BST_INST_FIT_LIST::CLEANUP



     
     ;+=================================================================
     ; 
     ; Define the BST_INST_FIT_LIST object.
     ;
     ; The only property is 'fit_list' which is a list of pointers
     ; to the fit results structure.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_LIST__DEFINE
       
       struct = {BST_INST_FIT_LIST $
                 ,INHERITS SIMPLELIST $
                }

     END ;PRO BST_INST_FIT_LIST__DEFINE
