



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Define an applicaiton object with an addon loader.
;
; PROGRAMMING NOTES:
;   There are a number of routines that simply wrap identically named
;   routines in the core.  This is done in an attempt to issolate the 
;   API for the app from that of the core.
;   
;
;-======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle initialization the <BST_INST_FIT_APP::> object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::INIT, _REF_EXTRA=extra

       ; Call the INIT methods for the base class.
       status = self->BST_INST_BASE::INIT()
       IF status NE 1 THEN MESSAGE, 'BST_INST_BASE::INIT failed.'

       self->INITIALIZE, _STRICT_EXTRA=extra

       RETURN, 1

     END ;FUNCTION BST_INST_FIT_APP::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the <BST_INST_FIT_APP::> object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::INITIALIZE, _REF_EXTRA=extra

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         ; Destroy the core if possible.
         IF OBJ_VALID(self.core) THEN BEGIN
           self.core->MESSAGE, 'Could not initialize BST_INST_FIT_APP.', /SEVERE_ERROR
           self.core->DESTROY
         ENDIF

         MESSAGE, 'Could not initialize [BST_INST_FIT_APP::].', /INFO
         MESSAGE, /REISSUE_LAST
       ENDIF
           

       ; NOTE: The order things are done here is important.
       ;       In particular the core must be initialized before
       ;       anything else.  This enables the log etc.

       ; First initialize the fitter core.
       self->MESSAGE, 'Initializing the fitter core.', /INFO
       self->INITIALIZE_CORE, _STRICT_EXTRA=extra

       ; Initialize the addons.
       self->MESSAGE, 'Initializing addons.', /INFO
       self->INITIALIZE_ADDONS

       ; Update the addons.
       self->UPDATE


     END ;PRO BST_INST_FIT_APP::INITIALIZE


     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> core object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::INITIALIZE_CORE, _REF_EXTRA=extra

       IF OBJ_VALID(self.core) THEN BEGIN
         self.core->DESTROY
       ENDIF

       self.core = OBJ_NEW('BST_INST_FIT', _STRICT_EXTRA=extra)

     END ;PRO BST_INST_FIT_APP::INITIALIZE_CORE


     ;+=================================================================
     ; PURPOSE:
     ;   Inititialize <BST_SPECTRAL_FIT> addons object.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::INITIALIZE_ADDONS

       IF OBJ_VALID(self.addons) THEN BEGIN
         self.addons->DESTROY
       ENDIF

       self.addons = OBJ_NEW('BST_INST_ADDON_DISPATCH', self.core, self)
       self.addons.INITIALIZE

     END ;PRO BST_INST_FIT_APP::INITIALIZE_ADDONS


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Generate the title for the main gui window.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_APP_NAME

       RETURN, 'BST_SPECTRAL_FIT - Version ' $
               + VERSION_STRING(self.core->GET_VERSION())

     END ;FUNCTION BST_INST_FIT_APP::GET_APP_NAME

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Update the state of the GUI and all addons.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::UPDATE

       self.addons->UPDATE

     END ;PRO BST_INST_FIT_APP::UPDATE




;=======================================================================
;#######################################################################
;#######################################################################
;
; Methods to handle interaction with the core and the addons.
;
;#######################################################################
;#######################################################################
;=======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This is the main fit loop for the application.
     ; 
     ; DESCRIPTION:
     ;   This method will be called when a fit loop is started.  This
     ;   is the default action for when the 'Analyze' button is pressed.
     ;
     ;   The fit loop is similar to [BST_INST_FIT::QUICKFIT], however
     ;   hooks are provided for the addons.
     ;
     ;   Here is a simplified description of what is taking place. It
     ;   is important to note the order in which these actions take
     ;   place.
     ;
     ;     gui->BEFORE_FIT
     ;       guiaddons->BEFORE_FIT
     ;     app->BEFORE_FIT
     ;       addons->BEFORE_FIT
     ;     core->BEFORE_FIT
     ;       spectrum->LOAD_SPECTRUM
     ;       models->SAVE_INITIAL_STATE
     ;
     ;     gui->PREFIT
     ;       guiaddons->PREFIT
     ;     app->PREFIT
     ;       addons->PREFIT
     ;     core->PPREFIT
     ;       models->PREFIT
     ;       models->PREFIT_CONSTRAINTS
     ;
     ;     FIT
     ;
     ;     core->POSTFIT
     ;       models->RESTORE_INITIAL_STATE
     ;     app->POSTFIT
     ;       addons->POSTFIT
     ;     gui->POSTFIT
     ;       guiaddons->POSTFIT
     ;    
     ;
     ;     core->AFTER_FIT
     ;     app->AFTER_FIT
     ;       addons->AFTER_FIT
     ;     gui->AFTER_FIT
     ;       guiaddons->AFTER_FIT
     ;
     ;     gui->UPDATE
     ;       guiaddons->UPDATE
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::FIT

       core = self->GET_CORE()

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         self->MESSAGE, 'Fit failed.', /ERROR

         core->POSTERROR
         self->POSTERROR
         RETURN
       ENDIF
       
       self->MESSAGE, ['','Starting Fit.'], /INFO
       
       IF core.debug.timer THEN MIR_TIMER, /START

       IF core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_APP::FIT | PRE-FITTING', /START
       self->BEFORE_FIT
       self->PREFIT
       IF core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_APP::FIT | PRE-FITTING', /STOP


       IF core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_APP::FIT | FITTING', /START
       self.core->FIT
       IF core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_APP::FIT | FITTING', /STOP


       IF core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_APP::FIT | POST-FITTING', /START
       self->POSTFIT
       self->AFTER_FIT
       IF core.debug.timer THEN MIR_TIMER, 'BST_INST_FIT_APP::FIT | POST-FITTING', /STOP

       IF core.debug.timer THEN MIR_TIMER, /STOP

       self->MESSAGE, 'Fit complete.', /INFO

     END ;PRO BST_INST_FIT_APP::FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action before starting starting the fit.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::BEFORE_FIT
       self->MESSAGE, 'Preparing for fit.', /DEBUG

       self.addons->BEFORE_FIT
       self.core->BEFORE_FIT
       
     END ;PRO BST_INST_FIT_APP::BEFORE_FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action immediatly before enting the fit loop.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PREFIT
       self->MESSAGE, 'APP: Performing prefit.', /DEBUG

       self.addons->PREFIT
       self.core->PREFIT

     END ;PRO BST_INST_FIT_APP::PREFIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action immediatly after exiting the fit loop.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::POSTFIT
       self->MESSAGE, 'APP: Performing postfit.', /DEBUG

       ; Perform any postfit actions.
       self.core->POSTFIT
       self.addons->POSTFIT

     END ;PRO BST_INST_FIT_APP::POSTFIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action after a fit has been completed
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::AFTER_FIT
       self->MESSAGE, 'APP: Performing after_fit.', /DEBUG

       ; Perform any postfit actions.
       self.core->AFTER_FIT
       self.addons->AFTER_FIT

       self->UPDATE

       ; Explicitly save the log file.
       self->MESSAGE, /SAVE


     END ;PRO BST_INST_FIT_APP::AFTER_FIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Take action if an error occurs while doing a fit.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::POSTERROR
       self->MESSAGE, 'Cleaning up after failed fit.', /INFO

       self.core->POSTERROR
       
       self.addons->AFTER_FIT
       self->UPDATE

       ; Explicitly save the log file.
       self->MESSAGE, /SAVE
       
     END ;PRO BST_INST_FIT_APP::POSTERROR


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Save the state of the application to a save file.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::SAVE, filename, DIALOG=dialog


       IF ~ ISA(filename) THEN BEGIN
         ; Generate a probable filename.
         filename = self->GET_SAVE_FILENAME()

         IF KEYWORD_SET(dialog) THEN BEGIN
           IF ISA(self.coregui) THEN BEGIN
             parentid = self.coregui.gui_param.gui_id
           ENDIF ELSE BEGIN
             parentid = 0
           ENDELSE
           
           ; Let the user pick the file in which to save.
           filename = DIALOG_PICKFILE(/WRITE $
                                      ,FILE=filename $
                                      ,PATH=self->GET_CURRENT_PATH() $
                                      ,GET_PATH=new_path $
                                      ,FILTER=['*.bst_inst;*.sav','*.bst_inst','*.sav'] $
                                      ,/OVERWRITE_PROMPT $
                                      ,TITLE='Chose file to save state in.' $
                                      ,DIALOG_PARENT=parentid)

         ENDIF
       ENDIF
       
       ; Check that a file was picked.
       IF filename EQ '' THEN RETURN

       ; Save the new path.
       IF ISA(new_path) THEN BEGIN
         IF new_path NE '' THEN BEGIN
           self->SET_CURRENT_PATH, new_path
         ENDIF
       ENDIF

       ; Get the save structure
       IF ISA(self.coregui) THEN BEGIN
         bst_inst_save_state = self.coregui->GET_STATE()
       ENDIF ELSE BEGIN
         bst_inst_save_state = self->GET_STATE()
       ENDELSE
       
       ; Anonymize the save structure.
       ;
       ; This will also ensure that we can recover the actuall save state
       ; with out any new/missing properties from changed structure
       ; definitions.
       ;
       ; This is also a workaround for a bug in IDL 6.2 (and earlyer?)
       bst_inst_save_state = STRUCT_ANONYMOUS(bst_inst_save_state)

       ; Save the structure.
       MIR_SAVE_OBJECT, bst_inst_save_state, FILENAME=filename, /COMPRESS



       self->MESSAGE, 'State saved to: ' + filename, /INFO

     END ;PRO BST_INST_FIT_APP::SAVE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Load the application state from a save file.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::LOAD, filename $
                                 ,DIALOG_PARENT=dialog_parent $
                                 ,FORCE=force

       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Could not load save state.', /ERROR
         RETURN
       ENDIF

       IF ~ ISA(filename) THEN BEGIN
         ; Generate a probable filename.
         filename = self->GET_SAVE_FILENAME()

         ; Let the user chose the save file.
         filename = DIALOG_PICKFILE(/READ $
                                    ,FILE=filename $
                                    ,PATH=self->GET_CURRENT_PATH() $
                                    ,GET_PATH=new_path $
                                    ,FILTER=['*.bst_inst;*.sav','*.bst_inst','*.sav'] $
                                    ,/MUST_EXIST $
                                    ,DIALOG_PARENT=dialog_parent)

         ; Check that a file was picked.
         IF filename EQ '' THEN RETURN

         ; Save the new path.
         IF new_path NE '' THEN BEGIN
           self->SET_CURRENT_PATH, new_path
         ENDIF
       ENDIF

       self->MESSAGE, ['Loading save state from:', filename], /INFO

       ; Restore the save structure
       bst_inst_save_state = MIR_RESTORE_OBJECT(FILENAME=filename, /RELAXED_STRUCTURE_ASSIGNMENT)

       ; Load the save structure.
       IF ISA(self.coregui) THEN BEGIN
         self.coregui->LOAD_STATE, bst_inst_save_state, FORCE=force
       ENDIF ELSE BEGIN
         self->LOAD_STATE, bst_inst_save_state, FORCE=force
       ENDELSE

     END ;PRO BST_INST_FIT_APP::LOAD


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Check the version number of the save state.
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::SAVE_STATE_CHECK_VERSION, state, FORCE=force

       app_version = self->GET_VERSION()

       IF (app_version.number[0] EQ state.version.number[0]) AND $
          (app_version.number[1] EQ state.version.number[1]) THEN BEGIN
         ; Major and minor versions are the same.  Ok to load.
         RETURN, 1
       ENDIF
       
       message = [ $
                   'This file was made with an older version of BST_SPECTRAL_FIT.' $
                   ,'File version: v' + VERSION_STRING(state.version) $
                   ,'App version:  v' + VERSION_STRING(app_version) $
                   ,'Load may be incomplete or invalid.' $
                   ,'' $
                   ,'Attempt load anyway?' $
                 ]

       IF KEYWORD_SET(force) THEN BEGIN
         self->MESSAGE, message[0:3], /WARNING
         self->MESSAGE, 'Attempting to load savefile from a older version.', /WARNING
         RETURN, 1
       ENDIF
       
       cont = DIALOG_MESSAGE(message, /QUESTION, /DEFAULT_NO)
       IF cont EQ 'No' THEN BEGIN
         RETURN, 0
       ENDIF ELSE BEGIN
         self->MESSAGE, 'Attempting to load savefile from a older version.', /INFO
         RETURN, 1
       ENDELSE
       
       
     END ;FUNCTION BST_INST_FIT_APP::SAVE_STATE_CHECK_VERSION


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Generate a filename for the save file.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_SAVE_FILENAME

       ; First get the prefix from the addons.
       filename = self.addons->GET_FILENAME_PREFIX()

       ; If no prefix was found generate a default.
       MIR_DEFAULT, filename, 'saved_fit'

       ; Add a file extension.
       filename += '.bst_inst'

       RETURN, filename

     END ; FUNCTION BST_INST_FIT_APP::GET_SAVE_FILENAME


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the save state for
     ;   [BST_INST_FIT_APP]
     ;
     ; DESCRIPTION:
     ;   This will return the state of:
     ;     fitter gui
     ;     fitter core
     ;     gui components
     ;     addons
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_STATE

       state = self.core->GET_STATE()

       state = CREATE_STRUCT(state, {addons:self.addons->GET_STATE()})

       RETURN, state

     END ; FUNCTION BST_INST_FIT_APP::GET_STATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Load the application state from a save file.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::LOAD_STATE, state, FORCE=force


       ; Check the version of the save state.
       IF ~ self->SAVE_STATE_CHECK_VERSION(state, FORCE=force) THEN BEGIN
         MESSAGE, 'Version mismatch. Could not load state.'
       ENDIF


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self->RESET_STATE
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'APP: Error resetting state.', /ERROR, /LOG
       ENDELSE


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.core->LOAD_STATE, state
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Error loading core state.', /ERROR, /LOG
       ENDELSE


       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.addons->LOAD_STATE, state.addons
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Error loading addons state.', /ERROR, /LOG
       ENDELSE


       ; Update the addons.
       self->UPDATE


     END ;PRO BST_INST_FIT_APP::LOAD_STATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Clear all cached values. Also applies to MODELS and ADDONS.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::CLEAR_CACHE
  
       ; Perform any postfit actions.
       self.core->CLEAR_CACHE
       self.addons->CLEAR_CACHE

     END ;PRO BST_INST_FIT_APP::CLEAR_CACHE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reset the application.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::RESET_STATE


     END ;PRO BST_INST_FIT_APP::RESET_STATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Fix all parameters.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::SET_FIXALL, fixall

       (self.core).models->SET_FIXALL, fixall
       self.addons->UPDATE


     END ;PRO BST_INST_FIT_APP::FIX_ALL


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the save status on all parameters.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::SET_SAVE, save

       (self.core).models->SET_SAVE, save
       self.addons->UPDATE


     END ;PRO BST_INST_FIT_APP::FIX_ALL


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Plot the spectrum and the fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PLOT, _REF_EXTRA=extra

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         self->MESSAGE, 'Plot failed.', /ERROR
         self->POSTERROR
         RETURN
       ENDIF

       plotter = self.addons->GET_ADDON('BST_INST_ADDON_PLOT')

       self->BEFORE_FIT

       plotter->PLOT, _STRICT_EXTRA=extra

       self->AFTER_FIT

     END ;PRO BST_INST_FIT_APP::PLOT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Plot the spectrum without displaying the fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PLOT_SPECTRUM, _REF_EXTRA=extra

       self->PLOT, /SPECTRUM, _STRICT_EXTRA=extra

     END ;PRO BST_INST_FIT_APP::PLOT_SPECTRUM


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the fitter core object
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_CORE

       RETURN, self.core

     END ; FUNCTION BST_INST_FIT_APP::GET_CORE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the addon dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_ADDON_DISPATCHER

       RETURN, self.addons

     END ; FUNCTION BST_INST_FIT_APP::GET_ADDON_DISPATCHER

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the addon dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_ADDON, name

       dispatcher = self->GET_ADDON_DISPATCHER()
       
       RETURN, dispatcher.GET_ADDON(name)

     END ; FUNCTION BST_INST_FIT_APP::GET_ADDON_DISPATCHER
     
     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return A reference to the model dispatcher object.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_MODEL_DISPATCHER

       RETURN, self.core->GET_MODEL_DISPATCHER()

     END ; FUNCTION BST_INST_FIT_APP::GET_MODEL_DISPATCHER




;=======================================================================
;#######################################################################
;#######################################################################
;
; Debugging methods.
;
;#######################################################################
;#######################################################################
;=======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Start the profiler.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PROFILER_START

       MIR_TIMER, /BEGIN_TIMING
       self.SET_OPTIONS_DEBUG, {timer:1}
       self.PROFILER_RESET

     END ;PRO BST_INST_FIT::START_PROFILER


     ;+=================================================================
     ; PURPOSE:
     ;   Stop the profiler and display the results.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PROFILER_STOP

       self.PROFILER_REPORT
       self.SET_OPTIONS_DEBUG, {timer:0}

       ; I probably don't want to stop the timer, incase I am timing
       ; a progarme at a higher level.
       ;MIR_TIMER, /END_TIMING

     END ;PRO BST_INST_FIT::STOP_PROFILER


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reset the MIR_TIMER for profiling.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PROFILER_RESET

       MIR_TIMER, /RESET

     END ;PRO BST_INST_FIT_APP::PROFILER_RESET


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Report the profiling results from MIR_TIMER.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::PROFILER_REPORT

       PRINT, '--------------------------------------'
       PRINT, 'Profiling results for BST_SPECTRAL_FIT'
       MIR_TIMER, /REPORT

     END ;PRO BST_INST_FIT_APP::PROFILER_REPORT




;=======================================================================
;#######################################################################
;#######################################################################
;
; Pass through methods.  
; These just call the equivilant core method.
;
;#######################################################################
;#######################################################################
;=======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the configuration dictionary.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_CONFIG

       RETURN, self.core->GET_CONFIG()

     END ;FUNCTION BST_INST_FIT_APP::GET_CONFIG


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the installation path.  This assumes that the
     ;   installation path has already be found and saved in the core.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_INSTALL_PATH

       RETURN, self.core->GET_INSTALL_PATH()

     END ; FUNCTION BST_INST_FIT_APP::GET_INSTALL_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current path.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_CURRENT_PATH

       RETURN, self.core->GET_CURRENT_PATH()

     END ; FUNCTION BST_INST_FIT_APP::GET_CURRENT_PATH


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current path.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::SET_CURRENT_PATH, path

       self.core->SET_CURRENT_PATH, path

     END ; PRO BST_INST_FIT_APP::SET_CURRENT_PATH

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Seth the user options
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::SET_OPTIONS_USER, struct

       self.core.SET_OPTIONS_USER, struct

     END ;PRO BST_INST_FIT_APP::SET_OPTIONS_USER



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the user options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_OPTIONS_USER

       RETURN, self.core.GET_OPTIONS_USER() 

     END ;FUNCTION BST_INST_FIT_APP::GET_OPTIONS_USER



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the debug options
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::SET_OPTIONS_DEBUG, struct

       self.core.SET_OPTIONS_DEBUG, struct

     END ;PRO BST_INST_FIT_APP::SET_OPTIONS_DEBUG



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a structure with the debug options.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_OPTIONS_DEBUG

       RETURN, self.core.GET_OPTIONS_DEBUG() 

     END ;FUNCTION BST_INST_FIT_APP::GET_OPTIONS_DEBUG



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle messages.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::MESSAGE, message_array $
                                    ,SCOPE_LEVEL=scope_level_in $
                                    ,_REF_EXTRA=extra

       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       
       IF OBJ_VALID(self.core) THEN BEGIN
         ; Pass off the message to the core messaging routine.
         self.core->MESSAGE, message_array $
                             ,SCOPE_LEVEL=scope_level $
                             ,_STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         ; The core is not initialized, so just print the message.
         MIR_LOGGER, message_array $
                     ,SCOPE_LEVEL=scope_level $
                     ,_EXTRA=extra
       ENDELSE

     END ;PRO BST_INST_FIT_APP::MESSAGE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the current application version.
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_VERSION

       RETURN, self.core->GET_VERSION()

     END ; FUNCTION BST_INST_FIT_APP::GET_VERSION

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a model object from the core.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_APP::GET_MODEL, name

       RETURN, self.core->GET_MODEL(name)

     END ; FUNCTION BST_INST_FIT_APP::GET_VERSION





;=======================================================================
;#######################################################################
;#######################################################################
;
; Object definition.
;
;#######################################################################
;#######################################################################
;=======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on object destruction object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP::CLEANUP

       ; Clean up garbage.
       CATCH, error
       IF error EQ 0 THEN BEGIN
PRINT, 'Starting: BST_INST_FIT_APP::CLEANUP'
HEAP_GC, /VERBOSE
       ENDIF ELSE BEGIN
         MESSAGE, 'Error cleaning up lost references.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE

       ; Destroy the addons.
       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.addons->DESTROY
PRINT, 'Addons destroyed.'
HEAP_GC, /VERBOSE
       ENDIF ELSE BEGIN
         MESSAGE, 'Error destroying addons.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE

       ; Destroy the fitter core.
       CATCH, error
       IF error EQ 0 THEN BEGIN
         self.core->DESTROY
PRINT, 'Core destroyed.'
HEAP_GC, /VERBOSE
       ENDIF ELSE BEGIN
         MESSAGE, 'Error destroying fitting core.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE

PRINT, 'Done: BST_INST_FIT_APP::CLEANUP'
HEAP_GC, /VERBOSE

     END ;PRO BST_INST_FIT_APP::CLEANUP




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object defines the main GUI for <BST_SPECTRAL_FIT>
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_FIT_APP__DEFINE

       struct = { BST_INST_FIT_APP $

                  ,addons:OBJ_NEW() $

                  ,INHERITS BST_INST_BASE $
                }

     END ;PRO BST_INST_FIT_APP__DEFINE
