



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2011-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Define an obejct that will be inherited by all gui components
;   that are part of the BST_ANALYSIS_SUITE.
;   
;
;-======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the [BST_INST_GUI_BASE] object.
     ;
     ;   This will be called when the component object is created.
     ;
     ;-=================================================================
     FUNCTION BST_INST_GUI_BASE::INIT, core $
                                       ,guicore $
                                       ,_REF_EXTRA=extra


       ; Call the INIT methods for the inherited classes.
       status = self->BST_INST_BASE::INIT(core, guicore)
       IF status NE 1 THEN MESSAGE, 'BST_INST_BASE::INIT failed.'

       status = self->WIDGET_OBJECT::INIT(_STRICT_EXTRA=extra)
       IF status NE 1 THEN MESSAGE, 'WIDGET_OBJECT::INIT failed.'

       RETURN, 1

     END ;FUNCTION BST_INST_GUI_BASE::INIT




     ;+=================================================================
     ;
     ; This is the main event handler for the BST_INST_GUI_BASE.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUI_BASE::EVENT_HANDLER, event $
                                          ,EVENT_HANDLED=event_handled

       ; First pass the event to the core class
       self->WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN

     END ; PRO BST_INST_GUI_BASE::EVENT_HANDLER



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on object destruction object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUI_BASE::CLEANUP

       ; Call the CLEANP method of the base GUI classes.
error = 0
       ;CATCH, error
       IF error EQ 0 THEN BEGIN
         self->BST_INST_BASE::CLEANUP
       ENDIF ELSE BEGIN
         MESSAGE, 'BST_INST_BASE::CLEANUP failed.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE
       

       ;CATCH, error
       IF error EQ 0 THEN BEGIN
         self->WIDGET_OBJECT::CLEANUP
       ENDIF ELSE BEGIN
         MESSAGE, 'WIDGET_OBJECT::CLEANUP failed.', /CONTINUE
         CATCH, /CANCEL
       ENDELSE

       


     END ;PRO BST_INST_GUI_BASE::CLEANUP




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object is to be inherited by all of the gui component
     ;   of the <BST_SPECTRAL_FIT>.
     ;
     ;   It defines the basic gui functions and calls.
     ;
     ;
     ;
     ;-=================================================================
     PRO BST_INST_GUI_BASE__DEFINE

       struct = { BST_INST_GUI_BASE $

                  ,INHERITS BST_INST_BASE $
                  ,INHERITS WIDGET_OBJECT $
                }


     END ;PRO BST_INST_GUI_BASE__DEFINE
