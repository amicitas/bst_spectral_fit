




     ;+=================================================================
     ; Generate the default filenames for the log files.
     ;
     ;-=================================================================
     FUNCTION BST_INST_LOG_GENERATE_FILENAME

       ; For now I will just send all log files to my directory.
       ; In the future I probably want to send this either to
       ; the  installation directory or to the users home directory.
       path = '/u/antoniuk/b-stark/idl/bst_instrumental/log/'
       filetail = 'bst_inst__'+GETENV('USER')+'.log'

       filepath = MIR_PATH_JOIN([path, filetail])
       RETURN, filepath

     END ; FUNCTION BST_INST_LOG_GENERATE_FILENAME



     ;+=================================================================
     ;
     ; Return a string with the current date and time. 
     ;
     ;-=================================================================
     FUNCTION BST_INST_LOG_GET_TIME_STRING


       ; Get the current time.
       time_string = STRING(SYSTIME(/JULIAN) $
                            ,FORMAT='(C(CYI4,"-",CMOI2.2, "-", CDI2.2' $
                            + ',1X, CHI2.2,":",CMI2.2,":",CSI2.2))')
       RETURN, time_string

     END ; FUNCTION BST_INST_LOG_GET_TIME_STRING



     ;+=================================================================
     ; Open the log file.
     ;
     ;-=================================================================
     PRO BST_INST_LOG_OPEN
       COMMON BST_INST_LOG, c_log_lun
       
       FORWARD_FUNCTION BST_INST_LOG_IS_OPEN

       IF BST_INST_LOG_IS_OPEN() THEN BEGIN
         MESSAGE, 'Log file is already open.'
       ENDIF


       ; Check to see if the 'BST_INST_LOG' evironmental variable
       ; has been set.
       log_filename = GETENV('BST_INST_LOG')
       IF log_filename EQ '' THEN BEGIN
         log_filename = BST_INST_LOG_GENERATE_FILENAME()
       ENDIF

       IF FILE_TEST(log_filename) THEN BEGIN

         ; I want to limit the size of the log file. I will always
         ; keep the entire log from the current session, but I want
         ; to truncate the log from the previous session.
         BST_INST_LOG_TRUNCATE, log_filename

         OPENU, c_log_lun, log_filename, /GET_LUN, /APPEND
       ENDIF ELSE BEGIN
         OPENW, c_log_lun, log_filename, /GET_LUN
       ENDELSE

       ; Print some white space
       PRINTF, c_log_lun, ''
       PRINTF, c_log_lun, ''

       ; Print a header
       time_string = BST_INST_LOG_GET_TIME_STRING()
       PRINTF, c_log_lun, 'BST_INSTRUMENTAL_FIT log file'
       PRINTF, c_log_lun, time_string
       
     END ;PRO BST_INST_LOG_OPEN



     ;+=================================================================
     
     ; Check if the log file is initialized and open.
     ;
     ;-=================================================================
     FUNCTION BST_INST_LOG_IS_OPEN
       COMMON BST_INST_LOG

       IF N_ELEMENTS(c_log_lun) EQ 0 THEN RETURN, 0

       ; Check if the log file is already open.
       file_info = FSTAT(c_log_lun)
       RETURN, KEYWORD_SET(file_info.open)

     END ; FUNCTION BST_INST_LOG_IS_OPEN



     ;+=================================================================
     ;
     ; Close the log file
     ;
     ;-=================================================================
     PRO BST_INST_LOG_CLOSE
       COMMON BST_INST_LOG

       FREE_LUN, c_log_lun

     END ;PRO BST_INST_LOG_CLOSE


     ;+=================================================================
     ;
     ; Truncate the log file.
     ;
     ;-=================================================================
     PRO BST_INST_LOG_TRUNCATE, log_filename
       COMMON BST_INST_LOG

       ; I want to limit the size of the log file. I will always
       ; keep the entire log from the current session, but I want
       ; to truncate the log from the previous session.
       max_previous_length = 5000

       ; First count the number of lines.
       num_lines = FILE_LINES(log_filename)

       IF num_lines LE max_previous_length THEN RETURN

       ; We need to truncate the file.
       buffer = STRARR(max_previous_length)

       OPENR, lun, log_filename, /GET_LUN

       num_to_truncate = num_lines - max_previous_length

       line = ''
       count = 0
       buffer_count = 0
       WHILE ~ EOF(lun) DO BEGIN
         READF, lun, line
         count += 1
         IF count GT num_to_truncate THEN BEGIN
           buffer[buffer_count] = line
           buffer_count += 1
         ENDIF
       ENDWHILE

       ; Done reading
       CLOSE, lun
           
       ; Now write the truncated file.
       OPENW, lun, log_filename
       FOR ii=0,max_previous_length-1 DO BEGIN
         PRINTF, lun, buffer[ii]
       ENDFOR

       FREE_LUN, lun
         

     END ; PRO BST_INST_LOG_TRUNCATE


     ;+=================================================================
     ; Saves the current log file, then reopens
     ;
     ;-=================================================================
     PRO BST_INST_LOG_SAVE
       COMMON BST_INST_LOG


       IF ~ BST_INST_LOG_IS_OPEN() THEN BEGIN
         MESSAGE, 'Log file is not open.'
       ENDIF

       file_info = FSTAT(c_log_lun)

       CLOSE, c_log_lun
       OPENU, c_log_lun, file_info.name, /APPEND

     END ;PRO BST_INST_LOG_CLOSE



     ;+=================================================================
     ; Write messages to the text frame
     ;
     ;-=================================================================
     PRO BST_INST_LOG, message, NOLOG=nolog
       COMMON BST_INST_LOG

       time_string = BST_INST_LOG_GET_TIME_STRING()
       
       ; Create the log entry
       log_entry = '['+time_string+'] '+message

       ; Print to the terminal
       PRINT, log_entry

       IF KEYWORD_SET(nolog) THEN RETURN
       IF ~ BST_INST_LOG_IS_OPEN() THEN BEGIN
         PRINT, 'Log file is not open'
       ENDIF

       PRINTF, c_log_lun, log_entry

     END ;PRO BST_INST_LOG



     ;+=================================================================
     ;
     ; Handle messages for <BST_SPECTRAL_FIT>.
     ;
     ; Messages can be directed to 3 places:
     ;   1. The status bar.
     ;   2. The message area
     ;   3. The log file.
     ;
     ; All messages will be sent to the log file unless /NOLOG is
     ; set.
     ;
     ; All messages will be set to the message area unless /LOG or
     ; /STATUS is set.
     ;
     ;-=================================================================
     PRO BST_INST_MESSAGE, message $
                           ,STATUS=status $
                           ,LOG=log $
                           ,NOLOG=nolog $
                           ,_EXTRA=extra

       COMMON BST_INST_GUI

       ; Send to the log.
       ; We do this even if /NOLOG is set since all log entries are
       ; echoed to the terminal.
       BST_INST_LOG, message, NOLOG=KEYWORD_SET(nolog)


       IF KEYWORD_SET(log) THEN RETURN


       IF KEYWORD_SET(status) THEN BEGIN
         WIDGET_CONTROL, c_gui_param.id_struct.ui_label_status, SET_VALUE=message
         RETURN
       ENDIF

       WIDGET_CONTROL, c_gui_param.id_struct.ui_text_message, GET_VALUE=value

       num_lines = N_ELEMENTS(value)
       num_message_lines = N_ELEMENTS(message)
       FOR i=0,num_message_lines-1 DO BEGIN
         ; Shift the lines up
         value[0:num_lines-2] = value[1:num_lines-1]
         ; Add the message
         value[num_lines-1] = message[i]
         
         WIDGET_CONTROL, c_gui_param.id_struct.ui_text_message, SET_VALUE=value
       ENDFOR

     END ;PRO BST_INST_MESSAGE
