




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Define a list object to use as part of the [BST_INST_MULTI_MODEL::]
;   object.
;
; DESCRIPTION:
;   This class is derived from simplelist but handles copying 
;   differntly.  It assumes that when copying from another
;   [BST_INST_MODELLIST::] the same number of objects will be
;   in each list and we can copy accross directly.
;   
;
;-======================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   Copy the data from another modellist.
     ; 
     ;   If the list contains objects, we retain the current objects
     ;   but copy over the data.
     ;
     ;-=================================================================
     PRO BST_INST_MODELLIST::COPY_FROM, data, RECURSIVE=recursive


       IF ~ KEYWORD_SET(recursive) THEN BEGIN
         self->SIMPLELIST::COPY_FROM, data
         RETURN
       ENDIF

       FOR ii=0,self.num_elements-1 DO BEGIN
         IF OBJ_VALID((*self.ptr_array)[ii]) THEN BEGIN
           (*self.ptr_array)[ii]->COPY_FROM, (*data.ptr_array)[ii], RECURSIVE=recursive
         ENDIF ELSE BEGIN
           (*self.ptr_array)[ii] = (*data.ptr_array)[ii]
         ENDELSE
       ENDFOR


     END  ;  PRO BST_INST_MODELLIST::COPY_FROM, data



     ;+=================================================================
     ; PURPOSE:
     ;   Object definition for the BST_INST_MODELLIST object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_MODELLIST__DEFINE
       
       struct = { BST_INST_MODELLIST $
                  ,INHERITS SIMPLELIST $
                }
                 
     END ;PRO BST_INST_MODELLIST__DEFINE
