

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   npablant@pppl.gov
;
; DATE:
;   2016-02
;
; PURPOSE:
;   This is the base class for <BST_SPECTRAL_FIT> addons.
;
;   All dispatched objects should be derived from this class.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Set the class flags.  These determine the class type and
     ;   default options.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::SET_FLAGS

       self.active = 0
       self.enabled = 0
       self.default = 0
       self.internal = 0

     END ; PRO BST_INST_DISPATCHED::SET_FLAGS



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the [BST_INST_DISPATCHED] object.
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::INIT, dispatcher

       IF ISA(dispatcher) THEN BEGIN
         self.dispatcher = dispatcher
       ENDIF

       ; Maybe useful for future standardization?
       ;self.options = MIR_HASH()
       
       self->RESET
       self->SET_FLAGS

       RETURN, 1

     END ; FUNCTION BST_INST_DISPATCHED::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set the reference to the dispatcher object.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::SET_DISPATCHER, dispatcher
       
       self.dispatcher = dispatcher

     END ;PRO BST_INST_DISPATCHED::SET_DISPATCHER



     ;+=================================================================
     ; PURPOSE:
     ;   This method is called after all of the dispatcheds have been
     ;   instantiated.  This allows for any initialization that
     ;   requires the presence of another dispatched.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::INITIALIZE
     END ; PRO BST_INST_DISPATCHED::INITIALIZE



     ;+=================================================================
     ; PURPOSE:
     ;   Clear all cached values. Also applies to MODELS and ADDONS.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::CLEAR_CACHE, _NULL=_null
       
       ; This should be reimplemented by any sub-classes.

     END ; PRO BST_INST_DISPATCHED::CLEAR_CACHE

     
     
     ;+=================================================================
     ; PURPOSE:
     ;   Reset the dispatched back to the default parameters.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::RESET, _NULL=_null
       
       ; This should be reimplemented by any sub-classes.

     END ; PRO BST_INST_DISPATCHED::RESET



     ;+=================================================================
     ; PURPOSE:
     ;   Perform celeanup for the [BST_INST_DISPATCHED] object before
     ;   it is destroyed.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::CLEANUP

       self->DISABLE

     END ; PRO BST_INST_DISPATCHED::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Enable the dispatched.
     ;
     ;   This method needs to be implemented by any sub-classes.
     ;
     ;
     ;   In general this method will setup the gui and any common
     ;   blocks and defaults for the dispatched.
     ;
     ;   NOTE: This method can be called even when the dispatched is already
     ;         active.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::ENABLE

       self->MESSAGE, 'Enabling '+OBJ_CLASS(self)+' object.', /LOG

       self->HANDLE_REQUIRED
       self->HANDLE_CONFLICTING

       self.enabled = 1
       
     END ; PRO BST_INST_DISPATCHED::ENABLE



     ;+=================================================================
     ; PURPOSE:
     ;   Disable the dispatched.
     ;
     ;   This method needs to be implemented by any sub-classes.
     ;
     ;
     ;   In general this method will destry the gui and reset any
     ;   changes dispatched specific settings in the main program.
     ;
     ;   NOTE: This method can be called even when the dispatched is already
     ;         inactive.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::DISABLE

       IF self->IS_ENABLED() THEN BEGIN
         self->MESSAGE, 'Disabling '+OBJ_CLASS(self)+' object.', /LOG
       ENDIF

       self.enabled = 0
       
     END ; PRO BST_INST_DISPATCHED::DISABLE




     ;+=================================================================
     ; PURPOSE:
     ;   Handle conflicting dispatcheds.
     ;
     ;   In general this method will be called before an dispatched is
     ;   enabled, and will disable any conflicting dispatcheds.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::HANDLE_CONFLICTING

       conflicting_array = STRSPLIT(self.conflicting, ', ', /EXTRACT)
       IF SIZE(conflicting_array, /DIM) EQ 0 THEN RETURN

       num_conflicting = N_ELEMENTS(conflicting_array)

       FOR ii=0,num_conflicting-1 DO BEGIN
         self.dispatcher->DISABLE_OBJECT, conflicting_array[ii], /QUIET
       ENDFOR

     END ; PRO BST_INST_DISPATCHED::HANDLE_CONFLICTING



     ;+=================================================================
     ; PURPOSE:
     ;   Handle required dispatcheds.
     ;
     ;   In general this method will be called before an dispatched is
     ;   enabled, and will enable any required dispatcheds
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::HANDLE_REQUIRED

       required_array = STRSPLIT(self.required, ', ', /EXTRACT)
       IF SIZE(required_array, /DIM) EQ 0 THEN RETURN

       num_required = N_ELEMENTS(required_array)

       FOR ii=0,num_required-1 DO BEGIN
         self.dispatcher->ENABLE_OBJECT, required_array[ii]
       ENDFOR

     END ; PRO BST_INST_DISPATCHED::HANDLE_REQUIRED



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Handle messaging for dispatcheds.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::MESSAGE, message_array $
                                      ,SCOPE_LEVEL=scope_level_in $
                                      ,_REF_EXTRA=extra
       ; Update the scope.
       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       
       IF OBJ_VALID(self.dispatcher) THEN BEGIN
         self.dispatcher->MESSAGE, message_array $
                                   ,SCOPE_LEVEL=scope_level $
                                   ,_STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         MIR_LOGGER, message_array $
                     ,SCOPE_LEVEL=scope_level $
                     ,_EXTRA=extra
       ENDELSE

     END ;PRO BST_INST_DISPATCHED::MESSAGE


; ======================================================================
; ======================================================================
; ######################################################################
;
; BEGIN METHODS TO BE CALLED BY THE FITTER
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called for all active dispatcheds before the
     ;   fit is started.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;   This method is the first thing when starting a fit. It is
     ;   called before any of the CORE is setup.
     ;
     ;
     ;   This routine should be reimplemented by any sub clases that
     ;   need to action before starting the fit procedure.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::BEFORE_FIT

       ; This method should be reimplemented by dispatcheds when needed.

     END ; PRO BST_INST_DISPATCHED::BEFORE_FIT


     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called for all active dispatcheds immediatly
     ;   before the fit loop is started.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;   This method is called after all of the models have been
     ;   setup and immediatly before the fit loop is started.
     ;
     ;
     ;   This routine should be reimplemented by any sub clases that
     ;   need prefit actions.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::PREFIT

       ; This method should be reimplemented by dispatcheds when needed.

     END ; PRO BST_INST_DISPATCHED::PREFIT



     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called for all active dispatcheds immediatly 
     ;   after the fit loop is completed.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;
     ;   This method is called immidiatly after the fit loop is 
     ;   complete.
     ;
     ;   Any analysis of the final fit results shoud happen here.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::POSTFIT

       ; This should be reimplemented by any sub-classes.

     END ; PRO BST_INST_DISPATCHED::POSTFIT



     ;+=================================================================
     ; PURPOSE:
     ;   This method will be called for all active dispatcheds after
     ;   the fit procedure is complete.
     ;
     ; DESCRIPTION:
     ;   See [BST_INST_FIT_GUI::FIT] for a description of the fit
     ;   procedure showing the order in which this routine is called.
     ;
     ;   This method is called after the fit procedure is complete 
     ;   and the models will have been restored to their initial
     ;   state except for parameters flagged with 'save'.
     ;
     ;
     ;   Any analysis of the final fit results shoud happen here.
     ;
     ;
     ;   This routine should be reimplemented by any sub clases that
     ;   need after-fit actions.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::AFTER_FIT

       ; This method should be reimplemented by dispatcheds when needed.

     END ; PRO BST_INST_DISPATCHED::AFTER_FIT



     ;+=================================================================
     ; PURPOSE:
     ;   Get the save structure for this dispatched.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::GET_STATE

       RETURN, {version:self.version $
                ,enabled:self.enabled}

     END ; FUNCTION BST_INST_DISPATCHED::GET_STATE



     ;+=================================================================
     ; PURPOSE:
     ;   Load a save structure for this dispatched.
     ;
     ;   This routine should be reimplemented by any sub clases.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::LOAD_STATE, state

       ; Reset the dispatched.
       self->RESET

       ; First check if the save_structure has this object enabled.
       IF state.enabled THEN BEGIN
         self->ENABLE
       ENDIF ELSE BEGIN
         self->DISABLE
       ENDELSE
       
     END ; PRO BST_INST_DISPATCHED::LOAD_STATE


     ;+=================================================================
     ; PURPOSE:
     ;   Get the output structure for this dispatched.
     ;   The contents of the output structure will be used when writing
     ;   output files.
     ;
     ;   This routine should be reimplemented by any classes.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::GET_OUTPUT_STRUCTURE

       ; This method should be reimplemented by dispatcheds when needed.
       RETURN, {NULL}

     END ; FUNCTION BST_INST_DISPATCHED::GET_OUTPUT_STRUCTURE



     ;+=================================================================
     ; PURPOSE:
     ;   Return a prefix to be used when generated filenames.
     ;
     ; DESCRIPTION:
     ;   This method will be used mostly with dispatcheds that control
     ;   the spectrum to be used.
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::GET_FILENAME_PREFIX, _EXTRA=extra

       ; This should be reimplemented by any dispatched sub-classes for
       ; which a filename prefix make sense.

       RETURN, ''

     END ; FUNCTION BST_INST_DISPATCHED::GET_FILENAME_PREFIX



     ;+=================================================================
     ; PURPOSE:
     ;   Return a plotlist to be added to the spectrum.
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::GET_SPECTRUM_PLOTLIST, x_values

       ; This method should be reimplemented by dispatcheds when needed.
       RETURN, OBJ_NEW('MIR_PLOTLIST')

     END ; FUNCTION BST_INST_DISPATCHED::GET_SPECTRUM_PLOTLIST



     ;+=================================================================
     ; PURPOSE:
     ;   Update the dispatched.
     ;
     ;   This routine will be called whenever the state of the
     ;   [BST_INST_FIT::] or any of the models is likely to have 
     ;   changed.
     ;
     ;-=================================================================
     PRO BST_INST_DISPATCHED::UPDATE

       ; This should be reimplemented by any sub-classes.

     END ; PRO BST_INST_DISPATCHED::UPDATE




; ======================================================================
; ======================================================================
; ######################################################################
;
; PROPERTY ACESS ROUTINES
;
; ######################################################################
; ======================================================================
; ======================================================================




     ;+=================================================================
     ;   Return whether or not this dispatched is active
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::IS_ACTIVE

       RETURN, self.active

     END ; FUNCTION BST_INST_DISPATCHED::IS_ACTIVE



     ;+=================================================================
     ;   Return whether or not this dispatched is enabled
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::IS_ENABLED

       RETURN, self.enabled

     END ; FUNCTION BST_INST_DISPATCHED::IS_ENABLED



     ;+=================================================================
     ;   Return whether or not this dispatched is an internal dispatched.
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::IS_INTERNAL

       RETURN, self.internal

     END ; FUNCTION BST_INST_DISPATCHED::IS_INTERNAL


     ;+=================================================================
     ;   Return whether or not this dispatched should be enabled by default.
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::IS_DEFAULT

       RETURN, self.default

     END ; FUNCTION BST_INST_DISPATCHED::IS_DEFAULT



     ;+=================================================================
     ;   Returen the name of this dispatched object
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::NAME

       RETURN, OBJ_CLASS(self)

     END ; FUNCTION BST_INST_DISPATCHED::NAME


     
     ;+=================================================================
     ;   Returen the options for this object.
     ;-=================================================================
     FUNCTION BST_INST_DISPATCHED::GET_OPTIONS

       RETURN, self.options

     END ; FUNCTION BST_INST_DISPATCHED::GET_OPTIONS


     
     ;+=================================================================
     ;   Returen the options for this object.
     ;-=================================================================
     PRO BST_INST_DISPATCHED::SET_OPTIONS, options_in

       RETURN
       
       ; Maybe useful for future standardization?
       ; For now this needs to be reimplemented.
       IF MIR_IS_STRUCT(options_in) THEN BEGIN
         options = MIR_HASH(options_in, /EXTRACT)
       ENDIF ELSE BEGIN
         options = options_in
       ENDELSE

       self.options.JOIN, options

     END ; FUNCTION BST_INST_DISPATCHED::SET_OPTIONS

     

; ======================================================================
; ======================================================================
; ######################################################################
;
; OBJECT DEFINITION
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Define the base class for <BST_SPECTRAL_FIT> dispatcheds.
     ;
     ;   Most dispatcheds should inherit [BST_INST_DISPATCHED_GUI::] wich is
     ;   derived from this dispatched.
     ;-=================================================================
     PRO BST_INST_DISPATCHED__DEFINE


       struct = { BST_INST_DISPATCHED $
                  ,dispatcher:OBJ_NEW() $
                  ,version:{VERSION} $
                          
                  ,conflicting:'' $
                  ,required:'' $

                  ,enabled:0 $
                  ,active:0 $
                  ,default:0 $
                  ,internal:0 $

                  ,add_to_menu:0 $
                  ,menu_title:'' $
                  ,title:'' $

                  ; Maybe useful for future standardization?
                  ;,options:OBJ_NEW() $
       
                  ,INHERITS OBJECT $
                }

     END ; PRO BST_INST_DISPATCHED__DEFINE
