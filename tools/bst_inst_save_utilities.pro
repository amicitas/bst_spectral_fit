


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-12
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   A collection of utilities to manipulate <BST_SPECTRAL_FIT>
;   save files.
;
;   The routine are for development use only.  If routine conversions
;   are needed between various production versions of the fitter
;   they need to be added to a portion of the <BST_SPECTRAL_FIT>
;   code.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Convert a <BST_SPECTRAL_FIT> save file to only contain
     ;   anonymous structures.
     ;
     ;-=================================================================
     PRO BST_INST_SAVE_ANONYMIZE, filepath, VERSION2=version2, BACKUP=backup

       path_parts = MIR_PATH_SPLIT(filepath)

       IF KEYWORD_SET(backup) THEN BEGIN
         filepath_named = MIR_PATH_JOIN([ path_parts.path $
                                      ,path_parts.filehead + '__named.' + path_parts.filetail])
         FILE_MOVE, filepath, filepath_named, /VERBOSE
       ENDIF ELSE BEGIN
         filepath_named = filepath
       ENDELSE

       IF KEYWORD_SET(version2) THEN BEGIN 
         RESTORE, FILENAME=filepath_named, /RELAXED, /VERBOSE
         bst_inst_save_state = STRUCT_ANONYMOUS(bst_inst_save_state)
         SAVE, bst_inst_save_state, FILENAME=filepath
       ENDIF ELSE BEGIN
         bst_inst_save_state = MIR_RESTORE_OBJECT(FILENAME=filepath_named, /RELAXED)
         bst_inst_save_state = STRUCT_ANONYMOUS(bst_inst_save_state)
         MIR_SAVE_OBJECT, bst_inst_save_state, FILENAME=filepath
       ENDELSE

       PRINT, 'Saved anonymized save file to:'
       PRINT, filepath

     END ;PRO BST_INST_SAVE_ANONYMIZE



     ;+=================================================================
     ; PURPOSE:
     ;   Convert a <BST_SPECTRAL_FIT> save file created using
     ;   the first development format of version 3.0.0 to the final
     ;   version.
     ;
     ;-=================================================================
     PRO BST_INST_SAVE_UPDATE, filepath

       RESTORE, FILENAME=filepath, /RELAXED
       
       new_save = {version:bst_inst_save_state.version $
                   ,core:bst_inst_save_state.core.core $
                   ,models:bst_inst_save_state.core.models $
                   ,addons:bst_inst_save_state.addons $
                  }
       
       MIR_SAVE_OBJECT, new_save, FILENAME=filepath

       PRINT, 'Updated save file:'
       PRINT, filepath

     END ;PRO BST_INST_SAVE_UPDATE



     ;+=================================================================
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SAVE_UTILITIES
     END ;PRO BST_INST_SAVE_UTILITIES
