


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-08
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   This object contains the interface to CURVEFIT_SWITCH for
;   <BST_SPECTRAL_FIT>
;
;-======================================================================

     
     

     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object.
     ;
     ;-=================================================================
     FUNCTION BST_INST_CURVEFIT::INIT, core, _REF_EXTRA=extra

       status = self->BST_INST_BASE::INIT(core, _STRICT_EXTRA=extra)
       
       self.input.parinfo = OBJ_NEW('SIMPLELIST')

       self.results.yfit = OBJ_NEW('SIMPLELIST')
       self.results.parameters = OBJ_NEW('SIMPLELIST')
       self.results.sigma = OBJ_NEW('SIMPLELIST')

       RETURN, status
     END ;FUNCTION BST_INST_CURVEFIT::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Return a blanck structure for the fit variable parameters
     ;   accepted by CURVEFIT_SWITCH and used by BST_INST_FIT
     ; 
     ;-=================================================================
     FUNCTION BST_INST_CURVEFIT::BLANK_PARINFO, VALUE=value $
                                                ,FIXED=fixed $
                                                ,LIMETED=limited $
                                                ,LIMITS=limits
       MIR_DEFAULT, value, 0D
       MIR_DEFAULT, fixed, 0
       MIR_DEFAULT, limited, [0,0]
       MIR_DEFAULT, limits, [0D,0D]


       parinfo = { $
                   value:value $
                   ,fixed:fixed $
                   ,limited:limited $
                   ,limits:limits $
                 }
       RETURN, parinfo

     END ;FUNCTION BST_INST_CURVEFIT::BLANK_PARINFO



     ;+=================================================================
     ; PURPOSE:
     ;   Create the array of independant fit variables and the
     ;   array of assosiated parameters from the models.
     ;
     ; DESCRIPTION:
     ;   CURVEFIT_SWITCH, the interface to the non-linear least
     ;   squares fitting core, requires as inputs:
     ;    1. An array with the independant fit variables.
     ;    2. An array of structures with the parameters for each
     ;       fit variable.
     ;
     ;   This routines extracts these parameters from the models
     ;   and saves them into the object.
     ; 
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::MODELS_TO_CURVEFIT

       self->CLEAR_INPUT

       ; Fill the fit array
       parinfo = (self.core).models->GET_PARAMETER_INFO()

       self.input.parinfo->FROM_ARRAY, parinfo

     END ;PRO BST_INST_CURVEFIT::MODELS_TO_CURVEFIT

     

     ;+=================================================================
     ; PURPOSE:
     ;   Take the array of independant fit variables (which is what
     ;   is actually manipulated by the fitter) and update the models 
     ;   with these values.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::CURVEFIT_TO_MODELS

       ; Fill the fit array.
       self->EXTRACT_PARAMETER_ARRAY, self.results.parameters->TO_ARRAY()

       ; Fill the sigma array.
       self->EXTRACT_SIGMA_ARRAY, self.results.sigma->TO_ARRAY()

     END ;PRO BST_INST_CURVEFIT::CURVEFIT_TO_MODELS



     ;+=================================================================
     ; PURPOSE:
     ;   Take the array of independant fit variables (which is what
     ;   is actually manipulated by the fitter) and update the models 
     ;   with these values.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::EXTRACT_PARAMETER_ARRAY, parameter_array

       ; Fill the fit array
       (self.core).models->EXTRACT_PARAMETER_ARRAY, parameter_array

     END ;PRO BST_INST_CURVEFIT::EXTRACT_PARAMETER_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Take the array of errors in the fits and up pdate the models 
     ;   with these values.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::EXTRACT_SIGMA_ARRAY, sigma_array

       ; Fill the fit array
       (self.core).models->EXTRACT_SIGMA_ARRAY, sigma_array

     END ;PRO BST_INST_CURVEFIT::EXTRACT_SIGMA_ARRAY


     ;+=================================================================
     ; PURPOSE:
     ;   Do the actual fit.
     ; 
     ;-=================================================================
     PRO BST_INST_CURVEFIT::FIT

       ; Do the fit
       ; ----------

       self->CLEAR_RESULTS

       ; Create the fit array from the model objects.
       ; the fit array is saved in a common variable.
       self->MODELS_TO_CURVEFIT


       ; Make local copies of fit_array & parinfo
       parinfo = self.input.parinfo->TO_ARRAY()


       ; Find the number of free fit parameters
       num_free = 0
       FOR ii=0,N_ELEMENTS(parinfo)-1 DO BEGIN
         IF parinfo[ii].fixed EQ 0 THEN num_free += 1
       ENDFOR


       ; Retrieve the spectrum
       spectrum = (self.core).spectrum->GET_SPECTRUM()

;PRINT, 'INITIAL:', parinfo.value


       ; Do the acutual fit
       ; CURVEFIT_SWITCH does the following, unless /MPCURVEFIT, or /CURVEFIT are used.
       ; 0 free variables: Simply evaluates the function.
       ; >0 free variable: MPCURVEFIT is used.
       ;
       ; This routine also handles error messages and calculates various things.
       yfit = CURVEFIT_SWITCH(spectrum.x $
                              ,spectrum.y $
                              ,spectrum.weight $
                              ,fit_array $
                              ,sigma $
                              ,PARINFO = parinfo $
                              ,FUNCTION_NAME = 'EVALUATE' $
                              ,OBJECT_REFERENCE = self $
                              ,CHISQ = chisq $
                              ,REDUCED_CHISQ = reduced_chisq $
                              ,FIT_STATUS = status $
                              ,VERBOSE=(self.core).debug.debug $
                              ,/NODERIVATIVE $
                              ,COVAR = covar $
                              ,USE_CURVEFIT=(self.core).options_user.use_curvefit $
                              ,/PRINT_STATUS $
                              ,DEBUG=(self.core).debug.debug $
                              ,MESSAGE_PRO='MESSAGE' $
                              ,MESSAGE_OBJ=self.core $
                             )


       free_parameters = TOTAL(~ parinfo.fixed)
       PRINT, FORMAT='(a0, i0)', 'Free parameters: ', free_parameters

;PRINT, 'FINAL  :', fit_array

       self.results.yfit->FROM_ARRAY, yfit
       self.results.parameters->FROM_ARRAY, fit_array
       self.results.sigma->FROM_ARRAY, sigma
       self.results.chisq = chisq
       self.results.reduced_chisq = reduced_chisq
       self.results.status = status
       self.results.time = SYSTIME(/JULIAN)


       ; Do a final update of the model state.
       self->CURVEFIT_TO_MODELS


     END ;PRO BST_INST_CURVEFIT::FIT



     ;+=================================================================
     ; PURPOSE:
     ;   Clear the results. 
     ;
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::CLEAR_RESULTS

       self.results.yfit->REMOVE, /ALL
       self.results.parameters->REMOVE, /ALL
       self.results.sigma->REMOVE, /ALL
       self.results.chisq = 0D
       self.results.reduced_chisq = 0D
       self.results.status = 0
       self.results.time = 0D

     END ;PRO BST_INST_CURVEFIT::CLEAR_RESULTS



     ;+=================================================================
     ; PURPOSE:
     ;   Get a structure with the current results.
     ;
     ;-=================================================================
     FUNCTION BST_INST_CURVEFIT::GET_RESULTS

       ; Make sure that there is a valid fit saved.
       IF self.results.time NE 0 THEN BEGIN
         results = { $
                     yfit:self.results.yfit->TO_ARRAY() $
                     ,parameters:self.results.parameters->TO_ARRAY() $
                     ,sigma:self.results.sigma->TO_ARRAY() $
                     ,chisq:self.results.chisq $
                     ,reduced_chisq:self.results.reduced_chisq $
                     ,status:self.results.status $
                     ,time:self.results.time $
                   }
       ENDIF ELSE BEGIN
         results = {NULL}
       ENDELSE

       RETURN, results

     END ;FUNCTION BST_INST_CURVEFIT::GET_RESULTS



     ;+=================================================================
     ; PURPOSE:
     ;   Clear the input. 
     ;
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::CLEAR_INPUT

       self.input.parinfo->REMOVE, /ALL

     END ;PRO BST_INST_CURVEFIT::CLEAR_INPUT



     ;+=================================================================
     ; PURPOSE:
     ;   This is where we actually compute the spectrum from the 
     ;   fit parameters. 
     ;
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::EVALUATE, x, fit_array, yfit


       ; Copy the fit array to the model objects
       self->EXTRACT_PARAMETER_ARRAY, fit_array


       ; Evaluate any constraints.
       (self.core).models->EVALUATE_CONSTRAINTS


       ; Evaluate the models.
       yfit = (self.core).models->EVALUATE(x)


     END ;PRO BST_INST_FIT_CURVEFIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup when destroying the BST_INST_CURVEFIT object.
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT::CLEANUP

       self.input.parinfo->DESTROY
       self.results.yfit->DESTROY
       self.results.parameters->DESTROY
       self.results.sigma->DESTROY

     END ;PRO BST_INST_CURVEFIT::CLEANUP


     ;+=================================================================
     ;
     ; Create the BST_INST_PRFOFILES object.
     ;
     ;-=================================================================
     PRO BST_INST_CURVEFIT__DEFINE

       bst_inst_curvefit = $
          { BST_INST_CURVEFIT $

            ,input:{ BST_INST_CURVEFIT__INPUT $
                     ,parinfo:OBJ_NEW() $
                   } $

            ,results:{ BST_INST_CURVEFIT__RESULTS $
                       ,parameters:OBJ_NEW() $
                       ,yfit:OBJ_NEW() $
                       ,sigma:OBJ_NEW() $
                       ,chisq:0D $
                       ,reduced_chisq:0D $
                       ,status:0D $
                       ,time:0D $
                     } $

            ,INHERITS BST_INST_BASE $
          }

     END ;PRO BST_INST_CURVEFIT__DEFINE
