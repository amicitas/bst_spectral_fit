




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   An script for <BST_SPECTRAL_FIT>
;
;   This is a script for <BST_SPECTRAL_FIT> that will fit multiple
;   columns of a white light image.  The goal is to come up with a
;   fit of the image that has the illumination of the individual fibers
;   as parameters.
;
;   There are 35 visible parameters, and 38 total fibers.
;
; DESCRIPTION:
;   The goal of making the reconstruction is to be able to use
;   it in creating a whitelight fringing correction that correctly
;   takes into account the illumintation of the individual fibers
;   in the linear fiber array.
;  
;   To do this we first fit a corrected whitelight image using
;   a model that includes the fiber intensities as a fit parameter.
;
;   Once this fit is complete then an image can be reconstructed
;   if the fiber illuminations are known.  See the B-Stark thesis
;   for more details on the procedure.
;
;   WARNING:  In general the fitting script here does a fairly
;             poor job determining the constant background term.
;
;             For the whitelight fringing problem this is ok
;             because I normalize each row before using
;             the reconstruction.
;
;             If these images will be used for another purpose
;             care must be taken to make sure the background fits
;             are acceptable.
;
;
; HOW TO USE THESE ROUTINES:
;   1. Start <BST_SPECTRAL_FIT>.
;
;   2. Start the following addons:
;      - Image Spectra (BST_INST_DETECTOR_VIEWER)
;      - Fiber Illumination (BST_INST_FIBER)
;      - Fit Saver (BST_INST_FIT_SAVER)
;
;   3. Open the image to be fit in DETECTOR_VIEWER.
;      In general this should be a background subtracted
;      whitelight image with fringing correction.
;
;   4. Set the Image Spectra addon to "Col Average".
;
;      Set the fit range to appropriate range for the
;      chord to be fit (b02=0:511, b01=512:1023).
;
;   5. Average a large number of columns and make a fit.
;      These columns should be be near Col=0.
;      My choise was cols = 100-300.
;      
;      The intensity of the the first fiber and the shift
;      of the fiber array should be fixed.
;
;      Use three gaussians for fiber responce and set
;      the profile to -1.
;
;      Use a constant background.
;
;      Save the fit for future reference.
;
;   6. Adjust the limits and starting values specified in
;      [BST_INST_FIT_FIBER_ILLUMINATION].
;
;      Also adjust the number of columns to average and
;      the step size.
;
;      Right now the constant background term is reset
;      for every fit.  This might not be the best way
;      to do things.
;
;      Set the output filename.
;
;   7. Fix all of the fiber illumination parameters.
;   
;      Add a background gaussian. And set it to
;      the desired starting value.
;
;      Make sure all parameters have the save button enabled.
;
;   8. Run [BST_INST_FIT_FIBER_ILLUMINATION].
;      This will fit each column and create a fit list.
;      The fit list will be saved to a file.
;
;   9. To create a reconstruction use
;      [BST_INST_FIT_FIBER_RECONSTRUCT_IMAGE] or
;      [BST_INST_FIBER_GET_RECONSTRUCTION].
;
;  10. The reconstruction routines allow the input of a 
;      fiber param structure.  This structure contains
;      the illuminations of the fibers as well as the
;      shift of the arrray.
;
;      To generate this structure:
;        a. Load the image from which to get the intensities
;        b. Choose an aprropriate column averaging range.
;        c. Load the whitelight image used for the
;           image fit and redo Item 5 with the new range.
;        b. Set all of the gaussians to fixed.
;        c. Fix spacing leave intensity and shift free
;           Free the first fiber intensity .
;        d. Reload the image from Item a.
;        e. Do a fit.
;        f. Extract the fiber param structure using
;           [BST_INST_FIBER_GET_PARAM] or save it directly using
;           [BST_INST_FIBER_SAVE_PARAMS]
;
;      The structure can now be used with the routines
;      mentioned in Item 9.
;
;
;
; SAVED FIT RESULTS:
;   fiber_illumination_fit_results_2009-06-03.sav
;       Image: 
;       Column Average: 10
;       Step size: 2
;       Num Gauss: 3
;       All intensities limited. Range:  0.5 - 1.0.
;       
;       For this run I limited all of the intensities, but there was
;       no other constraints on the gaussian intensity.  This leads
;       to the fitter having problems dealing with the off image
;       fibers.  The results should be ok though.
; 
;
;   fiber_illumination_fit_results_2009-06-05.sav
;       Column Average: 10
;       Image: 
;       Step size: 1
;       Num Gauss: 3
;       Fiber 1 intensity set to 1.0
;       All intensities limited. Range: 0.0-2.0
;
;       For this run I set the intesity of the first fiber to 1.0.
;       This provides a constraint for the gaussian intensity. The
;       Fitter now deals much better with the off image fibers.
;
;
;   fiber_illumination_fit_results_2009-06-07.sav
;       Image: 
;       Column Average: 10
;       Step size: 1
;       Num Gauss: 3
;       Num Gauss Back: 1
;       Fiber 1 intensities set to fit from col ave 0-200
;
;       For this run I set the intensity of all the fibers to be equal
;       to the intensities from a fit of the averaged columns between
;       0-200.
;  
;       To deal with the background, I have added a gaussian, whose
;       value will be reset for every fit, with the following
;       initial parameters:
;         amp: 0, wid: 150, loc: 600
;       The location of this gaussian is limited to the range 500-1023.
;       The width of this gaussian is limited to the range 50-500
;
;       For this run the background is initially set to 70.5, and is
;       reset between fits. The background is limited to the range
;       65-250.
;
;
;  fiber_illumination_fit_results__b01__2009_08_04.sav
;       Path:  /u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/image_fits
;       Image: /u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/
;                IMAGE_13096A_T5000_WHITE_PORT_CARD.TIF.
;
;              This is a fringing corrected image with background 
;              subtraction.
;
;       Fitting chord 'B01', rows 512 - 1023
;       Column Average: 11
;       Step size: 1
;       Num Gauss: 3
;       Num Gauss Back: 1
;
;       Fiber intensities taken from a fit with col ave = 100-300.
;       Fiber intensities and spacing held fixed for image fit
;
;       Limits and starting values:
;         limits_gauss_location = [400D,550D]
;         start_gauss_location = 500D
;          
;         limits_gauss_width = [50D,500D]
;         start_gauss_width = 150D
;
;         limits_background = [60D,100D]
;         start_background = 60D
;
;       To deal with the background I am using a constant background
;       plus one gaussian.  These values are limited and reset to
;       initial values for every fit.
;
;
;  fiber_illumination_fit_results__b02__2009_08_28.sav
;       Path:  /u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/image_fits
;       Image: /u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/
;                IMAGE_13096A_T5000_WHITE_PORT_CARD.TIF.
;
;              This is a fringing corrected image with background 
;              subtraction.
;
;       Fitting chord 'B02', rows 0 - 511
;       Column Average: 11
;       Step size: 1
;       Num Gauss: 3
;       Num Gauss Back: 1
;
;       Fiber intensities taken from a fit with col ave = 100-300.
;       Fiber intensities and spacing held fixed for image fit
;
;       Limits and starting values:
;         limits_gauss_location = [500D,600D]
;         start_gauss_location = 550D
;          
;         limits_gauss_width = [50D,500D]
;         start_gauss_width = 150D
;
;         limits_background = [60D,100D]
;         start_background = 59D
;
;       To deal with the background I am using a constant background
;       plus one gaussian.  These values are limited and reset to
;       initial values for every fit.
;
;       Note: The constant background did not fit well here.
;
;             When using these reconstructions I always normalize the
;             responce for a given column.  This should allow the
;             fit to be used even with the bad constant fit.
;
;             It might be possible to improve this by playing with
;             the limits or starting values for the background terms.
;
;             For the constant term it  might help to start with the
;             results from the previous fit.
;
;       
; 
;-======================================================================


     ;+=================================================================
     ;
     ; Get the current default fit_list. 
     ;
     ; This is the list of fit results from a column by column fit
     ; of a white light correction image.
     ;
     ; For now the fit list is from a fit of a corrected whitelight
     ; image taken on 2009-04-15.
     ;
     ; NOTE: While this is a fine choise for the image, it the 
     ;       correction might not be quite as good as if an image
     ;       was used from the scan on 2009-04-28.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_FIBER_GET_FIT_LIST, CHORD=chord_in
       ; This is necessary to make sure that the list object
       ; is defined with the current definition.
       RESOLVE_ROUTINE, [ $
                          'list__define' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE
       

       chord = BST_CHORD_NAME_PARSE(chord_in, /CHAR3)
       CASE chord OF
         'b01': BEGIN
           path = $
              '/u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/image_fits'
           filename = 'fiber_illumination_fit_results__b01__2009_08_04.sav'
         END
         'b02': BEGIN
           path = $
              '/u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/image_fits'
           filename = 'fiber_illumination_fit_results__b02__2009_08_28.sav'
         END
       ENDCASE


       filepath = MIR_PATH_JOIN([path, filename])

       PRINT, 'Loading fit_list from: ' + filepath
       RESTORE, filepath, /RELAXED_STRUCTURE_ASSIGNMENT

       RETURN, fit_list

     END ;FUNCTION BST_INST_FIT_FIBER_GET_FIT_LIST



     ;+=================================================================
     ;
     ;
     ; This script will fit a whitelight image taken on the
     ; the B-Stark system.
     ;
     ; Before this sript can be run:
     ; - BST_INSTRUMENTAL_FIT_GUI must be loaded.
     ; - BST_INST_DETECTOR_VIEWER, BST_INST_FIT_SAVER and
     ;   BST_INST_FIT_FIBER addons must be enabled.
     ; - A fit must be loaded that is valid for the left hand
     ;   side of the image (col ~ 0).
     ;
     ; See [File:BST_INST_FIT_FIBER_ILLUMINATION] for more details.
     ;
     ;
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_FIBER_ILLUMINATION
       COMMON BST_INSTRUMENTAL_FIT

       chord_in = 'b02'
       chord = BST_CHORD_NAME_PARSE(chord_in, /CHAR3)

       ; ---------------------------------------------------------------
       ; Setup where to save the results.
       path = '/u/antoniuk/b-stark/calibration/2009/images/2009-04-15/corrected/image_fits'
       ;filename = 'fiber_illumination_fit_results__b02__2009_08_28.sav'

       full_path = MIR_PATH_JOIN([path, filename])
       IF FILE_TEST(full_path) THEN BEGIN
         dialog_status = DIALOG_MESSAGE("File exists, overwrite?", /QUESTION, /DEFAULT_NO)
         IF dialog_status EQ 'No' THEN RETURN, 0
       ENDIF
       


       ; ---------------------------------------------------------------
       ; Setup limits and starting values for some of the parameters.
       ;
       ; Starting parameters are those that will be reset to the given
       ; values for every column fit.
       CASE chord OF
         'b01': BEGIN
           limits_gauss_location = [400D,550D]
           start_gauss_location = 500D
           start_background = 60D
         END
         'b02': BEGIN
           limits_gauss_location = [500D,600D]
           start_gauss_location = 550D
           start_background = 59D
         END
       ENDCASE

          
       limits_gauss_width = [50D,500D]
       start_gauss_width = 150D

       limits_background = [start_background,100D]

 

       ; ---------------------------------------------------------------
       ; Setup some of the fitting parameters.


       ; Setup the number of columns to average.
       col_ave = 11

       ; Setup the number of columns to step between each fit.
       col_step = 1


       starting_channel = 0
       ending_channel = 1023




       ; ---------------------------------------------------------------
       ; Pick up the object referances to the reqired addons
       fit_saver = BST_INST_ADDONS_GET_ADDON('BST_INST_FIT_SAVER')
       detector_viewer = BST_INST_ADDONS_GET_ADDON('BST_INST_DETECTOR_VIEWER')

       ; Clear all of the fits in the fit saver.
       fit_saver->CLEAR_ALL


       ; Set the limits on the background and backgound gaussian
       c_inst_model.bst_inst_gauss[3]->COPY_FROM, {limited:{ $
                                                            location:[1,1] $
                                                            ,width:[1,1] $
                                                           } $
                                                   ,limits:{ $
                                                             location:limits_gauss_location $
                                                             ,width:limits_gauss_width $
                                                           } $
                                                  }
       c_inst_model.bst_inst_back->COPY_FROM, {limited:{back:[[1,1],[0,0],[0,0]]} $
                                               ,limits:{back:[limits_background, [0D,0D], [0D,0D]]} $
                                              }

       ; Now start the fitting loop
       FOR col=starting_channel, ending_channel-col_ave+1, 1 DO BEGIN

         ; Set the column to fit
         detector_viewer->SET_RANGE, [col, col+col_ave-1], /COL

         ; Reset the background and backgound gaussian.
         c_inst_model.bst_inst_gauss[3]->COPY_FROM, {param:{amplitude:0D $
                                                           ,width:start_gauss_width $
                                                           ,location:start_gauss_location $
                                                           }}
         c_inst_model.bst_inst_back->COPY_FROM, {param:{back:[start_background, 0D, 0D]}}

         ; Do the fit
         BST_INST_GUI_DO_FIT

         ; Save the results
         fit_saver->SAVEFIT

         ; Print some stuff
         BST_INST_FIT_FIBER_PRINT

       ENDFOR

       fit_list = fit_saver->GET_FIT_LIST()

       
       SAVE, fit_list, FILENAME=full_path
       BST_INST_MESSAGE, 'Saved fit results to: '+full_path, /LOG

       RETURN, fit_list

     END ; FUNCTION BST_INST_FIT_FIBER




     ;+=================================================================
     ;
     ; Print some stuff after every fit.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_FIBER_PRINT
       COMMON BST_INSTRUMENTAL_FIT

       back = c_inst_model.bst_inst_back->GET_STRUCT()
       back_gauss = c_inst_model.bst_inst_gauss[3]->GET_STRUCT()
       detector_viewer = BST_INST_ADDONS_GET_ADDON('BST_INST_DETECTOR_VIEWER')

       BST_INST_MESSAGE, '', /LOG
       BST_INST_MESSAGE, '------------------------------------------------------------------------', /LOG
       BST_INST_MESSAGE, STRING('Column Range:', (detector_viewer->GET_PARAM()).avgrange_col $
                                ,FORMAT='(a15, 2x, 2(i4,2x))'), /LOG
       BST_INST_MESSAGE, STRING('Background:', back.param.back[0]$
                               ,FORMAT='(a15 ,2x, f7.2)'), /LOG
       BST_INST_MESSAGE, STRING('Back Gauss:' $
                                ,'Amplitude:', back_gauss.param.amplitude $
                                ,'Width:', back_gauss.param.width $
                                ,'Location:', back_gauss.param.location $
                                ,FORMAT='(a15 ,2x, 3(a10, 2x, f12.3))'), /LOG
       BST_INST_MESSAGE, '------------------------------------------------------------------------', /LOG
       BST_INST_MESSAGE, '', /LOG


     END ;PRO BST_INST_FIT_FIBER_PRINT




     ;+=================================================================
     ;
     ; Returen the intensities of the fibers, averaged over the given
     ; range. 
     ;
     ; If desired the intensities for each fit will be normalized
     ; my the mean intensity.
     ;
     ; INPUTS:
     ;   fit_list
     ;       A fit_list containing an image fit.  This  fit list is
     ;       generated by [BST_INST_FIT_FIBER_ILLUMINATION].
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_FIBER_GET_INTENSITY, fit_list $
                                                ,RANGE=range $
                                                ,NORMALIZE=normalize $
                                                ,PRINT=print

       
       fit_array = fit_list->TO_ARRAY()
       num_fits = fit_list->N_ELEMENTS()

       fiber_param = fit_array.model_state.bst_inst_fiber_illumination.param
       num_fibers = fit_array[0].model_state.bst_inst_fiber_illumination.num_fibers
       num_fibers_vis = 34

       dv_param = fit_array.addon_state.bst_inst_detector_viewer
       
       col_central = FLTARR(num_fits)
       FOR ii=0,num_fits-1 DO BEGIN
         col_central[ii] = TOTAL(dv_param[ii].avgrange_col)/2E
       ENDFOR

       ; Set the default range to the whole range.
       MIR_DEFAULT, range, [col_central[0], col_central[num_fits-1]]

       ; Find the mean intensity at each column.
       mean_intens = DBLARR(num_fits)
       FOR ii=0,num_fits-1 DO BEGIN
         mean_intens[ii] = MEAN(fiber_param[ii].intensity[0:34])
       ENDFOR

       intensity_out = DBLARR(num_fibers)

       ; Set the range.
       ; The range should be set in columns.
       ; This is in general not equal to the index
       ave_range = [0,0]
       w_lt = WHERE(col_central LT range[0], w_lt_count)
       w_gt = WHERE(col_central GT range[1], w_gt_count)
       
       IF w_lt_count EQ 0 THEN BEGIN
         ave_range[0] = 0
       ENDIF ELSE BEGIN
         ave_range[0] = w_lt[w_lt_count-1]+1
       ENDELSE

       IF w_gt_count EQ 0 THEN BEGIN
         ave_range[1] = num_fits-1
       ENDIF ELSE BEGIN
         ave_range[1] = w_gt[0]-1
       ENDELSE

       ; Now do the normalization and averaging
       FOR ii=0,num_fibers-1 DO BEGIN
         intensity = fiber_param.intensity[ii]

         ; If desired, normalize the intensities to the mean intensity of each column.
         IF KEYWORD_SET(normalize) THEN BEGIN
           intensity = intensity/mean_intens
         ENDIF

         intensity_out[ii] = MEAN(intensity[ave_range[0]:ave_range[1]])

         IF KEYWORD_SET(print) THEN BEGIN

           IF normalize THEN BEGIN
             norm_string = 'Normalized '
           ENDIF ELSE BEGIN
             norm_string = ''
           ENDELSE
           PRINT, ii, ' '+norm_string+'Intensity: ', intensity_out[ii]
         ENDIF
       ENDFOR

       RETURN, intensity_out

     END ;FUNCTION BST_INST_FIT_FIBER_GET_INTENSITY




     ;+=================================================================
     ;
     ; Plot some relevant parameters from the fit_list returned
     ; by [BST_INST_FIT_FIBER_ILLUMINATION()].
     ;
     ; This procedure is used to examine the fits in the fitlist.
     ; Mostly this is to be used in developing the fitting procedure.
     ;
     ;-=================================================================
     PRO BST_INST_FIT_FIBER_PLOT, fit_list, INTENSITY=intensity_out

       fit_array = fit_list->TO_ARRAY()
       num_fits = fit_list->N_ELEMENTS()

       fiber_param = fit_array.model_state.bst_inst_fiber_illumination.param
       num_fibers = fit_array[0].model_state.bst_inst_fiber_illumination.num_fibers
       num_fibers_vis = 34

       dv_param = fit_array.addon_state.bst_inst_detector_viewer
       
       col_central = FLTARR(num_fits)
       FOR ii=0,num_fits-1 DO BEGIN
         col_central[ii] = TOTAL(dv_param[ii].avgrange_col)/2E
       ENDFOR

       ; Find the mean intensity at each column.
       mean_intens = DBLARR(num_fits)
       FOR ii=0,num_fits-1 DO BEGIN
         mean_intens[ii] = MEAN(fiber_param[ii].intensity[0:34])
       ENDFOR


       ; Plot the mean fiber intensity
       IF 0 THEN BEGIN

         SUPERPLOT, col_central, mean_intens $
           ,YRANGE=[0.6,1.2] $
           ,THICK=2

       ENDIF


       ; Plot all of the fiber intensities.
       IF 0 THEN BEGIN
         normalize = 1
         oplot=1

         FOR ii=0,num_fibers_vis-1 DO BEGIN
           intensity = fiber_param.intensity[ii]

           ; If desired, normalized the intensities to the mean intensity of each column.
           IF normalize THEN BEGIN
             intensity = intensity/mean_intens
           ENDIF

           SUPERPLOT, col_central, intensity $
             ,YRANGE=[0.5,1.5] $
             ,COLOR=ii*255/(num_fibers-1) $
             ,OPLOT=oplot
           oplot = 1
         ENDFOR

       ENDIF



       IF 1 THEN BEGIN

         SUPERPLOT_CLEAR

         oplot = 0
         FOR ii=0,2 DO BEGIN
           gauss_param = fit_array.model_state.bst_inst_gauss[ii].param
           SUPERPLOT_ADD, {x:col_central, y:gauss_param.amplitude $
                           ,COLOR:(0+30*ii), YRANGE:[-400,1500], OPLOT:oplot $
                           ,TITLE:'amplitude'}
           OPLOT=1
         ENDFOR

         oplot = 0
         FOR ii=0,2 DO BEGIN
           gauss_param = fit_array.model_state.bst_inst_gauss[ii].param
           SUPERPLOT_ADD, {x:col_central, y:gauss_param.location $
                           ,COLOR:(0+30*ii), YRANGE:[420,480], OPLOT:oplot $
                           ,TITLE:'location'}
           OPLOT=1
         ENDFOR

         oplot = 0
         FOR ii=0,2 DO BEGIN
           gauss_param = fit_array.model_state.bst_inst_gauss[ii].param
           SUPERPLOT_ADD, {x:col_central, y:gauss_param.width $
                           ,COLOR:(0+30*ii), YRANGE:[0,10], OPLOT:oplot $
                           ,TITLE:'width'}
           OPLOT=1
         ENDFOR

         SUPERPLOT_MULTI
         SUPERPLOT_CLEAR
       ENDIF   


     END ; PRO BST_INST_FIT_FIBER_PLOT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Attempt to reconstruct the image from the fits saved in the
     ;   fit list.
     ;
     ; KEYWORDS:
     ;   FIBER_PARAM = struct
     ;       If given the parameters in this structure will be used in
     ;       the image reconstruction instead of values from the 
     ;       fit_list.
     ;
     ;       By default the folowing parameters will be used:
     ;         fiber intensities
     ;         array shift
     ;
     ;       The following parameters will be ignored:
     ;         fiber spacing
     ;
     ;   /NO_SHIFT
     ;       Ignore the the array shift parameter.
     ;       This means that only the fiber intensites will be used.
     ;
     ;
     ;   /FIBER_ONLY
     ;       If set the image will be reconstructed without using
     ;       background terms.
     ;
     ;       NOTE:  If the background is not carfully constrained
     ;              in the fits, using this will cause the average
     ;              intensity to vary for differt columns.
     ;
     ; INTERNAL OPTIONS:
     ;   copy_edge_columns
     ;     For columns outside of the range of columns that were fit.
     ;     copy the edge most column instead for using interpolation.
     ;     
     ;     Since the fit may not have been done for every column,
     ;     to reconstruct the image we generally interpolate to 
     ;     find the correct value at a given column.
     ;
     ;   remove_fits_with_edge_columns
     ;     Remove any fits that contained an edge column.
     ;
     ;     For the Sarnoff cameras the edge pixels are bad.
     ;     I would like an option to ignore these pixels.
     ;
     ;   
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIT_FIBER_RECONSTRUCT_IMAGE, fit_list $
                                                    ,FIBER_ONLY=fiber_only $
                                                    ,FIBER_PARAMETERS=fiber_param $
                                                    ,NO_SHIFT=no_shift
       COMMON BST_INSTRUMENTAL_FIT

       ; Options (See documentation in header)
       copy_edge_columns = 1
       remove_fits_with_edge_columns = 1


       ; Create a new empty image
       image = DBLARR(1024, 1024)
       row_index = FINDGEN(1024)
       col_index = FINDGEN(1024)

       ; Extract the fit data from the fit_array.
       fit_array = fit_list->TO_ARRAY()
       num_fits = fit_list->N_ELEMENTS()

       ; Create an array to hold the reconstruction
       fit_slices = DBLARR(num_fits, 1024)
       col_central = DBLARR(num_fits)

       contains_edge = DBLARR(num_fits)

       ; Loop over all of the fits.
       FOR ii=0,num_fits-1 DO BEGIN
         model_state = fit_array[ii].model_state
         dv_state = fit_array[ii].addon_state.bst_inst_detector_viewer
         col_range = dv_state.avgrange_col

         ; Load the model
         BST_INST_MODEL_LOAD_STATE, model_state


         ; If fiber parameters were given, then load them.
         IF N_ELEMENTS(fiber_param) NE 0 THEN BEGIN

           c_inst_model.bst_inst_fiber_illumination->COPY_FROM, $
              {param:{intensity:fiber_param.intensity}}

           IF ~ KEYWORD_SET(no_shift) THEN BEGIN
             c_inst_model.bst_inst_fiber_illumination->COPY_FROM, $
               {param:{shift:fiber_param.shift}}
           ENDIF

         ENDIF


         ; Evaluate the model
         IF KEYWORD_SET(fiber_only) THEN BEGIN
           ; Here I evaluate only the fiber model, not any of the background terms.
           y = c_inst_model.bst_inst_fiber_illumination->EVALUATE(row_index)
         ENDIF ELSE BEGIN
           ; Here I evaluate the whole model, including the background terms.
           y = BST_INST_MODEL_EVALUATE(row_index)
         ENDELSE

         ; Save the reconstruction
         fit_slices[ii,*] = y

         ; Extract the columns that were fit.
         col_central[ii] = TOTAL(dv_state.avgrange_col)/2E

         ; Check for edge pixels
         IF (col_range[0] EQ 0 OR col_range[1] EQ 1023) THEN BEGIN
           contains_edge[ii] = 1
         ENDIF

       ENDFOR
       
       ; Remove any fits than contained an edge column.
       IF remove_fits_with_edge_columns THEN BEGIN
         where_not_edge = WHERE(~ contains_edge, count_not_edge)
         IF count_not_edge GT 0 THEN BEGIN
           fit_slices = fit_slices[where_not_edge, *]
           col_central = col_central[where_not_edge]
           num_fits = N_ELEMENTS(col_central)
         ENDIF
       ENDIF


       ; Now I need to interpolate over the points that I have fit to get
       ; the rest of the image.
       ;
       ; For points outside of the fit use the last fit.
       IF KEYWORD_SET(copy_edge_columns) THEN BEGIN
         col_central_max = MAX(col_central, MIN=col_central_min)
         where_outside = WHERE( (col_index LT col_central_min) $
                                OR (col_index GT col_central_max) $
                                ,where_outside_count $
                                ,COMPLEMENT=where_inside $
                                ,NCOMPLEMENT=where_inside_count)

         ; Deal with the columns where we can interpolate.
         IF where_inside_count GT 0 THEN BEGIN
           FOR ii=0,1024-1 DO BEGIN
             image[where_inside,ii] = INTERPOL(fit_slices[*,ii] $
                                               ,col_central $
                                               ,col_index[where_inside], /QUADRATIC)
           ENDFOR
         ENDIF

         ; Now deal with columns at the edges.
         FOR ii=0,where_outside_count-1 DO BEGIN
           col = where_outside[ii]
           CASE 1 OF
             (col_index[col] LT col_central_min): BEGIN
               image[col,*] = fit_slices[0,*]
             END
             (col_index[col] GT col_central_max): BEGIN
               image[col,*] = fit_slices[num_fits-1,*]
             END
           ENDCASE
         ENDFOR

       ENDIF ELSE BEGIN
 
         ; Interpolate for all columns, even those on the edges.
         FOR ii=0,1024-1 DO BEGIN
           image[*,ii] = INTERPOL(fit_slices[*,ii], col_central, col_index, /QUADRATIC)
         ENDFOR

       ENDELSE
       
       RETURN, image


     END ; FUNCTION BST_INST_FIT_FIBER_RECONSTRUCT_IMAGE





     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return an image reconstruction.
     ;
     ;   This is a convinience routine to run
     ;   [BST_INST_FIT_FIBER_RECONSTRUCT_IMAGE]. It will handle getting
     ;   the fitlist, and generating the reconstruction.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIBER_GET_RECONSTRUCTION, CHORD=chord $
                                                 ,FIBER_ONLY=fiber_only $
                                                 ,FIBER_PARAMETERS=fiber_param $
                                                 ,NO_SHIFT=no_shift
       
       PRINT, 'Reconstructing image . . .'

       ; Get the fit_list
       fit_list = BST_INST_FIT_FIBER_GET_FIT_LIST(CHORD=chord)



       image = BST_INST_FIT_FIBER_RECONSTRUCT_IMAGE(fit_list $
                                                    ,FIBER_ONLY=fiber_only $
                                                    ,FIBER_PARAMETERS=fiber_param $
                                                    ,NO_SHIFT=no_shift)

       OBJ_DESTROY, fit_list

       RETURN, image
       
     END ;FUNCTION BST_INST_FIBER_GET_RECONSTRUCTION




; ======================================================================
; ======================================================================
; ######################################################################
;
; 
;
;   
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; Extract the intensities from the BST_INST_FIBER addon.
     ;
     ;-=================================================================
     FUNCTION BST_INST_FIBER_GET_PARAM
       COMMON BST_INSTRUMENTAL_FIT


       param = c_inst_model.bst_inst_fiber_illumination->GET_PARAM()


       RETURN, param

     END  ; FUNCTION BST_INST_FIBER_GET_PARAM



     ;+=================================================================
     ;
     ; Extract the intensities from the BST_INST_FIBER addon
     ; and save them to a file.
     ;
     ;-=================================================================
     PRO BST_INST_FIBER_SAVE_PARAMS, filepath
       COMMON BST_INSTRUMENTAL_FIT

       IF FILE_TEST(filepath) THEN BEGIN
         dialog_status = DIALOG_MESSAGE("File exists, overwrite?", /QUESTION, /DEFAULT_NO)
         IF dialog_status EQ 'No' THEN RETURN
       ENDIF

       fiber_param = BST_INST_FIBER_GET_PARAM()
       SAVE, fiber_param, FILENAME=filepath

     END ;PRO BST_INST_FIBER_SAVE_PARAMS
