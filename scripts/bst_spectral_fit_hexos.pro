
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   A general purpose spectral fitter.
;
; SYNTAX:
;   BST_SPECTRAL_FIT [, fitter][, CONFIGURATION=hash][, /DEBUG][, /HELP]
;
; DESCRIPTION:
;   Documentation for this program can be found in
;   [File:bst_inst_fit__define].
;
;   This was program was originaly built to be used to fit spectra for
;   the B-Stark and CER diagnostics at the DIII-D tokamak.  Recent
;   devolpment has been for analysis of the XICS system at LHD.
;
; PROGRAMMING NOTES:
;   The common block here is a convience that will allow the
;   use of the routines in [File:BST_INST_PUBLIC].
;
;   This common block is only useful if a single instance of
;   <BST_SPECTRAL_FIT> is started in a single IDL session.
;
;-======================================================================


     PRO BST_SPECTRAL_FIT_HEXOS, fitter $
                           ,CONFIGURATION=configuration $
                           ,XICS_AR16=xics_ar16 $
                           ,HELP=help $
                           ,_REF_EXTRA=_extra
       COMMON BST_SPECTRAL_FIT, c_fitter

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF

       RESOLVE_ROUTINE, [ $
                           'bst_inst_public' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE
       FORWARD_FUNCTION BST_INST_GET_GUIADDON 

       ; This is the standard configuration for xics.
       ; I am temporarily just making it the default.
       configuration = MIR_HASH()
       configuration['GENERAL'] = {models_default_active:0 $
                                   ,addons_default_active:0 $
                                   ,guiaddons_default_active:0}

       configuration['MODELS'] = { bst_inst_model_background:1 $
                                   ,bst_inst_model_voigt_multi:1 $
                                   ,bst_inst_model_gaussian_multi:1}

       configuration['ADDONS'] = { bst_inst_addon_fit_saver:1 $
                                   ,bst_inst_addon_plot:1 $
                                   ,bst_inst_addon_print:1 $
                                   ,bst_inst_addon_spectrum:1 $
                                   
                                   ;,bst_inst_addon_hexos_spectra:1 $
                                 }
       
       configuration['GUIADDONS'] = { bst_inst_guiaddon_background:1 $
                                      ,bst_inst_guiaddon_voigt:1 $
                                      ,bst_inst_guiaddon_voigt_limits:1 $
                                      ,bst_inst_guiaddon_gaussian_back:1 $
                                      ,bst_inst_guiaddon_gaussian_limits:1 $
                                      ,bst_inst_guiaddon_user_spectra:1 $
                                    }



       ; Simple enough.  
       ; The object will be destroyed when the GUI is destroyed.
       fitter = OBJ_NEW('BST_INST_FIT_GUI' $
                        ,CONFIGURATION=configuration $
                        ,_STRICT_EXTRA=_extra)

       c_fitter = fitter
       
       addon_user_spectra = BST_INST_GET_GUIADDON('USER_SPECTRA')
       addon_user_spectra.ENABLE
       addon_user_spectra.SET_PARAM, $
         {function_name:'MIR_W7X_HEXOS_SPECTRUM_CAM3' $
          ,use_function:1}



     END ;PRO BST_SPECTRAL_FIT_HEXOS
