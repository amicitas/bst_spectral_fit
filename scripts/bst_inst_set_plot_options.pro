

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-06
;
; PURPOSE:
;   Allow the plot options to be set.
;
; DESCRIPTION:
;   Eventually this stuff should be controlable through a plot options
;   gui.  For now this is a covenience routine to use as a work around.
;-======================================================================



     PRO BST_INST_SET_PLOT_OPTIONS, options $
                                    ,FITTER=fitter
       COMPILE_OPT STRICTARR

       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter

       plot_addon = BST_INST_GET_ADDON('BST_INST_ADDON_PLOT')

       plot_addon->SET_OPTIONS, options

     END ;PRO BST_INST_SET_PLOT_OPTIONS
