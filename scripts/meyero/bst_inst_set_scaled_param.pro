
; ============================================================
; This is a procedure that will set various parameters for
; the scaled model for <BST_SPECTRAL_FIT>.
;
; This script will need to be run before the fit is done.
;
; It will only need to be rerun if the location limits range
; needs to be reset to surround the current location value.
;
; In particular we will do the following:
;  1. Fix the scale to 1.0
;  2. Limit the location of the scaled peak
;
; 
; To acheve these things we will manipulate the parameters
; of the appropriate BST_INST_SCALED object.
;
; That object inherits the following classes:
;  BST_INST_MODEL
;  OBJECT
;
; All methods from those classes are available.
;
; ============================================================
PRO BST_INST_SET_SCALED_PARAM

  ; First setup the common block.  This will give access to the
  ; common variables used by <BST_SPECTRAL_FIT>.
  ; In particular this will allow us to access the models.
  COMMON BST_INSTRUMENTAL_FIT

  ; Here I am making a copy of the referance to  the first scaled object.
  ; This is done for clarity.
  ;
  ; You will probably want to add a loop at this point over all of the
  ; scaled objects
  scaled = c_inst_model.bst_inst_scaled[0]

  ; This will make a copy of the data inside the scaled object. This
  ; will allow us to extract any parameters that we wish.
  ;
  ; Alternivly we could use scaled->GET_PROPERTY(name) to extract a
  ; single property from the object.  This is a slow method however.
  old_struct = scaled->GET_STRUCT()

  ; Here I am creating a structure, with the properites of the scaled
  ; model that I want to change.
  param = {scale:1D}
  fixed = {scale:1}
  limited = {location:[1,1]}
  limit_range = 10
  limits = {location:[old_struct.param.location-limit_range, old_struct.param.location+limit_range]}

  new_struct = { $
                 param:param $
                 ,fixed:fixed $
                 ,limited:limited $
                 ,limits:limits $
               }

  ; Here I copy the values from the structure that I just created into
  ; the model object.
  scaled->COPY_FROM, new_struct


END ; PRO BST_INST_SET_SCALED_PARAM
