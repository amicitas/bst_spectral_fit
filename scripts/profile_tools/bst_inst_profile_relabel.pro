


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;   Will allow a profile number to be relabeled.
;-======================================================================



     PRO BST_INST_PROFILE_RELABEL, old_labels_in $
                                   ,new_labels_in $
                                   ,INVERT=invert $
                                   ,POSITIVE=positive $
                                   ,FITTER=fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter


       fitter_core = fitter->GET_CORE()
       model_dispatch = (fitter_core->GET_STRUCT()).models
       profile_dispatch = (fitter_core->GET_STRUCT()).profiles

       profile_list = profile_dispatch->GET_PROFILE_NUMBER_LIST(POSITIVE=positive)

       IF ISA(old_labels_in) THEN BEGIN
         old_labels = old_labels_in
       ENDIF ELSE BEGIN
         IF profile_list->N_ELEMENTS() GT 0 THEN BEGIN
           old_labels = profile_list->TO_ARRAY()
         ENDIF ELSE BEGIN
           MESSAGE, 'old_labels not given.'
         ENDELSE
       ENDELSE


       num_labels = N_ELEMENTS(old_labels)

       IF KEYWORD_SET(invert) THEN BEGIN
         ; Use the original labels, but multiply by -1.
         new_labels = -1 * old_labels
       ENDIF ELSE BEGIN
         ; Use the input new labels.
         IF ISA(new_labels_in) THEN BEGIN
           new_labels = new_labels_in
           ; If only one new label was given, then relabel all of the given old
           ; labels to the new label
           IF N_ELEMENTS(new_labels) EQ 1 THEN BEGIN
             new_labels = REPLICATE(new_labels, num_labels)
           ENDIF
         ENDIF ELSE BEGIN
           MESSAGE, 'new_labels not given.'
         ENDELSE
       ENDELSE
         




       model_gauss_multi = model_dispatch->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')
       num_gauss = model_gauss_multi->NUM_MODELS()
       FOR ii=0,num_gauss-1 DO BEGIN
         model = model_gauss_multi->GET_MODEL(ii)

         FOR ii_profile=0,num_labels-1 DO BEGIN
           IF (model->HAS_PROFILE(old_labels[ii_profile])) THEN BEGIN

             model->SET_PROFILE_NUMBER, new_labels[ii_profile]

           ENDIF
         ENDFOR

       ENDFOR


       model_profile_multi = model_dispatch->GET_MODEL('BST_INST_MODEL_PROFILE_MULTI')
       num_profile = model_profile_multi->NUM_MODELS()
       FOR ii=0,num_profile-1 DO BEGIN
         model = model_profile_multi->GET_MODEL(ii)

         FOR ii_profile=0,num_labels-1 DO BEGIN
           IF (model->HAS_PROFILE(old_labels[ii_profile])) THEN BEGIN

             model->SET_PROFILE_NUMBER, new_labels[ii_profile]

           ENDIF
         ENDFOR

       ENDFOR


       model_scaled_multi = model_dispatch->GET_MODEL('BST_INST_MODEL_SCALED_MULTI')
       num_scaled = model_scaled_multi->NUM_MODELS()
       FOR ii=0,num_scaled-1 DO BEGIN
         model = model_scaled_multi->GET_MODEL(ii)

         FOR ii_profile=0,num_labels-1 DO BEGIN
           IF (model->HAS_PROFILE(old_labels[ii_profile])) THEN BEGIN
             model->SET_PROFILE_NUMBER, new_labels[ii_profile]
           ENDIF

           IF (model->GET_SCALED_PROFILE_NUMBER() EQ old_labels[ii_profile]) THEN BEGIN
             model->SET_SCALED_PROFILE_NUMBER, new_labels[ii_profile]
           ENDIF
         ENDFOR

       ENDFOR


       fitter->UPDATE

     END ; PRO BST_INST_PROFILE_RELABEL
