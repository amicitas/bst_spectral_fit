


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;   Will shift all of the peak with a given profile number by the
;   given amount.
;
;   This will eventually be implemented as an addon.
;-======================================================================



     PRO BST_INST_PROFILE_SHIFT, shift, profile, FITTER=fitter
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter


       fitter_core = fitter->GET_CORE()
       all_models = (fitter_core->GET_STRUCT()).models
       
       num_profiles = N_ELEMENTS(profile)

       model_gauss_multi = all_models->GET_MODEL('BST_INST_MODEL_GAUSSIAN_MULTI')
       num_gauss = model_gauss_multi->NUM_MODELS()
       FOR ii=0,num_gauss-1 DO BEGIN
         model = model_gauss_multi->GET_MODEL(ii)

         FOR ii_profile=0,num_profiles-1 DO BEGIN
           IF (model->HAS_PROFILE(profile[ii_profile])) THEN BEGIN
             param = model->GET_PARAM()
             param.location = param.location + shift
             model->SET_PARAM, param
           ENDIF
         ENDFOR

       ENDFOR


       model_scaled_multi = all_models->GET_MODEL('BST_INST_MODEL_SCALED_MULTI')
       num_scaled = model_scaled_multi->NUM_MODELS()
       FOR ii=0,num_scaled-1 DO BEGIN
         model = model_scaled_multi->GET_MODEL(ii)

         FOR ii_profile=0,num_profiles-1 DO BEGIN
           IF (model->HAS_PROFILE(profile[ii_profile])) THEN BEGIN
             param = model->GET_PARAM()
             param.location = param.location + shift
             model->SET_PARAM, param
           ENDIF
         ENDFOR

       ENDFOR


     END ; PRO BST_INST_PROFILE_SHIFT
