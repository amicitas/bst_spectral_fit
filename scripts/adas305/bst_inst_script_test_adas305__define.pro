


;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2010-03
;
; PURPOSE:
;   Here we have a set of scripts to use in testing the ADAS305
;   spectral fitting model.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the ADAS305 testing object.
     ;
     ;-=================================================================
     FUNCTION BST_INST_SCRIPT_TEST_ADAS305::INIT, DEBUG=debug

       ; Create an instance of <BST_SPECTRAL_FIT>
       self.gui = OBJ_NEW('BST_INST_FIT_GUI', DEBUG=debug)
       self.core = self.gui->GET_CORE()

       self.results = OBJ_NEW('MIR_DICTIONARY')

       RETURN, 1

     END ; FUNCTION BST_INST_SCRIPT_TEST_ADAS305::INIT



     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::TEST

       filepath = '/u/antoniuk/b-stark/analysis/bst_instrumental/136735/136735_b01_330l__adas305_test.bst_inst'

       ; Load the save state.
       self.gui->LOAD, filepath
       
       ; Reset the results dictionary.
       self.results->REMOVE, /ALL, /HEAP_FREE

       ; Run the actual tests.
       self->RUN_TEST

     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::TEST



     ;+=================================================================
     ; PURPOSE:
     ;   This is the main method to call to do the testing procedure.
     ;   It will handle setting up the fitter and cleaning up afterward.
     ;   
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::RUN_TEST


       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         self.gui->MESSAGE, 'Fit failed.', /ERROR
         self.core->POSTERROR
         self.gui->POSTERROR
         RETURN
       ENDIF
       

       self.gui->BEFORE_FIT
       self.core->BEFORE_FIT

       self.core->PREFIT
       self.gui->PREFIT


       self->TEST_BPBT
       self->TEST_B
       self->TEST_BEAM_ENERGY

       self.core->POSTFIT
       self.gui->POSTFIT


       self.core->AFTER_FIT
       self.gui->AFTER_FIT


       self.core->MESSAGE, 'Testing complete.', /STATUS
       
     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::RUN_TEST



     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::TEST_BPBT


       ; By the time we get here everything should be setup so that
       ; we can do the testing that we want to do.

       ; Retrieve the spectrum
       spectrum = (self.core).spectrum->GET_SPECTRUM()
       
       ; Retrive the adas305 model.
       model_adas305 = (self.core).models->GET_MODEL('BST_INST_MODEL_ADAS305')

       model_adas305->SET_PARAM, {pi_sigma_ratio:1.0D}

       ; Define the parameters over which I wish to vary Bp/Bt.
       bpbt_start = 0.650006D
       bpbt_step = 1e-6
       bpbt_num_step = 1000L
       bpbt = DINDGEN(bpbt_num_step) * bpbt_step + bpbt_start
       
       test_results = OBJ_NEW('MIR_LIST_IDL7')

       FOR ii=0, bpbt_num_step-1 DO BEGIN
         
         ; Set the new value for bpbt (currently things are mislabeled in the model).
         model_adas305->SET_PARAM, {bfield_z:bpbt[ii]}

         ; Evaluate the model
         yfit = (self.core).models->EVALUATE(spectrum.x)

         ; Calculate chisq
         chisq = TOTAL((spectrum.y - yfit)^2)

         ; Calculate the area
         area = TOTAL(yfit)

         ; Get the lines from ADAS305.
         lines = model_adas305->GET_STARK_LINES()

         ; Save the results
         test_results->APPEND, { $
                                 value:bpbt[ii] $
                                 ,chisq:chisq $
                                 ,area:area $
                                 ,lines:lines $
                               }
       ENDFOR

       self.results->SET, 'bpbt', test_results
       
     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::TEST_BPBT


     ;+=================================================================
     ; PURPOSE:
     ;   Do a scan of |B| and save the results.
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::TEST_B


       ; By the time we get here everything should be setup so that
       ; we can do the testing that we want to do.

       ; Retrieve the spectrum
       spectrum = (self.core).spectrum->GET_SPECTRUM()
       
       ; Retrive the adas305 model.
       model_adas305 = (self.core).models->GET_MODEL('BST_INST_MODEL_ADAS305')

       model_adas305->SET_PARAM, {pi_sigma_ratio:1.0D}

       ; Define the parameters over which I wish to vary Bp/Bt.
       b_start = 2.0
       b_step = 1e-6
       b_num_step = 1000L
       b = DINDGEN(b_num_step) * b_step + b_start
       
       test_results = OBJ_NEW('MIR_LIST_IDL7')

       FOR ii=0, b_num_step-1 DO BEGIN
         
         ; Set the new value for b (currently things are mislabeled in the model).
         model_adas305->SET_PARAM, {bfield_toroidal:b[ii]}

         ; Evaluate the model
         yfit = (self.core).models->EVALUATE(spectrum.x)

         ; Calculate chisq
         chisq = TOTAL((spectrum.y - yfit)^2)

         ; Calculate the area
         area = TOTAL(yfit)

         ; Get the lines from ADAS305.
         lines = model_adas305->GET_STARK_LINES()

         ; Save the results
         test_results->APPEND, { $
                                 value:b[ii] $
                                 ,chisq:chisq $
                                 ,area:area $
                                 ,lines:lines $
                               }
       ENDFOR

       self.results->SET, 'b', test_results
       
     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::TEST_B



     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::TEST_BEAM_ENERGY


       ; By the time we get here everything should be setup so that
       ; we can do the testing that we want to do.

       ; Retrieve the spectrum
       spectrum = (self.core).spectrum->GET_SPECTRUM()
       
       ; Retrive the adas305 model.
       model_adas305 = (self.core).models->GET_MODEL('BST_INST_MODEL_ADAS305')

       model_adas305->SET_PARAM, {pi_sigma_ratio:1.0D}

       ; Define the parameters over which I wish to vary Bp/Bt.
       beam_energy_start = 40.0D
       beam_energy_step = 1e-6
       beam_energy_num_step = 1000L
       beam_energy = DINDGEN(beam_energy_num_step) * beam_energy_step + beam_energy_start
       
       test_results = OBJ_NEW('MIR_LIST_IDL7')

       FOR ii=0, beam_energy_num_step-1 DO BEGIN
         
         ; Set the new value for beam_energy (currently things are mislabeled in the model).
         model_adas305->SET_PARAM, {bfield_z:beam_energy[ii]}

         ; Evaluate the model
         yfit = (self.core).models->EVALUATE(spectrum.x)

         ; Calculate chisq
         chisq = TOTAL((spectrum.y - yfit)^2)

         ; Calculate the area
         area = TOTAL(yfit)

         ; Get the lines from ADAS305.
         lines = model_adas305->GET_STARK_LINES()

         ; Save the results
         test_results->APPEND, { $
                                 value:beam_energy[ii] $
                                 ,chisq:chisq $
                                 ,area:area $
                                 ,lines:lines $
                               }
       ENDFOR

       self.results->SET, 'beam_energy', test_results
       
     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::TEST_BEAM_ENERGY



     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::DISPLAY_RESULTS, results, PDF=pdf

       MIR_DEFAULT, pdf, 0

       IF ~ ISA(results) THEN BEGIN
         results = self.results
       ENDIF

       results_bpbt = results->GET('bpbt')
       num_points = N_ELEMENTS(results_bpbt)
       index = INDGEN(num_points)

       plotlist = OBJ_NEW('MIR_PLOTLIST')
       plotlist->SET_DEFAULTS, {pdf:pdf $
                                ,thesis_yfactor:3}
       
       
       ; First plot out chisq vs bpbt.
       plotlist->APPEND, { $
                           x:results_bpbt.value $
                           ,y:results_bpbt.chisq $
                           ,psym:0 $
                           ,ytitle:'\chi^2' $
                         }

       ; Now plot the total intensity of the various components.
       intensity_full = DBLARR(num_points)
       intensity_half = DBLARR(num_points)
       intensity_third = DBLARR(num_points)

       FOR ii=0,num_points-1 DO BEGIN
         intensity_full[ii] = TOTAL(results_bpbt[ii].lines.full.intensity)
         intensity_half[ii] = TOTAL(results_bpbt[ii].lines.half.intensity)
         intensity_third[ii] = TOTAL(results_bpbt[ii].lines.third.intensity)
       ENDFOR

       ; Get the range.
       yrange = [MIN([intensity_full, intensity_half, intensity_third], MAX=intens_max), intens_max]
       yrange[0] = yrange[0] - (yrange[1] - yrange[0])*0.1
       yrange[1] = yrange[1] + (yrange[1] - yrange[0])*0.1

       plotlist->APPEND, { $
                           x:results_bpbt.value $
                           ,y:intensity_full $
                           ,yrange:yrange $
                           ,psym:0 $
                           ,ytitle:'Total intensity' $
                           ,legend_label:'full' $
                         }
       plotlist->APPEND, { $
                           x:results_bpbt.value $
                           ,y:intensity_half $
                           ,psym:0 $
                           ,ystyle:0 $
                           ,oplot:1 $
                           ,color:50 $
                           ,legend_label:'half' $
                         }
       plotlist->APPEND, { $
                           x:results_bpbt.value $
                           ,y:intensity_third $
                           ,psym:0 $
                           ,ystyle:0 $
                           ,oplot:1 $
                           ,color:250 $
                           ,legend_label:'third' $
                         }

       WINDOWSET, 0
       SUPERPLOT_MULTI, plotlist
       plotlist->DESTROY

       WINDOWSET, 1

       plotlist = OBJ_NEW('MIR_PLOTLIST')
       plotlist->SET_DEFAULTS, {pdf:pdf $
                                ,thesis_yfactor:3}

       yrange = [MIN(intensity_third, MAX=intens_max), intens_max]
       yrange[0] = yrange[0] - (yrange[1] - yrange[0])*0.1
       yrange[1] = yrange[1] + (yrange[1] - yrange[0])*0.1

       plotlist->APPEND, { $
                           x:results_bpbt.value $
                           ,y:intensity_third $
                           ,psym:0 $
                           ,yrange:yrange $
                           ,ytitle:'Total intensity - third component' $
                           
                         }
       SUPERPLOT_MULTI, plotlist
       plotlist->DESTROY


       WINDOWSET, 2
       
       plotlist = OBJ_NEW('MIR_PLOTLIST')
       plotlist->SET_DEFAULTS, {pdf:pdf $
                                ,decomposed:1 $
                                ,thesis_yfactor:3}

       y = (results_bpbt[118].lines.third.intensity - results_bpbt[117].lines.third.intensity)
       plotlist->APPEND, { $
                           x:results_bpbt[117].lines.third.wavelength $
                           ,y:y $
                           ,psym:1 $
                           ,yrange:[-1, 15] $
                           ,title:'Change in intensity of lines for third component.' $
                           ,xtitle:'Wavelength' $
                           ,ytitle:'\Delta Intensity' $
                         }

       y = (results_bpbt[119].lines.third.intensity - results_bpbt[118].lines.third.intensity)
       plotlist->APPEND, { $
                           x:results_bpbt[118].lines.third.wavelength $
                           ,y:y $
                           ,psym:1 $
                           ,color:MIR_COLOR('PRINT_RED') $
                           ,oplot:1 $
                         }


       y = (results_bpbt[118].lines.third.intensity - results_bpbt[117].lines.third.intensity) $
           / results_bpbt[117].lines.third.intensity
       plotlist->APPEND, { $
                           x:results_bpbt[117].lines.third.wavelength $
                           ,y:y*100E $
                           ,psym:1 $
                           ,yrange:[-1, 2] $
                           ,title:'Percentage change in intensity of lines for third component.' $
                           ,xtitle:'Wavelength' $
                           ,ytitle:'% \Delta Intensity' $
                         }

       y = (results_bpbt[119].lines.third.intensity - results_bpbt[118].lines.third.intensity) $
           / results_bpbt[118].lines.third.intensity
       plotlist->APPEND, { $
                           x:results_bpbt[118].lines.third.wavelength $
                           ,y:y*100E $
                           ,psym:1 $
                           ,color:MIR_COLOR('PRINT_RED') $
                           ,oplot:1 $
                         }

       SUPERPLOT_MULTI, plotlist
       plotlist->DESTROY
       

     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::DISPLAY_RESULTS





     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::DISPLAY_RESULTS_LIST, results $
                                                             ,XRANGE=xrange $
                                                             ,PDF=pdf

       MIR_DEFAULT, pdf, 0
       MIR_DEFAULT, xrange, [0D,0D]

       IF ~ ISA(results) THEN BEGIN
         results = self.results
       ENDIF

       pagelist = OBJ_NEW('MIR_LIST_IDL7')

       ; First plot out the bpbt results.
       IF results->HAS_KEY('bpbt') THEN BEGIN
         results_bpbt = results->GET('bpbt')
         num_points = results_bpbt->N_ELEMENTS()
         index = INDGEN(num_points)
         
         bpbt = DBLARR(num_points)
         chisq = DBLARR(num_points)
         intensity_full = DBLARR(num_points)
         intensity_half = DBLARR(num_points)
         intensity_third = DBLARR(num_points)
         
         num_lines_full = LINDGEN(num_points)
         num_lines_half = LINDGEN(num_points)
         num_lines_third = LINDGEN(num_points)
         
         FOR ii=0,num_points-1 DO BEGIN
           r = results_bpbt->GET(ii)
           bpbt[ii] = r.value
           chisq[ii] = r.chisq
           intensity_full[ii] = TOTAL(r.lines.full.intensity)
           intensity_half[ii] = TOTAL(r.lines.half.intensity)
           intensity_third[ii] = TOTAL(r.lines.third.intensity)
           
           num_lines_full[ii] = N_ELEMENTS(r.lines.full.wavelength)
           num_lines_half[ii] = N_ELEMENTS(r.lines.half.wavelength)
           num_lines_third[ii] = N_ELEMENTS(r.lines.third.wavelength)         
         ENDFOR
         

         plotlist = OBJ_NEW('MIR_PLOTLIST')
         plotlist->SET_DEFAULTS, {pdf:pdf $
                                  ,thesis_yfactor:3 $
                                  ,xrange:xrange}
         
         
         ; First plot out chisq vs bpbt.
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:chisq $
                             ,psym:0 $
                             ,ytitle:'\chi^2' $
                             ,xtitle:'B_\theta/B_T' $
                           }
         
         
         ; Get the range.
         yrange = [MIN([intensity_full, intensity_half, intensity_third], MAX=intens_max), intens_max]
         yrange[0] = yrange[0] - (yrange[1] - yrange[0])*0.1
         yrange[1] = yrange[1] + (yrange[1] - yrange[0])*0.1
         
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:intensity_full $
                             ,yrange:yrange $
                             ,psym:0 $
                             ,ytitle:'Total intensity' $
                             ,legend_label:'full' $
                           }
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:intensity_half $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:50 $
                             ,legend_label:'half' $
                           }
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:intensity_third $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:250 $
                             ,legend_label:'third' $
                           }
         
         pagelist->APPEND, plotlist, /NO_COPY
         
         
         

         plotlist = OBJ_NEW('MIR_PLOTLIST')
         plotlist->SET_DEFAULTS, {pdf:pdf $
                                  ,thesis_yfactor:3 $
                                  ,xrange:xrange}

         yrange = [MIN([num_lines_full, num_lines_half, num_lines_third], MAX=intens_max), intens_max]
         yrange[0] = yrange[0] - (yrange[1] - yrange[0])*0.1
         yrange[1] = yrange[1] + (yrange[1] - yrange[0])*0.1
         
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:num_lines_full $
                             ,yrange:yrange $
                             ,psym:0 $
                             ,ytitle:'Number of lines.' $
                             ,xtitle:'B_\theta/B_T' $
                             ,legend_label:'full' $
                           }
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:num_lines_half $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:50 $
                             ,legend_label:'half' $
                           }
         plotlist->APPEND, { $
                             x:bpbt $
                             ,y:num_lines_third $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:250 $
                             ,legend_label:'third' $
                           }
         
         pagelist->APPEND, plotlist, /NO_COPY

       ENDIF




       ; First plot out the beam_energy results.
       IF results->HAS_KEY('beam_energy') THEN BEGIN
         results_beam_energy = results->GET('beam_energy')
         num_points = results_beam_energy->N_ELEMENTS()
         index = INDGEN(num_points)
         
         beam_energy = DBLARR(num_points)
         chisq = DBLARR(num_points)
         intensity_full = DBLARR(num_points)
         intensity_half = DBLARR(num_points)
         intensity_third = DBLARR(num_points)
         
         num_lines_full = LINDGEN(num_points)
         num_lines_half = LINDGEN(num_points)
         num_lines_third = LINDGEN(num_points)
         
         FOR ii=0,num_points-1 DO BEGIN
           r = results_beam_energy->GET(ii)
           beam_energy[ii] = r.value
           chisq[ii] = r.chisq
           intensity_full[ii] = TOTAL(r.lines.full.intensity)
           intensity_half[ii] = TOTAL(r.lines.half.intensity)
           intensity_third[ii] = TOTAL(r.lines.third.intensity)
           
           num_lines_full[ii] = N_ELEMENTS(r.lines.full.wavelength)
           num_lines_half[ii] = N_ELEMENTS(r.lines.half.wavelength)
           num_lines_third[ii] = N_ELEMENTS(r.lines.third.wavelength)         
         ENDFOR
         

         plotlist = OBJ_NEW('MIR_PLOTLIST')
         plotlist->SET_DEFAULTS, {pdf:pdf $
                                  ,thesis_yfactor:3 $
                                  ,xrange:xrange}
         
         
         ; First plot out chisq vs beam_energy.
         plotlist->APPEND, { $
                             x:beam_energy $
                             ,y:chisq $
                             ,psym:0 $
                             ,ytitle:'\chi^2' $
                             ,xtitle:'Beam Energy' $
                           }
         
         
         ; Get the range.
         yrange = [MIN([intensity_full, intensity_half, intensity_third], MAX=intens_max), intens_max]
         yrange[0] = yrange[0] - (yrange[1] - yrange[0])*0.1
         yrange[1] = yrange[1] + (yrange[1] - yrange[0])*0.1
         
         plotlist->APPEND, { $
                             x:beam_energy $
                             ,y:intensity_full $
                             ,yrange:yrange $
                             ,psym:0 $
                             ,ytitle:'Total intensity' $
                             ,legend_label:'full' $
                           }
         plotlist->APPEND, { $
                             x:beam_energy $
                             ,y:intensity_half $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:50 $
                             ,legend_label:'half' $
                           }
         plotlist->APPEND, { $
                             x:beam_energy $
                             ,y:intensity_third $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:250 $
                             ,legend_label:'third' $
                           }
         
         pagelist->APPEND, plotlist, /NO_COPY

       ENDIF





       ; First plot out the b results.
       IF results->HAS_KEY('b') THEN BEGIN
         results_b = results->GET('b')
         num_points = results_b->N_ELEMENTS()
         index = INDGEN(num_points)
         
         b = DBLARR(num_points)
         chisq = DBLARR(num_points)
         intensity_full = DBLARR(num_points)
         intensity_half = DBLARR(num_points)
         intensity_third = DBLARR(num_points)
         
         num_lines_full = LINDGEN(num_points)
         num_lines_half = LINDGEN(num_points)
         num_lines_third = LINDGEN(num_points)
         
         FOR ii=0,num_points-1 DO BEGIN
           r = results_b->GET(ii)
           b[ii] = r.value
           chisq[ii] = r.chisq
           intensity_full[ii] = TOTAL(r.lines.full.intensity)
           intensity_half[ii] = TOTAL(r.lines.half.intensity)
           intensity_third[ii] = TOTAL(r.lines.third.intensity)
           
           num_lines_full[ii] = N_ELEMENTS(r.lines.full.wavelength)
           num_lines_half[ii] = N_ELEMENTS(r.lines.half.wavelength)
           num_lines_third[ii] = N_ELEMENTS(r.lines.third.wavelength)         
         ENDFOR
         

         plotlist = OBJ_NEW('MIR_PLOTLIST')
         plotlist->SET_DEFAULTS, {pdf:pdf $
                                  ,thesis_yfactor:3 $
                                  ,xrange:xrange}
         
         
         ; First plot out chisq vs b.
         plotlist->APPEND, { $
                             x:b $
                             ,y:chisq $
                             ,psym:0 $
                             ,ytitle:'\chi^2' $
                             ,xtitle:'|B|' $
                           }
         
         
         ; Get the range.
         yrange = [MIN([intensity_full, intensity_half, intensity_third], MAX=intens_max), intens_max]
         yrange[0] = yrange[0] - (yrange[1] - yrange[0])*0.1
         yrange[1] = yrange[1] + (yrange[1] - yrange[0])*0.1
         
         plotlist->APPEND, { $
                             x:b $
                             ,y:intensity_full $
                             ,yrange:yrange $
                             ,psym:0 $
                             ,ytitle:'Total intensity' $
                             ,legend_label:'full' $
                           }
         plotlist->APPEND, { $
                             x:b $
                             ,y:intensity_half $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:50 $
                             ,legend_label:'half' $
                           }
         plotlist->APPEND, { $
                             x:b $
                             ,y:intensity_third $
                             ,psym:0 $
                             ,ystyle:0 $
                             ,oplot:1 $
                             ,color:250 $
                             ,legend_label:'third' $
                           }
         
         pagelist->APPEND, plotlist, /NO_COPY

       ENDIF



       plot = OBJ_NEW('MULTIPAGE_PLOT_WIDGET')
       plot->SET_PAGE_LIST, pagelist, /NO_COPY
       plot->SHOW

     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::DISPLAY_RESULTS_LIST


     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305::CLEANUP

       ; Reset the results dictionary.
       self.results->DESTROY, /HEAP_FREE
       self.gui->DESTROY

     END ; PRO BST_INST_SCRIPT_TEST_ADAS305::CLEANUP




     ;+=================================================================
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     PRO BST_INST_SCRIPT_TEST_ADAS305__DEFINE

       struct = { BST_INST_SCRIPT_TEST_ADAS305 $

                  ,results:OBJ_NEW() $

                  ,gui:OBJ_NEW() $
                  ,core:OBJ_NEW() $
                  
                  ,INHERITS OBJECT $
                }
       
     END ; PRO BST_INST_SCRIPT_TEST_ADAS305__DEFINE
