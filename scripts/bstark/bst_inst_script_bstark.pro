


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-12
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   A collection of scripts for analysis of fits using the
;   B-Stark model.
;
;-======================================================================





     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Plot parameters from fits made using the B-Stark model.
     ;
     ;-=================================================================
     PRO BST_INST_BSTARK_PLOT_FIT_PARAM, param_dict, PDF=pdf,  NORMALIZE=normalize

       plotlist = OBJ_NEW('MIR_PLOTLIST')
       plotlist->SET_DEFAULTS, {thesis_yfactor:5 $
                                ,pdf:KEYWORD_SET(pdf)}

       MIR_LOADCT, 0, /SILENT

       num_chords = param_dict->NUM_ENTRIES()


       ; ---------------------------------------------------------------
       ; Plot the level populations.


       level_labels = [ $
                        '|2,0,0>' $
                        ,'|1,0,1>' $
                        ,'|1,1,0>' $
                        ,'|0,0,2>' $
                        ,'|0,1,1>' $
                        ,'|0,2,0>' $
                      ]

       oplot=0
       FOR ii=0,num_chords-1 DO BEGIN
         param = param_dict->GET_BY_INDEX(ii, chord)

         color_index = 254/(num_chords-1) * ii + 1


         level_population = param.level_population.full
         level_population = MIR_NORMALIZE_TO_MEAN(level_population)

         yrange = [0.8, 1.2]
         plot = {x:FINDGEN(6) $
                 ,y:level_population $
                 ,yrange:yrange $
                 ,title:'Normalized level Populations | 136735.30L | T17-T20' $
                 ,ytitle:'Level population' $
                 ,color:color_index $
                 ,legend_label:chord $
                 ,psym:-1 $
                 ,symbol_thick:2 $
                 ,xstyle:2 $
                 ,xtickinterval:1 $
                 ,xtickname:STRJOIN(level_labels, STRING(10b)) $
                 ,oplot:oplot $
                }
         oplot=1
         plotlist->APPEND, plot
       ENDFOR



       ; ---------------------------------------------------------------
       ; Plot the line widths.



       line_labels = [ $
                        '|0,2,0> -> |0,1,0>' $
                        ,'|0,1,1> -> |0,0,1>' $
                        ,'|1,1,0> -> |1,0,0>' $
                        ,'|0,1,1> -> |0,1,0>' $
                        ,'|1,1,0> -> |0,0,1>  + |0,0,2> -> |0,0,1>' $
                        ,'|1,0,1> -> |1,0,0>' $
                        ,'|1,1,0> -> |0,1,0>' $
                        ,'|1,0,1> -> |0,0,1>' $
                        ,'|2,0,0> -> |1,0,0>' $
                      ]

       oplot=0
       FOR ii=0,num_chords-1 DO BEGIN
         param = param_dict->GET_BY_INDEX(ii, chord)

         color_index = 254/(num_chords-1) * ii + 1

         title = 'Line profile width | 136735.30L | T17-T20'

         profile_width = param.profile_width.full
         IF KEYWORD_SET(normalize) THEN BEGIN
           profile_width = profile_width / profile_width[4]
           title = 'Normalized ' + title
         ENDIF

         ;yrange = [0.8, 1.2]
         plot = {x:FINDGEN(9)+1 $
                 ,y:profile_width $
                 ;,yrange:yrange $
                 ,title:'Line profile width | 136735.30L | T17-T20' $
                 ,ytitle:'Width' $
                 ,color:color_index $
                 ,legend_label:chord $
                 ,psym:-1 $
                 ,symbol_thick:2 $
                 ,xstyle:2 $
                 ,xtickinterval:1 $
                 ;,xtickname:STRJOIN(level_labels, STRING(10b)) $
                 ,oplot:oplot $
                }
         oplot=1
         plotlist->APPEND, plot
       ENDFOR

       plot = {x:[1.0, 9.0] $
               ,y:[profile_width[4], profile_width[4]] $
               ,plots:1 $
               ,linestyle:1 $
               ,data:1 $
               ,psym:0 $
              }
       plotlist->APPEND, plot


       SUPERPLOT_MULTI, plotlist
       plotlist->DESTROY




     END ;PRO BST_INST_BSTARK_PLOT_FIT_PARAM




 

     ;+=================================================================
     ; Nothing here, just for IDL auto compilation.
     ;
     ; TAGS:
     ;   EXCLUDE
     ;-=================================================================
     PRO BST_INST_SCRIPT_BSTARK
     END ;PRO BST_INST_SCRIPT_BSTARK
