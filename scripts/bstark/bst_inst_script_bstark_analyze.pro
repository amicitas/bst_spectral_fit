

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Analyze B-Stark fits to extract |B| and Bp/Bt.
;
;-======================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   Find |B| and Bp/Bt given a fitter (<BST_INST_FIT::>) object.
     ;
     ;   If a fitter object is not given, the current fitter will be extracted
     ;   from the common block.
     ;-=========================================================================
     FUNCTION BST_INST_SCRIPT_BSTARK_ANALYZE, fitter, _REF_EXTRA=extra
       COMMON BST_SPECTRAL_FIT, c_fitter
       MIR_DEFAULT, fitter, c_fitter

       bstark_model = BST_INST_GET_MODEL('BST_INST_MODEL_BSTARK', fitter)
       
       state = bstark_model->GET_STATE()

       RETURN, BST_INST_BSTARK_ANALYZE(state, _STRICT_EXTRA=extra)

     END ; FUNCTION BST_INST_SCRIPT_BSTARK_ANALYZE
