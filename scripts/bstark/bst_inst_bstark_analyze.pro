

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Analyze B-Stark fits to extract |B| and Bp/Bt.
;
;-======================================================================




     ;+=========================================================================
     ; PURPOSE:
     ;   Find |B| and Bp/Bt given the BSTARK model parameters.
     ;
     ;-=========================================================================
     FUNCTION BST_INST_BSTARK_ANALYZE, model_state, _REF_EXTRA=extra
       COMPILE_OPT STRICTARR

       RESOLVE_ROUTINE, [ $
                          'bst_analysis_bstark' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE

       input = {ratio:model_state.param.ratio $
                ,vbeam_proj:model_state.param.vbeam $
                ,er_eff:model_state.param.er_eff}

       output = BST_ANALYSIS_BSTARK( input $
                                     ,model_state.shot_param $
                                     ,_STRICT_EXTRA=extra)


       RETURN, output

     END ; FUNCTION BST_INST_BSTARK_ANALYZE
