;=======================================================================
;=======================================================================
; A collection of routines needed to find the spectrometer parameters
; given dispersion calibration shots.
;
; CERVIEW must be running for these routines to work.
;
;=======================================================================
     ;==================================================================
     ;==================================================================
     ; This is an add-on to <BST_SPECTRAL_FIT> to find the 
     ; spectrometer parameters.
     ;
     ; It assumes that a calibration shot has been analyzed with
     ; two calibration lines (most likely from a neon pen lamp).
     ;
     PRO BST_INST_DISPERSION
       COMMON BST_INSTRUMENTAL_FIT, c_inst_fit, c_spectrum, c_profile
       COMMON BST_INST_DISPERSION, c_inst_disp

       RESOLVE_ROUTINE, [ $
                          'bst_dispersion' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE

       FORWARD_FUNCTION BST_INST_DISP_GET_LAMBDA

       ; For the time being I will just set this manually
       c_inst_disp = { $
                       ne_6520:1 $
                     }

       lambda_pair = BST_INST_DISP_GET_LAMBDA()
       
       ; Channels range from 0 - 325
       num_channels = 326
       ccd_central_channel = num_channels / 2

       order = 2

       ; Get the line locations
       num_profiles = N_TAGS(c_profile)
       IF num_profiles NE 2 THEN BEGIN
         PRINT, 'BST_INST_DISPERSION: Wrong number of profiles. Should be 2.'
         RETURN
       ENDIF
       location = [c_profile.(0).centroid, c_profile.(1).centroid]

       ; Make sure that these are in the correct order
       location = location(SORT(location))

       ; Do the dispersion calculation
       PRINT, 'Dispersion calculation @ ' + lambda_pair.label
       PRINT, 'Chord ' + c_inst_fit.param.chord
       BST_DISPERSION, location - ccd_central_channel, lambda_pair.lambda*order, /VERBOSE

     END ;BST_INST_DISPERSION
     ;==================================================================
     ;================================================================== 
     FUNCTION BST_INST_DISP_GET_LAMBDA
       COMMON BST_INST_DISPERSION, c_inst_disp
       
       lambda_pair = { $
                       lambda:[0D, 0D] $
                       ,label:'' $
                     }
       CASE 1 OF
         c_inst_disp.ne_6520: BEGIN
           lambda_pair.lambda = [6506.528D, 6532.882D]
           lambda_pair.label = 'Ne 6506.5, 6532.9'
         END
         ELSE: BEGIN
           PRINT, 'BST_INST_DISP_GET_LAMBDA: Requested line pair not found.'
         END
       ENDCASE
       
       RETURN, lambda_pair
     END ;FUNCTION BST_INST_DISP_GET_LAMBDA
